'use strict'

inflateTweets([
  {
    id: '1856627347451240737',
    created: 1731489428000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/NLL" rel="noopener noreferrer" target="_blank">@NLL</a> Please post on Bluesky. There are a bunch of people over there asking for lacrosse content.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1856438740526268660',
    created: 1731444461000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bdowns328" rel="noopener noreferrer" target="_blank">@bdowns328</a> <a class="mention" href="https://x.com/AdamLeviLAX" rel="noopener noreferrer" target="_blank">@AdamLeviLAX</a> In HS, one of our defense players (also a linebacker) broke my thumb. I had to learn to play lefty or sit on the bench. It was a blessing in disguise.<br><br>In reply to: <a href="#1856438524758896837">1856438524758896837</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1856438524758896837',
    created: 1731444409000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bdowns328" rel="noopener noreferrer" target="_blank">@bdowns328</a> <a class="mention" href="https://x.com/AdamLeviLAX" rel="noopener noreferrer" target="_blank">@AdamLeviLAX</a> I can relate. I found 2 tricks to help it translate. 1) always do things with your left around the house, like opening doors or mixing ingredients. 2) imagine yourself catching and throwing left handed, esp while laying in bed. It\'s about convincing your brain to trust that side.<br><br>In reply to: <a href="https://x.com/bdowns328/status/1856436849226531285" rel="noopener noreferrer" target="_blank">@bdowns328</a> <span class="status">1856436849226531285</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1856424713079210010',
    created: 1731441116000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bdowns328" rel="noopener noreferrer" target="_blank">@bdowns328</a> <a class="mention" href="https://x.com/AdamLeviLAX" rel="noopener noreferrer" target="_blank">@AdamLeviLAX</a> Funny how there are never enough lefties, and then there are too many lefties ;)<br><br>This is a key reason why I am working so hard to only have two strong hands (as I also did back in high school).<br><br>In reply to: <a href="https://x.com/bdowns328/status/1856393711799714105" rel="noopener noreferrer" target="_blank">@bdowns328</a> <span class="status">1856393711799714105</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1856286615926108278',
    created: 1731408192000,
    type: 'post',
    text: 'Goodbye X.<br><br>I should have done this long ago. Trying out Bluesky for now (same handle, as always).',
    likes: 2,
    retweets: 0
  },
  {
    id: '1856282420363837574',
    created: 1731407191000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bdowns328" rel="noopener noreferrer" target="_blank">@bdowns328</a> <a class="mention" href="https://x.com/AdamLeviLAX" rel="noopener noreferrer" target="_blank">@AdamLeviLAX</a> I\'m with Brian on this one. It\'s a wait and see. I totally get that O\'Neill has incredible talent, but what we\'re all still trying to understand is how that translates to team success. Nonetheless, it\'s something to watch for.<br><br>In reply to: <a href="https://x.com/bdowns328/status/1856101396031905953" rel="noopener noreferrer" target="_blank">@bdowns328</a> <span class="status">1856101396031905953</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1855728749096124553',
    created: 1731275186000,
    type: 'post',
    text: 'I\'m seeing (sometimes getting) messages like "you voted for genocide, you are a piece of shit go fuck yourself." Fight hatred with hatred and tell me how that works out for you. To those people, I say: It\'s naive. It\'s childish. And it\'s myopic. Grow up. But no one\'s listening.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1855686586484101499',
    created: 1731265133000,
    type: 'post',
    text: 'Their excuse seems to be, "things aren\'t happening fast enough, so we have to take it out on other people". Yeah, that\'s not a winning strategy. It\'s also regressive.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1855538590752883113',
    created: 1731229848000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> I really want Kirst to have a great season. To me, he\'s the kind of lacrosse player I aspire to be, and a great role model for the sport. I wish him the best. I\'m also a little biased as a Cornell alum, but my opinion wouldn\'t change if I wasn\'t.<br><br>In reply to: <a href="https://x.com/danarestia/status/1853812263322550694" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1853812263322550694</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1855537153780167000',
    created: 1731229506000,
    type: 'post',
    text: 'I don\'t identify with the Green Party in the US because so many of those who do are just jerks to me and others. Of course there are plenty of good members, I\'m sure. But that\'s just not what I see. Maybe take a lesson.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1855535927248183417',
    created: 1731229213000,
    type: 'post',
    text: 'I\'ve said this before. If you attack people (especially people who are trying to help society), you aren\'t progressive. You are just a bad person. You can call yourself whatever you want, but it doesn\'t make it true.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1855535230607868287',
    created: 1731229047000,
    type: 'post',
    text: 'I\'ll just say this (until I can figure out what else I want to say). The hatred is not just coming from the right. And that just makes me 🤮.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1855450494711554532',
    created: 1731208845000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RivianTrackr" rel="noopener noreferrer" target="_blank">@RivianTrackr</a> I\'d very strongly considering adding it if they made that an option for existing owners.<br><br>In reply to: <a href="https://x.com/RivianTrackr/status/1855340032917381129" rel="noopener noreferrer" target="_blank">@RivianTrackr</a> <span class="status">1855340032917381129</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1853975479956447430',
    created: 1730857174000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nearyd" rel="noopener noreferrer" target="_blank">@nearyd</a> No, they are deprecating an older version of an action. The email is informing you that you need to update to the new version. I\'ve never gotten such an email from GitHub.<br><br>In reply to: <a href="https://x.com/nearyd/status/1853972692409495698" rel="noopener noreferrer" target="_blank">@nearyd</a> <span class="status">1853972692409495698</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1853970708621074485',
    created: 1730856036000,
    type: 'post',
    text: 'GitHub actually sent a deprecation notice (regarding GitHub Actions) in an email. I\'m both shocked and impressed. This is a better path.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1853760828291358732',
    created: 1730805997000,
    type: 'post',
    text: 'It\'s election day and I already voted days ago and my vote has already been counted. That\'s how it should be done.',
    likes: 5,
    retweets: 0
  },
  {
    id: '1853756704980357136',
    created: 1730805014000,
    type: 'post',
    text: 'In Colorado, we vote on quite a number of laws. In fact, we are voting on ranked choice voting, ironically. So Colorado operates more like a democracy than perhaps other states.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1853756081165701393',
    created: 1730804865000,
    type: 'post',
    text: 'Currently we have an oligarchy (or duopoly if you want to call it that).',
    likes: 7,
    retweets: 0
  },
  {
    id: '1853755526242279671',
    created: 1730804733000,
    type: 'post',
    text: 'I\'d be kind of okay with first election, second election, but I think it\'s just an unnecessary expense to accomplish what ranked choice voting already provides.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1853755005578145802',
    created: 1730804609000,
    type: 'post',
    text: 'In the meantime, I\'m going to vote the way my vote would be counted if we had ranked choice voting. In other words, I know who I want to vote for in my heart, but I have to play this ridiculous system until my vote can be understood properly.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1853754701965058329',
    created: 1730804536000,
    type: 'post',
    text: 'Until we have ranked choice voting, I don\'t actually consider the US to be a democracy (or republic, if you want to call it that). It\'s absurd that we have votes thrown away because your vote wasn\'t understood.',
    likes: 7,
    retweets: 0
  },
  {
    id: '1853709548135370818',
    created: 1730793771000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RockNRoLL_85" rel="noopener noreferrer" target="_blank">@RockNRoLL_85</a> <a class="mention" href="https://x.com/tmorello" rel="noopener noreferrer" target="_blank">@tmorello</a> Just did it two times tonight, in fact.<br><br>In reply to: <a href="https://x.com/RockNRoLL_85/status/1853447916989399293" rel="noopener noreferrer" target="_blank">@RockNRoLL_85</a> <span class="status">1853447916989399293</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1853608489089679546',
    created: 1730769676000,
    type: 'post',
    text: 'Based on past experience, tomorrow\'s outcome won\'t be resolved tomorrow...sadly.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1853540803848851866',
    created: 1730753539000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bsideup" rel="noopener noreferrer" target="_blank">@bsideup</a> Oh, it will come. That\'s for sure ;)<br><br>In reply to: <a href="https://x.com/bsideup/status/1853522945701728560" rel="noopener noreferrer" target="_blank">@bsideup</a> <span class="status">1853522945701728560</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1853522411032916126',
    created: 1730749154000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bsideup" rel="noopener noreferrer" target="_blank">@bsideup</a> Indeed. Though don\'t be fooled, Colorado likes to show a provisional winter, then it almost becomes summer again, and THEN it hits you after the new year. It\'s a fun time of year.<br><br>In reply to: <a href="https://x.com/bsideup/status/1853519337317118070" rel="noopener noreferrer" target="_blank">@bsideup</a> <span class="status">1853519337317118070</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1853521183477731550',
    created: 1730748861000,
    type: 'post',
    text: '',
    photos: ['<div class="item"><img class="photo" src="media/1853521183477731550.png"></div>'],
    likes: 5,
    retweets: 0
  },
  {
    id: '1853258889485066503',
    created: 1730686325000,
    type: 'post',
    text: 'First lacrosse goal today in 28 years. Let\'s go!!!',
    likes: 4,
    retweets: 0
  },
  {
    id: '1852974875335389452',
    created: 1730618611000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_gettalong" rel="noopener noreferrer" target="_blank">@_gettalong</a> <a href="https://bugs.ruby-lang.org/issues/20857" rel="noopener noreferrer" target="_blank">bugs.ruby-lang.org/issues/20857</a><br><br>In reply to: <a href="https://x.com/_gettalong/status/1852110878826803368" rel="noopener noreferrer" target="_blank">@_gettalong</a> <span class="status">1852110878826803368</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1852644599480701066',
    created: 1730539867000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> For me, it\'s a daily struggle. I can sympathize.<br><br>In reply to: <a href="https://x.com/abelsromero/status/1852633456229941507" rel="noopener noreferrer" target="_blank">@abelsromero</a> <span class="status">1852633456229941507</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1852134437326328204',
    created: 1730418235000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_gettalong" rel="noopener noreferrer" target="_blank">@_gettalong</a> Yes, the issue has been reported by the Fedora Ruby team. We\'ll see what happens, but I\'m not holding my breath.<br><br>In reply to: <a href="https://x.com/_gettalong/status/1852110878826803368" rel="noopener noreferrer" target="_blank">@_gettalong</a> <span class="status">1852110878826803368</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1852049320494674140',
    created: 1730397942000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> My experience has been similar. I don\'t like to put down programming languages, but I\'ll just say that Python has never been for me.<br><br>In reply to: <a href="https://x.com/alexsotob/status/1852036141081399434" rel="noopener noreferrer" target="_blank">@alexsotob</a> <span class="status">1852036141081399434</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1852035213850714263',
    created: 1730394578000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> JavaScript (specifically Node.js) has been much more stable in recent years. Though no one can argue that Java wins handedly in this category.<br><br>In reply to: <a href="https://x.com/alexsotob/status/1851965707598156115" rel="noopener noreferrer" target="_blank">@alexsotob</a> <span class="status">1851965707598156115</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1851935152793850061',
    created: 1730370722000,
    type: 'post',
    text: 'I\'ve learned about another backward incompatibility introduced in Ruby, this time a change in how the to_s method formats a Hash...for seemingly no reason. Ruby is continuing to erode my trust.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1851817100924305887',
    created: 1730342576000,
    type: 'post',
    text: 'I don\'t think anyone even remotely understands how fucking angry I am about this piece of shit medical system we have in this country. Just think about someone who just ran over your dog. I\'m way more upset than that.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1851816803607155157',
    created: 1730342505000,
    type: 'post',
    text: 'I\'m strongly considering concierge medicine because, frankly, I don\'t want to die from having doctors that don\'t give a shit. It\'s wrong that I even need it. While it will add financial pressure, I\'m fortunate to even be able to consider it. But it could save my life one day.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1851545795582943512',
    created: 1730277892000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/vegasdesertdogs" rel="noopener noreferrer" target="_blank">@vegasdesertdogs</a> I really enjoy playing wall ball at rest areas on long road trips. Nothing gets the blood flowing again to the legs quite like it.<br><br>In reply to: <a href="#1851543453231890735">1851543453231890735</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1851543453231890735',
    created: 1730277333000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/vegasdesertdogs" rel="noopener noreferrer" target="_blank">@vegasdesertdogs</a> Wall ball of my dreams. Oh yes.<br><br>In reply to: <a href="https://x.com/vegasdesertdogs/status/1851374553248780476" rel="noopener noreferrer" target="_blank">@vegasdesertdogs</a> <span class="status">1851374553248780476</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1851412546441359660',
    created: 1730246123000,
    type: 'post',
    text: 'I find ballot guides from political organizations I follow to be very useful. It\'s not the "how should I vote" that I care about so much as the "why should I vote for or against" part. I like to understand the pros and cons so I can think about how they align with my values.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1851412123886260430',
    created: 1730246022000,
    type: 'post',
    text: 'Just because it\'s new doesn\'t mean it\'s good.',
    likes: 3,
    retweets: 1
  },
  {
    id: '1851360953604190327',
    created: 1730233822000,
    type: 'post',
    text: 'I show up at the lab today for my appointment and they say "we see you have an appointment, but you don\'t have any orders." Our medical system no longer functions. I\'ve never had this happen to me in my whole life. But it\'s commonplace now. There\'s a disconnect.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1851360549663379875',
    created: 1730233726000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AdamLeviLAX" rel="noopener noreferrer" target="_blank">@AdamLeviLAX</a> <a class="mention" href="https://x.com/AlbFireWolves" rel="noopener noreferrer" target="_blank">@AlbFireWolves</a> Btw, I wish the Redwoods would trade him because they simply do not know how to use or appreciate him.<br><br>In reply to: <a href="#1851348485855171056">1851348485855171056</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1851348485855171056',
    created: 1730230850000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AdamLeviLAX" rel="noopener noreferrer" target="_blank">@AdamLeviLAX</a> <a class="mention" href="https://x.com/AlbFireWolves" rel="noopener noreferrer" target="_blank">@AlbFireWolves</a> I\'m so excited to see him play again. Let\'s go!<br><br>In reply to: <a href="https://x.com/AdamLeviLAX/status/1851028946432790903" rel="noopener noreferrer" target="_blank">@AdamLeviLAX</a> <span class="status">1851028946432790903</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1851177679665709158',
    created: 1730190126000,
    type: 'post',
    text: 'If allowed to use ranked choice voting, Harris would be behind whichever party or parties I deem more progressive and compassionate. But my vote would ultimately count for Harris until this country wakes up. So in my mind, I will vote for how my vote would be counted.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1851176812459774219',
    created: 1730189919000,
    type: 'post',
    text: 'I hate the 2 party system. I hate that we don\'t have ranked choice voting. I hate that we have an archaic election system that produces choas every election. I hate that we pride ourselves on being a democracy but don\'t fully subscribe to it at home. But I hate bigots more.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1851175250186420695',
    created: 1730189547000,
    type: 'quote',
    text: 'One of few American politicians with a heart and the guts to speak truth. He puts into words precisely why I\'m voting for Harris while also capturing how I feel about doing so. I\'m mad as hell about the inaction of Democrats, but I\'ll be even more pissed to see bigots in power.<br><br>Quoting: <a href="https://x.com/BernieSanders/status/1851040553745432775" rel="noopener noreferrer" target="_blank">@BernieSanders</a> <span class="status">1851040553745432775</span>',
    likes: 8,
    retweets: 2
  },
  {
    id: '1850644974704226577',
    created: 1730063119000,
    type: 'post',
    text: 'I\'ve never felt so much despair going into a Presidential election. At best, we mitigate an absolute nightmare scenario, but we only do so knowing at least half the country is trying to usher in that havoc. It\'s just pathetic.',
    likes: 3,
    retweets: 1
  },
  {
    id: '1850640294217453995',
    created: 1730062004000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> <a class="mention" href="https://x.com/joaompinto4" rel="noopener noreferrer" target="_blank">@joaompinto4</a> 💯 The point is, they are solving 0 problems. What big or small thing has been done? And they are wasting so much time failing to solve small problems. It\'s just unacceptable.<br><br>In reply to: <a href="https://x.com/starbuxman/status/1850604239011103210" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">1850604239011103210</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1850422104635699322',
    created: 1730009983000,
    type: 'post',
    text: 'Most of us have to go to work and do our job to get paid. Not politicians.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1850420498640814176',
    created: 1730009600000,
    type: 'post',
    text: 'FFS, I thought we got rid of daylight savings time in the US. I just searched an apparently the bill failed to pass. I swear politicians are the most useless people on this planet. Can you even do one thing other than make speeches?',
    likes: 4,
    retweets: 1
  },
  {
    id: '1850282120368869542',
    created: 1729976608000,
    type: 'quote',
    text: 'This is the only take in politics that I want to hear. I want to know you will burn your political capital to get shit done. End of story.<br><br>Quoting: <a href="https://x.com/ArtCandee/status/1849933041021390913" rel="noopener noreferrer" target="_blank">@ArtCandee</a> <span class="status">1849933041021390913</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1850280544782684618',
    created: 1729976233000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> There are states controlled by Democrats that don\'t have this system and there\'s 0 excuse for it. Until every D state has it, I don\'t want to hear their whining because it\'s simply ineptness. Yes, disenfranchising voters is an R strategy, but a D one too apparently.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1850273991803670953" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1850273991803670953</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1850248871466717678',
    created: 1729968681000,
    type: 'quote',
    text: 'This is really how voting should be. Waiting in lines is such an outdated model. Colorado gets it. Many other states do not. Are we are democracy, or are we not?<br><br>Quoting: <a href="https://x.com/GovofCO/status/1850239530571588048" rel="noopener noreferrer" target="_blank">@GovofCO</a> <span class="status">1850239530571588048</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1849996386390937885',
    created: 1729908484000,
    type: 'post',
    text: 'If you\'re looking for good earbuds, honestly the nothing ear wireless earbuds are incredible.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1849912159658205683',
    created: 1729888403000,
    type: 'post',
    text: 'Email client creators: "We\'ve exhausted every idea that can be added to an email client. There are just no more features to implement."<br><br>Us: "How about a button to compose a new email to the same recipients as the email I\'m looking at?"<br><br>Email client creators: 🤔',
    likes: 2,
    retweets: 0
  },
  {
    id: '1849869396392685601',
    created: 1729878207000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Rivian" rel="noopener noreferrer" target="_blank">@Rivian</a> You should have seen the look on my spouse\'s face when I activated the costumes for the first time. It was like she had seen a ghost!<br><br>In reply to: <a href="https://x.com/Rivian/status/1848786737192992839" rel="noopener noreferrer" target="_blank">@Rivian</a> <span class="status">1848786737192992839</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1849565047372099826',
    created: 1729805645000,
    type: 'post',
    text: 'An awesome feature of Cornell men\'s lacrosse by Mitchell Pehlke. A must watch for the Big Red faithful. <a href="https://youtu.be/-u7T4zh2prg" rel="noopener noreferrer" target="_blank">youtu.be/-u7T4zh2prg</a> <a class="mention" href="https://x.com/CornellLacrosse" rel="noopener noreferrer" target="_blank">@CornellLacrosse</a> #YellCornell',
    likes: 1,
    retweets: 0,
    tags: ['yellcornell']
  },
  {
    id: '1849523746614169967',
    created: 1729795798000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Rivian" rel="noopener noreferrer" target="_blank">@Rivian</a> Glad to see the center console organizer to fill in the black hole. I love the black hole, but something to cap it, very helpful.<br><br>In reply to: <a href="https://x.com/Rivian/status/1849519537559130571" rel="noopener noreferrer" target="_blank">@Rivian</a> <span class="status">1849519537559130571</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1849522768041386030',
    created: 1729795565000,
    type: 'post',
    text: 'Is it true that if a song is available in Dolby Atmos, it\'s not necessarily available on all streaming services that support Dolby Atmos? For example, you can find a song in Apple Music in Dolby Atmos that\'s not available in Tidal, and vice versa. If true, that\'s really bizarre.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1849520927064613328',
    created: 1729795126000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Rivian" rel="noopener noreferrer" target="_blank">@Rivian</a> I like the idea of the power bank, though I wish it would still be a flashlight (even if it\'s not quite as strong). If it were both, that would be an immediate buy for me (because I tend to get chilly hands).<br><br>In reply to: <a href="https://x.com/Rivian/status/1849519537559130571" rel="noopener noreferrer" target="_blank">@Rivian</a> <span class="status">1849519537559130571</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1849190135872561397',
    created: 1729716259000,
    type: 'post',
    text: 'Saying "look, we launched a new website design!" is kind of like a store saying "look, we remodeled our building". It\'s something we will notice, sure, but not something we need to be told. Congratulate yourself instead. Perhaps you\'ll get more sales, that\'s the recognition.',
    likes: 5,
    retweets: 0
  },
  {
    id: '1849004613942346241',
    created: 1729672027000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> <a class="mention" href="https://x.com/jreleaser" rel="noopener noreferrer" target="_blank">@jreleaser</a> <a class="mention" href="https://x.com/sonatype" rel="noopener noreferrer" target="_blank">@sonatype</a> <a class="mention" href="https://x.com/sonatype_ops" rel="noopener noreferrer" target="_blank">@sonatype_ops</a> Oh, I see. I was looking at it from the lens of publishing a new artifact. I didn\'t realize there was a migration for existing namespaces. But it makes sense.<br><br>In reply to: <a href="https://x.com/aalmiray/status/1848994136738680983" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">1848994136738680983</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1848851384751259875',
    created: 1729635494000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> <a class="mention" href="https://x.com/jreleaser" rel="noopener noreferrer" target="_blank">@jreleaser</a> <a class="mention" href="https://x.com/sonatype" rel="noopener noreferrer" target="_blank">@sonatype</a> <a class="mention" href="https://x.com/sonatype_ops" rel="noopener noreferrer" target="_blank">@sonatype_ops</a> I thought that whole system was being deprecated. I\'m not using to to publish the Antora Maven plugin. Instead, I\'m using the new system, which seems to bypass all the quirks from the OSSRH system.<br><br>In reply to: <a href="https://x.com/aalmiray/status/1848802846386393346" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">1848802846386393346</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1848786871083798667',
    created: 1729620113000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> So glad to see the Unleashed players as part of this graphic. It\'s a sign of good things to come. Now that they have proven they can play box, how about let them play PLL rules next.<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1848712602731307303" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1848712602731307303</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1848546539553345668',
    created: 1729562814000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MammothLax" rel="noopener noreferrer" target="_blank">@MammothLax</a> Can\'t wait to attend my first game in person. This might be the year!<br><br>In reply to: <a href="https://x.com/MammothLax/status/1848508781891916226" rel="noopener noreferrer" target="_blank">@MammothLax</a> <span class="status">1848508781891916226</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1848142649665986636',
    created: 1729466519000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/danhillenbrand" rel="noopener noreferrer" target="_blank">@danhillenbrand</a> 👍<br><br>In reply to: <a href="https://x.com/marcsavy/status/1848142050610303324" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1848142050610303324</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1848140067967005091',
    created: 1729465903000,
    type: 'post',
    text: 'Second box lacrosse game in the books. Not quite as good of a showing as the first game, though just inches from a goal. 🥍🥅 I\'m still struggling with getting my legs to move the way I want them too and without soreness. There\'s no question this dialing up my conditioning.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1848138629396169021',
    created: 1729465560000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/eriklamb" rel="noopener noreferrer" target="_blank">@eriklamb</a> <a class="mention" href="https://x.com/Riviaan" rel="noopener noreferrer" target="_blank">@Riviaan</a> <a class="mention" href="https://x.com/Rivian" rel="noopener noreferrer" target="_blank">@Rivian</a> I should at least be able to play my cached playlist. Yes, I can play it over bluetooth, but then the speaker system doesn\'t work the same, so it kind of sucks.<br><br>In reply to: <a href="#1848138303356215345">1848138303356215345</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1848138303356215345',
    created: 1729465482000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/eriklamb" rel="noopener noreferrer" target="_blank">@eriklamb</a> <a class="mention" href="https://x.com/Riviaan" rel="noopener noreferrer" target="_blank">@Riviaan</a> <a class="mention" href="https://x.com/Rivian" rel="noopener noreferrer" target="_blank">@Rivian</a> One of the main issues with Apple Music is that it doesn\'t cache. So if you aren\'t connected to a data service (Connect+ or wi-fi from mobile hotspot) then you just can\'t play anything. So there is blame on the streaming service integration here too.<br><br>In reply to: <a href="#1848137955530919982">1848137955530919982</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1848137955530919982',
    created: 1729465399000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/eriklamb" rel="noopener noreferrer" target="_blank">@eriklamb</a> <a class="mention" href="https://x.com/Riviaan" rel="noopener noreferrer" target="_blank">@Riviaan</a> <a class="mention" href="https://x.com/Rivian" rel="noopener noreferrer" target="_blank">@Rivian</a> I think you have a very valid criticism. When I leased, I do remember the documents saying that the LTE service wouldn\'t be included indefinitely, but the details weren\'t clear. I can understand why Rivian needs to charge for it, but the communication about it has been very poor.<br><br>In reply to: <a href="https://x.com/eriklamb/status/1848026671183741034" rel="noopener noreferrer" target="_blank">@eriklamb</a> <span class="status">1848026671183741034</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1847783585798832310',
    created: 1729380911000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/danhillenbrand" rel="noopener noreferrer" target="_blank">@danhillenbrand</a> Yep, much much worse. I mean, at this point, I feel like talking to a doctor is like talking to a stranger on a train. Just no interest at all.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1847754519943262510" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1847754519943262510</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1847543185699996141',
    created: 1729323595000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> Whenever your\'re tempted, just say to yourself, "Don\'t do it, Emmanuel!"<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1847427888611807655" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1847427888611807655</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1847533652076626141',
    created: 1729321322000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danhillenbrand" rel="noopener noreferrer" target="_blank">@danhillenbrand</a> Quite the contrary. This is the worst it had ever been. Honestly, if we don\'t get a universal system some point soon (and get it out of the likes of corporate influence), I really worry about generations to come.<br><br>In reply to: <a href="https://x.com/danhillenbrand/status/1847485980380364998" rel="noopener noreferrer" target="_blank">@danhillenbrand</a> <span class="status">1847485980380364998</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1847443141953221079',
    created: 1729299743000,
    type: 'post',
    text: 'I saw a new primary care physician today since my new insurance doesn\'t accept my previous one. I was asked "do you have a history of medical conditions in your family?" Are you fucking serious?!? I\'m 46 and starting over. You have to be your own doctor in this country.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1846988487779647558',
    created: 1729191345000,
    type: 'reply',
    text: 'After some testing, I now understand that it\'s the cell service that you are paying for. If you hook to Wi-Fi that you provide somehow, then the service works again. This was very poorly communicated by <a class="mention" href="https://x.com/Rivian" rel="noopener noreferrer" target="_blank">@Rivian</a>.<br><br>In reply to: <a href="#1846701179885019392">1846701179885019392</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1846823058365206713',
    created: 1729151904000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DBurkland" rel="noopener noreferrer" target="_blank">@DBurkland</a> <a class="mention" href="https://x.com/omg_tesla" rel="noopener noreferrer" target="_blank">@omg_tesla</a> I\'m quite certain it\'s streaming (and not caching) the lossless files (and spacial if available and enabled). I confirmed by tethering and checked the size of the file it\'s pulling. They are massive files.<br><br>In reply to: <a href="https://x.com/DBurkland/status/1846776732461785447" rel="noopener noreferrer" target="_blank">@DBurkland</a> <span class="status">1846776732461785447</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1846822730047307971',
    created: 1729151825000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/omg_tesla" rel="noopener noreferrer" target="_blank">@omg_tesla</a> I\'m very eager to try Tidal. I\'ve been deeply testing Apple Music for the last month and the tonal quality is all over the place (can be good, but can also be really bad). Since I\'m very familiar with how the songs sound I\'ve been playing, I\'ll be able to do some A/B testing.<br><br>In reply to: <a href="https://x.com/omg_tesla/status/1846771432526958765" rel="noopener noreferrer" target="_blank">@omg_tesla</a> <span class="status">1846771432526958765</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1846820456482894018',
    created: 1729151283000,
    type: 'post',
    text: 'Dolby Atmos makes you appreciate classic rock so much more (Rush, Fleetwood Mac, The Who, etc). There\'s such a depth of sound in those recordings that was captured, but rarely before heard. Wow. Analog (remastered and presented with digital) FTW!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1846806258579390648',
    created: 1729147898000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Riviaan" rel="noopener noreferrer" target="_blank">@Riviaan</a> <a class="mention" href="https://x.com/Rivian" rel="noopener noreferrer" target="_blank">@Rivian</a> Now I get what you were saying.<br><br>In reply to: <a href="https://x.com/Riviaan/status/1846713978531062247" rel="noopener noreferrer" target="_blank">@Riviaan</a> <span class="status">1846713978531062247</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1846805837941010633',
    created: 1729147798000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Riviaan" rel="noopener noreferrer" target="_blank">@Riviaan</a> <a class="mention" href="https://x.com/Rivian" rel="noopener noreferrer" target="_blank">@Rivian</a> Okay, after some testing, I now understand that it\'s the mobile internet connection that is cut off without Connect+. If I tether to my phone\'s Wi-Fi, then the streaming app works. Rivian did a terrible job of communicating this to customers. Terrible.<br><br>In reply to: <a href="#1846714929732018325">1846714929732018325</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1846714929732018325',
    created: 1729126124000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Riviaan" rel="noopener noreferrer" target="_blank">@Riviaan</a> <a class="mention" href="https://x.com/Rivian" rel="noopener noreferrer" target="_blank">@Rivian</a> It\'s a dishonest move. It\'s not what I expect from Rivian. I know how to use hotspot another way. But to cripple the audio system is ridiculous. They either don\'t want to sell this service or they just want to turn away customers. This is not why I joined this community.<br><br>In reply to: <a href="https://x.com/Riviaan/status/1846713978531062247" rel="noopener noreferrer" target="_blank">@Riviaan</a> <span class="status">1846713978531062247</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1846712983742775572',
    created: 1729125660000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Riviaan" rel="noopener noreferrer" target="_blank">@Riviaan</a> <a class="mention" href="https://x.com/Rivian" rel="noopener noreferrer" target="_blank">@Rivian</a> I\'m find with having to pay for unique services provided by Connect+, such as hotspot. In fact, that\'s the closest thing to your analogy.<br><br>In reply to: <a href="#1846712760543203365">1846712760543203365</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1846712760543203365',
    created: 1729125607000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Riviaan" rel="noopener noreferrer" target="_blank">@Riviaan</a> <a class="mention" href="https://x.com/Rivian" rel="noopener noreferrer" target="_blank">@Rivian</a> No it\'s not, it\'s the same application.<br><br>In reply to: <a href="https://x.com/Riviaan/status/1846709596951310637" rel="noopener noreferrer" target="_blank">@Riviaan</a> <span class="status">1846709596951310637</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1846701179885019392',
    created: 1729122845000,
    type: 'post',
    text: 'It\'s absolutely absurd to me that I have to pay <a class="mention" href="https://x.com/Rivian" rel="noopener noreferrer" target="_blank">@Rivian</a> for a Connect+ subscription in order to pay Apple Music for a subscription to stream music. That makes no damn sense.',
    likes: 5,
    retweets: 1
  },
  {
    id: '1846666561391939802',
    created: 1729114592000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RivianTrackr" rel="noopener noreferrer" target="_blank">@RivianTrackr</a> Sound quality is highly subjective. They are trying to do something new with how these songs sounds. It\'s an experiment. I think the Rivian is pretty faithfully playing the songs, so it comes down to whether people even like Dolby Atmos. As I said, I\'m still evaluating it myself.<br><br>In reply to: <a href="#1846666291182030974">1846666291182030974</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1846666291182030974',
    created: 1729114527000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RivianTrackr" rel="noopener noreferrer" target="_blank">@RivianTrackr</a> I\'ve been studying this very thoroughly. There\'s no question that some Dolby Atmos songs in Apple Music are way out of range. So it definitely requires selecting songs carefully. To me, those songs sound really nice, but it is a more airy sound than when not using Dolby.<br><br>In reply to: <a href="https://x.com/RivianTrackr/status/1846623354209685920" rel="noopener noreferrer" target="_blank">@RivianTrackr</a> <span class="status">1846623354209685920</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1846111350151369158',
    created: 1728982219000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/steveo2025" rel="noopener noreferrer" target="_blank">@steveo2025</a> <a class="mention" href="https://x.com/RivianTrackr" rel="noopener noreferrer" target="_blank">@RivianTrackr</a> Same. For now, I only listen to Dolby Atmos songs, but I know that\'s not an acceptable situation. The reason I\'m doing it is really just to understand what it sounds like for comparison as other services get rolled out.<br><br>In reply to: <a href="https://x.com/steveo2025/status/1844923658457755763" rel="noopener noreferrer" target="_blank">@steveo2025</a> <span class="status">1844923658457755763</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1846110764387406028',
    created: 1728982079000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RivianTrackr" rel="noopener noreferrer" target="_blank">@RivianTrackr</a> I\'m so glad to see an option other than Apple for Dolby Atmos. There\'s no reason we should be locked into a single service, and I really don\'t like Apple. It will be nice to have a more device-agnostic service available.<br><br>In reply to: <a href="https://x.com/RivianTrackr/status/1844909412273541491" rel="noopener noreferrer" target="_blank">@RivianTrackr</a> <span class="status">1844909412273541491</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1845912570680447072',
    created: 1728934826000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/codeslubber" rel="noopener noreferrer" target="_blank">@codeslubber</a> It\'s just a style choice. There\'s no mandate to do that. I prefer it. Some do not. Neither is right or wrong. What\'s most important is that you feel comfortable writing.<br><br>In reply to: <a href="https://x.com/codeslubber/status/1845906365660672467" rel="noopener noreferrer" target="_blank">@codeslubber</a> <span class="status">1845906365660672467</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1845777354737565979',
    created: 1728902588000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/WorldLacrosse" rel="noopener noreferrer" target="_blank">@WorldLacrosse</a> Oh, that\'s excellent! I\'ll make another post to promote it because it\'s a must see! That Ally Kennedy goal to get it started had us dancing from the get go.<br><br>In reply to: <a href="https://x.com/WorldLacrosse/status/1845760877313528302" rel="noopener noreferrer" target="_blank">@WorldLacrosse</a> <span class="status">1845760877313528302</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1845639505493258610',
    created: 1728869723000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bdowns328" rel="noopener noreferrer" target="_blank">@bdowns328</a> I used that Powell stick and passes we\'re on the money...well, except for the one to the other team, but that was because I was still learning what team I\'m on.<br><br>In reply to: <a href="#1845639142585331868">1845639142585331868</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1845639142585331868',
    created: 1728869636000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bdowns328" rel="noopener noreferrer" target="_blank">@bdowns328</a> My legs were like "what are we doing!?!" I also run like a Tasmanian Devil, so maybe I need to dial it back slightly. Definitely going back. Playing though December and hopefully beyond.<br><br>The equipment now is so incredible it makes it more fun.<br><br>In reply to: <a href="https://x.com/bdowns328/status/1845611734666739966" rel="noopener noreferrer" target="_blank">@bdowns328</a> <span class="status">1845611734666739966</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1845553705460158853',
    created: 1728849266000,
    type: 'post',
    text: 'First box lacrosse game of my life in the books. No team practice, just right into the game. I\'m still in one piece, so that\'s a win. Wow, it\'s fast. And I got winded so quick. Lots of room for improvement for sure.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1845353895293247742',
    created: 1728801628000,
    type: 'quote',
    text: 'If you have espn+, go watch this game. It was one of the best box lacrosse games I\'ve ever witnessed. Women rock at box lacrosse.<br><br>Quoting: <a href="https://x.com/WorldLacrosse/status/1840503513522049248" rel="noopener noreferrer" target="_blank">@WorldLacrosse</a> <span class="status">1840503513522049248</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1845353591013273804',
    created: 1728801555000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/WorldLacrosse" rel="noopener noreferrer" target="_blank">@WorldLacrosse</a> Without a doubt. MVP across both men and women. Her performance was like nothing I\'ve ever seen. She could have easily been competing in a track meet at the same time and winning that too. I think the NLL should be giving her a look.<br><br>In reply to: <a href="https://x.com/WorldLacrosse/status/1840523166872903886" rel="noopener noreferrer" target="_blank">@WorldLacrosse</a> <span class="status">1840523166872903886</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1845353048580780444',
    created: 1728801426000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/WorldLacrosse" rel="noopener noreferrer" target="_blank">@WorldLacrosse</a> The women\'s gold medal game was electrifying. It was easily the best game of the whole tournament. Women\'s box lacrosse has arrived (internationally) and is here to stay. I hope it continues to grow faster than any other format. I\'ve been waiting so long for this turning point.<br><br>In reply to: <a href="https://x.com/WorldLacrosse/status/1840754724456595566" rel="noopener noreferrer" target="_blank">@WorldLacrosse</a> <span class="status">1840754724456595566</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1844285866480726187',
    created: 1728546990000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Rivian" rel="noopener noreferrer" target="_blank">@Rivian</a> <a class="mention" href="https://x.com/BillionOyster" rel="noopener noreferrer" target="_blank">@BillionOyster</a> Such a cool idea.<br><br>In reply to: <a href="https://x.com/Rivian/status/1844052657792799001" rel="noopener noreferrer" target="_blank">@Rivian</a> <span class="status">1844052657792799001</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1844274632217723318',
    created: 1728544311000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Devoxx" rel="noopener noreferrer" target="_blank">@Devoxx</a> Time to pop the Belgian Trappist in the fridge and get watching!<br><br>In reply to: <a href="https://x.com/Devoxx/status/1844261325784170575" rel="noopener noreferrer" target="_blank">@Devoxx</a> <span class="status">1844261325784170575</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1843917906910032041',
    created: 1728459262000,
    type: 'post',
    text: 'This is getting very real. I just got a full set of pads for playing box lacrosse. Am I insane? Probably. Am I going to have fun? Hopefully! We\'ll find out Sunday. Age is just a number (I hope).',
    likes: 3,
    retweets: 0
  },
  {
    id: '1843771231709082074',
    created: 1728424291000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GeoffreyDeSmet" rel="noopener noreferrer" target="_blank">@GeoffreyDeSmet</a> <a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <a class="mention" href="https://x.com/TCoolsIT" rel="noopener noreferrer" target="_blank">@TCoolsIT</a> I believe that was the same year I said Hello to the world in Java too (at Cornell).<br><br>In reply to: <a href="https://x.com/GeoffreyDeSmet/status/1843702378576654488" rel="noopener noreferrer" target="_blank">@GeoffreyDeSmet</a> <span class="status">1843702378576654488</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1843745427566539015',
    created: 1728418139000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kevindubois" rel="noopener noreferrer" target="_blank">@kevindubois</a> <a class="mention" href="https://x.com/QuarkusIO" rel="noopener noreferrer" target="_blank">@QuarkusIO</a> <a class="mention" href="https://x.com/clementplop" rel="noopener noreferrer" target="_blank">@clementplop</a> <a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <a class="mention" href="https://x.com/gsmet_" rel="noopener noreferrer" target="_blank">@gsmet_</a> <a class="mention" href="https://x.com/geoand86" rel="noopener noreferrer" target="_blank">@geoand86</a> <a class="mention" href="https://x.com/xstefank" rel="noopener noreferrer" target="_blank">@xstefank</a> <a class="mention" href="https://x.com/edeandrea" rel="noopener noreferrer" target="_blank">@edeandrea</a> Of course. Always! Love to see new and familiar faces!<br><br>In reply to: <a href="https://x.com/kevindubois/status/1843738576502304869" rel="noopener noreferrer" target="_blank">@kevindubois</a> <span class="status">1843738576502304869</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1843422025408459116',
    created: 1728341034000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ReneSchwietzke" rel="noopener noreferrer" target="_blank">@ReneSchwietzke</a> Wow, the space got lighter! Almost didn\'t recognize it.<br><br>In reply to: <a href="https://x.com/ReneSchwietzke/status/1843189868584902747" rel="noopener noreferrer" target="_blank">@ReneSchwietzke</a> <span class="status">1843189868584902747</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1843421181904621937',
    created: 1728340833000,
    type: 'quote',
    text: 'Scenes from the train station in Antwerpen never get old. Certainly a sight for sore eyes, as they say. Keep \'em coming. #Devoxx<br><br>Quoting: <a href="https://x.com/ogeisser/status/1843311038487552112" rel="noopener noreferrer" target="_blank">@ogeisser</a> <span class="status">1843311038487552112</span>',
    likes: 1,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '1843365491135685053',
    created: 1728327555000,
    type: 'post',
    text: 'What I see is people lamenting about the devastation of the hurricane and floods, then doing absolutely nothing to help mitigate climate change. Actions speak louder than words, folks.',
    likes: 3,
    retweets: 1
  },
  {
    id: '1843204668996174009',
    created: 1728289212000,
    type: 'post',
    text: 'I really wish Node.js provided a YAML parser in the stdlib.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1842175646812020952',
    created: 1728043874000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/WassymBensaid" rel="noopener noreferrer" target="_blank">@WassymBensaid</a> <a class="mention" href="https://x.com/RivianTrackr" rel="noopener noreferrer" target="_blank">@RivianTrackr</a> I appreciate you\'re working to get it right. We are all definitely looking forward to it, but that\'s all the more reason to take the time it takes. It will be a good day when it launches.<br><br>In reply to: <a href="https://x.com/WassymBensaid/status/1841646477904642245" rel="noopener noreferrer" target="_blank">@WassymBensaid</a> <span class="status">1841646477904642245</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1842174626048409716',
    created: 1728043631000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RivianUpdates" rel="noopener noreferrer" target="_blank">@RivianUpdates</a> My mom saw it this weekend and told me she thought it was really well done. So it\'s resonating.<br><br>In reply to: <a href="https://x.com/RivianUpdates/status/1841925909583929719" rel="noopener noreferrer" target="_blank">@RivianUpdates</a> <span class="status">1841925909583929719</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1842097749497409677',
    created: 1728025302000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bsideup" rel="noopener noreferrer" target="_blank">@bsideup</a> <a class="mention" href="https://x.com/dev2next" rel="noopener noreferrer" target="_blank">@dev2next</a> I\'m glad I finally had a chance to meet you in person!<br><br>In reply to: <a href="https://x.com/bsideup/status/1840755336308728256" rel="noopener noreferrer" target="_blank">@bsideup</a> <span class="status">1840755336308728256</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1842071454713577937',
    created: 1728019033000,
    type: 'post',
    text: 'Node.js is going in the right direction. Just used fs.glob and it\'s awesome. This just makes scripting that much simpler. (Yes, JavaScript still has it\'s quirks to be sure, but it\'s good for the purpose it serves and is improving).',
    likes: 2,
    retweets: 0
  },
  {
    id: '1841542571778072785',
    created: 1727892937000,
    type: 'post',
    text: 'When I\'m presented with a login screen for a service I use often, my impulse is now to curse at the screen...every. single. time.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1841538523586810336',
    created: 1727891972000,
    type: 'post',
    text: 'First day of PSL. Bring on the fall. ☕️',
    likes: 0,
    retweets: 0
  },
  {
    id: '1841379505417371970',
    created: 1727854059000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> Happy Birthday, buddy! I\'m going to kick back and watch some Devoxx talks next week with a 🍻 and imagining that we\'re hanging out again. 🎂',
    likes: 2,
    retweets: 0
  },
  {
    id: '1841258763514495444',
    created: 1727825272000,
    type: 'post',
    text: 'If customer support calls you, and you can\'t answer the phone, you\'ll never be able to get back to that advisor to continue the conversation. Customer support systems are so unbelievably broken it\'s just embarrassing. There has to be some sort of reentry flow.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1841256844754317780',
    created: 1727824815000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bsideup" rel="noopener noreferrer" target="_blank">@bsideup</a> I don\'t mind the diversity...it usually leads to better options. I\'m just very surprised Samsung\'s app is lagging behind given it\'s a core feature of the phone.<br><br>In reply to: <a href="https://x.com/bsideup/status/1841230511697453376" rel="noopener noreferrer" target="_blank">@bsideup</a> <span class="status">1841230511697453376</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1841230277248761933',
    created: 1727818481000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bsideup" rel="noopener noreferrer" target="_blank">@bsideup</a> The Samsung messaging apps lacks this feature, but I can confirm the Google message app has it. I\'ll just switch to it and see how it goes.<br><br>In reply to: <a href="https://x.com/bsideup/status/1841228887726178704" rel="noopener noreferrer" target="_blank">@bsideup</a> <span class="status">1841228887726178704</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1841228723154264268',
    created: 1727818110000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bsideup" rel="noopener noreferrer" target="_blank">@bsideup</a> Awesome! I\'m going to try that. Thanks for the tip!<br><br>In reply to: <a href="https://x.com/bsideup/status/1841226858500964859" rel="noopener noreferrer" target="_blank">@bsideup</a> <span class="status">1841226858500964859</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1841225963515875735',
    created: 1727817452000,
    type: 'post',
    text: 'Do you think messaging apps could automatically detect and delete one-time use codes after say 10min so I don\'t have to spend 5 minutes every night deleting them?',
    likes: 2,
    retweets: 0
  },
  {
    id: '1841085880800591917',
    created: 1727784054000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RivianUpdates" rel="noopener noreferrer" target="_blank">@RivianUpdates</a> I freaked out when I saw you reel. So glad you are okay. I know it feels terrible right now, but focus on the positive and look for the silver lining to get though it.<br><br>In reply to: <a href="https://x.com/RivianUpdates/status/1841008669888004101" rel="noopener noreferrer" target="_blank">@RivianUpdates</a> <span class="status">1841008669888004101</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1841056259014906072',
    created: 1727776991000,
    type: 'post',
    text: 'I\'m super excited to tune into the <a class="mention" href="https://x.com/Devoxx" rel="noopener noreferrer" target="_blank">@Devoxx</a> talks, now just a week away. (I really wish I could be there in person, but it\'s not possible for me to do so right now). Time to go shopping for some Belgian delicacies to enjoy while I\'m watching. 🍺🍫',
    likes: 10,
    retweets: 0
  },
  {
    id: '1841018648632443070',
    created: 1727768024000,
    type: 'post',
    text: 'I drove on I-40 heading from TN into NC only a summer ago on my trip to Nags Head. Freaky to think the road would be washed out just over a year from when this photo was taken.',
    photos: ['<div class="item"><img class="photo" src="media/1841018648632443070.jpg"></div>'],
    likes: 2,
    retweets: 0
  },
  {
    id: '1841005401325912293',
    created: 1727764866000,
    type: 'post',
    text: 'Another day, another failed build due to a broken dependency. 😞',
    likes: 1,
    retweets: 0
  },
  {
    id: '1840857056586449177',
    created: 1727729498000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <a class="mention" href="https://x.com/Jfokus" rel="noopener noreferrer" target="_blank">@Jfokus</a> The joke writes itself.<br><br>In reply to: <a href="https://x.com/gunnarmorling/status/1840855611061518554" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <span class="status">1840855611061518554</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1840844928617705681',
    created: 1727726606000,
    type: 'post',
    text: 'In fact, I think AI is a huge distraction for actually fixing the problems that exist. If you believe AI is really a solution, you are just getting caught up in the hype. We don\'t need more clueless bots having hallucinations. What we need is more informed people.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1840841765680521303',
    created: 1727725852000,
    type: 'post',
    text: 'I hate to break it to you, but AI is just not going to fix it. Customer support is a disaster right now and throwing some code at it isn\'t going to fix it. Humans needs to be better trained to to their jobs.',
    likes: 8,
    retweets: 0
  },
  {
    id: '1839993052322001089',
    created: 1727523503000,
    type: 'post',
    text: 'The exceptions to this rule are extremely rare.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1839992749203845525',
    created: 1727523431000,
    type: 'post',
    text: 'I find that I have to remind myself daily that consumer companies want your money, but beyond that they do not care about you or even the quality of the product they offer.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1839854228803625115',
    created: 1727490405000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_JamesWard" rel="noopener noreferrer" target="_blank">@_JamesWard</a> <a class="mention" href="https://x.com/Rivian" rel="noopener noreferrer" target="_blank">@Rivian</a> Amen to that!<br><br>A River. I like it!<br><br>In reply to: <a href="https://x.com/_JamesWard/status/1839853477549236469" rel="noopener noreferrer" target="_blank">@_JamesWard</a> <span class="status">1839853477549236469</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1839851032613958058',
    created: 1727489643000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_JamesWard" rel="noopener noreferrer" target="_blank">@_JamesWard</a> <a class="mention" href="https://x.com/Rivian" rel="noopener noreferrer" target="_blank">@Rivian</a> A) Awesome to hear you\'re a fellow Rivian owner (though I\'m just leasing).<br><br>B) It\'s awesome that they have confident to push updates on a Friday.<br><br>C) They have screwed up these pushes in the past, so they aren\'t perfect yet. My audio system didn\'t work for ~4 weeks (fixed now).<br><br>In reply to: <a href="https://x.com/_JamesWard/status/1839825331605430733" rel="noopener noreferrer" target="_blank">@_JamesWard</a> <span class="status">1839825331605430733</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1839760811973325147',
    created: 1727468133000,
    type: 'post',
    text: 'And wow, Apple support is dreadful. You just get kicked around from one senior advisor to the next, and no one really seems to be qualified to address or even understand the issue.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1839753741253181671',
    created: 1727466447000,
    type: 'post',
    text: 'For those wondering, the content provider pulled the Dolby Atmos version of this song. Who the content provider is I have no idea. What a boneheaded decision. The whole value of Apple Music is Dolby Atmos and now you can\'t even depend on the songs staying that way.<br><br>Quoting: <a href="#1839139108557566227">1839139108557566227</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1839511781603242414',
    created: 1727408759000,
    type: 'post',
    text: 'If you want me to vote for you, just STFU and show me receipts. I have no time or patience for lip service anymore (but I will give credeance to those receipts).',
    likes: 0,
    retweets: 0
  },
  {
    id: '1839511185907159469',
    created: 1727408617000,
    type: 'post',
    text: 'I\'m finding that social media "discussions" have become little more than pissing matches with some antagonizing thrown in. I\'m really just done with it all.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1839510672465445149',
    created: 1727408495000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/karenmcgrane" rel="noopener noreferrer" target="_blank">@karenmcgrane</a> Politics has become spam (even if it\'s well-intended).<br><br>In reply to: <a href="https://x.com/karenmcgrane/status/1839501105727758537" rel="noopener noreferrer" target="_blank">@karenmcgrane</a> <span class="status">1839501105727758537</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1839391206805320069',
    created: 1727380012000,
    type: 'post',
    text: 'Here\'s what I think happened. When syncing their library to the cloud, they inadvertently overwrote the audio file with an older version that was pre-spacial (pre-Dolby Atmos). And they just have no idea what\'s going on.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1839388459053846641',
    created: 1727379357000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/Rivian" rel="noopener noreferrer" target="_blank">@Rivian</a> Please, please, please get spacial audio working with some other service, such as YouTube Music. Having to use Apple Music is just too limited.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1839388205868847339',
    created: 1727379297000,
    type: 'post',
    text: 'I\'ve contact support about this and its shocking to me that <a class="mention" href="https://x.com/AppleMusic" rel="noopener noreferrer" target="_blank">@AppleMusic</a> doesn\'t even seem to know their own library. They can\'t even confirm the song is no longer available in Dolby Atmos. Like absolutely clueless. Wow.<br><br>Quoting: <a href="#1839139108557566227">1839139108557566227</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1839387721913319443',
    created: 1727379181000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/wimdeblauwe" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> If you use the theme default-for-print, it will set the font color to pure black for print <a href="https://github.com/asciidoctor/asciidoctor-pdf/blob/v2.3.x/data/themes/default-for-print-theme.yml" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-pdf/blob/v2.3.x/data/themes/default-for-print-theme.yml</a><br><br>In reply to: <a href="https://x.com/wimdeblauwe/status/1839372036122329152" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> <span class="status">1839372036122329152</span>',
    likes: 4,
    retweets: 1
  },
  {
    id: '1839380735071080529',
    created: 1727377515000,
    type: 'quote',
    text: 'I\'m really glad to see EclipseCon evolve into a broader event that\'s more inclusive for the open source community. I find that cross-platform discussions are some of the most valuable, and this event fosters those kinds of interactions. (It reminds me a lot of OSCON).<br><br>Quoting: <a href="https://x.com/EclipseFdn/status/1839244273197219880" rel="noopener noreferrer" target="_blank">@EclipseFdn</a> <span class="status">1839244273197219880</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1839374023731683476',
    created: 1727375915000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Stephan007" rel="noopener noreferrer" target="_blank">@Stephan007</a> <a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> 👆<br><br>In reply to: <a href="https://x.com/Stephan007/status/1839265081156157441" rel="noopener noreferrer" target="_blank">@Stephan007</a> <span class="status">1839265081156157441</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1839373650493141035',
    created: 1727375826000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> I also gives a chance for everyone to get to experience the talk. There are so many to chose from, so it\'s nice to be able to catch it after missing once or twice.<br><br>Naturally, I would expect there to be updates if there are any new developments, but that goes without saying.<br><br>In reply to: <a href="https://x.com/gunnarmorling/status/1839264337443102819" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <span class="status">1839264337443102819</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1839366469710459327',
    created: 1727374114000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> Having said that, the new release process that was introduced a few months ago is a lot simpler than in the past. I use it for the Antora Maven plugin. See <a href="https://gitlab.com/antora/antora-maven-plugin/-/blob/main/pom.xml?ref_type=heads#L300-343" rel="noopener noreferrer" target="_blank">gitlab.com/antora/antora-maven-plugin/-/blob/main/pom.xml?ref_type=heads#L300-343</a> It\'s still too much, but better than it was.<br><br>In reply to: <a href="#1839365963067920824">1839365963067920824</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1839365963067920824',
    created: 1727373994000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> I\'ve felt this way for a very long time. It\'s my biggest dread when starting a new Java project. I know the day will come when I have to wrangle the bits to get them published.<br><br>In reply to: <a href="https://x.com/starbuxman/status/1839105559544606994" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">1839105559544606994</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1839264859403219146',
    created: 1727349889000,
    type: 'post',
    text: 'And, as I said before, move the All-Star game (or whatever you want to call it) to the championship weekend for one last hoorah for non-championship players.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1839264574383526070',
    created: 1727349821000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> I have an idea about the PLL schedule. Memorial Day -&gt; Labor Day. Yes, Memorial Day weekend is packed, but we\'re in lacrosse mode. Play on Sun and Mon. You only then miss D1 players that made the championship. But they rarely play the first week of the PLL anyway.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1839139108557566227',
    created: 1727319907000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/AppleMusic" rel="noopener noreferrer" target="_blank">@AppleMusic</a> Ed Sheeran\'s "Shape of You" on the album ÷ used to be in Dolby Atmos, but now it\'s not (as of this week). What happened? Is it coming back?!?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1839072394394349662',
    created: 1727304001000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jwcarman" rel="noopener noreferrer" target="_blank">@jwcarman</a> <a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> <a class="mention" href="https://x.com/java" rel="noopener noreferrer" target="_blank">@java</a> That\'s really nice. This is something the OpenJDK project should provide if they want people to understand the release numbers. They just shouting numbers and, honestly, few people have any clue what they represent. That\'s how I felt around Java 17. This graphic is money.<br><br>In reply to: <a href="https://x.com/jwcarman/status/1838995897255936077" rel="noopener noreferrer" target="_blank">@jwcarman</a> <span class="status">1838995897255936077</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1838909444736127037',
    created: 1727265151000,
    type: 'post',
    text: 'For me, the all-star game on a weekend by itself takes the wind out of the sails of the season. Frankly, it\'s mundane. But as a bonus game on championship weekend, I\'d love it. One last chance to see some of the other players on the field do some crazy things.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1838908942875033787',
    created: 1727265031000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> I have an idea about what to do with the all-star game. I\'d love to see it as the same weekend as the championship (or even semis). Sure, it will be the all stars not in the championship, but who really cares? We just want to see PLL players playing. Another game.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1838899925771223252',
    created: 1727262882000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RJScaringe" rel="noopener noreferrer" target="_blank">@RJScaringe</a> Here\'s my referral code in case you need it: DANIEL1783828<br><br>In reply to: <a href="#1829476674590625811">1829476674590625811</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1838525646420553824',
    created: 1727173646000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/eddiejaoude" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> Indeed. Music is also a setting. You can take yourself to another place without moving your feet. I\'ve always said that I code (or do sports) so that I can listen to music.<br><br>In reply to: <a href="https://x.com/eddiejaoude/status/1838513622680576223" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> <span class="status">1838513622680576223</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1838505781315903768',
    created: 1727168910000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/eddiejaoude" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> Tons and tons of music. (I have a collection of DJ sets that I always go back to as well as several curated playlists that form the soundtrack for my life).<br><br>In reply to: <a href="https://x.com/eddiejaoude/status/1838494029836628471" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> <span class="status">1838494029836628471</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1838472317049151531',
    created: 1727160932000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/WorldLacrosse" rel="noopener noreferrer" target="_blank">@WorldLacrosse</a> I\'m loving women\'s box lacrosse! It\'s everything I was hoping it would be and then some. What a turning point for the sport!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1837095644475052212',
    created: 1726832707000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PaulRabil" rel="noopener noreferrer" target="_blank">@PaulRabil</a> <a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> For me, Dan is leading independent lacrosse media. I never miss a podcast and I\'ve been floored over how accurate his takes have been. Go back and listen to him praising Malone, Woodward, and Anderson during the college season. He saw the potential before many of us did.<br><br>In reply to: <a href="https://x.com/PaulRabil/status/1836036132871561662" rel="noopener noreferrer" target="_blank">@PaulRabil</a> <span class="status">1836036132871561662</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1837093189918093612',
    created: 1726832122000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joekeegs" rel="noopener noreferrer" target="_blank">@joekeegs</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> There\'s no question Dobson is the man, though I\'ve also been throughly impressed by the rise of Krebs. The goalies in this league are just on another level.<br><br>In reply to: <a href="https://x.com/joekeegs/status/1836462530606305359" rel="noopener noreferrer" target="_blank">@joekeegs</a> <span class="status">1836462530606305359</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1837092453221888233',
    created: 1726831947000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/BonniesMLAX" rel="noopener noreferrer" target="_blank">@BonniesMLAX</a> <a class="mention" href="https://x.com/bdobson_45" rel="noopener noreferrer" target="_blank">@bdobson_45</a> <a class="mention" href="https://x.com/PLLArchers" rel="noopener noreferrer" target="_blank">@PLLArchers</a> He deserves the best kicks in the league. That man has flair, that\'s for sure.<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1836778421264711812" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1836778421264711812</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1837091610716426685',
    created: 1726831746000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/LacrosseNetwork" rel="noopener noreferrer" target="_blank">@LacrosseNetwork</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> Woodward on defense. Dude was a beast.<br><br>In reply to: <a href="https://x.com/LacrosseNetwork/status/1836832787913216193" rel="noopener noreferrer" target="_blank">@LacrosseNetwork</a> <span class="status">1836832787913216193</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1837090842865000732',
    created: 1726831563000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLArchers" rel="noopener noreferrer" target="_blank">@PLLArchers</a> <a class="mention" href="https://x.com/GrantAment" rel="noopener noreferrer" target="_blank">@GrantAment</a> Grant\'s transition to midfield has been incredible to watch. I think there\'s a real future here for him to redefine what this position can do, much like Tom has done. The way he stepped into Tom\'s shoes also demonstrates his versatility. An amazing season and bright future.<br><br>In reply to: <a href="https://x.com/PLLArchers/status/1836920837313634797" rel="noopener noreferrer" target="_blank">@PLLArchers</a> <span class="status">1836920837313634797</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1837090027668656496',
    created: 1726831368000,
    type: 'quote',
    text: 'Love to see this.<br><br>Quoting: <a href="https://x.com/junitteam/status/1837082215974330823" rel="noopener noreferrer" target="_blank">@junitteam</a> <span class="status">1837082215974330823</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1837049615192916042',
    created: 1726821733000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/WorldLacrosse" rel="noopener noreferrer" target="_blank">@WorldLacrosse</a> This is the turning point in lacrosse I\'ve been waiting for my entire life. This is the day we all play one lacrosse as one human race. I hope this is the start of many new beginnings. 🥍 🥅 Let\'s go!<br><br>In reply to: <a href="https://x.com/WorldLacrosse/status/1836768987218108887" rel="noopener noreferrer" target="_blank">@WorldLacrosse</a> <span class="status">1836768987218108887</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1836660419328381285',
    created: 1726728942000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RivianTrackr" rel="noopener noreferrer" target="_blank">@RivianTrackr</a> How did I miss this? I need to show my partner on the next clean.<br><br>In reply to: <a href="https://x.com/RivianTrackr/status/1835420897466778097" rel="noopener noreferrer" target="_blank">@RivianTrackr</a> <span class="status">1835420897466778097</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1836659943941800354',
    created: 1726728828000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RivianTrackr" rel="noopener noreferrer" target="_blank">@RivianTrackr</a> I think they need to extend the trial. It\'s not a trial if you can\'t try it. I think the trial period should be reset once the Google integration is added.<br><br>In reply to: <a href="https://x.com/RivianTrackr/status/1836126042798371275" rel="noopener noreferrer" target="_blank">@RivianTrackr</a> <span class="status">1836126042798371275</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1836659622280614037',
    created: 1726728752000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RivianTrackr" rel="noopener noreferrer" target="_blank">@RivianTrackr</a> Let\'s hope. It has been a rocky road so far (as new software integrations often are), but I must admit that it sounds great when it works.<br><br>In reply to: <a href="https://x.com/RivianTrackr/status/1836130807829680260" rel="noopener noreferrer" target="_blank">@RivianTrackr</a> <span class="status">1836130807829680260</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1836658903494377903',
    created: 1726728580000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Rivian" rel="noopener noreferrer" target="_blank">@Rivian</a> Btw, saw 3 other Rivians while in the park. Also met the parents of one of your employees (the other RJ).<br><br>In reply to: <a href="#1836658551504118041">1836658551504118041</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1836658551504118041',
    created: 1726728496000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Rivian" rel="noopener noreferrer" target="_blank">@Rivian</a> Many Glacier campground in Glacier National Park. Absolute peace and serenity in the wilderness. Got up the next morning refreshed and hiked to Grinnell Glacier with the fam.<br><br>In reply to: <a href="https://x.com/Rivian/status/1835731829661602010" rel="noopener noreferrer" target="_blank">@Rivian</a> <span class="status">1835731829661602010</span>',
    photos: ['<div class="item"><img class="photo" src="media/1836658551504118041.jpg"></div>', '<div class="item"><img class="photo" src="media/1836658551504118041-2.jpg"></div>', '<div class="item"><img class="photo" src="media/1836658551504118041-3.jpg"></div>', '<div class="item"><img class="photo" src="media/1836658551504118041-4.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1836110055889944697',
    created: 1726597725000,
    type: 'post',
    text: 'I cannot tell you how excited I am to watch the first ever women\'s World Lacrosse Box Championship that starts on Friday. This has been a long time coming, and it\'s so time! <a class="mention" href="https://x.com/WorldLacrosse" rel="noopener noreferrer" target="_blank">@WorldLacrosse</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1835947247906770983',
    created: 1726558908000,
    type: 'post',
    text: 'Frankly, Ruby has lost my trust. I\'ve been using it for close to two decades. The trust earned from all those years is now being eroded with reckless abandon.',
    likes: 2,
    retweets: 1
  },
  {
    id: '1835944267631436219',
    created: 1726558198000,
    type: 'post',
    text: 'Yet another warning about an unbundled gem in Ruby that has caused my builds to fail (this time fiddle). Honesty, I\'ve had it with Ruby. They are breaking the ecosystem with this harebrained effort. I\'m at wits end and nobody will listen.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1833681223593697539',
    created: 1726018646000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bsideup" rel="noopener noreferrer" target="_blank">@bsideup</a> <a class="mention" href="https://x.com/testcontainers" rel="noopener noreferrer" target="_blank">@testcontainers</a> Damn, sorry to hear that! Good luck with the visa issues. Fingers crossed.<br><br>In reply to: <a href="https://x.com/bsideup/status/1833142256288625125" rel="noopener noreferrer" target="_blank">@bsideup</a> <span class="status">1833142256288625125</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1832449826455482398',
    created: 1725725058000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> <a class="mention" href="https://x.com/Argorak" rel="noopener noreferrer" target="_blank">@Argorak</a> <a class="mention" href="https://x.com/hsbt" rel="noopener noreferrer" target="_blank">@hsbt</a> It\'s both. The warning is sloppy. The decision to remove from stdlib is frustrating and time consuming. The situation overall makes me question what APIs I can even rely on. Are we going to gemify YAML too? Where does this end?<br><br>In reply to: <a href="https://x.com/headius/status/1832312145171591530" rel="noopener noreferrer" target="_blank">@headius</a> <span class="status">1832312145171591530</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1832449419607773412',
    created: 1725724961000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ds26gte" rel="noopener noreferrer" target="_blank">@ds26gte</a> Just hell for me having to maintain it.<br><br>In reply to: <a href="https://x.com/ds26gte/status/1832307114888282596" rel="noopener noreferrer" target="_blank">@ds26gte</a> <span class="status">1832307114888282596</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1832301388329840798',
    created: 1725689668000,
    type: 'post',
    text: 'I can deal with ostruct (OpenStruct) going. That can be easily replaced (even though I do so begrudgingly). If logger goes, I go.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1832301033227481282',
    created: 1725689583000,
    type: 'post',
    text: 'I don\'t dislike Ruby the way it has been. Ruby is excellent. (And it has been so for many, many years). But it\'s being ruined by poor decisions that make no sense. Disruption for disruption\'s sake. (And, as a maintainer, I pay the price for that disruption).',
    likes: 2,
    retweets: 0
  },
  {
    id: '1832300048593645758',
    created: 1725689348000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bdowns328" rel="noopener noreferrer" target="_blank">@bdowns328</a> They are asking application users to install extra packages. Alternately, every library (gem) that uses these APIs has to declare a dependency on a new gem, which will of course conflict with the gem installed in older versions of the language runtime.<br><br>In reply to: <a href="https://x.com/bdowns328/status/1832299114052383068" rel="noopener noreferrer" target="_blank">@bdowns328</a> <span class="status">1832299114052383068</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1832299517955432940',
    created: 1725689222000,
    type: 'post',
    text: 'I also find it extremely sloppy that they\'re using stderr to notify developers of API deprecations...since stderr messages go to users of the applications. So now everyone has to see these messages instead of just putting them in release notes like a sane project does.',
    likes: 3,
    retweets: 1
  },
  {
    id: '1832299016388014259',
    created: 1725689102000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bdowns328" rel="noopener noreferrer" target="_blank">@bdowns328</a> Here\'s another:<br><br><a href="https://docs.ruby-lang.org/en/3.3/OpenStruct.html" rel="noopener noreferrer" target="_blank">docs.ruby-lang.org/en/3.3/OpenStruct.html</a><br><br>In reply to: <a href="#1832298675495706993">1832298675495706993</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1832298675495706993',
    created: 1725689021000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bdowns328" rel="noopener noreferrer" target="_blank">@bdowns328</a> Here\'s one API they\'re removing (I mean, the logger!)<br><br><a href="https://docs.ruby-lang.org/en/3.3/Logger.html" rel="noopener noreferrer" target="_blank">docs.ruby-lang.org/en/3.3/Logger.html</a><br><br>Here\'s the stderr (that can\'t be suppressed) added in the point release:<br><br>warning: logger was loaded from the standard library, but will no longer be part of the default gems starting from Ruby 3.5.0.<br><br>In reply to: <a href="https://x.com/bdowns328/status/1832295753974247590" rel="noopener noreferrer" target="_blank">@bdowns328</a> <span class="status">1832295753974247590</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1832296862323503387',
    created: 1725688589000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bdowns328" rel="noopener noreferrer" target="_blank">@bdowns328</a> If it continues to degrade, I think I might be hitting the wall even harder and breaking a few more balls. At least my shot is getting faster ;)<br><br>In reply to: <a href="https://x.com/bdowns328/status/1832295753974247590" rel="noopener noreferrer" target="_blank">@bdowns328</a> <span class="status">1832295753974247590</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1832294974429523991',
    created: 1725688138000,
    type: 'post',
    text: 'There\'s just little to no time anymore to actually add features. I\'m constantly trying to fix what was already working.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1832294750034260066',
    created: 1725688085000,
    type: 'post',
    text: 'Just to understand where I\'m coming from, my job as a maintainer is now predominately occupied by fixing problems because platforms no longer provide a stable foundation and change with little or no warning on a rolling basis. It\'s maddening to not know what you can rely on.<br><br>Quoting: <a href="#1832290909012320701">1832290909012320701</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1832294101091221791',
    created: 1725687930000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bdowns328" rel="noopener noreferrer" target="_blank">@bdowns328</a> Great question! It sure better be worth something pretty substantial for all the disruption, pain, and confusion they are causing. (I doubt it).<br><br>In reply to: <a href="https://x.com/bdowns328/status/1832293253238157408" rel="noopener noreferrer" target="_blank">@bdowns328</a> <span class="status">1832293253238157408</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1832290909012320701',
    created: 1725687169000,
    type: 'post',
    text: 'I\'m ready to walk away from Ruby because the project is removing libraries from stdlib in point releases. I just wonder, is anyone listening or do they even give a shit? Does anyone care about users anymore? Fuck it, just break everything. What does it matter?<br><br>Quoting: <a href="#1832289904396828723">1832289904396828723</a>',
    likes: 5,
    retweets: 3
  },
  {
    id: '1832289904396828723',
    created: 1725686930000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> <a class="mention" href="https://x.com/hsbt" rel="noopener noreferrer" target="_blank">@hsbt</a> I just don\'t trust the way the Ruby language project is being run anymore and frankly I\'m ready to just say goodbye to it. I just don\'t think you understand how upset I am. This is undermining all the projects I\'ve created with it and is costing me *shitloads* of time.<br><br>In reply to: <a href="#1832289418830327835">1832289418830327835</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1832289418830327835',
    created: 1725686814000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> <a class="mention" href="https://x.com/hsbt" rel="noopener noreferrer" target="_blank">@hsbt</a> And if I\'m irritated with Ruby, after having invested in it so deeply for well over a decade, I can assure you that users just getting started are never going to put up with this mess.<br><br>In reply to: <a href="#1832289084527489520">1832289084527489520</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1832289084527489520',
    created: 1725686734000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> <a class="mention" href="https://x.com/hsbt" rel="noopener noreferrer" target="_blank">@hsbt</a> APIs should never be removed from stdlib. That\'s why is called stdlib. I get if they want to deprecate an API and phase it out over time. That\'s one thing. But to do this stuff, especially in a point release (3.3.5), is just nasty. And the stderr warning is even nastier.<br><br>In reply to: <a href="#1832288684877738283">1832288684877738283</a>',
    likes: 0,
    retweets: 1
  },
  {
    id: '1832288684877738283',
    created: 1725686639000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> <a class="mention" href="https://x.com/hsbt" rel="noopener noreferrer" target="_blank">@hsbt</a> Because for people running older versions of Ruby that already have the gem, now they get two versions of that gem installed. And some people have to install it while others don\'t. I don\'t get why this is not confusing. This gemification of the stdlib is a disaster.<br><br>In reply to: <a href="https://x.com/headius/status/1832285288627863850" rel="noopener noreferrer" target="_blank">@headius</a> <span class="status">1832285288627863850</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1832199144393560375',
    created: 1725665291000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> <a class="mention" href="https://x.com/hsbt" rel="noopener noreferrer" target="_blank">@hsbt</a> It\'s stuff like this that makes users hate Ruby. Like you just don\'t know how much anger I deal with on a weekly basis. People come to me livid and at wits end. This is not how you gain users. It\'s just a huge middle finger.<br><br>In reply to: <a href="#1832197878468768143">1832197878468768143</a>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1832198217196597514',
    created: 1725665070000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Argorak" rel="noopener noreferrer" target="_blank">@Argorak</a> <a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> <a class="mention" href="https://x.com/hsbt" rel="noopener noreferrer" target="_blank">@hsbt</a> But now users of 3.3.5 are getting a deprecation warning. This is just sloppy. I\'m getting fed up with these antics from the Ruby project. It\'s unacceptable.<br><br>In reply to: <a href="https://x.com/Argorak/status/1832093346913046707" rel="noopener noreferrer" target="_blank">@Argorak</a> <span class="status">1832093346913046707</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1832197878468768143',
    created: 1725664989000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> <a class="mention" href="https://x.com/hsbt" rel="noopener noreferrer" target="_blank">@hsbt</a> From the deprecation message, it seems like the plan is to remove it from stdlib and require users to add it to their Gemfile and library authors to the gemspec. But this just isn\'t acceptable for a minor release and creates all sorts of compatibility problems and confusion.<br><br>In reply to: <a href="https://x.com/headius/status/1832090139344166993" rel="noopener noreferrer" target="_blank">@headius</a> <span class="status">1832090139344166993</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1831827079715012933',
    created: 1725576584000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> Do you have any influence over this? Because this situation is extremely upsetting.<br><br>Quoting: <a href="#1831779466508730676">1831779466508730676</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1831804185660289435',
    created: 1725571125000,
    type: 'post',
    text: 'Another Ruby security release (3.3.5), another broken feature that has worked for years. This time, some sort of time zone bug. Haven\'t figured it out yet. Security &gt; SemVer.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1831779466508730676',
    created: 1725565232000,
    type: 'post',
    text: 'If Ruby removes the logger from stdlib in Ruby 3.5, I\'m going to stop doing anything new with Ruby. I\'ve had it with removing core APIs from stdlib. What the hell Ruby?!?',
    likes: 6,
    retweets: 0
  },
  {
    id: '1831264797028909429',
    created: 1725442525000,
    type: 'post',
    text: 'Spacial audio is pretty special (though not just a flip of a switch...it does require that the channel selection is done well on the song).',
    likes: 1,
    retweets: 0
  },
  {
    id: '1830554059553522125',
    created: 1725273072000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sebi2706" rel="noopener noreferrer" target="_blank">@sebi2706</a> Glad to hear it!<br><br>In reply to: <a href="https://x.com/sebi2706/status/1830533089707102226" rel="noopener noreferrer" target="_blank">@sebi2706</a> <span class="status">1830533089707102226</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1829476674590625811',
    created: 1725016203000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RJScaringe" rel="noopener noreferrer" target="_blank">@RJScaringe</a> I\'m now even more motivated to chat up people about all things Rivian on my trips, especially at charging stations. What a great incentive. What matters most to me is that I can help answer their questions so they make the choice that is best for them.<br><br>In reply to: <a href="https://x.com/RJScaringe/status/1829259017539199339" rel="noopener noreferrer" target="_blank">@RJScaringe</a> <span class="status">1829259017539199339</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1829309631094571243',
    created: 1724976377000,
    type: 'post',
    text: 'The vegan search in Google maps is dreadful. How hard is it to identify a restaurant that has vegan options? It\'s not like it changes by the day. They either do or they don\'t.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1829222161678250409',
    created: 1724955523000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <a class="mention" href="https://x.com/venkat_s" rel="noopener noreferrer" target="_blank">@venkat_s</a> I\'ve also used "we\'re not out of the woods yet" in both contexts.<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1829219464598171960" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1829219464598171960</span>',
    likes: 3,
    retweets: 2
  },
  {
    id: '1829222029490360453',
    created: 1724955491000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tim_beeren" rel="noopener noreferrer" target="_blank">@tim_beeren</a> <a class="mention" href="https://x.com/venkat_s" rel="noopener noreferrer" target="_blank">@venkat_s</a> I\'m imagining Venkat being a Xero shoes kind of guy ;)<br><br>In reply to: <a href="https://x.com/tim_beeren/status/1829219565852618824" rel="noopener noreferrer" target="_blank">@tim_beeren</a> <span class="status">1829219565852618824</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1828909672365269131',
    created: 1724881020000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/HAU_Nationals" rel="noopener noreferrer" target="_blank">@HAU_Nationals</a> <a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> <a class="mention" href="https://x.com/WorldLacrosse" rel="noopener noreferrer" target="_blank">@WorldLacrosse</a> What a dream team!!<br><br>In reply to: <a href="https://x.com/HAU_Nationals/status/1828208863385911409" rel="noopener noreferrer" target="_blank">@HAU_Nationals</a> <span class="status">1828208863385911409</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1828578283836887275',
    created: 1724802010000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/saegerart" rel="noopener noreferrer" target="_blank">@saegerart</a> The best is the AsciiDoc plugin for IntelliJ, hands down.<br><br>In reply to: <a href="https://x.com/saegerart/status/1828554301113045071" rel="noopener noreferrer" target="_blank">@saegerart</a> <span class="status">1828554301113045071</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1828540816987693170',
    created: 1724793078000,
    type: 'post',
    text: 'The menu system is designed so it\'s impossible to find the function you need. And I can\'t stand all the floating menus that pop up when you least expect them too. It\'s a trainwreck.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1828530564229669230',
    created: 1724790633000,
    type: 'post',
    text: 'I\'m using Word in order to convert a batch of documents to another format and wow has Word gotten terrible. I honestly don\'t know how anyone uses this software.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1827081294850707622',
    created: 1724445100000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jnorthr" rel="noopener noreferrer" target="_blank">@jnorthr</a> I\'m starting to disassociate, to be honest.<br><br>In reply to: <a href="https://x.com/jnorthr/status/1827076166043234663" rel="noopener noreferrer" target="_blank">@jnorthr</a> <span class="status">1827076166043234663</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1827068696335937864',
    created: 1724442097000,
    type: 'post',
    text: 'Oh, and now you have to find a new doctor too...because clearly you have all the time in the world and don\'t give a shit about any sort of continuity in your health care.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1827067925347307562',
    created: 1724441913000,
    type: 'post',
    text: 'And it\'s up to you as the patient to keep all these private relationships straight...while also trying to worry about your...you know, health!',
    likes: 1,
    retweets: 0
  },
  {
    id: '1827067773098586466',
    created: 1724441877000,
    type: 'post',
    text: 'My point is, you don\'t have health insurance, what you have is a relationship with a private company. Depending on their relationship with providers, you may not actually have health insurance. The fact that any person, specifically any American, is okay with this is just insane.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1827067448559816757',
    created: 1724441799000,
    type: 'post',
    text: 'Not saying anything profound here, but medical insurance in the US is extremely unethical. If you have an existing provider (doctor), switch insurance, then go to an existing appointment after the switch, the new insurance can deny the claim for the provider being out of network.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1826374293900402716',
    created: 1724276538000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/styfle" rel="noopener noreferrer" target="_blank">@styfle</a> Wow! 😍<br><br>In reply to: <a href="https://x.com/styfle/status/1826310997818441893" rel="noopener noreferrer" target="_blank">@styfle</a> <span class="status">1826310997818441893</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1825990389267837209',
    created: 1724185008000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/warriorlax" rel="noopener noreferrer" target="_blank">@warriorlax</a> Can you PLEASE allow custom gloves to be purchased as a single pair? I really want to continue using the Warrior Evo lacrosse gloves, but I can\'t find the color I currently have anymore. I\'m not able to buy 12 pairs of gloves, which is required to order custom gloves.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1825605432142082107',
    created: 1724093227000,
    type: 'quote',
    text: 'I want to be as happy as Jane was in that moment as many times as I can in my life. That was just pure. That\'s why we do this.<br><br>Quoting: <a href="https://x.com/EdelMairs/status/1822282045860503561" rel="noopener noreferrer" target="_blank">@EdelMairs</a> <span class="status">1822282045860503561</span>',
    likes: 5,
    retweets: 1
  },
  {
    id: '1825604622519799840',
    created: 1724093034000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/RobPannell3" rel="noopener noreferrer" target="_blank">@RobPannell3</a> That yellow head is 🔥!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1825468551777730623',
    created: 1724060592000,
    type: 'post',
    text: 'If I were the <a class="mention" href="https://x.com/PLLWaterdogs" rel="noopener noreferrer" target="_blank">@PLLWaterdogs</a>, I would trade a pick for Alex Simmons.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1825457921435619676',
    created: 1724058058000,
    type: 'post',
    text: 'I was really excited to discover <a class="mention" href="https://x.com/Web3Forms" rel="noopener noreferrer" target="_blank">@Web3Forms</a>. Makes creating a contact form on a static site super easy.<br><br>Anyone have any experience with this service to share?',
    likes: 1,
    retweets: 0
  },
  {
    id: '1825099107725173228',
    created: 1723972510000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bdowns328" rel="noopener noreferrer" target="_blank">@bdowns328</a> <a class="mention" href="https://x.com/powelllacrosse" rel="noopener noreferrer" target="_blank">@powelllacrosse</a> Btw, the new stick builder (StickTown) is elite. Best in the business.<br><br>In reply to: <a href="https://x.com/bdowns328/status/1824990520244068835" rel="noopener noreferrer" target="_blank">@bdowns328</a> <span class="status">1824990520244068835</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1825089713532731595',
    created: 1723970270000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bdowns328" rel="noopener noreferrer" target="_blank">@bdowns328</a> <a class="mention" href="https://x.com/powelllacrosse" rel="noopener noreferrer" target="_blank">@powelllacrosse</a> Indeed. There is a soul and a story in their equipment, and that means a lot to me. The pocket also catches like a dream.<br><br>In reply to: <a href="https://x.com/bdowns328/status/1824990520244068835" rel="noopener noreferrer" target="_blank">@bdowns328</a> <span class="status">1824990520244068835</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1824988139565510786',
    created: 1723946053000,
    type: 'post',
    text: 'My new twig. Made by my heroes. Sporting colors of their rivals (Cornell).',
    photos: ['<div class="item"><img class="photo" src="media/1824988139565510786.jpg"></div>'],
    likes: 4,
    retweets: 0
  },
  {
    id: '1824918147188859028',
    created: 1723929366000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bsideup" rel="noopener noreferrer" target="_blank">@bsideup</a> Same.<br><br>In reply to: <a href="https://x.com/bsideup/status/1824912776764219580" rel="noopener noreferrer" target="_blank">@bsideup</a> <span class="status">1824912776764219580</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1824529002264138173',
    created: 1723836586000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/wimdeblauwe" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> <a class="mention" href="https://x.com/springrestdocs" rel="noopener noreferrer" target="_blank">@springrestdocs</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> From what I\'ve seen of Swagger, I tend to agree.<br><br>In reply to: <a href="https://x.com/wimdeblauwe/status/1824437999179907279" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> <span class="status">1824437999179907279</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1824226571760308524',
    created: 1723764481000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Rivian" rel="noopener noreferrer" target="_blank">@Rivian</a> You get us.<br><br>In reply to: <a href="https://x.com/Rivian/status/1824135356972929228" rel="noopener noreferrer" target="_blank">@Rivian</a> <span class="status">1824135356972929228</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1823506841822728703',
    created: 1723592884000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Rivian" rel="noopener noreferrer" target="_blank">@Rivian</a> <a class="mention" href="https://x.com/AppleMusic" rel="noopener noreferrer" target="_blank">@AppleMusic</a> <a class="mention" href="https://x.com/Dolby" rel="noopener noreferrer" target="_blank">@Dolby</a> espn+ would be a really nice bonus.<br><br>In reply to: <a href="#1823503849153331634">1823503849153331634</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1823503849153331634',
    created: 1723592171000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Rivian" rel="noopener noreferrer" target="_blank">@Rivian</a> <a class="mention" href="https://x.com/AppleMusic" rel="noopener noreferrer" target="_blank">@AppleMusic</a> <a class="mention" href="https://x.com/Dolby" rel="noopener noreferrer" target="_blank">@Dolby</a> I\'m most interested in Google Cast and YouTube. I really want to be able to watch videos while charging, and I also want to be able to pull up my playlist on YouTube Music when I get in the car to drive. I have no interest in Amazon Music.<br><br>In reply to: <a href="https://x.com/Rivian/status/1823350477888864579" rel="noopener noreferrer" target="_blank">@Rivian</a> <span class="status">1823350477888864579</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1823501455871107170',
    created: 1723591600000,
    type: 'post',
    text: 'This is why I give 0 f\'s about companies looking to fill a position. When they need a position filled, they annoy us all to death about it. When we notify them about someone who\'s (desperately) looking for a job, crickets. It\'s so self-centered.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1823457401200353450',
    created: 1723581097000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/chacon" rel="noopener noreferrer" target="_blank">@chacon</a> I, for one, have never been confused about the terminology.<br><br>In reply to: <a href="https://x.com/chacon/status/1823416898379505749" rel="noopener noreferrer" target="_blank">@chacon</a> <span class="status">1823416898379505749</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1823408468243726422',
    created: 1723569430000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/saronyitbarek" rel="noopener noreferrer" target="_blank">@saronyitbarek</a> 💯<br><br>In reply to: <a href="https://x.com/saronyitbarek/status/1823404229572321305" rel="noopener noreferrer" target="_blank">@saronyitbarek</a> <span class="status">1823404229572321305</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1822876534748872815',
    created: 1723442607000,
    type: 'post',
    text: 'The breaking was phenomenal. It really celebrates both movement and camaraderie. That\'s the true spirit of the #Olympics. #Olympics2024Paris',
    likes: 3,
    retweets: 0,
    tags: ['olympics', 'olympics2024paris']
  },
  {
    id: '1822118685697622324',
    created: 1723261922000,
    type: 'post',
    text: 'Devoxx France now needs to start off each day with les coups de bâton.',
    likes: 5,
    retweets: 1
  },
  {
    id: '1821461611112169696',
    created: 1723105263000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> No, I did not ask for it. It just defaults to this ridiculous behavior. (I can understand an override, but we aren\'t talking about special cases here). They are burning an insane amount of computing resources with this absurd default.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1821456814044520922" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1821456814044520922</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1821373912342503741',
    created: 1723084354000,
    type: 'post',
    text: 'I can\'t believe GitHub still hasn\'t fixed this. If you have a fork and you submit a PR, the scheduled workflow will now run for weeks until GitHub once again decides there isn\'t enough activity. WE DON\'T NEED SCHEDULED WORKFLOWS TO RUN IN IN FORKS!!',
    likes: 3,
    retweets: 0
  },
  {
    id: '1821129107314196501',
    created: 1723025988000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> Node.js is a tremendous platform and ecosystem. I wouldn\'t have chosen it if I didn\'t think it was superior for the use case (and it has proven to be that and more). But with all platforms, there are dead ends and wrong choices. We learn as we go.<br><br>In reply to: <a href="https://x.com/aalmiray/status/1821123566164746348" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">1821123566164746348</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1821114168734568695',
    created: 1723022427000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> I\'m abandoning gulp for the more simple npm scripts. But there\'s still some legacy stuff in there that needs cleaning up. gulp just turned out to be not the one.<br><br>In reply to: <a href="https://x.com/aalmiray/status/1821112553323327827" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">1821112553323327827</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1821075848382050575',
    created: 1723013290000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/justicedems" rel="noopener noreferrer" target="_blank">@justicedems</a> <a class="mention" href="https://x.com/sunrisemvmt" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a> <a class="mention" href="https://x.com/CoriBush" rel="noopener noreferrer" target="_blank">@CoriBush</a> Cori has always been my favorite person in Congress. Even though she\'s not my rep, I still stayed on her list to follow what\'s she\'s done...and will do from here.<br><br>In reply to: <a href="https://x.com/justicedems/status/1821030094984401081" rel="noopener noreferrer" target="_blank">@justicedems</a> <span class="status">1821030094984401081</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1820950918461800627',
    created: 1722983505000,
    type: 'post',
    text: 'My productivity is severely diminished due to the number of times I have to log in to shit, even though my computer is in a private residence and has absolutely 0 risk of being hacked on premises.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1820356225353314379',
    created: 1722841719000,
    type: 'post',
    text: 'The point is, I know exactly what I\'m getting and I can support projects in return for what I got. There are no false promises or value that isn\'t delivered. I spend way too much on things these days that just don\'t come through.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1820318690887315650',
    created: 1722832770000,
    type: 'post',
    text: 'One of the things that has always drawn me to open source is that when it doesn\'t work, at least I don\'t feel regret that I paid money for it I didn\'t get value. That sounds negative, but it\'s actually positive. It means I get out what I put in, and I like that. It\'s fair.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1820317630529450301',
    created: 1722832517000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/TeamYouTube" rel="noopener noreferrer" target="_blank">@TeamYouTube</a> Frankly, I can\'t believe I pay for this.<br><br>In reply to: <a href="#1820316993569923408">1820316993569923408</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1820316993569923408',
    created: 1722832365000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/TeamYouTube" rel="noopener noreferrer" target="_blank">@TeamYouTube</a> Don\'t patronize me. Fire the existing team of software designers because this is a dreadful product and get some people who actually know how to write software.<br><br>In reply to: <a href="https://x.com/TeamYouTube/status/1820238424592322699" rel="noopener noreferrer" target="_blank">@TeamYouTube</a> <span class="status">1820238424592322699</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1820316850162467297',
    created: 1722832331000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/codeslubber" rel="noopener noreferrer" target="_blank">@codeslubber</a> Not nitpicky at all. Just facts. In fact, it\'s how I manage to stay calm. I just know to expect it, as sad as that sounds...it saves me from getting bent out of shape each time.<br><br>In reply to: <a href="https://x.com/codeslubber/status/1820234202371231800" rel="noopener noreferrer" target="_blank">@codeslubber</a> <span class="status">1820234202371231800</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1820232184835219658',
    created: 1722812145000,
    type: 'post',
    text: 'It\'s unbelievable how bad the playlist editor is in YouTube Music. You have to be in one mode to move songs and a different mode to play them. And each time you switch modes, it scrolls all the way past the end of the playlist. Nothing is good anymore.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1817307592349372921',
    created: 1722114868000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/eddiejaoude" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> I\'ve found that the key is to set extremely rigid boundaries. Know your limit and refuse to exceed it for anyone but yourself (or spouse). It works.<br><br>In reply to: <a href="https://x.com/eddiejaoude/status/1817299871453356367" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> <span class="status">1817299871453356367</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1817264066315853996',
    created: 1722104491000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bml_khubbard" rel="noopener noreferrer" target="_blank">@bml_khubbard</a> <a class="mention" href="https://x.com/scrumtuous" rel="noopener noreferrer" target="_blank">@scrumtuous</a> If you need help, I invite you to join the public chat at <a href="https://chat.asciidoctor.org" rel="noopener noreferrer" target="_blank">chat.asciidoctor.org</a>. There are always people there willing to help out new and seasoned users alike.<br><br>In reply to: <a href="https://x.com/bml_khubbard/status/1817263246472303009" rel="noopener noreferrer" target="_blank">@bml_khubbard</a> <span class="status">1817263246472303009</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1817114224922947684',
    created: 1722068766000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dadoonet" rel="noopener noreferrer" target="_blank">@dadoonet</a> <a class="mention" href="https://x.com/Paris2024" rel="noopener noreferrer" target="_blank">@Paris2024</a> Have fun! I absolutely love seeing coed sports too!<br><br>In reply to: <a href="https://x.com/dadoonet/status/1817092379142234271" rel="noopener noreferrer" target="_blank">@dadoonet</a> <span class="status">1817092379142234271</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1817113738467545096',
    created: 1722068650000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/t_millstream" rel="noopener noreferrer" target="_blank">@t_millstream</a> <a class="mention" href="https://x.com/jponge" rel="noopener noreferrer" target="_blank">@jponge</a> <a class="mention" href="https://x.com/Olympics" rel="noopener noreferrer" target="_blank">@Olympics</a> And also a message of peace &amp; solidarity. Definitely some strong, positive messages in there. And to end with Celine just belting it out to the world was really something too. 🥰<br><br>In reply to: <a href="https://x.com/t_millstream/status/1816931849022648727" rel="noopener noreferrer" target="_blank">@t_millstream</a> <span class="status">1816931849022648727</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1817112030303621353',
    created: 1722068242000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <a class="mention" href="https://x.com/Olympics" rel="noopener noreferrer" target="_blank">@Olympics</a> I know! It\'s going to be lit!<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1817078071054532681" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1817078071054532681</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1817077385831022848',
    created: 1722059983000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <a class="mention" href="https://x.com/Olympics" rel="noopener noreferrer" target="_blank">@Olympics</a> The flame is technically a hot air balloon, so back?<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1817068880273244249" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1817068880273244249</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1817077047061303610',
    created: 1722059902000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <a class="mention" href="https://x.com/Olympics" rel="noopener noreferrer" target="_blank">@Olympics</a> Lacrosse will be back in 2028.<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1817068880273244249" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1817068880273244249</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1816968454815305869',
    created: 1722034011000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/TomCooper11111" rel="noopener noreferrer" target="_blank">@TomCooper11111</a> <a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> The Woods are just one terrible decision after another this year. Nothing about this lineup makes sense. It\'s just a crap shoot.<br><br>In reply to: <a href="https://x.com/TomCooper11111/status/1816942901231849894" rel="noopener noreferrer" target="_blank">@TomCooper11111</a> <span class="status">1816942901231849894</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1815442413281804377',
    created: 1721670175000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Kiview" rel="noopener noreferrer" target="_blank">@Kiview</a> <a class="mention" href="https://x.com/habuma" rel="noopener noreferrer" target="_blank">@habuma</a> Yes!<br><br>In reply to: <a href="https://x.com/Kiview/status/1815364243375435899" rel="noopener noreferrer" target="_blank">@Kiview</a> <span class="status">1815364243375435899</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1815442370076561688',
    created: 1721670164000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/looselytyped" rel="noopener noreferrer" target="_blank">@looselytyped</a> <a class="mention" href="https://x.com/habuma" rel="noopener noreferrer" target="_blank">@habuma</a> Sentence per line FTW!<br><br>In reply to: <a href="https://x.com/looselytyped/status/1815180276445675620" rel="noopener noreferrer" target="_blank">@looselytyped</a> <span class="status">1815180276445675620</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1814585874673508729',
    created: 1721465960000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Rivian" rel="noopener noreferrer" target="_blank">@Rivian</a> Great concept!<br><br>In reply to: <a href="https://x.com/Rivian/status/1814357823293366312" rel="noopener noreferrer" target="_blank">@Rivian</a> <span class="status">1814357823293366312</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1814408838377845153',
    created: 1721423751000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> Please put the timezone of the game next to the game time on the game preview pages. It\'s really difficult to communicate to friends and family when to turn on the TV when you\'re not quite sure what timezone the time is being presented in.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1814408565441831009',
    created: 1721423686000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> I really enjoyed the skills competition at All-Star weekend this year. It was a great format. Very entertaining. It\'s a keeper!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1814223627488817265',
    created: 1721379594000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jbaruch" rel="noopener noreferrer" target="_blank">@jbaruch</a> <a class="mention" href="https://x.com/bmarwell" rel="noopener noreferrer" target="_blank">@bmarwell</a> <a class="mention" href="https://x.com/github" rel="noopener noreferrer" target="_blank">@github</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> AsciiDoc is older than Markdown by several years.<br><br>In reply to: <a href="https://x.com/jbaruch/status/1814150697186722269" rel="noopener noreferrer" target="_blank">@jbaruch</a> <span class="status">1814150697186722269</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1814060977245360608',
    created: 1721340815000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> Objectively, I just disagree. I don\'t understand why people are so high on Rambo. He\'s a good talker, but I\'m just not seeing him be all that effective. I think Malone and Anderson have tons of potential to click with Zed.<br><br>In reply to: <a href="https://x.com/danarestia/status/1814041872748130676" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1814041872748130676</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1814057971510632667',
    created: 1721340098000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> Finally we\'re going to get to see Simmons!<br><br>In reply to: <a href="https://x.com/danarestia/status/1814049126280990753" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1814049126280990753</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1813686185350639924',
    created: 1721251457000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ReneSchwietzke" rel="noopener noreferrer" target="_blank">@ReneSchwietzke</a> Absolutely perfect record. The vehicle is an EV SUV. It\'s on the higher end in terms of price in the market, but the issue is more about it being co-signed by our small business. Apparently, that just doubles the insurance for no justifiable reason.<br><br>In reply to: <a href="https://x.com/ReneSchwietzke/status/1813684791516226049" rel="noopener noreferrer" target="_blank">@ReneSchwietzke</a> <span class="status">1813684791516226049</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1813669866920132743',
    created: 1721247567000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/codeslubber" rel="noopener noreferrer" target="_blank">@codeslubber</a> Ain\'t that the damn truth!<br><br>In reply to: <a href="https://x.com/codeslubber/status/1813666324247375904" rel="noopener noreferrer" target="_blank">@codeslubber</a> <span class="status">1813666324247375904</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1813658267123675303',
    created: 1721244801000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Seriously, right?!? (and no, of course).<br><br>In reply to: <a href="https://x.com/maxandersen/status/1813657212650422610" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1813657212650422610</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1813650897131708859',
    created: 1721243044000,
    type: 'post',
    text: 'For my new vehicle, the insurance company wants $7,380.00 to cover it per year. How the fuck is this even legal? Insurance companies are nothing but crooks.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1812933461063131188',
    created: 1721071994000,
    type: 'post',
    text: 'I have a friend who\'s reentering the workforce after raising a family + assisting w/ her partner\'s small business. &gt;20 years exp in marketing, corporate communications, &amp; branding. Worked in cyber, gov\'t tech, &amp; higher Ed. Looking for full-time, remote job. Any leads appreciated.',
    likes: 4,
    retweets: 1
  },
  {
    id: '1812763095665692789',
    created: 1721031376000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Kiview" rel="noopener noreferrer" target="_blank">@Kiview</a> Sage advice. I appreciate it!<br><br>In reply to: <a href="https://x.com/Kiview/status/1812732848169738247" rel="noopener noreferrer" target="_blank">@Kiview</a> <span class="status">1812732848169738247</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1812276635518869652',
    created: 1720915395000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> Have fun in Colorado! It\'s just that kind of weekend. (We\'re up in Salida).<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1812236407571914810" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1812236407571914810</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1812234088893518268',
    created: 1720905251000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bresreports" rel="noopener noreferrer" target="_blank">@bresreports</a> <a class="mention" href="https://x.com/EmmaVigeland" rel="noopener noreferrer" target="_blank">@EmmaVigeland</a> <a class="mention" href="https://x.com/RepJayapal" rel="noopener noreferrer" target="_blank">@RepJayapal</a> This is why we hate politics. This says absolutely nothing. What does this caucus actually think?!?! FFS.<br><br>In reply to: <a href="https://x.com/bresreports/status/1812227297812107421" rel="noopener noreferrer" target="_blank">@bresreports</a> <span class="status">1812227297812107421</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1812233387576586534',
    created: 1720905083000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jnorthr" rel="noopener noreferrer" target="_blank">@jnorthr</a> You bet it is! Fuel for releases!<br><br>In reply to: <a href="https://x.com/jnorthr/status/1812230562985234902" rel="noopener noreferrer" target="_blank">@jnorthr</a> <span class="status">1812230562985234902</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1812228752694485409',
    created: 1720903978000,
    type: 'post',
    text: 'Enjoying some us time.',
    photos: ['<div class="item"><img class="photo" src="media/1812228752694485409.jpg"></div>'],
    likes: 14,
    retweets: 1
  },
  {
    id: '1811789911420469291',
    created: 1720799350000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Kiview" rel="noopener noreferrer" target="_blank">@Kiview</a> I had a hunch that was the direction I was going to need to go. Time to learn something new it is!<br><br>In reply to: <a href="https://x.com/Kiview/status/1811760098655547901" rel="noopener noreferrer" target="_blank">@Kiview</a> <span class="status">1811760098655547901</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1811473594087801289',
    created: 1720723935000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jimisola" rel="noopener noreferrer" target="_blank">@jimisola</a> Here\'s the image I created for a similar purpose. <a href="https://gitlab.com/gadhs/pamms/-/blob/main/Dockerfile?ref_type=heads" rel="noopener noreferrer" target="_blank">gitlab.com/gadhs/pamms/-/blob/main/Dockerfile?ref_type=heads</a><br><br>In reply to: <a href="https://x.com/jimisola/status/1811347550806434264" rel="noopener noreferrer" target="_blank">@jimisola</a> <span class="status">1811347550806434264</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1811178515389374864',
    created: 1720653582000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jimisola" rel="noopener noreferrer" target="_blank">@jimisola</a> That\'s exactly what we are doing. The issue is that LibreOffice is still not perfectly compatible with Word, so there are layout problems in the output.<br><br>Btw, there is no reason to use unoconv. The libreoffice command now supports all the same features.<br><br>In reply to: <a href="https://x.com/jimisola/status/1811164510771871973" rel="noopener noreferrer" target="_blank">@jimisola</a> <span class="status">1811164510771871973</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1811109872559394984',
    created: 1720637217000,
    type: 'post',
    text: 'Is it possible to install Microsoft Office (or at least Word) in a CI workflow that runs on Windows? Or is there a CI host that offers this combination? I need to use Word to convert legacy documents to PDF and I have no idea how to get the software needed to do that.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1810804628340232290',
    created: 1720564441000,
    type: 'post',
    text: 'I do not like that Zulip uses overflow-x: scroll on code blocks. Soft wrapping is much more accessible and familiar to most programmers. I wish that could be configured somehow.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1810371928949191088',
    created: 1720461277000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <a class="mention" href="https://x.com/edburns" rel="noopener noreferrer" target="_blank">@edburns</a> 🫂<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1810358704329601359" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1810358704329601359</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1810370637174812773',
    created: 1720460969000,
    type: 'post',
    text: 'The support my friends are giving to a friend in need is inspiring and it gives me hope. True love is a special thing.',
    likes: 2,
    retweets: 1
  },
  {
    id: '1809352350265376886',
    created: 1720218191000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mkheck" rel="noopener noreferrer" target="_blank">@mkheck</a> 🙏<br><br>In reply to: <a href="https://x.com/mkheck/status/1809316750498099556" rel="noopener noreferrer" target="_blank">@mkheck</a> <span class="status">1809316750498099556</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1809169551197614089',
    created: 1720174608000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLRedwoods" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> <a class="mention" href="https://x.com/warriorlax" rel="noopener noreferrer" target="_blank">@warriorlax</a> Why are you not playing Alex Simmons? Do you watch NLL?<br><br>In reply to: <a href="https://x.com/PLLRedwoods/status/1808931499796017309" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> <span class="status">1808931499796017309</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1808978021325623672',
    created: 1720128944000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> Please start ejecting players for fighting. This is just getting trashy. Lacrosse is not hockey and this doesn\'t help grow the game. It\'s also not good for the welfare of the players, who we don\'t want to be injured.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1806617266642911421',
    created: 1719566096000,
    type: 'post',
    text: 'I don\'t get how Node.js can claim to be THE runtime for server-side web dev and not provide an HTML and XML parser. It\'s literally what this platform is about. The most obvious choice to me is to bundle the standard DOMParser and its compliment, XMLSerializer, from the web APIs.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1806223156211863699',
    created: 1719472133000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/itsjamesherring" rel="noopener noreferrer" target="_blank">@itsjamesherring</a> cc: <a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a><br><br>In reply to: <a href="https://x.com/itsjamesherring/status/1805505380027359684" rel="noopener noreferrer" target="_blank">@itsjamesherring</a> <span class="status">1805505380027359684</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1805685454777262336',
    created: 1719343935000,
    type: 'post',
    text: '',
    photos: ['<div class="item"><img class="photo" src="media/1805685454777262336.png"></div>'],
    likes: 2,
    retweets: 0
  },
  {
    id: '1805343240402481406',
    created: 1719262344000,
    type: 'post',
    text: 'Before you tell a software project maintainer that you don\'t have the time, just think about the fact that they probably already put in way more time for you that they didn\'t have.',
    likes: 5,
    retweets: 2
  },
  {
    id: '1805326589774774315',
    created: 1719258375000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/MacOKeefe3" rel="noopener noreferrer" target="_blank">@MacOKeefe3</a> He absolutely wasn\'t on the line. He got robbed.<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1805249985656582271" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1805249985656582271</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1805186223062818851',
    created: 1719224908000,
    type: 'post',
    text: 'I now consider all dependencies that are outside my sphere of influence (projects I don\'t work on or oversee) to be a bug. And not because the software isn\'t good, but because dependency systems are being poisoned by deprecation warnings and no context vulnerability reports.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1804227802473840982',
    created: 1718996403000,
    type: 'post',
    text: 'Put a logger in Node.js FFS!',
    likes: 2,
    retweets: 0
  },
  {
    id: '1803744159376855253',
    created: 1718881094000,
    type: 'post',
    text: 'Maybe they should do the work to check that the project is still in compliance and automatically renew. But no, they want us to shoulder all the effort. It\'s the same old "we don\'t have the time" song and dance. OSS maintainers hear it every day.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1803742553608429923',
    created: 1718880711000,
    type: 'post',
    text: 'After my objection, GitLab approved my application retroactively. But now I\'m on the hook to resubmit the application once a year and go though it all again. Yet another task on my plate to run this project. Companies just don\'t understand what it takes to be an OSS maintainer.<br><br>Quoting: <a href="#1671269140538204161">1671269140538204161</a>',
    likes: 12,
    retweets: 1
  },
  {
    id: '1800968596106908021',
    created: 1718219348000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLCannons" rel="noopener noreferrer" target="_blank">@PLLCannons</a> <a class="mention" href="https://x.com/ChefBoy_RD_" rel="noopener noreferrer" target="_blank">@ChefBoy_RD_</a> I\'m seeing Drenner emerge as a great finisher. I\'d like to see him used more in that capacity so he doesn\'t get lost in the offense. The Atlas are proving just how effective a good finisher can be to production.<br><br>In reply to: <a href="https://x.com/PLLCannons/status/1800959076391845954" rel="noopener noreferrer" target="_blank">@PLLCannons</a> <span class="status">1800959076391845954</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1800967952637784364',
    created: 1718219194000,
    type: 'post',
    text: 'Likes are now private? Who the hell decided that was what we wanted? What a dumb decision. One more nail in the coffin of this dying platform.',
    likes: 10,
    retweets: 1
  },
  {
    id: '1799026470846898195',
    created: 1717756309000,
    type: 'post',
    text: 'It\'s also hard to introduce new people to the broadcast because it just feels uncomfortable. It\'s so over the top that you almost feel embarrassed that you pressed play. ESPN has an incredible presentation, but they are talking over it way too much and it chokes it to death.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1799025251487302083',
    created: 1717756018000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> I completely agree with your critique of the broadcast week 1. All of it, esp the interviews over the play and the Matty P mic. I\'ll also add that Carc needs to dial it back. Someone will score a goal and immediately he\'s the best player in lacrosse history. Chill!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1799007884166209676',
    created: 1717751878000,
    type: 'post',
    text: 'Women\'s box lacrosse is going to be big. Watching the clips from the training rounds has me juiced up. First-ever world championships this September. I can\'t wait.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1798836604603351407',
    created: 1717711041000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> I know Chris Gray is just unavailable, but I think Alex Simmons is going to perform better in the long run. I see him as the Ryan Lee replacement.<br><br>In reply to: <a href="https://x.com/danarestia/status/1798786428715012473" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1798786428715012473</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1797696818656219292',
    created: 1717439295000,
    type: 'post',
    text: 'Funny how some people seem to think that bashing the project maintainer is a sensible way to get what you want when the discussion isn\'t going your way. Are we 5? The maturity level in tech is just plummeting.',
    likes: 3,
    retweets: 1
  },
  {
    id: '1796811524557701489',
    created: 1717228225000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> Michael Long is going to be back next year for Cornell (in addition to CJ Kirst). Source: <a href="https://alumni.cornell.edu/cornellians/buczek-lacrosse/" rel="noopener noreferrer" target="_blank">alumni.cornell.edu/cornellians/buczek-lacrosse/</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1796808402867302711',
    created: 1717227480000,
    type: 'post',
    text: 'I\'m really glad that the git project acknowledged and reverted overly aggressive changes that were made in response to a CVE. Reacting to (often overblown) security reports should not be an excuse to break APIs in a patch release. <a href="https://github.com/git/git/blob/master/Documentation/RelNotes/2.45.2.txt" rel="noopener noreferrer" target="_blank">github.com/git/git/blob/master/Documentation/RelNotes/2.45.2.txt</a>',
    likes: 1,
    retweets: 2
  },
  {
    id: '1796280739228520803',
    created: 1717101676000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> I wonder if offering it on ESPN+ impacted these numbers at all. I, for one, was thrilled that they did that as it was the first time I could watch it without having to find a bootleg. And I watched it once it hit on demand. I have no cable service and have no plans to get it.<br><br>In reply to: <a href="https://x.com/danarestia/status/1796272691776803221" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1796272691776803221</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1796063731299815905',
    created: 1717049937000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLRedwoods" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> <a class="mention" href="https://x.com/cole_kirst" rel="noopener noreferrer" target="_blank">@cole_kirst</a> Cole is the most articulate nice guy I can think of. We need more guys like Cole. That\'s how you lift up teammates. Let\'s go!<br><br>In reply to: <a href="https://x.com/PLLRedwoods/status/1795996459373855134" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> <span class="status">1795996459373855134</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1795721522910560423',
    created: 1716968348000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLRedwoods" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> Even with all the young talent coming in, <a class="mention" href="https://x.com/RobPannell3" rel="noopener noreferrer" target="_blank">@RobPannell3</a> will always be my favorite to watch (with CJ being a close second). Go Big Red! Roll woods!<br><br>In reply to: <a href="https://x.com/PLLRedwoods/status/1795501429903241537" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> <span class="status">1795501429903241537</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1795720961360294363',
    created: 1716968214000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLRedwoods" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> My favorite duo.<br><br>In reply to: <a href="https://x.com/PLLRedwoods/status/1795495240544960928" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> <span class="status">1795495240544960928</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1795338978268491828',
    created: 1716877142000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GeneDaniels24" rel="noopener noreferrer" target="_blank">@GeneDaniels24</a> My one suggestion would be for Pat and Liam to share the Tewaaraton. But if that can\'t happen, it should be Liam.<br><br>In reply to: <a href="https://x.com/GeneDaniels24/status/1795186132491133216" rel="noopener noreferrer" target="_blank">@GeneDaniels24</a> <span class="status">1795186132491133216</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1795338037691662473',
    created: 1716876918000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dan_aburn_" rel="noopener noreferrer" target="_blank">@dan_aburn_</a> I totally agree. My mind simply won\'t let me think one should get it and not the other. What made this team work was having them on both ends (and tons of talent in between). I think they would both be happy if they shared it too, at least that\'s the impression I get.<br><br>In reply to: <a href="https://x.com/dan_aburn_/status/1795206062116585790" rel="noopener noreferrer" target="_blank">@dan_aburn_</a> <span class="status">1795206062116585790</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1795034408799396192',
    created: 1716804527000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLRedwoods" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> Alex Simmons, let\'s roll!<br><br>In reply to: <a href="https://x.com/PLLRedwoods/status/1793680631328645474" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> <span class="status">1793680631328645474</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1794992465650315734',
    created: 1716794527000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/paulcarcaterra" rel="noopener noreferrer" target="_blank">@paulcarcaterra</a> <a class="mention" href="https://x.com/CornellLacrosse" rel="noopener noreferrer" target="_blank">@CornellLacrosse</a> 💯 For me, it\'s the ❤️. Second to none.<br><br>In reply to: <a href="https://x.com/paulcarcaterra/status/1788696110938124752" rel="noopener noreferrer" target="_blank">@paulcarcaterra</a> <span class="status">1788696110938124752</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1794929194453102922',
    created: 1716779442000,
    type: 'post',
    text: 'I just broke another lacrosse ball. I\'m going to take it this means I\'m getting a lot stronger.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1794275494168592839',
    created: 1716623588000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> Our couch theory was that his partner broke up with him and took his cat. There\'s just no other explanation.<br><br>In reply to: <a href="https://x.com/danarestia/status/1794060017349390566" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1794060017349390566</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1794263179473895569',
    created: 1716620652000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sebi2706" rel="noopener noreferrer" target="_blank">@sebi2706</a> <a class="mention" href="https://x.com/UnFroMage" rel="noopener noreferrer" target="_blank">@UnFroMage</a> Same experience here.<br><br>In reply to: <a href="https://x.com/sebi2706/status/1794262143338250415" rel="noopener noreferrer" target="_blank">@sebi2706</a> <span class="status">1794262143338250415</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1793764960344121532',
    created: 1716501867000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fortysevenfx" rel="noopener noreferrer" target="_blank">@fortysevenfx</a> <a class="mention" href="https://x.com/nodejs" rel="noopener noreferrer" target="_blank">@nodejs</a> <a class="mention" href="https://x.com/matteocollina" rel="noopener noreferrer" target="_blank">@matteocollina</a> <a class="mention" href="https://x.com/yagiznizipli" rel="noopener noreferrer" target="_blank">@yagiznizipli</a> I\'m elated to be able to continue to remove dependencies.<br><br>In reply to: <a href="https://x.com/fortysevenfx/status/1793741390171852881" rel="noopener noreferrer" target="_blank">@fortysevenfx</a> <span class="status">1793741390171852881</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1793579250068443218',
    created: 1716457590000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> I agree with you 💯 about feeling like we haven\'t seen street lacrosse yet. It\'s like we\'re looking though a crack in a fence and not really getting the picture of what\'s happening on the other side. Just glimpses.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1792956559888171205',
    created: 1716309130000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fsfarimani" rel="noopener noreferrer" target="_blank">@fsfarimani</a> The forum is now at <a href="https://chat.asciidoctor.org" rel="noopener noreferrer" target="_blank">chat.asciidoctor.org</a>. Please ask there.<br><br>In reply to: <a href="https://x.com/fsfarimani/status/1792920124820799639" rel="noopener noreferrer" target="_blank">@fsfarimani</a> <span class="status">1792920124820799639</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1792796483504501081',
    created: 1716270964000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ToddG96" rel="noopener noreferrer" target="_blank">@ToddG96</a> <a class="mention" href="https://x.com/USALacrosseMag" rel="noopener noreferrer" target="_blank">@USALacrosseMag</a> <a class="mention" href="https://x.com/UVAMensLax" rel="noopener noreferrer" target="_blank">@UVAMensLax</a> I agree about the line color. The paint at Hoftra was neon yellow. It left no doubt. This blue was just murky. As a result, I could understand why it was hard to overturn. So the mistake was made well before the game. UVA also missed the endline in OT because it was invisible.<br><br>In reply to: <a href="https://x.com/ToddG96/status/1792309468413227067" rel="noopener noreferrer" target="_blank">@ToddG96</a> <span class="status">1792309468413227067</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1792795185786896736',
    created: 1716270655000,
    type: 'quote',
    text: 'This is one of the most beautiful question marks I\'ve ever seen, and I\'ve seen all of the ones by <a class="mention" href="https://x.com/RobPannell3" rel="noopener noreferrer" target="_blank">@RobPannell3</a>. Now I\'m going to go practice it 1,000 times.<br><br>Quoting: <a href="https://x.com/LacrosseNetwork/status/1792282539740045560" rel="noopener noreferrer" target="_blank">@LacrosseNetwork</a> <span class="status">1792282539740045560</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1792441067649876016',
    created: 1716186227000,
    type: 'post',
    text: 'Wow! <a class="mention" href="https://x.com/DU_MLAX" rel="noopener noreferrer" target="_blank">@DU_MLAX</a> is going to the big show. Let\'s go Pios!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1792250318954082725',
    created: 1716140749000,
    type: 'post',
    text: 'It just dawned on me that wall ball is lacrosse kata. 🥍',
    likes: 1,
    retweets: 0
  },
  {
    id: '1792101461742047724',
    created: 1716105258000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DU_MLAX" rel="noopener noreferrer" target="_blank">@DU_MLAX</a> <a class="mention" href="https://x.com/NCAALAX" rel="noopener noreferrer" target="_blank">@NCAALAX</a> <a class="mention" href="https://x.com/Inside_Lacrosse" rel="noopener noreferrer" target="_blank">@Inside_Lacrosse</a> <a class="mention" href="https://x.com/USALacrosseMag" rel="noopener noreferrer" target="_blank">@USALacrosseMag</a> Share that rock and the magic will happen. Go Pios!!<br><br>In reply to: <a href="https://x.com/DU_MLAX/status/1791946391666962501" rel="noopener noreferrer" target="_blank">@DU_MLAX</a> <span class="status">1791946391666962501</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1791735478594543989',
    created: 1716018001000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SealsLax" rel="noopener noreferrer" target="_blank">@SealsLax</a> Here\'s the complaint I sent to the <a class="mention" href="https://x.com/NLL" rel="noopener noreferrer" target="_blank">@NLL</a>.<br><br>In reply to: <a href="#1791658020687970458">1791658020687970458</a>',
    photos: ['<div class="item"><img class="photo" src="media/1791735478594543989.png"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1791658020687970458',
    created: 1715999534000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SealsLax" rel="noopener noreferrer" target="_blank">@SealsLax</a> I\'ve given this more thought and I\'m going to contact the NLL and ESPN to complain. His behavior was just unacceptable. This is a professional league and that was the furthest thing from professional.<br><br>In reply to: <a href="#1791624227801505823">1791624227801505823</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1791624362149204166',
    created: 1715991509000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SealsLax" rel="noopener noreferrer" target="_blank">@SealsLax</a> I would be a #1 fan if he cleaned up his act.<br><br>In reply to: <a href="#1791624227801505823">1791624227801505823</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1791624227801505823',
    created: 1715991477000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SealsLax" rel="noopener noreferrer" target="_blank">@SealsLax</a> Staats needs to stop acting like a complete asshole when his team loses. He\'s such a talented lacrosse player and he\'s ruining all of that by acting like trash.<br><br>In reply to: <a href="https://x.com/SealsLax/status/1787227197477913006" rel="noopener noreferrer" target="_blank">@SealsLax</a> <span class="status">1787227197477913006</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1791623661889179650',
    created: 1715991342000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MichaelKarvelis" rel="noopener noreferrer" target="_blank">@MichaelKarvelis</a> <a class="mention" href="https://x.com/DailyDiveLAX" rel="noopener noreferrer" target="_blank">@DailyDiveLAX</a> I don\'t get Staats. He has so much talent and I love to watch him play, but he just acts like shit. I wish he would grow up, maybe learn something from Lyle.<br><br>In reply to: <a href="https://x.com/MichaelKarvelis/status/1788956986760991089" rel="noopener noreferrer" target="_blank">@MichaelKarvelis</a> <span class="status">1788956986760991089</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1791539195602698403',
    created: 1715971204000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/winamp" rel="noopener noreferrer" target="_blank">@winamp</a> This is still the best mp3 player that was ever created. Sometimes, simpler is just better.<br><br>In reply to: <a href="https://x.com/winamp/status/1791121664689725683" rel="noopener noreferrer" target="_blank">@winamp</a> <span class="status">1791121664689725683</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1791395006818767082',
    created: 1715936826000,
    type: 'post',
    text: 'I can no longer count on one hand the times I\'ve had to fix the Asciidoctor PDF build this month (a build that I know works). The platforms on which we rely just aren\'t stable anymore. Sorry, AI isn\'t going to fix this. You actually need to use your heads and not break things.',
    likes: 8,
    retweets: 1
  },
  {
    id: '1791249748835303604',
    created: 1715902194000,
    type: 'post',
    text: 'RVM is broken on my machine (Fedora 39). Just a long line of things that have worked for me forever that all of a sudden have stopped working this week.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1791246350035603747',
    created: 1715901384000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ninaturner" rel="noopener noreferrer" target="_blank">@ninaturner</a> It\'s quite clear to me that all but a few politicians in Washington haven\'t even been to most of the towns and cities in their own state. If they had, they\'d be just as horrified (at least I would hope so) and actually do something to address what\'s going on.<br><br>In reply to: <a href="#1791187680316797065">1791187680316797065</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1791187680316797065',
    created: 1715887396000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ninaturner" rel="noopener noreferrer" target="_blank">@ninaturner</a> I just drove across the country (Colorado to Maryland and back) and I was horrified by what I saw. There were so many ghost towns and abandoned areas. And don\'t get me started on the signs, which just exude hatred. I\'m severely concerned about the state of this country.<br><br>In reply to: <a href="https://x.com/ninaturner/status/1791183778397798724" rel="noopener noreferrer" target="_blank">@ninaturner</a> <span class="status">1791183778397798724</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1791183603537510805',
    created: 1715886424000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jamespearce" rel="noopener noreferrer" target="_blank">@jamespearce</a> <a class="mention" href="https://x.com/schickling" rel="noopener noreferrer" target="_blank">@schickling</a> Software is so critically timezone challenged that it\'s just embarrassing.<br><br>In reply to: <a href="https://x.com/jamespearce/status/1791176844001309146" rel="noopener noreferrer" target="_blank">@jamespearce</a> <span class="status">1791176844001309146</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1790863711042781534',
    created: 1715810156000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GrowTheGame3" rel="noopener noreferrer" target="_blank">@GrowTheGame3</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/boardroom" rel="noopener noreferrer" target="_blank">@boardroom</a> <a class="mention" href="https://x.com/BGCA_Clubs" rel="noopener noreferrer" target="_blank">@BGCA_Clubs</a> <a class="mention" href="https://x.com/ymca" rel="noopener noreferrer" target="_blank">@ymca</a> <a class="mention" href="https://x.com/OWLSLAX" rel="noopener noreferrer" target="_blank">@OWLSLAX</a> <a class="mention" href="https://x.com/bridgelacrosse" rel="noopener noreferrer" target="_blank">@bridgelacrosse</a> <a class="mention" href="https://x.com/DenverCityLax" rel="noopener noreferrer" target="_blank">@DenverCityLax</a> 💯<br><br>In reply to: <a href="https://x.com/GrowTheGame3/status/1789767872270434333" rel="noopener noreferrer" target="_blank">@GrowTheGame3</a> <span class="status">1789767872270434333</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1790863540535824447',
    created: 1715810115000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/boardroom" rel="noopener noreferrer" target="_blank">@boardroom</a> I love this idea of promoting lacrosse as a pickup sport. That\'s always been a part of the culture. It counters the myth that lacrosse is too expensive. You need a stick and ball (which you can borrow) and the rest is free. I hit the wall 3x/week and it\'s free!! And it should be.<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1789763418821390751" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1789763418821390751</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1790862606103695508',
    created: 1715809892000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLWaterdogs" rel="noopener noreferrer" target="_blank">@PLLWaterdogs</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/Michaelsowers22" rel="noopener noreferrer" target="_blank">@Michaelsowers22</a> <a class="mention" href="https://x.com/jcrew" rel="noopener noreferrer" target="_blank">@jcrew</a> Immediately added this to my wall ball routine.<br><br>In reply to: <a href="https://x.com/PLLWaterdogs/status/1790090937109999871" rel="noopener noreferrer" target="_blank">@PLLWaterdogs</a> <span class="status">1790090937109999871</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1790862122710077819',
    created: 1715809777000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLWaterdogs" rel="noopener noreferrer" target="_blank">@PLLWaterdogs</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/MikieSchlosser" rel="noopener noreferrer" target="_blank">@MikieSchlosser</a> Finally!!!<br><br>In reply to: <a href="https://x.com/PLLWaterdogs/status/1790786081501122957" rel="noopener noreferrer" target="_blank">@PLLWaterdogs</a> <span class="status">1790786081501122957</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1790862034164150629',
    created: 1715809756000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RJKaminski" rel="noopener noreferrer" target="_blank">@RJKaminski</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> In my mind, ESPN+ is what makes PLL and NLL the first pro lacrosse leagues that the masses actually watch (and bonus for linear). I can tune in to the games I want to watch when I want to watch them, and replay the highlights when they happen. There\'s no better way to watch.<br><br>In reply to: <a href="https://x.com/RJKaminski/status/1790838074953580690" rel="noopener noreferrer" target="_blank">@RJKaminski</a> <span class="status">1790838074953580690</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1790812510297186732',
    created: 1715797948000,
    type: 'post',
    text: 'Apparently, you can now break any API in a patch release as long as you say, "reasons, security". Fun times.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1790440928550760780',
    created: 1715709356000,
    type: 'post',
    text: 'I just broke a lacrosse ball (one of the best on the market) while doing wall ball. It just erupted when it hit the wall. I didn\'t even realize that was possible. I\'m going to count that as a win (minus the loss of one ball).',
    likes: 3,
    retweets: 0
  },
  {
    id: '1789947980239290752',
    created: 1715591828000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/NCAALAX" rel="noopener noreferrer" target="_blank">@NCAALAX</a> The playing without a stick penalty after a stick breaks is absolutely absurd. When it happens, it makes me want to turn the game off. It\'s incredibly awkward and pointless. Please get rid of it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1789777742180852096',
    created: 1715551240000,
    type: 'post',
    text: 'With all this 2FA now, I need a summon feature for my phone. "Phone, please come to me, wherever you are."',
    likes: 4,
    retweets: 0
  },
  {
    id: '1789717874304487825',
    created: 1715536967000,
    type: 'post',
    text: 'I\'m reminded almost every week why dependencies are bad more often times than not.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1788314856762089571',
    created: 1715202461000,
    type: 'post',
    text: 'Navigation systems are timezone challenged.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1787724305167491483',
    created: 1715061663000,
    type: 'post',
    text: 'The value of ESPN is and always has been the camera work. It\'s impeccable. It brings games to life.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1787593828179845320',
    created: 1715030555000,
    type: 'post',
    text: 'And how about timezones lines?!?',
    likes: 1,
    retweets: 0
  },
  {
    id: '1787589648769769517',
    created: 1715029558000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> I\'ll live without it. I\'m just pointing out that there is a huge difference for those who like to think there isn\'t.<br><br>In reply to: <a href="https://x.com/starbuxman/status/1787588578664693760" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">1787588578664693760</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1787572267158757456',
    created: 1715025414000,
    type: 'post',
    text: 'Tesla\'s self driving is miles ahead of Rivian. There\'s just no comparison. Rivian can stay in the lane, but I never know when it\'s just going to give up. In a Tesla, I could trust it to do the right thing, even go around slow cars or obstacles. Hopefully Rivian can catch up.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1787553862347296855',
    created: 1715021026000,
    type: 'post',
    text: 'Why are rest areas not labeled on the map for navigation systems in cars? That\'s literally what they are for.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1787497490582741237',
    created: 1715007586000,
    type: 'post',
    text: 'Every state in the US should allow 24hr overnight parking at rest areas (or at least 12hr). Kansas and West Virginia do, but few others. It\'s ridiculous. It\'s like they ignore the fact that people need sleep. This is so far from a free country, especially for things like this.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1787496102758904188',
    created: 1715007255000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/spencerbgibb" rel="noopener noreferrer" target="_blank">@spencerbgibb</a> And blind corners. And endless traffic.<br><br>In reply to: <a href="https://x.com/spencerbgibb/status/1787302681427562613" rel="noopener noreferrer" target="_blank">@spencerbgibb</a> <span class="status">1787302681427562613</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1787288401797910938',
    created: 1714957735000,
    type: 'post',
    text: 'Maryland roads are the worst. That\'s just a fact. Rarely a shoulder or sidewalk and frequently meandering. The highways that do exist often just come to an end at the most random spots.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1786405553742151999',
    created: 1714747248000,
    type: 'post',
    text: 'Ah, breeze. I had almost forgotten what it feels like. Definitely something I miss from the East Coast. Colorado has wind, but not breeze.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1785422733590876315',
    created: 1714512925000,
    type: 'quote',
    text: '"AsciiDoc seems to be the logical next step."<br><br>🎉<br><br>If you need anything, just know that the AsciiDoc community is always there, ready to jump in.<br><br>Quoting: <a href="https://x.com/Tubanter/status/1783535141257654391" rel="noopener noreferrer" target="_blank">@Tubanter</a> <span class="status">1783535141257654391</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1785412080067031150',
    created: 1714510385000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/couchbase" rel="noopener noreferrer" target="_blank">@couchbase</a> <a class="mention" href="https://x.com/ldoguin" rel="noopener noreferrer" target="_blank">@ldoguin</a> <a class="mention" href="https://x.com/k33g_org" rel="noopener noreferrer" target="_blank">@k33g_org</a> <a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> <a class="mention" href="https://x.com/simonw" rel="noopener noreferrer" target="_blank">@simonw</a> <a class="mention" href="https://x.com/OpenAI" rel="noopener noreferrer" target="_blank">@OpenAI</a> <a class="mention" href="https://x.com/LangChainAI" rel="noopener noreferrer" target="_blank">@LangChainAI</a> <a class="mention" href="https://x.com/streamlit" rel="noopener noreferrer" target="_blank">@streamlit</a> I like no clothing requirements. That works for my t-shirt and jogger style ;)<br><br>In reply to: <a href="https://x.com/couchbase/status/1785410889199374769" rel="noopener noreferrer" target="_blank">@couchbase</a> <span class="status">1785410889199374769</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1785409649795015089',
    created: 1714509806000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ldoguin" rel="noopener noreferrer" target="_blank">@ldoguin</a> <a class="mention" href="https://x.com/k33g_org" rel="noopener noreferrer" target="_blank">@k33g_org</a> <a class="mention" href="https://x.com/couchbase" rel="noopener noreferrer" target="_blank">@couchbase</a> <a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> <a class="mention" href="https://x.com/simonw" rel="noopener noreferrer" target="_blank">@simonw</a> <a class="mention" href="https://x.com/OpenAI" rel="noopener noreferrer" target="_blank">@OpenAI</a> <a class="mention" href="https://x.com/LangChainAI" rel="noopener noreferrer" target="_blank">@LangChainAI</a> <a class="mention" href="https://x.com/streamlit" rel="noopener noreferrer" target="_blank">@streamlit</a> ...just checking.<br><br>In reply to: <a href="https://x.com/ldoguin/status/1785363584798663164" rel="noopener noreferrer" target="_blank">@ldoguin</a> <span class="status">1785363584798663164</span>',
    photos: ['<div class="item"><img class="photo" src="media/1785409649795015089.jpg"></div>'],
    likes: 2,
    retweets: 0
  },
  {
    id: '1785006156475187557',
    created: 1714413606000,
    type: 'post',
    text: 'The friendship in this picture spans more than 40 years.',
    photos: ['<div class="item"><img class="photo" src="media/1785006156475187557.jpg"></div>'],
    likes: 7,
    retweets: 0
  },
  {
    id: '1783925096739958950',
    created: 1714155861000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> <a class="mention" href="https://x.com/MichaelPaus" rel="noopener noreferrer" target="_blank">@MichaelPaus</a> <a class="mention" href="https://x.com/github" rel="noopener noreferrer" target="_blank">@github</a> I solved it (for now) by changing the runner to macos-12.<br><br>In reply to: <a href="https://x.com/aalmiray/status/1783495219612205167" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">1783495219612205167</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1783922411571761221',
    created: 1714155221000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> <a class="mention" href="https://x.com/github" rel="noopener noreferrer" target="_blank">@github</a> Yep, confirmed this is a regression in Node.js. <a href="https://github.com/nodejs/node/issues/52705" rel="noopener noreferrer" target="_blank">github.com/nodejs/node/issues/52705</a><br><br>In reply to: <a href="#1783915062815592583">1783915062815592583</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1783915062815592583',
    created: 1714153469000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> <a class="mention" href="https://x.com/github" rel="noopener noreferrer" target="_blank">@github</a> Yep, there\'s some sort of bug in Node.js that is causing Antora to enter an infinite loop when setting the mtime on a file handle.<br><br>In reply to: <a href="#1783914101086748799">1783914101086748799</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1783914101086748799',
    created: 1714153239000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> <a class="mention" href="https://x.com/github" rel="noopener noreferrer" target="_blank">@github</a> There may be an issue with Node.js 22. It was just released and we\'ve not yet had a chance to test it with Antora. I recommend using Node.js 20 for now.<br><br>In reply to: <a href="https://x.com/aalmiray/status/1783896333041991956" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">1783896333041991956</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1783890448773238953',
    created: 1714147600000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> <a class="mention" href="https://x.com/MichaelPaus" rel="noopener noreferrer" target="_blank">@MichaelPaus</a> <a class="mention" href="https://x.com/github" rel="noopener noreferrer" target="_blank">@github</a> GitHub did a terrible job of communicating this change.<br><br>In reply to: <a href="https://x.com/aalmiray/status/1783495219612205167" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">1783495219612205167</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1783474111924084954',
    created: 1714048338000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> Same. Having problems installing brew casks.<br><br>In reply to: <a href="https://x.com/aalmiray/status/1783471328718107091" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">1783471328718107091</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1781475689855602727',
    created: 1713571877000,
    type: 'post',
    text: 'If someone added a commit like that to a project I lead, I would take away their commit access immediately.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1781474932712472838',
    created: 1713571696000,
    type: 'post',
    text: 'One of the ugliest parts of git is "Merge remote-tracking branch ...". Given how mature and widespread git is now, how is that even still a thing that git does by default?',
    likes: 1,
    retweets: 0
  },
  {
    id: '1781403690294747268',
    created: 1713554711000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dashaun" rel="noopener noreferrer" target="_blank">@dashaun</a> <a class="mention" href="https://x.com/phillip_webb" rel="noopener noreferrer" target="_blank">@phillip_webb</a> <a class="mention" href="https://x.com/m_halbritter" rel="noopener noreferrer" target="_blank">@m_halbritter</a> <a class="mention" href="https://x.com/GalletVictor" rel="noopener noreferrer" target="_blank">@GalletVictor</a> <a class="mention" href="https://x.com/rob_winch" rel="noopener noreferrer" target="_blank">@rob_winch</a> 😊<br><br>In reply to: <a href="https://x.com/dashaun/status/1781396115092373688" rel="noopener noreferrer" target="_blank">@dashaun</a> <span class="status">1781396115092373688</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1781034335858036876',
    created: 1713466650000,
    type: 'post',
    text: 'I\'m now adopting the practice of locking closed issues in the projects I run. I don\'t want discussions happening on zombie threads. It\'s not actionable. Those follow-ups should be directed to the chat/forum or a new issue. (If an issue needs to be unlocked/reopened, I\'ll do it).',
    likes: 6,
    retweets: 0
  },
  {
    id: '1780796787642527869',
    created: 1713410014000,
    type: 'quote',
    text: '<a class="mention" href="https://x.com/BMcG1230" rel="noopener noreferrer" target="_blank">@BMcG1230</a> 👇<br><br>Quoting: <a href="https://x.com/PremierLacrosse/status/1779981384850825481" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1779981384850825481</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1780188340597264592',
    created: 1713264949000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/jfreewright" rel="noopener noreferrer" target="_blank">@jfreewright</a> <a class="mention" href="https://x.com/PaulRabil" rel="noopener noreferrer" target="_blank">@PaulRabil</a> Lacrosse is such a great equalizer. No matter who we are or what we\'ve done, we all need to hit the wall to dial in the toss.<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1779981384850825481" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1779981384850825481</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1779243937800110441',
    created: 1713039786000,
    type: 'post',
    text: 'Victory in Denver! Go Pios! <a class="mention" href="https://x.com/DU_MLAX" rel="noopener noreferrer" target="_blank">@DU_MLAX</a>',
    photos: ['<div class="item"><img class="photo" src="media/1779243937800110441.jpg"></div>'],
    likes: 1,
    retweets: 0
  },
  {
    id: '1778556169369313386',
    created: 1712875809000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/trisha_gee" rel="noopener noreferrer" target="_blank">@trisha_gee</a> <a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <a class="mention" href="https://x.com/devnexus" rel="noopener noreferrer" target="_blank">@devnexus</a> 💯<br><br>In reply to: <a href="https://x.com/trisha_gee/status/1778532348801454547" rel="noopener noreferrer" target="_blank">@trisha_gee</a> <span class="status">1778532348801454547</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1778213873088155776',
    created: 1712794199000,
    type: 'post',
    text: 'Colorado Pint Day! The sun is shining bright. The highlight of the day is that we actually found a brewery that had some glasses still available. Thanks <a class="mention" href="https://x.com/IronMuleBrewery" rel="noopener noreferrer" target="_blank">@IronMuleBrewery</a>',
    photos: ['<div class="item"><img class="photo" src="media/1778213873088155776.jpg"></div>'],
    likes: 3,
    retweets: 1
  },
  {
    id: '1777607766938464746',
    created: 1712649692000,
    type: 'quote',
    text: 'Interesting. 🤔<br><br>My first reaction is that there\'s an abundance of open source, and how it gets created varies a ton. Therefore we need many homes. I\'ll be very curious to see how this takes shape.<br><br>Quoting: <a href="https://x.com/myfear/status/1777606126760071342" rel="noopener noreferrer" target="_blank">@myfear</a> <span class="status">1777606126760071342</span>',
    likes: 8,
    retweets: 0
  },
  {
    id: '1777607234740007383',
    created: 1712649565000,
    type: 'post',
    text: 'I recently learned that a dot in a property name breaks the Maven build on Windows (PowerShell). For example, -Dmaven.test.skip=true. The option has to be written as "-Dmaven.test.skip=true". That\'s really unfortunate.',
    likes: 1,
    retweets: 1
  },
  {
    id: '1776443598227067004',
    created: 1712372133000,
    type: 'post',
    text: 'The Asciidoctor PDF build is being a brat. There\'s just no other way to say it. 🍼🍼🍼',
    likes: 6,
    retweets: 2
  },
  {
    id: '1776186392336290077',
    created: 1712310810000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CornellLacrosse" rel="noopener noreferrer" target="_blank">@CornellLacrosse</a> Insane comeback. Way to go!<br><br>In reply to: <a href="https://x.com/CornellLacrosse/status/1775367680700145852" rel="noopener noreferrer" target="_blank">@CornellLacrosse</a> <span class="status">1775367680700145852</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1775473058691600469',
    created: 1712140738000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rmannibucau" rel="noopener noreferrer" target="_blank">@rmannibucau</a> Actually, it is clear in the docs, I just missed it. Thanks again!<br><br>In reply to: <a href="#1775472926977851560">1775472926977851560</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1775472926977851560',
    created: 1712140707000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rmannibucau" rel="noopener noreferrer" target="_blank">@rmannibucau</a> OMG, that was it! I was missing the "invoker." prefix. That was not clear from the docs, but it makes total sense in retrospect. Thanks!<br><br>In reply to: <a href="https://x.com/rmannibucau/status/1775470745298014655" rel="noopener noreferrer" target="_blank">@rmannibucau</a> <span class="status">1775470745298014655</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1775469666657251546',
    created: 1712139929000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> I\'m not sure where that goes here: <a href="https://gitlab.com/antora/antora-maven-plugin/-/blob/main/pom.xml?ref_type=heads#L59-84" rel="noopener noreferrer" target="_blank">gitlab.com/antora/antora-maven-plugin/-/blob/main/pom.xml?ref_type=heads#L59-84</a><br><br>In reply to: <a href="https://x.com/alexsotob/status/1775464425127878794" rel="noopener noreferrer" target="_blank">@alexsotob</a> <span class="status">1775464425127878794</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1775462932454719519',
    created: 1712138324000,
    type: 'post',
    text: 'What\'s the trick to get Maven invoker to write JUnit reports? It\'s not working for me (even when setting writeJunitReport=true).',
    likes: 2,
    retweets: 0
  },
  {
    id: '1775408119142564256',
    created: 1712125255000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/adrianfcole" rel="noopener noreferrer" target="_blank">@adrianfcole</a> Indeed. And just to be clear, I accept the path I have chosen and there are many reasons I do it. What I object to is someone saying that money wouldn\'t make it easier (or help the project). That\'s just a laughable claim.<br><br>In reply to: <a href="https://x.com/adrianfcole/status/1775362681740103759" rel="noopener noreferrer" target="_blank">@adrianfcole</a> <span class="status">1775362681740103759</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1775328623999832123',
    created: 1712106302000,
    type: 'post',
    text: 'I could also hire someone to help me, which would solve the other problem of being swamped all the time.',
    likes: 6,
    retweets: 0
  },
  {
    id: '1775296923425018116',
    created: 1712098744000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cra" rel="noopener noreferrer" target="_blank">@cra</a> Money is and always will be issue #1. I can do so much more in OSS if I wasn\'t constantly having to worry about going broke. I\'m not talking about wealth. That\'s an imposed goal of capitalism. I\'m talking about being able to eat and cover expenses so I can focus on the task.<br><br>In reply to: <a href="https://x.com/cra/status/1775287868350480874" rel="noopener noreferrer" target="_blank">@cra</a> <span class="status">1775287868350480874</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1775289621611500031',
    created: 1712097003000,
    type: 'quote',
    text: 'Bull shit. If I was paid to work on OSS projects full time without having to secure contracts to eat and pay expenses, my life would be so much easier. My life is a constant battle to balance income and expenditures (and it doesn\'t help that the cost of food has become absurd).<br><br>Quoting: <a href="https://x.com/lorenc_dan/status/1775285656383639778" rel="noopener noreferrer" target="_blank">@lorenc_dan</a> <span class="status">1775285656383639778</span>',
    likes: 34,
    retweets: 5
  },
  {
    id: '1775288766564876591',
    created: 1712096799000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cortizq" rel="noopener noreferrer" target="_blank">@cortizq</a> <a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> <a class="mention" href="https://x.com/ggrossetie" rel="noopener noreferrer" target="_blank">@ggrossetie</a> Yep, we never intended to give preference to Gradle. But someone needed to step up to finish developing the Maven plugin (and bring it to feature parity with the Gradle plugin). No one did, so I finally had to do it (thanks to support from a contract).<br><br>In reply to: <a href="https://x.com/cortizq/status/1775274046109917303" rel="noopener noreferrer" target="_blank">@cortizq</a> <span class="status">1775274046109917303</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1775248371285897663',
    created: 1712087168000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cortizq" rel="noopener noreferrer" target="_blank">@cortizq</a> <a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> <a class="mention" href="https://x.com/ggrossetie" rel="noopener noreferrer" target="_blank">@ggrossetie</a> I\'ve literally spent the last two weeks working on a Maven plugin. See <a href="https://gitlab.com/antora/antora-maven-plugin" rel="noopener noreferrer" target="_blank">gitlab.com/antora/antora-maven-plugin</a><br><br>In reply to: <a href="https://x.com/cortizq/status/1775232651017724156" rel="noopener noreferrer" target="_blank">@cortizq</a> <span class="status">1775232651017724156</span>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1775084132667662384',
    created: 1712048011000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> <a class="mention" href="https://x.com/cortizq" rel="noopener noreferrer" target="_blank">@cortizq</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> <a class="mention" href="https://x.com/ggrossetie" rel="noopener noreferrer" target="_blank">@ggrossetie</a> You might be interested to know there\'s now an Antora plugin for Gradle. See <a href="https://docs.antora.org/gradle-plugin/latest/" rel="noopener noreferrer" target="_blank">docs.antora.org/gradle-plugin/latest/</a> That would allow you to use Gradle to drive all aspects of the build, including the site.<br><br>In reply to: <a href="https://x.com/mraible/status/1774832355112280506" rel="noopener noreferrer" target="_blank">@mraible</a> <span class="status">1774832355112280506</span>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1775057455090639119',
    created: 1712041650000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danhillenbrand" rel="noopener noreferrer" target="_blank">@danhillenbrand</a> I\'ve definitely been practicing it regularly...practicing 🥍 instead.<br><br>In reply to: <a href="https://x.com/danhillenbrand/status/1775047185396039953" rel="noopener noreferrer" target="_blank">@danhillenbrand</a> <span class="status">1775047185396039953</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1774722627170439203',
    created: 1711961821000,
    type: 'post',
    text: 'It\'s ignore social media day.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1774343474810061169',
    created: 1711871424000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dnsmichi" rel="noopener noreferrer" target="_blank">@dnsmichi</a> I\'d really like to be able to do this: <a href="https://github.com/asciidoctor/asciidoctor-pdf/blob/main/.github/workflows/release.yml#L2" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-pdf/blob/main/.github/workflows/release.yml#L2</a> Otherwise, I can\'t see which pipelines/workflows trigger a release. Without the custom name, it just looks like a regular run.<br><br>In reply to: <a href="https://x.com/dnsmichi/status/1774136169170612647" rel="noopener noreferrer" target="_blank">@dnsmichi</a> <span class="status">1774136169170612647</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1774016010049196454',
    created: 1711793351000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> Not a single other language package system I know of (at least that I use) uses gpg signed artifacts. And those are no less secure. It\'s not only theater, but it gates the Java community to newcomers who struggle to go through these ridiculous hoops.<br><br>In reply to: <a href="#1774015669912088784">1774015669912088784</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1774015669912088784',
    created: 1711793269000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> We can agree to disagree. In all my time using Java, those signatures have never proved anything. All it tells me is that someone signed it. I\'d have to do a key exchange w/ the signer to verify it\'s who I think it is. That still doesn\'t tell me anything about the source code.<br><br>In reply to: <a href="https://x.com/odrotbohm/status/1774009775019188571" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <span class="status">1774009775019188571</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1773989787113767332',
    created: 1711787099000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> To help the situation, I do plan to bundle up the script I use for releasing (not limited to Java) so others, including myself in the future, can save a week getting a new project set up. And it won\'t be just yet another blog post, but something that\'s truly reusable.<br><br>In reply to: <a href="#1773989417562059189">1773989417562059189</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1773989417562059189',
    created: 1711787010000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> Even though it\'s not something that has to be done regularly, I just don\'t find it acceptable. It\'s a perfect example of why the other language platforms consistently win out over Java for newcomers. It\'s just too cumbersome.<br><br>In reply to: <a href="https://x.com/joschi83/status/1773986085590241355" rel="noopener noreferrer" target="_blank">@joschi83</a> <span class="status">1773986085590241355</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1773985614414655775',
    created: 1711786104000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jwf_foss" rel="noopener noreferrer" target="_blank">@jwf_foss</a> 100%<br><br>In reply to: <a href="https://x.com/jwf_foss/status/1773876393710882896" rel="noopener noreferrer" target="_blank">@jwf_foss</a> <span class="status">1773876393710882896</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1773984192952422407',
    created: 1711785765000,
    type: 'post',
    text: 'Am I just missing something, or is there no way to name a pipeline in GitLab CI when you run it from the web? I\'d like to be able to see in the list of pipelines which ones I ran manually.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1773977120630345827',
    created: 1711784079000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bsideup" rel="noopener noreferrer" target="_blank">@bsideup</a> <a class="mention" href="https://x.com/testcontainers" rel="noopener noreferrer" target="_blank">@testcontainers</a> What I settled on is that I have to submit the private key as an input parameter to the CI job that kicks off the release. I\'m actually fine with that because it\'s not a terrible inconvenience and it never gets stored. And it works with how I like to do releases.<br><br>In reply to: <a href="#1773976781407625332">1773976781407625332</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1773976781407625332',
    created: 1711783998000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bsideup" rel="noopener noreferrer" target="_blank">@bsideup</a> <a class="mention" href="https://x.com/testcontainers" rel="noopener noreferrer" target="_blank">@testcontainers</a> Thanks for sharing. The problem is (on top of everything else I went through) is that Maven Central is no longer allowing new artifacts to use OSSRH. They have a new plugin and process and it works completely differently. Now the signatures have to be uploaded with the bundle.<br><br>In reply to: <a href="https://x.com/bsideup/status/1773906584864784535" rel="noopener noreferrer" target="_blank">@bsideup</a> <span class="status">1773906584864784535</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1773976195912085528',
    created: 1711783858000,
    type: 'post',
    text: 'I should clarify that this was time spent to do it right. I\'m not talking about a hack job. I\'m talking about the time it takes to get the correct result the first time without a dozen burned versions in the repository with botched releases.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1773974529821270507',
    created: 1711783461000,
    type: 'post',
    text: 'Of course, it all looks easy once you\'ve figured it out, which is probably why the situation never gets better. But the next person down the line can kiss a week away struggling to get their project published.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1773973838864269357',
    created: 1711783296000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> Then you need to figure out how to orchestrate the gpg process into the Maven build at the right time, with the right version, with the right inputs so the right version gets signed. And that doesn\'t even touch on the credentials for pushing to the git repository.<br><br>In reply to: <a href="#1773973471761977472">1773973471761977472</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1773973471761977472',
    created: 1711783209000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> Again, it\'s not the software that\'s the problem, it\'s the concept. Make a key and know how to actually make it correctly. Then you have to find a non-broken keyserver to publish it. Then you need to protect it in CI properly. It\'s not the software that\'s the issue.<br><br>In reply to: <a href="https://x.com/joschi83/status/1773967276980445304" rel="noopener noreferrer" target="_blank">@joschi83</a> <span class="status">1773967276980445304</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1773973204203106551',
    created: 1711783145000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> <a class="mention" href="https://x.com/sonatype" rel="noopener noreferrer" target="_blank">@sonatype</a> <a class="mention" href="https://x.com/projectsigstore" rel="noopener noreferrer" target="_blank">@projectsigstore</a> It\'s pompous to say gpg signing isn\'t hard. It\'s fucking ridiculously hard. It requires a high level of technology knowledge, esp to do it with any sort of integrity. I\'ve used Linux for 2 decades and have achieved the highest job role in software dev and it still took me days.<br><br>In reply to: <a href="https://x.com/aalmiray/status/1773938001598447682" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">1773938001598447682</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1773971615497191816',
    created: 1711782766000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> And if the key ever gets lost or stolen, now all the artifacts that were signed by a "good key" also look burned, which is not true. What matters is the integrity of the project and the source it points to. That\'s all that really matters.<br><br>In reply to: <a href="#1773971286332482008">1773971286332482008</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1773971286332482008',
    created: 1711782688000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> Because it\'s complete security theater. I can create a key today, publish it, sign it, and still publish artifacts. And that key can contain any email address I can think of. No one is validating what I\'m doing, only that a bunch of .asc files are there. Who does this protect?<br><br>In reply to: <a href="https://x.com/odrotbohm/status/1773932530586034409" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <span class="status">1773932530586034409</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1773884152498315711',
    created: 1711761913000,
    type: 'post',
    text: 'But I\'m pleased with myself because I did manage to figure it out.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1773883698867560581',
    created: 1711761805000,
    type: 'post',
    text: 'It took me 24+ hours (I\'m talking time spent, not time elapsed) to publish artifacts from CI for a new project to Maven Central. And no, some new release tool isn\'t going to help. It\'s mostly because artifacts have to be gpg signed, which is absurd. It\'s just too damn hard.',
    likes: 6,
    retweets: 2
  },
  {
    id: '1772949029757866268',
    created: 1711538963000,
    type: 'post',
    text: 'The Francis Scott Key Bridge was completed the year before I\'ve born, so it\'s been there my whole life. I\'ve been over it a few times. (It\'s kind of out of the way, so it\'s usually faster to take 95). It\'s so surreal to see it laying in the river.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1772734163860193364',
    created: 1711487735000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lorenzo_bettini" rel="noopener noreferrer" target="_blank">@lorenzo_bettini</a> 🤦<br><br>In reply to: <a href="https://x.com/lorenzo_bettini/status/1772705181332890056" rel="noopener noreferrer" target="_blank">@lorenzo_bettini</a> <span class="status">1772705181332890056</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1772734105639071758',
    created: 1711487721000,
    type: 'quote',
    text: '<a class="mention" href="https://x.com/simonebordet" rel="noopener noreferrer" target="_blank">@simonebordet</a> Lots of votes for htmx too.<br><br>Quoting: <a href="https://x.com/bahadir/status/1772704818382913993" rel="noopener noreferrer" target="_blank">@bahadir</a> <span class="status">1772704818382913993</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1772697259093733451',
    created: 1711478936000,
    type: 'quote',
    text: '<a class="mention" href="https://x.com/simonebordet" rel="noopener noreferrer" target="_blank">@simonebordet</a> 👇<br><br>Quoting: <a href="https://x.com/NiestrojRobert/status/1772693746963878376" rel="noopener noreferrer" target="_blank">@NiestrojRobert</a> <span class="status">1772693746963878376</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1772696995183923357',
    created: 1711478873000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/imgracehuang" rel="noopener noreferrer" target="_blank">@imgracehuang</a> We\'re always happen to answer questions in the project chat at <a href="https://chat.asciidoctor.org" rel="noopener noreferrer" target="_blank">chat.asciidoctor.org</a>. Hope to see you there!<br><br>In reply to: <a href="https://x.com/imgracehuang/status/1772680019061457311" rel="noopener noreferrer" target="_blank">@imgracehuang</a> <span class="status">1772680019061457311</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1772696853504483499',
    created: 1711478839000,
    type: 'post',
    text: 'It just hit me that Process#getInputStream() returns the output of a process and Process#getOutputStream() returns the input of a process. Java, you so crazy.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1772673609003147702',
    created: 1711473297000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/imgracehuang" rel="noopener noreferrer" target="_blank">@imgracehuang</a> Here\'s a better (more current) reference: <a href="https://docs.asciidoctor.org/asciidoc/latest/verbatim/callouts/" rel="noopener noreferrer" target="_blank">docs.asciidoctor.org/asciidoc/latest/verbatim/callouts/</a><br><br>In reply to: <a href="https://x.com/imgracehuang/status/1772652625692922110" rel="noopener noreferrer" target="_blank">@imgracehuang</a> <span class="status">1772652625692922110</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1772669245714788792',
    created: 1711472257000,
    type: 'post',
    text: 'If you were making an archetype project today that has a web-based UI, what client-side JavaScript framework would you target? What would have the most appeal to new users?',
    likes: 1,
    retweets: 0
  },
  {
    id: '1770659416070648298',
    created: 1710993076000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> The privileged live an insufferable existence. I\'m finally learning to not let them have control over me and close, then block. Heaven forbid they struggle for a second in their lives. Thanks to their attitude, they\'ll have to face their newfound challenges all on their own.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1770599610744000882" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1770599610744000882</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1770563548843765768',
    created: 1710970220000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/flounder4130" rel="noopener noreferrer" target="_blank">@flounder4130</a> <a class="mention" href="https://x.com/OMFGNuts" rel="noopener noreferrer" target="_blank">@OMFGNuts</a> <a class="mention" href="https://x.com/theredcuber" rel="noopener noreferrer" target="_blank">@theredcuber</a> <a class="mention" href="https://x.com/onwriterside" rel="noopener noreferrer" target="_blank">@onwriterside</a> lines are for a very specific use case when you can rely on them to be stable. Otherwise, you\'d use tags. This is all documented in the AsciiDoc docs. <a href="https://docs.asciidoctor.org/asciidoc/latest/directives/include/" rel="noopener noreferrer" target="_blank">docs.asciidoctor.org/asciidoc/latest/directives/include/</a><br><br>In reply to: <a href="https://x.com/flounder4130/status/1770547962826952891" rel="noopener noreferrer" target="_blank">@flounder4130</a> <span class="status">1770547962826952891</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1770563153790681459',
    created: 1710970125000,
    type: 'post',
    text: 'OH: "I never got the memo that my butt muscles would just stop working after 40."<br><br>Hard relate. Gotta do those glute bridges every morning!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1770562833438150977',
    created: 1710970049000,
    type: 'post',
    text: 'I got a marketing message with the subject "Take charge of your phone." I was under the impression I was already in charge of my own phone, unless we\'re living in the matrix. 🤔',
    likes: 0,
    retweets: 0
  },
  {
    id: '1770373532549033984',
    created: 1710924916000,
    type: 'post',
    text: 'If you play hockey, you\'re probably going to be very good at lacrosse already. Try it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1770373378819428858',
    created: 1710924880000,
    type: 'post',
    text: 'I helped a newcomer to lacrosse with his stick skills today and hopefully gave him tips that will set him on a path to rapidly improve. It felt really good.<br><br>Granted, he was picking it up so quickly, I had to ask what other sport he played before lacrosse. Hockey. Explains a ton.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1770030900165722309',
    created: 1710843226000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Rivian" rel="noopener noreferrer" target="_blank">@Rivian</a> Huge, huge, huge!<br><br>In reply to: <a href="https://x.com/Rivian/status/1769758297438159021" rel="noopener noreferrer" target="_blank">@Rivian</a> <span class="status">1769758297438159021</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1769860755304649214',
    created: 1710802661000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/__Cloudia" rel="noopener noreferrer" target="_blank">@__Cloudia</a> Happy Birthday! Hope you\'re having a ball! 🍻',
    likes: 0,
    retweets: 0
  },
  {
    id: '1769856545859240093',
    created: 1710801657000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <a class="mention" href="https://x.com/TerpsMLax" rel="noopener noreferrer" target="_blank">@TerpsMLax</a> 💯 Always.<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1769852628060848202" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1769852628060848202</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1769841253175157230',
    created: 1710798011000,
    type: 'post',
    text: 'UVA lacrosse has the ugliest uniforms I\'ve ever seen. I\'m not even really that picky. They are just hard to look at.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1768943097214288019',
    created: 1710583874000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/oleg_nenashev" rel="noopener noreferrer" target="_blank">@oleg_nenashev</a> I so wish I could travel by train around the US, but it\'s so impractical and expensive it causes a headache just thinking about it. I think I would have made a good European because I truly appreciate the value of a good train system.<br><br>In reply to: <a href="https://x.com/oleg_nenashev/status/1768934589542682838" rel="noopener noreferrer" target="_blank">@oleg_nenashev</a> <span class="status">1768934589542682838</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1768849061887594521',
    created: 1710561454000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mattfarina" rel="noopener noreferrer" target="_blank">@mattfarina</a> Years. (But it\'s finally starting to bear fruit).<br><br>In reply to: <a href="https://x.com/mattfarina/status/1768815673323454922" rel="noopener noreferrer" target="_blank">@mattfarina</a> <span class="status">1768815673323454922</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1768797239743553859',
    created: 1710549099000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mattfarina" rel="noopener noreferrer" target="_blank">@mattfarina</a> That\'s why we\'re working on a specification. The tools are held back because the language doesn\'t have a formal definition (though it\'s taking shape). Once it does, more implementations will happen and tools will follow. The AsciiDoc WG is tasked with advocating for this growth.<br><br>In reply to: <a href="https://x.com/mattfarina/status/1768750593567301741" rel="noopener noreferrer" target="_blank">@mattfarina</a> <span class="status">1768750593567301741</span>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1768791753841066241',
    created: 1710547791000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <a class="mention" href="https://x.com/jetbrains" rel="noopener noreferrer" target="_blank">@jetbrains</a> <a class="mention" href="https://x.com/intellijidea" rel="noopener noreferrer" target="_blank">@intellijidea</a> <a class="mention" href="https://x.com/java" rel="noopener noreferrer" target="_blank">@java</a> <a class="mention" href="https://x.com/JetBrains_Edu" rel="noopener noreferrer" target="_blank">@JetBrains_Edu</a> <a class="mention" href="https://x.com/eMalaGupta" rel="noopener noreferrer" target="_blank">@eMalaGupta</a> <a class="mention" href="https://x.com/MaritvanDijk77" rel="noopener noreferrer" target="_blank">@MaritvanDijk77</a> It\'s IntelliJ for me too right now. That\'s because the AsciiDoc support is world class (thanks to the work of <a class="mention" href="https://x.com/ahus1de" rel="noopener noreferrer" target="_blank">@ahus1de</a> and community).<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1768676052107428253" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1768676052107428253</span>',
    likes: 11,
    retweets: 1
  },
  {
    id: '1768588659828162890',
    created: 1710499369000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Yes, the correct behavior is that it should consider that back reference to be a failed match if it wasn\'t captured. Every regexp engine gets this right except JavaScript.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1768584789018226762" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1768584789018226762</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1768575493802983520',
    created: 1710496230000,
    type: 'post',
    text: 'Why in the world does JavaScript consider a back reference to a non-matching group a successful match? Who thought that was a good idea? 😩',
    likes: 1,
    retweets: 0
  },
  {
    id: '1768359478582206957',
    created: 1710444728000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bsideup" rel="noopener noreferrer" target="_blank">@bsideup</a> It\'s a welcoming gift!<br><br>In reply to: <a href="https://x.com/bsideup/status/1768353941626949896" rel="noopener noreferrer" target="_blank">@bsideup</a> <span class="status">1768353941626949896</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1767648109453033941',
    created: 1710275125000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/josephbottinger" rel="noopener noreferrer" target="_blank">@josephbottinger</a> <a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> I encourage you to make use of the Edit this Page feature.<br><br>In reply to: <a href="https://x.com/josephbottinger/status/1767644245702156479" rel="noopener noreferrer" target="_blank">@josephbottinger</a> <span class="status">1767644245702156479</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1767636281222127895',
    created: 1710272305000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/saronyitbarek" rel="noopener noreferrer" target="_blank">@saronyitbarek</a> Btw, I don\'t consider myself good at debugging, just persistent.<br><br>In reply to: <a href="#1767635687627510137">1767635687627510137</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1767635687627510137',
    created: 1710272163000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/saronyitbarek" rel="noopener noreferrer" target="_blank">@saronyitbarek</a> First and foremost, it\'s about mindset. You have to believe there\'s a reason something is going wrong, because there is. Once you establish that realization, the outcome seems far more achievable. You\'re in the right frame of mind to solve it.<br><br>In reply to: <a href="https://x.com/saronyitbarek/status/1767634971672678642" rel="noopener noreferrer" target="_blank">@saronyitbarek</a> <span class="status">1767634971672678642</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1767634410948481352',
    created: 1710271859000,
    type: 'post',
    text: 'At last, the docs for the Asciidoctor Gradle plugin are now available at <a href="https://docs.asciidoctor.org" rel="noopener noreferrer" target="_blank">docs.asciidoctor.org</a>. You can thank <a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> for putting in the work to make this happen.',
    likes: 20,
    retweets: 6
  },
  {
    id: '1767481309025190128',
    created: 1710235356000,
    type: 'quote',
    text: 'You can play with the Asciidoctor API directly in the browser using the following URL:<br><br><a href="https://runruby.dev/?gem=asciidoctor" rel="noopener noreferrer" target="_blank">runruby.dev/?gem=asciidoctor</a><br><br>Quoting: <a href="https://x.com/skryukov_dev/status/1757495602697454072" rel="noopener noreferrer" target="_blank">@skryukov_dev</a> <span class="status">1757495602697454072</span>',
    likes: 5,
    retweets: 2
  },
  {
    id: '1767384288108953774',
    created: 1710212225000,
    type: 'post',
    text: 'These warnings and pending removals are continuing to cause me tremendous problems.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1767384062405001456',
    created: 1710212171000,
    type: 'post',
    text: 'It\'s more than just base64. Several default gems are being removed, including bigdecimal. The decision to remove these stdlib libraries in a minor release violates semantic versioning and was an extremely poor choice.<br><br>Quoting: <a href="#1765870415753183732">1765870415753183732</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1767123044328219097',
    created: 1710149939000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DU_MLAX" rel="noopener noreferrer" target="_blank">@DU_MLAX</a> First, that was a thrilling game. Sure, it was a tough loss, but learn from it and keep your heads high. This is a great team and the goal is to have fun and make the tournament. #1 was just a recognition of what\'s still to come. Go Pios!<br><br>In reply to: <a href="https://x.com/DU_MLAX/status/1766922813460324695" rel="noopener noreferrer" target="_blank">@DU_MLAX</a> <span class="status">1766922813460324695</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1766578304293785726',
    created: 1710020063000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jnorthr" rel="noopener noreferrer" target="_blank">@jnorthr</a> ❤️<br><br>In reply to: <a href="https://x.com/jnorthr/status/1766577322415550827" rel="noopener noreferrer" target="_blank">@jnorthr</a> <span class="status">1766577322415550827</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1766575277885272313',
    created: 1710019342000,
    type: 'post',
    text: 'It\'s important to understand that we build open source for us. If you want it to be built for you, then you can join us to help build it and then it will be built for you too. That\'s how it works.',
    likes: 51,
    retweets: 14
  },
  {
    id: '1766207087325896739',
    created: 1709931558000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Joklinztech" rel="noopener noreferrer" target="_blank">@Joklinztech</a> Thanks for writing this up. Feel free to share this in the community chat at <a href="https://chat.antora.org" rel="noopener noreferrer" target="_blank">chat.antora.org</a>. Hope to see you there.<br><br>In reply to: <a href="https://x.com/Joklinztech/status/1766111401045631364" rel="noopener noreferrer" target="_blank">@Joklinztech</a> <span class="status">1766111401045631364</span>',
    likes: 4,
    retweets: 1
  },
  {
    id: '1765870415753183732',
    created: 1709851290000,
    type: 'post',
    text: 'At least two times now in the Ruby 3.x series, Ruby has violated semantic versioning. In one case, they changed the signature of the Logger constructor. In the other, they added a warning about the use of the base64 library (which used to be stdlib). This is concerning.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1765340058079375841',
    created: 1709724842000,
    type: 'post',
    text: 'It also screws up pro games because it\'s hard for new players to adjust to not having the rule. It just annoys me to no end.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1765339766042640877',
    created: 1709724773000,
    type: 'post',
    text: 'I absolutely hate the playing without a stick infraction in <a class="mention" href="https://x.com/NCAALAX" rel="noopener noreferrer" target="_blank">@NCAALAX</a> (lacrosse). It\'s just silly and unnecessarily disrupts game play. There\'s just no reason for it, and it\'s not a rule in the pros. That rule has to go.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1764074278603907358',
    created: 1709423057000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CornellLacrosse" rel="noopener noreferrer" target="_blank">@CornellLacrosse</a> Yeah!<br><br>In reply to: <a href="https://x.com/CornellLacrosse/status/1764021854694457829" rel="noopener noreferrer" target="_blank">@CornellLacrosse</a> <span class="status">1764021854694457829</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1762248549650038853',
    created: 1708987769000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> I feel like that\'s how a lot of my days go ;)<br><br>In reply to: <a href="https://x.com/headius/status/1762243215283945783" rel="noopener noreferrer" target="_blank">@headius</a> <span class="status">1762243215283945783</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1761869455812895028',
    created: 1708897386000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bbatsov" rel="noopener noreferrer" target="_blank">@bbatsov</a> Personally, I don\'t really care if it ever becomes more popular. My focus is to serve those who use it today and want it to be a solid foundation for years to come. Maybe that leads to growth. Great. But it\'s not my end game.<br><br>In reply to: <a href="https://x.com/bbatsov/status/1761860874111779217" rel="noopener noreferrer" target="_blank">@bbatsov</a> <span class="status">1761860874111779217</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1761868962722173107',
    created: 1708897269000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bbatsov" rel="noopener noreferrer" target="_blank">@bbatsov</a> There\'s no draft yet, just an outline. But we have drafted and committed the first section (paragraph). We have no idea how long it\'s going to take. It\'s the hardest thing I\'ve ever done by far. We can only take it one day at at time.<br><br>In reply to: <a href="https://x.com/bbatsov/status/1761860874111779217" rel="noopener noreferrer" target="_blank">@bbatsov</a> <span class="status">1761860874111779217</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1761859470672453642',
    created: 1708895006000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bbatsov" rel="noopener noreferrer" target="_blank">@bbatsov</a> With that said, I think a lot of adoption is still waiting on a specification and multiple implementations. Fortunately, we\'re making strides. It\'s no small task, but we\'re committed to seeing it happen.<br><br>In reply to: <a href="#1761859210017431820">1761859210017431820</a>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1761859210017431820',
    created: 1708894944000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bbatsov" rel="noopener noreferrer" target="_blank">@bbatsov</a> In my experience, polls never accurately reflect the usage of AsciiDoc. I can assure you I\'m inundated with questions from the community on a daily basis and have learned from those interactions that the adoption is broader than even I expected.<br><br>In reply to: <a href="https://x.com/bbatsov/status/1761818883034423771" rel="noopener noreferrer" target="_blank">@bbatsov</a> <span class="status">1761818883034423771</span>',
    likes: 5,
    retweets: 0
  },
  {
    id: '1761519377541705760',
    created: 1708813921000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BigRedEQ" rel="noopener noreferrer" target="_blank">@BigRedEQ</a> <a class="mention" href="https://x.com/CornellLacrosse" rel="noopener noreferrer" target="_blank">@CornellLacrosse</a> <a class="mention" href="https://x.com/CornellSports" rel="noopener noreferrer" target="_blank">@CornellSports</a> <a class="mention" href="https://x.com/CornellBRSN" rel="noopener noreferrer" target="_blank">@CornellBRSN</a> <a class="mention" href="https://x.com/STXmlax" rel="noopener noreferrer" target="_blank">@STXmlax</a> <a class="mention" href="https://x.com/ESPNIthaca" rel="noopener noreferrer" target="_blank">@ESPNIthaca</a> <a class="mention" href="https://x.com/CornellLaxAssn" rel="noopener noreferrer" target="_blank">@CornellLaxAssn</a> What a game! It didn\'t go our way, but it was something to build on. Loving the potential in Firth. Keep grinding! Spring is coming.<br><br>In reply to: <a href="https://x.com/BigRedEQ/status/1761440722253656342" rel="noopener noreferrer" target="_blank">@BigRedEQ</a> <span class="status">1761440722253656342</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1761501854729449666',
    created: 1708809743000,
    type: 'post',
    text: 'What a game! As advertised. The fans won this one, for sure.<br><br>Quoting: <a href="#1761466175702098262">1761466175702098262</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1761493625693733161',
    created: 1708807781000,
    type: 'post',
    text: 'There\'s our man, CJ Kirst!',
    photos: ['<div class="item"><img class="photo" src="media/1761493625693733161.jpg"></div>'],
    likes: 1,
    retweets: 0
  },
  {
    id: '1761476591463387606',
    created: 1708803720000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mp911de" rel="noopener noreferrer" target="_blank">@mp911de</a> It\'s a beaut!<br><br>In reply to: <a href="https://x.com/mp911de/status/1761467386794737973" rel="noopener noreferrer" target="_blank">@mp911de</a> <span class="status">1761467386794737973</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1761473271453646916',
    created: 1708802929000,
    type: 'reply',
    text: 'Barton stadium is as packed as it is for <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> games. This game is high stakes and fans turned out on both sides.<br><br>In reply to: <a href="#1761466175702098262">1761466175702098262</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1761466175702098262',
    created: 1708801237000,
    type: 'post',
    text: 'Cornell at Denver. Here we go!',
    photos: ['<div class="item"><img class="photo" src="media/1761466175702098262.jpg"></div>'],
    likes: 2,
    retweets: 0
  },
  {
    id: '1761341111790866606',
    created: 1708771419000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/PLLCannons" rel="noopener noreferrer" target="_blank">@PLLCannons</a> To me, the winner was the fans. That was great lacrosse. Both teams deserved the win. I think they should both be proud.<br><br>Didn\'t like the hit on Sowers though. That\'s not going to grow the game. That\'s going to scare people away and make them feel uneasy.<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1759642473914376559" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1759642473914376559</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1761154956248469574',
    created: 1708727036000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> 🎉🍻<br><br>In reply to: <a href="https://x.com/alexsotob/status/1761152464001118558" rel="noopener noreferrer" target="_blank">@alexsotob</a> <span class="status">1761152464001118558</span>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1760960162498510958',
    created: 1708680594000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SanneGrinovero" rel="noopener noreferrer" target="_blank">@SanneGrinovero</a> Same and agreed.<br><br>In reply to: <a href="https://x.com/SanneGrinovero/status/1760958836238700773" rel="noopener noreferrer" target="_blank">@SanneGrinovero</a> <span class="status">1760958836238700773</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1758085513100013597',
    created: 1707995224000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alvaro_sanchez" rel="noopener noreferrer" target="_blank">@alvaro_sanchez</a> <a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> Check again ;)<br><br>In reply to: <a href="https://x.com/alvaro_sanchez/status/1758052343164100935" rel="noopener noreferrer" target="_blank">@alvaro_sanchez</a> <span class="status">1758052343164100935</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1757315715638378935',
    created: 1707811690000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aheritier" rel="noopener noreferrer" target="_blank">@aheritier</a> <a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> <a class="mention" href="https://x.com/snicoll" rel="noopener noreferrer" target="_blank">@snicoll</a> <a class="mention" href="https://x.com/khmarbaise" rel="noopener noreferrer" target="_blank">@khmarbaise</a> The alternate approach would be to build a dist that the other application can run. But that\'s extra steps both in terms of maintenance and build time. The build-classpath seemed suitable, but perhaps it is not.<br><br>In reply to: <a href="#1757315496796356702">1757315496796356702</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1757315496796356702',
    created: 1707811638000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aheritier" rel="noopener noreferrer" target="_blank">@aheritier</a> <a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> <a class="mention" href="https://x.com/snicoll" rel="noopener noreferrer" target="_blank">@snicoll</a> <a class="mention" href="https://x.com/khmarbaise" rel="noopener noreferrer" target="_blank">@khmarbaise</a> I need to identify all jars that have been installed into the Maven repository to build a classpath for another application. Relying on a jar in the target directory is not stable, whereas the local repository is.<br><br>In reply to: <a href="https://x.com/aheritier/status/1757280812754416028" rel="noopener noreferrer" target="_blank">@aheritier</a> <span class="status">1757280812754416028</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1757225857670623397',
    created: 1707790266000,
    type: 'post',
    text: 'When using the build-classpath goal in Maven, is there a way to force it to resolve the artifact from the local repository (the installed artifact) instead of the one from the reactor (the target directory of its project?)',
    likes: 0,
    retweets: 0
  },
  {
    id: '1756872167587799182',
    created: 1707705940000,
    type: 'post',
    text: 'Enjoying the day watching some fantastic lacrosse...and it\'s only February.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1756826511821414713',
    created: 1707695055000,
    type: 'post',
    text: 'NCAA men\'s lacrosse has to increase the length of penalties. 1 min for a blatant illegal body check is just silly. It\'s a slap on the wrist. That would be 5 min in NLL.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1756636895369371699',
    created: 1707649847000,
    type: 'post',
    text: 'On the one hand, I wish I hadn\'t stopped playing lacrosse after school. On the other hand, I\'m glad I rediscovered my love for the game in this new era. It\'s a great time to play and to be a spectator.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1756598757884313925',
    created: 1707640754000,
    type: 'post',
    text: 'The <a class="mention" href="https://x.com/EclipseFdn" rel="noopener noreferrer" target="_blank">@EclipseFdn</a> is removing PHP from project sites by the end of the year in favor of static sites built using CI, rightfully citing security as a concern. Long live static sites! (The <a class="mention" href="https://x.com/JettyProject" rel="noopener noreferrer" target="_blank">@JettyProject</a> is ahead of the curve and is currently migrating to Antora).',
    likes: 28,
    retweets: 5
  },
  {
    id: '1755939119954371048',
    created: 1707483484000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <a class="mention" href="https://x.com/YouTube" rel="noopener noreferrer" target="_blank">@YouTube</a> Have you seen the analysis by The Charismatic Voice? It\'s a great way to appreciate the song and video. <a href="https://youtu.be/2xCqb6bWOiM" rel="noopener noreferrer" target="_blank">youtu.be/2xCqb6bWOiM</a><br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/1755933235085787488" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">1755933235085787488</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1755816643186286715',
    created: 1707454283000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> If you have questions, don\'t hesitate to reach out. I learned Ruby by jumping into a project may years ago (I think you know the one). I\'ve accumulated a lot of knowledge, tips, and tricks along the way. Happy to share.<br><br>In reply to: <a href="https://x.com/settermjd/status/1755767380607729733" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1755767380607729733</span>',
    likes: 3,
    retweets: 1
  },
  {
    id: '1755176113683194032',
    created: 1707301569000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/briandemers" rel="noopener noreferrer" target="_blank">@briandemers</a> I\'m a just emoji kind of guy.<br><br>In reply to: <a href="https://x.com/briandemers/status/1754964728944669059" rel="noopener noreferrer" target="_blank">@briandemers</a> <span class="status">1754964728944669059</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1755142736343187923',
    created: 1707293611000,
    type: 'post',
    text: 'Got to hang out today with a friend from college I haven\'t seen for over a decade. What a day. Friends are special.',
    likes: 9,
    retweets: 0
  },
  {
    id: '1754051647712444739',
    created: 1707033476000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DU_MLAX" rel="noopener noreferrer" target="_blank">@DU_MLAX</a> What a win!<br><br>In reply to: <a href="https://x.com/DU_MLAX/status/1753866984855351580" rel="noopener noreferrer" target="_blank">@DU_MLAX</a> <span class="status">1753866984855351580</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1753710343178555837',
    created: 1706952102000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/spencerbgibb" rel="noopener noreferrer" target="_blank">@spencerbgibb</a> <a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <a class="mention" href="https://x.com/springboot" rel="noopener noreferrer" target="_blank">@springboot</a> I mention that in this thread because Spring gets quite a lot of credit for increasing the exposure of Antora. 😊<br><br>In reply to: <a href="#1753708973495099849">1753708973495099849</a>',
    likes: 3,
    retweets: 1
  },
  {
    id: '1753708973495099849',
    created: 1706951776000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/spencerbgibb" rel="noopener noreferrer" target="_blank">@spencerbgibb</a> <a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <a class="mention" href="https://x.com/springboot" rel="noopener noreferrer" target="_blank">@springboot</a> On top of that, they use Antora to create their site (not just the docs). See <a href="https://github.com/apple/pkl-lang.org/blob/main/src/site-remote.yml" rel="noopener noreferrer" target="_blank">github.com/apple/pkl-lang.org/blob/main/src/site-remote.yml</a><br><br>In reply to: <a href="https://x.com/spencerbgibb/status/1753634432550871303" rel="noopener noreferrer" target="_blank">@spencerbgibb</a> <span class="status">1753634432550871303</span>',
    likes: 12,
    retweets: 2
  },
  {
    id: '1753402773486387681',
    created: 1706878772000,
    type: 'post',
    text: 'The most important rule of lacrosse practice (at least to me). Whatever you do with your right hand, you then need to do with your left hand. It\'s all about being ready for whatever comes your way.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1753332511927566685',
    created: 1706862020000,
    type: 'quote',
    text: 'One of the things I love about Ruby is the ability to set up the state before delegating to super. It makes the super method more reusable. It\'s awesome to see this coming to Java. I\'m sure developers are going to love it.<br><br>Quoting: <a href="https://x.com/java/status/1753078253084893196" rel="noopener noreferrer" target="_blank">@java</a> <span class="status">1753078253084893196</span>',
    likes: 6,
    retweets: 1
  },
  {
    id: '1753258359896023339',
    created: 1706844341000,
    type: 'post',
    text: 'In this case, fire as in lay off. A proper firing would be a different matter, maybe even the right move. But I\'m not talking about that here.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1753257811323105745',
    created: 1706844210000,
    type: 'post',
    text: 'What companies don\'t realize is that when you fire a prominent advocate, we no longer care about your offering. Next!',
    likes: 8,
    retweets: 5
  },
  {
    id: '1753186891946410035',
    created: 1706827302000,
    type: 'post',
    text: 'To be fair, I would be checking my phone too. That\'s why I leave it in the car.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1753009207199084640',
    created: 1706784938000,
    type: 'post',
    text: 'I also consult the docs whenever answering a question. I always want to know if the question came up in the first place because the information was missing from the docs. If so, I make it a priority to add it. (Known as the "just in time" approach).',
    likes: 2,
    retweets: 0
  },
  {
    id: '1752999109286408626',
    created: 1706782531000,
    type: 'post',
    text: 'I consult the docs for my own projects ALL THE TIME. Even I forget how things work, all the features it has, or how to do things. Knowing they help even for software I know best proves to me just how valuable docs are. The docs are always my first stop when looking for answers.',
    likes: 18,
    retweets: 2
  },
  {
    id: '1752994691006505347',
    created: 1706781478000,
    type: 'post',
    text: 'I think people get this confused. I don\'t care if you don\'t use the open source software I create. I\'m focused on those who find it useful and want to participate constructively in some way, which may be helping others, sharing success stories, or contributing to the project.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1752823920850288674',
    created: 1706740763000,
    type: 'post',
    text: 'The PLL is a business and the games are an exhibition, so there\'s no reason it couldn\'t happen...other than a lack of will TBH.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1752823745767415832',
    created: 1706740721000,
    type: 'post',
    text: 'If the women showed up during the PLL championship series and played by the same rules as the men (including equipment), I think that would mark a huge turning point in lacrosse history. (The reality is that it won\'t happen, but I can hope).',
    likes: 0,
    retweets: 0
  },
  {
    id: '1752823245009564059',
    created: 1706740602000,
    type: 'post',
    text: 'When I see players fight in box lacrosse, I see a team that doesn\'t want to win a championship. (It\'s a huge disadvantage to have a person in the penalty box for 5 minutes...plus it\'s just poor sportsmanship).',
    likes: 1,
    retweets: 0
  },
  {
    id: '1752663425061195937',
    created: 1706702498000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/eMalaGupta" rel="noopener noreferrer" target="_blank">@eMalaGupta</a> <a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> So true. Life comes at you fast ;)<br><br>In reply to: <a href="https://x.com/eMalaGupta/status/1752659556134510827" rel="noopener noreferrer" target="_blank">@eMalaGupta</a> <span class="status">1752659556134510827</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1752648484719915142',
    created: 1706698936000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> Happy Birthday buddy. Welcome to life after 40 trips around the sun. My advice, just keep moving! More fun, less pain. 🏃🎂',
    likes: 0,
    retweets: 0
  },
  {
    id: '1752604009859350928',
    created: 1706688332000,
    type: 'post',
    text: '*staring',
    likes: 0,
    retweets: 0
  },
  {
    id: '1752536872503386446',
    created: 1706672325000,
    type: 'post',
    text: 'I may be starring down 46, but when I\'m out at the lacrosse wall, I\'m working harder than the teenagers. They need to follow my lead and leave their phones in the car. When you show up to work, you need to put in the work. No one won a championship lollygagging.',
    likes: 7,
    retweets: 0
  },
  {
    id: '1751410257035690090',
    created: 1706403719000,
    type: 'post',
    text: 'As a consumer, I much prefer sponsors over ads. Ads to me are just noise. Sponsors are memorable (esp when it\'s relevant, like a vendor for a sport or something that\'s somehow connected to the event).',
    likes: 6,
    retweets: 1
  },
  {
    id: '1751192925311021303',
    created: 1706351903000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/noctarius2k" rel="noopener noreferrer" target="_blank">@noctarius2k</a> We didn\'t pay for utilities, which is why we won that round ;)<br><br>In reply to: <a href="https://x.com/noctarius2k/status/1751184210851742134" rel="noopener noreferrer" target="_blank">@noctarius2k</a> <span class="status">1751184210851742134</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1751179679472464250',
    created: 1706348745000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/noctarius2k" rel="noopener noreferrer" target="_blank">@noctarius2k</a> In college, our landlord refused to turn on the heating system in the building during the winter, so this is how we heated our apartment (which was the size of shoe box, so it didn\'t take much).<br><br>In reply to: <a href="https://x.com/noctarius2k/status/1751167142949486874" rel="noopener noreferrer" target="_blank">@noctarius2k</a> <span class="status">1751167142949486874</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1750687139484840251',
    created: 1706231314000,
    type: 'post',
    text: 'The absolute arrogance of some people is just incredible.<br><br><a href="https://github.com/isomorphic-git/isomorphic-git/issues/1855#issuecomment-1911242971" rel="noopener noreferrer" target="_blank">github.com/isomorphic-git/isomorphic-git/issues/1855#issuecomment-1911242971</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1750491614823461363',
    created: 1706184698000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/apr" rel="noopener noreferrer" target="_blank">@apr</a> <a class="mention" href="https://x.com/dflc" rel="noopener noreferrer" target="_blank">@dflc</a> <a class="mention" href="https://x.com/bjornekelund" rel="noopener noreferrer" target="_blank">@bjornekelund</a> It\'s not the theory that\'s bad. It does have application. But it\'s just plain fragile and slow. I\'ve spent so many minutes of my life waiting on negotiation (if it even connects), and it frequently kills the mood or vibe.<br><br>In reply to: <a href="https://x.com/apr/status/1750484285771939895" rel="noopener noreferrer" target="_blank">@apr</a> <span class="status">1750484285771939895</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1750478014926745679',
    created: 1706181455000,
    type: 'post',
    text: 'Why do we accept Bluetooth as state of the art?',
    likes: 3,
    retweets: 0
  },
  {
    id: '1750476178224263449',
    created: 1706181017000,
    type: 'post',
    text: 'AsciiDoc writers, your voices have been heard!<br><br><a href="https://youtrack.jetbrains.com/issue/WRS-1779#focus=Comments-27-8895943.0-0" rel="noopener noreferrer" target="_blank">youtrack.jetbrains.com/issue/WRS-1779#focus=Comments-27-8895943.0-0</a><br><br>🤞',
    likes: 35,
    retweets: 7
  },
  {
    id: '1750435420406522276',
    created: 1706171300000,
    type: 'quote',
    text: '👇<br><br>Quoting: <a href="https://x.com/birthmarkbart/status/1750103650398265473" rel="noopener noreferrer" target="_blank">@birthmarkbart</a> <span class="status">1750103650398265473</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1750431470357794910',
    created: 1706170358000,
    type: 'post',
    text: 'Oh, and add the -I (as in ignore) flag to effectively skip over binary files without a warning.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1750330859847082019',
    created: 1706146371000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/untappd" rel="noopener noreferrer" target="_blank">@untappd</a> Shop says it can\'t deliver to my zip code. Got a coverage map we follow for updates?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1749972751220101571',
    created: 1706060991000,
    type: 'post',
    text: 'I\'m gradually learning that the best path towards sustainability in open source is to be extremely tough (just like your favorite coaches/teachers). Fair, yes. But also very tough. Set hard boundaries. Ask for help. Ask for support. Ask to be hired (if appropriate). Take no shit.',
    likes: 8,
    retweets: 0
  },
  {
    id: '1749738732746404114',
    created: 1706005197000,
    type: 'post',
    text: 'Update: My reaction seemed to have changed the maintainer\'s mind (or attitude). The test case did end up helping the maintainer understand how to resolve the issue. All\'s well that ends well, I suppose.<br><br>Still, please respect people\'s time if you ask them to do something for you.',
    likes: 6,
    retweets: 0
  },
  {
    id: '1749504244019786138',
    created: 1705949290000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> Exactly. It\'s not that a contribution wasn\'t accepted. That happens often. It\'s that the maintainer asked me to spend time to write a test case, then proceeded to disregard it without even trying to understand what I provided. The maintainer clearly wanted to waste my time.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1749430287363080410" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1749430287363080410</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1749411915082711129',
    created: 1705927277000,
    type: 'post',
    text: 'A maintainer asked me to create a test case for a bug in an open source project. I spent two hours putting together a simple test in that suite that specifically demonstrates the problem, and the person closed it with "must be your code that\'s wrong". Damn that makes me angry.',
    likes: 10,
    retweets: 0
  },
  {
    id: '1749259065824297381',
    created: 1705890835000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLCannons" rel="noopener noreferrer" target="_blank">@PLLCannons</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/ChrisAz03" rel="noopener noreferrer" target="_blank">@ChrisAz03</a> Good.<br><br>In reply to: <a href="https://x.com/PLLCannons/status/1747695647762116980" rel="noopener noreferrer" target="_blank">@PLLCannons</a> <span class="status">1747695647762116980</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1748493152841798130',
    created: 1705708227000,
    type: 'post',
    text: 'HTML needs to define an admonition tag. Admonitions are defined in DocBook, DITA, reStructuredText, AsciiDoc, GFM, and a plethora of UI and CSS frameworks. Yet, we\'re stuck with a bunch of nested divs or quibbling over whether this is what &lt;aside&gt; was designed for. Come on HTML.',
    likes: 6,
    retweets: 0
  },
  {
    id: '1748438970797613200',
    created: 1705695309000,
    type: 'post',
    text: 'If anyone is in this situation, I found that switching to the la capitaine cursor theme (rpm: la-capitaine-cursor-theme) solves the sizing issue. It must provide the necessary size when the display is scaled to 300%.<br><br>Quoting: <a href="#1747181698284998905">1747181698284998905</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1748291116212896105',
    created: 1705660058000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/simonbrown" rel="noopener noreferrer" target="_blank">@simonbrown</a> Now it makes sense ;) 💯<br><br>In reply to: <a href="https://x.com/simonbrown/status/1748288467790889449" rel="noopener noreferrer" target="_blank">@simonbrown</a> <span class="status">1748288467790889449</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1748290937250349301',
    created: 1705660015000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/simonbrown" rel="noopener noreferrer" target="_blank">@simonbrown</a> <a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> 😕<br><br>In reply to: <a href="https://x.com/simonbrown/status/1748289090800222413" rel="noopener noreferrer" target="_blank">@simonbrown</a> <span class="status">1748289090800222413</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1748286339210965152',
    created: 1705658919000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/simonbrown" rel="noopener noreferrer" target="_blank">@simonbrown</a> He\'s saying that the statement is nonsense. In fact, we should be recommending diagram tools (but done in the right way, of course).<br><br>In reply to: <a href="https://x.com/marcsavy/status/1748284522117120480" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1748284522117120480</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1748127156679352334',
    created: 1705620967000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/max_summe" rel="noopener noreferrer" target="_blank">@max_summe</a> Indeed, the one standout...which also frightens me because who\'s to say it will stay?<br><br>In reply to: <a href="https://x.com/max_summe/status/1748094137407311887" rel="noopener noreferrer" target="_blank">@max_summe</a> <span class="status">1748094137407311887</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1748089639066284096',
    created: 1705612022000,
    type: 'post',
    text: 'Whoa! I just noticed that GitHub provides a tabbed interface on the repository home page to view the text of the README, CoC, License, etc, without having to navigate to separate pages. See <a href="https://github.com/spring-projects/spring-security" rel="noopener noreferrer" target="_blank">github.com/spring-projects/spring-security</a> for an example.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1748081214240493963',
    created: 1705610013000,
    type: 'post',
    text: '"Google &lt;something&gt; is going away."<br><br>How many times have we received that message? At this point, just assume any service is ephemeral.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1747791696975597804',
    created: 1705540987000,
    type: 'post',
    text: 'A quick tip about using grep, since I always forgot it. If you want to suppress the "Is a directory" or "Permission denied" warning messages, add the -s flag.',
    likes: 6,
    retweets: 1
  },
  {
    id: '1747758781973348401',
    created: 1705533140000,
    type: 'post',
    text: 'People who disregard the issue template, which requests that all questions be asked in the chat, and go ahead and create an issue anyway, upset me.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1747742714907549730',
    created: 1705529309000,
    type: 'post',
    text: 'If you are setting up a .gitattributes file, use the following command to check which attributes git applies to any given file:<br><br>git check-attr --all name-of-file<br><br>Then check how it actually handles the file after the auto detection is applied<br><br>git ls-files --eol name-of-file',
    likes: 7,
    retweets: 1
  },
  {
    id: '1747358648710410598',
    created: 1705437740000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CornellAlumni" rel="noopener noreferrer" target="_blank">@CornellAlumni</a> Awesome!<br><br>In reply to: <a href="https://x.com/CornellAlumni/status/1744768530178793737" rel="noopener noreferrer" target="_blank">@CornellAlumni</a> <span class="status">1744768530178793737</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1747181698284998905',
    created: 1705395552000,
    type: 'post',
    text: 'It\'s frustrating to see that the huge, clown shoe sized cursor in GTK apps is still present in Fedora 39 when the desktop is scaled to 300% (though not Fedora\'s fault). 🤡 I don\'t understand how this stuff gets past QA.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1746740876460630353',
    created: 1705290452000,
    type: 'post',
    text: 'This weather is the pits. I\'m getting cabin fever. 🥶',
    likes: 4,
    retweets: 0
  },
  {
    id: '1746302165692387672',
    created: 1705185855000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/chacon" rel="noopener noreferrer" target="_blank">@chacon</a> I agree.<br><br>In reply to: <a href="https://x.com/chacon/status/1746284162875539947" rel="noopener noreferrer" target="_blank">@chacon</a> <span class="status">1746284162875539947</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1745728305221738995',
    created: 1705049036000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/Ticketmaster" rel="noopener noreferrer" target="_blank">@Ticketmaster</a> <a class="mention" href="https://x.com/unleashedwlax" rel="noopener noreferrer" target="_blank">@unleashedwlax</a> And, for the love of the creator, don\'t make them wear skirts.<br><br>In reply to: <a href="#1744506586939220320">1744506586939220320</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1745626288654602271',
    created: 1705024714000,
    type: 'post',
    text: 'The marketing strategy for 2024 seems to be "10x the emails!" Stop with it! I\'m unsubscribing from everything. I\'m tired of it.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1745624230002106487',
    created: 1705024223000,
    type: 'post',
    text: 'Nothing works anymore.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1744506586939220320',
    created: 1704757756000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/Ticketmaster" rel="noopener noreferrer" target="_blank">@Ticketmaster</a> <a class="mention" href="https://x.com/unleashedwlax" rel="noopener noreferrer" target="_blank">@unleashedwlax</a> My only wish is that they play by the same exact rules as the men and not some watered down version with no pads and those pointless, fiddly face masks. Let the women play on equal footing (like in box). There\'s no reason this can\'t be done as this is 100% exhibition.<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1744403811588886569" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1744403811588886569</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1744029696218578991',
    created: 1704644056000,
    type: 'post',
    text: 'How are your New Year\'s resolutions coming along?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1743442985818231011',
    created: 1704504174000,
    type: 'post',
    text: 'Repository owners are *finally* understanding that OTP doesn\'t work for automated releases (the only way you should release) and is now offering a viable alternative that is. <a href="https://blog.rubygems.org/2023/12/14/trusted-publishing.html" rel="noopener noreferrer" target="_blank">blog.rubygems.org/2023/12/14/trusted-publishing.html</a><br><br>(now we just need this for npm!)',
    likes: 4,
    retweets: 0
  },
  {
    id: '1743436715698667648',
    created: 1704502679000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CornellSports" rel="noopener noreferrer" target="_blank">@CornellSports</a> <a class="mention" href="https://x.com/CornellMHockey" rel="noopener noreferrer" target="_blank">@CornellMHockey</a> <a class="mention" href="https://x.com/CornellWHockey" rel="noopener noreferrer" target="_blank">@CornellWHockey</a> That looks really nice. I always thought that what was behind those doors was cloaked by the drab exterior. Now it provides a good sense of the magic that lies ahead! A proper welcome for the Lynah faithful!<br><br>In reply to: <a href="https://x.com/CornellSports/status/1737970855710253239" rel="noopener noreferrer" target="_blank">@CornellSports</a> <span class="status">1737970855710253239</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1743247101889249585',
    created: 1704457471000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> Amen to your rant against the endless news drip from the PLL. I feel it too.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1743085259011346938',
    created: 1704418885000,
    type: 'quote',
    text: 'I\'m be honest, I don\'t know why this isn\'t the default. It\'s the default in GitLab CI.<br><br>Quoting: <a href="https://x.com/stauffermatt/status/1743051089501470788" rel="noopener noreferrer" target="_blank">@stauffermatt</a> <span class="status">1743051089501470788</span>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1743059064454619513',
    created: 1704412640000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> <a class="mention" href="https://x.com/bsideup" rel="noopener noreferrer" target="_blank">@bsideup</a> <a class="mention" href="https://x.com/KamenevaMila" rel="noopener noreferrer" target="_blank">@KamenevaMila</a> <a class="mention" href="https://x.com/venkat_s" rel="noopener noreferrer" target="_blank">@venkat_s</a> ...here\'s the one I use <a href="https://www.seaandsummitsunscreen.com/blogs/news/sea-summit-the-portable-sunscreen-facial-stick" rel="noopener noreferrer" target="_blank">www.seaandsummitsunscreen.com/blogs/news/sea-summit-the-portable-sunscreen-facial-stick</a><br><br>In reply to: <a href="#1743034449816678597">1743034449816678597</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1743034449816678597',
    created: 1704406771000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> <a class="mention" href="https://x.com/bsideup" rel="noopener noreferrer" target="_blank">@bsideup</a> <a class="mention" href="https://x.com/KamenevaMila" rel="noopener noreferrer" target="_blank">@KamenevaMila</a> <a class="mention" href="https://x.com/venkat_s" rel="noopener noreferrer" target="_blank">@venkat_s</a> And please wear sunscreen if you are outside for an extended period of time. You won\'t regret it. I like a good face stick since it\'s easy to apply.<br><br>In reply to: <a href="https://x.com/mraible/status/1743021899683139831" rel="noopener noreferrer" target="_blank">@mraible</a> <span class="status">1743021899683139831</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1743019059984134450',
    created: 1704403102000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bsideup" rel="noopener noreferrer" target="_blank">@bsideup</a> <a class="mention" href="https://x.com/KamenevaMila" rel="noopener noreferrer" target="_blank">@KamenevaMila</a> Oh yeah! We\'ll have to get together sometime. I\'m in the south side of Denver.<br><br>In reply to: <a href="https://x.com/bsideup/status/1742991810811642034" rel="noopener noreferrer" target="_blank">@bsideup</a> <span class="status">1742991810811642034</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1742535383982985495',
    created: 1704287784000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/NLL" rel="noopener noreferrer" target="_blank">@NLL</a> That was one of the most smokin\' shots I\'ve ever seen in box. That ball was humming!<br><br>In reply to: <a href="https://x.com/NLL/status/1740918060192592054" rel="noopener noreferrer" target="_blank">@NLL</a> <span class="status">1740918060192592054</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1742277377596543115',
    created: 1704226271000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_Unsolved_" rel="noopener noreferrer" target="_blank">@_Unsolved_</a> For me, line and block comments have always been sufficient. If they\'re any more intertwined with the text, I find it difficult to follow. Keep it simple is how I like to think of it. But perhaps there is a better way after all.<br><br>In reply to: <a href="https://x.com/_Unsolved_/status/1742176862309146677" rel="noopener noreferrer" target="_blank">@_Unsolved_</a> <span class="status">1742176862309146677</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1742142001804406893',
    created: 1704193995000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mp911de" rel="noopener noreferrer" target="_blank">@mp911de</a> I refuse to play this game. I just use 2012-present so I can focus on the important work.<br><br>In reply to: <a href="https://x.com/mp911de/status/1742087202429890838" rel="noopener noreferrer" target="_blank">@mp911de</a> <span class="status">1742087202429890838</span>',
    likes: 2,
    retweets: 0
  }
])
