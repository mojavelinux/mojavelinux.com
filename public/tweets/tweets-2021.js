'use strict'

inflateTweets([
  {
    id: '1477131472213397505',
    created: 1641010561000,
    type: 'post',
    text: 'Listening to some hardstyle and French core on New Year\'s Eve because, honestly, I don\'t know what else will prepare me for the year to come.',
    photos: ['<div class="item"><img class="photo" src="media/1477131472213397505.gif"></div>'],
    likes: 1,
    retweets: 0
  },
  {
    id: '1477099692764762115',
    created: 1641002984000,
    type: 'post',
    text: 'It wasn\'t a snowy Xmas, but it is a snowy New Year\'s eve. And that\'s great news for Coloradans in the wake of yesterday\'s fires. My heart goes out to those who were affected.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1477067190432002053',
    created: 1640995235000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ChrisGPackham" rel="noopener noreferrer" target="_blank">@ChrisGPackham</a> I love your work. I love your message. Keep being you. We need more strong, principled people like you in this world. 🐦<br><br>In reply to: <a href="https://x.com/ChrisGPackham/status/1477048669346189316" rel="noopener noreferrer" target="_blank">@ChrisGPackham</a> <span class="status">1477048669346189316</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1477066437906690051',
    created: 1640995056000,
    type: 'post',
    text: 'Happy New Year to my friends around the world! 🎉🥂<br><br>I have a New Year\'s resolution for the world. End fossil fuel extraction. These natural disasters aren\'t going to subside until we do. You can\'t negotiate with science.',
    likes: 12,
    retweets: 1
  },
  {
    id: '1476175299930775552',
    created: 1640782592000,
    type: 'post',
    text: 'Ever since the Fedora docs were moved to Antora, I\'m finding it a lot easier to locate relevant pages using a search engine. The pages are just much better indexed. Kudos to those restructuring the content too.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1476169726187474955',
    created: 1640781263000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> <a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> Sarah convinced me to get an iFix kit. Such a great investment. If something needs to opened, closed, or tightened, it\'s got the goods.<br><br>In reply to: <a href="https://x.com/majson/status/1476092790119542785" rel="noopener noreferrer" target="_blank">@majson</a> <span class="status">1476092790119542785</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1476169232534716421',
    created: 1640781145000,
    type: 'post',
    text: 'I just upgraded from Fedora 33 to Fedora 35. That was simplest and smoothest Linux upgrade I\'ve ever done. The <a class="mention" href="https://x.com/fedora" rel="noopener noreferrer" target="_blank">@fedora</a> maintainers and contributors are crushing it.',
    likes: 71,
    retweets: 9
  },
  {
    id: '1475398165515829248',
    created: 1640597308000,
    type: 'post',
    text: 'Anyone who tells you that not using dependencies is hubris is, in fact, demonstrating hubris. Ignore those people. You must trust your dependencies like you would trust your own project members. Choose wisely.',
    likes: 6,
    retweets: 2
  },
  {
    id: '1474938742196477952',
    created: 1640487773000,
    type: 'post',
    text: '"As we begin 2020, part 3...". 😱',
    likes: 7,
    retweets: 0
  },
  {
    id: '1474843903576580097',
    created: 1640465162000,
    type: 'quote',
    text: 'I can attest that the performance gains of the --yjit command-line option are real. Try for yourself.<br><br>Quoting: <a href="https://x.com/rubylangorg/status/1474736153978982400" rel="noopener noreferrer" target="_blank">@rubylangorg</a> <span class="status">1474736153978982400</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1474620751776944133',
    created: 1640411959000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/codepitbull" rel="noopener noreferrer" target="_blank">@codepitbull</a> We\'re having a seitan roast as well!<br><br>In reply to: <a href="https://x.com/codepitbull/status/1474465813684006912" rel="noopener noreferrer" target="_blank">@codepitbull</a> <span class="status">1474465813684006912</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1474615220219744259',
    created: 1640410640000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cosmicgate" rel="noopener noreferrer" target="_blank">@cosmicgate</a> That was us all year. Keep the tunes coming for sanity!<br><br>In reply to: <a href="https://x.com/cosmicgate/status/1474342087483006979" rel="noopener noreferrer" target="_blank">@cosmicgate</a> <span class="status">1474342087483006979</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1474597470185275393',
    created: 1640406408000,
    type: 'post',
    text: 'This performance by I Hate Models and FEMUR is absolutely insane. A total audio / visual journey. <a href="https://youtu.be/Kz9uoNiQJiI" rel="noopener noreferrer" target="_blank">youtu.be/Kz9uoNiQJiI</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1474492918404632576',
    created: 1640381481000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/g_scheibel" rel="noopener noreferrer" target="_blank">@g_scheibel</a> <a class="mention" href="https://x.com/Wakaze_Sake" rel="noopener noreferrer" target="_blank">@Wakaze_Sake</a> <a class="mention" href="https://x.com/Gastronogeek" rel="noopener noreferrer" target="_blank">@Gastronogeek</a> <a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> 👆<br><br>In reply to: <a href="https://x.com/g_scheibel/status/1474476528129957889" rel="noopener noreferrer" target="_blank">@g_scheibel</a> <span class="status">1474476528129957889</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1474492491147710466',
    created: 1640381379000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ChrisGSeaton" rel="noopener noreferrer" target="_blank">@ChrisGSeaton</a> <a class="mention" href="https://x.com/LashaKrikheli" rel="noopener noreferrer" target="_blank">@LashaKrikheli</a> It\'s gotten me through this pandemic. No doubt about it.<br><br>In reply to: <a href="https://x.com/ChrisGSeaton/status/1474485769553301515" rel="noopener noreferrer" target="_blank">@ChrisGSeaton</a> <span class="status">1474485769553301515</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1474151477274836993',
    created: 1640300075000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/webdevwithmatt" rel="noopener noreferrer" target="_blank">@webdevwithmatt</a> Tell me about it ;)<br><br>In reply to: <a href="https://x.com/webdevwithmatt/status/1474143931457802243" rel="noopener noreferrer" target="_blank">@webdevwithmatt</a> <span class="status">1474143931457802243</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1474135340600344585',
    created: 1640296227000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> This is absolutely my policy now.<br><br>In reply to: <a href="https://x.com/gunnarmorling/status/1474053962034782212" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <span class="status">1474053962034782212</span>',
    likes: 6,
    retweets: 0
  },
  {
    id: '1474116628874883086',
    created: 1640291766000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/webdevwithmatt" rel="noopener noreferrer" target="_blank">@webdevwithmatt</a> At this point, it would be better to wait another year when there will be (hopefully) an official spec to go on.<br><br>In reply to: <a href="https://x.com/webdevwithmatt/status/1474109148140822537" rel="noopener noreferrer" target="_blank">@webdevwithmatt</a> <span class="status">1474109148140822537</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1473954930448764929',
    created: 1640253214000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/j2r2b" rel="noopener noreferrer" target="_blank">@j2r2b</a> I just had that situation come up this week. After brainstorming and deliberating for several hours, we finally decided it was simply too difficult to explain and the code had to be changed (which was really the right thing to do regardless).<br><br>In reply to: <a href="https://x.com/j2r2b/status/1473952988733612033" rel="noopener noreferrer" target="_blank">@j2r2b</a> <span class="status">1473952988733612033</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1473811916707418117',
    created: 1640219117000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/TylerPentecost" rel="noopener noreferrer" target="_blank">@TylerPentecost</a> I\'ve got receipts ;)<br><br>In reply to: <a href="https://x.com/TylerPentecost/status/1473778871556349955" rel="noopener noreferrer" target="_blank">@TylerPentecost</a> <span class="status">1473778871556349955</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1473774809095933953',
    created: 1640210270000,
    type: 'post',
    text: 'When you write documentation, you find bugs. That\'s just a fact.',
    likes: 110,
    retweets: 21
  },
  {
    id: '1473139621441662976',
    created: 1640058830000,
    type: 'post',
    text: 'I\'ve never wrestled with a pig. But I have wrestled with CI. So I pretty much know what it would be like.',
    likes: 9,
    retweets: 2
  },
  {
    id: '1472682049232269315',
    created: 1639949736000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> And working on it has made me realize how much more we can still do with it. It also paves the way for integrating other search providers.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1472675053288439813" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1472675053288439813</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1472005405018660866',
    created: 1639788411000,
    type: 'quote',
    text: '100% this.<br><br>Quoting: <a href="https://x.com/michlbrmly/status/1169571193550192641" rel="noopener noreferrer" target="_blank">@michlbrmly</a> <span class="status">1169571193550192641</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1471997835050504193',
    created: 1639786606000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/michaelminella" rel="noopener noreferrer" target="_blank">@michaelminella</a> Same!<br><br>In reply to: <a href="https://x.com/michaelminella/status/1446551157250265088" rel="noopener noreferrer" target="_blank">@michaelminella</a> <span class="status">1446551157250265088</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1471957642574581760',
    created: 1639777024000,
    type: 'post',
    text: 'It\'s not that I don\'t like someone exploring creative ideas. That\'s great. Do it. The problem is, they\'re dragging us all through their explorations and it\'s making us feel totally insane.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1471957241443979266',
    created: 1639776928000,
    type: 'post',
    text: 'When I\'m interviewing designers for a future contract, I\'m just going to say "here\'s what I need, and if you do anything like what Dropbox is doing, we\'re through." That brand identity has become a burning hot mess.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1471954844290224132',
    created: 1639776357000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agentdero" rel="noopener noreferrer" target="_blank">@agentdero</a> 💯<br><br>In reply to: <a href="https://x.com/agentdero/status/1471953936001089537" rel="noopener noreferrer" target="_blank">@agentdero</a> <span class="status">1471953936001089537</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1471945463288848385',
    created: 1639774120000,
    type: 'post',
    text: 'It was nice to catch up with an old friend today.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1471215301559685122',
    created: 1639600036000,
    type: 'post',
    text: 'For any project I set down to focus on other projects, I will eventually get attacked for making that decision. I can live with that. What I can\'t live with is people telling me that won\'t happen. It will happen.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1470832124852793345',
    created: 1639508679000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CedricChampeau" rel="noopener noreferrer" target="_blank">@CedricChampeau</a> <a class="mention" href="https://x.com/smaldini" rel="noopener noreferrer" target="_blank">@smaldini</a> 💔😢<br><br>In reply to: <a href="https://x.com/CedricChampeau/status/1470701069072314369" rel="noopener noreferrer" target="_blank">@CedricChampeau</a> <span class="status">1470701069072314369</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1470371369284079618',
    created: 1639398827000,
    type: 'post',
    text: 'I have had a brutal couple of months of work.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1470184590014771201',
    created: 1639354295000,
    type: 'post',
    text: 'I\'m quite liking the Firefox Colorways. I settled on Graffiti Bold.',
    photos: ['<div class="item"><img class="photo" src="media/1470184590014771201.jpg"></div>'],
    likes: 1,
    retweets: 0
  },
  {
    id: '1470148079609208835',
    created: 1639345590000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/darkjabberwock" rel="noopener noreferrer" target="_blank">@darkjabberwock</a> Thanks! That\'s perfect. We\'re off to the races now.<br><br>In reply to: <a href="https://x.com/darkjabberwock/status/1470136843727429634" rel="noopener noreferrer" target="_blank">@darkjabberwock</a> <span class="status">1470136843727429634</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1470135560408756225',
    created: 1639342606000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/darkjabberwock" rel="noopener noreferrer" target="_blank">@darkjabberwock</a> At your earliest convenience, could you chime in here? <a href="https://gitlab.com/antora/antora-lunr-extension/-/merge_requests/5" rel="noopener noreferrer" target="_blank">gitlab.com/antora/antora-lunr-extension/-/merge_requests/5</a> We need your sign-off to move forward with a release of the new Antora Lunr extension under MPL-2.0.',
    likes: 1,
    retweets: 1
  },
  {
    id: '1469650300935958531',
    created: 1639226911000,
    type: 'post',
    text: 'PSA: npm 7 and 8 no longer run lifecycle scripts defined in node_modules/.hooks/',
    likes: 1,
    retweets: 2
  },
  {
    id: '1469649308978794498',
    created: 1639226674000,
    type: 'post',
    text: 'Setting up a release process never takes a short amount of time. (But it does always pay off).',
    likes: 11,
    retweets: 0
  },
  {
    id: '1468369862112079872',
    created: 1638921630000,
    type: 'post',
    text: 'Whenever I\'m working the most is also when I feel like I\'m failing the most.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1468121133245353987',
    created: 1638862329000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/seancarroll_" rel="noopener noreferrer" target="_blank">@seancarroll_</a> Thanks! To answer your question, they use custom CSS to rearrange the section blocks into a tile pattern. Antora allows you to customize the UI in any way imaginable and they\'ve made heavy use of it.<br><br>In reply to: <a href="https://x.com/seancarroll_/status/1468064208487075843" rel="noopener noreferrer" target="_blank">@seancarroll_</a> <span class="status">1468064208487075843</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1467685923575910403',
    created: 1638758567000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> Absolutely. gulp is now gone. I\'ll keep chipping away at the rest until I get there.<br><br>In reply to: <a href="https://x.com/brunoborges/status/1467328104129445888" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1467328104129445888</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1467685736543444994',
    created: 1638758522000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pvaneeckhoudt" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> Third party dependencies are most definitely a liability. I Hard earned experience has taught me that again and again. I\'m not opposed to dependencies, but they are like partners. You have to trust them. The JS mini module mindset was a huge mistake.<br><br>In reply to: <a href="https://x.com/pvaneeckhoudt/status/1467428577461350402" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> <span class="status">1467428577461350402</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1467363962459066369',
    created: 1638681805000,
    type: 'post',
    text: 'I raised a concern about a security vulnerability that was being ignored. The response was to mark my comment as spam. Just think about that. I don\'t deserve that. No one does. It was unquestionablely an unprofessional response. I\'m extremely upset about it.',
    likes: 9,
    retweets: 2
  },
  {
    id: '1467348667208278023',
    created: 1638678158000,
    type: 'post',
    text: 'Common decency is just to much to ask for anymore.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1467328954587484160',
    created: 1638673459000,
    type: 'post',
    text: 'I\'m just so tired of the disrespect. Society is so broken right now it makes me nauseous.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1467326771662000129',
    created: 1638672938000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> It won\'t be trivial. But I think it\'s the best course of action at this point.<br><br>In reply to: <a href="https://x.com/brunoborges/status/1467325352691130368" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1467325352691130368</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1467323593881108480',
    created: 1638672180000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> At this point, it would be easier just to use my own code.<br><br>In reply to: <a href="https://x.com/brunoborges/status/1467323457297784837" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1467323457297784837</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1467322351184334850',
    created: 1638671884000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/BlaineBublitz" rel="noopener noreferrer" target="_blank">@BlaineBublitz</a> I *really* don\'t appreciate you marking my offer to help fix a security vulnerability issue in vinyl-fs by pushing me off, marking my comment as spam, and locking the issue. Grow up.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1467321663071031298',
    created: 1638671720000,
    type: 'post',
    text: 'I\'m done with vinyl-fs. This is a shit attitude. <a href="https://github.com/gulpjs/vinyl-fs/issues/325#issuecomment-986154128" rel="noopener noreferrer" target="_blank">github.com/gulpjs/vinyl-fs/issues/325#issuecomment-986154128</a>',
    likes: 7,
    retweets: 1
  },
  {
    id: '1465862906566770691',
    created: 1638323925000,
    type: 'post',
    text: 'When selecting health insurance options in the US, the last question might as well be:<br><br>Now do you want to pay us in gold, silver, or bronze?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1465763128155664385',
    created: 1638300136000,
    type: 'post',
    text: 'I really wish Node.js shipped with a logger. In the meantime, pino is awesome.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1465498999947890692',
    created: 1638237163000,
    type: 'post',
    text: 'I discovered that the setting is only accessible via the mobile app. That\'s just nasty. It\'s a bug, not a feature.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1465494564664320005',
    created: 1638236106000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/twitter" rel="noopener noreferrer" target="_blank">@twitter</a> stop spamming my phone about live chats by people I don\'t even know!!! WTF? Just no!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1464064993314279427',
    created: 1637895270000,
    type: 'post',
    text: 'If it\'s love that your feeling, take your arm and lift it up. Let the music heal your soul. 🎶',
    likes: 0,
    retweets: 0
  },
  {
    id: '1463968755369848834',
    created: 1637872325000,
    type: 'reply',
    text: '@jbryant787 Thanks Jay! I hope your day is full of delight as well. 🍻<br><br>In reply to: @jbryant787 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1463648956416598017',
    created: 1637796079000,
    type: 'post',
    text: 'I\'ve been working my tailbone off. Looking forward to a nice feast tomorrow to replenish the reservoir.',
    likes: 5,
    retweets: 0
  },
  {
    id: '1462212601660653569',
    created: 1637453625000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> <a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/rob_winch" rel="noopener noreferrer" target="_blank">@rob_winch</a> Love you too! And we\'re going to keep working to realize its full potential. So much still to explore.<br><br>In reply to: <a href="https://x.com/starbuxman/status/1462211332778774529" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">1462211332778774529</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1461269506228047872',
    created: 1637228774000,
    type: 'quote',
    text: 'For what it\'s worth, I\'m with Emmanuel on this.<br><br>Quoting: <a href="https://x.com/emmanuelbernard/status/1460262079500279811" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">1460262079500279811</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1461101033665572865',
    created: 1637188607000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> I second both of your statements.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1461014971702255627" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1461014971702255627</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1460223323241996293',
    created: 1636979344000,
    type: 'post',
    text: 'I honestly don\'t know if I like programming publishing tools in Node.js or Ruby better. I\'m just grateful to have both.',
    likes: 5,
    retweets: 0
  },
  {
    id: '1458711669551472642',
    created: 1636618938000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/benignbala" rel="noopener noreferrer" target="_blank">@benignbala</a> Skip to minute 59:12 of this video. It\'s a fantastic message with a great delivery. It\'s the kind of message I think has the potential to change hearts and minds rapidly. <a href="https://youtu.be/YB6BkD1QX24?t=3552" rel="noopener noreferrer" target="_blank">youtu.be/YB6BkD1QX24?t=3552</a><br><br>In reply to: <a href="https://x.com/benignbala/status/1458704967213064192" rel="noopener noreferrer" target="_blank">@benignbala</a> <span class="status">1458704967213064192</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1458706385810509825',
    created: 1636617678000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/benignbala" rel="noopener noreferrer" target="_blank">@benignbala</a> We\'ve always known we\'d be up against corrupt governments. What worries me is the second part. I thought individuals, who represent their own interests, would be capable of comprehending the problem we all face.<br><br>In reply to: <a href="https://x.com/benignbala/status/1458704967213064192" rel="noopener noreferrer" target="_blank">@benignbala</a> <span class="status">1458704967213064192</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1458703970545799170',
    created: 1636617102000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dashorst" rel="noopener noreferrer" target="_blank">@dashorst</a> Legislators are individuals too. And they only act when the vast majority of the people act and/or speak up. That\'s how we create substantive change. Politicians will not save us. The change will come from the bottom up. It always has. It always will.<br><br>In reply to: <a href="https://x.com/dashorst/status/1458702436818903040" rel="noopener noreferrer" target="_blank">@dashorst</a> <span class="status">1458702436818903040</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1458703134566408193',
    created: 1636616903000,
    type: 'post',
    text: 'It\'s true that nature is a system of give and take. We humans have taken more than we give for far too long. Now we need to give to nature more than we take. It\'s that simple.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1458702723398864900',
    created: 1636616805000,
    type: 'post',
    text: 'I also think we need to shift towards preserving all life, not just human life. No more killing of animals or destroying animal and plant habitats. It\'s not just a moral issue. It threatens the very balance of nature on which we rely. We have to stop this death march.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1458701540512841735',
    created: 1636616523000,
    type: 'post',
    text: 'There\'s no single solution, but there\'s at least one thing we know must happen. That is, we must end fossil fuel extraction as rapidly as possible, being ready to leave our comfort zone in the short term to get there.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1458700336730804226',
    created: 1636616236000,
    type: 'post',
    text: 'COVID-19 became a crisis and, by in large, the world sprung into action to solve it and save lives. But I still don\'t think people are getting just how catastrophic the collapse of the natural world will be, and to an extent, is already. COVID-19 is a sniffle in comparison.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1458697698719059971',
    created: 1636615607000,
    type: 'quote',
    text: 'THIS is how we have to be talking about this crisis. Not by tinkering and dancing around the edges. But rather by addressing it HEAD on. Bravo Chris.<br><br>Quoting: <a href="https://x.com/itvpeston/status/1458579449579032585" rel="noopener noreferrer" target="_blank">@itvpeston</a> <span class="status">1458579449579032585</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1458696574737940482',
    created: 1636615339000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ChrisGPackham" rel="noopener noreferrer" target="_blank">@ChrisGPackham</a> <a class="mention" href="https://x.com/MeganMcCubbin" rel="noopener noreferrer" target="_blank">@MeganMcCubbin</a> That talk was truly excellent. The message was clear. It was emphatic. It was relatable. It was kind (to humans and nature alike). Top work!<br><br>In reply to: <a href="https://x.com/ChrisGPackham/status/1458396681851097088" rel="noopener noreferrer" target="_blank">@ChrisGPackham</a> <span class="status">1458396681851097088</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1458394044699070473',
    created: 1636543210000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> <a class="mention" href="https://x.com/docToolchain" rel="noopener noreferrer" target="_blank">@docToolchain</a> <a class="mention" href="https://x.com/j2r2b" rel="noopener noreferrer" target="_blank">@j2r2b</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> Our goal with Antora is to make the very best technical documentation sites we can make. We also want to advocate for a standard structure for documentation content. That\'s the path we\'ve been on and will continue to be one for years to come. And it\'s no small task.<br><br>In reply to: <a href="#1458393528271196160">1458393528271196160</a>',
    likes: 10,
    retweets: 3
  },
  {
    id: '1458393528271196160',
    created: 1636543087000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> <a class="mention" href="https://x.com/docToolchain" rel="noopener noreferrer" target="_blank">@docToolchain</a> <a class="mention" href="https://x.com/j2r2b" rel="noopener noreferrer" target="_blank">@j2r2b</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> It\'s specifically a *documentation* static site generator, not just a general static site generator. And yes, it\'s geared specially for technical content written in AsciiDoc, and for those who write it. It was made, in part, to power the documentation for Asciidoctor itself.<br><br>In reply to: <a href="https://x.com/RalfDMueller/status/1458361115105275905" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <span class="status">1458361115105275905</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1458331140217978883',
    created: 1636528212000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> <a class="mention" href="https://x.com/docToolchain" rel="noopener noreferrer" target="_blank">@docToolchain</a> <a class="mention" href="https://x.com/j2r2b" rel="noopener noreferrer" target="_blank">@j2r2b</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> Agreed that they are different tools for different jobs. Each tool in the Asciidoctor ecosystem helps to bring in new ideas and strengthen its suitability for writers. We rely on that research, development, and exploration to learn and grow together.<br><br>In reply to: <a href="https://x.com/abelsromero/status/1458121645458132994" rel="noopener noreferrer" target="_blank">@abelsromero</a> <span class="status">1458121645458132994</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1457060245713289216',
    created: 1636225208000,
    type: 'reply',
    text: '@jbryant787 Yes. Autocorrect once again fighting the revolution.<br><br>In reply to: @jbryant787 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1456691322690027524',
    created: 1636137250000,
    type: 'quote',
    text: 'Patagonia gets it.<br><br>Quoting: <a href="https://x.com/patagonia/status/1455958195910836225" rel="noopener noreferrer" target="_blank">@patagonia</a> <span class="status">1455958195910836225</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1456687181955670022',
    created: 1636136262000,
    type: 'post',
    text: 'PSA: If the team size in your pricing model does not start a 2, you are being exclusionary to small businesses. Team means not an individual. It does mean you get to dictate the size of my team.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1456459799663501316',
    created: 1636082050000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ethomson" rel="noopener noreferrer" target="_blank">@ethomson</a> <a class="mention" href="https://x.com/bitandbang" rel="noopener noreferrer" target="_blank">@bitandbang</a> <a class="mention" href="https://x.com/npmjs" rel="noopener noreferrer" target="_blank">@npmjs</a> I\'m very pleased with the options as they are now. I\'ll pass this on to other maintainers I know and encourage them to enable them.<br><br>In reply to: <a href="https://x.com/ethomson/status/1456448584124141568" rel="noopener noreferrer" target="_blank">@ethomson</a> <span class="status">1456448584124141568</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1456397768725917718',
    created: 1636067261000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bitandbang" rel="noopener noreferrer" target="_blank">@bitandbang</a> I\'m trying to help by sharing what would have made sense to me. The original post was an inquiry about why developers aren\'t following the recommendations. I\'m sharing what words would have gotten that message across to me. I\'m not dictating. I\'m providing feedback.<br><br>In reply to: <a href="https://x.com/bitandbang/status/1456394802841505796" rel="noopener noreferrer" target="_blank">@bitandbang</a> <span class="status">1456394802841505796</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1456390830449520665',
    created: 1636065607000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bitandbang" rel="noopener noreferrer" target="_blank">@bitandbang</a> <a class="mention" href="https://x.com/npmjs" rel="noopener noreferrer" target="_blank">@npmjs</a> FWIW, I\'ve updated the settings on my packages to the strongest settings that make sense for automated publishing. So thanks for giving me a reason to have a second look.<br><br>In reply to: <a href="#1456388031808618523">1456388031808618523</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1456390179212521492',
    created: 1636065451000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bitandbang" rel="noopener noreferrer" target="_blank">@bitandbang</a> <a class="mention" href="https://x.com/npmjs" rel="noopener noreferrer" target="_blank">@npmjs</a> But I can see requiring 2FA for publishing to prevent someone from using the account to publish locally. So from that vector, it does make sense.<br><br>In reply to: <a href="#1456388031808618523">1456388031808618523</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1456388705707442193',
    created: 1636065100000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bitandbang" rel="noopener noreferrer" target="_blank">@bitandbang</a> <a class="mention" href="https://x.com/npmjs" rel="noopener noreferrer" target="_blank">@npmjs</a> The way this should be explained is that you should enable a token for publishing, and you should enable 2FA to create that token so no one else can create one from your account. That\'s the correct messaging.<br><br>In reply to: <a href="#1456388336952614913">1456388336952614913</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1456388336952614913',
    created: 1636065012000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bitandbang" rel="noopener noreferrer" target="_blank">@bitandbang</a> <a class="mention" href="https://x.com/npmjs" rel="noopener noreferrer" target="_blank">@npmjs</a> What\'s unclear is that a token is NOT 2FA. What you are asking people to enable is a token, not 2FA. No one should be publishing packages from a local machine. That\'s a horrible practice.<br><br>In reply to: <a href="https://x.com/bitandbang/status/1456385992341008388" rel="noopener noreferrer" target="_blank">@bitandbang</a> <span class="status">1456385992341008388</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1456388031808618523',
    created: 1636064939000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bitandbang" rel="noopener noreferrer" target="_blank">@bitandbang</a> <a class="mention" href="https://x.com/npmjs" rel="noopener noreferrer" target="_blank">@npmjs</a> I see that it\'s now possible to enable 2FA or a token. I can at least enable that. <a href="https://docs.npmjs.com/requiring-2fa-for-package-publishing-and-settings-modification#configuring-two-factor-authentication" rel="noopener noreferrer" target="_blank">docs.npmjs.com/requiring-2fa-for-package-publishing-and-settings-modification#configuring-two-factor-authentication</a> It\'s still not 2FA, so I think there is still a major messaging problem. It\'s restricted access, not 2FA. 2FA makes *no* sense for publishing.<br><br>In reply to: <a href="https://x.com/bitandbang/status/1456385992341008388" rel="noopener noreferrer" target="_blank">@bitandbang</a> <span class="status">1456385992341008388</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1456383577558433814',
    created: 1636063877000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bitandbang" rel="noopener noreferrer" target="_blank">@bitandbang</a> <a class="mention" href="https://x.com/npmjs" rel="noopener noreferrer" target="_blank">@npmjs</a> I see it\'s now possible to enable it only for authorization. I was not aware that\'s an option. <a href="https://docs.npmjs.com/about-two-factor-authentication#authorization-only" rel="noopener noreferrer" target="_blank">docs.npmjs.com/about-two-factor-authentication#authorization-only</a> But I still don\'t get who would use 2FA for publishing. Publishing packages manually is the worse of the two practices.<br><br>In reply to: <a href="#1456381653949956129">1456381653949956129</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1456381653949956129',
    created: 1636063419000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bitandbang" rel="noopener noreferrer" target="_blank">@bitandbang</a> <a class="mention" href="https://x.com/npmjs" rel="noopener noreferrer" target="_blank">@npmjs</a> ...and I, for one, already use a token to automate releases. So what exactly is the goal of shaming people over something that can\'t be used for automation? This whole campaign makes no sense to me.<br><br>In reply to: <a href="#1456380964339269654">1456380964339269654</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1456380964339269654',
    created: 1636063254000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bitandbang" rel="noopener noreferrer" target="_blank">@bitandbang</a> <a class="mention" href="https://x.com/npmjs" rel="noopener noreferrer" target="_blank">@npmjs</a> My point is, tokens are not 2FA. And if that\'s the case, the push for 2FA is a pointless one because it won\'t be used anyway.<br><br>In reply to: <a href="https://x.com/bitandbang/status/1456378818332172295" rel="noopener noreferrer" target="_blank">@bitandbang</a> <span class="status">1456378818332172295</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1456377582425817106',
    created: 1636062448000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bitandbang" rel="noopener noreferrer" target="_blank">@bitandbang</a> <a class="mention" href="https://x.com/npmjs" rel="noopener noreferrer" target="_blank">@npmjs</a> I still don\'t understand how this works with automated releases. How can the CI server look at a phone and type a pin?<br><br>In reply to: <a href="https://x.com/bitandbang/status/1456355728235630597" rel="noopener noreferrer" target="_blank">@bitandbang</a> <span class="status">1456355728235630597</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1456221977480421387',
    created: 1636025349000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/phillip_webb" rel="noopener noreferrer" target="_blank">@phillip_webb</a> Thanks again. <a href="https://docs.antora.org/antora/3.0/extend/extensions/#the-generator-context" rel="noopener noreferrer" target="_blank">docs.antora.org/antora/3.0/extend/extensions/#the-generator-context</a><br><br>In reply to: <a href="https://x.com/phillip_webb/status/1451659724189954057" rel="noopener noreferrer" target="_blank">@phillip_webb</a> <span class="status">1451659724189954057</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1455665402621480960',
    created: 1635892651000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/POTUS" rel="noopener noreferrer" target="_blank">@POTUS</a> Ban fracking. Forbid extraction on public lands. End subsidies for fossil fuels. Close down drilling in the Arctic. That\'s action.<br><br>In reply to: <a href="https://x.com/POTUS/status/1455545355743989775" rel="noopener noreferrer" target="_blank">@POTUS</a> <span class="status">1455545355743989775</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1455664365076566017',
    created: 1635892404000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/POTUS" rel="noopener noreferrer" target="_blank">@POTUS</a> Nice words. But I\'m more interested in action.<br><br>In reply to: <a href="https://x.com/POTUS/status/1455605589644648455" rel="noopener noreferrer" target="_blank">@POTUS</a> <span class="status">1455605589644648455</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1455664179398930435',
    created: 1635892359000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/benignbala" rel="noopener noreferrer" target="_blank">@benignbala</a> Way too late indeed.<br><br>In reply to: <a href="https://x.com/benignbala/status/1455495360978173958" rel="noopener noreferrer" target="_blank">@benignbala</a> <span class="status">1455495360978173958</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1455100208589651968',
    created: 1635757898000,
    type: 'post',
    text: 'I absolutely hate when I choose a library (carefully, I might add), integrate it, and then it\'s completely rewritten in an incompatible way. As if it wasn\'t hard enough to integrate it once.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1455037928304889857',
    created: 1635743050000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AlexandriaV2005" rel="noopener noreferrer" target="_blank">@AlexandriaV2005</a> Corporate Overlords Preforming.<br><br>In reply to: <a href="https://x.com/AlexandriaV2005/status/1455016932302737409" rel="noopener noreferrer" target="_blank">@AlexandriaV2005</a> <span class="status">1455016932302737409</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1454206871863115779',
    created: 1635544910000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jzb" rel="noopener noreferrer" target="_blank">@jzb</a> Woot! May your day be filled with happiness and joy! 💍🎉<br><br>In reply to: <a href="https://x.com/jzb/status/1454177676344897538" rel="noopener noreferrer" target="_blank">@jzb</a> <span class="status">1454177676344897538</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1453673489362079744',
    created: 1635417742000,
    type: 'post',
    text: 'When I write documentation, I always end up improving the code. It would serve me well to remind myself that these two activities are not mutually exclusive.',
    likes: 55,
    retweets: 14
  },
  {
    id: '1453614750445699076',
    created: 1635403738000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agentdero" rel="noopener noreferrer" target="_blank">@agentdero</a> You found the words I\'ve been searching for all day. 💯<br><br>In reply to: <a href="https://x.com/agentdero/status/1453574124454825985" rel="noopener noreferrer" target="_blank">@agentdero</a> <span class="status">1453574124454825985</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1452736196631662592',
    created: 1635194274000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/untappd" rel="noopener noreferrer" target="_blank">@untappd</a> Stop fucking deleting my entries! Once again you\'ve deleted an entry I made for a Hoplark crafted brewed hop tea. By removing it, you\'re being exclusionary to the non-alcoholic community who wants to be able to participate with fellow hop lovers minus the alcohol.<br><br>Quoting: <a href="#1448926754043621378">1448926754043621378</a>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1452592723895013376',
    created: 1635160067000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fanf42" rel="noopener noreferrer" target="_blank">@fanf42</a> Got a link to the API doc or some sort of intro to it?<br><br>In reply to: <a href="https://x.com/fanf42/status/1452387165774745602" rel="noopener noreferrer" target="_blank">@fanf42</a> <span class="status">1452387165774745602</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1452572121700319237',
    created: 1635155155000,
    type: 'post',
    text: 'I guess it\'s more commonly called column editing. <a href="https://en.wikipedia.org/wiki/Selection_(user_interface)#Column_selection" rel="noopener noreferrer" target="_blank">en.wikipedia.org/wiki/Selection_(user_interface)#Column_selection</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1452571513849139204',
    created: 1635155011000,
    type: 'post',
    text: 'I really wish Firefox supported block editing in a textarea. It would make writing comments in issues and online chat apps so much easier.',
    likes: 1,
    retweets: 1
  },
  {
    id: '1452555000350068738',
    created: 1635151073000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Blessed_Madonna" rel="noopener noreferrer" target="_blank">@Blessed_Madonna</a> I\'m at the point in my life where if that happened to me and I came out of it without an injury, I\'d take it as a win.<br><br>In reply to: <a href="https://x.com/Blessed_Madonna/status/1452552996131377154" rel="noopener noreferrer" target="_blank">@Blessed_Madonna</a> <span class="status">1452552996131377154</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1452012107742937091',
    created: 1635021638000,
    type: 'post',
    text: 'For those who cared to take this question seriously instead of responding with snark, the consensus seams to be that this is a context object, more specifically an application context (like in Spring). I might opt to name the context after the application itself.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1452011429431693315',
    created: 1635021476000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mauricioaniche" rel="noopener noreferrer" target="_blank">@mauricioaniche</a> Flow and Context from that project seem to be almost identical to what I have in mind. I need to control which steps are called, and notify observer of their completion, passing along any variables that are in scope at those transition points.<br><br>In reply to: <a href="https://x.com/mauricioaniche/status/1451997673783955461" rel="noopener noreferrer" target="_blank">@mauricioaniche</a> <span class="status">1451997673783955461</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1451850580121374721',
    created: 1634983127000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/thomasdarimont" rel="noopener noreferrer" target="_blank">@thomasdarimont</a> Super, super nice!<br><br>In reply to: <a href="https://x.com/thomasdarimont/status/1451655379667013641" rel="noopener noreferrer" target="_blank">@thomasdarimont</a> <span class="status">1451655379667013641</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1451787067021688833',
    created: 1634967984000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kakamelatte" rel="noopener noreferrer" target="_blank">@kakamelatte</a> That\'s always something to consider, but doesn\'t answer this question. The question is, if an object is passed through the system, what is the name of such an object? That doesn\'t immediately mean it\'s doing too much. It simply means it\'s has a long scope.<br><br>In reply to: <a href="https://x.com/kakamelatte/status/1451759441213329411" rel="noopener noreferrer" target="_blank">@kakamelatte</a> <span class="status">1451759441213329411</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1451716640169607170',
    created: 1634951193000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> I need that every week (not that I would know how to take it).<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/1451668872621395974" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">1451668872621395974</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1451687974421217284',
    created: 1634944358000,
    type: 'quote',
    text: 'Awareness is the best defense against breast cancer (and other cancers). But it\'s equally important to support those who survive it. Life is a gift. And it\'s even more beautiful to see volunteers crafting innovative, sustainable solutions to show they care. Amazing!<br><br>Quoting: <a href="https://x.com/Sharat_Chander/status/1451587862827175937" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1451587862827175937</span>',
    likes: 5,
    retweets: 1
  },
  {
    id: '1451679870577119233',
    created: 1634942426000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/eldermael" rel="noopener noreferrer" target="_blank">@eldermael</a> Those, plus RuntimeContext, definitely seem to be the front runners.<br><br>In reply to: <a href="https://x.com/eldermael/status/1451663245794676739" rel="noopener noreferrer" target="_blank">@eldermael</a> <span class="status">1451663245794676739</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1451679601386606597',
    created: 1634942362000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/phillip_webb" rel="noopener noreferrer" target="_blank">@phillip_webb</a> Now that you mention it...ApplicationContext is right along the lines of what I had in mind. Other frameworks refer to it as RuntimeContext. What I might want to do is attach the event emitter to this object to make it more well-structured.<br><br>In reply to: <a href="https://x.com/phillip_webb/status/1451659724189954057" rel="noopener noreferrer" target="_blank">@phillip_webb</a> <span class="status">1451659724189954057</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1451642714580160512',
    created: 1634933568000,
    type: 'post',
    text: 'What do you call an object that\'s passed throughout the application and that holds variables, emits events &amp; provides replaceable functions. I\'m thinking something like engine, pipeline, or runtime context, but can\'t land on the right word. What projects have something like that?',
    likes: 1,
    retweets: 0
  },
  {
    id: '1451635048092823552',
    created: 1634931740000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> They are already painfully slow. Besides, I\'m willing to pay for compute time. It\'s a utility and I\'m asking for more of it. If a company intentionally slows down normal builds, it would break my trust and I would go elsewhere.<br><br>In reply to: <a href="https://x.com/abelsromero/status/1451632012960337921" rel="noopener noreferrer" target="_blank">@abelsromero</a> <span class="status">1451632012960337921</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1451622046119514117',
    created: 1634928640000,
    type: 'post',
    text: 'I would pay money to accelerate CI jobs on demand. Are you hearing that CI companies? Add a button next to job that says "charge $1 to make to make this build run faster". On release day, I might spend up to $10-20 just so my time is not wasted. I hate waiting on release day.',
    likes: 34,
    retweets: 6
  },
  {
    id: '1451444952366391297',
    created: 1634886417000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/aheritier" rel="noopener noreferrer" target="_blank">@aheritier</a> <a class="mention" href="https://x.com/dadoonet" rel="noopener noreferrer" target="_blank">@dadoonet</a> <a class="mention" href="https://x.com/elastic" rel="noopener noreferrer" target="_blank">@elastic</a> It\'s much more complex than that. Google results are highly tailored. I\'ve observed it on numerous occasions. In other words, if someone says they get different results, believe them. It\'s very likely.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1451441906341269504" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1451441906341269504</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1451437840739360769',
    created: 1634884722000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dadoonet" rel="noopener noreferrer" target="_blank">@dadoonet</a> <a class="mention" href="https://x.com/aheritier" rel="noopener noreferrer" target="_blank">@aheritier</a> <a class="mention" href="https://x.com/elastic" rel="noopener noreferrer" target="_blank">@elastic</a> For what it\'s worth, I also get a 404 on the first result for that query. Remember that search engines often provide tailored results, which could be why you aren\'t seeing it. Try an anonymous browser.<br><br>In reply to: <a href="https://x.com/dadoonet/status/1451421772453228544" rel="noopener noreferrer" target="_blank">@dadoonet</a> <span class="status">1451421772453228544</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1451421080296509452',
    created: 1634880726000,
    type: 'reply',
    text: '@patrickbkoetter Facts. 🎯<br><br>In reply to: @patrickbkoetter <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1451310317632319490',
    created: 1634854318000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lukedary" rel="noopener noreferrer" target="_blank">@lukedary</a> Clever!<br><br>In reply to: <a href="https://x.com/lukedary/status/1451309609369677825" rel="noopener noreferrer" target="_blank">@lukedary</a> <span class="status">1451309609369677825</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1451301353637048320',
    created: 1634852181000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DrDooleyMD" rel="noopener noreferrer" target="_blank">@DrDooleyMD</a> I support you 💯 if you can manage to do it on the first try. It\'s the failed attempts that are rage inducing.<br><br>In reply to: <a href="https://x.com/DrDooleyMD/status/1451299604574711809" rel="noopener noreferrer" target="_blank">@DrDooleyMD</a> <span class="status">1451299604574711809</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1451300895841402894',
    created: 1634852072000,
    type: 'post',
    text: 'I was on a Zoom call with IT today and the representative\'s background was a bedroom. Not an actual bedroom. A picture of a bedroom. Is this peak pandemic?',
    likes: 4,
    retweets: 0
  },
  {
    id: '1451298154674995213',
    created: 1634851418000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/knowledge_owl" rel="noopener noreferrer" target="_blank">@knowledge_owl</a> <a class="mention" href="https://x.com/jaredmorgs" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> Hard agree.<br><br>In reply to: <a href="https://x.com/knowledge_owl/status/1441101054309847043" rel="noopener noreferrer" target="_blank">@knowledge_owl</a> <span class="status">1441101054309847043</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1450910121433862144',
    created: 1634758904000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/clebertsuconic" rel="noopener noreferrer" target="_blank">@clebertsuconic</a> I never knew your child went through that. While I don\'t have to know someone involved to care deeply about it, it does make it all the more harrowing. I\'m very glad to hear that she\'s safe. 🙏<br><br>In reply to: <a href="https://x.com/clebertsuconic/status/1450906947633520640" rel="noopener noreferrer" target="_blank">@clebertsuconic</a> <span class="status">1450906947633520640</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1450733560751529990',
    created: 1634716808000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/chvest" rel="noopener noreferrer" target="_blank">@chvest</a> I published some documentation in the Antora docs about how to set up git worktrees to manage docs in multiple branches: <a href="https://docs.antora.org/antora/3.0/playbook/author-mode/#multiple-worktrees" rel="noopener noreferrer" target="_blank">docs.antora.org/antora/3.0/playbook/author-mode/#multiple-worktrees</a><br><br>In reply to: <a href="https://x.com/chvest/status/1450731291050192897" rel="noopener noreferrer" target="_blank">@chvest</a> <span class="status">1450731291050192897</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1450701353425248257',
    created: 1634709130000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sierrahull" rel="noopener noreferrer" target="_blank">@sierrahull</a> Definitely! We recently got one and it\'s become our digital pet. We enjoy watching it roam around, encourage it when it gets confused, scold it when it tries to mount the feet of our desks, and apologize to it when we drop crumbs 🙇. It\'s fun, quiet, and keeps the floor clean.<br><br>In reply to: <a href="https://x.com/sierrahull/status/1450660854425427969" rel="noopener noreferrer" target="_blank">@sierrahull</a> <span class="status">1450660854425427969</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1450588071817142286',
    created: 1634682121000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GovofCO" rel="noopener noreferrer" target="_blank">@GovofCO</a> Why is it ever NOT free to apply?<br><br>In reply to: <a href="https://x.com/GovofCO/status/1450572781188378625" rel="noopener noreferrer" target="_blank">@GovofCO</a> <span class="status">1450572781188378625</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1450586117028212736',
    created: 1634681655000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jaredmorgs" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <a class="mention" href="https://x.com/hellodavidryan" rel="noopener noreferrer" target="_blank">@hellodavidryan</a> Love, love, love me some Netsky.<br><br>In reply to: <a href="https://x.com/jaredmorgs/status/1450573030028046336" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <span class="status">1450573030028046336</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1450060226724446210',
    created: 1634556273000,
    type: 'post',
    text: 'When the weekend comes to a close, I feel like I need a weekend before the week begins. I think I\'m weekending wrong.',
    likes: 10,
    retweets: 0
  },
  {
    id: '1450009042915446785',
    created: 1634544070000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kvegh" rel="noopener noreferrer" target="_blank">@kvegh</a> Not strange at all in my view. I\'ve tried a couple of those as well, including Grüvi. I\'m more of a beer guy, but I\'m so glad those options exist. More to explore and enjoy for all.<br><br>In reply to: <a href="https://x.com/kvegh/status/1449955862127292419" rel="noopener noreferrer" target="_blank">@kvegh</a> <span class="status">1449955862127292419</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1449954251392815108',
    created: 1634531007000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PavelAnni" rel="noopener noreferrer" target="_blank">@PavelAnni</a> I think Althletic is getting pretty close. There\'s also a brewery named Bootstrap in Colorado that has a great NA beer called Strapless. I\'m giving it a second try this week. These beers are awesome for post workout.<br><br>In reply to: <a href="https://x.com/PavelAnni/status/1449918920874467330" rel="noopener noreferrer" target="_blank">@PavelAnni</a> <span class="status">1449918920874467330</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1449878332980400139',
    created: 1634512906000,
    type: 'post',
    text: 'I\'ve been experimenting with non-alcoholic beers lately, and I\'m not hating it. I\'ve got a few favorites so far, like Athletic Brewing Free Wave, but I\'m still exploring what\'s out there to find which ones I like best.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1449815394030546944',
    created: 1634497900000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BriceDutheil" rel="noopener noreferrer" target="_blank">@BriceDutheil</a> You might have better luck getting the relevant people\'s attention in the community chat. If you haven\'t joined yet, now would be a good time.<br><br>In reply to: <a href="https://x.com/BriceDutheil/status/1449730760022405123" rel="noopener noreferrer" target="_blank">@BriceDutheil</a> <span class="status">1449730760022405123</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1449665894477545475',
    created: 1634462257000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/neugens" rel="noopener noreferrer" target="_blank">@neugens</a> What angers me so much is that they stole something from us that has such potential for good and corrupted it with this "both sides" rubbish. The only thing someone has to do now to be platformed is to denigrate others. Shoots them straight to the top.<br><br>In reply to: <a href="https://x.com/neugens/status/1449664678326571010" rel="noopener noreferrer" target="_blank">@neugens</a> <span class="status">1449664678326571010</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1449663765918588934',
    created: 1634461749000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/neugens" rel="noopener noreferrer" target="_blank">@neugens</a> And why would they do that? It would go against their business plan of sowing conflict and spreading misinformation. Twitter has become just as bad as Facebook, and I resent that fact.<br><br>In reply to: <a href="https://x.com/neugens/status/1449660078169145348" rel="noopener noreferrer" target="_blank">@neugens</a> <span class="status">1449660078169145348</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1449659903551803393',
    created: 1634460829000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/neugens" rel="noopener noreferrer" target="_blank">@neugens</a> I look away.<br><br>In reply to: <a href="https://x.com/neugens/status/1449655227414679553" rel="noopener noreferrer" target="_blank">@neugens</a> <span class="status">1449655227414679553</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1449647368601686019',
    created: 1634457840000,
    type: 'post',
    text: 'I have to admit, I have really just tuned out of Twitter trends, or any sort of recommended content. It\'s mostly just garbage and pointless banter these days.',
    likes: 7,
    retweets: 0
  },
  {
    id: '1448927772093542402',
    created: 1634286275000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bmarwell" rel="noopener noreferrer" target="_blank">@bmarwell</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Those docs are now available. See <a href="https://docs.asciidoctor.org/asciidoctor/latest/convert/templates/" rel="noopener noreferrer" target="_blank">docs.asciidoctor.org/asciidoctor/latest/convert/templates/</a><br><br>In reply to: <a href="#1448208055900905472">1448208055900905472</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1448926754043621378',
    created: 1634286032000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/untappd" rel="noopener noreferrer" target="_blank">@untappd</a> I\'ve really put my time, effort, and trust into your community. (Just look at how thorough and thoughtful my entries are). I\'m appalled that you\'d treat such a veteran member this way. Have you nothing to say?<br><br>Quoting: <a href="#1448369780239388673">1448369780239388673</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1448913774660833287',
    created: 1634282938000,
    type: 'post',
    text: 'I just published documentation I\'ve been wanting to write for over 8 years. It covers how to use converter templates in Asciidoctor.<br><br><a href="https://docs.asciidoctor.org/asciidoctor/latest/convert/templates/" rel="noopener noreferrer" target="_blank">docs.asciidoctor.org/asciidoctor/latest/convert/templates/</a><br><br>I really do have to credit Antora for giving me the motivation to write documentation. Clearly, it\'s making a difference.',
    likes: 25,
    retweets: 6
  },
  {
    id: '1448589352322277377',
    created: 1634205589000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MStine" rel="noopener noreferrer" target="_blank">@MStine</a> For me it\'s when it reads like a story. When I step through it and can say, "yes, that explains what I want it to do," then I know I\'ve arrived.<br><br>In reply to: @mstine <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1448588477860220931',
    created: 1634205381000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/erinfoox" rel="noopener noreferrer" target="_blank">@erinfoox</a> OMG, I am so doing this when \'tis the season.<br><br>In reply to: <a href="https://x.com/erinfoox/status/1448375299742126084" rel="noopener noreferrer" target="_blank">@erinfoox</a> <span class="status">1448375299742126084</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1448377961367756800',
    created: 1634155190000,
    type: 'post',
    text: 'I stand corrected that Hoplark is not a woman-owned business. I was thinking of Gruvi.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1448375858175959042',
    created: 1634154688000,
    type: 'post',
    text: 'I also want to add that Hoplark is tapped into the craft brew scene. They\'re currently featuring a collaboration with Outer Range Brewing. Would you disallow that entry too? Do you think Outer Range Brewing thinks they aren\'t brewing a beer? Perhaps you should ask them.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1448369780239388673',
    created: 1634153239000,
    type: 'post',
    text: 'I also really, really, really don\'t appreciate you removing my submission without giving me a chance to defend it. I\'ve been a user for almost a decade and that action feels incredibly disrespectful. It turns me off, way off.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1448368276153667588',
    created: 1634152881000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/untappd" rel="noopener noreferrer" target="_blank">@untappd</a> Please don\'t shun <a class="mention" href="https://x.com/drinkhoplark" rel="noopener noreferrer" target="_blank">@drinkhoplark</a> beverages from your database. These drinks are marketed for NA beer drinkers, are brewed like beer, feature a hop profile, and are a crucial option when trying to avoid consuming alcohol. They\'re also a woman-owned brewing business.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1448232263934627840',
    created: 1634120453000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/samaaron" rel="noopener noreferrer" target="_blank">@samaaron</a> One day though 🙏. The range of acts they have hosted tells me they\'re all about experimentation. I think it would open a window into this new world off the Pioneer DJ decks.<br><br>In reply to: <a href="https://x.com/samaaron/status/1448230697190182913" rel="noopener noreferrer" target="_blank">@samaaron</a> <span class="status">1448230697190182913</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1448230406201884674',
    created: 1634120010000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/samaaron" rel="noopener noreferrer" target="_blank">@samaaron</a> I would absolutely love to see you play at HÖR Berlin. Are they open to it?<br><br>In reply to: <a href="https://x.com/samaaron/status/1448227038977011715" rel="noopener noreferrer" target="_blank">@samaaron</a> <span class="status">1448227038977011715</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1448208055900905472',
    created: 1634114681000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bmarwell" rel="noopener noreferrer" target="_blank">@bmarwell</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> You can use templates to customize the output of a converter, which allows you to avoid writing code (aside from what\'s in the template file). I\'m just finishing up the docs on how to do that now. It\'s the easiest path.<br><br>In reply to: <a href="https://x.com/bmarwell/status/1448204902254190592" rel="noopener noreferrer" target="_blank">@bmarwell</a> <span class="status">1448204902254190592</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1448179118764294144',
    created: 1634107782000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bmarwell" rel="noopener noreferrer" target="_blank">@bmarwell</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Asciidoctor allows you to customize any of the output it produces. See <a href="https://docs.asciidoctor.org/asciidoctor/latest/convert/custom/" rel="noopener noreferrer" target="_blank">docs.asciidoctor.org/asciidoctor/latest/convert/custom/</a><br><br>In reply to: <a href="https://x.com/bmarwell/status/1448034250792517635" rel="noopener noreferrer" target="_blank">@bmarwell</a> <span class="status">1448034250792517635</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1448062034721923073',
    created: 1634079867000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bmarwell" rel="noopener noreferrer" target="_blank">@bmarwell</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> That sounds like a great idea for the in progress modern HTML 5 converter. See <a href="https://github.com/asciidoctor/asciidoctor/projects/1" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor/projects/1</a><br><br>In reply to: <a href="https://x.com/bmarwell/status/1448036131031879688" rel="noopener noreferrer" target="_blank">@bmarwell</a> <span class="status">1448036131031879688</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1447776105381855233',
    created: 1634011696000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AlexandriaV2005" rel="noopener noreferrer" target="_blank">@AlexandriaV2005</a> I\'m profoundly disappointed by this decision. They\'re stealing your future while hiding behind bureaucracy to ease their guilt. Those who make peaceful revolution impossible will make violent revolution inevitable.<br><br>In reply to: <a href="https://x.com/AlexandriaV2005/status/1447729760411418625" rel="noopener noreferrer" target="_blank">@AlexandriaV2005</a> <span class="status">1447729760411418625</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1447519503013711875',
    created: 1633950517000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/eddiejaoude" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> <a class="mention" href="https://x.com/FrancescoCiull4" rel="noopener noreferrer" target="_blank">@FrancescoCiull4</a> <a class="mention" href="https://x.com/dailydotdev" rel="noopener noreferrer" target="_blank">@dailydotdev</a> And if you like Asciidoctor, then <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> is the docs site generator you\'re looking for 😉.<br><br>In reply to: <a href="https://x.com/eddiejaoude/status/1447494790833131521" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> <span class="status">1447494790833131521</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1447496077909790723',
    created: 1633944932000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ChrisGPackham" rel="noopener noreferrer" target="_blank">@ChrisGPackham</a> <a class="mention" href="https://x.com/nationaltrust" rel="noopener noreferrer" target="_blank">@nationaltrust</a> I will keep watching. I will keep learning. And I will keep supporting your vision for a sustainable environment in which all life is respected. Change is coming whether those cowardly trolls like it or not.<br><br>In reply to: <a href="#1447495473862885382">1447495473862885382</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1447495473862885382',
    created: 1633944788000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ChrisGPackham" rel="noopener noreferrer" target="_blank">@ChrisGPackham</a> <a class="mention" href="https://x.com/nationaltrust" rel="noopener noreferrer" target="_blank">@nationaltrust</a> I want to first say that I\'m incensed that this happened to you. Your voice has helped transform my appreciation and reverence of the natural world. I stand in full solidarity with you and your causes. I\'m also inspired by your courage to stand resolute in face of these threats.<br><br>In reply to: <a href="https://x.com/ChrisGPackham/status/1446932226839564374" rel="noopener noreferrer" target="_blank">@ChrisGPackham</a> <span class="status">1446932226839564374</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1446949555442913281',
    created: 1633814631000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jmbruel" rel="noopener noreferrer" target="_blank">@jmbruel</a> Funny you should say that.<br><br>In reply to: <a href="https://x.com/jmbruel/status/1446801593345654790" rel="noopener noreferrer" target="_blank">@jmbruel</a> <span class="status">1446801593345654790</span>',
    photos: ['<div class="item"><img class="photo" src="media/1446949555442913281.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1446698740811763712',
    created: 1633754832000,
    type: 'post',
    text: 'Also Mt Yale.',
    photos: ['<div class="item"><img class="photo" src="media/1446698740811763712.jpg"></div>'],
    likes: 2,
    retweets: 0
  },
  {
    id: '1446697325603311616',
    created: 1633754495000,
    type: 'post',
    text: 'This is Mt Yale.',
    photos: ['<div class="item"><img class="photo" src="media/1446697325603311616.jpg"></div>'],
    likes: 2,
    retweets: 0
  },
  {
    id: '1446692607283793922',
    created: 1633753370000,
    type: 'post',
    text: 'Correction. This is Mt Princeton. Mt Yale is the peak to the north. I got them mixed up.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1446370501371310106',
    created: 1633676574000,
    type: 'post',
    text: 'That\'s Timberline Lake in the Holy Cross Wilderness (Lake County, CO).',
    likes: 0,
    retweets: 0
  },
  {
    id: '1446325027587059724',
    created: 1633665732000,
    type: 'post',
    text: 'It\'s not every day that you get to walk across a lake bed of an alpine lake.',
    photos: ['<div class="item"><img class="photo" src="media/1446325027587059724.jpg"></div>'],
    likes: 3,
    retweets: 0
  },
  {
    id: '1446322092719230978',
    created: 1633665032000,
    type: 'post',
    text: 'I\'ve been trying NA beers recently and I\'ve really been enjoying them. My favorites so far are Grüvi IPA, Bootstrap Strapless, Althletic Free Wave, and HopTea (various). I haven\'t found *the one* yet, but the search continues.',
    photos: ['<div class="item"><img class="photo" src="media/1446322092719230978.jpg"></div>'],
    likes: 2,
    retweets: 0
  },
  {
    id: '1446312869776084994',
    created: 1633662834000,
    type: 'post',
    text: 'When you see Mt. Yale in person, you understand why we must protect our national parks at all costs. There\'s only one word that comes to mind. Vast. Incomprehensibly vast.',
    photos: ['<div class="item"><img class="photo" src="media/1446312869776084994.jpg"></div>'],
    likes: 5,
    retweets: 0
  },
  {
    id: '1446311372870938625',
    created: 1633662477000,
    type: 'post',
    text: 'Colorado, is there anything more beautiful than you?',
    photos: ['<div class="item"><img class="photo" src="media/1446311372870938625.jpg"></div>'],
    likes: 23,
    retweets: 0
  },
  {
    id: '1446310097857040391',
    created: 1633662173000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> Message received ;)<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/1446307002611904559" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">1446307002611904559</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1446304700417159184',
    created: 1633660886000,
    type: 'quote',
    text: 'Apropos as I\'m currently visiting some of the most beautiful places in this country (Colorado\'s highest peaks). Our national parks are and will forever be America\'s best idea.<br><br>Quoting: <a href="https://x.com/NPR/status/1446267271362535426" rel="noopener noreferrer" target="_blank">@NPR</a> <span class="status">1446267271362535426</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1446303371644874768',
    created: 1633660569000,
    type: 'quote',
    text: 'Protect our national monuments at all costs.<br><br>Quoting: <a href="https://x.com/kylegriffin1/status/1446266829853265945" rel="noopener noreferrer" target="_blank">@kylegriffin1</a> <span class="status">1446266829853265945</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1446296221199388673',
    created: 1633658864000,
    type: 'post',
    text: 'When drinking beer at <a class="mention" href="https://x.com/TwoMileBrewing" rel="noopener noreferrer" target="_blank">@TwoMileBrewing</a>, it\'s no joke. It hits you quick. That\'s an experience in and of itself. Proper beer, a bonus. 🍻',
    likes: 1,
    retweets: 0
  },
  {
    id: '1446025331169796096',
    created: 1633594279000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> GitLab has a package registry for publishing private or public gems.<br><br>In reply to: <a href="https://x.com/headius/status/1445826218750709763" rel="noopener noreferrer" target="_blank">@headius</a> <span class="status">1445826218750709763</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1446024600643686403',
    created: 1633594105000,
    type: 'post',
    text: 'I\'m officially declaring Leadville a planetarium.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1446023936987344897',
    created: 1633593947000,
    type: 'post',
    text: 'I\'m in Leadville, CO, the highest incorporated town in North America. I stepped outside to look at the night sky, and WOW! The stars are brighter than I\'ve ever seen them. 🤩 Orion is so clear that I could capture it with my phone camera (and that\'s only half of what I can see).',
    photos: ['<div class="item"><img class="photo" src="media/1446023936987344897.jpg"></div>'],
    likes: 2,
    retweets: 0
  },
  {
    id: '1445651246166118402',
    created: 1633505090000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RichardSmedley" rel="noopener noreferrer" target="_blank">@RichardSmedley</a> <a class="mention" href="https://x.com/cefn" rel="noopener noreferrer" target="_blank">@cefn</a> <a class="mention" href="https://x.com/TechySquirrel" rel="noopener noreferrer" target="_blank">@TechySquirrel</a> <a class="mention" href="https://x.com/opendevise" rel="noopener noreferrer" target="_blank">@opendevise</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> And you helped me get to the bottom of a problem that\'s been driving me crazy for months. Sometimes, when two problems collide, we get a rare opportunity to solve both of them at once. And we learned something at the same time. Success!<br><br>In reply to: <a href="https://x.com/RichardSmedley/status/1445641431142391808" rel="noopener noreferrer" target="_blank">@RichardSmedley</a> <span class="status">1445641431142391808</span>',
    likes: 3,
    retweets: 1
  },
  {
    id: '1445464425394171916',
    created: 1633460549000,
    type: 'post',
    text: 'Why when things get difficult do people immediately think that a private list is the solution?',
    likes: 2,
    retweets: 0
  },
  {
    id: '1445331836465340416',
    created: 1633428937000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AmelieLens" rel="noopener noreferrer" target="_blank">@AmelieLens</a> Answer: The very BEST techno.<br><br>In reply to: <a href="https://x.com/AmelieLens/status/1445317420315459584" rel="noopener noreferrer" target="_blank">@AmelieLens</a> <span class="status">1445317420315459584</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1445243515575799810',
    created: 1633407880000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> They seem to think we came to hear them.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1445220269728096260" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1445220269728096260</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1445155196691116033',
    created: 1633386823000,
    type: 'post',
    text: 'To be clear, other people can prefer live festivals and have the time of their lives. That\'s great. Do it. Party on. But just don\'t forgot that there are those of us who don\'t. And we don\'t want to be shut out.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1445146732619190272',
    created: 1633384805000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/codeslubber" rel="noopener noreferrer" target="_blank">@codeslubber</a> True, but relying on Facebook for authentication and to serve assets definitely is (in the sense that the logic is offloaded to servers outside of the your control). But my point is, cloud doesn\'t mean "not my problem anymore". It just means the problem is over there.<br><br>In reply to: <a href="https://x.com/codeslubber/status/1445145259025965059" rel="noopener noreferrer" target="_blank">@codeslubber</a> <span class="status">1445145259025965059</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1445145984497905670',
    created: 1633384626000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jimisola" rel="noopener noreferrer" target="_blank">@jimisola</a> <a class="mention" href="https://x.com/mishunov" rel="noopener noreferrer" target="_blank">@mishunov</a> <a class="mention" href="https://x.com/gitlab" rel="noopener noreferrer" target="_blank">@gitlab</a> For me, it reports 2.0.15. It really should be using the latest patch release, which is 2.0.16.<br><br>In reply to: <a href="#1445145724279144469">1445145724279144469</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1445145724279144469',
    created: 1633384564000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jimisola" rel="noopener noreferrer" target="_blank">@jimisola</a> <a class="mention" href="https://x.com/mishunov" rel="noopener noreferrer" target="_blank">@mishunov</a> <a class="mention" href="https://x.com/gitlab" rel="noopener noreferrer" target="_blank">@gitlab</a> Yes, GitLab uses Asciidoctor. You can find out which version by putting {asciidoctor-version} in your document. Since GitLab is open, the Asciidoctor project leads and community are active in helping to improve support for AsciiDoc on GitLab.<br><br>In reply to: <a href="https://x.com/jimisola/status/1445112533216550914" rel="noopener noreferrer" target="_blank">@jimisola</a> <span class="status">1445112533216550914</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1445144735803269123',
    created: 1633384329000,
    type: 'post',
    text: 'I much prefer virtual festivals. I just can\'t stand all the screaming. I think it ruins the music. My kingdom for a virtual festival any day.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1445144483138453518',
    created: 1633384268000,
    type: 'post',
    text: 'The internet seems to be collectively melting down today. Just a reminder that the cloud is just someone else\'s computer.',
    likes: 12,
    retweets: 2
  },
  {
    id: '1445099330038231052',
    created: 1633373503000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mishunov" rel="noopener noreferrer" target="_blank">@mishunov</a> <a class="mention" href="https://x.com/jimisola" rel="noopener noreferrer" target="_blank">@jimisola</a> <a class="mention" href="https://x.com/gitlab" rel="noopener noreferrer" target="_blank">@gitlab</a> Please don\'t treat AsciiDoc as a second class citizen. It\'s mission critical for a large number of companies and organizations, large and small. And that adoption is growing. Better AsciiDoc support is also a driving factor for why users are choosing GitLab over GitHub.<br><br>In reply to: <a href="https://x.com/mishunov/status/1444922507581083649" rel="noopener noreferrer" target="_blank">@mishunov</a> <span class="status">1444922507581083649</span>',
    likes: 3,
    retweets: 2
  },
  {
    id: '1444221463674847235',
    created: 1633164203000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> Rain, rain, go away! Come again when it\'s not my friend\'s birthday.<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/1444212797315993604" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">1444212797315993604</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1444211982014423042',
    created: 1633161943000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> Happy birthday, buddy. May your furry (and perhaps not so furry) friends at Dale Store gård bring you another year of joy. I\'m having an Orval today in your name (first check-in, if you can believe it). Miss you!',
    likes: 1,
    retweets: 0
  },
  {
    id: '1444005772065591300',
    created: 1633112779000,
    type: 'post',
    text: 'It\'s not "wake me up when September ends." It\'s more like "September has ended, now can I sleep?"',
    likes: 5,
    retweets: 0
  },
  {
    id: '1443993567471435787',
    created: 1633109869000,
    type: 'quote',
    text: 'Asciidoctor at work, helping with cyberspace defense!<br><br>Quoting: <a href="https://x.com/MISPProject/status/1443235182614614027" rel="noopener noreferrer" target="_blank">@MISPProject</a> <span class="status">1443235182614614027</span>',
    likes: 5,
    retweets: 2
  },
  {
    id: '1443744515836153857',
    created: 1633050490000,
    type: 'post',
    text: 'Sometimes, people ask you to confirm something that\'s so obviously true, it\'s hard to find any resources to back it up. How can I prove to someone that a search engine doesn\'t prioritize a subfolder over a subdomain given the contents of the page are the same. Cite page rank?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1443705393482395689',
    created: 1633041163000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> I\'d challenge people who think they don\'t have a choice. FB is one out of millions of companies, &gt;100,000 in tech alone. I find it inconceivable that a person who can work at FB couldn\'t get an offer elsewhere. We can\'t keep sleep walking into the collapse of our democracy.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1443695609131814916" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1443695609131814916</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1443694381651816461',
    created: 1633038537000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> You captured my sentiments exactly.<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/1443692698599297024" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">1443692698599297024</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1443618396172476421',
    created: 1633020421000,
    type: 'reply',
    text: '@hughsient News to me. Is it just a new name, or different software?<br><br>In reply to: @hughsient <span class="status">&lt;deleted&gt;</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1442795080700489730',
    created: 1632824127000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> That\'s a great point. And when you know that a majority of readers are going to overlook a point which cannot be missed, that\'s a solid justification for making it an admonition. (Often it\'s an FAQ).<br><br>In reply to: <a href="https://x.com/marcsavy/status/1442788481739235329" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1442788481739235329</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1442787149451956227',
    created: 1632822236000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> My first rule of thumb is to try not to use them. When that doesn\'t work, or doesn\'t feel right, then I consider where I want to take the reader out of the flow and for what purpose. It should really only be used when you want to interrupt the dialog. It can also aid in scanning.<br><br>In reply to: <a href="https://x.com/settermjd/status/1442776020227137539" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1442776020227137539</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1442628935301222405',
    created: 1632784515000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/NewslineCO" rel="noopener noreferrer" target="_blank">@NewslineCO</a> Missing the point. Stop putting this on commuters. Yes, they should be encouraged to take mass transit, or work from home. But the real solution is to ban fracking and all other fossil fuel extraction.<br><br>In reply to: <a href="https://x.com/NewslineCO/status/1442616638101204993" rel="noopener noreferrer" target="_blank">@NewslineCO</a> <span class="status">1442616638101204993</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1442611882079129601',
    created: 1632780449000,
    type: 'reply',
    text: '@uracreative <a class="mention" href="https://x.com/bitshiftmask" rel="noopener noreferrer" target="_blank">@bitshiftmask</a> <a class="mention" href="https://x.com/OxidizeConf" rel="noopener noreferrer" target="_blank">@OxidizeConf</a> <a class="mention" href="https://x.com/opensrcdesign" rel="noopener noreferrer" target="_blank">@opensrcdesign</a> I\'ll definitely make use of that design board to connect with designers in the future. The situation I\'m currently dealing with is sensitive and thus prevents me from being able to make a public post. Please put me in contact with anyone you think I should reach out to.<br><br>In reply to: <a href="https://x.com/uradotdesign/status/1442249270996791298" rel="noopener noreferrer" target="_blank">@uradotdesign</a> <span class="status">1442249270996791298</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1442402380604063746',
    created: 1632730500000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GeoffreyDeSmet" rel="noopener noreferrer" target="_blank">@GeoffreyDeSmet</a> <a class="mention" href="https://x.com/riduidel" rel="noopener noreferrer" target="_blank">@riduidel</a> <a class="mention" href="https://x.com/javabake" rel="noopener noreferrer" target="_blank">@javabake</a> My experience is mostly with JavaScript and Ruby template engines. But what I can tell you is to look for engines that compile to platform code. There are a lot that don\'t, and the performance shows. Where possible, I just try to avoid them doing anything complex.<br><br>In reply to: <a href="https://x.com/GeoffreyDeSmet/status/1442400672398254081" rel="noopener noreferrer" target="_blank">@GeoffreyDeSmet</a> <span class="status">1442400672398254081</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1442396311358083075',
    created: 1632729053000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GeoffreyDeSmet" rel="noopener noreferrer" target="_blank">@GeoffreyDeSmet</a> <a class="mention" href="https://x.com/riduidel" rel="noopener noreferrer" target="_blank">@riduidel</a> <a class="mention" href="https://x.com/javabake" rel="noopener noreferrer" target="_blank">@javabake</a> In my experience, the templating system is often the culprit. They have to do a lot of parsing and interpolation, which can slow things down considerably.<br><br>In reply to: <a href="https://x.com/GeoffreyDeSmet/status/1442393869199499271" rel="noopener noreferrer" target="_blank">@GeoffreyDeSmet</a> <span class="status">1442393869199499271</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1442244643953909760',
    created: 1632692893000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bitshiftmask" rel="noopener noreferrer" target="_blank">@bitshiftmask</a> @uracreative <a class="mention" href="https://x.com/OxidizeConf" rel="noopener noreferrer" target="_blank">@OxidizeConf</a> Thanks for the recommendation! That appears to be precisely the starting point I\'m looking for.<br><br>In reply to: <a href="https://x.com/bitshiftmask/status/1441919436433158157" rel="noopener noreferrer" target="_blank">@bitshiftmask</a> <span class="status">1441919436433158157</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1442240771214635008',
    created: 1632691970000,
    type: 'quote',
    text: 'Let\'s be real. Software developers do exactly the same thing when they generate a PDF, only to use OCR to read the text back in. It hurts because it\'s true.<br><br>Quoting: <a href="https://x.com/FordMwool/status/1441528733962149888" rel="noopener noreferrer" target="_blank">@FordMwool</a> <span class="status">1441528733962149888</span>',
    likes: 3,
    retweets: 1
  },
  {
    id: '1441912652691910658',
    created: 1632613740000,
    type: 'post',
    text: 'I\'d also prefer a designer who understands how to work in the OSS space. Getting approval from fellow maintainers, community leaders, and, within reason, the community at large, is essential. Design doesn\'t happen in a vacuum.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1441911977627033603',
    created: 1632613579000,
    type: 'post',
    text: 'If I don\'t use something like 99designs, how can I find a good designer who I can trust to do quality work? I\'m willing to pay a competitive rate (what the work is truly worth). I just don\'t want to waste time and money on an amateur effort. I need results.',
    likes: 7,
    retweets: 0
  },
  {
    id: '1441878823461552134',
    created: 1632605674000,
    type: 'quote',
    text: '"It\'s hypocritical to talk about saving the climate as long as you\'re still expanding fossil fuel infrastructure."<br><br>Quoting: <a href="https://x.com/AlexandriaV2005/status/1441851006728294401" rel="noopener noreferrer" target="_blank">@AlexandriaV2005</a> <span class="status">1441851006728294401</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1441876427511500802',
    created: 1632605103000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/badmannersfood" rel="noopener noreferrer" target="_blank">@badmannersfood</a> I can attest that recipe is legit. Just had it for 3 nights in a row in this house. Amazing. Good with guac on top too.<br><br>In reply to: <a href="https://x.com/badmannersfood/status/1441873650609623040" rel="noopener noreferrer" target="_blank">@badmannersfood</a> <span class="status">1441873650609623040</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1441500364335841282',
    created: 1632515443000,
    type: 'quote',
    text: '"Sorry we murdered your children."<br><br>Quoting: <a href="https://x.com/ajplus/status/1441494785739538433" rel="noopener noreferrer" target="_blank">@ajplus</a> <span class="status">1441494785739538433</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1441498531861123076',
    created: 1632515006000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasonbrooks" rel="noopener noreferrer" target="_blank">@jasonbrooks</a> <a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> Now that would be something.<br><br>In reply to: <a href="https://x.com/jasonbrooks/status/1441497593020702720" rel="noopener noreferrer" target="_blank">@jasonbrooks</a> <span class="status">1441497593020702720</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1441493915027210244',
    created: 1632513905000,
    type: 'post',
    text: 'Liquid rain. I\'m sorry, is there any other type?',
    likes: 2,
    retweets: 0
  },
  {
    id: '1441478663371821058',
    created: 1632510269000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bmarwell" rel="noopener noreferrer" target="_blank">@bmarwell</a> 9 times out of 10, you should use the shorthand syntax [#&lt;idname&gt;]. The [[]] is legacy, and the id attribute is the formal assignment available only because it\'s the low-level mechanism.<br><br>In reply to: <a href="https://x.com/bmarwell/status/1441111539415597071" rel="noopener noreferrer" target="_blank">@bmarwell</a> <span class="status">1441111539415597071</span>',
    likes: 5,
    retweets: 1
  },
  {
    id: '1441329720595943435',
    created: 1632474758000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jakekorab" rel="noopener noreferrer" target="_blank">@jakekorab</a> 🙏<br><br>In reply to: <a href="https://x.com/jakekorab/status/1441113597325688832" rel="noopener noreferrer" target="_blank">@jakekorab</a> <span class="status">1441113597325688832</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1441108671006576642',
    created: 1632422056000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MrLarrieu" rel="noopener noreferrer" target="_blank">@MrLarrieu</a> <a class="mention" href="https://x.com/revealjs" rel="noopener noreferrer" target="_blank">@revealjs</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> As <a class="mention" href="https://x.com/hsablonniere" rel="noopener noreferrer" target="_blank">@hsablonniere</a> will tell you, making an HTML slide deck is a fantastic way to learn CSS and/or hone your proficiency.<br><br>In reply to: <a href="https://x.com/MrLarrieu/status/1441045464598540296" rel="noopener noreferrer" target="_blank">@MrLarrieu</a> <span class="status">1441045464598540296</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1440463112205832195',
    created: 1632268143000,
    type: 'post',
    text: 'I think this is a defining moment for Democrats in the US. Either they get these bills passed in tandem, or it will be impossible to trust any promise they make in the future. Are they going to fight for the workers or are they going to soak up money from special interests?',
    likes: 1,
    retweets: 0
  },
  {
    id: '1440277014120853506',
    created: 1632223773000,
    type: 'post',
    text: 'And then there\'s the advancement of drone videography to consider. We see places, things, and perspectives now that we never saw before thanks to incredible drone operators. And the correlation with music is what makes it work.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1440276300887826436',
    created: 1632223603000,
    type: 'post',
    text: 'That\'s not to say clubs and festivals aren\'t important. They\'re vital to society and sanity. I support them 💯. But they don\'t make the music better just because of what they are. Sometimes, it\'s worse. Sometimes, it\'s magic. But music can be great outside of them, often better.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1440275179968425994',
    created: 1632223336000,
    type: 'post',
    text: 'Many people say music will be better once it returns to clubs and festivals. I don\'t agree. I think music went through a renaissance during the pandemic. Not just the performances, but the recording quality and production. Clubs and festivals are about dancing. That\'s different.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1440271840899833860',
    created: 1632222540000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> Is both not an option?<br><br>In reply to: <a href="https://x.com/majson/status/1440223116785971201" rel="noopener noreferrer" target="_blank">@majson</a> <span class="status">1440223116785971201</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1440271727951450116',
    created: 1632222513000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jakekorab" rel="noopener noreferrer" target="_blank">@jakekorab</a> My thoughts are with you. Well wishes to AM.<br><br>In reply to: <a href="https://x.com/jakekorab/status/1440253925383303178" rel="noopener noreferrer" target="_blank">@jakekorab</a> <span class="status">1440253925383303178</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1440249912428400641',
    created: 1632217312000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/LaurenJBuchman" rel="noopener noreferrer" target="_blank">@LaurenJBuchman</a> Cosign.<br><br>In reply to: <a href="https://x.com/LaurenJBuchman/status/1439983548283305985" rel="noopener noreferrer" target="_blank">@LaurenJBuchman</a> <span class="status">1439983548283305985</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1440249763715108884',
    created: 1632217276000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/briannekimmel" rel="noopener noreferrer" target="_blank">@briannekimmel</a> Tell them you will stop anything at any time to hear their concerns. Any reasonable person will respect your time, but the offer will totally change their trust in you as a leader.<br><br>In reply to: <a href="https://x.com/briannekimmel/status/1440017091084447744" rel="noopener noreferrer" target="_blank">@briannekimmel</a> <span class="status">1440017091084447744</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1439325835928899585',
    created: 1631996995000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/badmannersfood" rel="noopener noreferrer" target="_blank">@badmannersfood</a> Ordered, just in time to say f$%k the mushroom cloud of bullshit from this past week and do some (not so) serious cooking. Thanks for the vice!<br><br>In reply to: <a href="https://x.com/badmannersfood/status/1436433767602610179" rel="noopener noreferrer" target="_blank">@badmannersfood</a> <span class="status">1436433767602610179</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1438976668232683522',
    created: 1631913747000,
    type: 'quote',
    text: 'As soon as it has public archives, I\'m prepared to say it\'s one of the best community tools I\'ve ever used.<br><br>Quoting: <a href="https://x.com/jmsfbs/status/1438777025859256320" rel="noopener noreferrer" target="_blank">@jmsfbs</a> <span class="status">1438777025859256320</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1438949234103885832',
    created: 1631907206000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aheritier" rel="noopener noreferrer" target="_blank">@aheritier</a> <a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> I happen to think it\'s a pretty awesome place to hang out ;)<br><br>In reply to: <a href="#1438949024892026886">1438949024892026886</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1438949024892026886',
    created: 1631907156000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aheritier" rel="noopener noreferrer" target="_blank">@aheritier</a> <a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> 👍<br><br>In reply to: <a href="https://x.com/aheritier/status/1438931499638722560" rel="noopener noreferrer" target="_blank">@aheritier</a> <span class="status">1438931499638722560</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1438930808778092547',
    created: 1631902813000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aheritier" rel="noopener noreferrer" target="_blank">@aheritier</a> <a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> In the future, can you direct questions like this to the community chat at <a href="https://asciidoctor.zulipchat.com" rel="noopener noreferrer" target="_blank">asciidoctor.zulipchat.com</a>? Thanks!<br><br>In reply to: <a href="#1438930601495523334">1438930601495523334</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1438930601495523334',
    created: 1631902764000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aheritier" rel="noopener noreferrer" target="_blank">@aheritier</a> <a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> It was a mistake to offer built-in color names in the first place. Generic colors rarely fit with an existing theme, and should thus be defined by the theme.<br><br>In reply to: <a href="#1438930441059266565">1438930441059266565</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1438930441059266565',
    created: 1631902725000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aheritier" rel="noopener noreferrer" target="_blank">@aheritier</a> <a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> The default theme in Asciidoctor does not provide roles for predefined color names out of the box (and they\'ll soon be removed from the default stylesheet for HTML). You need to define roles for colors using the PDF theme. See <a href="https://github.com/asciidoctor/asciidoctor-pdf/blob/v1.6.x/docs/theming-guide.adoc#keys-role" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-pdf/blob/v1.6.x/docs/theming-guide.adoc#keys-role</a><br><br>In reply to: <a href="https://x.com/aheritier/status/1438882276952190976" rel="noopener noreferrer" target="_blank">@aheritier</a> <span class="status">1438882276952190976</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1438755661056131072',
    created: 1631861055000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ryanbigg" rel="noopener noreferrer" target="_blank">@ryanbigg</a> In the Node.js source code, you are looking for lib/internal/modules/cjs/loader.js<br><br><a href="https://github.com/nodejs/node/blob/master/lib/internal/modules/cjs/loader.js#L667-L704" rel="noopener noreferrer" target="_blank">github.com/nodejs/node/blob/master/lib/internal/modules/cjs/loader.js#L667-L704</a><br><br>In reply to: <a href="#1438754554867113987">1438754554867113987</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1438754554867113987',
    created: 1631860791000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ryanbigg" rel="noopener noreferrer" target="_blank">@ryanbigg</a> You can get a slightly less accurate result using:<br><br>require.resolve.paths(\'name-of-module\')<br><br>In reply to: <a href="#1438754293608169477">1438754293608169477</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1438754293608169477',
    created: 1631860729000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ryanbigg" rel="noopener noreferrer" target="_blank">@ryanbigg</a> I can help you here since I had to investigate that very topic recently. See <a href="https://gist.github.com/mojavelinux/4bfc2ff1b41fa86a336ea83be672b549" rel="noopener noreferrer" target="_blank">gist.github.com/mojavelinux/4bfc2ff1b41fa86a336ea83be672b549</a><br><br>In reply to: <a href="https://x.com/ryanbigg/status/1438667667213193216" rel="noopener noreferrer" target="_blank">@ryanbigg</a> <span class="status">1438667667213193216</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1438391900680060929',
    created: 1631774327000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> I love to see that you\'ve found a project for which you have so much passion. It\'s the same way I feel about Antora.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1438388036392341508" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1438388036392341508</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1438218821169414145',
    created: 1631733062000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jensimmons" rel="noopener noreferrer" target="_blank">@jensimmons</a> Web developers need to take a stand. Refuse to add this garbage to the site. We need an activism of sorts.<br><br>In reply to: <a href="https://x.com/jensimmons/status/1437762023782297606" rel="noopener noreferrer" target="_blank">@jensimmons</a> <span class="status">1437762023782297606</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1438212940830703617',
    created: 1631731660000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bmarwell" rel="noopener noreferrer" target="_blank">@bmarwell</a> <a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> I strongly recommend using kramdown-asciidoc (kramdoc) instead of pandoc. It\'s going to give you better results. (Unless you have already tried it and it didn\'t work out for you). See <a href="https://github.com/asciidoctor/kramdown-asciidoc" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/kramdown-asciidoc</a><br><br>In reply to: <a href="https://x.com/bmarwell/status/1438083002882527232" rel="noopener noreferrer" target="_blank">@bmarwell</a> <span class="status">1438083002882527232</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1438085029976969217',
    created: 1631701164000,
    type: 'post',
    text: 'I\'m finally getting around to writing docs for Asciidoctor that I have put off for years (many years). And it\'s all thanks to Antora (and AsciiDoc, of course).',
    likes: 24,
    retweets: 0
  },
  {
    id: '1437955170013384705',
    created: 1631670203000,
    type: 'post',
    text: 'The person who you should be screaming at is Manchin, who\'s a money drunk treacherous traitor of the working class...and who has absolutely no class.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1437954358092599300',
    created: 1631670009000,
    type: 'post',
    text: 'And yes, I agree the met is an elitist, frivolous event. But activism doesn\'t just happen in the streets. You need to get right up in peoples\' faces...and AOC did that. That doesn\'t make her a hero. It just makes her an activist that did a thing.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1437953124082208769',
    created: 1631669715000,
    type: 'post',
    text: 'That goes double if you think you are better than AOC.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1437951872980041733',
    created: 1631669417000,
    type: 'quote',
    text: 'If you liked this picture, but you don\'t follow-up by voting to tax the rich, you were just in it for the spectacle. Don\'t blame AOC (or anyone else) if you don\'t have the spine (or sense) to follow through.<br><br>Quoting: <a href="https://x.com/AOC/status/1437635268664774659" rel="noopener noreferrer" target="_blank">@AOC</a> <span class="status">1437635268664774659</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1437305274998558723',
    created: 1631515256000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/badmannersfood" rel="noopener noreferrer" target="_blank">@badmannersfood</a> Every. Single. Day.<br><br>In reply to: <a href="https://x.com/badmannersfood/status/1437246638020460544" rel="noopener noreferrer" target="_blank">@badmannersfood</a> <span class="status">1437246638020460544</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1436893684163760131',
    created: 1631417125000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/carolinegleich" rel="noopener noreferrer" target="_blank">@carolinegleich</a> <a class="mention" href="https://x.com/patagonia" rel="noopener noreferrer" target="_blank">@patagonia</a> Thanks! I hope you\'re enjoying it too. Indeed, our dads are rewriting the rules of aging. I just went on a hike with my father last week and could barely keep up...and I\'ve been serious about exercising during the pandemic. I need to up my game! 🏋️<br><br>In reply to: <a href="https://x.com/carolinegleich/status/1436859351533195267" rel="noopener noreferrer" target="_blank">@carolinegleich</a> <span class="status">1436859351533195267</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1436260817826041857',
    created: 1631266238000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/carolinegleich" rel="noopener noreferrer" target="_blank">@carolinegleich</a> <a class="mention" href="https://x.com/patagonia" rel="noopener noreferrer" target="_blank">@patagonia</a> Thank you for sharing this story about your dad, and about you. It was touching and has helped me better understand how to cherish the relationship with my own father. Like your father, my father defies age, participating in 6 sports a week. I aspire to be that fit all life long.<br><br>In reply to: <a href="https://x.com/carolinegleich/status/1406738244742512640" rel="noopener noreferrer" target="_blank">@carolinegleich</a> <span class="status">1406738244742512640</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1435498660301733894',
    created: 1631084525000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GeoffreyDeSmet" rel="noopener noreferrer" target="_blank">@GeoffreyDeSmet</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> It\'s right in the README for the project. <a href="https://gitlab.com/antora/antora#getting-help" rel="noopener noreferrer" target="_blank">gitlab.com/antora/antora#getting-help</a><br><br>In reply to: <a href="#1435498292930965506">1435498292930965506</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1435498292930965506',
    created: 1631084437000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GeoffreyDeSmet" rel="noopener noreferrer" target="_blank">@GeoffreyDeSmet</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> <a href="https://antora.zulipchat.com" rel="noopener noreferrer" target="_blank">antora.zulipchat.com</a><br><br>In reply to: <a href="https://x.com/GeoffreyDeSmet/status/1435494497631313922" rel="noopener noreferrer" target="_blank">@GeoffreyDeSmet</a> <span class="status">1435494497631313922</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1434811823484653570',
    created: 1630920770000,
    type: 'reply',
    text: '@ndw GitHub Actions. By no means am I an evangelist of the platform. I\'m sure there are lots of great options. But it has worked very well for the open source projects I run. (My second choice is GitLab CI, which is my first choice for projects hosted on GitLab).<br><br>In reply to: @ndw <span class="status">&lt;deleted&gt;</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1434785331555954690',
    created: 1630914454000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ilopmar" rel="noopener noreferrer" target="_blank">@ilopmar</a> <a class="mention" href="https://x.com/CedricChampeau" rel="noopener noreferrer" target="_blank">@CedricChampeau</a> <a class="mention" href="https://x.com/Mattermost" rel="noopener noreferrer" target="_blank">@Mattermost</a> I strongly encourage you to evaluate Zulip. We\'ve been using it for Asciidoctor and Antora and I\'ve never been happier using chat.<br><br>In reply to: <a href="https://x.com/ilopmar/status/1434782627039490048" rel="noopener noreferrer" target="_blank">@ilopmar</a> <span class="status">1434782627039490048</span>',
    likes: 7,
    retweets: 1
  },
  {
    id: '1433744295018721283',
    created: 1630666252000,
    type: 'post',
    text: 'Live event streaming platforms that require you to watch the event when it actually happens still aren\'t getting it.<br><br>(I understand if it has to be time limited for licensing reasons, just not real time).',
    likes: 3,
    retweets: 0
  },
  {
    id: '1433699549218938885',
    created: 1630655584000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobbytank42" rel="noopener noreferrer" target="_blank">@bobbytank42</a> I\'m of the belief that people who abuse and attack us know exactly what they\'re doing and the negative impact it has. We must not tolerate it. People can act professionally or find another place to be.<br><br>In reply to: <a href="#1433698100703490049">1433698100703490049</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1433698100703490049',
    created: 1630655238000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobbytank42" rel="noopener noreferrer" target="_blank">@bobbytank42</a> Thanks. While it\'s a pain to deal with, I take comfort in the fact that I get to work alongside the best people I know, including you, and I always feel very supported. And I\'m motivated to protect the space we have made.<br><br>In reply to: <a href="https://x.com/bobbytank42/status/1433697127201394706" rel="noopener noreferrer" target="_blank">@bobbytank42</a> <span class="status">1433697127201394706</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1433696737084903425',
    created: 1630654913000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobbytank42" rel="noopener noreferrer" target="_blank">@bobbytank42</a> Sadly, it\'s been happening a lot lately (though not coming from anyone who has a shred of respect for the project and the people who work on it). We live in an increasingly toxic world and there are people who believe it\'s okay to verbally abuse people to get what they want.<br><br>In reply to: <a href="https://x.com/bobbytank42/status/1433696149513375744" rel="noopener noreferrer" target="_blank">@bobbytank42</a> <span class="status">1433696149513375744</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1433691419303051269',
    created: 1630653645000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> Never have. Never will. (Though, as is necessary, I do use them for testing all the time).<br><br>In reply to: <a href="https://x.com/joschi83/status/1433667198892093448" rel="noopener noreferrer" target="_blank">@joschi83</a> <span class="status">1433667198892093448</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1433592279541846017',
    created: 1630630008000,
    type: 'post',
    text: 'I\'m just not putting up with it anymore. I have taken abuse my entire life. I know what it looks like. And I won\'t stand for it in the communities that I run. Full stop.',
    likes: 6,
    retweets: 0
  },
  {
    id: '1433572546373304323',
    created: 1630625304000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> I trust Red Hat more than any other company to get that right. (It\'s not blind trust, but trust nonetheless).<br><br>In reply to: <a href="https://x.com/brunoborges/status/1433568648552071189" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1433568648552071189</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1433564079583477774',
    created: 1630623285000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> I see.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1433563486697639938" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1433563486697639938</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1433551559414083586',
    created: 1630620300000,
    type: 'post',
    text: 'If you come into an issue and use aggression to try to shame me into working on an issue, I\'m going to stand up for myself. So you better be ready for the consequences if you choose childish behavior.',
    likes: 5,
    retweets: 0
  },
  {
    id: '1433550513572769816',
    created: 1630620051000,
    type: 'post',
    text: 'I\'ll be honest. I didn\'t even know that Docker needed a desktop application. (I use podman anyway, also without a destkop app).',
    likes: 16,
    retweets: 0
  },
  {
    id: '1433209104248414213',
    created: 1630538652000,
    type: 'post',
    text: 'TIP: Using Refined GitHub, you can set the font on GitHub to whatever you want it to be using the custom CSS field. Apply the font-family property to this selector: body, .markdown-body <a href="https://github.com/sindresorhus/refined-github" rel="noopener noreferrer" target="_blank">github.com/sindresorhus/refined-github</a>',
    likes: 0,
    retweets: 1
  },
  {
    id: '1433199219653836804',
    created: 1630536296000,
    type: 'post',
    text: 'There\'s a difference between having an open dialogue and dragging me into a mismanaged situation. I\'m not going to be party to the latter. In other words, it\'s not my job to convince a staff member in a public forum to do a job they refuse to do properly.',
    likes: 8,
    retweets: 0
  },
  {
    id: '1432762449627807745',
    created: 1630432162000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CedricChampeau" rel="noopener noreferrer" target="_blank">@CedricChampeau</a> I\'ve reached the same conclusion.<br><br>In reply to: <a href="https://x.com/CedricChampeau/status/1432686528816418818" rel="noopener noreferrer" target="_blank">@CedricChampeau</a> <span class="status">1432686528816418818</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1432362149444161547',
    created: 1630336723000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GeoffreyDeSmet" rel="noopener noreferrer" target="_blank">@GeoffreyDeSmet</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> There is no official plugin at this time.<br><br>In reply to: <a href="https://x.com/GeoffreyDeSmet/status/1432322566304190470" rel="noopener noreferrer" target="_blank">@GeoffreyDeSmet</a> <span class="status">1432322566304190470</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1431906271314079748',
    created: 1630228033000,
    type: 'post',
    text: 'I\'m enjoying a visit from a family member for the first time in 2 years. It\'s also the first time I\'m testing my newfound culinary skills on someone other than my spouse and I. It almost feels like a dream.',
    likes: 13,
    retweets: 0
  },
  {
    id: '1431753826772549633',
    created: 1630191687000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tkadlec" rel="noopener noreferrer" target="_blank">@tkadlec</a> That\'s a nice looking haul!<br><br>In reply to: <a href="https://x.com/tkadlec/status/1431708508626505730" rel="noopener noreferrer" target="_blank">@tkadlec</a> <span class="status">1431708508626505730</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1431527175820230660',
    created: 1630137649000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> Music unites us.<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1431468792391946240" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1431468792391946240</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1431366672103870469',
    created: 1630099382000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> <a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> <a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> It\'s in part the tool\'s fault, and in part GitHub\'s fault. What they did was totally irresponsible and lacked any consideration of the consequences. And now we\'re left figuring out how to recover from here.<br><br>In reply to: <a href="https://x.com/majson/status/1431365059872182278" rel="noopener noreferrer" target="_blank">@majson</a> <span class="status">1431365059872182278</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1431364560758345732',
    created: 1630098879000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> <a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> <a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> The key phrase there is "you can configure". It\'s not being configured, and now we are left with complete trash everywhere. I\'m so upset about it.<br><br>In reply to: <a href="https://x.com/majson/status/1431364114824241159" rel="noopener noreferrer" target="_blank">@majson</a> <span class="status">1431364114824241159</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1431364367900024835',
    created: 1630098833000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> It may be true that renovate is better. But that\'s not the point here. We\'re talking about dependabot and the nightmare GitHub unleashed by enabling it by default on repositories / encouraging its usage. I see this as a major threat to OSS sanity.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1431227294039818240" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1431227294039818240</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1431363696807215104',
    created: 1630098673000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> <a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> <a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> No. You\'re rationalizing. Dependabot is at fault here.<br><br>In reply to: <a href="https://x.com/majson/status/1431363448655466496" rel="noopener noreferrer" target="_blank">@majson</a> <span class="status">1431363448655466496</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1431362779772977152',
    created: 1630098454000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> <a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> <a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> I\'ll repeat. Dependabot is a plague. That\'s not an opinion. That\'s a fact.<br><br>In reply to: <a href="#1431362464692592640">1431362464692592640</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1431362464692592640',
    created: 1630098379000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> <a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> <a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> And we see the same mess in the PRs. Finding anything in a project like that is a fishing expedition. <a href="https://github.com/cheeriojs/cheerio/pulls?q=is%3Apr+is%3Aclosed" rel="noopener noreferrer" target="_blank">github.com/cheeriojs/cheerio/pulls?q=is%3Apr+is%3Aclosed</a> I HATE IT.<br><br>In reply to: <a href="#1431361962236018694">1431361962236018694</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1431362156184813569',
    created: 1630098306000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> <a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> It\'s only because JavaScript libraries tend to get released more often (part of the culture). Over time, every project that has dependencies would see this problem.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1431330038121652232" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1431330038121652232</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1431361962236018694',
    created: 1630098259000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> <a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> <a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> Exhibit A. Look at this commit history: <a href="https://github.com/cheeriojs/cheerio/commits/main" rel="noopener noreferrer" target="_blank">github.com/cheeriojs/cheerio/commits/main</a> It\'s a nightmare. It\'s not my projects that have a problem, but now I have to deal with projects that look like that.<br><br>In reply to: <a href="https://x.com/majson/status/1431325445472067590" rel="noopener noreferrer" target="_blank">@majson</a> <span class="status">1431325445472067590</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1431318041011253249',
    created: 1630087788000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> <a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> <a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> Good point about it being a hint to cut out dependencies. I agree with that 💯. But for dependencies you must have, dependabot completely wrecks the git history and wastes an incredible amount of the limited time maintainers have. Projects stall out just doing busy work.<br><br>In reply to: <a href="https://x.com/majson/status/1431226746066636805" rel="noopener noreferrer" target="_blank">@majson</a> <span class="status">1431226746066636805</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1431211250298474499',
    created: 1630062327000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> Dependabot is a plague.<br><br>In reply to: <a href="https://x.com/gunnarmorling/status/1430931354972303366" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <span class="status">1430931354972303366</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1431050101892411401',
    created: 1630023906000,
    type: 'post',
    text: 'Projects that don\'t maintain a CHANGELOG file annoy me. And it\'s doubly annoying if there are no tag commits in the main branch. It turns upgrading into a research project.',
    likes: 17,
    retweets: 2
  },
  {
    id: '1430830551242448905',
    created: 1629971561000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MissCNovelli" rel="noopener noreferrer" target="_blank">@MissCNovelli</a> That was such a great set! It had an elegant intensity to it, and the vocals played in so well. I really enjoyed it (and will enjoy it again and again). Good luck!<br><br>In reply to: <a href="https://x.com/MissCNovelli/status/1429187466309799937" rel="noopener noreferrer" target="_blank">@MissCNovelli</a> <span class="status">1429187466309799937</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1430429325367468035',
    created: 1629875901000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SaraSoueidan" rel="noopener noreferrer" target="_blank">@SaraSoueidan</a> Totally agree. When I\'m at a conference, I\'ve always made it a point to be in the front row of any talk being delivered by someone in my community. In fact, I see it as part of my commitment as a leader of the community to be there.<br><br>In reply to: <a href="https://x.com/SaraSoueidan/status/1430418150248550403" rel="noopener noreferrer" target="_blank">@SaraSoueidan</a> <span class="status">1430418150248550403</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1430428111925960704',
    created: 1629875612000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/j2r2b" rel="noopener noreferrer" target="_blank">@j2r2b</a> <a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <a class="mention" href="https://x.com/java" rel="noopener noreferrer" target="_blank">@java</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Exactly. We could be flexible about the syntax of the tag itself. The point is that all the mechanics are there if we need to adapt to an emerging standard. The tag/end syntax in Asciidoctor is just something we invented in the absence of a de facto standard.<br><br>In reply to: <a href="https://x.com/j2r2b/status/1430427000393641990" rel="noopener noreferrer" target="_blank">@j2r2b</a> <span class="status">1430427000393641990</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1430244015694176256',
    created: 1629831720000,
    type: 'post',
    text: 'If I click "I am not a robot" enough times in a row, I\'m half expecting the computer to respond with:<br><br>"Doth protest too much."',
    likes: 2,
    retweets: 0
  },
  {
    id: '1430231514885681154',
    created: 1629828740000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/waynebeaton" rel="noopener noreferrer" target="_blank">@waynebeaton</a> I\'m so excited about this. It closes a 20-year old loop, and the proposal is looking very strong.<br><br>In reply to: <a href="https://x.com/waynebeaton/status/1430201215611281409" rel="noopener noreferrer" target="_blank">@waynebeaton</a> <span class="status">1430201215611281409</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1430231032700145667',
    created: 1629828625000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/j2r2b" rel="noopener noreferrer" target="_blank">@j2r2b</a> <a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <a class="mention" href="https://x.com/java" rel="noopener noreferrer" target="_blank">@java</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> ...and we could definitely consider supporting that syntax in AsciiDoc. Seems like it\'s general enough that it could stand in for tag::name[] ... end::name[].<br><br>In reply to: <a href="https://x.com/j2r2b/status/1430056604695769112" rel="noopener noreferrer" target="_blank">@j2r2b</a> <span class="status">1430056604695769112</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1428639162832363530',
    created: 1629449093000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/eddiejaoude" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> You bet! Oh, and btw, happy birthday! 🥳<br><br>In reply to: <a href="https://x.com/eddiejaoude/status/1428621999753109512" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> <span class="status">1428621999753109512</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1428545639290327040',
    created: 1629426796000,
    type: 'quote',
    text: 'I\'m 💯 in support of this strategy, whether you\'re building a new product / project or adding a feature to an existing one. Have a short architecture / strategy session, then try to code a rough prototype. You\'ll learn so much that the next discussion will be far more productive.<br><br>Quoting: <a href="https://x.com/eddiejaoude/status/1428477921434509314" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> <span class="status">1428477921434509314</span>',
    likes: 8,
    retweets: 3
  },
  {
    id: '1428477770233876481',
    created: 1629410614000,
    type: 'quote',
    text: 'Although I don\'t use Bootstrap, I use it as a reference. To me, that\'s the purpose CSS / UI frameworks serve best. They provide models. And I truly appreciate Bootstrap for the source of inspiration it is.<br><br>Quoting: <a href="https://x.com/getbootstrap/status/1428402303086694402" rel="noopener noreferrer" target="_blank">@getbootstrap</a> <span class="status">1428402303086694402</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1428446280536104960',
    created: 1629403107000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gjtorikian" rel="noopener noreferrer" target="_blank">@gjtorikian</a> Salient point. Wondering where your next watering is coming from and when would be too stressful.<br><br>In reply to: <a href="https://x.com/gjtorikian/status/1428445902646124544" rel="noopener noreferrer" target="_blank">@gjtorikian</a> <span class="status">1428445902646124544</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1428445047544967172',
    created: 1629402813000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gjtorikian" rel="noopener noreferrer" target="_blank">@gjtorikian</a> Now you\'ve got me thinking...what plant would I want to be? 🤔 What plant have you decided to be?<br><br>In reply to: <a href="https://x.com/gjtorikian/status/1428443893503205377" rel="noopener noreferrer" target="_blank">@gjtorikian</a> <span class="status">1428443893503205377</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1428326126934315016',
    created: 1629374460000,
    type: 'quote',
    text: 'This is a tremendous shock. My spouse and I only discovered Sean early last year. We would look forward every night to watching his latest bit on Cats or Cats Does Countdown. His humor helped make life worth living during pandemic. I\'ll always remember that gift he gave us.<br><br>Quoting: <a href="https://x.com/8Outof10Cats/status/1428020112834056193" rel="noopener noreferrer" target="_blank">@8Outof10Cats</a> <span class="status">1428020112834056193</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1428081828573810690',
    created: 1629316215000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/OurRevolution" rel="noopener noreferrer" target="_blank">@OurRevolution</a> Medicare for ALL. NO more fossil fuels. Stop promoting a washed-down agenda.<br><br>In reply to: <a href="https://x.com/OurRevolution/status/1428037567400845319" rel="noopener noreferrer" target="_blank">@OurRevolution</a> <span class="status">1428037567400845319</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1428038255178625024',
    created: 1629305826000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/briandemers" rel="noopener noreferrer" target="_blank">@briandemers</a> Congrats Brian! Great to have another awesome chap and champion in our humble group. 🎈<br><br>In reply to: <a href="https://x.com/briandemers/status/1427384133718876165" rel="noopener noreferrer" target="_blank">@briandemers</a> <span class="status">1427384133718876165</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1427958670109773830',
    created: 1629286851000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> That\'s why I now believe in a code linter precheck in the CI build. I didn\'t used to, but I was wrong. It eliminates code formatting concerns because it\'s already done by the time you review. And if it\'s part of the test suite, you never introduce problems when writing code.<br><br>In reply to: <a href="https://x.com/gunnarmorling/status/1427580879711444994" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <span class="status">1427580879711444994</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1427796597513981955',
    created: 1629248210000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/OurRevolution" rel="noopener noreferrer" target="_blank">@OurRevolution</a> What we\'ve learned from the pandemic is that people really enjoy interacting with a broadcast. Twitch just exploded. It let\'s us get to know the candidates, how they talk about issues, and who they really are. It won\'t reach everyone, everywhere, but it will reach many.<br><br>In reply to: <a href="#1427796028753801220">1427796028753801220</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1427796028753801220',
    created: 1629248075000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/OurRevolution" rel="noopener noreferrer" target="_blank">@OurRevolution</a> I just don\'t believe in this strategy...and I don\'t think it\'s really working. For one, I really hate getting calls and texts. It\'s just annoying. What would be better is to up the video production game so that there are more interviews and Twitch-like sessions on YouTube.<br><br>In reply to: <a href="https://x.com/OurRevolution/status/1427794876054310912" rel="noopener noreferrer" target="_blank">@OurRevolution</a> <span class="status">1427794876054310912</span>',
    likes: 6,
    retweets: 0
  },
  {
    id: '1427769695789215749',
    created: 1629241796000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_JamesWard" rel="noopener noreferrer" target="_blank">@_JamesWard</a> Congrats dude. That was an easy +1 from me. You\'re a champ indeed!<br><br>In reply to: <a href="https://x.com/_JamesWard/status/1427613432325550085" rel="noopener noreferrer" target="_blank">@_JamesWard</a> <span class="status">1427613432325550085</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1427760313458315271',
    created: 1629239559000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/crichardson" rel="noopener noreferrer" target="_blank">@crichardson</a> That\'s not to say there aren\'t real and imminent problems. There are. But we are being made to feel so overwhelmed that we\'re far too distressed to do anything about them. We have to separate the two, then also punish the owners for this unethical activity they are engaging in.<br><br>In reply to: <a href="#1427757164693655552">1427757164693655552</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1427757164693655552',
    created: 1629238809000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/crichardson" rel="noopener noreferrer" target="_blank">@crichardson</a> I think it\'s more sinister than that. I think the owners of these platforms are actively sowing conflict and distress. It\'s been heading that way for years, but we\'re now witnessing a unabashed onslaught of it.<br><br>In reply to: <a href="https://x.com/crichardson/status/1427742734866022402" rel="noopener noreferrer" target="_blank">@crichardson</a> <span class="status">1427742734866022402</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1427735175438364674',
    created: 1629233566000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/crichardson" rel="noopener noreferrer" target="_blank">@crichardson</a> While things are undoubtedly bad, there\'s no question in my mind that Twitter (and any other social media platform, for that matter) is being exploited by its owners as a propaganda tool that\'s making us feel worse.<br><br>In reply to: <a href="https://x.com/crichardson/status/1427303216245448713" rel="noopener noreferrer" target="_blank">@crichardson</a> <span class="status">1427303216245448713</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1427376946539728922',
    created: 1629148158000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/om26er" rel="noopener noreferrer" target="_blank">@om26er</a> It\'s important to be able to recognize the difference.<br><br>In reply to: <a href="#1427376761424121884">1427376761424121884</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1427376761424121884',
    created: 1629148113000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/om26er" rel="noopener noreferrer" target="_blank">@om26er</a> I\'m always glad to help, in a public forum where others can benefit from the answer. Asking me to do it privately is taking advantage of my time. Full stop.<br><br>In reply to: <a href="https://x.com/om26er/status/1427372475978067975" rel="noopener noreferrer" target="_blank">@om26er</a> <span class="status">1427372475978067975</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1427327356675395584',
    created: 1629136334000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> From time to time, I\'ll also help a friend in need. I still have to remind myself, though, that I could be taking something away from the community by doing it in a back channel.<br><br>In reply to: <a href="#1427327159543078919">1427327159543078919</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1427327159543078919',
    created: 1629136287000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> I agree that if you\'re already being paid to do the work, then essentially someone is paying, and it\'s legit. That\'s a special situation. The main point is, don\'t let people take advantage of you (especially when it takes away from the community).<br><br>In reply to: <a href="https://x.com/headius/status/1427326131724640256" rel="noopener noreferrer" target="_blank">@headius</a> <span class="status">1427326131724640256</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1427325780493697029',
    created: 1629135959000,
    type: 'post',
    text: 'I don\'t know who needs to hear this, but...<br><br>If you volunteer on an OSS project, and someone asks you for help in a private message, don\'t be afraid to ask them to post the question in a public channel or to hire you. You aren\'t obligated to provide 1-on-1 support for free.',
    likes: 86,
    retweets: 17
  },
  {
    id: '1427324090801528862',
    created: 1629135556000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/vlad_mihalcea" rel="noopener noreferrer" target="_blank">@vlad_mihalcea</a> The reason I say it\'s universal, and not just a FLOSS issue, is because we see the same everywhere. Someone will try to put a new roof on their house and either cause a major leak or fall from it and break their arm before hiring a professional / specialist.<br><br>In reply to: <a href="#1427301242095775751">1427301242095775751</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1427301330478059529',
    created: 1629130129000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/vlad_mihalcea" rel="noopener noreferrer" target="_blank">@vlad_mihalcea</a> <a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> Yep.<br><br>In reply to: <a href="https://x.com/vlad_mihalcea/status/1427258977407094791" rel="noopener noreferrer" target="_blank">@vlad_mihalcea</a> <span class="status">1427258977407094791</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1427301242095775751',
    created: 1629130108000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/vlad_mihalcea" rel="noopener noreferrer" target="_blank">@vlad_mihalcea</a> Oh, I\'ve had a client that banged their head for over a year, then finally came around. This dance is universal. It\'s a long game, and that makes for better clients in the end. Trust me, they will pay. Sometimes, they just have a need to feel extreme pain first.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1427258151255121923" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1427258151255121923</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1427236676540276737',
    created: 1629114715000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/vlad_mihalcea" rel="noopener noreferrer" target="_blank">@vlad_mihalcea</a> Most of the problem is with developers being to timid to actually stand their ground. Can\'t blame the people who don\'t pay you if you never ask them to. Be unapologetic.<br><br>In reply to: <a href="#1427236392313270278">1427236392313270278</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1427236392313270278',
    created: 1629114647000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/vlad_mihalcea" rel="noopener noreferrer" target="_blank">@vlad_mihalcea</a> Many of my clients I got simply by making it clear that what they need is paid support. You just have to be forward and ask (sure, there will be plenty who pass, but they don\'t get the help either).<br><br>In reply to: <a href="https://x.com/marcsavy/status/1427231864260079620" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1427231864260079620</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1427231146673270786',
    created: 1629113396000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/vlad_mihalcea" rel="noopener noreferrer" target="_blank">@vlad_mihalcea</a> I would phrase it another way. You need to hire me if you feel the need to DM me.<br><br>In reply to: <a href="https://x.com/vlad_mihalcea/status/1427224320825249794" rel="noopener noreferrer" target="_blank">@vlad_mihalcea</a> <span class="status">1427224320825249794</span>',
    likes: 5,
    retweets: 0
  },
  {
    id: '1427029447463428097',
    created: 1629065307000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/KillerMike" rel="noopener noreferrer" target="_blank">@KillerMike</a> Pissed.<br><br>In reply to: <a href="https://x.com/KillerMike/status/1426840198521004034" rel="noopener noreferrer" target="_blank">@KillerMike</a> <span class="status">1426840198521004034</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1427029267095834627',
    created: 1629065264000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> Dang. It\'s frustrating to be in the dark about it. But no variant gonna hold you down.<br><br>In reply to: <a href="https://x.com/starbuxman/status/1427025428032593923" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">1427025428032593923</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1427020743246696451',
    created: 1629063232000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> Do you know if it\'s the delta variant?<br><br>In reply to: <a href="#1426869540521185280">1426869540521185280</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1426869540521185280',
    created: 1629027183000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> I\'m really sorry to hear that Josh. I know how worried you\'ve been about catching it and I\'ve been hoping it would miss you. Get well soon, and kick COVID19\'s ass! You are a champ.<br><br>In reply to: <a href="https://x.com/starbuxman/status/1426502213912784900" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">1426502213912784900</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1426457392401633280',
    created: 1628928919000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/scottdavis99" rel="noopener noreferrer" target="_blank">@scottdavis99</a> I love me a can with some good artwork. It definitely enhances the experience.<br><br>In reply to: <a href="https://x.com/scottdavis99/status/1426379359179317249" rel="noopener noreferrer" target="_blank">@scottdavis99</a> <span class="status">1426379359179317249</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1426338788914077698',
    created: 1628900642000,
    type: 'post',
    text: 'How it started: I\'m cracking open a release beer!<br><br>How it\'s going: I\'m going to keep having beers until this release is out.',
    likes: 10,
    retweets: 1
  },
  {
    id: '1426276465717366786',
    created: 1628885783000,
    type: 'post',
    text: 'Frankly, we already have enough IDEs for writing code and don\'t need any more of them. Do something NEW!',
    likes: 9,
    retweets: 1
  },
  {
    id: '1426276235257126912',
    created: 1628885728000,
    type: 'post',
    text: 'I\'m so tired of reading about yet another IDE that completely ignores tooling for documentation writers. If your solution doesn\'t recognize AsciiDoc, Markdown, &amp; other formats, with reference checking, syntax highlighting &amp; validation, autocomplete, &amp; refactoring, it ain\'t ready.',
    likes: 27,
    retweets: 3
  },
  {
    id: '1426274513004630020',
    created: 1628885317000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cra" rel="noopener noreferrer" target="_blank">@cra</a> I\'d also like to see them stop ignoring documentation. Good tooling for documentation is just as important as good tooling for code. There\'s virtually no difference in the kinds of assistance that can be provided (refactoring, autocomplete, validation, preview, build, etc).<br><br>In reply to: <a href="https://x.com/cra/status/1425500804912386055" rel="noopener noreferrer" target="_blank">@cra</a> <span class="status">1425500804912386055</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1425772801542148108',
    created: 1628765700000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bethgriggs_" rel="noopener noreferrer" target="_blank">@bethgriggs_</a> <a class="mention" href="https://x.com/nodejs" rel="noopener noreferrer" target="_blank">@nodejs</a> I, for one, appreciate you and your work. As someone who spends double-digit hours getting out my own releases, I absolutely can relate to the effort involved.<br><br>In reply to: @BethGriggs_ <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1425554809776607232',
    created: 1628713726000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_gettalong" rel="noopener noreferrer" target="_blank">@_gettalong</a> <a class="mention" href="https://x.com/aried3r" rel="noopener noreferrer" target="_blank">@aried3r</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> ...and it\'s a pretty important step. Prawn produces less than optimal PDF documents, and only hexapdf really knows how to clean them up properly.<br><br>In reply to: <a href="https://x.com/_gettalong/status/1425154214737686535" rel="noopener noreferrer" target="_blank">@_gettalong</a> <span class="status">1425154214737686535</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1424832962894336000',
    created: 1628541625000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/toastal" rel="noopener noreferrer" target="_blank">@toastal</a> It\'s not the AsciiDoc render. It\'s their HTML sanitizer, which applies to all HTML in a GitHub repository, no matter how it\'s produced.<br><br>In reply to: <a href="https://x.com/toastal/status/1424691148228947979" rel="noopener noreferrer" target="_blank">@toastal</a> <span class="status">1424691148228947979</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1424832743523774465',
    created: 1628541572000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BrianLinuxing" rel="noopener noreferrer" target="_blank">@BrianLinuxing</a> <a class="mention" href="https://x.com/Raspberry_Pi" rel="noopener noreferrer" target="_blank">@Raspberry_Pi</a> This is the authority on the format: <a href="https://docs.asciidoctor.org/asciidoc/latest/" rel="noopener noreferrer" target="_blank">docs.asciidoctor.org/asciidoc/latest/</a> (<a href="http://asciidoc.org" rel="noopener noreferrer" target="_blank">asciidoc.org</a> is outdated and will soon be replaced)<br><br>In reply to: <a href="https://x.com/BrianLinuxing/status/1424734594616774656" rel="noopener noreferrer" target="_blank">@BrianLinuxing</a> <span class="status">1424734594616774656</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1423543070549708810',
    created: 1628234090000,
    type: 'reply',
    text: '@robintrietsch And running the latest alpha, I see!<br><br>In reply to: @robintrietsch <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1422829970422784008',
    created: 1628064074000,
    type: 'post',
    text: 'There were 3 major tasks holding up the switch of the default branch to 2.1. 1) verify compatibility between Antora &amp; Asciidoctor.js 2. 2) import the source of the default stylesheet into core. 3) format the code and enforce a code style. All 3 are now done. Let\'s go!',
    likes: 10,
    retweets: 0
  },
  {
    id: '1422828511803232258',
    created: 1628063726000,
    type: 'quote',
    text: 'I\'m excited to announce the release of Asciidoctor 2.0.16! While this is still just a patch release, it\'s important for two reasons. First, it fixes a major performance issue for Asciidoctor.js (and thus Antora). Second, it marks the beginning of work on 2.1 and beyond!<br><br>Quoting: <a href="https://x.com/asciidoctor/status/1422641753589354497" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <span class="status">1422641753589354497</span>',
    likes: 38,
    retweets: 7
  },
  {
    id: '1422644814550503424',
    created: 1628019929000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pbakker" rel="noopener noreferrer" target="_blank">@pbakker</a> That was me a few Olympics ago. I just got tired of fighting it. And that really pains me to say. The Olympics have been a HUGE part of my life and my relationship. But it just became more trouble than it was worth.<br><br>In reply to: <a href="https://x.com/pbakker/status/1422643336385597451" rel="noopener noreferrer" target="_blank">@pbakker</a> <span class="status">1422643336385597451</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1422642911187922945',
    created: 1628019476000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pbakker" rel="noopener noreferrer" target="_blank">@pbakker</a> That\'s one of many reasons I now boycott the Olympics. Access to it should be free and open for all the world to enjoy. And it\'s anything but.<br><br>In reply to: <a href="https://x.com/pbakker/status/1422642391119507457" rel="noopener noreferrer" target="_blank">@pbakker</a> <span class="status">1422642391119507457</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1422632390975901702',
    created: 1628016967000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MariaHealydj" rel="noopener noreferrer" target="_blank">@MariaHealydj</a> <a class="mention" href="https://x.com/NatureOne" rel="noopener noreferrer" target="_blank">@NatureOne</a> <a class="mention" href="https://x.com/PAULVANDYK" rel="noopener noreferrer" target="_blank">@PAULVANDYK</a> It\'s safe to say he adores that song...and so do we!<br><br>In reply to: <a href="https://x.com/MariaHealydj/status/1422620045927231492" rel="noopener noreferrer" target="_blank">@MariaHealydj</a> <span class="status">1422620045927231492</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1422480061316231174',
    created: 1627980649000,
    type: 'post',
    text: 'Users who lament over losing an *entire* day because of a bug often fail to realize the maintainer will spend several days, if not longer, trying to diagnose and solve the bug the person reported. We get it. Bugs can be frustrating. They\'re also an opportunity to learn.',
    likes: 14,
    retweets: 1
  },
  {
    id: '1422354702243569687',
    created: 1627950761000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/FredBrooker" rel="noopener noreferrer" target="_blank">@FredBrooker</a> <a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> We got really bitten by an incompatibility between Docker and Alpine Linux. But thanks to <a class="mention" href="https://x.com/DamienDuportal" rel="noopener noreferrer" target="_blank">@DamienDuportal</a>, we\'re back in action!<br><br>In reply to: <a href="https://x.com/FredBrooker/status/1422202930552348677" rel="noopener noreferrer" target="_blank">@FredBrooker</a> <span class="status">1422202930552348677</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1422351147331854350',
    created: 1627949914000,
    type: 'quote',
    text: 'Let me tell you. It\'s a long way around.<br><br>Quoting: <a href="https://x.com/ColoradoDOT/status/1421978755170410501" rel="noopener noreferrer" target="_blank">@ColoradoDOT</a> <span class="status">1421978755170410501</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1421796100349042690',
    created: 1627817580000,
    type: 'quote',
    text: 'How is this not a bigger story? Over 10 million Americans are at risk of being evicted in a pandemic which is raging on and a sitting member of Congress is protesting in front of the US Capitol to save her constituents. Pay attention.<br><br>Quoting: <a href="https://x.com/CoriBush/status/1421716065084641280" rel="noopener noreferrer" target="_blank">@CoriBush</a> <span class="status">1421716065084641280</span>',
    likes: 2,
    retweets: 2
  },
  {
    id: '1421359182922412037',
    created: 1627713411000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobbytank42" rel="noopener noreferrer" target="_blank">@bobbytank42</a> <a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> <a class="mention" href="https://x.com/j2r2b" rel="noopener noreferrer" target="_blank">@j2r2b</a> Since it\'s an old release, I think nothing more should really be done other than to make a note of it and move on.<br><br>In reply to: <a href="https://x.com/bobbytank42/status/1421358479017758720" rel="noopener noreferrer" target="_blank">@bobbytank42</a> <span class="status">1421358479017758720</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1421339187593187329',
    created: 1627708644000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/j2r2b" rel="noopener noreferrer" target="_blank">@j2r2b</a> Ah. I see. Looks like a bum release. Perhaps something worth adding a disclaimer for in the README for AsciidoctorJ...unless it can be republished.<br><br>In reply to: <a href="https://x.com/j2r2b/status/1421330847014207488" rel="noopener noreferrer" target="_blank">@j2r2b</a> <span class="status">1421330847014207488</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1421220001378234369',
    created: 1627680227000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/j2r2b" rel="noopener noreferrer" target="_blank">@j2r2b</a> It did get tagged. But I think that was the release that didn\'t succeed. It was around the time of the migration away from BinTray. Can you use a newer version?<br><br>In reply to: <a href="https://x.com/j2r2b/status/1421217217191063556" rel="noopener noreferrer" target="_blank">@j2r2b</a> <span class="status">1421217217191063556</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1421170950486204418',
    created: 1627668533000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/j2r2b" rel="noopener noreferrer" target="_blank">@j2r2b</a> Not according to <a href="https://search.maven.org/artifact/org.asciidoctor/asciidoctorj" rel="noopener noreferrer" target="_blank">search.maven.org/artifact/org.asciidoctor/asciidoctorj</a><br><br>In reply to: <a href="https://x.com/j2r2b/status/1421029908193685507" rel="noopener noreferrer" target="_blank">@j2r2b</a> <span class="status">1421029908193685507</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1421148983599132676',
    created: 1627663296000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sebi2706" rel="noopener noreferrer" target="_blank">@sebi2706</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/richsharples" rel="noopener noreferrer" target="_blank">@richsharples</a> <a class="mention" href="https://x.com/RedHat" rel="noopener noreferrer" target="_blank">@RedHat</a> <a class="mention" href="https://x.com/jbossorg" rel="noopener noreferrer" target="_blank">@jbossorg</a> <a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> I remember it like it was yesterday. For various reasons, it was a once in a lifetime experience.<br><br>In reply to: <a href="https://x.com/sebi2706/status/1421086534556688390" rel="noopener noreferrer" target="_blank">@sebi2706</a> <span class="status">1421086534556688390</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1421066012842029056',
    created: 1627643514000,
    type: 'quote',
    text: 'Finally!<br><br>Quoting: <a href="https://x.com/styfle/status/1420951279954763779" rel="noopener noreferrer" target="_blank">@styfle</a> <span class="status">1420951279954763779</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1420884326393073668',
    created: 1627600196000,
    type: 'post',
    text: 'Unpopular opinion but a CI build should run faster than a local build.',
    likes: 25,
    retweets: 1
  },
  {
    id: '1420875001629069313',
    created: 1627597973000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/Netlify" rel="noopener noreferrer" target="_blank">@Netlify</a> Cancelling a build is not the same thing as a failed build. Stop causing panic.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1420122186149289986',
    created: 1627418488000,
    type: 'reply',
    text: 'To give credit where credit is due, that someone is <a class="mention" href="https://x.com/javajudd" rel="noopener noreferrer" target="_blank">@javajudd</a>.<br><br>In reply to: <a href="#1419955785535160322">1419955785535160322</a>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1419976804593397764',
    created: 1627383826000,
    type: 'post',
    text: 'I discovered <a class="mention" href="https://x.com/Mile_Twelve" rel="noopener noreferrer" target="_blank">@Mile_Twelve</a> over the weekend. I think they\'re my new favorite bluegrass band. Their songs just get me.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1419955785535160322',
    created: 1627378815000,
    type: 'post',
    text: 'Someone pointed me to <a href="https://javaalmanac.io" rel="noopener noreferrer" target="_blank">javaalmanac.io</a> and now I wish every programming language / platform had something like that (Ruby and Node.js come to mind).',
    likes: 115,
    retweets: 41
  },
  {
    id: '1419603809425231874',
    created: 1627294897000,
    type: 'post',
    text: 'We\'ve had trouble growing plants on our balcony in CO due to huge variations in 🌡️ &amp; ⛈️. (The temp is particularly dynamic due to the hothouse the balcony creates). But what we\'ve found grows exceptionally well is kale. It even shows up randomly. We call that our side kale ;)',
    likes: 2,
    retweets: 0
  },
  {
    id: '1419558354960084992',
    created: 1627284060000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SparkleMom6" rel="noopener noreferrer" target="_blank">@SparkleMom6</a> <a class="mention" href="https://x.com/heyheydellamae" rel="noopener noreferrer" target="_blank">@heyheydellamae</a> 💯 agreed. That performance was 🔥.<br><br>In reply to: <a href="https://x.com/SparkleMom6/status/1419448661239554049" rel="noopener noreferrer" target="_blank">@SparkleMom6</a> <span class="status">1419448661239554049</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1419556559793709057',
    created: 1627283632000,
    type: 'post',
    text: '...and another incredible performance by the two of you and the rest of the crew on Sunday. Wow. 🏔️🌱',
    likes: 0,
    retweets: 0
  },
  {
    id: '1419077379348070402',
    created: 1627169387000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/sierrahull" rel="noopener noreferrer" target="_blank">@sierrahull</a> My spouse and I really enjoyed you and Justin\'s performance at RockyGrass over the livestream. You two are AMAZING! 🔥 Thank you!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1418702266777817089',
    created: 1627079953000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/1ovthafew" rel="noopener noreferrer" target="_blank">@1ovthafew</a> It was a pun. I expected it to be read as such. Obviously, no one knows exactly what the optimal number is. But it certainly isn\'t a perpetual rise with no limit in sight. ;)<br><br>In reply to: <a href="https://x.com/1ovthafew/status/1418701698630029314" rel="noopener noreferrer" target="_blank">@1ovthafew</a> <span class="status">1418701698630029314</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1418698488959823876',
    created: 1627079052000,
    type: 'post',
    text: 'If you do something good for the climate, and a climate activist celebrates that, it doesn\'t take the win away from you. You win and we all win too. It\'s not a zero sum game...except when it comes to how much the global temperature rises. That we do want to keep at 0.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1418656174774255617',
    created: 1627068964000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> In essence, it does (it\'s a glacial memory region). The reason seems to be to discipline memory usage. It\'s explained more here: <a href="https://github.com/nodejs/node/issues/29187" rel="noopener noreferrer" target="_blank">github.com/nodejs/node/issues/29187</a><br><br>In reply to: <a href="https://x.com/marcsavy/status/1418652902739042306" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1418652902739042306</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1418650037249134595',
    created: 1627067500000,
    type: 'post',
    text: 'By switching from a stack to a queue to iterate over lines, we may have solved the performance issue of Asciidoctor.js on Node.js 12 and above. The time to process a single file with 200,000 lines dropped from 4m to 4s. If this holds, I\'ll be beside myself.',
    likes: 42,
    retweets: 1
  },
  {
    id: '1416135477527158786',
    created: 1626467983000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matthewrevell" rel="noopener noreferrer" target="_blank">@matthewrevell</a> <a class="mention" href="https://x.com/rmoff" rel="noopener noreferrer" target="_blank">@rmoff</a> Hit me up here: <a href="https://opendevise.com/services/" rel="noopener noreferrer" target="_blank">opendevise.com/services/</a><br><br>In reply to: <a href="https://x.com/matthewrevell/status/1416015338328690697" rel="noopener noreferrer" target="_blank">@matthewrevell</a> <span class="status">1416015338328690697</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1415880202886909954',
    created: 1626407120000,
    type: 'post',
    text: '“If the struggle is real, then the success is real.”<br>— Joshua Joel Vela',
    likes: 3,
    retweets: 0
  },
  {
    id: '1415766849292148736',
    created: 1626380095000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/gitlab" rel="noopener noreferrer" target="_blank">@gitlab</a> Thank you for adding a redirect to the default branch after the master branch is deleted. That makes the transition so much smoother. Much appreciated!',
    likes: 6,
    retweets: 1
  },
  {
    id: '1415628972080058368',
    created: 1626347222000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/antonarhipov" rel="noopener noreferrer" target="_blank">@antonarhipov</a> <a class="mention" href="https://x.com/kittylyst" rel="noopener noreferrer" target="_blank">@kittylyst</a> Your best bet is the community chat at <a href="https://asciidoctor.zulipchat.com" rel="noopener noreferrer" target="_blank">asciidoctor.zulipchat.com</a>. Everyone working on Asciidoctor hangs out there, myself included.<br><br>In reply to: <a href="https://x.com/antonarhipov/status/1415620535719968769" rel="noopener noreferrer" target="_blank">@antonarhipov</a> <span class="status">1415620535719968769</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1415576763686477827',
    created: 1626334775000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MariaHealydj" rel="noopener noreferrer" target="_blank">@MariaHealydj</a> <a class="mention" href="https://x.com/DreamstateUSA" rel="noopener noreferrer" target="_blank">@DreamstateUSA</a> <a class="mention" href="https://x.com/asteroid_trance" rel="noopener noreferrer" target="_blank">@asteroid_trance</a> YES! I listened to it as soon as it landed on Insomniac. Your sound is just awesome. Keep crushing it!<br><br>In reply to: <a href="https://x.com/MariaHealydj/status/1410980347475603459" rel="noopener noreferrer" target="_blank">@MariaHealydj</a> <span class="status">1410980347475603459</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1415575468154056704',
    created: 1626334466000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AmelieLens" rel="noopener noreferrer" target="_blank">@AmelieLens</a> That set was massive! Thanks for sharing the stream!<br><br>In reply to: <a href="https://x.com/AmelieLens/status/1413280574723264512" rel="noopener noreferrer" target="_blank">@AmelieLens</a> <span class="status">1413280574723264512</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1414850974322085889',
    created: 1626161733000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/dailyburn" rel="noopener noreferrer" target="_blank">@dailyburn</a> I just want to share that I\'m loving the workouts led by Ellen Barrett. Of course, I love all the trainers equally. It\'s just that Ellen\'s sessions are really working for me right now. She\'s a great addition to the team!',
    likes: 1,
    retweets: 0
  },
  {
    id: '1414771598205231116',
    created: 1626142808000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ktosopl" rel="noopener noreferrer" target="_blank">@ktosopl</a> Good for you, my friend! I\'m so happy for you. I think I get where you\'re coming from. I threw out my back, then my shoulder...then the pandemic hit. I wasn\'t feeling great about my health. Since then, I\'ve been working out every week. I feel so much stronger &amp; happier. 💪😁<br><br>In reply to: <a href="https://x.com/ktosopl/status/1414114017170190337" rel="noopener noreferrer" target="_blank">@ktosopl</a> <span class="status">1414114017170190337</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1414741074216579076',
    created: 1626135531000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ninaturner" rel="noopener noreferrer" target="_blank">@ninaturner</a> “A lot of people think that unions are a referendum on working conditions, but for most of us, it\'s about making a good place to work a great place to work.”<br><br>👆<br><br>In reply to: <a href="#1414711951670734853">1414711951670734853</a>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1414713400614002689',
    created: 1626128933000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/scottdavis99" rel="noopener noreferrer" target="_blank">@scottdavis99</a> It\'s happening here too. Can\'t wait! 🍅<br><br>In reply to: <a href="https://x.com/scottdavis99/status/1414710800296517635" rel="noopener noreferrer" target="_blank">@scottdavis99</a> <span class="status">1414710800296517635</span>',
    photos: ['<div class="item"><img class="photo" src="media/1414713400614002689.jpg"></div>'],
    likes: 1,
    retweets: 0
  },
  {
    id: '1414711951670734853',
    created: 1626128588000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ninaturner" rel="noopener noreferrer" target="_blank">@ninaturner</a> Add an avid craft beer drinker, I stand in solidarity. ✊<br><br>In reply to: <a href="https://x.com/ninaturner/status/1414702717465874457" rel="noopener noreferrer" target="_blank">@ninaturner</a> <span class="status">1414702717465874457</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1414526108444237826',
    created: 1626084279000,
    type: 'post',
    text: 'Setting up an automated release takes ages. Never underestimate that. But it\'s worth every minute of it (in the long run).',
    likes: 17,
    retweets: 0
  },
  {
    id: '1414426290006892545',
    created: 1626060480000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasnell" rel="noopener noreferrer" target="_blank">@jasnell</a> I definitely appreciate the public service announcement, but also encourage you to be patient with those of us who are learning about this classification for the first time. In my career, legacy and deprecated have often been interchanged. So it just takes some adjustment.<br><br>In reply to: <a href="https://x.com/jasnell/status/1414075410229325826" rel="noopener noreferrer" target="_blank">@jasnell</a> <span class="status">1414075410229325826</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1413935514302881793',
    created: 1625943470000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/connolly_s" rel="noopener noreferrer" target="_blank">@connolly_s</a> I\'m not falling for PDF libraries that over promise anymore. I\'ve been down that road. There are dragons at every turn. We\'re betting on web technologies moving forward.<br><br>In reply to: <a href="https://x.com/connolly_s/status/1413862668922929159" rel="noopener noreferrer" target="_blank">@connolly_s</a> <span class="status">1413862668922929159</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1413439380039946241',
    created: 1625825183000,
    type: 'reply',
    text: '@patrickbkoetter <a class="mention" href="https://x.com/zulip" rel="noopener noreferrer" target="_blank">@zulip</a> Thanks! Wow, such amazing detail!<br><br>In reply to: @patrickbkoetter <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1413394745758404610',
    created: 1625814541000,
    type: 'reply',
    text: '@patrickbkoetter <a class="mention" href="https://x.com/zulip" rel="noopener noreferrer" target="_blank">@zulip</a> 👍<br><br>In reply to: @patrickbkoetter <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1413380428170371073',
    created: 1625811128000,
    type: 'reply',
    text: '@patrickbkoetter <a class="mention" href="https://x.com/zulip" rel="noopener noreferrer" target="_blank">@zulip</a> We have a tips stream in the Asciidoctor community. I encourage you to share this tip in that stream (using Zulip) so that other community members might benefit. I certainly want everyone to be able to participate in the way that works best for them.<br><br>In reply to: @patrickbkoetter <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1413369853470150659',
    created: 1625808606000,
    type: 'reply',
    text: '@patrickbkoetter <a class="mention" href="https://x.com/zulip" rel="noopener noreferrer" target="_blank">@zulip</a> What\'s most important, it changed completely how I interact with the community. I was getting very close to giving up on being able to manage communication until Zulip came along. It\'s been transformative. I encourage you to give the UI a chance.<br><br>In reply to: <a href="#1413369605251158016">1413369605251158016</a>',
    likes: 3,
    retweets: 1
  },
  {
    id: '1413369605251158016',
    created: 1625808547000,
    type: 'reply',
    text: '@patrickbkoetter <a class="mention" href="https://x.com/zulip" rel="noopener noreferrer" target="_blank">@zulip</a> The font size is a known issue that\'s being worked on. In the meantime, it can be fixed using zoom. See <a href="https://github.com/zulip/zulip/issues/12611" rel="noopener noreferrer" target="_blank">github.com/zulip/zulip/issues/12611</a> There\'s always going to those who don\'t like a UI for some reason. In this instance, nearly all our community members have responded positively to it.<br><br>In reply to: @patrickbkoetter <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1413272165273595905',
    created: 1625785316000,
    type: 'post',
    text: 'Someone just described <a class="mention" href="https://x.com/zulip" rel="noopener noreferrer" target="_blank">@zulip</a> as akin to newsgroups, and I couldn\'t agree more. Newsgroups were my gateway into learning about the world of open source. If Zulip is providing a similar experience, I think that\'s fantastic.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1413029428947406848',
    created: 1625727443000,
    type: 'reply',
    text: '@zregvart I agree that would be great, if it worked. It can\'t be used by the width attribute in CSS.<br><br>In reply to: @zregvart <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1412944173234212869',
    created: 1625707116000,
    type: 'post',
    text: 'I\'m leaning towards giving up on conformance (which is really quite pointless tbh) and just using the width attribute anyway.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1412943865716174852',
    created: 1625707043000,
    type: 'post',
    text: 'With the rise of CSP, the HTML spec group has *got* to do something about the width situation on table columns. Please take the deprecation off the width attribute! Otherwise, we\'re forced to create a CSS class for every possible distribution permutation, which is insane.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1412889219224150016',
    created: 1625694014000,
    type: 'post',
    text: 'Whenever I\'m cleaning spinach, kale, or the like for storage, I think of the heroes who so skillfully bunched it up in the field and what an important job they do. Thank you farm workers! I\'ll forever be in your corner. ✊',
    photos: ['<div class="item"><img class="photo" src="media/1412889219224150016.jpg"></div>'],
    likes: 3,
    retweets: 0
  },
  {
    id: '1412496603420643331',
    created: 1625600407000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/amywilesmusic" rel="noopener noreferrer" target="_blank">@amywilesmusic</a> As a fan and avid listener of trance, I support you. Having equal representation in the lineups is extremely important to me and I\'m using my voice to speak up about the problem. Organizers have no excuse for allowing it to be so unbalanced. My daily playlist of sets proves it.<br><br>In reply to: <a href="https://x.com/amywilesmusic/status/1412347727996989440" rel="noopener noreferrer" target="_blank">@amywilesmusic</a> <span class="status">1412347727996989440</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1412141409939836932',
    created: 1625515723000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/realpestano" rel="noopener noreferrer" target="_blank">@realpestano</a> I think what he\'s suggesting is whether a true return value keeps or discards an item. In other words, I think the confusion comes from which way the filter is facing. The fact that items are being removed is already the clear part.<br><br>In reply to: <a href="https://x.com/realpestano/status/1412130472780574721" rel="noopener noreferrer" target="_blank">@realpestano</a> <span class="status">1412130472780574721</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1412000536652509184',
    created: 1625482136000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cbeust" rel="noopener noreferrer" target="_blank">@cbeust</a> <a class="mention" href="https://x.com/BruceEckel" rel="noopener noreferrer" target="_blank">@BruceEckel</a> <a class="mention" href="https://x.com/HappyPathProg" rel="noopener noreferrer" target="_blank">@HappyPathProg</a> Same for JavaScript. I like Ruby\'s select as a name.<br><br>In reply to: <a href="https://x.com/cbeust/status/1411752541386285056" rel="noopener noreferrer" target="_blank">@cbeust</a> <span class="status">1411752541386285056</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1411996989705785354',
    created: 1625481290000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> Wow, what a birthday present! Happy Birthday and Happy Vaccination Day! I\'ll sleep better knowing you have the 🦠🛡️.<br><br>In reply to: <a href="https://x.com/alexsotob/status/1411986787577348096" rel="noopener noreferrer" target="_blank">@alexsotob</a> <span class="status">1411986787577348096</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1411640083745165317',
    created: 1625396197000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/eddiejaoude" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> <a class="mention" href="https://x.com/github" rel="noopener noreferrer" target="_blank">@github</a> This is a much, much, much better user experience than templates. Bravo to GitHub for adding this. It will make the process of submitting issues much more pleasant for the submitter and result in better reports for the maintainer. A win win.<br><br>In reply to: <a href="https://x.com/eddiejaoude/status/1411638525355778050" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> <span class="status">1411638525355778050</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1411596699177406464',
    created: 1625385853000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ilopmar" rel="noopener noreferrer" target="_blank">@ilopmar</a> <a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> Since I may have caused confusion with my first reply, I want to follow up. Asciidoctor itself doesn\'t log invalid image references (it doesn\'t check for them). That\'s probably why you aren\'t getting the result you expect. If you set the `data-uri` attribute, you can force it.<br><br>In reply to: <a href="https://x.com/ilopmar/status/1410890003765465088" rel="noopener noreferrer" target="_blank">@ilopmar</a> <span class="status">1410890003765465088</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1411534428459835392',
    created: 1625371007000,
    type: 'post',
    text: 'While my spouse pollinates our indoor tomato plants, she sings, "I wanna sex you up!" 🤣',
    likes: 6,
    retweets: 0
  },
  {
    id: '1411524021695356930',
    created: 1625368526000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AngryBlackLady" rel="noopener noreferrer" target="_blank">@AngryBlackLady</a> I had that exact same reaction when I moved to Colorado almost a decade ago, and I have great news for you. I still have that feeling every single day. Hopefully you will too. Welcome!! 🏔️<br><br>In reply to: <a href="https://x.com/AngryBlackLady/status/1411293960425910272" rel="noopener noreferrer" target="_blank">@AngryBlackLady</a> <span class="status">1411293960425910272</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1411289339569446921',
    created: 1625312573000,
    type: 'quote',
    text: 'Don\'t just listen to this address and react. Listen to it and think. Then act. There\'s no vaccine coming to save the climate (or our ability to survive in it).<br><br>Quoting: <a href="https://x.com/GretaThunberg/status/1411017496212168706" rel="noopener noreferrer" target="_blank">@GretaThunberg</a> <span class="status">1411017496212168706</span>',
    likes: 5,
    retweets: 3
  },
  {
    id: '1411104470927216647',
    created: 1625268497000,
    type: 'quote',
    text: 'We know what\'s going on. They\'re destroying our planet for short term profits. That\'s the whole story.<br><br>Quoting: <a href="https://x.com/ryangrim/status/1411102082082480128" rel="noopener noreferrer" target="_blank">@ryangrim</a> <span class="status">1411102082082480128</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1410878908312932356',
    created: 1625214719000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ilopmar" rel="noopener noreferrer" target="_blank">@ilopmar</a> <a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> OMG, I just realized I answered the question about Antora. My mistake. Though the design of the log failure mechanism in Antora is inspired by the Asciidoctor Gradle plugin.<br><br>In reply to: <a href="#1410878583824785409">1410878583824785409</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1410878583824785409',
    created: 1625214641000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ilopmar" rel="noopener noreferrer" target="_blank">@ilopmar</a> <a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> It\'s planned for Antora 3.0. We now log an error if an xref cannot be found. The next step is to apply that to images as well, since we have everything we need to do it. <a href="https://gitlab.com/antora/antora/-/issues/800" rel="noopener noreferrer" target="_blank">gitlab.com/antora/antora/-/issues/800</a><br><br>In reply to: <a href="https://x.com/ilopmar/status/1410874590394568705" rel="noopener noreferrer" target="_blank">@ilopmar</a> <span class="status">1410874590394568705</span>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1410694469091926016',
    created: 1625170745000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CityofLoneTree" rel="noopener noreferrer" target="_blank">@CityofLoneTree</a> Does the milling process reuse the existing road surface to produce the material being used to pave the road?<br><br>In reply to: <a href="https://x.com/CityofLoneTree/status/1410687120323325952" rel="noopener noreferrer" target="_blank">@CityofLoneTree</a> <span class="status">1410687120323325952</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1410559954545790978',
    created: 1625138674000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sentreh" rel="noopener noreferrer" target="_blank">@sentreh</a> I know the feeling.<br><br>In reply to: <a href="https://x.com/sentreh/status/1410551727107887107" rel="noopener noreferrer" target="_blank">@sentreh</a> <span class="status">1410551727107887107</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1410550889320914948',
    created: 1625136513000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sentreh" rel="noopener noreferrer" target="_blank">@sentreh</a> Thanks for the insight! I think I now have a grasp on how these policies are applied.<br><br>In reply to: <a href="https://x.com/sentreh/status/1410550050913013770" rel="noopener noreferrer" target="_blank">@sentreh</a> <span class="status">1410550050913013770</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1410550224657911814',
    created: 1625136354000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sentreh" rel="noopener noreferrer" target="_blank">@sentreh</a> ...but that at least solves my problem with unsafe-inline.<br><br>In reply to: <a href="#1410550115601850371">1410550115601850371</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1410550115601850371',
    created: 1625136328000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sentreh" rel="noopener noreferrer" target="_blank">@sentreh</a> Shoot! It only works for unsafe-inline. I guess the best thing to do is figure out how to get rid of this eval code in the script...because I realize that\'s the really problematic part.<br><br>In reply to: <a href="https://x.com/sentreh/status/1410549087837974535" rel="noopener noreferrer" target="_blank">@sentreh</a> <span class="status">1410549087837974535</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1410548070194974720',
    created: 1625135841000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sentreh" rel="noopener noreferrer" target="_blank">@sentreh</a> What I think I can do is create a nonce or hash entry for the script I want to allow it on, which is probably better anyway. Unfortunately, only CSP 3.0 supports that for external scripts...but it seems like most modern browsers are there.<br><br>In reply to: <a href="https://x.com/sentreh/status/1410543211676782601" rel="noopener noreferrer" target="_blank">@sentreh</a> <span class="status">1410543211676782601</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1410541960364924936',
    created: 1625134384000,
    type: 'post',
    text: 'Is it possible to apply a different CSP for a specific domain? I need to enable unsafe-eval for the site\'s own scripts (\'self\'), but not for scripts loaded from elsewhere.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1410497361827090434',
    created: 1625123751000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bk2204" rel="noopener noreferrer" target="_blank">@bk2204</a> And by "now", I mean once I actually manage to implement it ;)<br><br>In reply to: <a href="#1410341474961485829">1410341474961485829</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1410344448903376901',
    created: 1625087294000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Det_Conan_Kudo" rel="noopener noreferrer" target="_blank">@Det_Conan_Kudo</a> <a class="mention" href="https://x.com/fedora" rel="noopener noreferrer" target="_blank">@fedora</a> Very interesting. Thanks for sharing that insight.<br><br>It seems like the real breakthrough was made when the repository managers (GitHub, GitLab, Bitbucket, etc) started supporting it transparently. That made it accessible.<br><br>In reply to: <a href="https://x.com/Det_Conan_Kudo/status/1410335334773182466" rel="noopener noreferrer" target="_blank">@Det_Conan_Kudo</a> <span class="status">1410335334773182466</span>',
    likes: 0,
    retweets: 1
  },
  {
    id: '1410343799499366400',
    created: 1625087139000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Kiview" rel="noopener noreferrer" target="_blank">@Kiview</a> As a maintainer of said git tooling, I know what you mean ;) I\'m in the process of figuring out how to add that support to isomorphic-git and/or Antora.<br><br>In reply to: <a href="https://x.com/Kiview/status/1410336500219265024" rel="noopener noreferrer" target="_blank">@Kiview</a> <span class="status">1410336500219265024</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1410341474961485829',
    created: 1625086585000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bk2204" rel="noopener noreferrer" target="_blank">@bk2204</a> I\'m really excited to have unlocked this in my head because git-lfs really is a good fit for the use case that Antora presents. It\'s very common to see large files in a documentation repository. Now we should be able to pass them through without a lot of overhead.<br><br>In reply to: <a href="#1410340991828041729">1410340991828041729</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1410340991828041729',
    created: 1625086469000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bk2204" rel="noopener noreferrer" target="_blank">@bk2204</a> Thanks! I really appreciate it.<br><br>One key mistake I made was missing the API reference link on <a href="https://git-lfs.github.com/" rel="noopener noreferrer" target="_blank">git-lfs.github.com/</a>. I think that\'s because it\'s under the heading "Git LFS is an open source project", which threw me off. One bit of advice would be to use a more accurate heading.<br><br>In reply to: <a href="https://x.com/bk2204/status/1410335427073122306" rel="noopener noreferrer" target="_blank">@bk2204</a> <span class="status">1410335427073122306</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1410333987654881280',
    created: 1625084800000,
    type: 'post',
    text: 'I\'m happy to say it\'s no longer a mystery to me. It\'s actually a lot simpler than I realized. git stores an info file in place of the large file. That info file contains an oid that can be resolved (by the lfs service) to a download URL. That\'s actually quite elegant.<br><br>Quoting: <a href="#1410177522570956803">1410177522570956803</a>',
    likes: 9,
    retweets: 0
  },
  {
    id: '1410333519990038530',
    created: 1625084688000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bk2204" rel="noopener noreferrer" target="_blank">@bk2204</a> Got it. Now it makes sense.<br><br>In reply to: <a href="https://x.com/bk2204/status/1410216815494668290" rel="noopener noreferrer" target="_blank">@bk2204</a> <span class="status">1410216815494668290</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1410287887308902402',
    created: 1625073808000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pvaneeckhoudt" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> I must have done something wrong because the files ended up in the git repository on GitHub. Time for take 2.<br><br>In reply to: <a href="https://x.com/pvaneeckhoudt/status/1410200650844155905" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> <span class="status">1410200650844155905</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1410192262458318853',
    created: 1625051010000,
    type: 'post',
    text: 'I guess my main question is does this really save space and network? The files are still in (or at least with) the repository. Is it just making the pack files smaller? Sharding the repo? Isn\'t it better just to put large files in a binary repository?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1410191128649818117',
    created: 1625050739000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> I wasn\'t aware of partial clones. That looks interesting, though would require support from the git client I\'m using. I wonder if it actually reduces files that come over the wire, or whether it\'s more like a bare repo.<br><br>In reply to: <a href="#1410188053570756617">1410188053570756617</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1410189333445832707',
    created: 1625050311000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kennybastani" rel="noopener noreferrer" target="_blank">@kennybastani</a> Thanks! Having an example is very helpful. Question, does a client like jgit need to know about git lfs? Or does cloning and fetching just work?<br><br>In reply to: <a href="https://x.com/kennybastani/status/1410180887279509505" rel="noopener noreferrer" target="_blank">@kennybastani</a> <span class="status">1410180887279509505</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1410188053570756617',
    created: 1625050006000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> It\'s more that I have to support users who have git lfs enabled on their repositories, which is why I need to understand it. Antora is currently using shallow cloning, which I agree really slims things down.<br><br>In reply to: <a href="https://x.com/garrett/status/1410186944781422594" rel="noopener noreferrer" target="_blank">@garrett</a> <span class="status">1410186944781422594</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1410177522570956803',
    created: 1625047495000,
    type: 'post',
    text: 'git lfs is still a complete mystery to me. Anyone have any good tutorials to recommend that will help demystify it for me?',
    likes: 2,
    retweets: 0
  },
  {
    id: '1409974383548895234',
    created: 1624999063000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> Hmm. What I really need though is on.schedule.if. While I could stop the job if scheduled and not upstream, that still runs the job and eventually sends the user a notification that the scheduled job is being decommissioned. I think we really do need a switch in the UI.<br><br>In reply to: <a href="#1409973588791234561">1409973588791234561</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1409973588791234561',
    created: 1624998874000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> I should at least add that in the repositories that I control. Thanks for the hint!<br><br>In reply to: <a href="https://x.com/marcsavy/status/1409971616017043456" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1409971616017043456</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1409968532788449281',
    created: 1624997668000,
    type: 'post',
    text: 'It seems to me that GitHub Actions should ask if you want scheduled jobs to run on a fork. I find it odd when I fork a repo, then start getting failure reports from scheduled jobs running on that fork. It\'s not only confusing, but a waste of resources.',
    likes: 13,
    retweets: 1
  },
  {
    id: '1409702273400201217',
    created: 1624934187000,
    type: 'post',
    text: 'When we made the switch to <a class="mention" href="https://x.com/zulip" rel="noopener noreferrer" target="_blank">@zulip</a> a few months ago for chat, never in my wildest dreams did I imagine it was going to become the beating heart of the community, and so quickly. It\'s a game changer. 🧑‍💻🗨️👩‍💻',
    likes: 20,
    retweets: 3
  },
  {
    id: '1409621066692009986',
    created: 1624914826000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JenaGriswold" rel="noopener noreferrer" target="_blank">@JenaGriswold</a> Nice! This is the only sane way to deal with the problem of candidates winning because the votes they didn\'t earn got split.<br><br>In reply to: <a href="https://x.com/JenaGriswold/status/1409617815326367749" rel="noopener noreferrer" target="_blank">@JenaGriswold</a> <span class="status">1409617815326367749</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1409607768500101122',
    created: 1624911655000,
    type: 'reply',
    text: '@patrickbkoetter That\'s a good suggestion. I\'ve actually been giving it a try this time around to see how well it plays.<br><br>In reply to: @patrickbkoetter <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1409597445118390273',
    created: 1624909194000,
    type: 'post',
    text: 'It also hinges on your ability to be persuasive in a debate, and on that particular day. Honestly, it\'s one of the hardest things to do in Open Source.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1409595645883682822',
    created: 1624908765000,
    type: 'post',
    text: 'What no one often talks about regarding submitting changes upstream is the fight you have to endure to get that proposal accepted. (It\'s often a necessary one, but it does take time, patience, and energy...and a lot of it).',
    likes: 6,
    retweets: 0
  },
  {
    id: '1409576392652562472',
    created: 1624904175000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/plaindocs" rel="noopener noreferrer" target="_blank">@plaindocs</a> There are a few apps that doesn\'t scale by default, but usually can be forced to using an environment variable like QT_SCALE_FACTOR (such as Zoom). But it\'s those apps that are dragging their feet.<br><br>In reply to: <a href="https://x.com/plaindocs/status/1409556561152856069" rel="noopener noreferrer" target="_blank">@plaindocs</a> <span class="status">1409556561152856069</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1409551033005600775',
    created: 1624898129000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/plaindocs" rel="noopener noreferrer" target="_blank">@plaindocs</a> In my experience, I disagree. I think Linux handles HiDPI better than any other OS. The display is incredibly sharp and scales just as I world expect. My experience is with Fedora.<br><br>In reply to: <a href="https://x.com/plaindocs/status/1409470995287707651" rel="noopener noreferrer" target="_blank">@plaindocs</a> <span class="status">1409470995287707651</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1409019934093447174',
    created: 1624771505000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agentdero" rel="noopener noreferrer" target="_blank">@agentdero</a> I\'ve been saying all day, "California, I think we found your water." It\'s ridiculous how much rain and cool air we\'re getting in Denver for this time of the year. But it\'s coming down so hard it would probably drown crops. Our peppers are hurting.<br><br>In reply to: <a href="https://x.com/agentdero/status/1408942682374762497" rel="noopener noreferrer" target="_blank">@agentdero</a> <span class="status">1408942682374762497</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1408709934464729089',
    created: 1624697595000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> Thanks for those kind words and for spreading the word, Marc! That really means a lot to me and I really appreciate it. I\'m all fired up now to apply another round of improvements. This story is just getting started.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1407721956229763080" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1407721956229763080</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1407088070353059840',
    created: 1624310913000,
    type: 'post',
    text: 'One bad practice I\'m observing is that when someone decides they need help with something, they pepper every channel they can find with the same question in the course of a few minutes. Don\'t do that (and don\'t tolerate it if someone else does it).',
    likes: 11,
    retweets: 0
  },
  {
    id: '1406754204291534852',
    created: 1624231313000,
    type: 'post',
    text: 'I\'m most proud of my father for not only coming out as transgender, but for advocating for others who are on the same path, and for continuing to nurture those in need. To me, parenting is about showing your children how to love. 🏳️‍🌈 <a href="https://columns.wlu.edu/nurturing-the-nasdaq/" rel="noopener noreferrer" target="_blank">columns.wlu.edu/nurturing-the-nasdaq/</a>',
    likes: 16,
    retweets: 0
  },
  {
    id: '1406751318799126528',
    created: 1624230625000,
    type: 'post',
    text: 'My brother is an awesome dad, and I\'m very proud of him for that.',
    photos: ['<div class="item"><img class="photo" src="media/1406751318799126528.jpg"></div>'],
    likes: 5,
    retweets: 0
  },
  {
    id: '1405691451418189824',
    created: 1623977933000,
    type: 'post',
    text: 'I guess not.',
    photos: ['<div class="item"><img class="photo" src="media/1405691451418189824.png"></div>'],
    likes: 6,
    retweets: 0
  },
  {
    id: '1405414651731402754',
    created: 1623911938000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nigjo" rel="noopener noreferrer" target="_blank">@nigjo</a> Below you can find an example of an include processor that preprocesses the content, then delegates to the real include processor to handle the processing of lines and tags. It\'s a bit of a hack, but leaves me with ideas for how to improve the situation. <a href="https://github.com/asciidoctor/asciidoctor-extensions-lab/blob/master/lib/pre-include-processor/extension.rb" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-extensions-lab/blob/master/lib/pre-include-processor/extension.rb</a><br><br>In reply to: <a href="#1405262646387953668">1405262646387953668</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1405262646387953668',
    created: 1623875698000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nigjo" rel="noopener noreferrer" target="_blank">@nigjo</a> It is possible to delegate back to the built-in include processor. If I have time today, I\'ll add an example to the lab that shows how.<br><br>In reply to: <a href="https://x.com/nigjo/status/1405259920040251399" rel="noopener noreferrer" target="_blank">@nigjo</a> <span class="status">1405259920040251399</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1405262531497644034',
    created: 1623875670000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nigjo" rel="noopener noreferrer" target="_blank">@nigjo</a> Then that\'s where the include processor extension would come in. Here\'s a basic example: <a href="https://github.com/asciidoctor/asciidoctor-extensions-lab/blob/master/lib/uri-include-processor/extension.rb" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-extensions-lab/blob/master/lib/uri-include-processor/extension.rb</a> (which happens to emulate a feature already supported by Asciidoctor).<br><br>In reply to: <a href="https://x.com/nigjo/status/1405259920040251399" rel="noopener noreferrer" target="_blank">@nigjo</a> <span class="status">1405259920040251399</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1405257991788457988',
    created: 1623874588000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nigjo" rel="noopener noreferrer" target="_blank">@nigjo</a> You can trim the indentation by setting indent=0 on the include directive. For any other type of prefix, it would require using an include processor extension.<br><br>In reply to: <a href="https://x.com/nigjo/status/1405164541093285891" rel="noopener noreferrer" target="_blank">@nigjo</a> <span class="status">1405164541093285891</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1404151937214214144',
    created: 1623610884000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agentdero" rel="noopener noreferrer" target="_blank">@agentdero</a> So true about other uses as well. My point is that it\'s a fact that meat production (end to end) uses far more water than growing veg. And eating meat is a major problem both for the environment &amp; animal rights, so we all really should care. But that\'s a separate discussion.<br><br>In reply to: <a href="https://x.com/agentdero/status/1404149983796858881" rel="noopener noreferrer" target="_blank">@agentdero</a> <span class="status">1404149983796858881</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1404148256636948481',
    created: 1623610006000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agentdero" rel="noopener noreferrer" target="_blank">@agentdero</a> And I\'m sure they went home and ate meat like the hypocrites they are.<br><br>In reply to: <a href="https://x.com/agentdero/status/1404110404146532355" rel="noopener noreferrer" target="_blank">@agentdero</a> <span class="status">1404110404146532355</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1403660997240365061',
    created: 1623493835000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pvaneeckhoudt" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> I could set the version lower than what anyone would package. But then the information on RubyGems is very misleading. Better to have no version specified than an arbitrary one.<br><br>In reply to: <a href="#1403660261202939908">1403660261202939908</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1403660261202939908',
    created: 1623493659000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pvaneeckhoudt" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> I\'ve received feedback from packagers that the answer to that question is no. It requires patching RubyGems in the build, but not in what\'s shipped. So it\'s an untenable situation for them.<br><br>In reply to: <a href="https://x.com/pvaneeckhoudt/status/1403652057933004806" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> <span class="status">1403652057933004806</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1403614179324354561',
    created: 1623482672000,
    type: 'post',
    text: 'RubyGems does not allow the minimum runtime version declared by a gem to be overridden.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1403614068309594114',
    created: 1623482646000,
    type: 'post',
    text: 'I refuse to set the minimum runtime version on the libraries I publish unless the packaging system allows it to be overridden. Otherwise, it creates an untenable situation for system packagers, who have every right to maintain patches to use the library on an older runtime.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1403558278261461001',
    created: 1623469345000,
    type: 'post',
    text: 'Having a listening party for No Gods, No Masters by <a class="mention" href="https://x.com/garbage" rel="noopener noreferrer" target="_blank">@garbage</a> while enjoying one of my favorite brews and celebrating Friday. YEAS! Pretty surreal that my favorite band has put out such a piercing masterpiece. Most pertinent lyric might be, "Make it a crime to tell a lie again."',
    photos: ['<div class="item"><img class="photo" src="media/1403558278261461001.jpg"></div>', '<div class="item"><img class="photo" src="media/1403558278261461001-2.jpg"></div>'],
    likes: 1,
    retweets: 0
  },
  {
    id: '1403509093692706818',
    created: 1623457618000,
    type: 'post',
    text: 'I managed to get something working that I didn\'t think would be possible by the end of the week. I\'m going to call that a win and go celebrate. 🍺',
    likes: 12,
    retweets: 0
  },
  {
    id: '1403430117531996163',
    created: 1623438789000,
    type: 'post',
    text: 'But I can live with having to adjust two settings.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1403430044744122370',
    created: 1623438771000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> I\'m happy to report scrolling w/ mouse wheel works nearly out of the box in Firefox 89. I still have to activate these settings:<br><br>mousewheel.system_scroll_override_on_root_content.enabled;true<br>general.smoothScroll.msdPhysics.enabled;true<br><br>Otherwise, fling doesn\'t work.<br><br>Quoting: <a href="#1349098275899469824">1349098275899469824</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1403265295439962112',
    created: 1623399492000,
    type: 'reply',
    text: 'If you\'re a white, cis, male artist performing at <a class="mention" href="https://x.com/tomorrowland" rel="noopener noreferrer" target="_blank">@tomorrowland</a> and you aren\'t speaking up about this, you are part of the problem.<br><br>In reply to: <a href="#1403265023548354563">1403265023548354563</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1403265023548354563',
    created: 1623399427000,
    type: 'post',
    text: 'Just so it\'s abundantly clear, <a class="mention" href="https://x.com/tomorrowland" rel="noopener noreferrer" target="_blank">@tomorrowland</a> does not care about trying to fix this problem one iota. The responses I\'ve received from them range from "women weren\'t available" to "can you give us a list of artists?" to "we had women in our 2020 program" (which they hardly did).<br><br>Quoting: <a href="#1397142014387310593">1397142014387310593</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1403261596806225921',
    created: 1623398610000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pvaneeckhoudt" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> And I do appreciate your support. 100%<br><br>In reply to: <a href="https://x.com/pvaneeckhoudt/status/1403259762586816514" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> <span class="status">1403259762586816514</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1403261067824828419',
    created: 1623398484000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pvaneeckhoudt" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> I realize you were just kidding. I\'ve just been in a very serious mode about it all ;)<br><br>In reply to: <a href="https://x.com/pvaneeckhoudt/status/1403259762586816514" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> <span class="status">1403259762586816514</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1403122553107800064',
    created: 1623365460000,
    type: 'post',
    text: 'It\'s the antithesis of the "upstream first" mantra. And from someone with a Principal title no less.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1403119651459010561',
    created: 1623364768000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pvaneeckhoudt" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> I don\'t really wish to shame anyone out of spite. I\'d rather build bridges. I\'m just not going to put up with it anymore.<br><br>In reply to: <a href="https://x.com/pvaneeckhoudt/status/1403034990586339330" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> <span class="status">1403034990586339330</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1403119287296880642',
    created: 1623364681000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Of course ;)<br><br>Joking aside, you\'re a real one.<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/1403116305172078599" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">1403116305172078599</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1403115847464280065',
    created: 1623363861000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> I was not referring to you. I\'m happy to see your progress on the jbang site. It\'s exciting!<br><br>In reply to: <a href="https://x.com/maxandersen/status/1402952249446481924" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1402952249446481924</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1402941392066805763',
    created: 1623322267000,
    type: 'post',
    text: '"Looks like I\'ll be looking at other technologies" over something that\'s absolutely trivial and so not a showstopper. It\'s childish.',
    likes: 15,
    retweets: 0
  },
  {
    id: '1402940552576847872',
    created: 1623322067000,
    type: 'post',
    text: 'Another person shaming me into implementing a feature, this time a person from Red Hat. Honestly, I expect better from Red Hat.',
    likes: 16,
    retweets: 2
  },
  {
    id: '1402780386422181890',
    created: 1623283881000,
    type: 'reply',
    text: '@DJ____DAVE <a class="mention" href="https://x.com/samaaron" rel="noopener noreferrer" target="_blank">@samaaron</a> <a class="mention" href="https://x.com/Sonic_Pi" rel="noopener noreferrer" target="_blank">@Sonic_Pi</a> Wow, that was incredible! I can\'t get my jaw off the floor. I watched your other live set afterwards and was entranced. Look out top DJs!<br><br>In reply to: <a href="https://x.com/dj_dave____/status/1396329174084866050" rel="noopener noreferrer" target="_blank">@dj_dave____</a> <span class="status">1396329174084866050</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1402745628849889280',
    created: 1623275594000,
    type: 'post',
    text: 'I wish npm would only install the newest version it can that still honors the value of the engine field. That would allow specifying newer dependency versions for new Node.js versions while not breaking it for older Node.js versions.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1402221084615741447',
    created: 1623150533000,
    type: 'post',
    text: 'I did not know that Docker / OCI images had to be made per the architecture they are run on. 🤔',
    likes: 3,
    retweets: 0
  },
  {
    id: '1402180181792464899',
    created: 1623140781000,
    type: 'post',
    text: 'To be more specific, I really love that techno spin you put on trace. You really get my hips moving. And I can\'t believe you did a 4 hour set! That\'s some incredible creative stamina. Let\'s go!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1402042971197870103',
    created: 1623108067000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/MariaHealydj" rel="noopener noreferrer" target="_blank">@MariaHealydj</a> Wow! Now I\'m listening to your set from Transmission back in October. It\'s different, fascinating, and powerful. The range of your sound continues to impress. YES! I\'m definitely a fan. 🙌',
    likes: 0,
    retweets: 0
  },
  {
    id: '1401840142487146499',
    created: 1623059709000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mp911de" rel="noopener noreferrer" target="_blank">@mp911de</a> <a class="mention" href="https://x.com/MariaHealydj" rel="noopener noreferrer" target="_blank">@MariaHealydj</a> <a class="mention" href="https://x.com/MissCNovelli" rel="noopener noreferrer" target="_blank">@MissCNovelli</a> I agree with that observation. I really got the sense of a PVD 2.0 when listening to Maria\'s set...which is great because the world needs more of the PVD sound.<br><br>In reply to: <a href="https://x.com/mp911de/status/1401837050249334786" rel="noopener noreferrer" target="_blank">@mp911de</a> <span class="status">1401837050249334786</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1401825138635542531',
    created: 1623056132000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> That reminds me of a story. For whatever reason, Santa Barbara has no bugs. When we lived there for post-grad, we used to leave door wide open all day. One evening, while watching a movie, a racoon came in, did a lap around the apartment, then left. We were like 👀👀😱<br><br>In reply to: <a href="https://x.com/marcsavy/status/1401821125273919491" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1401821125273919491</span>',
    likes: 5,
    retweets: 1
  },
  {
    id: '1401823900078534658',
    created: 1623055837000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/mp911de" rel="noopener noreferrer" target="_blank">@mp911de</a> Listened to <a class="mention" href="https://x.com/MariaHealydj" rel="noopener noreferrer" target="_blank">@MariaHealydj</a> and <a class="mention" href="https://x.com/MissCNovelli" rel="noopener noreferrer" target="_blank">@MissCNovelli</a> all day yesterday. It was a perfect Sunday trance. Amazing. Thanks for the recommendations!',
    likes: 3,
    retweets: 0
  },
  {
    id: '1401692038471438337',
    created: 1623024398000,
    type: 'quote',
    text: 'As a maintainer of Ruby-based software, I totally agree.<br><br>Quoting: <a href="https://x.com/postmodern_mod3/status/1401488638689431552" rel="noopener noreferrer" target="_blank">@postmodern_mod3</a> <span class="status">1401488638689431552</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1401607572918992896',
    created: 1623004260000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/royvanrijn" rel="noopener noreferrer" target="_blank">@royvanrijn</a> I feel like this should be the new <a class="mention" href="https://x.com/Glitterbox" rel="noopener noreferrer" target="_blank">@Glitterbox</a> motto.<br><br>In reply to: <a href="https://x.com/royvanrijn/status/1401429028398153731" rel="noopener noreferrer" target="_blank">@royvanrijn</a> <span class="status">1401429028398153731</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1401498963396595713',
    created: 1622978366000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AmelieLens" rel="noopener noreferrer" target="_blank">@AmelieLens</a> My spouse and I have become obsessed with HÖR. It\'s so raw and always surprising. We were just imagining seeing you on the decks there. You\'d absolutely smash it. You should do it!<br><br>In reply to: <a href="https://x.com/AmelieLens/status/1400161095235608576" rel="noopener noreferrer" target="_blank">@AmelieLens</a> <span class="status">1400161095235608576</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1401443340067872770',
    created: 1622965104000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ellispratt" rel="noopener noreferrer" target="_blank">@ellispratt</a> I want to get more into curries, but my partner needs some more convincing ;) Sounds delicious though!<br><br>In reply to: <a href="https://x.com/ellispratt/status/1401435817910939648" rel="noopener noreferrer" target="_blank">@ellispratt</a> <span class="status">1401435817910939648</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1401427955457216516',
    created: 1622961436000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ellispratt" rel="noopener noreferrer" target="_blank">@ellispratt</a> First one, yes (though only recently). Absolutely essential for managing recipes that call for beans. I\'ve never heard of the second one. Since I\'m vegan, I rarely need to grill. But I do have a Cuisinart Steam &amp; Convection Countertop Oven. Essential.<br><br>In reply to: <a href="https://x.com/ellispratt/status/1401416887028883456" rel="noopener noreferrer" target="_blank">@ellispratt</a> <span class="status">1401416887028883456</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1401347934210322435',
    created: 1622942357000,
    type: 'post',
    text: 'I used to rely on restaurants daily in order to get the food I needed. I have since learned how to make food (including dessert!) for my household every single day (in cooperation w/ my partner). I cannot tell you what a sense of freedom that\'s given me.',
    likes: 13,
    retweets: 0
  },
  {
    id: '1401090586170568710',
    created: 1622881001000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mp911de" rel="noopener noreferrer" target="_blank">@mp911de</a> If you aren\'t yet familiar with Davidi, I highly recommend checking out this set. I think you might find her trace sound to be right up your alley (based on your prior recommendations). <a href="https://youtu.be/oZduYV1oyyo" rel="noopener noreferrer" target="_blank">youtu.be/oZduYV1oyyo</a><br><br>In reply to: <a href="#1397296510711197700">1397296510711197700</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1401077075742625792',
    created: 1622877780000,
    type: 'post',
    text: 'I went to the store for the first time since getting the vaccine (still wearing a mask). Generally, it went smoothly. But I have a tip to share from the experience. Don\'t come up behind someone and just start talking to them. I almost jumped into the next aisle.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1400727059408703492',
    created: 1622794329000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bentolor" rel="noopener noreferrer" target="_blank">@bentolor</a> I think the solution is even simpler. Know your limits and tell people either "no" or "be part of the solution". I don\'t buy into the argument that OSS is harder to make sustainable than other professional work. But I do think people fall into the trap of pleasing the passerby.<br><br>In reply to: <a href="https://x.com/bentolor/status/1400723275416748032" rel="noopener noreferrer" target="_blank">@bentolor</a> <span class="status">1400723275416748032</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1400720228066074625',
    created: 1622792701000,
    type: 'post',
    text: 'Every so often, I get a passerby trying to peddle the case "if you don\'t implement this change, people won\'t adopt your software and it won\'t be successful."<br><br>I know it\'s hard to believe, but you\'re not the judge of my success.',
    likes: 17,
    retweets: 1
  },
  {
    id: '1400167535270531073',
    created: 1622660928000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/benignbala" rel="noopener noreferrer" target="_blank">@benignbala</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/ApacheGroovy" rel="noopener noreferrer" target="_blank">@ApacheGroovy</a> I feel very strongly about recognizing the work that people do and making improvements through a process of mutual support. That\'s the culture I want to sustain.<br><br>In reply to: <a href="https://x.com/benignbala/status/1400138107538710533" rel="noopener noreferrer" target="_blank">@benignbala</a> <span class="status">1400138107538710533</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1400164194448646145',
    created: 1622660132000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/POTUS" rel="noopener noreferrer" target="_blank">@POTUS</a> You need to end the filibuster or else these are just empty words.<br><br>In reply to: <a href="https://x.com/POTUS/status/1400079048743788546" rel="noopener noreferrer" target="_blank">@POTUS</a> <span class="status">1400079048743788546</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1398782320291696640',
    created: 1622330667000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RJDickenson" rel="noopener noreferrer" target="_blank">@RJDickenson</a> filter:follows -filter:replies<br><br>In reply to: <a href="https://x.com/RJDickenson/status/1398769896209543168" rel="noopener noreferrer" target="_blank">@RJDickenson</a> <span class="status">1398769896209543168</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1398040077335502850',
    created: 1622153703000,
    type: 'post',
    text: 'The search on <a href="http://docs.netlify.com" rel="noopener noreferrer" target="_blank">docs.netlify.com</a> only seems to search page titles, so the results are absolutely useless. <a class="mention" href="https://x.com/Netlify" rel="noopener noreferrer" target="_blank">@Netlify</a>, please tune the settings so the search actually returns relevant pages in the docs. As an example, search for "netlify.toml". 0 relevant results.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1397981779865210880',
    created: 1622139804000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ammkrn" rel="noopener noreferrer" target="_blank">@ammkrn</a> The whole point of the AsciiDoc Language project <a class="mention" href="https://x.com/EclipseFdn" rel="noopener noreferrer" target="_blank">@EclipseFdn</a> is to develop a grammar as part of a specification for AsciiDoc. The project is in the early stages. We\'re starting from the docs hosted at <a href="https://docs.asciidoctor.org/asciidoc/latest/" rel="noopener noreferrer" target="_blank">docs.asciidoctor.org/asciidoc/latest/</a> The site you linked to is legacy and will be replaced.<br><br>In reply to: <a href="https://x.com/ammkrn/status/1397964422082142208" rel="noopener noreferrer" target="_blank">@ammkrn</a> <span class="status">1397964422082142208</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1397648010335559681',
    created: 1622060227000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mp911de" rel="noopener noreferrer" target="_blank">@mp911de</a> I meant to say "trance". This set by Nifra at Landschaftspark is by far one of my favorites I discovered during the course of the pandemic. Fun fact: I learned about the park from an episode of Gardners\' World (2020 E29). <a href="https://youtu.be/IdSK8LHwK5g" rel="noopener noreferrer" target="_blank">youtu.be/IdSK8LHwK5g</a><br><br>In reply to: <a href="#1397294123346890752">1397294123346890752</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1397646700525719556',
    created: 1622059915000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dblevins" rel="noopener noreferrer" target="_blank">@dblevins</a> It truly is exciting to see the relationship between the Eclipse Foundation and Apache Software Foundation evolve. I totally agree that Apache plays a critical role in Jakarta EE\'s mission statement. This is the kind of working relationship this community never had before.<br><br>In reply to: <a href="#1397645850382258177">1397645850382258177</a>',
    likes: 3,
    retweets: 1
  },
  {
    id: '1397645850382258177',
    created: 1622059712000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dblevins" rel="noopener noreferrer" target="_blank">@dblevins</a> Congratulations to you, your teams at Tomitribe and Apache, and all others involved in this remarkable achievement!!! Having observed this situation play out over the last decade, and even before that, I agree it\'s nothing short of extraordinary that this day has come to pass. 🍾<br><br>In reply to: <a href="https://x.com/dblevins/status/1397285979359088642" rel="noopener noreferrer" target="_blank">@dblevins</a> <span class="status">1397285979359088642</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1397638330704039936',
    created: 1622057919000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/delvedor" rel="noopener noreferrer" target="_blank">@delvedor</a> I want to thank you for developing hpagent. I was looking for an HTTP(S) agent for Antora that doesn\'t have any dependencies and is easy to use. hpagent hits the mark! 🎯',
    likes: 0,
    retweets: 0
  },
  {
    id: '1397310018441998340',
    created: 1621979643000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mp911de" rel="noopener noreferrer" target="_blank">@mp911de</a> Yes, very likely the same reasons as in tech. Artificial + societal barriers.<br><br>In reply to: <a href="https://x.com/mp911de/status/1397298469946236930" rel="noopener noreferrer" target="_blank">@mp911de</a> <span class="status">1397298469946236930</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1397308348303745026',
    created: 1621979245000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mp911de" rel="noopener noreferrer" target="_blank">@mp911de</a> Argh. Forgot to mention Saffron Stone.<br><br>In reply to: <a href="#1397304816565555200">1397304816565555200</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1397306960265977860',
    created: 1621978914000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mp911de" rel="noopener noreferrer" target="_blank">@mp911de</a> A notable male DJ pair outside of Europe is Aly &amp; Fila.<br><br>In reply to: <a href="#1397304816565555200">1397304816565555200</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1397304816565555200',
    created: 1621978403000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mp911de" rel="noopener noreferrer" target="_blank">@mp911de</a> I missed Eli &amp; Fur, Sama’ Abdulhadi, Nervo, Krewella, Gioli &amp; Assia, Jeny Preston.<br><br>In reply to: <a href="#1397297583958073344">1397297583958073344</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1397299599224676353',
    created: 1621977159000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mp911de" rel="noopener noreferrer" target="_blank">@mp911de</a> I\'ll check those out. Thanks for the tips!<br><br>In reply to: <a href="https://x.com/mp911de/status/1397295696571768834" rel="noopener noreferrer" target="_blank">@mp911de</a> <span class="status">1397295696571768834</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1397299277949374464',
    created: 1621977083000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mp911de" rel="noopener noreferrer" target="_blank">@mp911de</a> Haha. It definitely makes my head spin. But that\'s part of what makes it so fun. So many styles. So many moods.<br><br>In reply to: <a href="https://x.com/mp911de/status/1397298814285983750" rel="noopener noreferrer" target="_blank">@mp911de</a> <span class="status">1397298814285983750</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1397298575244103682',
    created: 1621976915000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mp911de" rel="noopener noreferrer" target="_blank">@mp911de</a> But that\'s beside the issue at hand, because Tomorrowland features most styles. So there\'s plenty of room for them to expand their line-up to include these artists.<br><br>In reply to: <a href="#1397297460519653376">1397297460519653376</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1397298076956594176',
    created: 1621976796000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mp911de" rel="noopener noreferrer" target="_blank">@mp911de</a> ...though I would agree that\'s she\'s not squarely in trance, but rather a melodic trance / deep house. But again, sets vary widely.<br><br>In reply to: <a href="#1397296510711197700">1397296510711197700</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1397297583958073344',
    created: 1621976679000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mp911de" rel="noopener noreferrer" target="_blank">@mp911de</a> One I left off is Elena Volos (Helen&amp;Boys).<br><br>In reply to: <a href="#1397295457542369280">1397295457542369280</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1397297460519653376',
    created: 1621976649000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mp911de" rel="noopener noreferrer" target="_blank">@mp911de</a> While I get the differences between EDM styles, Sarah &amp; I have concluded, after become very avid listeners, that the lines are incredibly blurred, both within a DJ\'s repertoire and in a single set. Many DJs seem to be comfortable adjusting what they play for a particular event.<br><br>In reply to: <a href="https://x.com/mp911de/status/1397296829004501001" rel="noopener noreferrer" target="_blank">@mp911de</a> <span class="status">1397296829004501001</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1397296665321644033',
    created: 1621976460000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bryanwklein" rel="noopener noreferrer" target="_blank">@bryanwklein</a> See list starting at<br><br>Quoting: <a href="#1397294123346890752">1397294123346890752</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1397296510711197700',
    created: 1621976423000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mp911de" rel="noopener noreferrer" target="_blank">@mp911de</a> In the trance space, I recently discovered Pretty Pink, who\'s doing sets just as good if not better than Markus Schulz and Paul Van Dyk (both of whom I enjoy very much).<br><br>In reply to: <a href="https://x.com/mp911de/status/1397295696571768834" rel="noopener noreferrer" target="_blank">@mp911de</a> <span class="status">1397295696571768834</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1397296049862049792',
    created: 1621976313000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mp911de" rel="noopener noreferrer" target="_blank">@mp911de</a> Correction: Alexandra Badoi<br><br>In reply to: <a href="#1397294123346890752">1397294123346890752</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1397295457542369280',
    created: 1621976172000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mp911de" rel="noopener noreferrer" target="_blank">@mp911de</a> Bklava. Misstress Barbara DJ. Anna Tur. Lauren Mia. Fatima Hajji. Courtesy. Sam Divine. Pretty Pink. Helena Hauff. Patrice Baumel. (And that is by no means an exhaustive list).<br><br>In reply to: <a href="#1397295027861135360">1397295027861135360</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1397295027861135360',
    created: 1621976069000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mp911de" rel="noopener noreferrer" target="_blank">@mp911de</a> Mariana Bo. Rezz. Deborah De Luca. Sam Divine. ANNA. Ida Engberg. Monika Kruse. Nora en Pure. Miss Monique. Lilly Palmer. Mollie Collins. DJane Djoly. Maxinne. Eliza Rose. Kristina Sky. Giorgia Angiuli. salute. JVNA. Patrick Mason. Dana Montana. SPFDJ. Carmen Electro. ...<br><br>In reply to: <a href="#1397294704933294080">1397294704933294080</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1397294704933294080',
    created: 1621975992000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mp911de" rel="noopener noreferrer" target="_blank">@mp911de</a> Here\'s the list of women and/or Black DJs I shared. There are many outside of techno. Charlotte De Witte. Amelie Lens. Koralova. Xenia UA. Alison Wonderland. Ellen Allien. Nifra. Black Coffee. Nina Kraviz. Koven. Mandy. Sofi Tukker. Honey Dijon. Sherelle. The Blessed Madonna. ...<br><br>In reply to: <a href="#1397294123346890752">1397294123346890752</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1397294123346890752',
    created: 1621975854000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mp911de" rel="noopener noreferrer" target="_blank">@mp911de</a> After exploring the EDM landscape in great depth for over a year, I can say with certainty this is not the case. There\'s definitely imbalance in the ratio, but to say there\'s not so much is untrue. Notable trace artists include Nifra and Alexandria Badoi. I\'ll follow with more.<br><br>In reply to: <a href="https://x.com/mp911de/status/1397155417386627072" rel="noopener noreferrer" target="_blank">@mp911de</a> <span class="status">1397155417386627072</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1397149483490627586',
    created: 1621941369000,
    type: 'post',
    text: 'The reason I\'m speaking up is mainly because this is a blatant problem. But it\'s also because it\'s not enough just to strive for inclusion in tech. We need to push for it in all aspects of society. As a patron of music, I find myself in a position to use my voice.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1397148439104737286',
    created: 1621941120000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/domdorn" rel="noopener noreferrer" target="_blank">@domdorn</a> I think a virtual festival can be an amazing, albeit different, experience. However, in light of this situation, I will be doing exactly as you suggest.<br><br>In reply to: <a href="https://x.com/domdorn/status/1397146874386264064" rel="noopener noreferrer" target="_blank">@domdorn</a> <span class="status">1397146874386264064</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1397148162985369601',
    created: 1621941054000,
    type: 'post',
    text: 'I also want to add that the line-up for 2020 was just as bad in this regard. (5 women out of 65 artists). But I was willing to give them the benefit of the doubt since it was early in the pandemic and logistics were damn near impossible. But I probably shouldn\'t have.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1397146510165307399',
    created: 1621940660000,
    type: 'post',
    text: 'It\'s ironic to name the event "Around the World" when nearly every DJ is from Europe. And it\'s doubly ironic since the event is virtual, so a DJ could theoretically perform from anywhere in the world.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1397144167906635777',
    created: 1621940101000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/domdorn" rel="noopener noreferrer" target="_blank">@domdorn</a> The last several festivals have been virtual, so anyone in the world can get a ticket. It also means that any DJ in the world can potentially perform without having to travel.<br><br>In reply to: <a href="https://x.com/domdorn/status/1397142716111335424" rel="noopener noreferrer" target="_blank">@domdorn</a> <span class="status">1397142716111335424</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1397143841476468738',
    created: 1621940024000,
    type: 'post',
    text: 'I think Tomorrowland needs to pull this line-up and repost once they have one that actually represents the talent that\'s available.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1397142650222899202',
    created: 1621939740000,
    type: 'post',
    text: 'What Tomorrowland is doing is discrimination, pure and simple. And it\'s disgusting.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1397142521763926027',
    created: 1621939709000,
    type: 'post',
    text: 'I\'ve been listening to sets on YouTube daily since the pandemic began. This line-up in no way represents the talent that\'s out there. I sent Tomorrowland a list of 43 woman and minority DJs that have at least as many if not more views than any of the DJs in the line-up.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1397142014387310593',
    created: 1621939588000,
    type: 'quote',
    text: 'Tomorrowland has a diversity problem. Only 3 of 43 artists are women, &amp; the line-up is mostly artists with privilege. This is totally unacceptable. I\'ll be boycotting Tomorrowland until they do better. I\'m calling on <a class="mention" href="https://x.com/arminvanbuuren" rel="noopener noreferrer" target="_blank">@arminvanbuuren</a>, <a class="mention" href="https://x.com/davidguetta" rel="noopener noreferrer" target="_blank">@davidguetta</a>, &amp; <a class="mention" href="https://x.com/MartinGarrix" rel="noopener noreferrer" target="_blank">@MartinGarrix</a> to speak up too.<br><br>Quoting: <a href="https://x.com/tomorrowland/status/1395363752392921089" rel="noopener noreferrer" target="_blank">@tomorrowland</a> <span class="status">1395363752392921089</span>',
    likes: 4,
    retweets: 1
  },
  {
    id: '1396789298775564290',
    created: 1621855494000,
    type: 'post',
    text: '"I won\'t use a dynamic language unless I already have the interpretor for it installed" seems like the antithesis of a principled stance to me.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1396615155316756481',
    created: 1621813975000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ahus1de" rel="noopener noreferrer" target="_blank">@ahus1de</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/waynebeaton" rel="noopener noreferrer" target="_blank">@waynebeaton</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> It pretty much gets everything right. The one thing the GitHub diff does that this does not is detect a change to the semantics (e.g., an example block became a sidebar block). It probably makes more sense to show the block removed/added.<br><br>In reply to: <a href="#1396614034661679106">1396614034661679106</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1396614034661679106',
    created: 1621813708000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ahus1de" rel="noopener noreferrer" target="_blank">@ahus1de</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/waynebeaton" rel="noopener noreferrer" target="_blank">@waynebeaton</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> That is *tremendously* cool. Nice work!<br><br>In reply to: <a href="https://x.com/ahus1de/status/1396407311275548672" rel="noopener noreferrer" target="_blank">@ahus1de</a> <span class="status">1396407311275548672</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1396393038897840129',
    created: 1621761018000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/httpJunkie" rel="noopener noreferrer" target="_blank">@httpJunkie</a> <a class="mention" href="https://x.com/Gatsby" rel="noopener noreferrer" target="_blank">@Gatsby</a> Marqeta uses Gatsby with AsciiDoc. See <a href="https://medium.marqeta.com/write-your-developer-docs-in-asciidoc-78febf2a1304" rel="noopener noreferrer" target="_blank">medium.marqeta.com/write-your-developer-docs-in-asciidoc-78febf2a1304</a><br><br>In reply to: <a href="https://x.com/httpJunkie/status/1396199813331361792" rel="noopener noreferrer" target="_blank">@httpJunkie</a> <span class="status">1396199813331361792</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1395826859435196417',
    created: 1621626031000,
    type: 'post',
    text: 'As of today, I\'m fully vaccinated against COVID-19. I recognize it both as a civic responsibility and a tremendous privilege. But it shouldn\'t be a privilege. I\'ll continue to advocate for free access to this and future vaccines as a human right.',
    likes: 36,
    retweets: 2
  },
  {
    id: '1395796097910546439',
    created: 1621618697000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/EndaSportswear" rel="noopener noreferrer" target="_blank">@EndaSportswear</a> <a class="mention" href="https://x.com/tkadlec" rel="noopener noreferrer" target="_blank">@tkadlec</a> Here they are, btw. I got the original Itens, which are currently being updated. I got green for go time!<br><br>In reply to: <a href="https://x.com/EndaSportswear/status/1395710825017577473" rel="noopener noreferrer" target="_blank">@EndaSportswear</a> <span class="status">1395710825017577473</span>',
    photos: ['<div class="item"><img class="photo" src="media/1395796097910546439.jpg"></div>'],
    likes: 1,
    retweets: 0
  },
  {
    id: '1395417769341030400',
    created: 1621528496000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tkadlec" rel="noopener noreferrer" target="_blank">@tkadlec</a> I encourage you to check out Enda. I got a pair last summer and really love them. They\'re a woman-run business out of Kenya, where all the shoes are made. <a href="https://www.endasportswear.com/" rel="noopener noreferrer" target="_blank">www.endasportswear.com/</a><br><br>In reply to: <a href="https://x.com/tkadlec/status/1395401220555169796" rel="noopener noreferrer" target="_blank">@tkadlec</a> <span class="status">1395401220555169796</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1395305219114762243',
    created: 1621501662000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mp911de" rel="noopener noreferrer" target="_blank">@mp911de</a> It\'s annotate now. I just couldn\'t remember the name (and GitHub is still using the terminology blame).<br><br>In reply to: <a href="https://x.com/mp911de/status/1395299554959020033" rel="noopener noreferrer" target="_blank">@mp911de</a> <span class="status">1395299554959020033</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1395287526496112640',
    created: 1621497444000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/benignbala" rel="noopener noreferrer" target="_blank">@benignbala</a> Aha! I couldn\'t remember what the alternative name was. Thanks for reminding me of it!<br><br>In reply to: <a href="https://x.com/benignbala/status/1395285069867020288" rel="noopener noreferrer" target="_blank">@benignbala</a> <span class="status">1395285069867020288</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1395281930900295682',
    created: 1621496110000,
    type: 'post',
    text: 'If you\'re viewing a source file on GitHub, and you want to inspect the blame (praise) around a line, click on the line number, then press "b". GitHub will switch to the blame (praise) view with that line focused. Saves you from a bunch of scrolling.',
    likes: 24,
    retweets: 4
  },
  {
    id: '1394534989304197124',
    created: 1621318025000,
    type: 'post',
    text: 'I\'m not sure what GitHub changed recently, but I\'m getting a lot of timeouts when trying to access pages on <a href="http://github.com" rel="noopener noreferrer" target="_blank">github.com</a>. Some requests just hang. Others go right through.',
    likes: 1,
    retweets: 1
  },
  {
    id: '1394197465985781764',
    created: 1621237553000,
    type: 'quote',
    text: 'I was today years old when I learned about Jerry Lawson and the fact that he invented interchangeable video game cartridges, but I owe him my childhood.<br><br>Quoting: <a href="https://x.com/netflix/status/1296162303079927808" rel="noopener noreferrer" target="_blank">@netflix</a> <span class="status">1296162303079927808</span>',
    likes: 6,
    retweets: 0
  },
  {
    id: '1393694772825509888',
    created: 1621117702000,
    type: 'post',
    text: 'In my experience, people who object to a reputable code of conduct are the ones who want there to be problems.',
    likes: 6,
    retweets: 1
  },
  {
    id: '1393627611683688448',
    created: 1621101689000,
    type: 'post',
    text: 'War IS terror.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1393627242090012672',
    created: 1621101601000,
    type: 'quote',
    text: 'It makes me sick to my stomach to see "air strikes" in the headlines &amp; read about attacks by Israel against the people of Palestine, particularly during a pandemic. It\'s gross. As I can\'t speak with expertise on the matter, I choose to lift the voice of @RhashidaTalib in support.<br><br>Quoting: <a href="https://x.com/People4Bernie/status/1392978125223469057" rel="noopener noreferrer" target="_blank">@People4Bernie</a> <span class="status">1392978125223469057</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1393479992776953859',
    created: 1621066494000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/TheBrousse" rel="noopener noreferrer" target="_blank">@TheBrousse</a> More like...<br><br>In reply to: <a href="https://x.com/TheBrousse/status/1393476385730174977" rel="noopener noreferrer" target="_blank">@TheBrousse</a> <span class="status">1393476385730174977</span>',
    photos: ['<div class="item"><img class="photo" src="media/1393479992776953859.jpg"></div>'],
    likes: 1,
    retweets: 0
  },
  {
    id: '1393388685115097092',
    created: 1621044725000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> And tell Abbie congratulations from me for graduating HS, with a 4.0 no less. I\'m damn impressed!! Like, damn!<br><br>In reply to: <a href="#1393383983652610049">1393383983652610049</a>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1393383983652610049',
    created: 1621043604000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> I just had a dream we were sitting at Coors Field together. Must have been some precognition. Enjoy!<br><br>In reply to: <a href="https://x.com/mraible/status/1393381893932945409" rel="noopener noreferrer" target="_blank">@mraible</a> <span class="status">1393381893932945409</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1393353659950387200',
    created: 1621036374000,
    type: 'post',
    text: 'I will continue to wear my mask around other people until this pandemic is over in solidarity with those who are still fighting to keep themselves and loved ones alive and well. We are, and have always been, in this together. 😷🌐',
    likes: 36,
    retweets: 4
  },
  {
    id: '1393312063431270401',
    created: 1621026457000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bk2204" rel="noopener noreferrer" target="_blank">@bk2204</a> s/have/having/ 🤦‍♂️<br><br>In reply to: <a href="#1393305286312484866">1393305286312484866</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1393305286312484866',
    created: 1621024841000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bk2204" rel="noopener noreferrer" target="_blank">@bk2204</a> I was just have a discussion about this kind of situation with someone. It\'s one thing to have a healthy debate about a decision when it\'s warranted. It\'s a whole other thing to have to debate every single action. I see it as the aggression that it is.<br><br>In reply to: <a href="https://x.com/bk2204/status/1393303799578587136" rel="noopener noreferrer" target="_blank">@bk2204</a> <span class="status">1393303799578587136</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1393255062538002437',
    created: 1621012866000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jaredmorgs" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> Absolutely the same for me.<br><br>In reply to: <a href="https://x.com/jaredmorgs/status/1393158994466799619" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <span class="status">1393158994466799619</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1393087354399707136',
    created: 1620972882000,
    type: 'quote',
    text: 'YES! I\'m a huge fan of Zulip. It has increased the quality of engagement within my communities while melting away the stress of having to catch up on messages. I forget what life was like before it. 🚣<br><br>Quoting: <a href="https://x.com/zulip/status/1392994381410430978" rel="noopener noreferrer" target="_blank">@zulip</a> <span class="status">1392994381410430978</span>',
    likes: 15,
    retweets: 3
  },
  {
    id: '1392611074004819970',
    created: 1620859328000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mmilinkov" rel="noopener noreferrer" target="_blank">@mmilinkov</a> <a class="mention" href="https://x.com/JustinTrudeau" rel="noopener noreferrer" target="_blank">@JustinTrudeau</a> Did you see the "shot and chaser" story? Apparently, it was quite effective. <a href="https://www.msn.com/en-us/news/us/erie-county-hosting-shot-and-chaser-covid-19-vaccine-clinic-saturday-at-resurgence-brewing/ar-BB1gvG0r" rel="noopener noreferrer" target="_blank">www.msn.com/en-us/news/us/erie-county-hosting-shot-and-chaser-covid-19-vaccine-clinic-saturday-at-resurgence-brewing/ar-BB1gvG0r</a><br><br>In reply to: <a href="https://x.com/mmilinkov/status/1392608360088866818" rel="noopener noreferrer" target="_blank">@mmilinkov</a> <span class="status">1392608360088866818</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1392281017046626304',
    created: 1620780636000,
    type: 'post',
    text: 'The font on Twitter looks different, so I did a quick search. Top result: "No, you\'re not crazy — Twitter has a new font." Well, I definitely felt crazy.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1392004528224931841',
    created: 1620714716000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> I fully support cinnamon buns for dinner. We are definitely at the breakfast for dinner, or any meal of the day, stage of the pandemic.<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/1391912858733236224" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">1391912858733236224</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1391888384348491776',
    created: 1620687025000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/petitroll" rel="noopener noreferrer" target="_blank">@petitroll</a> I highly recommend just getting the book. Everything in it has been a grand slam. Another one of my favorite recipes is the Portobello Frites, which transports me right to Antwerpen.<br><br>In reply to: <a href="https://x.com/petitroll/status/1391886410886615047" rel="noopener noreferrer" target="_blank">@petitroll</a> <span class="status">1391886410886615047</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1391886130551787520',
    created: 1620686488000,
    type: 'post',
    text: 'My favorite dish from that book is the Artichoke Crab(less) Cakes. I grew up in Maryland and I *know* what a crab cake is supposed to taste like. And this one is exactly on point. There\'s absolutely no difference in flavor or texture.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1391885093933703171',
    created: 1620686241000,
    type: 'post',
    text: 'Btw, the claim that you will miss your favorite foods is a wild misconception. In the end, food is science. There are many ways to get the same flavor and texture, some are just more humane. The cookbook "But I Could Never Go Vegan" is all about finding these alternate pathways.<br><br>Quoting: <a href="#1391833930144247808">1391833930144247808</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1391833930144247808',
    created: 1620674042000,
    type: 'post',
    text: 'Two years ago today I stopped eating meat, and committed to being vegan a few months later. It\'s taken me on the most interesting culinary journey of my entire life. Not only have I had better tasting food than I ever thought possible, I\'ve learned how to make it myself. 🌿😋<br><br>Quoting: <a href="#1129605075687677954">1129605075687677954</a>',
    likes: 19,
    retweets: 0
  },
  {
    id: '1391714718616674304',
    created: 1620645620000,
    type: 'post',
    text: 'It\'s been over 3 days now and I can report that I had very little reaction to the 2nd vaccine shot. I may have been a bit woozy the first evening, and I\'ve probably slept a bit more than normal. But it really didn\'t stop me from going about my daily activities. 💪',
    likes: 10,
    retweets: 0
  },
  {
    id: '1391484045125558273',
    created: 1620590623000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aheritier" rel="noopener noreferrer" target="_blank">@aheritier</a> <a class="mention" href="https://x.com/benwalding" rel="noopener noreferrer" target="_blank">@benwalding</a> I\'m pleased to report that full symlink support is coming in the next alpha of Antora 3 (3.0.0-alpha.5)!! You won\'t have to do anything special to take advantage of it (other than evaluate the alpha release).<br><br>In reply to: <a href="https://x.com/aheritier/status/1364116947701542918" rel="noopener noreferrer" target="_blank">@aheritier</a> <span class="status">1364116947701542918</span>',
    likes: 3,
    retweets: 1
  },
  {
    id: '1390780300825923588',
    created: 1620422837000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> <a class="mention" href="https://x.com/bobbytank42" rel="noopener noreferrer" target="_blank">@bobbytank42</a> On the other hand, I\'m jealous you live in countries where citizens actually adhere to the guidelines that help prevent the spread. Our only hope here is the vaccine.<br><br>In reply to: <a href="https://x.com/alexsotob/status/1390636455383404544" rel="noopener noreferrer" target="_blank">@alexsotob</a> <span class="status">1390636455383404544</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1390608836025884674',
    created: 1620381957000,
    type: 'post',
    text: 'I decided to get the shot in my other arm to see if it was any different since the first shot made my arm quite sore. I barely felt it this time. The muscle on that side must have a lot less scar tissue. Still a little sore after the fact, but noticeably less.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1390565060527853569',
    created: 1620371520000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/benignbala" rel="noopener noreferrer" target="_blank">@benignbala</a> I hope so too. I\'ve signed countless petitions to get the vaccine tech and materials flowing your way.<br><br>In reply to: <a href="https://x.com/benignbala/status/1390562247613632512" rel="noopener noreferrer" target="_blank">@benignbala</a> <span class="status">1390562247613632512</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1390497462801027078',
    created: 1620355403000,
    type: 'post',
    text: 'Vaccination card complete. So far, no side effects (but the night is young). I\'m 😁 under the masks knowing I did my part to eradicate this virus. As soon as you can, please do your part too.',
    photos: ['<div class="item"><img class="photo" src="media/1390497462801027078.jpg"></div>'],
    likes: 12,
    retweets: 0
  },
  {
    id: '1390389842421305345',
    created: 1620329745000,
    type: 'post',
    text: 'Today\'s the day! Heading out to get 💉 #2 of the Pfizer-BioNTech vaccine shortly.<br><br>I got the jab in my right arm last time. Should I even it out?',
    likes: 20,
    retweets: 0
  },
  {
    id: '1389329649952456707',
    created: 1620076975000,
    type: 'post',
    text: 'I truly hope that the vaccination program in the US helps people understand how transformative free (at the point of service) healthcare would be for this country.',
    likes: 15,
    retweets: 2
  },
  {
    id: '1389121357523283969',
    created: 1620027314000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DuncanLock" rel="noopener noreferrer" target="_blank">@DuncanLock</a> When I say inline styles are discouraged, what I mean is that they can be forbidden by the style-src CSP setting. And this is my point. That restriction is at odds with HTML itself.<br><br>In reply to: <a href="https://x.com/DuncanLock/status/1389070955016691715" rel="noopener noreferrer" target="_blank">@DuncanLock</a> <span class="status">1389070955016691715</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1388963144316317696',
    created: 1619989593000,
    type: 'post',
    text: 'So if inline styles in HTML are discouraged, and the width attribute on the col tag is deprecated, how in the world are we supposed to specify column widths for tables? (And I\'m ruling out having hundreds of CSS classes to account for each width, which is absurd).',
    likes: 5,
    retweets: 0
  },
  {
    id: '1388573807518969857',
    created: 1619896768000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DuncanLock" rel="noopener noreferrer" target="_blank">@DuncanLock</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> I encourage you to ask questions like this in the community chat. That\'s where most of the community hangs out. And I think you\'ll find you get answers faster that way. <a href="https://asciidoctor.zulipchat.com" rel="noopener noreferrer" target="_blank">asciidoctor.zulipchat.com</a>.<br><br>In reply to: <a href="https://x.com/DuncanLock/status/1388370763720249347" rel="noopener noreferrer" target="_blank">@DuncanLock</a> <span class="status">1388370763720249347</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1387816189359915010',
    created: 1619716138000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Infoxicador" rel="noopener noreferrer" target="_blank">@Infoxicador</a> No doubt, everyone\'s experience is different. I\'ve run numerous large projects for over a decade and I\'ve never found it to be a problem. It\'s true, there have been bogus requests. But I don\'t consider that to be the same thing. I find it easy to tell the difference.<br><br>In reply to: <a href="https://x.com/Infoxicador/status/1387736425471295490" rel="noopener noreferrer" target="_blank">@Infoxicador</a> <span class="status">1387736425471295490</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1387733032925163523',
    created: 1619696312000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Infoxicador" rel="noopener noreferrer" target="_blank">@Infoxicador</a> I love it. It tells me someone is not only reading the docs or source, but cares enough to keep it sharp.<br><br>In reply to: <a href="https://x.com/Infoxicador/status/1387706643719262208" rel="noopener noreferrer" target="_blank">@Infoxicador</a> <span class="status">1387706643719262208</span>',
    likes: 144,
    retweets: 3
  },
  {
    id: '1387496881882734595',
    created: 1619640009000,
    type: 'post',
    text: 'Did I mention that another patch release of Asciidoctor is available? We\'ve been on a roll this month. This time, it\'s 2.0.15. It\'s a small one, but nonetheless notable for reasons stated. Check it out! <a href="https://github.com/asciidoctor/asciidoctor/releases/tag/v2.0.15" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor/releases/tag/v2.0.15</a>',
    likes: 17,
    retweets: 6
  },
  {
    id: '1387337227433390080',
    created: 1619601945000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> Aside from that issue, the Asciidoctor PDF tests all pass on JRuby 9.3 (snapshot).<br><br>In reply to: <a href="#1387336373103996930">1387336373103996930</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1387336373103996930',
    created: 1619601741000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> Asciidoctor (core / Ruby) is working great on JRuby 9.3 (snapshot). However, the Asciidoctor PDF tests are failing due to a bug in Marshal.load. I reported it here with a simple test case: <a href="https://github.com/jruby/jruby/issues/6662" rel="noopener noreferrer" target="_blank">github.com/jruby/jruby/issues/6662</a><br><br>In reply to: <a href="https://x.com/headius/status/1387051686405951490" rel="noopener noreferrer" target="_blank">@headius</a> <span class="status">1387051686405951490</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1387182427705995264',
    created: 1619565038000,
    type: 'post',
    text: 'Fedora 34 includes Ruby 3. Fedora is awesome like that 😉',
    likes: 3,
    retweets: 1
  },
  {
    id: '1387181801404207107',
    created: 1619564888000,
    type: 'quote',
    text: 'I keep going around saying how impressed I am with the incredible work the Fedora Project is doing. It truly is an amazing OS and everyone who works on it should be immensely proud of what they have created together. I\'m looking forward to upgrading!<br><br>Quoting: <a href="https://x.com/fedora/status/1387044514322272256" rel="noopener noreferrer" target="_blank">@fedora</a> <span class="status">1387044514322272256</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1386463164322123776',
    created: 1619393552000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/domdorn" rel="noopener noreferrer" target="_blank">@domdorn</a> What would be fair is to point at anyone who did participate and say "take responsibility for the impact you have had on others". Because that is on them. But it\'s still not the only reason this is happening.<br><br>In reply to: <a href="#1386462062314807303">1386462062314807303</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1386462062314807303',
    created: 1619393289000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/domdorn" rel="noopener noreferrer" target="_blank">@domdorn</a> That\'s not the right way to look at this situation. Yes, people shouldn\'t have gotten together and by doing so put millions of lives at risk. But not all who were impacted were the ones who participated (by a long shot). Saying they had it coming is not fair or humane.<br><br>In reply to: <a href="https://x.com/domdorn/status/1386353171572871168" rel="noopener noreferrer" target="_blank">@domdorn</a> <span class="status">1386353171572871168</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1386233093439119366',
    created: 1619338699000,
    type: 'post',
    text: 'Words cannot express how much sadness I feel about the COVID-19 outbreak right now in India. I\'m exploring ways I can help that are beyond words. If you have suggestions, please post them below.',
    likes: 13,
    retweets: 1
  },
  {
    id: '1385533265134571526',
    created: 1619171847000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> podman, my friend. podman<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1385493677959639040',
    created: 1619162408000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ghillert" rel="noopener noreferrer" target="_blank">@ghillert</a> <a class="mention" href="https://x.com/pfizer" rel="noopener noreferrer" target="_blank">@pfizer</a> <a class="mention" href="https://x.com/BioNTech_Group" rel="noopener noreferrer" target="_blank">@BioNTech_Group</a> 👍 The shirt says it all!🏃<br><br>In reply to: <a href="https://x.com/ghillert/status/1385492560068562946" rel="noopener noreferrer" target="_blank">@ghillert</a> <span class="status">1385492560068562946</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1385333025488084993',
    created: 1619124106000,
    type: 'post',
    text: 'Emails are my meetings.<br><br>* requiring just as much time, effort, and attention',
    likes: 0,
    retweets: 0
  },
  {
    id: '1384998290781921282',
    created: 1619044299000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dailyburn" rel="noopener noreferrer" target="_blank">@dailyburn</a> Yes! I\'m so happy to see another dance cardio party by Jenna this week. My spouse and I love them!<br><br>In reply to: <a href="https://x.com/dailyburn/status/1383803825710981121" rel="noopener noreferrer" target="_blank">@dailyburn</a> <span class="status">1383803825710981121</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1384997847787970561',
    created: 1619044193000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dailyburn" rel="noopener noreferrer" target="_blank">@dailyburn</a> The timing of this program worked out perfectly for me! I threw out my back a few weeks ago and I used it as part of my road to recovery. Now, I\'m back to 100%. But I still love sprinkling these routines into my week. Thanks!<br><br>In reply to: <a href="https://x.com/dailyburn/status/1381983244275445763" rel="noopener noreferrer" target="_blank">@dailyburn</a> <span class="status">1381983244275445763</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1384976323639226370',
    created: 1619039061000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> My thoughts are with you and your loved ones. ❤️<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1384971584453107718" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1384971584453107718</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1384636736639102979',
    created: 1618958097000,
    type: 'post',
    text: 'A verdict can never bring a person back to life, but one can only hope that it does bring about change. Justice is about not profiling someone for the color of their skin or anything of the sort. Justice is not kneeling on someone\'s neck. Defund this cycle.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1384553096072728577',
    created: 1618938156000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/NetflixEng" rel="noopener noreferrer" target="_blank">@NetflixEng</a> For the record, I was in kindergarten or first grade.<br><br>In reply to: <a href="#1384399958200094723">1384399958200094723</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1384399958200094723',
    created: 1618901645000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/NetflixEng" rel="noopener noreferrer" target="_blank">@NetflixEng</a> Logo. And I literally wrote it. Like with pencil and paper.<br><br>In reply to: <a href="https://x.com/NetflixEng/status/1384200622551883777" rel="noopener noreferrer" target="_blank">@NetflixEng</a> <span class="status">1384200622551883777</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1384244038308532225',
    created: 1618864471000,
    type: 'post',
    text: 'Speaking of taking off &amp; landing, Asciidoctor 2.0.14 just touched down! Marking a quick turnaround from the previous release, it patches a few issues we discovered while testing integration with Antora, as well as a few oldies. Read all about it here: <a href="https://github.com/asciidoctor/asciidoctor/releases/tag/v2.0.14" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor/releases/tag/v2.0.14</a>',
    likes: 27,
    retweets: 9
  },
  {
    id: '1383866843513819155',
    created: 1618774541000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobbytank42" rel="noopener noreferrer" target="_blank">@bobbytank42</a> I\'ve added you as an admin. Please proceed. Btw, you can PM me in Zulip for similar requests.<br><br>In reply to: <a href="#1383852306576207873">1383852306576207873</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1383852306576207873',
    created: 1618771075000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobbytank42" rel="noopener noreferrer" target="_blank">@bobbytank42</a> I\'ll make you an admin since we want to fix that too.<br><br>In reply to: <a href="https://x.com/bobbytank42/status/1383779813932998658" rel="noopener noreferrer" target="_blank">@bobbytank42</a> <span class="status">1383779813932998658</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1383155212068614145',
    created: 1618604874000,
    type: 'post',
    text: 'I\'m slightly sore at the location of injection point, but otherwise, I feel quite energetic. I attribute that energy in part to the psychological burden being lifted.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1383154650652692482',
    created: 1618604741000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> Words to live by. Tests are part of my daily affirmations.<br><br>In reply to: <a href="https://x.com/majson/status/1383152547427401731" rel="noopener noreferrer" target="_blank">@majson</a> <span class="status">1383152547427401731</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1383147201744891907',
    created: 1618602965000,
    type: 'post',
    text: 'So I never knew this about myself, but apparently, I love disco. 🕺',
    likes: 7,
    retweets: 0
  },
  {
    id: '1383138106748985344',
    created: 1618600796000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> Hmm, that\'s surprising. Seems like it might have been an oversight seeing how smooth it works elsewhere.<br><br>In reply to: <a href="https://x.com/majson/status/1383113666602676224" rel="noopener noreferrer" target="_blank">@majson</a> <span class="status">1383113666602676224</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1383137886392881156',
    created: 1618600744000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/venkat_s" rel="noopener noreferrer" target="_blank">@venkat_s</a> And I\'ve always preferred snow over rain, so I welcome it whenever it wants to happen. Make no mistake, though, I love those sunny days too. That\'s why I call Colorado home!<br><br>In reply to: <a href="https://x.com/venkat_s/status/1383128153028055042" rel="noopener noreferrer" target="_blank">@venkat_s</a> <span class="status">1383128153028055042</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1383105484433985536',
    created: 1618593019000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/venkat_s" rel="noopener noreferrer" target="_blank">@venkat_s</a> I\'ve come to accept that in Colorado, April showers refer to the snow variety. 🌨️<br><br>In reply to: <a href="https://x.com/venkat_s/status/1383069531996712961" rel="noopener noreferrer" target="_blank">@venkat_s</a> <span class="status">1383069531996712961</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1382964674937819136',
    created: 1618559447000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pvaneeckhoudt" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> That\'s actually what I was expecting here. But then the schedule moved way up. 😱<br><br>In reply to: <a href="https://x.com/pvaneeckhoudt/status/1382961136841195520" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> <span class="status">1382961136841195520</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1382878781241655296',
    created: 1618538968000,
    type: 'post',
    text: '💉 #1 down. Let\'s beat this thing once and for all.',
    photos: ['<div class="item"><img class="photo" src="media/1382878781241655296.jpg"></div>'],
    likes: 13,
    retweets: 0
  },
  {
    id: '1382862123840139268',
    created: 1618534997000,
    type: 'post',
    text: 'The administration of the vaccine in Colorado seems to be efficient, at least at the site where we were. We got in and out in &lt; 30 minutes, during which time we witnessed hundreds of people being vaccinated. It was *busy*.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1382792130419036164',
    created: 1618518309000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> I think it may be a bug in JRuby for Windows. IO#mtime does not honor the TZ environment variable, whereas Time::now and Time::at does. But there\'s an easy workaround. See <a href="https://github.com/asciidoctor/asciidoctor/commit/6ce59d10651434a625cfbbd819611b8c8d932b50#diff-5179b161eee45fc3ebb845fdf45ee9ee688ef3df38d46ed48488ab490c6d716bR56-R57" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor/commit/6ce59d10651434a625cfbbd819611b8c8d932b50#diff-5179b161eee45fc3ebb845fdf45ee9ee688ef3df38d46ed48488ab490c6d716bR56-R57</a><br><br>In reply to: <a href="https://x.com/headius/status/1382788225866072066" rel="noopener noreferrer" target="_blank">@headius</a> <span class="status">1382788225866072066</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1382646200403324933',
    created: 1618483517000,
    type: 'post',
    text: 'This may come as a surprise, but I only just managed to get the Asciidoctor tests running on JRuby 9.1 and 9.2 for Windows in CI. In doing so, I discovered one bug that is now fixed. And it will now stay fixed ;)',
    likes: 11,
    retweets: 1
  },
  {
    id: '1382437405005733889',
    created: 1618433736000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ashleymcnamara" rel="noopener noreferrer" target="_blank">@ashleymcnamara</a> I can\'t say I\'ve ever looked forward to getting sick before. This will be a first. Thanks for helping me to be prepared!<br><br>In reply to: <a href="https://x.com/ashleymcnamara/status/1382325737923833859" rel="noopener noreferrer" target="_blank">@ashleymcnamara</a> <span class="status">1382325737923833859</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1382437110582419457',
    created: 1618433666000,
    type: 'post',
    text: 'I\'m scheduled for my first vaccine shot (Pfizer) tomorrow. 💉😷',
    likes: 46,
    retweets: 0
  },
  {
    id: '1382137725340377088',
    created: 1618362287000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> <a class="mention" href="https://x.com/saranutter" rel="noopener noreferrer" target="_blank">@saranutter</a> My deepest deepest condolences. I\'m struggling to find any words of comfort other than my heart is with you and your loved ones. There are so many people in the community who care about you. We\'re here if you need support. ❤️<br><br>In reply to: <a href="https://x.com/headius/status/1382126958885097472" rel="noopener noreferrer" target="_blank">@headius</a> <span class="status">1382126958885097472</span>',
    likes: 5,
    retweets: 0
  },
  {
    id: '1382037032776585216',
    created: 1618338280000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/connolly_s" rel="noopener noreferrer" target="_blank">@connolly_s</a> <a class="mention" href="https://x.com/github" rel="noopener noreferrer" target="_blank">@github</a> Yep. More attention definitely seems to be needed here. I found at least 2 abuse jobs this week in one of the repositories, and now I\'m on the hunt for more.<br><br>In reply to: <a href="https://x.com/connolly_s/status/1382036761438793729" rel="noopener noreferrer" target="_blank">@connolly_s</a> <span class="status">1382036761438793729</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1382036796205191168',
    created: 1618338223000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/connolly_s" rel="noopener noreferrer" target="_blank">@connolly_s</a> I like your idea that for a PR, I have to click a button to allow the workflow to proceed. That would be totally reasonable.<br><br>In reply to: <a href="https://x.com/connolly_s/status/1382036445628665858" rel="noopener noreferrer" target="_blank">@connolly_s</a> <span class="status">1382036445628665858</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1382036432609419264',
    created: 1618338137000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/connolly_s" rel="noopener noreferrer" target="_blank">@connolly_s</a> It was GitHub. It just took me a minute to find that page. I have that disabled already, yet the workflow still ran.<br><br>In reply to: <a href="https://x.com/connolly_s/status/1382036063582089217" rel="noopener noreferrer" target="_blank">@connolly_s</a> <span class="status">1382036063582089217</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1382036036943900673',
    created: 1618338042000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/connolly_s" rel="noopener noreferrer" target="_blank">@connolly_s</a> I already restrict which actions can be used.<br><br>In reply to: <a href="https://x.com/connolly_s/status/1382035864239362049" rel="noopener noreferrer" target="_blank">@connolly_s</a> <span class="status">1382035864239362049</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1382035936377131011',
    created: 1618338018000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/connolly_s" rel="noopener noreferrer" target="_blank">@connolly_s</a> Is that GitHub or GitLab?<br><br>In reply to: <a href="https://x.com/connolly_s/status/1382035388433379328" rel="noopener noreferrer" target="_blank">@connolly_s</a> <span class="status">1382035388433379328</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1382034889894031363',
    created: 1618337769000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/connolly_s" rel="noopener noreferrer" target="_blank">@connolly_s</a> Thanks for the tip!<br><br>In reply to: <a href="https://x.com/connolly_s/status/1382034606115946503" rel="noopener noreferrer" target="_blank">@connolly_s</a> <span class="status">1382034606115946503</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1382032123549212672',
    created: 1618337109000,
    type: 'post',
    text: 'Argh. GitHub Actions spam is insidious. (Someone submits a workflow that replaces the existing one just to spin up computers and get them to do work). Surely there is some sort of protection against this behavior.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1381484995923955713',
    created: 1618206664000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> I did a fair amount of research before buying mine and settled on the Samsung Galaxy Tab line. There are several tiers to choose from. I went with the S6, though newer models are now available.<br><br>In reply to: <a href="https://x.com/settermjd/status/1381483379846172672" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1381483379846172672</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1381135750033330176',
    created: 1618123397000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JordanChariton" rel="noopener noreferrer" target="_blank">@JordanChariton</a> 😢 I must have watched every interview you did with her...and I learned so much from her patient and compassionate words.<br><br>In reply to: <a href="https://x.com/JordanChariton/status/1381091830377418752" rel="noopener noreferrer" target="_blank">@JordanChariton</a> <span class="status">1381091830377418752</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1381019803628883969',
    created: 1618095753000,
    type: 'post',
    text: 'Asciidoctor 2.0.13 is out! Most notably, this release resolves a regression &amp; a security vuln. It also fixes numerous bugs in the man page output &amp; adds integration with pygments.rb 2. It will be the candidate for switching Antora to Asciidoctor 2. See <a href="https://github.com/asciidoctor/asciidoctor/releases/tag/v2.0.13" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor/releases/tag/v2.0.13</a>',
    likes: 68,
    retweets: 18
  },
  {
    id: '1380629279751303169',
    created: 1618002645000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sunrisemvmt" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a> <a class="mention" href="https://x.com/POTUS" rel="noopener noreferrer" target="_blank">@POTUS</a> An extremely disappointing decision. This is not the leadership from this administration we voted for.<br><br>In reply to: <a href="https://x.com/sunrisemvmt/status/1380626661314494466" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a> <span class="status">1380626661314494466</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1380598549163859968',
    created: 1617995319000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> We\'ve been following this too and think it\'s absolutely fascinating. I definitely recommend catching the NASA report specials on PBS. That\'s where we learned about this part of the mission.<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1380530886697910273" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1380530886697910273</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1380597585505742850',
    created: 1617995089000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/vmassol" rel="noopener noreferrer" target="_blank">@vmassol</a> <a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> That\'s outdated language and we\'re still in the process of finding it and cleaning it up. There is one AsciiDoc syntax and there are official extension points for introducing custom elements. <a href="http://AsciiDoc.py" rel="noopener noreferrer" target="_blank">AsciiDoc.py</a> is legacy and no longer recognized as AsciiDoc.<br><br>In reply to: <a href="https://x.com/vmassol/status/1380539510711316485" rel="noopener noreferrer" target="_blank">@vmassol</a> <span class="status">1380539510711316485</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1380461020561371137',
    created: 1617962529000,
    type: 'post',
    text: 'I really appreciate how seamless renaming the default branch of a repository on GitHub is. It\'s especially nice that the URLs are redirected, so you don\'t end up breaking any existing links.',
    likes: 7,
    retweets: 0
  },
  {
    id: '1380439582085238787',
    created: 1617957418000,
    type: 'post',
    text: 'Warming up some releases this week. Recovering from my back injury has me motivated.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1380238127860568066',
    created: 1617909387000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> <a class="mention" href="https://x.com/vmassol" rel="noopener noreferrer" target="_blank">@vmassol</a> And that work will start from the initial contribution that originated from the Asciidoctor project. That\'s the recognized definition of AsciiDoc until the first version of the specification is ratified (probably next year).<br><br>In reply to: <a href="#1380236978369622017">1380236978369622017</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1380236978369622017',
    created: 1617909113000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> <a class="mention" href="https://x.com/vmassol" rel="noopener noreferrer" target="_blank">@vmassol</a> Correct, though probably more accurate to say that the leaders and major adopters of AsciiDoc have formed a group so we can clarify and evolve the accepted definition of the language together. You\'re welcome to get involved.<br><br>In reply to: <a href="https://x.com/ggrossetie/status/1380195078614958081" rel="noopener noreferrer" target="_blank">@ggrossetie</a> <span class="status">1380195078614958081</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1380036925973667841',
    created: 1617861417000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> Same.<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1380036760701263874" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1380036760701263874</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1379915997595848705',
    created: 1617832586000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ashleymcnamara" rel="noopener noreferrer" target="_blank">@ashleymcnamara</a> Congrats! To us, you always were ;)<br><br>In reply to: <a href="https://x.com/ashleymcnamara/status/1379199699442298880" rel="noopener noreferrer" target="_blank">@ashleymcnamara</a> <span class="status">1379199699442298880</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1379889079941177345',
    created: 1617826168000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Prathkum" rel="noopener noreferrer" target="_blank">@Prathkum</a> Solid reference. ex seems to be missing, which is the height of the "x" glyph (aka x-height) of the element\'s font. (Since x is usually square, I sometimes use this in place of ch).<br><br>In reply to: <a href="https://x.com/Prathkum/status/1379719813593894914" rel="noopener noreferrer" target="_blank">@Prathkum</a> <span class="status">1379719813593894914</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1379679614537134081',
    created: 1617776227000,
    type: 'reply',
    text: '@serverlessgirl <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> <a class="mention" href="https://x.com/couchbase" rel="noopener noreferrer" target="_blank">@couchbase</a> You\'re welcome! You can also find the code for it here: <a href="https://gitlab.com/antora/antora-asciidoctor-extensions" rel="noopener noreferrer" target="_blank">gitlab.com/antora/antora-asciidoctor-extensions</a><br><br>In reply to: @serverlessgirl <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1378824469846745092',
    created: 1617572345000,
    type: 'post',
    text: 'I may just be shouting into the void here, but there absolutely needs to be a code of conduct for elected officials. The shit I\'m seeing some of them say from official accounts is so blatantly racist and repulsive that I\'m absolutely beside myself. It will tear society apart.',
    likes: 10,
    retweets: 1
  },
  {
    id: '1378502271160360961',
    created: 1617495527000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DigitalNotions" rel="noopener noreferrer" target="_blank">@DigitalNotions</a> Sweet! I think this will be a great fit. In my experience, I think ADRs fit best outside the repository so they don\'t get caught up in branches. They are more meta.<br><br>In reply to: <a href="https://x.com/DigitalNotions/status/1378501453753487360" rel="noopener noreferrer" target="_blank">@DigitalNotions</a> <span class="status">1378501453753487360</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1378264757812162560',
    created: 1617438899000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bmarwell" rel="noopener noreferrer" target="_blank">@bmarwell</a> <a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> <a class="mention" href="https://x.com/khmarbaise" rel="noopener noreferrer" target="_blank">@khmarbaise</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Woot!<br><br>In reply to: <a href="https://x.com/bmarwell/status/1378264657052450816" rel="noopener noreferrer" target="_blank">@bmarwell</a> <span class="status">1378264657052450816</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1378229393458294784',
    created: 1617430468000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bmarwell" rel="noopener noreferrer" target="_blank">@bmarwell</a> <a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> <a class="mention" href="https://x.com/khmarbaise" rel="noopener noreferrer" target="_blank">@khmarbaise</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Too late for what? AsciiDoc has incredibly high adoption. This move will benefit scores of users across many different tech communities and beyond.<br><br>In reply to: <a href="https://x.com/bmarwell/status/1378060980719128582" rel="noopener noreferrer" target="_blank">@bmarwell</a> <span class="status">1378060980719128582</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1378228835645153282',
    created: 1617430335000,
    type: 'reply',
    text: '@buraitopengin <a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> It\'s worth pointing out that GitLab does. I\'m beginning to see this more and more as the intentional omission by GitHub that it is.<br><br>In reply to: @buraitopengin <span class="status">&lt;deleted&gt;</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1378045783128240131',
    created: 1617386692000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ahus1de" rel="noopener noreferrer" target="_blank">@ahus1de</a> @patrickbkoetter Thanks for the feedback. Indeed, that\'s what we do in the Antora project. I just thought perhaps we could shorten the workflow, and not have to worry about branches, by using this new functionality.<br><br>In reply to: <a href="https://x.com/ahus1de/status/1378024492102991883" rel="noopener noreferrer" target="_blank">@ahus1de</a> <span class="status">1378024492102991883</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1378045227806584832',
    created: 1617386559000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bryanwklein" rel="noopener noreferrer" target="_blank">@bryanwklein</a> Thanks for the feedback! Money isn\'t a concern since it\'s already enabled on the instance I\'m considering using it (Eclipse\'s GitLab instance). I agree it could be done using a regular issue. I just thought maybe this could be a good fit.<br><br>In reply to: <a href="https://x.com/bryanwklein/status/1378003287455109122" rel="noopener noreferrer" target="_blank">@bryanwklein</a> <span class="status">1378003287455109122</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1378044383182790656',
    created: 1617386358000,
    type: 'reply',
    text: '@patrickbkoetter Thanks for the feedback. I had not considered the absence of history. It looks like it\'s something they may add in the future, but certainty a limitation at present.<br><br>In reply to: @patrickbkoetter <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1377882081485746176',
    created: 1617347662000,
    type: 'post',
    text: 'Would GitLab\'s Requirements Management be a good fit for hosting ADRs for a project?<br><br><a href="https://docs.gitlab.com/ee/user/project/requirements/" rel="noopener noreferrer" target="_blank">docs.gitlab.com/ee/user/project/requirements/</a><br><a href="https://adr.github.io/" rel="noopener noreferrer" target="_blank">adr.github.io/</a><br><a href="https://architecturedecisionrecord.com/" rel="noopener noreferrer" target="_blank">architecturedecisionrecord.com/</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1376619316427116545',
    created: 1617046596000,
    type: 'post',
    text: 'I play this game daily. What non-obvious term did they assign to that emoji I desperately want to use? 🤔',
    likes: 5,
    retweets: 0
  },
  {
    id: '1376409919608385538',
    created: 1616996671000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mesirii" rel="noopener noreferrer" target="_blank">@mesirii</a> Thanks! So far the improvement is significant. I think the investment in my workout routine is paying dividends. Exactly as I had hoped ;)<br><br>In reply to: <a href="https://x.com/mesirii/status/1376350586879279105" rel="noopener noreferrer" target="_blank">@mesirii</a> <span class="status">1376350586879279105</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1376338372038258692',
    created: 1616979613000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> Thanks buddy!<br><br>In reply to: <a href="https://x.com/starbuxman/status/1376304040829612033" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">1376304040829612033</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1376279171492552704',
    created: 1616965499000,
    type: 'post',
    text: 'The pain has been excruciating, but I may have turned a corner using a combination of icing and light stretching. Come on body, you can do it!',
    likes: 5,
    retweets: 0
  },
  {
    id: '1376252374390480896',
    created: 1616959110000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cramTeXeD" rel="noopener noreferrer" target="_blank">@cramTeXeD</a> Indeed, it really drains the motivation. But that was renewed by the first sense of progress. Here\'s hoping.<br><br>In reply to: <a href="https://x.com/cramTeXeD/status/1375961464696094723" rel="noopener noreferrer" target="_blank">@cramTeXeD</a> <span class="status">1375961464696094723</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1375956172243562504',
    created: 1616888490000,
    type: 'post',
    text: 'I\'ve been working out very consistently this past year to prevent this very thing from happening, though this one is a sports injury. I landed a little sideways on a jump and it shocked my lower back. But the conditioning should help it heal quickly.',
    likes: 5,
    retweets: 0
  },
  {
    id: '1375954718900416512',
    created: 1616888143000,
    type: 'post',
    text: 'Argh. I threw out my back. Not the way I was planning to spend my weekend. But I\'m going to try hard to be patient and rest so it can recover.',
    likes: 8,
    retweets: 0
  },
  {
    id: '1374448906382184455',
    created: 1616529129000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joshsimmons" rel="noopener noreferrer" target="_blank">@joshsimmons</a> I stand with you in solidarity with those who have been harmed and continue to be disenfranchised. You have my full support. (In fact, I happen to be working on a code of conduct today for my project).<br><br>In reply to: <a href="https://x.com/joshsimmons/status/1374004997063540736" rel="noopener noreferrer" target="_blank">@joshsimmons</a> <span class="status">1374004997063540736</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1374290855029219329',
    created: 1616491447000,
    type: 'post',
    text: 'The good news is that speaking up (rather than trying to convince myself I\'m being too sensitive) is proving effective. So I\'m grateful for that. And I\'ll show my appreciation graciously to those who choose to reconcile.<br><br>Quoting: <a href="#1373963828904353794">1373963828904353794</a>',
    likes: 16,
    retweets: 1
  },
  {
    id: '1374268080180752386',
    created: 1616486017000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/richardschoen" rel="noopener noreferrer" target="_blank">@richardschoen</a> The authoritative resource on AsciiDoc as it\'s used today can be found here: <a href="https://docs.asciidoctor.org/asciidoc/latest/" rel="noopener noreferrer" target="_blank">docs.asciidoctor.org/asciidoc/latest/</a><br><br>In reply to: <a href="https://x.com/richardschoen/status/1373969339070693384" rel="noopener noreferrer" target="_blank">@richardschoen</a> <span class="status">1373969339070693384</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1373968911301894148',
    created: 1616414690000,
    type: 'post',
    text: 'I actually had someone tell me the other day that they were failing at their job and I was the one to blame.',
    likes: 5,
    retweets: 1
  },
  {
    id: '1373967066617638916',
    created: 1616414250000,
    type: 'post',
    text: 'But I\'m not going to let those people ruin it for everyone. What I\'m going to do is start enforcing a code of conduct in the most strict way possible. Frankly, I don\'t care how many users it costs me, because the cost of being treated with disrespect is just too damn high.',
    likes: 30,
    retweets: 0
  },
  {
    id: '1373966493348622342',
    created: 1616414113000,
    type: 'post',
    text: 'For the first time in over two decades, I\'ve actually had doubts about why I\'m doing this. The way people treat each other has just gone down hill. And that\'s sucks the community spirit right out of it.',
    likes: 19,
    retweets: 2
  },
  {
    id: '1373963828904353794',
    created: 1616413478000,
    type: 'post',
    text: 'I am really, really getting sick of people shaming me in issue comments because they can\'t get the feature they want this instant. Look, I get that COVID-19 sucks and it has put everyone on edge, but don\'t take it out on me. And if you see others doing it, please say something.',
    likes: 72,
    retweets: 9
  },
  {
    id: '1373405612718002176',
    created: 1616280389000,
    type: 'reply',
    text: '@jbryant787 Vegan sausage (made from sunflower seeds) and gravy (made from oat milk). I would have taken a picture, but I hoovered it ;)<br><br>In reply to: @jbryant787 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1373386765482950657',
    created: 1616275895000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> My thoughts are with you and your family today. Hug whoever you can.<br><br>In reply to: <a href="https://x.com/agoncal/status/1373346475065872384" rel="noopener noreferrer" target="_blank">@agoncal</a> <span class="status">1373346475065872384</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1373381357838110722',
    created: 1616274606000,
    type: 'post',
    text: 'Birthday biscuits!',
    photos: ['<div class="item"><img class="photo" src="media/1373381357838110722.jpg"></div>'],
    likes: 9,
    retweets: 0
  },
  {
    id: '1373357511223341056',
    created: 1616268921000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/eddiejaoude" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> Ah yes, the changelog is also a very, very important step in the process. Good one! There\'s no doubt that the only right way to do it is the way that makes you feel most confident. Trust the automation. That\'s the key.<br><br>In reply to: <a href="https://x.com/eddiejaoude/status/1373357005176500225" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> <span class="status">1373357005176500225</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1373356514010828802',
    created: 1616268683000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/eddiejaoude" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> What people often get confused about is when GitHub Actions should do the release. The process I like to use is to push a tag to the repository, then have the action trigger on that tag and update it once the release is done. But I see room for more tutorials around it.<br><br>In reply to: <a href="#1373355675049361411">1373355675049361411</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1373355675049361411',
    created: 1616268483000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/eddiejaoude" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> The most important one for me is publishing a package. That\'s not something you want to rely on a local machine to do because it may not always follow the same process or produce a consistent result. So we let GitHub Actions handle it. See <a href="https://github.com/asciidoctor/asciidoctor-pdf/blob/main/.github/workflows/release.yml#L30-L31" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-pdf/blob/main/.github/workflows/release.yml#L30-L31</a><br><br>In reply to: <a href="https://x.com/eddiejaoude/status/1373353988062593026" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> <span class="status">1373353988062593026</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1373032909494317056',
    created: 1616191529000,
    type: 'post',
    text: 'I think the pandemic has helped me understand that I don\'t need toxic people in my life. I used to feel obligated to appease them or make the relationship work. I\'m past all that now. I\'m only going to focus on positive relationships that are built on a foundation of respect.',
    likes: 29,
    retweets: 2
  },
  {
    id: '1372710910041595909',
    created: 1616114759000,
    type: 'post',
    text: 'It appears that GitLab has no controls for managing abuse at the project or group level. That means having to make your case to the GitLab host (for .com that\'s GitLab itself) to deal with abuse. That\'s a major drawback for autonomy when compared to GitHub.',
    likes: 7,
    retweets: 0
  },
  {
    id: '1372633270710464514',
    created: 1616096248000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> We had that in common. But the morning sun is there to comfort us. Take care and enjoy the flowers.<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1372380194359582728" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1372380194359582728</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1372156330052571140',
    created: 1615982537000,
    type: 'post',
    text: 'The grid-template-rows essentially says to place the footer using its content height, then allocate the remaining height to the first row as needed up to the max height. If the top row wants more, make it scroll.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1372156328991424514',
    created: 1615982536000,
    type: 'post',
    text: 'Using CSS grid, it\'s possible with just 4 properties!<br><br>First, on the max height container:<br><br>display: grid;<br>grid-template-columns: 3fr 1fr;<br>grid-template-rows: 1fr auto;<br><br>Then on the footer:<br><br>grid-column: 1 / -1;<br><br>Finally, add overflow:auto to the elements in the top row.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1372156328198688775',
    created: 1615982536000,
    type: 'post',
    text: 'I had a jump out of my chair moment while developing today when I discovered a new CSS technique. It felt so good!<br><br>I needed to make a max height container with two columns and a fixed footer, where the columns scroll if they exceed the remaining space above the footer.',
    likes: 7,
    retweets: 0
  },
  {
    id: '1371741497549189121',
    created: 1615883633000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pilhuhn" rel="noopener noreferrer" target="_blank">@pilhuhn</a> As I like to say, what this apartment lacks in size it makes up for in view ;)<br><br>In reply to: <a href="https://x.com/pilhuhn/status/1371732161695510530" rel="noopener noreferrer" target="_blank">@pilhuhn</a> <span class="status">1371732161695510530</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1371527784812572675',
    created: 1615832680000,
    type: 'post',
    text: 'Calm after the storm (always the best mornings in Colorado).',
    photos: ['<div class="item"><img class="photo" src="media/1371527784812572675.jpg"></div>'],
    likes: 9,
    retweets: 0
  },
  {
    id: '1371508796762243074',
    created: 1615828153000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gregturn" rel="noopener noreferrer" target="_blank">@gregturn</a> Protecting my peeps. I love to see it! 💉<br><br>In reply to: <a href="https://x.com/gregturn/status/1371469719400816640" rel="noopener noreferrer" target="_blank">@gregturn</a> <span class="status">1371469719400816640</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1371507936812396553',
    created: 1615827948000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> @kingarthurflour Being able to make my own desserts has truly become one of life\'s greatest rewards. I have cheered myself up on more than one occasion. But even more important, I love to share them with Sarah and see her smile.<br><br>In reply to: <a href="https://x.com/arungupta/status/1371460626619400193" rel="noopener noreferrer" target="_blank">@arungupta</a> <span class="status">1371460626619400193</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1371349926303567873',
    created: 1615790275000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> @kingarthurflour I forgot to snap a pic of the most recent batch, but here ones I made at the start of the year.<br><br>In reply to: <a href="https://x.com/arungupta/status/1368582055081078784" rel="noopener noreferrer" target="_blank">@arungupta</a> <span class="status">1368582055081078784</span>',
    photos: ['<div class="item"><img class="photo" src="media/1371349926303567873.jpg"></div>'],
    likes: 1,
    retweets: 0
  },
  {
    id: '1371190985250594822',
    created: 1615752381000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/evanchooly" rel="noopener noreferrer" target="_blank">@evanchooly</a> Yes. The idea was not to disrupt both communities if it didn\'t work out. Fortunately, it is working out incredibly well, so Antora is up next.<br><br>In reply to: <a href="https://x.com/evanchooly/status/1371091670020816900" rel="noopener noreferrer" target="_blank">@evanchooly</a> <span class="status">1371091670020816900</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1371063998246920193',
    created: 1615722104000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Thanks! As I always like to say, we\'re successful when you\'re successful. Spread that knowledge!<br><br>In reply to: <a href="https://x.com/agoncal/status/1371062032930385920" rel="noopener noreferrer" target="_blank">@agoncal</a> <span class="status">1371062032930385920</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1371062969472229382',
    created: 1615721859000,
    type: 'post',
    text: 'I\'m also connecting you with the essential content written by contributors to the numerous projects now that Antora pulls all that content together. I\'m merely the messenger ;)',
    likes: 2,
    retweets: 0
  },
  {
    id: '1371061988227387399',
    created: 1615721625000,
    type: 'post',
    text: 'Now, if you navigate to a page from the search results, then click back, it will restore the previous search. (On mobile, this currently only works on Firefox).',
    likes: 3,
    retweets: 0
  },
  {
    id: '1371061364576243717',
    created: 1615721477000,
    type: 'post',
    text: 'I\'ve put a lot of work into the search on <a href="http://docs.asciidoctor.org" rel="noopener noreferrer" target="_blank">docs.asciidoctor.org</a> to help you find the information you need when using Asciidoctor. (And that work is still ongoing).',
    likes: 22,
    retweets: 2
  },
  {
    id: '1371009765631926276',
    created: 1615709174000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> Welcome! I\'m so glad to see you at our new home!<br><br>In reply to: <a href="https://x.com/agoncal/status/1371005011648524288" rel="noopener noreferrer" target="_blank">@agoncal</a> <span class="status">1371005011648524288</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1370945238454267906',
    created: 1615693790000,
    type: 'post',
    text: 'The transition to Zulip has given me the strongest sense of community I\'ve ever felt since starting the Asciidoctor project. The way conversations are organized (by streams, then by topics) gives a much greater sense of space.',
    likes: 34,
    retweets: 8
  },
  {
    id: '1370283017147609089',
    created: 1615535904000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> Snow days live on!<br><br>In reply to: <a href="https://x.com/mraible/status/1370282227439865860" rel="noopener noreferrer" target="_blank">@mraible</a> <span class="status">1370282227439865860</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1370282676188454914',
    created: 1615535823000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sten_aksel" rel="noopener noreferrer" target="_blank">@sten_aksel</a> That exactly. I\'m waiting on the storm to hit to snap my shot.<br><br>In reply to: <a href="https://x.com/sten_aksel/status/1370281452324143105" rel="noopener noreferrer" target="_blank">@sten_aksel</a> <span class="status">1370281452324143105</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1370279968547106818',
    created: 1615535177000,
    type: 'post',
    text: 'If we\'re already stuck inside, can we still say that we\'re snowed in?',
    likes: 2,
    retweets: 0
  },
  {
    id: '1370135542042562560',
    created: 1615500743000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> Another thing to consider is that there isn\'t pressure to be on your game on the day of the conference. If you aren\'t feeling great one day, you get more chances for everything to fall into place, so you can catch up.<br><br>In reply to: <a href="https://x.com/brunoborges/status/1370134443793346562" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1370134443793346562</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1370134795657703427',
    created: 1615500565000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> For me, 3 days is the sweet spot. Sometimes you jump right in, then decide you\'ve had enough. Other times, you miss the first day, but you feel like there\'s still plenty of time left to participate. And other times, you might invest a little bit each day, leaving time to recoup.<br><br>In reply to: <a href="#1370133809539162113">1370133809539162113</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1370133809539162113',
    created: 1615500330000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> I really want conference organizers to understand that a single day virtual conference is very stressful. With all activity happening at home, it\'s impossible to carve out a single block of time for an event. And it goes by in a flash. There needs to be a bigger target to hit.<br><br>In reply to: <a href="https://x.com/brunoborges/status/1370132545828835329" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1370132545828835329</span>',
    likes: 7,
    retweets: 0
  },
  {
    id: '1369923803657113601',
    created: 1615450261000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rfdonnelly" rel="noopener noreferrer" target="_blank">@rfdonnelly</a> Observing NASA &amp; its partners at work continues to teach me valuable lessons about how to approach software development. I\'m in awe of the precision planning and extensive testing. (I still get nervous just publishing a software release to a package repository without error).<br><br>In reply to: <a href="https://x.com/rfdonnelly/status/1362650612890968070" rel="noopener noreferrer" target="_blank">@rfdonnelly</a> <span class="status">1362650612890968070</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1369819847274557444',
    created: 1615425476000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dailyburn" rel="noopener noreferrer" target="_blank">@dailyburn</a> LOVE IT! And we can\'t help it that we break into that reggaeton step at random points during the day. 🕺<br><br>In reply to: <a href="https://x.com/dailyburn/status/1369806856990773249" rel="noopener noreferrer" target="_blank">@dailyburn</a> <span class="status">1369806856990773249</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1369747501272551428',
    created: 1615408227000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> My condolences, Marc. If she was given an extra day for every person she helped, she would have surely outlived us all. This world can be so cruel.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1368973040671985664" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1368973040671985664</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1369582031026327552',
    created: 1615368776000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/thiloginkel" rel="noopener noreferrer" target="_blank">@thiloginkel</a> I can confirm that this coalescer does a perfect job of fusing the includes into the main document, which is why I\'m strongly advocating for it to replace the one in the extensions lab. I just haven\'t had a chance to get to it yet.<br><br>In reply to: <a href="#1369581680072126465">1369581680072126465</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1369581680072126465',
    created: 1615368692000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/thiloginkel" rel="noopener noreferrer" target="_blank">@thiloginkel</a> I do, but it is currently written in JavaScript. I\'ll share that with you, but also try to get the Ruby version published later this week. <a href="https://gitlab.com/opendevise/oss/antora-site-generator-with-pdf-exporter/-/blob/v2.3.x/lib/asciidoc-source-coalescer-extension.js" rel="noopener noreferrer" target="_blank">gitlab.com/opendevise/oss/antora-site-generator-with-pdf-exporter/-/blob/v2.3.x/lib/asciidoc-source-coalescer-extension.js</a><br><br>In reply to: <a href="https://x.com/thiloginkel/status/1369577433423372288" rel="noopener noreferrer" target="_blank">@thiloginkel</a> <span class="status">1369577433423372288</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1369467418054066176',
    created: 1615341450000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dailyburn" rel="noopener noreferrer" target="_blank">@dailyburn</a> My spouse and I have really, really enjoyed Jenna\'s last two dance party sets. Thank you for keeping us in shape and in rhythm!<br><br>In reply to: <a href="https://x.com/dailyburn/status/1366070666877800448" rel="noopener noreferrer" target="_blank">@dailyburn</a> <span class="status">1366070666877800448</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1369250092356624385',
    created: 1615289636000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> The secret is the pageshow window event, which executes when a page is displayed, even if it comes from the in-memory cache. So I can hook into that.<br><br>In reply to: <a href="#1369249709357817858">1369249709357817858</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1369249709357817858',
    created: 1615289544000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> Actually, I don\'t think you\'ll have to after all. I think I\'ve found a way to detect the back button and restore the previous search automatically. Although Firefox was doing it, it was only going part way. With the solution I\'m working on, all browsers will do the same thing.<br><br>In reply to: <a href="https://x.com/RalfDMueller/status/1369249007588044801" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <span class="status">1369249007588044801</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1369242735773843458',
    created: 1615287882000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> I might be able to emulate Firefox\'s behavior. I haven\'t given up on it yet.<br><br>In reply to: <a href="#1369230180858744838">1369230180858744838</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1369230397880463362',
    created: 1615284940000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> I should say "remains open" rather than "never closes" because of course you can still close the search results.<br><br>In reply to: <a href="#1369230180858744838">1369230180858744838</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1369230180858744838',
    created: 1615284888000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> This step isn\'t necessary if you use Firefox. Firefox restores the previous page state when you click back, so the search box never closes. Chrome reloads the page, which is what causes the search box to close.<br><br>In reply to: <a href="#1369223916527357952">1369223916527357952</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1369223916527357952',
    created: 1615283395000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> When you return to the page, you can now restore the previous search by typing Ctrl+&lt; in the search box (which you can focus by pressing Ctrl+/).<br><br>In reply to: <a href="#1367934998431469570">1367934998431469570</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1369037220145864706',
    created: 1615238883000,
    type: 'post',
    text: 'On International Women\'s Day, I renew my commitment to fight for gender equality every single day of the year. I know we can\'t just settle for progress. We must keep going until the fight is won and upheld. I stand and act in solidarity. ♀️',
    likes: 9,
    retweets: 0
  },
  {
    id: '1368886458270904321',
    created: 1615202938000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ReneSchwietzke" rel="noopener noreferrer" target="_blank">@ReneSchwietzke</a> Join us at <a href="https://asciidoctor.zulipchat.com" rel="noopener noreferrer" target="_blank">asciidoctor.zulipchat.com</a> if you have more questions.<br><br>In reply to: <a href="https://x.com/ReneSchwietzke/status/1368866792203038721" rel="noopener noreferrer" target="_blank">@ReneSchwietzke</a> <span class="status">1368866792203038721</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1368693236563275780',
    created: 1615156871000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ReneSchwietzke" rel="noopener noreferrer" target="_blank">@ReneSchwietzke</a> Here\'s another from another Asciidoctor community member: <a href="https://github.com/akosma/eBook-Template" rel="noopener noreferrer" target="_blank">github.com/akosma/eBook-Template</a><br><br>In reply to: <a href="#1368692510239780865">1368692510239780865</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1368692510239780865',
    created: 1615156698000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ReneSchwietzke" rel="noopener noreferrer" target="_blank">@ReneSchwietzke</a> You might consider this one: <a href="https://github.com/mraible/infoq-mini-book" rel="noopener noreferrer" target="_blank">github.com/mraible/infoq-mini-book</a><br><br>In reply to: <a href="https://x.com/ReneSchwietzke/status/1368534615581855744" rel="noopener noreferrer" target="_blank">@ReneSchwietzke</a> <span class="status">1368534615581855744</span>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1368457302219493379',
    created: 1615100620000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> @kingarthurflour Lookin\' nice! I just made carrot and raisin muffins tonight, so I\'m sharing the vibe!<br><br>In reply to: <a href="https://x.com/arungupta/status/1368413307862847489" rel="noopener noreferrer" target="_blank">@arungupta</a> <span class="status">1368413307862847489</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1368006130169880580',
    created: 1614993052000,
    type: 'reply',
    text: '@ringods <a class="mention" href="https://x.com/resmo79" rel="noopener noreferrer" target="_blank">@resmo79</a> <a class="mention" href="https://x.com/svg" rel="noopener noreferrer" target="_blank">@svg</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> You can find lots more examples here: <a href="https://gitlab.com/antora/antora.org/-/issues/20" rel="noopener noreferrer" target="_blank">gitlab.com/antora/antora.org/-/issues/20</a><br><br>In reply to: @ringods <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1367934998431469570',
    created: 1614976093000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> I don\'t know if we could just assume that we are going to blindly send you to next result. But what you seem to want is to be able to reopen the search box with your last query and results. Currently, we clear the results on blur. But best is just to keep the results open.<br><br>In reply to: <a href="https://x.com/RalfDMueller/status/1367931816666095622" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <span class="status">1367931816666095622</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1367919060772556802',
    created: 1614972293000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/jaredmorgs" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Isn\'t next result what the down arrow does? Or are you talking about something else?<br><br>In reply to: <a href="https://x.com/RalfDMueller/status/1367807348014714881" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <span class="status">1367807348014714881</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1367792257936461828',
    created: 1614942061000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jaredmorgs" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> You can now use Ctrl+Enter to open links from the results when navigating the list using the up and down arrows. I also changed the keyboard shortcuts to focus the input box to "s" without a modifier and "Ctrl+/".<br><br>In reply to: <a href="#1367598635010064387">1367598635010064387</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1367784153727467526',
    created: 1614940129000,
    type: 'quote',
    text: 'If you want to get a sense of Russel\'s magnificent sense of humor and personality, be sure to catch at least a few minutes of his last talk at Devoxx UK. <a href="https://youtu.be/f3tl9awko5w" rel="noopener noreferrer" target="_blank">youtu.be/f3tl9awko5w</a> He\'ll never be an ex-human in our minds (a reference to his own humor on the subject).<br><br>Quoting: <a href="https://x.com/Java_Champions/status/1367714935212544000" rel="noopener noreferrer" target="_blank">@Java_Champions</a> <span class="status">1367714935212544000</span>',
    likes: 14,
    retweets: 2
  },
  {
    id: '1367629336321302528',
    created: 1614903217000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rmannibucau" rel="noopener noreferrer" target="_blank">@rmannibucau</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Fixed, btw.<br><br>In reply to: <a href="#1367539514093174790">1367539514093174790</a>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1367621710564917250',
    created: 1614901399000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/OndraZizka" rel="noopener noreferrer" target="_blank">@OndraZizka</a> That tirade really isn\'t necessary.<br><br>In reply to: <a href="https://x.com/OndraZizka/status/1367619141142740995" rel="noopener noreferrer" target="_blank">@OndraZizka</a> <span class="status">1367619141142740995</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1367598635010064387',
    created: 1614895897000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jaredmorgs" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> I figured out how to get Ctrl+Enter to work in the search result list. Update on the way!<br><br>In reply to: <a href="https://x.com/jaredmorgs/status/1367588173107261440" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <span class="status">1367588173107261440</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1367539514093174790',
    created: 1614881802000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rmannibucau" rel="noopener noreferrer" target="_blank">@rmannibucau</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Oops! That\'s an unintended consequence of binding "f" to focus the search box. I didn\'t realize it would trap ctrl+f too. I\'ll fix it asap.<br><br>In reply to: <a href="https://x.com/rmannibucau/status/1367464341658865666" rel="noopener noreferrer" target="_blank">@rmannibucau</a> <span class="status">1367464341658865666</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1367383604645429255',
    created: 1614844630000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> I\'ve been continuing to make some improves to the search. Now you can focus the box using "f" instead of "s", and the keyboard navigation of the results should be much smoother. Still more to come.<br><br>In reply to: <a href="#1366328040863621122">1366328040863621122</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1367037965499727873',
    created: 1614762223000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AndyBai59873550" rel="noopener noreferrer" target="_blank">@AndyBai59873550</a> The flexible grouping is the really important part. 👍<br><br>In reply to: <a href="https://x.com/AndyBai59873550/status/1367037428993826817" rel="noopener noreferrer" target="_blank">@AndyBai59873550</a> <span class="status">1367037428993826817</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1367033996559847427',
    created: 1614761277000,
    type: 'post',
    text: 'I feel like one of the next big things is spatial video chat, in which your volume is scaled based on your distance from another live video avatar. I found at least one active OSS project in this spaced named Calla (based on Jitsi Meet). Are there others?',
    likes: 2,
    retweets: 0
  },
  {
    id: '1366680126415532038',
    created: 1614676908000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jaredmorgs" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> That\'s one of the benefits of a search box. It can be responsive. And Algolia docsearch does indeed offer those callbacks. You\'re right, though, it will take time to explore and make use of all the possibilities.<br><br>In reply to: <a href="https://x.com/jaredmorgs/status/1366679488252190721" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <span class="status">1366679488252190721</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366679644821352449',
    created: 1614676793000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JayarOnTV" rel="noopener noreferrer" target="_blank">@JayarOnTV</a> <a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> I agree that, above all else, it\'s often the keybindings that keep users faithful to (even entrenched in) an application, whether it\'s a graphics program, developer IDE, text editor, or word processor. DJs probably stick to a board series for the same reason.<br><br>In reply to: <a href="https://x.com/JayarOnTV/status/1366675068684623875" rel="noopener noreferrer" target="_blank">@JayarOnTV</a> <span class="status">1366675068684623875</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1366672462914625536',
    created: 1614675081000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JayarOnTV" rel="noopener noreferrer" target="_blank">@JayarOnTV</a> <a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> RAM comes down to a heat issue (as I understand it). Most laptops overheat. This one doesn\'t. I prefer to have a laptop that doesn\'t overheat because then it performs at its peak.<br><br>In reply to: <a href="https://x.com/JayarOnTV/status/1366669093189816329" rel="noopener noreferrer" target="_blank">@JayarOnTV</a> <span class="status">1366669093189816329</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1366672085284573184',
    created: 1614674991000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JayarOnTV" rel="noopener noreferrer" target="_blank">@JayarOnTV</a> <a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> Regarding the SSD, I find the bigger it is, the more junk I put on it. I\'ve been using 512GB SSD for years and I think it keeps my file hoarding under control. Also, a wallet-sized external SSD over thunderbolt is just as fast, so I can expand without even feeling it.<br><br>In reply to: <a href="https://x.com/JayarOnTV/status/1366669093189816329" rel="noopener noreferrer" target="_blank">@JayarOnTV</a> <span class="status">1366669093189816329</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1366489421273473026',
    created: 1614631440000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/rotnroll666" rel="noopener noreferrer" target="_blank">@rotnroll666</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Thank you for helping me to better understand your usage pattern and expectations.<br><br>In reply to: <a href="https://x.com/RalfDMueller/status/1366392952738709506" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <span class="status">1366392952738709506</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1366373146153074688',
    created: 1614603718000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <a class="mention" href="https://x.com/rotnroll666" rel="noopener noreferrer" target="_blank">@rotnroll666</a> <a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> One idea is to map "f" in addition to "s" (the current shortcut). That makes it closer at hand. I\'m open to that if it helps.<br><br>In reply to: <a href="https://x.com/odrotbohm/status/1366372553883795457" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <span class="status">1366372553883795457</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1366372752756776963',
    created: 1614603624000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rotnroll666" rel="noopener noreferrer" target="_blank">@rotnroll666</a> <a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> You literally captured my day. I have to fight myself sometimes, but I genuinely want to seek understanding and improvement. After all, if the community is not happy, I\'m not happy. We just need to figure out what happy is sometimes ;)<br><br>In reply to: <a href="https://x.com/rotnroll666/status/1366372070888771599" rel="noopener noreferrer" target="_blank">@rotnroll666</a> <span class="status">1366372070888771599</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1366371982019821570',
    created: 1614603441000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <a class="mention" href="https://x.com/rotnroll666" rel="noopener noreferrer" target="_blank">@rotnroll666</a> <a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Thanks. I worked pretty hard on getting it where it is today (though better is always possible).<br><br>Keep in mind we can make the shortcut whatever we want. We just have to commit to something.<br><br>In reply to: <a href="https://x.com/odrotbohm/status/1366371153200242690" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <span class="status">1366371153200242690</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1366371500396265475',
    created: 1614603326000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> I missed the opportunity to say thanks for saying the docs look great. We worked incredibly hard on them and it feels nice to have that work appreciated.<br><br>In reply to: <a href="https://x.com/RalfDMueller/status/1366297726149869570" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <span class="status">1366297726149869570</span>',
    likes: 5,
    retweets: 0
  },
  {
    id: '1366366800779206656',
    created: 1614602205000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bclozel" rel="noopener noreferrer" target="_blank">@bclozel</a> <a class="mention" href="https://x.com/rotnroll666" rel="noopener noreferrer" target="_blank">@rotnroll666</a> <a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Well said.<br><br>In reply to: <a href="https://x.com/bclozel/status/1366366396695855105" rel="noopener noreferrer" target="_blank">@bclozel</a> <span class="status">1366366396695855105</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366364252282388483',
    created: 1614601598000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rotnroll666" rel="noopener noreferrer" target="_blank">@rotnroll666</a> The not yet fully working code is here: <a href="https://gitlab.com/opendevise/oss/antora-site-generator-with-pdf-exporter" rel="noopener noreferrer" target="_blank">gitlab.com/opendevise/oss/antora-site-generator-with-pdf-exporter</a> (it will likely be renamed to single page exporter or something).<br><br>In reply to: <a href="#1366363681831878660">1366363681831878660</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366363681831878660',
    created: 1614601462000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rotnroll666" rel="noopener noreferrer" target="_blank">@rotnroll666</a> <a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Absolutely. That\'s why I firmly believe in chunking content and using an assembler to make longer documents out of it. Once you see the single page generator I\'ve been working on for Antora, you\'ll see it creates a full document, not just a list of links.<br><br>In reply to: <a href="https://x.com/rotnroll666/status/1366362903809523712" rel="noopener noreferrer" target="_blank">@rotnroll666</a> <span class="status">1366362903809523712</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1366362243491794945',
    created: 1614601119000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rotnroll666" rel="noopener noreferrer" target="_blank">@rotnroll666</a> <a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> You could argue that it makes it possible to navigate a term from start to finish. That is valid. It\'s slower than searching, but it is a capability unique to that format. And that certainly has value.<br><br>In reply to: <a href="#1366362015149686785">1366362015149686785</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366362015149686785',
    created: 1614601064000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rotnroll666" rel="noopener noreferrer" target="_blank">@rotnroll666</a> <a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> I\'m not opposed to a single page HTML. It has its uses (offline access being the main one), and we\'re going to enable it once the code is working. I just dislike the justification that it makes search easier, because it really doesn\'t. The page is going to be very, very long.<br><br>In reply to: <a href="https://x.com/rotnroll666/status/1366360738965692418" rel="noopener noreferrer" target="_blank">@rotnroll666</a> <span class="status">1366360738965692418</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366359561993510920',
    created: 1614600479000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> I tried to do a bit more with the keyboard navigation, but I ran into some problems. Technically it\'s feasible, but the autocomplete library is blocking some of the configuration from working. So I\'ll need upstream changes.<br><br>In reply to: <a href="#1366358299096932352">1366358299096932352</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366358348547821568',
    created: 1614600190000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a href="https://docs.asciidoctor.org/home/#search-tips" rel="noopener noreferrer" target="_blank">docs.asciidoctor.org/home/#search-tips</a><br><br>In reply to: <a href="https://x.com/RalfDMueller/status/1366315914078085122" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <span class="status">1366315914078085122</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366358299096932352',
    created: 1614600178000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> I believe the problem with the panel closing prematurely is now fixed.<br><br>In reply to: <a href="https://x.com/joschi83/status/1366309383567769600" rel="noopener noreferrer" target="_blank">@joschi83</a> <span class="status">1366309383567769600</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1366328040863621122',
    created: 1614592964000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> ...and soon we will have a section, because once I started writing... ;)<br><br>In reply to: <a href="#1366320075062476800">1366320075062476800</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1366320075062476800',
    created: 1614591065000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> We don\'t have a section in the docs about how to search the docs, but it\'s a good idea. For now, I\'ll mention in on the intro page.<br><br>In reply to: <a href="https://x.com/RalfDMueller/status/1366315914078085122" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <span class="status">1366315914078085122</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1366310486036254720',
    created: 1614588779000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> The search field is activated by pressing "s". I don\'t think we should hijack the browser\'s native search key combo as that would upset people. (I\'m open to other combos). Keeping the search results open is something I can fix.<br><br>In reply to: <a href="https://x.com/joschi83/status/1366309383567769600" rel="noopener noreferrer" target="_blank">@joschi83</a> <span class="status">1366309383567769600</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1366309580351803399',
    created: 1614588563000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> I think I see what it is. It\'s getting certain clicks mixed up so it ends up closing even when you are not doing a single left click. I think I can just disable the eager closing all together since it just causes trouble.<br><br>In reply to: <a href="#1366308992113278976">1366308992113278976</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366308992113278976',
    created: 1614588423000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Wait a minute. Now it does stay open. Something seems to be wrong here. Must be a timing issue.<br><br>In reply to: <a href="#1366308768900784129">1366308768900784129</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366308768900784129',
    created: 1614588369000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Ah! It doesn\'t stay open on Chrome. Okay, that\'s a bug.<br><br>In reply to: <a href="#1366308661761466368">1366308661761466368</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1366308661761466368',
    created: 1614588344000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Which browser? Because it definitely stays open on Firefox and Chrome (in my tests).<br><br>In reply to: <a href="https://x.com/joschi83/status/1366308363383037958" rel="noopener noreferrer" target="_blank">@joschi83</a> <span class="status">1366308363383037958</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366308472959029250',
    created: 1614588299000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> I was using it to mean not a fair representation. The search is very powerful and is helping a lot of people find the information they need. So it\'s disingenuous to just say "it doesn\'t work" without specifically saying how it doesn\'t work.<br><br>In reply to: <a href="https://x.com/joschi83/status/1366307769742217217" rel="noopener noreferrer" target="_blank">@joschi83</a> <span class="status">1366307769742217217</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366307777551896581',
    created: 1614588133000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> In my tests, right clicking the search result to open it in a new tab (or middle click) does work on <a href="http://docs.aciidoctor.org" rel="noopener noreferrer" target="_blank">docs.aciidoctor.org</a>. (It doesn\'t work in all Algolia search boxes because it is not enabled by default).<br><br>In reply to: <a href="https://x.com/RalfDMueller/status/1366306961688584195" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <span class="status">1366306961688584195</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366307319949135875',
    created: 1614588024000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> And that comes from the perspective of a user. I used Ctrl-f on the Asciidoctor user manual for a decade. It was always incredibly frustrating to next, next, next, next through the document. I often ended up scrolling the page because it was faster to find it with my own eyes.<br><br>In reply to: <a href="#1366306804959879168">1366306804959879168</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366306804959879168',
    created: 1614587901000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> It\'s disingenuous because we put an unbelievable amount of work into making the information easier to find, and I believe we accomplished that. Essentially saying make it the way it was sets us all the way back.<br><br>In reply to: <a href="https://x.com/joschi83/status/1366302637990305793" rel="noopener noreferrer" target="_blank">@joschi83</a> <span class="status">1366302637990305793</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366306320631037953',
    created: 1614587786000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Another possibility is to re-enable the search box when hitting the back button (which would build on the usage pattern of a Google search). That seems really reasonable. We\'d just need to find the right knobs and switches.<br><br>In reply to: <a href="#1366305637068533761">1366305637068533761</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366305637068533761',
    created: 1614587623000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> I\'m pretty sure there\'s an option to restore the previous search when the search box gets focus. I didn\'t understand why that was needed before, but now I see what it would be used for. I\'ll explore it.<br><br>In reply to: <a href="https://x.com/RalfDMueller/status/1366305101816733697" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <span class="status">1366305101816733697</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1366305240421519364',
    created: 1614587528000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> That second point is a strong one. Down the road, I would like to use something self-hosted. Unfortunately, there really isn\'t a strong alternative...though there is ongoing research in the Antora community into making Lunr more viable.<br><br>In reply to: <a href="https://x.com/RalfDMueller/status/1366304144118661121" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <span class="status">1366304144118661121</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366304842830909443',
    created: 1614587433000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> We can have a single HTML version (I have code in prototype to do it), I just don\'t believe it will get you where you need to go quicker. But if you believe that (or have some clever trick I don\'t know about), who am I to argue?<br><br>In reply to: <a href="https://x.com/joschi83/status/1366302637990305793" rel="noopener noreferrer" target="_blank">@joschi83</a> <span class="status">1366302637990305793</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366304256551096323',
    created: 1614587294000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> <a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> So propose what keyboard shortcuts we need and we can work together to add them. Just saying "it doesn\'t work for me, so change it" is just not constructive.<br><br>In reply to: <a href="https://x.com/joschi83/status/1366302637990305793" rel="noopener noreferrer" target="_blank">@joschi83</a> <span class="status">1366302637990305793</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366302458926964740',
    created: 1614586865000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Ctrl-f is a terrible substituted for a search. It has no concept of context, it completely misses variations of a phrase, such as the inclusion of a hyphen or comma, and it always starts from the top even when the target may be well past the midpoint of the document.<br><br>In reply to: <a href="#1366301735661146114">1366301735661146114</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1366301735661146114',
    created: 1614586693000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> If you have a way to make the search box even more integrated with the keyboard, feel free to submit a proposal. If it improves the usability, I\'m all for it.<br><br>In reply to: <a href="#1366301439841116163">1366301439841116163</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366301439841116163',
    created: 1614586622000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> First, you can navigate it entirely with the keyboard, so saying it\'s a popup is a disingenuous argument. It\'s very integrated. Second, if the single page is tens of thousands of paragraphs long, how many times do you have to hit "next" to get there, especially for common words?<br><br>In reply to: <a href="https://x.com/RalfDMueller/status/1366300646157275136" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <span class="status">1366300646157275136</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366299485186322436',
    created: 1614586156000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> It\'s easy to say Ctrl-f gets you there faster, but I\'m willing to bet if put to the test, hitting "next" dozens of times is far slower than an ordered search result. So it just doesn\'t hold up.<br><br>In reply to: <a href="https://x.com/RalfDMueller/status/1366297726149869570" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <span class="status">1366297726149869570</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366299136882929668',
    created: 1614586073000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> We will add a single page PDF export, I just haven\'t gotten around to it yet. Though I\'m curious why the search isn\'t working for you. Ctrl-f requires hitting "next" dozens of times, whereas the search gets you right there.<br><br>In reply to: <a href="https://x.com/RalfDMueller/status/1366297726149869570" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <span class="status">1366297726149869570</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366298794590048263',
    created: 1614585991000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> Not yet. Though there are plenty of places to have a proper conversation (mailing list, Zulip, issue tracker, etc). I\'m just pointing out that this isn\'t one of them.<br><br>In reply to: <a href="https://x.com/RalfDMueller/status/1366298262836289537" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <span class="status">1366298262836289537</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366297942173224967',
    created: 1614585788000,
    type: 'post',
    text: 'I don\'t fault the people replying because Twitter is limiting what they see and when they see it. So it\'s really the Twitter interface that\'s breaking up the conversation.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1366297094114316294',
    created: 1614585586000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/FakeUnicode" rel="noopener noreferrer" target="_blank">@FakeUnicode</a> @krejcil Understood. Thanks for pointing that out.<br><br>In reply to: <a href="https://x.com/FakeUnicode/status/1366296604186091523" rel="noopener noreferrer" target="_blank">@FakeUnicode</a> <span class="status">1366296604186091523</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366296253743595521',
    created: 1614585386000,
    type: 'post',
    text: 'Conversations on Twitter are fundamentally broken because most people just reply to the original post (or a highlighted reply). That means the conversation never evolves. I think I\'m going to stop using Twitter for discourse that requires that. It\'s just a waste of time.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1366295637671612417',
    created: 1614585239000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/FakeUnicode" rel="noopener noreferrer" target="_blank">@FakeUnicode</a> @krejcil If I\'m reading that correctly, that actually fits the usage I need perfectly. I\'m aiming to group space, possibly no-break space, tab, and newlines into a common term.<br><br>In reply to: <a href="https://x.com/FakeUnicode/status/1366295062380826624" rel="noopener noreferrer" target="_blank">@FakeUnicode</a> <span class="status">1366295062380826624</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366286602130006023',
    created: 1614583084000,
    type: 'reply',
    text: '@krejcil But the problem with the Unicode Space property is that it\'s too easily confused with the space character, hence why I believe we still need some other designation. And I believe "spacing character" offers an ideal fit.<br><br>In reply to: <a href="#1366285669929152513">1366285669929152513</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366286027460026377',
    created: 1614582947000,
    type: 'reply',
    text: '@krejcil It\'s not just American culture.<br><br>In reply to: @krejcil <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366285669929152513',
    created: 1614582862000,
    type: 'reply',
    text: '@krejcil But that\'s just it. It *doesn\'t* change the meaning of the word. Space and whitespace are being used to represent exactly the same thing (and the Space property in Unicode, \\p{Space}, reaffirms this).<br><br>In reply to: @krejcil <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366269769087807490',
    created: 1614579071000,
    type: 'reply',
    text: '@krejcil And that\'s what I believe could make people feel uncomfortable. It\'s like we had to add white to give it meaning. It\'s just unnecessary. And if we\'re going to avoid using black and white as prefixes, I think we should just be consistent instead of litigating each case.<br><br>In reply to: <a href="#1366267757415649281">1366267757415649281</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366267757415649281',
    created: 1614578592000,
    type: 'reply',
    text: '@krejcil I\'ll admit this one isn\'t as clear cut, and maybe replacing it doesn\'t help us achieve the goal of making a more inclusive space. That said, to me this is about misusing a color label when it doesn\'t add anything and invites discussion about what it could be suggesting.<br><br>In reply to: @krejcil <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366202079048425475',
    created: 1614562933000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_codewriter" rel="noopener noreferrer" target="_blank">@_codewriter</a> <a class="mention" href="https://x.com/tine2k" rel="noopener noreferrer" target="_blank">@tine2k</a> No, it\'s your arrogance. You have both talked down to me and hurled insults at me. You are way out of line.<br><br>In reply to: <a href="https://x.com/_codewriter/status/1366201756481433600" rel="noopener noreferrer" target="_blank">@_codewriter</a> <span class="status">1366201756481433600</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366201023182737410',
    created: 1614562681000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_codewriter" rel="noopener noreferrer" target="_blank">@_codewriter</a> <a class="mention" href="https://x.com/tine2k" rel="noopener noreferrer" target="_blank">@tine2k</a> I want you to apologize for that last part of your comment or else I\'m never speaking to you again.<br><br>In reply to: <a href="#1366194505641172995">1366194505641172995</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366194505641172995',
    created: 1614561127000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_codewriter" rel="noopener noreferrer" target="_blank">@_codewriter</a> <a class="mention" href="https://x.com/tine2k" rel="noopener noreferrer" target="_blank">@tine2k</a> Stop being an asshole. Because that is exactly what you are being right now.<br><br>In reply to: <a href="https://x.com/_codewriter/status/1366193851594080256" rel="noopener noreferrer" target="_blank">@_codewriter</a> <span class="status">1366193851594080256</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366192240993787904',
    created: 1614560587000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_codewriter" rel="noopener noreferrer" target="_blank">@_codewriter</a> <a class="mention" href="https://x.com/tine2k" rel="noopener noreferrer" target="_blank">@tine2k</a> I don\'t find it boring and, frankly, I\'m not asking you whether you think it\'s boring. I am working on crafting the best set of terminology I can, and this is the hard word that goes into it.<br><br>In reply to: <a href="https://x.com/_codewriter/status/1366191658946220032" rel="noopener noreferrer" target="_blank">@_codewriter</a> <span class="status">1366191658946220032</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366192020880982016',
    created: 1614560535000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dmatej" rel="noopener noreferrer" target="_blank">@dmatej</a> I don\'t see what the problem with "invisible character" is, or even "invisibles" for short. I\'m going to consider that one. Naturally, we have to still define which invisible characters we\'re talking about, but not a bad fit.<br><br>In reply to: <a href="https://x.com/dmatej/status/1366107947110137859" rel="noopener noreferrer" target="_blank">@dmatej</a> <span class="status">1366107947110137859</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366191648296767488',
    created: 1614560446000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobbytank42" rel="noopener noreferrer" target="_blank">@bobbytank42</a> The main problem with blank (and there are several) is that we aren\'t just talking about the ASCII space character \\u0020. We are also talking about newlines, and blank doesn\'t fit. We are talking about *spacing*.<br><br>In reply to: <a href="https://x.com/bobbytank42/status/1366133181313912836" rel="noopener noreferrer" target="_blank">@bobbytank42</a> <span class="status">1366133181313912836</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366191175640604674',
    created: 1614560333000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_codewriter" rel="noopener noreferrer" target="_blank">@_codewriter</a> <a class="mention" href="https://x.com/tine2k" rel="noopener noreferrer" target="_blank">@tine2k</a> This is all about the careless use of color to infer meaning. It\'s not at all about offending white people (that is an absurd suggestion). It\'s about being mindful of the way we use words for relaying accurate, descriptive meaning. The word whitespace is just plain lazy.<br><br>In reply to: <a href="https://x.com/_codewriter/status/1366173271692087296" rel="noopener noreferrer" target="_blank">@_codewriter</a> <span class="status">1366173271692087296</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366190348888797184',
    created: 1614560136000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pvaneeckhoudt" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> This has nothing to do with word policing. It\'s about not being careless with terminology. The use of color here is both lazy, serves no purpose, and is distracting. We can do better.<br><br>In reply to: <a href="https://x.com/pvaneeckhoudt/status/1366095514257788937" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> <span class="status">1366095514257788937</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1366189416348143617',
    created: 1614559914000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_codewriter" rel="noopener noreferrer" target="_blank">@_codewriter</a> <a class="mention" href="https://x.com/tine2k" rel="noopener noreferrer" target="_blank">@tine2k</a> You have totally misread this.<br><br>In reply to: <a href="https://x.com/_codewriter/status/1366173271692087296" rel="noopener noreferrer" target="_blank">@_codewriter</a> <span class="status">1366173271692087296</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1365962942580072455',
    created: 1614505918000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tine2k" rel="noopener noreferrer" target="_blank">@tine2k</a> No matter how you apply them, using black or white to label something that has nothing to do with color is going to invite meaning you don\'t intend. It\'s not about this word or that word, it\'s about the mislabeling. It\'s the same problem as whitelist.<br><br>In reply to: <a href="https://x.com/tine2k/status/1365959877009543169" rel="noopener noreferrer" target="_blank">@tine2k</a> <span class="status">1365959877009543169</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1365951930153848832',
    created: 1614503292000,
    type: 'reply',
    text: '@Avalanche1979 <a class="mention" href="https://x.com/justinhopper" rel="noopener noreferrer" target="_blank">@justinhopper</a> No it is not. The emphasis on color in the context is completely unnecessary. A space character already has meaning. And the act of spacing does as well. The use of color here is archaic. The color of space between and around words in technology is entirely fluid.<br><br>In reply to: @Avalanche1979 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1365949143122804737',
    created: 1614502628000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tine2k" rel="noopener noreferrer" target="_blank">@tine2k</a> ...I mean, you could try saying that at a conference with a diverse audience. Let me know how it goes.<br><br>In reply to: <a href="#1365948525247295488">1365948525247295488</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1365948525247295488',
    created: 1614502481000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tine2k" rel="noopener noreferrer" target="_blank">@tine2k</a> Wow. It just is.<br><br>In reply to: <a href="https://x.com/tine2k/status/1365948196409790466" rel="noopener noreferrer" target="_blank">@tine2k</a> <span class="status">1365948196409790466</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1365948407068520450',
    created: 1614502452000,
    type: 'reply',
    text: '@Avalanche1979 <a class="mention" href="https://x.com/justinhopper" rel="noopener noreferrer" target="_blank">@justinhopper</a> Sure, everything comes from something before it. But it was solidified by tech in an unnecessary way. And it\'s time to shed it. At least, I will be doing so in the specs I write.<br><br>In reply to: @Avalanche1979 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1365946688582508544',
    created: 1614502043000,
    type: 'reply',
    text: '@Avalanche1979 <a class="mention" href="https://x.com/justinhopper" rel="noopener noreferrer" target="_blank">@justinhopper</a> The point I was trying to emphasize originally is that we (the tech industry) made up a word when a new word simply wasn\'t necessary.<br><br>In reply to: <a href="#1365946273157685254">1365946273157685254</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1365946273157685254',
    created: 1614501944000,
    type: 'reply',
    text: '@Avalanche1979 <a class="mention" href="https://x.com/justinhopper" rel="noopener noreferrer" target="_blank">@justinhopper</a> I agree. That\'s why I just want to leave the qualifier out entirely as I don\'t think it adds any meaning or value (yet it does add conflict). I keep coming back to "spacing character(s)" as that\'s technically the most accurate use of the English language in this case.<br><br>In reply to: @Avalanche1979 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1365939417152507905',
    created: 1614500309000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/connolly_s" rel="noopener noreferrer" target="_blank">@connolly_s</a> OMG ;)<br><br>In reply to: <a href="https://x.com/connolly_s/status/1365933300154531843" rel="noopener noreferrer" target="_blank">@connolly_s</a> <span class="status">1365933300154531843</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1365925018018848770',
    created: 1614496876000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/justinhopper" rel="noopener noreferrer" target="_blank">@justinhopper</a> I\'m leaning towards "spacing" because the definition fits extremely well. "the act of providing with spaces or placing at intervals". We use these characters to space things out, hence we are "spacing" words.<br><br>In reply to: <a href="https://x.com/justinhopper/status/1365812920832069632" rel="noopener noreferrer" target="_blank">@justinhopper</a> <span class="status">1365812920832069632</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1365924430317166593',
    created: 1614496736000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tine2k" rel="noopener noreferrer" target="_blank">@tine2k</a> Just change white to black and think about how that would sound. Besides, space has absolutely nothing to do with color, so adding color does not enhance the meaning in any way.<br><br>In reply to: <a href="https://x.com/tine2k/status/1365912947327971328" rel="noopener noreferrer" target="_blank">@tine2k</a> <span class="status">1365912947327971328</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1365924014166736899',
    created: 1614496637000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/avishek_20" rel="noopener noreferrer" target="_blank">@avishek_20</a> The problem with "space character" in singular is that represents exactly one specific character (\\u0020). The term in question refers to a whole range of space characters, which includes a tab and often newlines as well.<br><br>In reply to: <a href="https://x.com/avishek_20/status/1365888664761298944" rel="noopener noreferrer" target="_blank">@avishek_20</a> <span class="status">1365888664761298944</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1365843838422585344',
    created: 1614477521000,
    type: 'post',
    text: '...and that the implementation will need to come from the community.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1365841624207515648',
    created: 1614476993000,
    type: 'post',
    text: 'Out of fairness to the community, I\'m trying to be more clear when I have no intention of working on a feature request. That doesn\'t mean I don\'t support the idea. It just means I don\'t have the time to commit to it, and it isn\'t something that I need personally.',
    likes: 10,
    retweets: 0
  },
  {
    id: '1365838289589080064',
    created: 1614476198000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> 🎯<br><br>In reply to: <a href="https://x.com/ALRubinger/status/1365817375048695810" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">1365817375048695810</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1365813159748018177',
    created: 1614470207000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/justinhopper" rel="noopener noreferrer" target="_blank">@justinhopper</a> Thanks for chiming in. It wouldn\'t be empty since they do occupy space. The problem with blank is that sometimes the character has more function than just leaving a blank, like a newline.<br><br>In reply to: <a href="https://x.com/justinhopper/status/1365812717706121220" rel="noopener noreferrer" target="_blank">@justinhopper</a> <span class="status">1365812717706121220</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1365808060791279618',
    created: 1614468991000,
    type: 'post',
    text: 'What is the inclusive, culturally sensitive alternative to "whitespace character"? Would it be "spacing character"? Any other suggestions?',
    likes: 4,
    retweets: 0
  },
  {
    id: '1365625841267273730',
    created: 1614425547000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> ...though they did get the shield by itself, which has proven ineffective for obvious reasons. The one I\'m looking for is the double mask. That\'s me.<br><br>In reply to: <a href="https://x.com/garrett/status/1365593689087688706" rel="noopener noreferrer" target="_blank">@garrett</a> <span class="status">1365593689087688706</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1365437243716870146',
    created: 1614380582000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_codewriter" rel="noopener noreferrer" target="_blank">@_codewriter</a> Yeah, that\'s the harsh reality. I have no disillusions about how it operates, but it doesn\'t stop me from demanding better.<br><br>In reply to: <a href="https://x.com/_codewriter/status/1365433747991101442" rel="noopener noreferrer" target="_blank">@_codewriter</a> <span class="status">1365433747991101442</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1365422755575861248',
    created: 1614377127000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> I strongly encourage you to check out the Razer Book 13. (It\'s 13 but feels like a 14 because it\'s 16:10). It\'s the best laptop I\'ve ever owned by leaps and bounds. It has top of the line components and very well made. (The Stealth is also very good).<br><br>In reply to: <a href="https://x.com/headius/status/1365395498950156289" rel="noopener noreferrer" target="_blank">@headius</a> <span class="status">1365395498950156289</span>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1365422057962438656',
    created: 1614376961000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_codewriter" rel="noopener noreferrer" target="_blank">@_codewriter</a> I shouldn\'t have to know about every last job in the government because every last job should not be blocking the lawmakers we actually elected.<br><br>In reply to: <a href="#1365421634539065344">1365421634539065344</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1365421634539065344',
    created: 1614376860000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_codewriter" rel="noopener noreferrer" target="_blank">@_codewriter</a> Sure, but power keeps coming out of the woodwork to undermine the function of this government to serve the people. These are not normal times, and the last thing we need is unelected officials going against the will and needs of the people. It\'s outrageous.<br><br>In reply to: <a href="https://x.com/_codewriter/status/1365294410381017095" rel="noopener noreferrer" target="_blank">@_codewriter</a> <span class="status">1365294410381017095</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1365243167738454018',
    created: 1614334310000,
    type: 'post',
    text: 'What the f- is the Senate Parliamentarian?!? I\'ve lived in this country for nearly half a century and I\'ve never heard of that role before. One thing is for damn sure. That person should not be deciding shit for this country. Lose it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1365152022568214530',
    created: 1614312580000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dailyburn" rel="noopener noreferrer" target="_blank">@dailyburn</a> To be clear, we love ALL the trainers equally, each in different ways. It\'s the diversity of your programming that make it so strong (and makes us strong too!)<br><br>In reply to: <a href="#1365150645347487744">1365150645347487744</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1365150645347487744',
    created: 1614312251000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dailyburn" rel="noopener noreferrer" target="_blank">@dailyburn</a> Absolutely. Just saw it before our workout today. We\'re going to be all over that one! And we really like Josh! That Reggaeton workout it 🔥!<br><br>In reply to: <a href="https://x.com/dailyburn/status/1365111517318799362" rel="noopener noreferrer" target="_blank">@dailyburn</a> <span class="status">1365111517318799362</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1365149278289547267',
    created: 1614311925000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dailyburn" rel="noopener noreferrer" target="_blank">@dailyburn</a> It was absolutely brilliant. Let JD and Jenna know that it was the perfect idea at the perfect time. We needed that so badly.<br><br>In reply to: <a href="https://x.com/dailyburn/status/1365111361634623493" rel="noopener noreferrer" target="_blank">@dailyburn</a> <span class="status">1365111361634623493</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1365035653025603585',
    created: 1614284835000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gregturn" rel="noopener noreferrer" target="_blank">@gregturn</a> Yeah, we\'ve been following this one. Amazon pulled the plug on KindleGen in a very dramatic way. They left a lot of authors high and dry.<br><br>In reply to: <a href="https://x.com/gregturn/status/1365035081388208129" rel="noopener noreferrer" target="_blank">@gregturn</a> <span class="status">1365035081388208129</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1365033344895913984',
    created: 1614284285000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gregturn" rel="noopener noreferrer" target="_blank">@gregturn</a> Interesting. I know Marat has been following these changes, so he probably knows what\'s going on best. I think there\'s progress on making the KDP-friendly epub as an output option (because it\'s not exactly epub as I understand it).<br><br>In reply to: <a href="https://x.com/gregturn/status/1365019919914516483" rel="noopener noreferrer" target="_blank">@gregturn</a> <span class="status">1365019919914516483</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1364901540197138436',
    created: 1614252860000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/plaindocs" rel="noopener noreferrer" target="_blank">@plaindocs</a> True. True.<br><br>In reply to: <a href="https://x.com/plaindocs/status/1364890194843951105" rel="noopener noreferrer" target="_blank">@plaindocs</a> <span class="status">1364890194843951105</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1364886348360937476',
    created: 1614249238000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dailyburn" rel="noopener noreferrer" target="_blank">@dailyburn</a> And please, please, please do more dance workouts without the dash of HIIT! We love the cardio-strength workouts too, but not when we\'re just looking to just dance. Besides, dance is already intense all on its own.<br><br>In reply to: <a href="#1364885753977659393">1364885753977659393</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1364885753977659393',
    created: 1614249096000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dailyburn" rel="noopener noreferrer" target="_blank">@dailyburn</a> Thank you for this party / workout! My spouse and I had a blast dancing through the decades with Jenna. We had a smile on our faces the whole time. We\'ll definitely be coming back to this one time and again.<br><br>In reply to: <a href="https://x.com/dailyburn/status/1359207112321114112" rel="noopener noreferrer" target="_blank">@dailyburn</a> <span class="status">1359207112321114112</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1364885006892425220',
    created: 1614248918000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/plaindocs" rel="noopener noreferrer" target="_blank">@plaindocs</a> Or the absolute simplicity of returns. I just mark return and drop it off at the partner store down the street. Couldn\'t be easier.<br><br>In reply to: <a href="https://x.com/plaindocs/status/1364878382350098439" rel="noopener noreferrer" target="_blank">@plaindocs</a> <span class="status">1364878382350098439</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1364860850666754051',
    created: 1614243159000,
    type: 'post',
    text: 'I think this answers my question: <a href="https://github.com/zulip/zulip-archive" rel="noopener noreferrer" target="_blank">github.com/zulip/zulip-archive</a><br><br>Would be great if that could be enabled with a click, though I can manage to get it working.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1364859819241312259',
    created: 1614242913000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/zulip" rel="noopener noreferrer" target="_blank">@zulip</a> Is it possible to spy on the archives of a channel without signing in? This is possible in Gitter as you can see here: <a href="https://gitter.im/asciidoctor/asciidoctor" rel="noopener noreferrer" target="_blank">gitter.im/asciidoctor/asciidoctor</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1364857039667695619',
    created: 1614242250000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> I\'ll be honest, too. We\'ve waited for over 5 years for any improvement in Gitter and never saw it. So it\'s nice to be able to enjoy some new capabilities finally.<br><br>In reply to: <a href="#1364853391227191299">1364853391227191299</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1364856773878845449',
    created: 1614242187000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> Credits goes to Quarkus for being the pioneer, at least for us. It was Max and Emmanuel who recommended it to me.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1364855817497956352" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1364855817497956352</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1364853467215396866',
    created: 1614241398000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> What aspects didn\'t you like?<br><br>In reply to: <a href="https://x.com/settermjd/status/1364849824131080192" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1364849824131080192</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1364853391227191299',
    created: 1614241380000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> Streams and topics, full stop. Gitter conversations are extremely disorganized, and threads are cumbersome at best. Zulip\'s design elegantly solves this problem. It also has really nice surprises like custom emoji, custom profile fields, and a custom linkifier. C\'est top!<br><br>In reply to: <a href="https://x.com/marcsavy/status/1364848847361572866" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1364848847361572866</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1364853018986827776',
    created: 1614241292000,
    type: 'reply',
    text: '@ringods Zulip is open source, whereas Discord is not. We prefer open source software, especially when it\'s well designed. And the streams/topic design in Zulip is extremely suitable for our needs. Plus, many users in our community have already tried Zulip and are now advocating for it.<br><br>In reply to: @ringods <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1364722666439995393',
    created: 1614210213000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/djwfyi" rel="noopener noreferrer" target="_blank">@djwfyi</a> Hahaha. Nope, this is a community-driven request.<br><br>In reply to: <a href="https://x.com/djwfyi/status/1364716452247388164" rel="noopener noreferrer" target="_blank">@djwfyi</a> <span class="status">1364716452247388164</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1364716048537194497',
    created: 1614208635000,
    type: 'post',
    text: 'We\'re starting to explore making the leap from Gitter to Zulip to host the Asciidoctor community chat channels.',
    likes: 12,
    retweets: 0
  },
  {
    id: '1364696070144462850',
    created: 1614203872000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rbowen" rel="noopener noreferrer" target="_blank">@rbowen</a> The first item on the list could be:<br><br>[ ] Make a TODO list for today<br><br>That way, it really is a task that you did.<br><br>In reply to: <a href="https://x.com/rbowen/status/1364695273101004804" rel="noopener noreferrer" target="_blank">@rbowen</a> <span class="status">1364695273101004804</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1364646323505356805',
    created: 1614192012000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aheritier" rel="noopener noreferrer" target="_blank">@aheritier</a> The workaround is to add a file extension like .txt.<br><br>In reply to: <a href="https://x.com/aheritier/status/1364601568612544517" rel="noopener noreferrer" target="_blank">@aheritier</a> <span class="status">1364601568612544517</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1364445710767124481',
    created: 1614144182000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Extremely noble indeed.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/1364315692057595904" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">1364315692057595904</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1364120375852822532',
    created: 1614066616000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aheritier" rel="noopener noreferrer" target="_blank">@aheritier</a> I\'m not suggesting that you would accept this long term. This is just a stepping stone to cross, and we will. We know why it needs to happen, but that ever fleeting resource, time, is the constraint we have right now.<br><br>In reply to: <a href="https://x.com/aheritier/status/1364116627407667200" rel="noopener noreferrer" target="_blank">@aheritier</a> <span class="status">1364116627407667200</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1364113507499712515',
    created: 1614064978000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aheritier" rel="noopener noreferrer" target="_blank">@aheritier</a> When we implemented the content aggregator, we started by only scanning the standard hierarchy. We can look other places, but haven\'t yet sorted out the logic. We can\'t grab every file, so we need some way to declare what to pick up and how to classify it.<br><br>In reply to: <a href="https://x.com/aheritier/status/1364096056070397953" rel="noopener noreferrer" target="_blank">@aheritier</a> <span class="status">1364096056070397953</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1364112568856416260',
    created: 1614064755000,
    type: 'post',
    text: 'Today it dawned on me that I\'m vegan not out of guilt, but out of love. Love for the planet. Love for the animals. Love for the plants (that enrich my life and keep me alive). Love for cooking. Love for my fellow humans (especially my partner). Love for my own well-being.',
    likes: 13,
    retweets: 0
  },
  {
    id: '1363998358071775235',
    created: 1614037525000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aheritier" rel="noopener noreferrer" target="_blank">@aheritier</a> Yes, that\'s currently a limitation. See <a href="https://gitlab.com/antora/antora/-/issues/195" rel="noopener noreferrer" target="_blank">gitlab.com/antora/antora/-/issues/195</a><br><br>In reply to: <a href="https://x.com/aheritier/status/1363982866191892481" rel="noopener noreferrer" target="_blank">@aheritier</a> <span class="status">1363982866191892481</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1363972984197632002',
    created: 1614031475000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/waynebeaton" rel="noopener noreferrer" target="_blank">@waynebeaton</a> <a class="mention" href="https://x.com/spotfoss" rel="noopener noreferrer" target="_blank">@spotfoss</a> <a class="mention" href="https://x.com/EclipseFdn" rel="noopener noreferrer" target="_blank">@EclipseFdn</a> That\'s great news. Thanks for clarifying that, Wayne!<br><br>In reply to: <a href="https://x.com/waynebeaton/status/1363971430682136576" rel="noopener noreferrer" target="_blank">@waynebeaton</a> <span class="status">1363971430682136576</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1363970390993764355',
    created: 1614030857000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/spotfoss" rel="noopener noreferrer" target="_blank">@spotfoss</a> I was not previously aware of the distinction between a CLA and a CA. I\'m still trying to discover what my own position is on the matter. <a class="mention" href="https://x.com/waynebeaton" rel="noopener noreferrer" target="_blank">@waynebeaton</a> does the Eclipse CLA grant the rights for the Eclipse Foundation to change the license of a project without permission? 👆<br><br>In reply to: <a href="https://x.com/spotfoss/status/1363879108917272578" rel="noopener noreferrer" target="_blank">@spotfoss</a> <span class="status">1363879108917272578</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1363960846917857281',
    created: 1614028581000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/melissajmckay" rel="noopener noreferrer" target="_blank">@melissajmckay</a> Congratulations Melissa! It\'s an honor to have you as part of this group!<br><br>In reply to: <a href="https://x.com/melissajmckay/status/1363960088554213376" rel="noopener noreferrer" target="_blank">@melissajmckay</a> <span class="status">1363960088554213376</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1363949417745682437',
    created: 1614025856000,
    type: 'reply',
    text: '@wmhilton <a class="mention" href="https://x.com/knownasilya" rel="noopener noreferrer" target="_blank">@knownasilya</a> I love BBQ too. I have uncles who used to run a BBQ restaurant, so I know what good BBQ tastes like. It\'s all in the sauce. Jackfruit fills in perfectly for the meat and soaks up even more of the flavor. And bonus, no bones or slimy bits to worry about.<br><br>In reply to: @wmhilton <span class="status">&lt;deleted&gt;</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1363948573939802112',
    created: 1614025655000,
    type: 'reply',
    text: '@wmhilton Here\'s the recipe we use. It\'s out of this world. <a href="https://www.badmanners.com/recipes/cola-braised-jackfruit" rel="noopener noreferrer" target="_blank">www.badmanners.com/recipes/cola-braised-jackfruit</a><br><br>In reply to: <a href="#1363948323590184960">1363948323590184960</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1363948323590184960',
    created: 1614025595000,
    type: 'reply',
    text: '@wmhilton I\'m thrilled to share with you that there\'s is a replacement that can only be described as magical. It\'s jackfruit. That fruit is so deceptively close to the texture of pulled meat (yet way more flavorful) that you could trick someone into thinking it is.<br><br>In reply to: @wmhilton <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1363947805362905090',
    created: 1614025472000,
    type: 'reply',
    text: '@wmhilton If it helps, I can tell you that you won\'t miss eating meat. The world of vegetarian / vegan dishes adds such an incredible spice to life that you\'ll look back at meat-based meals and think "how boring". At least that\'s been my experience.<br><br>In reply to: @wmhilton <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1363944443489910786',
    created: 1614024670000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/KonaTech140" rel="noopener noreferrer" target="_blank">@KonaTech140</a> That\'s what I\'m most excited about. I\'m about to try that next. The first batch I did I already had soaked beans. But that\'s the part I really want to eliminate.<br><br>In reply to: <a href="https://x.com/KonaTech140/status/1363939744351014912" rel="noopener noreferrer" target="_blank">@KonaTech140</a> <span class="status">1363939744351014912</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1363819747884691456',
    created: 1613994941000,
    type: 'post',
    text: 'Got a pressure cooker today. Watch out beans! 🫕',
    likes: 2,
    retweets: 0
  },
  {
    id: '1363649319681724416',
    created: 1613954307000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> Love that one.<br><br>In reply to: <a href="https://x.com/settermjd/status/1363587786964631558" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1363587786964631558</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1363456113488891905',
    created: 1613908243000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sparkycollier" rel="noopener noreferrer" target="_blank">@sparkycollier</a> I\'m glad to hear you\'re doing okay, though I know that many of your neighbors are not. I\'ve donated to several relief funds to help them out. Stay strong!<br><br>In reply to: <a href="https://x.com/sparkycollier/status/1363352528239345667" rel="noopener noreferrer" target="_blank">@sparkycollier</a> <span class="status">1363352528239345667</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1362946305287315458',
    created: 1613786696000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/zauberfinger" rel="noopener noreferrer" target="_blank">@zauberfinger</a> Here\'s a site I\'m often reminded of that aims to cover that topic: <a href="https://documentation.divio.com/" rel="noopener noreferrer" target="_blank">documentation.divio.com/</a><br><br>In reply to: <a href="https://x.com/zauberfinger/status/1362701259661279233" rel="noopener noreferrer" target="_blank">@zauberfinger</a> <span class="status">1362701259661279233</span>',
    likes: 4,
    retweets: 1
  },
  {
    id: '1362299551801991168',
    created: 1613632498000,
    type: 'post',
    text: 'There seem to be a number of competing offers in this space. I look forward to trying more of them out to find out which one best serves my community.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1362293347788812289',
    created: 1613631018000,
    type: 'quote',
    text: 'If you\'re looking for a platform to host a virtual expo or breakout sessions (e.g., unconference or BOFs), check out <a href="https://spatial.chat/" rel="noopener noreferrer" target="_blank">spatial.chat/</a>. It offers the closest experience I\'ve ever seen to a physical hall. By moving your live avatar around, it tunes you into the conversation!<br><br>Quoting: <a href="https://x.com/ameliaeiras/status/1362252568378810369" rel="noopener noreferrer" target="_blank">@ameliaeiras</a> <span class="status">1362252568378810369</span>',
    likes: 11,
    retweets: 2
  },
  {
    id: '1362206320800137217',
    created: 1613610270000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> 🥰<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/1362191642858577926" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">1362191642858577926</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1362073816755625993',
    created: 1613578678000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tiesebarrell" rel="noopener noreferrer" target="_blank">@tiesebarrell</a> Indeed. It\'s going to happen, once I get the chance. I\'d like to coincide it with the Asciidoctor PDF 2 release (which thus far has proved elusive, again due to time).<br><br>In reply to: <a href="https://x.com/tiesebarrell/status/1362073090738581507" rel="noopener noreferrer" target="_blank">@tiesebarrell</a> <span class="status">1362073090738581507</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1361940612438659076',
    created: 1613546920000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tiesebarrell" rel="noopener noreferrer" target="_blank">@tiesebarrell</a> Time.<br><br>In reply to: <a href="https://x.com/tiesebarrell/status/1361930232526221313" rel="noopener noreferrer" target="_blank">@tiesebarrell</a> <span class="status">1361930232526221313</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1361631732802281473',
    created: 1613473277000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> I wouldn\'t, because I wouldn\'t get on a plane. Not until we start taking climate change seriously.<br><br>In reply to: <a href="https://x.com/starbuxman/status/1360313794925260800" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">1360313794925260800</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1361620664457723910',
    created: 1613470638000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/chacon" rel="noopener noreferrer" target="_blank">@chacon</a> I don\'t think cost is the right deterrent for squatting. That just favors people who have wealth. It doesn\'t actually address the problem.<br><br>In reply to: <a href="https://x.com/chacon/status/1361615833525612545" rel="noopener noreferrer" target="_blank">@chacon</a> <span class="status">1361615833525612545</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1361592855605567488',
    created: 1613464008000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iphigenie" rel="noopener noreferrer" target="_blank">@iphigenie</a> I don\'t know whether a domain I just renewed changed in premium status, but the price did double. And that has not been uncommon in my experience. (The price usually changes on first renewal, but my billing shows regular fluctuation in cost for even renewals).<br><br>In reply to: <a href="https://x.com/iphigenie/status/1361591558492069891" rel="noopener noreferrer" target="_blank">@iphigenie</a> <span class="status">1361591558492069891</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1361591485947252736',
    created: 1613463682000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iphigenie" rel="noopener noreferrer" target="_blank">@iphigenie</a> Not that this is the only issue in the world. And probably not the most important either. Just my thoughts on it.<br><br>In reply to: <a href="#1361591197433618436">1361591197433618436</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1361591197433618436',
    created: 1613463613000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iphigenie" rel="noopener noreferrer" target="_blank">@iphigenie</a> That\'s just where I stand on the issue. Registrars should be paid for the service they do, but be highly regulated and prices negotiated by the gov\'t or similar body.<br><br>In reply to: <a href="#1361590872505131012">1361590872505131012</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1361590872505131012',
    created: 1613463535000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iphigenie" rel="noopener noreferrer" target="_blank">@iphigenie</a> We seem to agree that the names themselves are pretty meaningless (despite some TLDs being in the luxury car price range). Given that, I think anyone who wants a domain name to start/run a business should be able to get one free of cost (perhaps from gov\'t or other civic works).<br><br>In reply to: <a href="https://x.com/iphigenie/status/1361589604663652358" rel="noopener noreferrer" target="_blank">@iphigenie</a> <span class="status">1361589604663652358</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1361588391347818498',
    created: 1613462944000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iphigenie" rel="noopener noreferrer" target="_blank">@iphigenie</a> Where they get you though is in an established name. New ones can be cheap. But once it becomes part of your brand, you have to keep it indefinitely. And that\'s when they start making money from your success, and the cost is less reliable with rare, if any, multi-year discounts.<br><br>In reply to: <a href="https://x.com/iphigenie/status/1361587439907860482" rel="noopener noreferrer" target="_blank">@iphigenie</a> <span class="status">1361587439907860482</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1361587471796998149',
    created: 1613462725000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iphigenie" rel="noopener noreferrer" target="_blank">@iphigenie</a> Fortunately (and I feel some shame in using that word here), I can afford it. But I have already migrated so many times because so many registrars seem to be scum (which is yet another problem of being mostly unregulated).<br><br>In reply to: <a href="https://x.com/iphigenie/status/1361586936020893697" rel="noopener noreferrer" target="_blank">@iphigenie</a> <span class="status">1361586936020893697</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1361586742831116289',
    created: 1613462551000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iphigenie" rel="noopener noreferrer" target="_blank">@iphigenie</a> *registrars<br><br>In reply to: <a href="#1361586039064723456">1361586039064723456</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1361586617119432707',
    created: 1613462521000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iphigenie" rel="noopener noreferrer" target="_blank">@iphigenie</a> So I totally agree the argument is really against the gatekeeping. We should be looking very carefully at mechanisms that are increasing the wealth gap in this country and around the world. And web properties &amp; tech are at the center of it.<br><br>In reply to: <a href="#1361586039064723456">1361586039064723456</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1361586039064723456',
    created: 1613462383000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iphigenie" rel="noopener noreferrer" target="_blank">@iphigenie</a> I want to be clear, I\'m not advocating for registers not getting paid. Everyone should be paid for honest work. It\'s just about who pays it. The money should be collected in such a way that it does not disproportionately impact lower-income people. Taxes come to mind. (con\'t)<br><br>In reply to: <a href="https://x.com/iphigenie/status/1361585483646664705" rel="noopener noreferrer" target="_blank">@iphigenie</a> <span class="status">1361585483646664705</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1361584865062182915',
    created: 1613462103000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/seijoed" rel="noopener noreferrer" target="_blank">@seijoed</a> I love mint. It\'s such a fighter.<br><br>In reply to: @seijoed <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1361584427017465860',
    created: 1613461999000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iphigenie" rel="noopener noreferrer" target="_blank">@iphigenie</a> <a href="https://news.gandi.net/en/2020/09/how-much-does-a-domain-cost-and-what-comes-with-it" rel="noopener noreferrer" target="_blank">news.gandi.net/en/2020/09/how-much-does-a-domain-cost-and-what-comes-with-it</a><br><br>In reply to: <a href="#1361583360372432900">1361583360372432900</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1361583899982241793',
    created: 1613461873000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iphigenie" rel="noopener noreferrer" target="_blank">@iphigenie</a> Another major problem currently is the lack of universal pricing. One registrar can charge significantly more than another for doing exactly the same thing. And these prices can fluctuate year-by-year so you can end up being asked for more money for the same web property.<br><br>In reply to: <a href="#1361582666382893056">1361582666382893056</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1361583360372432900',
    created: 1613461744000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iphigenie" rel="noopener noreferrer" target="_blank">@iphigenie</a> I was reading an article for <a href="http://gandi.net" rel="noopener noreferrer" target="_blank">gandi.net</a> trying to justify why domains need to cost so much. And I\'m just thinking daily about inequality, justice, and access to building wealth for people without the privilege of inheritance.<br><br>In reply to: <a href="https://x.com/iphigenie/status/1361580928842289155" rel="noopener noreferrer" target="_blank">@iphigenie</a> <span class="status">1361580928842289155</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1361582666382893056',
    created: 1613461579000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iphigenie" rel="noopener noreferrer" target="_blank">@iphigenie</a> It\'s good that some TLDs are run that way. But the fact that there\'s so much inequity between TLDs is also very problematic &amp; confusing. Having to pay &gt; $20/year for a single domain renewal is unnecessarily expensive &amp; reinforces severe socioeconomic barriers we have today.<br><br>In reply to: <a href="https://x.com/iphigenie/status/1361580053558145025" rel="noopener noreferrer" target="_blank">@iphigenie</a> <span class="status">1361580053558145025</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1361579383551533059',
    created: 1613460796000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Yes, the value is in the power structure it creates to keep marginalized people from getting to play in the fiefdoms owned by the rich. It\'s garbage.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1361566154821615623" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1361566154821615623</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1361496007050076160',
    created: 1613440918000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/afazrihady" rel="noopener noreferrer" target="_blank">@afazrihady</a> Strong disagree. This system is stronghold of privilege. All it effectively does is keep underrepresented groups out of tech. There are other ways to socialize names that doesn\'t have this drawback. One example is registration limits. Another is utility (whether it\'s in use).<br><br>In reply to: <a href="https://x.com/afazrihady/status/1361494445363879936" rel="noopener noreferrer" target="_blank">@afazrihady</a> <span class="status">1361494445363879936</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1361488179077935105',
    created: 1613439051000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iphigenie" rel="noopener noreferrer" target="_blank">@iphigenie</a> ...which can be covered as public works (basically a civic service).<br><br>In reply to: <a href="https://x.com/iphigenie/status/1361475369392562181" rel="noopener noreferrer" target="_blank">@iphigenie</a> <span class="status">1361475369392562181</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1361473899909713924',
    created: 1613435647000,
    type: 'post',
    text: 'I mean, it\'s a freakin\' mapping between a sequence of characters and a number. The fact that they charge for this is extortion, plain and simple.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1361473428973228033',
    created: 1613435535000,
    type: 'post',
    text: 'Gen Z, while you\'re busy tearing down institutions of oppression and inequality, add this one to your list. (I\'ll be right there with you).<br><br>Quoting: <a href="#1361472293231202304">1361472293231202304</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1361472543475990528',
    created: 1613435323000,
    type: 'post',
    text: 'Sure, there may need to be some sort of nominal acquisition cost just to manage access to them (so someone doesn\'t sweep up and take every domain in the world), but once ownership is established, they should be free.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1361472293231202304',
    created: 1613435264000,
    type: 'post',
    text: 'domain names should be free',
    likes: 4,
    retweets: 1
  },
  {
    id: '1361438065609248770',
    created: 1613427103000,
    type: 'post',
    text: 'For those looking for some deep dive tech content, all workshops at <a class="mention" href="https://x.com/devnexus" rel="noopener noreferrer" target="_blank">@devnexus</a> later this week are free and virtual (Thursday, EST, limited spots available). <a href="https://www.eventbrite.com/e/workshop-day-at-devnexus-tickets-136914098755?discount=FREE" rel="noopener noreferrer" target="_blank">www.eventbrite.com/e/workshop-day-at-devnexus-tickets-136914098755?discount=FREE</a>',
    likes: 2,
    retweets: 4
  },
  {
    id: '1361397472925392897',
    created: 1613417425000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pvaneeckhoudt" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> <a class="mention" href="https://x.com/bobbytank42" rel="noopener noreferrer" target="_blank">@bobbytank42</a> Ack. It\'s extremely negative.<br><br>In reply to: <a href="https://x.com/pvaneeckhoudt/status/1361323802261676035" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> <span class="status">1361323802261676035</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1361278633210355717',
    created: 1613389092000,
    type: 'post',
    text: 'As my grandfather used to say, it takes a long time to earn trust, but you can destroy entirely with a single act.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1361278101188083715',
    created: 1613388965000,
    type: 'post',
    text: 'Just leaving this here since too many of my fellow citizens seem to have forgotten what this word means.',
    photos: ['<div class="item"><img class="photo" src="media/1361278101188083715.jpg"></div>'],
    likes: 2,
    retweets: 1
  },
  {
    id: '1361276973880774660',
    created: 1613388696000,
    type: 'post',
    text: 'But I know that I will fight for justice and equality for as long as I live. Because, symbols aside, that\'s what I was taught to value. It\'s what makes me who I am. And I will never be a traitor to myself or those values.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1361276682523406338',
    created: 1613388627000,
    type: 'post',
    text: 'I don\'t even know what I can say that would make a goddamn difference. I\'m so disgusted right now I feel like a traitor to justice just being a citizen of this country.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1361276555435986948',
    created: 1613388596000,
    type: 'post',
    text: 'I took this photo of the U.S. Capitol under moonlight in 2010. This building used to mean something to me. I grew up just outside The Beltway and took many field trips to The Mall. All the symbolism about this place, which has been central to my life, has been shattered.',
    photos: ['<div class="item"><img class="photo" src="media/1361276555435986948.jpg"></div>'],
    likes: 2,
    retweets: 0
  },
  {
    id: '1361273372047413250',
    created: 1613387837000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/hsablonniere" rel="noopener noreferrer" target="_blank">@hsablonniere</a> Came across this image I took of Big Ben during my JAX London 2011 trip and I thought of you.',
    photos: ['<div class="item"><img class="photo" src="media/1361273372047413250.jpg"></div>'],
    likes: 1,
    retweets: 0
  },
  {
    id: '1360570213817745415',
    created: 1613220191000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/recena" rel="noopener noreferrer" target="_blank">@recena</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> There are no official themes. The expectation is that most users develop their own look and feel. But you can find tons of examples here: <a href="https://gitlab.com/antora/antora.org/-/issues/20" rel="noopener noreferrer" target="_blank">gitlab.com/antora/antora.org/-/issues/20</a><br><br>In reply to: <a href="https://x.com/recena/status/1360549586151092227" rel="noopener noreferrer" target="_blank">@recena</a> <span class="status">1360549586151092227</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1359770308190687232',
    created: 1613029479000,
    type: 'quote',
    text: 'Everyone should have access to free c̶o̶m̶m̶u̶n̶i̶t̶y̶ ̶c̶o̶l̶l̶e̶g̶e̶ public colleges, universities, and trade schools.<br><br>Quoting: <a href="https://x.com/FLOTUS/status/1359601608607346695" rel="noopener noreferrer" target="_blank">@FLOTUS</a> <span class="status">1359601608607346695</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1359613772885749760',
    created: 1612992158000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/justinhopper" rel="noopener noreferrer" target="_blank">@justinhopper</a> DailyBurn. And the mobility trainer is Gregg Cook.<br><br>In reply to: <a href="https://x.com/justinhopper/status/1359606042057793538" rel="noopener noreferrer" target="_blank">@justinhopper</a> <span class="status">1359606042057793538</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1359596137171152896',
    created: 1612987953000,
    type: 'post',
    text: 'If there\'s only one type of workout you have time for, make it mobility. It has drastically improved how I feel, my posture, and, you guessed it, my mobility. (And I only do it twice a week for 30 minutes).',
    likes: 2,
    retweets: 0
  },
  {
    id: '1359040635534102528',
    created: 1612855511000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> A lot of ttfunk is understanding font tables. So even though I work with Ruby on a daily basis, I\'m still lost looking at those tables. There are some I know, but a lot are just cryptic to me.<br><br>In reply to: <a href="https://x.com/odrotbohm/status/1359040347104563200" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <span class="status">1359040347104563200</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1359040021181812739',
    created: 1612855365000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> As pointlessone states in the issue, ttfunk isn\'t extracting the ligature tables from the font. Thus, Prawn wouldn\'t know to apply them anyway. If ttfunk did extract them, then Asciidoctor PDF could do the replacement itself (using the same method that applies hyphenation).<br><br>In reply to: <a href="https://x.com/odrotbohm/status/1359038708461301762" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <span class="status">1359038708461301762</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1358970633346981889',
    created: 1612838822000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> The change would actually be in ttfunk, which is the library that handles font metrics in Prawn. Another option is Asciidoctor Web PDF, which uses browser tech to make PDFs from HTML. Then you get advanced font capabilities "for free". Font rendering is complex.<br><br>In reply to: <a href="https://x.com/odrotbohm/status/1358821747655639041" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <span class="status">1358821747655639041</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1358524304497856514',
    created: 1612732409000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/simonstl" rel="noopener noreferrer" target="_blank">@simonstl</a> That\'s really too bad to hear because Cornell has been taking a very aggressive and science-based approach to containing the virus. I hope they can get back on track quickly. And those who violated the guidelines should be disciplined sharply.<br><br>In reply to: <a href="https://x.com/simonstl/status/1358516707241955328" rel="noopener noreferrer" target="_blank">@simonstl</a> <span class="status">1358516707241955328</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1358523332413431809',
    created: 1612732177000,
    type: 'quote',
    text: 'This is absolutely going to be all of us when we find out the pandemic is over and the partying can resume. Our feet will be 10 leaps ahead of our minds.<br><br>Quoting: <a href="https://x.com/mufasatweetss/status/1182714033557905408" rel="noopener noreferrer" target="_blank">@mufasatweetss</a> <span class="status">1182714033557905408</span>',
    likes: 8,
    retweets: 0
  },
  {
    id: '1358493095973638144',
    created: 1612724968000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> Actually, that\'s exactly what I do when there\'s only a bunch of broken bits left. And it\'s pretty darn good 😋<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/1358218979307909133" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">1358218979307909133</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1357998483752423430',
    created: 1612607043000,
    type: 'reply',
    text: '@RealKaylaJames Mainz. Loved my time there. Got to visit the Gutenberg museum and learn about early printing. Sadly, I\'ve never been to Berlin.<br><br>In reply to: @RealKaylaJames <span class="status">&lt;deleted&gt;</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1357833198252617728',
    created: 1612567636000,
    type: 'quote',
    text: 'Reference sample.<br><br>Quoting: <a href="https://x.com/SheriffAlert/status/1357718112599240706" rel="noopener noreferrer" target="_blank">@SheriffAlert</a> <span class="status">1357718112599240706</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1357443212131012608',
    created: 1612474656000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tcurdt" rel="noopener noreferrer" target="_blank">@tcurdt</a> <a class="mention" href="https://x.com/rhatdan" rel="noopener noreferrer" target="_blank">@rhatdan</a> I don\'t use Docker compose. As for both running images and building them using a Dockerfile, I have never run into any issues. Like I said, I find no need for Docker in my daily workflow.<br><br>In reply to: <a href="https://x.com/tcurdt/status/1357442781363470339" rel="noopener noreferrer" target="_blank">@tcurdt</a> <span class="status">1357442781363470339</span>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1357440939841650688',
    created: 1612474114000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tcurdt" rel="noopener noreferrer" target="_blank">@tcurdt</a> <a class="mention" href="https://x.com/rhatdan" rel="noopener noreferrer" target="_blank">@rhatdan</a> I\'ve been using it now for close to two years and I can tell you it\'s a joy to use. I don\'t even have Docker set up on my machine anymore.<br><br>In reply to: <a href="https://x.com/tcurdt/status/1357440416065474564" rel="noopener noreferrer" target="_blank">@tcurdt</a> <span class="status">1357440416065474564</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1357090765269204992',
    created: 1612390626000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SachaLabourey" rel="noopener noreferrer" target="_blank">@SachaLabourey</a> <a class="mention" href="https://x.com/swdoutloud" rel="noopener noreferrer" target="_blank">@swdoutloud</a> Congrats boss!<br><br>In reply to: <a href="https://x.com/SachaLabourey/status/1356974429729935361" rel="noopener noreferrer" target="_blank">@SachaLabourey</a> <span class="status">1356974429729935361</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1357040020306104320',
    created: 1612378528000,
    type: 'post',
    text: 'If you write docs using AsciiDoc and Antora, or are curious about this powerful combo, be sure to check out this session at FOSDEM hosted by <a class="mention" href="https://x.com/ahus1de" rel="noopener noreferrer" target="_blank">@ahus1de</a>. <a href="https://fosdem.org/2021/schedule/event/ttdasciidocantora/" rel="noopener noreferrer" target="_blank">fosdem.org/2021/schedule/event/ttdasciidocantora/</a>',
    likes: 23,
    retweets: 14
  },
  {
    id: '1357033848534487041',
    created: 1612377056000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AndyBai59873550" rel="noopener noreferrer" target="_blank">@AndyBai59873550</a> <a class="mention" href="https://x.com/CedricChampeau" rel="noopener noreferrer" target="_blank">@CedricChampeau</a> I\'ll be honest, I\'m not sure how the article really helps. It gives us dates, but it doesn\'t actually tell us what code to change. So it\'s really just a press release. Does reading it actually help all that much in this case?<br><br>In reply to: <a href="https://x.com/AndyBai59873550/status/1357032357044375557" rel="noopener noreferrer" target="_blank">@AndyBai59873550</a> <span class="status">1357032357044375557</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1357026222849421312',
    created: 1612375238000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AndyBai59873550" rel="noopener noreferrer" target="_blank">@AndyBai59873550</a> <a class="mention" href="https://x.com/CedricChampeau" rel="noopener noreferrer" target="_blank">@CedricChampeau</a> I\'ll also add that this is extremely short notice. Do you think those of us who work on open source can just drop everything and work on it immediately? It\'s not our day job.<br><br>In reply to: <a href="#1357024911504539648">1357024911504539648</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1357025565513928705',
    created: 1612375081000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AndyBai59873550" rel="noopener noreferrer" target="_blank">@AndyBai59873550</a> <a class="mention" href="https://x.com/CedricChampeau" rel="noopener noreferrer" target="_blank">@CedricChampeau</a> But I retweeted simply to make sure others are seeing this. Because action is necessary, one way or another.<br><br>In reply to: <a href="#1357024911504539648">1357024911504539648</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1357025262798401538',
    created: 1612375009000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AndyBai59873550" rel="noopener noreferrer" target="_blank">@AndyBai59873550</a> <a class="mention" href="https://x.com/CedricChampeau" rel="noopener noreferrer" target="_blank">@CedricChampeau</a> This is on top of just having to rewrite ALL our builds to move away from Travis CI. I\'m really burnt out right now on builds.<br><br>In reply to: <a href="#1357024911504539648">1357024911504539648</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1357024911504539648',
    created: 1612374925000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AndyBai59873550" rel="noopener noreferrer" target="_blank">@AndyBai59873550</a> <a class="mention" href="https://x.com/CedricChampeau" rel="noopener noreferrer" target="_blank">@CedricChampeau</a> The fuss is that many, many projects now have to stop forward progress to make time to fix their automated publishing process, which is always really, really hard to get working correctly. (I should know). So yeah, it\'s a big deal.<br><br>In reply to: <a href="https://x.com/AndyBai59873550/status/1357023911771308035" rel="noopener noreferrer" target="_blank">@AndyBai59873550</a> <span class="status">1357023911771308035</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1357023812773007360',
    created: 1612374663000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/spotfoss" rel="noopener noreferrer" target="_blank">@spotfoss</a> As I understand it from a Belgian, it\'s the same recipe passed around (comes from Westmalle) and this is the version that is exported. (I\'ve had both).<br><br>In reply to: <a href="https://x.com/spotfoss/status/1357022764926980097" rel="noopener noreferrer" target="_blank">@spotfoss</a> <span class="status">1357022764926980097</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1357022464237211648',
    created: 1612374342000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/spotfoss" rel="noopener noreferrer" target="_blank">@spotfoss</a> St Bernardus Abt 12 is basically the same and available throughout the states. <a href="https://www.sintbernardus.be/en/brewery/our-beers/stbernardus-abt-12-en" rel="noopener noreferrer" target="_blank">www.sintbernardus.be/en/brewery/our-beers/stbernardus-abt-12-en</a><br><br>In reply to: <a href="https://x.com/spotfoss/status/1357015642436739076" rel="noopener noreferrer" target="_blank">@spotfoss</a> <span class="status">1357015642436739076</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1357021218818322433',
    created: 1612374045000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rotnroll666" rel="noopener noreferrer" target="_blank">@rotnroll666</a> I am seriously impressed! I would say bon appetit, but it looks like it\'s too late for that. Bravo!<br><br>In reply to: <a href="https://x.com/rotnroll666/status/1357014467058532354" rel="noopener noreferrer" target="_blank">@rotnroll666</a> <span class="status">1357014467058532354</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1356943718847180801',
    created: 1612355568000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/__Cloudia" rel="noopener noreferrer" target="_blank">@__Cloudia</a> What I wouldn\'t give for another one of those moments right now.<br><br>In reply to: <a href="https://x.com/__Cloudia/status/1356932335661375491" rel="noopener noreferrer" target="_blank">@__Cloudia</a> <span class="status">1356932335661375491</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1356920980040425474',
    created: 1612350146000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/__Cloudia" rel="noopener noreferrer" target="_blank">@__Cloudia</a> Check out the picture I found when reorganizing the files on my computer. Remember when happy hours were a thing? Good times.',
    photos: ['<div class="item"><img class="photo" src="media/1356920980040425474.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1356459043208880128',
    created: 1612240012000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_codewriter" rel="noopener noreferrer" target="_blank">@_codewriter</a> <a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> Certainly feels like there was no life before Fedora at this point. But I totally get the sentiment and am still on the lookout for great interfaces.<br><br>In reply to: <a href="https://x.com/_codewriter/status/1356457560627437569" rel="noopener noreferrer" target="_blank">@_codewriter</a> <span class="status">1356457560627437569</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1356451341837561857',
    created: 1612238176000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_codewriter" rel="noopener noreferrer" target="_blank">@_codewriter</a> <a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> Hmm. I\'ve never tried PopOS, but now I\'m intrigued.<br><br>In reply to: <a href="https://x.com/_codewriter/status/1356432659648512002" rel="noopener noreferrer" target="_blank">@_codewriter</a> <span class="status">1356432659648512002</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1355969865207562241',
    created: 1612123383000,
    type: 'reply',
    text: '@wmhilton <a class="mention" href="https://x.com/IsomorphicGit" rel="noopener noreferrer" target="_blank">@IsomorphicGit</a> Maybe a newsletter? I\'d certainly be interested.<br><br>In reply to: @wmhilton <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1355826289337266181',
    created: 1612089152000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JnBrymn" rel="noopener noreferrer" target="_blank">@JnBrymn</a> <a class="mention" href="https://x.com/struberg" rel="noopener noreferrer" target="_blank">@struberg</a> Remove duplicates. It\'s almost impossible to search since so many results are just the same file over and over. And it\'s not just exact duplicates, but like duplicates as well. I\'m looking for variety in results.<br><br>In reply to: <a href="https://x.com/JnBrymn/status/1355654689652690953" rel="noopener noreferrer" target="_blank">@JnBrymn</a> <span class="status">1355654689652690953</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1355642642814517248',
    created: 1612045367000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/indianatwork" rel="noopener noreferrer" target="_blank">@indianatwork</a> I\'m not eligible at this point. Right now, it\'s for the people who are most vulnerable. I will be sure to get it as soon as it is my time.<br><br>In reply to: <a href="https://x.com/indianatwork/status/1355636719127842818" rel="noopener noreferrer" target="_blank">@indianatwork</a> <span class="status">1355636719127842818</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1355624610616627200',
    created: 1612041068000,
    type: 'quote',
    text: 'Colorado getting it done. Let\'s eradicate this 👿🦠.<br><br>Quoting: <a href="https://x.com/mullen_david/status/1355573420956778497" rel="noopener noreferrer" target="_blank">@mullen_david</a> <span class="status">1355573420956778497</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1355622218353373184',
    created: 1612040497000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nithyaruff" rel="noopener noreferrer" target="_blank">@nithyaruff</a> After grocery shopping in person with a mask for most of the pandemic, we switched to ordering groceries and beverages online at the start of the year. Now the only in-store appearance is to the pharmacy (ironically) since that has proved harder to avoid.<br><br>In reply to: <a href="https://x.com/nithyaruff/status/1355525780680355843" rel="noopener noreferrer" target="_blank">@nithyaruff</a> <span class="status">1355525780680355843</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1355422670452477953',
    created: 1611992921000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nearyd" rel="noopener noreferrer" target="_blank">@nearyd</a> We have been conditioned to believe we should have less (as a society) if we think all of these couldn\'t be done at the flip of a switch. The truth is, we can do far, far more than these very basic three. It\'s absolutely absurd to observe what we\'ve done to ourselves.<br><br>In reply to: <a href="https://x.com/nearyd/status/1355406898242215938" rel="noopener noreferrer" target="_blank">@nearyd</a> <span class="status">1355406898242215938</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1355421432113549317',
    created: 1611992626000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nearyd" rel="noopener noreferrer" target="_blank">@nearyd</a> I am not thinking about myself.<br><br>In reply to: <a href="https://x.com/nearyd/status/1355406898242215938" rel="noopener noreferrer" target="_blank">@nearyd</a> <span class="status">1355406898242215938</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1355406146295615488',
    created: 1611988982000,
    type: 'quote',
    text: 'I refuse to choose. It\'s a false dichotomy. We can and will have all three. Anything less is cruelty.<br><br>Quoting: <a href="https://x.com/TaraLMosley/status/1355212293689274368" rel="noopener noreferrer" target="_blank">@TaraLMosley</a> <span class="status">1355212293689274368</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1355279884499578883',
    created: 1611958879000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rotnroll666" rel="noopener noreferrer" target="_blank">@rotnroll666</a> That\'s so cool and impressive! Looking back now, I really wish I had learned to cook in my youth. I would have enjoyed much more freedom in my adult life. And it would have made me a better partner (which I\'m trying to make up for now).<br><br>In reply to: <a href="https://x.com/rotnroll666/status/1355267137519157251" rel="noopener noreferrer" target="_blank">@rotnroll666</a> <span class="status">1355267137519157251</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1355265543196999680',
    created: 1611955459000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rotnroll666" rel="noopener noreferrer" target="_blank">@rotnroll666</a> I say that to myself after every new meal I enjoy as well as my favorites I return to. I thought going into it that I\'d always know that I\'m eating vegan food. But I honestly don\'t. What\'s more, I\'d argue that my culinary experience has enhanced substantially.<br><br>In reply to: <a href="https://x.com/rotnroll666/status/1355036768135950336" rel="noopener noreferrer" target="_blank">@rotnroll666</a> <span class="status">1355036768135950336</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1355136475499614209',
    created: 1611924687000,
    type: 'post',
    text: 'It used to be that to charge your device, you\'d just looked for an outlet. Now, every wall charger requires like a dozen certifications.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1355136024175726595',
    created: 1611924580000,
    type: 'post',
    text: 'One of the side effects of working out consistently is that it makes you want to dance whenever you hear music playing. 🎶🕺',
    likes: 2,
    retweets: 0
  },
  {
    id: '1354942947771305986',
    created: 1611878547000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> Indeed.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1354917033520078852" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1354917033520078852</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1354914187802099712',
    created: 1611871690000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> Not much of a point with those exceptions, so we certainly need to demand it be applied universally (and perhaps even progressively).<br><br>In reply to: <a href="https://x.com/marcsavy/status/1354910843390066692" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1354910843390066692</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1354907783850819588',
    created: 1611870163000,
    type: 'post',
    text: 'If we had a transaction / speculation tax on Wall Street, we could probably fund most of the proposed social programs, including the $2K checks. Now do you get how effective it could be?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1354739221412474883',
    created: 1611829974000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/andypiper" rel="noopener noreferrer" target="_blank">@andypiper</a> Had the privilege of hearing him perform live back in \'00. Brilliant musician.<br><br>In reply to: @andypiper <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1354543065495605250',
    created: 1611783207000,
    type: 'post',
    text: 'Build Back Fossil Free',
    likes: 2,
    retweets: 1
  },
  {
    id: '1354184143215620098',
    created: 1611697633000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/caroljmcdonald" rel="noopener noreferrer" target="_blank">@caroljmcdonald</a> Ah yes, I saw that one. There seem to be a number of companies using a similar design.<br><br>In reply to: <a href="https://x.com/caroljmcdonald/status/1354160533931315200" rel="noopener noreferrer" target="_blank">@caroljmcdonald</a> <span class="status">1354160533931315200</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1353184083921100800',
    created: 1611459201000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sjmaple" rel="noopener noreferrer" target="_blank">@sjmaple</a> <a class="mention" href="https://x.com/russel_winder" rel="noopener noreferrer" target="_blank">@russel_winder</a> I just saw this news. I\'m gutted. 😢 Russel was one of a kind. He was also very gracious, kind, and sure to light up a room with his sparkling lustrous personality. He\'ll be missed dearly.<br><br>In reply to: <a href="https://x.com/sjmaple/status/1353089731018567680" rel="noopener noreferrer" target="_blank">@sjmaple</a> <span class="status">1353089731018567680</span>',
    likes: 8,
    retweets: 0
  },
  {
    id: '1352949002904326144',
    created: 1611403153000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fwilhe" rel="noopener noreferrer" target="_blank">@fwilhe</a> It also avoids the conflict with the existing Washington State, which has always been a source of confusion.<br><br>In reply to: <a href="#1352948543045079040">1352948543045079040</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1352948543045079040',
    created: 1611403043000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fwilhe" rel="noopener noreferrer" target="_blank">@fwilhe</a> The proposal lists it as, "The State of Washington, Douglas Commonwealth" named after Frederick Douglass. As a native of the area, I think it should just be shortened to Douglass Commonwealth so we can continue to call it D.C. as we always have.<br><br>In reply to: <a href="https://x.com/fwilhe/status/1352918408237641729" rel="noopener noreferrer" target="_blank">@fwilhe</a> <span class="status">1352918408237641729</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1352903682895925248',
    created: 1611392348000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> Yep, that\'s all I\'m looking for. Not necessarily "here\'s how to structure a readable article" but rather "here are the rules so all the content you want included gets included".<br><br>In reply to: <a href="https://x.com/garrett/status/1352895658521485312" rel="noopener noreferrer" target="_blank">@garrett</a> <span class="status">1352895658521485312</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1352903406424268800',
    created: 1611392282000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> Yep, that seems like the next logical step in my investigation.<br><br>In reply to: <a href="https://x.com/garrett/status/1352895189090770944" rel="noopener noreferrer" target="_blank">@garrett</a> <span class="status">1352895189090770944</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1352892418207608832',
    created: 1611389662000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> It\'s especially problematic for content generated by Asciidoctor since it almost always truncates the article after the first section. This affect a lot of technical content that\'s available online.<br><br>In reply to: <a href="#1352892184647766017">1352892184647766017</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1352892184647766017',
    created: 1611389606000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> I can see the code. What I\'m missing is a clear spec. And even the README say it\'s very hit or miss, which frankly is rather concerning for something as important as content truncation.<br><br>In reply to: <a href="https://x.com/garrett/status/1352881444591448065" rel="noopener noreferrer" target="_blank">@garrett</a> <span class="status">1352881444591448065</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1352794802715201536',
    created: 1611366389000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> Immunity? We can still dream, right?<br><br>In reply to: <a href="https://x.com/garrett/status/1352772320230834182" rel="noopener noreferrer" target="_blank">@garrett</a> <span class="status">1352772320230834182</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1352774032370798592',
    created: 1611361437000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> There are two major problems I have with it. First, it\'s deciding what is main content and what is not and second, there\'s no clear spec for how to inform it to make correct semantic choices. I think it\'s overstepping. I love the idea, just not the implementation.<br><br>In reply to: <a href="https://x.com/garrett/status/1352646929985974275" rel="noopener noreferrer" target="_blank">@garrett</a> <span class="status">1352646929985974275</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1352773118532620288',
    created: 1611361219000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Stephan007" rel="noopener noreferrer" target="_blank">@Stephan007</a> <a class="mention" href="https://x.com/Devoxx" rel="noopener noreferrer" target="_blank">@Devoxx</a> Dude, I actually dreamed about this last night! I knew he\'d be in the front row.<br><br>In reply to: <a href="https://x.com/Stephan007/status/1352694671819104263" rel="noopener noreferrer" target="_blank">@Stephan007</a> <span class="status">1352694671819104263</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1352772942631890945',
    created: 1611361177000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> I know, right? I hope it\'s kind. Maybe we can end the filibuster so this gov\'t can actually function again? Not sure where the letter "i" fits there, other than the two occurrences in the word.<br><br>In reply to: <a href="https://x.com/garrett/status/1352772320230834182" rel="noopener noreferrer" target="_blank">@garrett</a> <span class="status">1352772320230834182</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1352772656542539776',
    created: 1611361109000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> Since you mentioned flatpak in our last thread, I\'m curious to know more about it. Is it safe to use alongside dnf? Is there a good article you recommend to learn more about it?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1352762877350797313',
    created: 1611358777000,
    type: 'post',
    text: 'If you\'re feeling a bit dizzy after living through this month, just remember that for the first three Wednesdays in January, we witnessed an insurrection, an impeachment, and an inauguration. Call it a hat trick of political whiplash.',
    likes: 10,
    retweets: 1
  },
  {
    id: '1352734504750780417',
    created: 1611352013000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mesirii" rel="noopener noreferrer" target="_blank">@mesirii</a> <a class="mention" href="https://x.com/rothgar" rel="noopener noreferrer" target="_blank">@rothgar</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> ^ That\'s something the AsciiDoc WG should look at. Perhaps we can help clarify it.<br><br>In reply to: <a href="#1352734425398669312">1352734425398669312</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1352734425398669312',
    created: 1611351994000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mesirii" rel="noopener noreferrer" target="_blank">@mesirii</a> <a class="mention" href="https://x.com/rothgar" rel="noopener noreferrer" target="_blank">@rothgar</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Apparently there\'s now a proposed spec for it: <a href="https://sembr.org/" rel="noopener noreferrer" target="_blank">sembr.org/</a><br><br>In reply to: <a href="https://x.com/mesirii/status/1352465862880616449" rel="noopener noreferrer" target="_blank">@mesirii</a> <span class="status">1352465862880616449</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1352682796431499269',
    created: 1611339684000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/domdorn" rel="noopener noreferrer" target="_blank">@domdorn</a> <a class="mention" href="https://x.com/elastic" rel="noopener noreferrer" target="_blank">@elastic</a> I was referred to this post for clarification: <a href="https://sfconservancy.org/blog/2020/jan/06/copyleft-equality/" rel="noopener noreferrer" target="_blank">sfconservancy.org/blog/2020/jan/06/copyleft-equality/</a><br><br>In reply to: <a href="https://x.com/domdorn/status/1352666109460295683" rel="noopener noreferrer" target="_blank">@domdorn</a> <span class="status">1352666109460295683</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1352459941261438977',
    created: 1611286552000,
    type: 'quote',
    text: 'Seeing #WheresBernie in my timeline nonstop has absolutely flipped my spirits from the start of this week. He\'s waiting for us to support his progressive agenda that will pull us out of this darkness.<br><br>Quoting: <a href="https://x.com/GCarbin/status/1352417913530163203" rel="noopener noreferrer" target="_blank">@GCarbin</a> <span class="status">1352417913530163203</span>',
    likes: 2,
    retweets: 0,
    tags: ['wheresbernie']
  },
  {
    id: '1352455962360041473',
    created: 1611285603000,
    type: 'post',
    text: 'Firefox\'s Reader View mode is a joke. I really think they should just remove it. It only renders about half an article, giving the reader the impression there\'s a lot less content than there really is. So what exactly is the point of it?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1352341078754488320',
    created: 1611258213000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> Logitech does have a full keyboard version of it. It\'s the K780. That\'s the one Sarah uses. Do note that the keys are quiet, so the touch is softer than on other keyboards. I happen to like that, and we don\'t annoy each other with our typing.<br><br>In reply to: <a href="https://x.com/settermjd/status/1352221497427947526" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1352221497427947526</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1352219633412046848',
    created: 1611229258000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/werner_fouche" rel="noopener noreferrer" target="_blank">@werner_fouche</a> If I can\'t get this resolved, I\'ll very likely switch back to the Gboard app. That\'s what I used on my old phone, but I was giving the stock installation a chance. I\'m also missing the emoji key (as emoji can only be accessed via suggestions).<br><br>In reply to: <a href="https://x.com/werner_fouche/status/1352216536728088577" rel="noopener noreferrer" target="_blank">@werner_fouche</a> <span class="status">1352216536728088577</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1352218969009123334',
    created: 1611229099000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MaxGrobe" rel="noopener noreferrer" target="_blank">@MaxGrobe</a> Very strange indeed. It feels like Samsung is unilaterally deciding typographic quotes are no longer important.<br><br>In reply to: <a href="https://x.com/MaxGrobe/status/1352213304345825282" rel="noopener noreferrer" target="_blank">@MaxGrobe</a> <span class="status">1352213304345825282</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1352209773773656067',
    created: 1611226907000,
    type: 'post',
    text: 'Does anyone with a Samsung phone know how to entry typographic quotes (like “ and ”) using the Samsung keyboard?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1352183247501815810',
    created: 1611220583000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> That\'s tough because the answer is really subjective. To me it feels right, but that may be precisely because the keys feel close. What I can tell you is that it\'s very consistent from key to key. It\'s well made.<br><br>In reply to: <a href="https://x.com/settermjd/status/1352180547125202946" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1352180547125202946</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1352039818503483394',
    created: 1611186387000,
    type: 'quote',
    text: 'We\'re off to a good start, but we need to do so much more. Let\'s keep this trend going.<br><br>Quoting: <a href="https://x.com/sunrisemvmt/status/1352023862473592835" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a> <span class="status">1352023862473592835</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1352039571417010177',
    created: 1611186328000,
    type: 'quote',
    text: '...now let\'s keep it going!<br><br>Quoting: <a href="https://x.com/OurRevolution/status/1352026116349980674" rel="noopener noreferrer" target="_blank">@OurRevolution</a> <span class="status">1352026116349980674</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1352039509626535936',
    created: 1611186313000,
    type: 'quote',
    text: 'Hell yeah!<br><br>Quoting: <a href="https://x.com/janeosanders/status/1352030080109867008" rel="noopener noreferrer" target="_blank">@janeosanders</a> <span class="status">1352030080109867008</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1352009133512224769',
    created: 1611179071000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> Looks slick! For me, it\'s critical that the keyboard have a laptop form factor because it\'s the only keyboard layout I know. It can also pair with multiple computers, which is great for collaboration.<br><br>In reply to: <a href="https://x.com/settermjd/status/1352003629314674692" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1352003629314674692</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1352008486448553984',
    created: 1611178916000,
    type: 'post',
    text: '"History is on our side and I\'ll be damned if we let it slip away again."',
    likes: 3,
    retweets: 0
  },
  {
    id: '1352008485681041410',
    created: 1611178916000,
    type: 'post',
    text: '"I was there to see the people I don\'t know who I\'m committed to fighting for. And I saw many. We need to keep growing, keep coming together as one, and over-win."',
    likes: 1,
    retweets: 0
  },
  {
    id: '1352008484171120640',
    created: 1611178916000,
    type: 'post',
    text: '"It\'s hard to put into words because it\'s really something I feel. It feels right. It feels hopeful. It feels like a movement, not a moment."',
    likes: 0,
    retweets: 0
  },
  {
    id: '1352008483378323456',
    created: 1611178916000,
    type: 'post',
    text: 'I wrote these words almost a year ago after attending the Bernie rally in Denver. I feel like they\'re still relevant, even if this isn\'t exactly the outcome we hoped for.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1352001162145218560',
    created: 1611177170000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/caroljmcdonald" rel="noopener noreferrer" target="_blank">@caroljmcdonald</a> It\'s this one: <a href="https://www.amazon.com/OMOTON-Detachable-Aluminum-Compatible-MacBook/dp/B08B13JGN4" rel="noopener noreferrer" target="_blank">www.amazon.com/OMOTON-Detachable-Aluminum-Compatible-MacBook/dp/B08B13JGN4</a> I have several stands by this vendor and they\'re all very well made.<br><br>In reply to: <a href="https://x.com/caroljmcdonald/status/1351927582141407233" rel="noopener noreferrer" target="_blank">@caroljmcdonald</a> <span class="status">1351927582141407233</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1351796051863224322',
    created: 1611128268000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jaredmorgs" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> More room for the brain to focus on writing docs 💭<br><br>In reply to: <a href="https://x.com/jaredmorgs/status/1351727652114055172" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <span class="status">1351727652114055172</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1351795648853544961',
    created: 1611128172000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pvaneeckhoudt" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> I\'m documenting it precisely because it\'s such a rare occasion ;) We\'ll see how long it lasts ⏱️<br><br>In reply to: <a href="https://x.com/pvaneeckhoudt/status/1351790420548841472" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> <span class="status">1351790420548841472</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1351671610705526787',
    created: 1611098599000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RoganJoshua" rel="noopener noreferrer" target="_blank">@RoganJoshua</a> <a class="mention" href="https://x.com/Razer" rel="noopener noreferrer" target="_blank">@Razer</a> <a class="mention" href="https://x.com/shobull" rel="noopener noreferrer" target="_blank">@shobull</a> I got the top of the line with the 4K touch screen (since the company was paying).<br><br>In reply to: @roganjoshua <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1351659752028160001',
    created: 1611095772000,
    type: 'post',
    text: 'If you\'re a developer (and have the means), I strongly recommend evaluating the <a class="mention" href="https://x.com/Razer" rel="noopener noreferrer" target="_blank">@Razer</a> Book 13. When they say it\'s the ultimate productivity laptop, they aren\'t kidding. And it works flawlessly with Linux! Thanks to my bro <a class="mention" href="https://x.com/shobull" rel="noopener noreferrer" target="_blank">@shobull</a> for turning me on to it. <a href="https://www.razer.com/productivity-laptops/razer-book-13" rel="noopener noreferrer" target="_blank">www.razer.com/productivity-laptops/razer-book-13</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1351658260017762304',
    created: 1611095416000,
    type: 'post',
    text: 'All set up with Fedora 33 on a sweet new Razer Book 13.',
    photos: ['<div class="item"><img class="photo" src="media/1351658260017762304.jpg"></div>'],
    likes: 18,
    retweets: 0
  },
  {
    id: '1351651269157425153',
    created: 1611093749000,
    type: 'post',
    text: '...and here\'s the wiring: <a href="https://github.com/mojavelinux/retry-netlify-deploy-preview-action/blob/main/index.js" rel="noopener noreferrer" target="_blank">github.com/mojavelinux/retry-netlify-deploy-preview-action/blob/main/index.js</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1351650312273096707',
    created: 1611093521000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Netflixhelps" rel="noopener noreferrer" target="_blank">@Netflixhelps</a> Good news! Now that I\'m Android 11, I no longer experience this problem. The volume for the headphones and media are now unified and the sound level is the same for both apps. So problem solved!<br><br>In reply to: <a href="#1349825966080724993">1349825966080724993</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1351648217063047169',
    created: 1611093022000,
    type: 'quote',
    text: 'Just one more reason to use Firefox as my main browser.<br><br>Quoting: <a href="https://x.com/spotfoss/status/1351624743510827015" rel="noopener noreferrer" target="_blank">@spotfoss</a> <span class="status">1351624743510827015</span>',
    likes: 7,
    retweets: 4
  },
  {
    id: '1351451411645657088',
    created: 1611046099000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> Another interesting observation. When the cursor isn\'t selected from the Gnome theme, it also doesn\'t scale. (I use 300% scaling on my display). So for me, the cursor is very small in addition to being white.<br><br>In reply to: <a href="https://x.com/garrett/status/1351450562773053441" rel="noopener noreferrer" target="_blank">@garrett</a> <span class="status">1351450562773053441</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1351450938607714306',
    created: 1611045987000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> Good to know. Honestly, I feel better just knowing I didn\'t somehow mess up my profile (since I just set up a new computer).<br><br>In reply to: <a href="https://x.com/garrett/status/1351450562773053441" rel="noopener noreferrer" target="_blank">@garrett</a> <span class="status">1351450562773053441</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1351448783570104322',
    created: 1611045473000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> Chrome displays it correctly, so it seems like the problem is with Firefox rather than Gnome. Perhaps missing a mapping somewhere?<br><br>In reply to: <a href="https://x.com/garrett/status/1351447094771515394" rel="noopener noreferrer" target="_blank">@garrett</a> <span class="status">1351447094771515394</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1351438945146474499',
    created: 1611043127000,
    type: 'post',
    text: 'I just learned that Firefox now supports controlling the tab currently playing audio or video using the media controls on your keyboard. That\'s really handy! <a href="https://support.mozilla.org/en-US/kb/control-audio-or-video-playback-your-keyboard" rel="noopener noreferrer" target="_blank">support.mozilla.org/en-US/kb/control-audio-or-video-playback-your-keyboard</a>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1351415837769388032',
    created: 1611037618000,
    type: 'quote',
    text: 'I feel like there needs to be a DJ set that goes along with this.<br><br>Quoting: <a href="https://x.com/cspan/status/1351328724386533379" rel="noopener noreferrer" target="_blank">@cspan</a> <span class="status">1351328724386533379</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1351287653778374656',
    created: 1611007057000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> Are you seeing this issue? <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1270155" rel="noopener noreferrer" target="_blank">bugzilla.mozilla.org/show_bug.cgi?id=1270155</a> You can see it in action by hovering over the overview panel for a GitHub Actions workflow. For example: <a href="https://github.com/asciidoctor/asciidoctor/actions/runs/492614209" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor/actions/runs/492614209</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1350893224450134016',
    created: 1610913017000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Argorak" rel="noopener noreferrer" target="_blank">@Argorak</a> <a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> I\'m sorry to hear that. It\'s all I can offer.<br><br>In reply to: <a href="https://x.com/Argorak/status/1350892096094408706" rel="noopener noreferrer" target="_blank">@Argorak</a> <span class="status">1350892096094408706</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1350890593551937537',
    created: 1610912390000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <a class="mention" href="https://x.com/Argorak" rel="noopener noreferrer" target="_blank">@Argorak</a> Antora is what I\'m going to recommend. All software can stand to be improved, but we do so together as a community.<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/1350823850687287296" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">1350823850687287296</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1350726643627671552',
    created: 1610873301000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ryanbigg" rel="noopener noreferrer" target="_blank">@ryanbigg</a> Then I encourage you to report it. It\'s certainly not intentional and has been valid until recently to my knowledge.<br><br>In reply to: <a href="https://x.com/ryanbigg/status/1350706264272039943" rel="noopener noreferrer" target="_blank">@ryanbigg</a> <span class="status">1350706264272039943</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1350702116080259073',
    created: 1610867453000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ryanbigg" rel="noopener noreferrer" target="_blank">@ryanbigg</a> Why not use Asciidoctor EPUB3?<br><br>In reply to: <a href="https://x.com/ryanbigg/status/1350673802666000388" rel="noopener noreferrer" target="_blank">@ryanbigg</a> <span class="status">1350673802666000388</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1350673523442814976',
    created: 1610860636000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ryanbigg" rel="noopener noreferrer" target="_blank">@ryanbigg</a> I spent almost a year trying to make it happen. Now I wish I didn\'t. The best you can hope for is a primitive layout with some nice typography. Beyond that, the rate of return decrees dramatically.<br><br>In reply to: <a href="#1350673113516630017">1350673113516630017</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1350673113516630017',
    created: 1610860539000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ryanbigg" rel="noopener noreferrer" target="_blank">@ryanbigg</a> I have to be honest, I lost faith in EPUB3. It\'s not that it doesn\'t have tremendous potential. It\'s just like web dev in the early 2000s. Everything you hope to work just doesn\'t.<br><br>In reply to: <a href="https://x.com/ryanbigg/status/1350669652356001793" rel="noopener noreferrer" target="_blank">@ryanbigg</a> <span class="status">1350669652356001793</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1350578854088200192',
    created: 1610838066000,
    type: 'post',
    text: 'I wrote my first GitHub Action! It was more out of necessity than curiosity. I created an action that retries a Netlify deploy preview for a pull request. See <a href="https://github.com/mojavelinux/retry-netlify-deploy-preview-action" rel="noopener noreferrer" target="_blank">github.com/mojavelinux/retry-netlify-deploy-preview-action</a>',
    likes: 5,
    retweets: 0
  },
  {
    id: '1350373803046518784',
    created: 1610789178000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_msw_" rel="noopener noreferrer" target="_blank">@_msw_</a> <a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> <a class="mention" href="https://x.com/neugens" rel="noopener noreferrer" target="_blank">@neugens</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/yigalatz" rel="noopener noreferrer" target="_blank">@yigalatz</a> <a class="mention" href="https://x.com/rkennke" rel="noopener noreferrer" target="_blank">@rkennke</a> I may be skeptical, but the reason for them doing this seems more like greed than sustainability. (I have my issues with Amazon, but that\'s an orthogonal point in my mind).<br><br>In reply to: <a href="https://x.com/_msw_/status/1350372573352140801" rel="noopener noreferrer" target="_blank">@_msw_</a> <span class="status">1350372573352140801</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1350372432884862976',
    created: 1610788851000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> <a class="mention" href="https://x.com/neugens" rel="noopener noreferrer" target="_blank">@neugens</a> <a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/_msw_" rel="noopener noreferrer" target="_blank">@_msw_</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/yigalatz" rel="noopener noreferrer" target="_blank">@yigalatz</a> <a class="mention" href="https://x.com/rkennke" rel="noopener noreferrer" target="_blank">@rkennke</a> And the reason it\'s important to say "yes it\'s open, but we also reject it" is to protest using creative writing to co-opt open source. We\'re not asking others to be more clever in the future, but to honor the spirit of the term. Otherwise, we\'ll just be back here again.<br><br>In reply to: <a href="#1350369841870368768">1350369841870368768</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1350369841870368768',
    created: 1610788233000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> <a class="mention" href="https://x.com/neugens" rel="noopener noreferrer" target="_blank">@neugens</a> <a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/_msw_" rel="noopener noreferrer" target="_blank">@_msw_</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/yigalatz" rel="noopener noreferrer" target="_blank">@yigalatz</a> <a class="mention" href="https://x.com/rkennke" rel="noopener noreferrer" target="_blank">@rkennke</a> I ran out of characters, so that probably doesn\'t do the point justice. Bottom line is, what is the license protecting? Is it protecting profits or software freedom? If it\'s the former, we can reject it on the basis of not being in the spirit of the FOSS community.<br><br>In reply to: <a href="#1350368512083062786">1350368512083062786</a>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1350368512083062786',
    created: 1610787916000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> <a class="mention" href="https://x.com/neugens" rel="noopener noreferrer" target="_blank">@neugens</a> <a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/_msw_" rel="noopener noreferrer" target="_blank">@_msw_</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/yigalatz" rel="noopener noreferrer" target="_blank">@yigalatz</a> <a class="mention" href="https://x.com/rkennke" rel="noopener noreferrer" target="_blank">@rkennke</a> It might be better to say what we\'ve said all along. The source can be open, but that doesn\'t make it Open Source (proper noun). In Open Source, we\'re all subject to the same terms. Those terms are to protect your freedom to use/modify it, not as a scare tactic to sell software.<br><br>In reply to: <a href="https://x.com/brunoborges/status/1350365012976492545" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1350365012976492545</span>',
    likes: 5,
    retweets: 1
  },
  {
    id: '1350367166642544644',
    created: 1610787595000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/_msw_" rel="noopener noreferrer" target="_blank">@_msw_</a> <a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> <a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/yigalatz" rel="noopener noreferrer" target="_blank">@yigalatz</a> <a class="mention" href="https://x.com/rkennke" rel="noopener noreferrer" target="_blank">@rkennke</a> <a class="mention" href="https://x.com/neugens" rel="noopener noreferrer" target="_blank">@neugens</a> After reading Bradley\'s analysis, I better understand your point. I\'ll revise mine. It\'s not that SSPL isn\'t open. It\'s that many of us in the FOSS community reject it as not aligning with our values and efforts. I think that protest is more effective as it compels a reckoning.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1350357490265956352" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1350357490265956352</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1350362957339791360',
    created: 1610786592000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_msw_" rel="noopener noreferrer" target="_blank">@_msw_</a> <a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/yigalatz" rel="noopener noreferrer" target="_blank">@yigalatz</a> <a class="mention" href="https://x.com/rkennke" rel="noopener noreferrer" target="_blank">@rkennke</a> <a class="mention" href="https://x.com/neugens" rel="noopener noreferrer" target="_blank">@neugens</a> This is a great framing of this issue and how it relates to history. It\'s also a point I\'ve long made, which is that copyleft is a tool to achieve software freedom (and as with all tools, not always wielded with good intentions). But the idea was to be a statute for good.<br><br>In reply to: <a href="#1350360648962564096">1350360648962564096</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1350360648962564096',
    created: 1610786041000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_msw_" rel="noopener noreferrer" target="_blank">@_msw_</a> <a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/yigalatz" rel="noopener noreferrer" target="_blank">@yigalatz</a> <a class="mention" href="https://x.com/rkennke" rel="noopener noreferrer" target="_blank">@rkennke</a> <a class="mention" href="https://x.com/neugens" rel="noopener noreferrer" target="_blank">@neugens</a> Reading that article now. Thanks for sharing. I like the "unintended consequences of copyleft languages" point. That kind of gets to what I was trying to say in a much more eloquent way. Lots of nuance, to be sure!<br><br>In reply to: <a href="https://x.com/_msw_/status/1350355342182412291" rel="noopener noreferrer" target="_blank">@_msw_</a> <span class="status">1350355342182412291</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1350359100719153152',
    created: 1610785672000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/yigalatz" rel="noopener noreferrer" target="_blank">@yigalatz</a> <a class="mention" href="https://x.com/rkennke" rel="noopener noreferrer" target="_blank">@rkennke</a> <a class="mention" href="https://x.com/neugens" rel="noopener noreferrer" target="_blank">@neugens</a> There is no light side, though that\'s a separate conversation. Reasonably, I\'d just be happy if they were broken up and regulated.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1350358574241894400" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1350358574241894400</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1350358487839039491',
    created: 1610785526000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/yigalatz" rel="noopener noreferrer" target="_blank">@yigalatz</a> <a class="mention" href="https://x.com/rkennke" rel="noopener noreferrer" target="_blank">@rkennke</a> <a class="mention" href="https://x.com/neugens" rel="noopener noreferrer" target="_blank">@neugens</a> I\'m going steer in a new direction to point out that the force at work here is the massive shift of wealth (largely to companies that pay no tax). Among many other things, it threatens open source (and fair competition). It\'s no surprise these license changes are happening now.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1350356824554352642" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1350356824554352642</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1350357235919032322',
    created: 1610785228000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_msw_" rel="noopener noreferrer" target="_blank">@_msw_</a> <a class="mention" href="https://x.com/neugens" rel="noopener noreferrer" target="_blank">@neugens</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> <a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/yigalatz" rel="noopener noreferrer" target="_blank">@yigalatz</a> <a class="mention" href="https://x.com/rkennke" rel="noopener noreferrer" target="_blank">@rkennke</a> That last part is precisely why I personally don\'t like the license. I think it amounts to calling yourself open without really believing in it. Yet I recognized their right to live that hypocrisy.<br><br>In reply to: <a href="https://x.com/_msw_/status/1350356542004989952" rel="noopener noreferrer" target="_blank">@_msw_</a> <span class="status">1350356542004989952</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1350356529975705600',
    created: 1610785059000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/_msw_" rel="noopener noreferrer" target="_blank">@_msw_</a> <a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> <a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/yigalatz" rel="noopener noreferrer" target="_blank">@yigalatz</a> <a class="mention" href="https://x.com/rkennke" rel="noopener noreferrer" target="_blank">@rkennke</a> <a class="mention" href="https://x.com/neugens" rel="noopener noreferrer" target="_blank">@neugens</a> The "mixing" aspect is probably the strongest point. Though in that sense open cannot always be mixed with open (there are charts). This is one more in that list (however extreme) and therefore must be approached carefully. But the issue of mixing is not new to OSS.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1350355347417018370" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1350355347417018370</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1350355645443186689',
    created: 1610784848000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/yigalatz" rel="noopener noreferrer" target="_blank">@yigalatz</a> <a class="mention" href="https://x.com/rkennke" rel="noopener noreferrer" target="_blank">@rkennke</a> <a class="mention" href="https://x.com/neugens" rel="noopener noreferrer" target="_blank">@neugens</a> The warning feels fair. I think what you\'re saying is to do your research, just like you would if you were selecting proprietary software, for the very reason that it could surprise you.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1350354398946467841" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1350354398946467841</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1350354350699278338',
    created: 1610784540000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_msw_" rel="noopener noreferrer" target="_blank">@_msw_</a> <a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/yigalatz" rel="noopener noreferrer" target="_blank">@yigalatz</a> <a class="mention" href="https://x.com/rkennke" rel="noopener noreferrer" target="_blank">@rkennke</a> <a class="mention" href="https://x.com/neugens" rel="noopener noreferrer" target="_blank">@neugens</a> I\'m not making the case OSI should recognize this license. I trust the OSI. I\'m simply saying we can\'t just call it "closed". What it\'s attempting to do is co-op / extend copyleft (as I understand it). So that\'s where the criticism should be focused. We need accurate words.<br><br>In reply to: <a href="https://x.com/_msw_/status/1350353118844735488" rel="noopener noreferrer" target="_blank">@_msw_</a> <span class="status">1350353118844735488</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1350353147269505025',
    created: 1610784253000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/yigalatz" rel="noopener noreferrer" target="_blank">@yigalatz</a> <a class="mention" href="https://x.com/rkennke" rel="noopener noreferrer" target="_blank">@rkennke</a> <a class="mention" href="https://x.com/neugens" rel="noopener noreferrer" target="_blank">@neugens</a> It\'s very important that when we talk about this subject that we be honest. It\'s not helpful to use the word "closed" or "proprietary" because then those words lose their real meaning (and we lose people\'s trust). It\'s fair to criticize, but we must do so in context.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1350349805709651969" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1350349805709651969</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1350352354734837762',
    created: 1610784064000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/yigalatz" rel="noopener noreferrer" target="_blank">@yigalatz</a> <a class="mention" href="https://x.com/rkennke" rel="noopener noreferrer" target="_blank">@rkennke</a> <a class="mention" href="https://x.com/neugens" rel="noopener noreferrer" target="_blank">@neugens</a> Freedom is doing whatever I want. That\'s free software. Open source is not that (for good and justifiable reasons). You never have exclusive freedom with an OSS license. But the freedom you do have is protected.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1350351692026552320" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1350351692026552320</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1350351998982373377',
    created: 1610783979000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/yigalatz" rel="noopener noreferrer" target="_blank">@yigalatz</a> <a class="mention" href="https://x.com/rkennke" rel="noopener noreferrer" target="_blank">@rkennke</a> <a class="mention" href="https://x.com/neugens" rel="noopener noreferrer" target="_blank">@neugens</a> GPL, and more specifically AGPL.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1350351692026552320" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1350351692026552320</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1350351550456098816',
    created: 1610783872000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/yigalatz" rel="noopener noreferrer" target="_blank">@yigalatz</a> <a class="mention" href="https://x.com/rkennke" rel="noopener noreferrer" target="_blank">@rkennke</a> <a class="mention" href="https://x.com/neugens" rel="noopener noreferrer" target="_blank">@neugens</a> My understanding is that you can modify SSPL code. The viral aspect just forces you to open source your service. That\'s like GPL to an extreme. If I modify GPL code and use it, my app must be GPL (granted this is the most strict interpretation). To me, it\'s the same sentiment.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1350349805709651969" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1350349805709651969</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1350350199961526272',
    created: 1610783550000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/yigalatz" rel="noopener noreferrer" target="_blank">@yigalatz</a> <a class="mention" href="https://x.com/rkennke" rel="noopener noreferrer" target="_blank">@rkennke</a> <a class="mention" href="https://x.com/neugens" rel="noopener noreferrer" target="_blank">@neugens</a> First, I don\'t think there is a line. It\'s a spectrum. (And it doesn\'t mean I personally like everything on that spectrum). Open source is about freedom to use and modify. Yet most licenses do impose terms. That\'s a big part of open source\'s history (which differs from Free).<br><br>In reply to: <a href="https://x.com/maxandersen/status/1350347813457113089" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1350347813457113089</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1350348525561757696',
    created: 1610783151000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/yigalatz" rel="noopener noreferrer" target="_blank">@yigalatz</a> <a class="mention" href="https://x.com/rkennke" rel="noopener noreferrer" target="_blank">@rkennke</a> <a class="mention" href="https://x.com/neugens" rel="noopener noreferrer" target="_blank">@neugens</a> I could get behind calling it something else (in which I would group the AGPL). But remember that permissive and open source aren\'t one in the same. MIT/BSD is all permissive. GPL is on the other end of the permissive spectrum. So FLOSS isn\'t exclusively permissive.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1350345650823720962" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1350345650823720962</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1350344358038364161',
    created: 1610782157000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/yigalatz" rel="noopener noreferrer" target="_blank">@yigalatz</a> <a class="mention" href="https://x.com/rkennke" rel="noopener noreferrer" target="_blank">@rkennke</a> <a class="mention" href="https://x.com/neugens" rel="noopener noreferrer" target="_blank">@neugens</a> I also agree it is open source. It\'s very copyleft (in an very specific and targeted way) and therefore not my taste. But to say it is closed is to not understand open source and its history.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1350340304168222722" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1350340304168222722</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1350343450940456964',
    created: 1610781941000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/obilodeau" rel="noopener noreferrer" target="_blank">@obilodeau</a> No, it\'s calling out hypocrisy. A project can\'t make a big press release about a new feature without ever having tested it, which they clearly did not.<br><br>In reply to: <a href="https://x.com/obilodeau/status/1350342032171143168" rel="noopener noreferrer" target="_blank">@obilodeau</a> <span class="status">1350342032171143168</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1350258036317724672',
    created: 1610761577000,
    type: 'post',
    text: 'Gimp 2.10 added support for HiDPI screens, but even with the icon size set to huge, they are tiny on a 3840x2400 screen (which is scaled to 300%). So no, not there yet.',
    photos: ['<div class="item"><img class="photo" src="media/1350258036317724672.jpg"></div>'],
    likes: 2,
    retweets: 0
  },
  {
    id: '1349869192569229315',
    created: 1610668869000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gregturn" rel="noopener noreferrer" target="_blank">@gregturn</a> <a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <a class="mention" href="https://x.com/rotnroll666" rel="noopener noreferrer" target="_blank">@rotnroll666</a> Yep. See <a href="https://github.com/asciidoctor/asciidoctor-pdf#optimizing-the-generated-pdf" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-pdf#optimizing-the-generated-pdf</a>.<br><br>In reply to: <a href="https://x.com/gregturn/status/1349740690775941120" rel="noopener noreferrer" target="_blank">@gregturn</a> <span class="status">1349740690775941120</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1349825966080724993',
    created: 1610658563000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Netflixhelps" rel="noopener noreferrer" target="_blank">@Netflixhelps</a> It\'s not that sound doesn\'t work. I believe the app is attenuating the sound. It doesn\'t match other apps so I keep having to adjust the volume.<br><br>In reply to: <a href="https://x.com/Netflixhelps/status/1349706115685220354" rel="noopener noreferrer" target="_blank">@Netflixhelps</a> <span class="status">1349706115685220354</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1349698139838681088',
    created: 1610628087000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/netflix" rel="noopener noreferrer" target="_blank">@netflix</a> The sound for Netflix on Android that plays though my headphones is significantly lower than YouTube. Is there anyway to make it louder? (This has nothing to do with my system sound level).',
    likes: 0,
    retweets: 0
  },
  {
    id: '1349682247050268674',
    created: 1610624298000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> It\'s amazing how different touchpad and mousewheel scrolling are in Gnome / Fedora. I would say that settings configured for the touchpad are the normal ones. But I guess it really depends on how your mousewheel works. Mine doesn\'t fling.<br><br>In reply to: <a href="#1349681898910453763">1349681898910453763</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1349681898910453763',
    created: 1610624215000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> I agree it roughly matches the behavior for other apps. It\'s just that behavior is dreadful for the web. I have to adjust it everywhere (that I can). Perhaps what Firefox needs is a setting in the preferences panel to enable acceleration and physics for those who like it.<br><br>In reply to: <a href="https://x.com/garrett/status/1349672991316185088" rel="noopener noreferrer" target="_blank">@garrett</a> <span class="status">1349672991316185088</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1349654297621893120',
    created: 1610617634000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> I looked into it, and it turns out Fedora already enables this by default. But the issue isn\'t with smooth scrolling in general. Scrolling with the touchpad is as smooth as silk. The issue is with the mousewheel settings. Acceleration is disabled by default, which makes it crawl.<br><br>In reply to: <a href="https://x.com/garrett/status/1349135264824905736" rel="noopener noreferrer" target="_blank">@garrett</a> <span class="status">1349135264824905736</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1349426039777202176',
    created: 1610563213000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lanceball" rel="noopener noreferrer" target="_blank">@lanceball</a> <a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> I agree that this every state makes their own strategy makes beating this absolutely futile.<br><br>In reply to: <a href="https://x.com/lanceball/status/1349341074440024064" rel="noopener noreferrer" target="_blank">@lanceball</a> <span class="status">1349341074440024064</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1349340475770114050',
    created: 1610542813000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lanceball" rel="noopener noreferrer" target="_blank">@lanceball</a> <a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> I am noticing that they\'re pushing it to every device (opt in, of course), at least in Colorado. I have no idea if it\'s at all effective.<br><br>In reply to: <a href="https://x.com/lanceball/status/1349339665871069184" rel="noopener noreferrer" target="_blank">@lanceball</a> <span class="status">1349339665871069184</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1349186404400594946',
    created: 1610506080000,
    type: 'post',
    text: 'To confirm, I got it working. Now it\'s just a matter of wiring it all together. I\'ll share a link to the code once I get it sorted out.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1349159173800411136',
    created: 1610499587000,
    type: 'post',
    text: 'It turns out that the Netlify API may have all the endpoints I need to make this happen. See <a href="https://open-api.netlify.com" rel="noopener noreferrer" target="_blank">open-api.netlify.com</a>. The retrySiteDeploy method is currently undocumented, but it exists.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1349148646567133184',
    created: 1610497077000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> At first, I thought I had a bad configuration. But I\'ve been able to reproduce on fresh profiles. But you are right that\'s incredibly hard to reproduce consistently.<br><br>In reply to: <a href="#1349148375829020673">1349148375829020673</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1349148375829020673',
    created: 1610497013000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> If I click, then right click, then click, then right click, then the menu will eventually come up. It\'s like I have to tease it out.<br><br>In reply to: <a href="#1349148258703077376">1349148258703077376</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1349148258703077376',
    created: 1610496985000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> Yes, in my experience, those two are related (and I\'ve seen both). It\'s when the menu needs to draw near the edge of a screen, often an a HiDPI display.<br><br>In reply to: <a href="https://x.com/garrett/status/1349138601926393857" rel="noopener noreferrer" target="_blank">@garrett</a> <span class="status">1349138601926393857</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1349137267013505025',
    created: 1610494364000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> I still have the problem where the context menu won\'t appear when I right click (even though it\'s interactive). Happens on multiple computers with different profiles. I wonder if that\'s also related.<br><br>In reply to: <a href="#1349136795762507778">1349136795762507778</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1349136795762507778',
    created: 1610494252000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> I\'ll definitely try your suggestion and report back.<br><br>In reply to: <a href="#1349136519789875200">1349136519789875200</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1349136519789875200',
    created: 1610494186000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> I\'m able to get smooth scrolling working reasonably well using apz.gtk.kinetic_scroll.enabled;false, mousewheel.system_scroll_override_on_root_content.enabled;true, and mousewheel.acceleration.start;0. Just not sure why it\'s not the default. And the physics is still a bit off.<br><br>In reply to: <a href="https://x.com/garrett/status/1349135264824905736" rel="noopener noreferrer" target="_blank">@garrett</a> <span class="status">1349135264824905736</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1349110497426120704',
    created: 1610487982000,
    type: 'post',
    text: 'Details here: <a href="https://github.com/asciidoctor/asciidoctor-pdf/releases/tag/v1.5.4" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-pdf/releases/tag/v1.5.4</a>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1349106837363200000',
    created: 1610487109000,
    type: 'quote',
    text: 'A new patch release of Asciidoctor PDF to fix compatibility with the latest Asciidoctor release. This should hopefully tide you over until we manage to get 2.0 finished and released.<br><br>Quoting: <a href="https://x.com/RubygemsN/status/1347847089590423553" rel="noopener noreferrer" target="_blank">@RubygemsN</a> <span class="status">1347847089590423553</span>',
    likes: 10,
    retweets: 1
  },
  {
    id: '1349099738126172165',
    created: 1610485417000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mkheck" rel="noopener noreferrer" target="_blank">@mkheck</a> My every day, all day. 🐧<br><br>In reply to: <a href="https://x.com/mkheck/status/1349098374788751362" rel="noopener noreferrer" target="_blank">@mkheck</a> <span class="status">1349098374788751362</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1349099535935553537',
    created: 1610485369000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joshsimmons" rel="noopener noreferrer" target="_blank">@joshsimmons</a> <a class="mention" href="https://x.com/SalesforceEng" rel="noopener noreferrer" target="_blank">@SalesforceEng</a> I can attest to the fact that <a class="mention" href="https://x.com/salesforce" rel="noopener noreferrer" target="_blank">@salesforce</a> is a great supporter and advocate of open source and standards. We\'re so glad to have their participation in the AsciiDoc WG.<br><br>In reply to: <a href="https://x.com/joshsimmons/status/1349075859466977280" rel="noopener noreferrer" target="_blank">@joshsimmons</a> <span class="status">1349075859466977280</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1349098275899469824',
    created: 1610485068000,
    type: 'post',
    text: 'I wish Firefox could get smooth scrolling working normally out of the box (on Linux, specifically). It\'s not really the physics I want to spend my time trying to solve. Just make it smooth! (Tinkering with it cuts into development time.)',
    likes: 0,
    retweets: 0
  },
  {
    id: '1348911219625791493',
    created: 1610440470000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/michael37744050" rel="noopener noreferrer" target="_blank">@michael37744050</a> This country has many flaws, absolutely. But I\'ll be DAMNED if a group of privileged assholes is going to try to destroy it. The soldiers who fought in WWII against fascism would spit in their faces for what they\'ve done.<br><br>In reply to: <a href="#1348910878637232129">1348910878637232129</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1348910878637232129',
    created: 1610440389000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/michael37744050" rel="noopener noreferrer" target="_blank">@michael37744050</a> When I put my hand on my chest each day in grade school, I pledged allegiance to this country and the tenets of democracy. If you think I\'m going to take threats to this society lightly, you\'re DEAD WRONG.<br><br>In reply to: <a href="#1348910033279414272">1348910033279414272</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1348910033279414272',
    created: 1610440188000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/michael37744050" rel="noopener noreferrer" target="_blank">@michael37744050</a> Hell no. We don\'t just move forward from an insurrection incited by a sitting president. It\'s also not over as leading members of the culpable party are still fomenting it. We must take decisive action to hold all those accountable to protect democracy at home &amp; around the world.<br><br>In reply to: <a href="https://x.com/michael37744050/status/1348427009081401345" rel="noopener noreferrer" target="_blank">@michael37744050</a> <span class="status">1348427009081401345</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1348535303078744065',
    created: 1610350845000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kenglxn" rel="noopener noreferrer" target="_blank">@kenglxn</a> It\'s partially a denial of reality and partially a refusal to accept any consequences. What that adds up to me are people who have turned their back not only on this country, but on all our ancestors who have woven it and fought for democracy. It\'s an absolute betrayal.<br><br>In reply to: <a href="https://x.com/kenglxn/status/1348528005572947968" rel="noopener noreferrer" target="_blank">@kenglxn</a> <span class="status">1348528005572947968</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1348400775651762176',
    created: 1610318771000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Congrats! I\'m rather fond of pet projects ;)<br><br>In reply to: <a href="https://x.com/maxandersen/status/1348392035590565889" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1348392035590565889</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1348349589628207105',
    created: 1610306567000,
    type: 'post',
    text: 'We can\'t get to the healing part without first dealing with the accountability part. There\'s no shortcut here.',
    likes: 17,
    retweets: 1
  },
  {
    id: '1348061479216377856',
    created: 1610237877000,
    type: 'quote',
    text: 'Again, for those who are like \'Trump doesn\'t represent the GOP". That\'s my idiot representative siding with Trump. The GOP is complicit in all this. Don\'t try to tell me otherwise because facts are not on your side.<br><br>Quoting: <a href="https://x.com/JenniferJJacobs/status/1348032696333774849" rel="noopener noreferrer" target="_blank">@JenniferJJacobs</a> <span class="status">1348032696333774849</span>',
    likes: 4,
    retweets: 1
  },
  {
    id: '1347994298822742017',
    created: 1610221860000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/wimdeblauwe" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> <a class="mention" href="https://x.com/Netlify" rel="noopener noreferrer" target="_blank">@Netlify</a> API. That\'s the point.<br><br>In reply to: <a href="https://x.com/wimdeblauwe/status/1347886110861627393" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> <span class="status">1347886110861627393</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1347844545346555904',
    created: 1610186156000,
    type: 'post',
    text: 'Is there any way to retry a <a class="mention" href="https://x.com/Netlify" rel="noopener noreferrer" target="_blank">@Netlify</a> deploy preview for a pull request other than pushing a new commit to the branch? I feel like there must be an API I\'m missing.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347697154249302023',
    created: 1610151015000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> Indeed. A vital point.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1347696532200566784" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1347696532200566784</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1347694951186894848',
    created: 1610150489000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> As to whether this fans the flames, that\'s just a matter of how seriously they take enforcement. If there is no enforcement, then yes, it will encourage worse actors. Violence is violence and must never be given a comfortable place to be exercised.<br><br>In reply to: <a href="#1347694384012148737">1347694384012148737</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1347694384012148737',
    created: 1610150354000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Absolutely not. Censoring is about opinions. When it\'s inciting violence, especially from someone who wields power that can threaten the entire globe, that\'s an entirely different conversation. Twitter made this extremely clear in the statement.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1347693757790085120" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1347693757790085120</span>',
    likes: 6,
    retweets: 0
  },
  {
    id: '1347692185580261376',
    created: 1610149830000,
    type: 'quote',
    text: 'For those who are like "I\'m cool with Republicans, just not Trump"... 👇<br><br>Quoting: <a href="https://x.com/TheSteve0/status/1347688707525021698" rel="noopener noreferrer" target="_blank">@TheSteve0</a> <span class="status">1347688707525021698</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347690935887384578',
    created: 1610149532000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PeterBell" rel="noopener noreferrer" target="_blank">@PeterBell</a> <a class="mention" href="https://x.com/realDonaldTrump" rel="noopener noreferrer" target="_blank">@realDonaldTrump</a> Spot on.<br><br>In reply to: <a href="https://x.com/PeterBell/status/1347690178882650115" rel="noopener noreferrer" target="_blank">@PeterBell</a> <span class="status">1347690178882650115</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347413117014142978',
    created: 1610083295000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tlberglund" rel="noopener noreferrer" target="_blank">@tlberglund</a> s/has/had/ Gardner is gone now, but the damage has been done.<br><br>In reply to: <a href="#1347380968932274178">1347380968932274178</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347396448296398848',
    created: 1610079321000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> <a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> While I was happy with the outcome of the special election in Georgia, I did not like the means. They were among the most expensive elections in American history, which does very little to get money into the pockets of Americans. It\'s a damn waste. We have to stop this.<br><br>In reply to: <a href="#1347396088148316162">1347396088148316162</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347396088148316162',
    created: 1610079235000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> <a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> I support the spirit of what you\'re saying, just not the solution of requiring people to vote (which might have worked if the country started out that way). But we can strike at the roots by reforming campaign financing (candidates get funded by vouchers) &amp; voting rights.<br><br>In reply to: <a href="https://x.com/brunoborges/status/1347383856689676289" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1347383856689676289</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347394735229423616',
    created: 1610078912000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> <a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> And I talk about voting in Colorado a lot because it\'s such a fantastic experience that I really want it for everyone. I\'ve never had to stand in a line here. Instead, I use that time to read about each candidate.<br><br>In reply to: <a href="https://x.com/jasondlee/status/1347393699064520706" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">1347393699064520706</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347394128003301377',
    created: 1610078768000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> <a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> Uninformed people already vote. I don\'t think there would be more or less uninformed people voting if all people were required to vote. I just think requiring people to vote creates problems we don\'t need to have right now (anti-voters...geez).<br><br>In reply to: <a href="https://x.com/jasondlee/status/1347384042895900672" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">1347384042895900672</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347393372844040193',
    created: 1610078588000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> <a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> I think that requiring people to vote would be the wrong approach (and not because they are uninformed, but just because it wouldn\'t work). Where we can start is making sure everyone who wants to vote can. And we\'re so unbelievably far from that, it gives us plenty of work to do.<br><br>In reply to: <a href="https://x.com/jasondlee/status/1347384042895900672" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">1347384042895900672</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347380968932274178',
    created: 1610075630000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tlberglund" rel="noopener noreferrer" target="_blank">@tlberglund</a> It\'s also inexcusable because you live in a state that has one of the worse Senators in the nation (Gardner) who has not only sided with Trump, but enabled and championed him. That not only hurts residents, but the precious environment + wilderness over which this state presides.<br><br>In reply to: <a href="https://x.com/tlberglund/status/1347365153919049728" rel="noopener noreferrer" target="_blank">@tlberglund</a> <span class="status">1347365153919049728</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1347378811357204487',
    created: 1610075116000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nearyd" rel="noopener noreferrer" target="_blank">@nearyd</a> <a class="mention" href="https://x.com/tlberglund" rel="noopener noreferrer" target="_blank">@tlberglund</a> If the only outcome that could be achieved is that last part, then we\'d certainly be better off than we are now.<br><br>In reply to: <a href="https://x.com/nearyd/status/1347378482498719744" rel="noopener noreferrer" target="_blank">@nearyd</a> <span class="status">1347378482498719744</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347378151702228994',
    created: 1610074959000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nearyd" rel="noopener noreferrer" target="_blank">@nearyd</a> <a class="mention" href="https://x.com/tlberglund" rel="noopener noreferrer" target="_blank">@tlberglund</a> The degree to which I entertain conservative thought is in the same ballpark as your approach. I think "what role does personal responsibility and initiative play here?" If it can work fairly, and we can achieve prosperity for all, then I would agree to utilize it.<br><br>In reply to: <a href="#1347377553330167809">1347377553330167809</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347377553330167809',
    created: 1610074816000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nearyd" rel="noopener noreferrer" target="_blank">@nearyd</a> <a class="mention" href="https://x.com/tlberglund" rel="noopener noreferrer" target="_blank">@tlberglund</a> In short, conservative thought, okay (-ish). Attacks on democracy and human rights, no, full stop.<br><br>In reply to: <a href="#1347377374996828162">1347377374996828162</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347377374996828162',
    created: 1610074773000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nearyd" rel="noopener noreferrer" target="_blank">@nearyd</a> <a class="mention" href="https://x.com/tlberglund" rel="noopener noreferrer" target="_blank">@tlberglund</a> I\'m not going to deny that I entertain some conservative ideas from time to time. (Again, doesn\'t mean I accept them, but I do think about them). Although I wouldn\'t champion it, I do acknowledge there is room for a new right. We must get away from attacks on democracy.<br><br>In reply to: <a href="https://x.com/nearyd/status/1347376984909873152" rel="noopener noreferrer" target="_blank">@nearyd</a> <span class="status">1347376984909873152</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1347376990320427008',
    created: 1610074682000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nearyd" rel="noopener noreferrer" target="_blank">@nearyd</a> <a class="mention" href="https://x.com/tlberglund" rel="noopener noreferrer" target="_blank">@tlberglund</a> Yep, I acknowledge that you\'re arguing the point in the context, not about your own beliefs. I\'m just saying you (or rather that point) is lost. Republicans are not a reputable party. They proved that for many years, but especially so yesterday. We have to get mad about this.<br><br>In reply to: <a href="https://x.com/nearyd/status/1347376030122696707" rel="noopener noreferrer" target="_blank">@nearyd</a> <span class="status">1347376030122696707</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347376384016961537',
    created: 1610074537000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nearyd" rel="noopener noreferrer" target="_blank">@nearyd</a> <a class="mention" href="https://x.com/tlberglund" rel="noopener noreferrer" target="_blank">@tlberglund</a> Yes, I acknowledge that. And I\'m fine with conservative thought in political discourse (even though I\'ll debate in opposition). To that person, I say be an independent then. The Republicans are by and large fascists and anti-science, not conservatives.<br><br>In reply to: <a href="https://x.com/nearyd/status/1347375234933026816" rel="noopener noreferrer" target="_blank">@nearyd</a> <span class="status">1347375234933026816</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347375792192307202',
    created: 1610074396000,
    type: 'quote',
    text: 'This sums up the crux of the Republican "objections" to the election results. Look, we have to make our elections more accessible and secure using mail-in paper ballots. Yes. But buying into conspiracy theories is not how we get there. <a class="mention" href="https://x.com/JenaGriswold" rel="noopener noreferrer" target="_blank">@JenaGriswold</a> can show you the actual way.<br><br>Quoting: <a href="https://x.com/brunoborges/status/1347103377436463104" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1347103377436463104</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1347373887793688577',
    created: 1610073942000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nearyd" rel="noopener noreferrer" target="_blank">@nearyd</a> <a class="mention" href="https://x.com/tlberglund" rel="noopener noreferrer" target="_blank">@tlberglund</a> You are lost. That is simply not the case. They are one and the same. To claim otherwise is to deny the reality of the situation.<br><br>In reply to: <a href="https://x.com/nearyd/status/1347373046827528196" rel="noopener noreferrer" target="_blank">@nearyd</a> <span class="status">1347373046827528196</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347373757845753859',
    created: 1610073911000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nearyd" rel="noopener noreferrer" target="_blank">@nearyd</a> <a class="mention" href="https://x.com/tlberglund" rel="noopener noreferrer" target="_blank">@tlberglund</a> Enough are riding the popularity, and were doing this before Trump was around, that it\'s enough to say that the party is wretched. (I\'m not making excuses for Democrats, they have major issues too. But they\'re not fanning the flames of fascism).<br><br>In reply to: <a href="https://x.com/nearyd/status/1347372121438248967" rel="noopener noreferrer" target="_blank">@nearyd</a> <span class="status">1347372121438248967</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347372770787684353',
    created: 1610073676000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nearyd" rel="noopener noreferrer" target="_blank">@nearyd</a> <a class="mention" href="https://x.com/tlberglund" rel="noopener noreferrer" target="_blank">@tlberglund</a> That\'s like saying, "Look, this gang is not bad. There are several members who have not raped or murdered people." That is not a strong argument.<br><br>In reply to: <a href="https://x.com/nearyd/status/1347371382338936834" rel="noopener noreferrer" target="_blank">@nearyd</a> <span class="status">1347371382338936834</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347369872863285249',
    created: 1610072985000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tlberglund" rel="noopener noreferrer" target="_blank">@tlberglund</a> As a person who is not a woman, not LGBTQ+, not black, and not an immigrant, you\'re essentially saying that all of those people are wrong when they say that Republicans have threatened their rights, well-being, and livelihoods. That\'s pretty bold, IMO.<br><br>In reply to: <a href="https://x.com/tlberglund/status/1347365153919049728" rel="noopener noreferrer" target="_blank">@tlberglund</a> <span class="status">1347365153919049728</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347368723020017664',
    created: 1610072711000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tlberglund" rel="noopener noreferrer" target="_blank">@tlberglund</a> But I do hope you think, "wow, Dan seems dead serious about this. Maybe he\'s trying to alert me to something I should look in to." I always try to be a guiding light.<br><br>In reply to: <a href="#1347366929716060160">1347366929716060160</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347366929716060160',
    created: 1610072283000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tlberglund" rel="noopener noreferrer" target="_blank">@tlberglund</a> I\'m definitely willing to give you the benefit of the doubt beyond one Twitter thread. (If we weren\'t in a pandemic, I\'d say beers tomorrow).<br><br>In reply to: <a href="#1347366648873775104">1347366648873775104</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347366648873775104',
    created: 1610072216000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tlberglund" rel="noopener noreferrer" target="_blank">@tlberglund</a> For me, the acknowledgement of human rights &amp; the principles of democracy are central criteria of friendships to me. Maybe you have trouble seeing how these things are being violated. Okay, we can discuss it. But if you aren\'t willing to acknowledge it after that, it\'s a problem.<br><br>In reply to: <a href="#1347366190197338112">1347366190197338112</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347366190197338112',
    created: 1610072107000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tlberglund" rel="noopener noreferrer" target="_blank">@tlberglund</a> The party was on this path before Trump, so while I hate him for what he\'s done, you can\'t pin it all on him. The things I said are baked deep into that party. That\'s just a fact. Just look at the policies they have advocated for against woman and LGBTQ+, to name a few.<br><br>In reply to: <a href="https://x.com/tlberglund/status/1347365153919049728" rel="noopener noreferrer" target="_blank">@tlberglund</a> <span class="status">1347365153919049728</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347365741813669892',
    created: 1610072000000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> Not until we at least clean up the mess the previous administration made (not just because it was "the other party", but because they truly screwed things up). And then, only if we add ranked choice voting so we can get some more independents in there.<br><br>In reply to: <a href="https://x.com/brunoborges/status/1347362424806993923" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1347362424806993923</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1347361714501603328',
    created: 1610071040000,
    type: 'post',
    text: 'If you want to be an independent conservative, fine. We can debate the merits of solutions to societal challenges. But that assumes we both start with the best interest and rights of fellow citizens in mind.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347361332618690560',
    created: 1610070949000,
    type: 'post',
    text: 'It absolutely makes me sick to my stomach that there are intellectual white men who still support Republicans* after all they\'ve done to destroy this country. Get over your damn selves!<br><br>* I\'m not talking about leaning conservative, which is different, albeit ill informed.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1347358009492606977',
    created: 1610070156000,
    type: 'post',
    text: 'After the events of yesterday, maybe you understand better why I have taken this position. I want no association with these attacks on democracy. I\'m dead serious about this.<br><br>Quoting: <a href="#1323370680251940864">1323370680251940864</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1347355653245964289',
    created: 1610069595000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tlberglund" rel="noopener noreferrer" target="_blank">@tlberglund</a> I should add anti-science and anti-environment to that list as well.<br><br>In reply to: <a href="#1347355511826518017">1347355511826518017</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347355511826518017',
    created: 1610069561000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tlberglund" rel="noopener noreferrer" target="_blank">@tlberglund</a> At this point, you simply cannot separate "Republican" from white supremacy, racism, anti-woman, anti-LGBTQ+, and anti-immigrant. There is room for conservative thought in political discourse, but this party is not that at all. It\'s anti-democratic and anti-human rights.<br><br>In reply to: <a href="https://x.com/tlberglund/status/1347349088862261248" rel="noopener noreferrer" target="_blank">@tlberglund</a> <span class="status">1347349088862261248</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347354920622542848',
    created: 1610069420000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tlberglund" rel="noopener noreferrer" target="_blank">@tlberglund</a> The gridlock that\'s occurred is an extraordinarily reckless thing to champion. Lives have been lost &amp; rights have been eroded because of the gridlock. I\'m disappointed in that statement, as well as most of this thread. I expected so much better from a person I call a friend.<br><br>In reply to: <a href="https://x.com/tlberglund/status/1347349088862261248" rel="noopener noreferrer" target="_blank">@tlberglund</a> <span class="status">1347349088862261248</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347348246914469890',
    created: 1610067829000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GeoffMiami" rel="noopener noreferrer" target="_blank">@GeoffMiami</a> Probably because we don\'t believe or trust him. Having said that, still good news if true.<br><br>In reply to: <a href="https://x.com/GeoffMiami/status/1347346436564934658" rel="noopener noreferrer" target="_blank">@GeoffMiami</a> <span class="status">1347346436564934658</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1347110825429925888',
    created: 1610011223000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/andrespedes" rel="noopener noreferrer" target="_blank">@andrespedes</a> If you think I would side with the traitors that stormed the Capitol today, then you don\'t know me very well ;)<br><br>In reply to: <a href="#1347109994328588288">1347109994328588288</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1347110341780520963',
    created: 1610011108000,
    type: 'post',
    text: 'To be clear, I\'m saying if you object to the results of a certified election.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1347109994328588288',
    created: 1610011025000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/andrespedes" rel="noopener noreferrer" target="_blank">@andrespedes</a> The objection to the election results.<br><br>In reply to: <a href="https://x.com/andrespedes/status/1347104288238161922" rel="noopener noreferrer" target="_blank">@andrespedes</a> <span class="status">1347104288238161922</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1347109910580875267',
    created: 1610011005000,
    type: 'post',
    text: 'Ugh. Another roadblock in my quest for a reliable SVG strategy. Firefox does not cache references to the same SVG that is referenced using different fragment identifiers (such as an icon in a sprite). <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1027106" rel="noopener noreferrer" target="_blank">bugzilla.mozilla.org/show_bug.cgi?id=1027106</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1347102771099549697',
    created: 1610009303000,
    type: 'quote',
    text: 'At this point, to say otherwise is an act of treason as far as I\'m concerned. Not just treason against the US, but against democracy. This isn\'t about which politician or policy you support. It\'s about whether you defend democracy, something I once thought all Americans did.<br><br>Quoting: <a href="https://x.com/ajplus/status/1347101456726446081" rel="noopener noreferrer" target="_blank">@ajplus</a> <span class="status">1347101456726446081</span>',
    likes: 5,
    retweets: 0
  },
  {
    id: '1347091309908033544',
    created: 1610006570000,
    type: 'post',
    text: 'Call a spade a spade. Also, call a traitor a traitor.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1347041478267133952',
    created: 1609994689000,
    type: 'post',
    text: 'I\'m stress baking two desserts tonight. 🍰',
    likes: 8,
    retweets: 0
  },
  {
    id: '1346750221330464770',
    created: 1609925248000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CKrger" rel="noopener noreferrer" target="_blank">@CKrger</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> The tentative plan is to create some sort of plugin portal page / site. The documentation doesn\'t replace all the pages on <a href="http://asciidoctor.org" rel="noopener noreferrer" target="_blank">asciidoctor.org</a>, so that list is still available. <a href="https://asciidoctor.org/docs/extensions/" rel="noopener noreferrer" target="_blank">asciidoctor.org/docs/extensions/</a><br><br>In reply to: <a href="https://x.com/CKrger/status/1346747961242431495" rel="noopener noreferrer" target="_blank">@CKrger</a> <span class="status">1346747961242431495</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1346731102623264770',
    created: 1609920690000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CKrger" rel="noopener noreferrer" target="_blank">@CKrger</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> 3rd party extensions will likely have their own sites, but can reuse a lot of the infra that was put in place for this one. We want to avoid the temptation of creating a monolithic site, so it will act more like a family of sites in that regard.<br><br>In reply to: <a href="https://x.com/CKrger/status/1346728192414015489" rel="noopener noreferrer" target="_blank">@CKrger</a> <span class="status">1346728192414015489</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1346626592622088194',
    created: 1609895773000,
    type: 'post',
    text: 'To learn more about Antora, see <a href="https://docs.antora.org" rel="noopener noreferrer" target="_blank">docs.antora.org</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1346624966448451585',
    created: 1609895385000,
    type: 'post',
    text: 'As of today, the old user manual for Asciidoctor is being redirected to the new docs site. Rest assured, the router preserves deep links. So if you\'ve bookmarked a section, the router will direct you to the corresponding page in the new docs.',
    likes: 4,
    retweets: 1
  },
  {
    id: '1346623846493765634',
    created: 1609895118000,
    type: 'reply',
    text: 'The new docs site for <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> also hosts the documentation for the AsciiDoc language, which we\'ve submitted as the initial contribution for the AsciiDoc Language project under the AsciiDoc Working Group (<a class="mention" href="https://x.com/EclipseFdn" rel="noopener noreferrer" target="_blank">@EclipseFdn</a>). You can read more about it at <a href="https://docs.asciidoctor.org/asciidoc/latest/#about-this-documentation" rel="noopener noreferrer" target="_blank">docs.asciidoctor.org/asciidoc/latest/#about-this-documentation</a><br><br>In reply to: <a href="#1346623458860417024">1346623458860417024</a>',
    likes: 14,
    retweets: 4
  },
  {
    id: '1346623458860417024',
    created: 1609895026000,
    type: 'post',
    text: 'Starting a new year with a new chapter for <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a>, I\'m thrilled to announce that the new docs site is live at <a href="https://docs.asciidoctor.org" rel="noopener noreferrer" target="_blank">docs.asciidoctor.org</a>. All content is written in AsciiDoc and published by Antora. The site aggregates docs from participating projects, with more on the way!',
    likes: 81,
    retweets: 25
  },
  {
    id: '1346598934714544128',
    created: 1609889179000,
    type: 'quote',
    text: 'It\'s not really breaking news if we already knew it was going to happen. And it\'s going to keep on happening until we all collectively demand that things change. #DefundThePolice #BlackLivesMatter<br><br>Quoting: <a href="https://x.com/AttorneyCrump/status/1346566659860000768" rel="noopener noreferrer" target="_blank">@AttorneyCrump</a> <span class="status">1346566659860000768</span>',
    likes: 2,
    retweets: 0,
    tags: ['defundthepolice', 'blacklivesmatter']
  },
  {
    id: '1346305664432279553',
    created: 1609819258000,
    type: 'post',
    text: '"English is like JavaScript. Full of idiosyncrasies."<br><br>Can\'t argue with that 🙃',
    likes: 5,
    retweets: 1
  },
  {
    id: '1346183718558306304',
    created: 1609790184000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rotnroll666" rel="noopener noreferrer" target="_blank">@rotnroll666</a> I think Sarah would be like "hey, where did the oatmilk carafe disappear to?!?" when she goes to make her mocha the next morning ;)<br><br>In reply to: <a href="https://x.com/rotnroll666/status/1346179619041513475" rel="noopener noreferrer" target="_blank">@rotnroll666</a> <span class="status">1346179619041513475</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1346168628014505985',
    created: 1609786586000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rkennke" rel="noopener noreferrer" target="_blank">@rkennke</a> I look at it two ways. First, as a puzzle. Second, as completely uninterrupted time to listen to music. It\'s quite therapeutic on both accounts.<br><br>In reply to: <a href="https://x.com/rkennke/status/1346082764987240450" rel="noopener noreferrer" target="_blank">@rkennke</a> <span class="status">1346082764987240450</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1346053681767186432',
    created: 1609759180000,
    type: 'post',
    text: 'You know when you put the last item in the dishwasher and it fits like a tetris piece? Yeah, that\'s a good feeling.',
    likes: 11,
    retweets: 0
  },
  {
    id: '1346036430871560192',
    created: 1609755067000,
    type: 'quote',
    text: 'No one is too small to make a difference. In my case, no one is too far away to make a difference either. Greta woke up the feeling inside of me to truly love and care for the planet and the animals that coexist with us on it.<br><br>Quoting: <a href="https://x.com/NathanMackBrown/status/1345729149038694400" rel="noopener noreferrer" target="_blank">@NathanMackBrown</a> <span class="status">1345729149038694400</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1346031813462933509',
    created: 1609753967000,
    type: 'post',
    text: 'I\'m so angry at Chrome over the decision to apply such a careless and naive security policy for SVG references that I\'ll never use it as my primary browser ever again. This policy is actively harming the web community and it infuriates me. Thankfully, Firefox is more thoughtful.<br><br>Quoting: <a href="#1345551549880094722">1345551549880094722</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1345801263670169600',
    created: 1609698999000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ideabile" rel="noopener noreferrer" target="_blank">@ideabile</a> <a class="mention" href="https://x.com/DuncanLock" rel="noopener noreferrer" target="_blank">@DuncanLock</a> Because I actually have file permissions on my operating system. Regardless, as I stated, I could understand disabling scripting in SVG, but not breaking it entirely. Chrome is just wrong here.<br><br>In reply to: <a href="https://x.com/ideabile/status/1345698725268758535" rel="noopener noreferrer" target="_blank">@ideabile</a> <span class="status">1345698725268758535</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1345663397036220418',
    created: 1609666129000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ideabile" rel="noopener noreferrer" target="_blank">@ideabile</a> <a class="mention" href="https://x.com/DuncanLock" rel="noopener noreferrer" target="_blank">@DuncanLock</a> It could just load the SVG vector data without running scripts. Completing breaking SVG is absurd and damaging. And besides, what does that have to do with the file protocol anyway? I can still load JavaScript files.<br><br>In reply to: <a href="https://x.com/ideabile/status/1345649489403654146" rel="noopener noreferrer" target="_blank">@ideabile</a> <span class="status">1345649489403654146</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1345608555194245122',
    created: 1609653054000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DuncanLock" rel="noopener noreferrer" target="_blank">@DuncanLock</a> It\'s really, really bad for static site generators that rely on shared assets that are preoptimized.<br><br>In reply to: <a href="#1345607355682103297">1345607355682103297</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1345607904829661184',
    created: 1609652899000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GeoffMiami" rel="noopener noreferrer" target="_blank">@GeoffMiami</a> So totally agree. This line of attack is myopic, deflating, and self-aggrandizing.<br><br>In reply to: <a href="https://x.com/GeoffMiami/status/1345606179758747656" rel="noopener noreferrer" target="_blank">@GeoffMiami</a> <span class="status">1345606179758747656</span>',
    likes: 11,
    retweets: 0
  },
  {
    id: '1345607355682103297',
    created: 1609652768000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DuncanLock" rel="noopener noreferrer" target="_blank">@DuncanLock</a> I\'m using svg use to load icons from a sprite, then set the color using CSS. I\'m not going to load each icon from separate files, and I\'m not going to embed the whole svg into the page unnecessarily.<br><br>In reply to: <a href="https://x.com/DuncanLock/status/1345601020697534464" rel="noopener noreferrer" target="_blank">@DuncanLock</a> <span class="status">1345601020697534464</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1345584018218106882',
    created: 1609647204000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DuncanLock" rel="noopener noreferrer" target="_blank">@DuncanLock</a> But it makes no sense in this case. Why not break all images? It\'s just draconian.<br><br>In reply to: <a href="https://x.com/DuncanLock/status/1345565865920483329" rel="noopener noreferrer" target="_blank">@DuncanLock</a> <span class="status">1345565865920483329</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1345552561579782144',
    created: 1609639704000,
    type: 'post',
    text: 'The workaround is to use a CSS filter, but that requires computing how to get from black to the color you want using a combination of color transforms. Fun.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1345552363432550401',
    created: 1609639657000,
    type: 'post',
    text: 'It\'s incredibly frustrating that you cannot change the fill color of an SVG that is loaded as a background image in CSS. This is another reason why SVG is so promising in theory, but so deficient in practice.',
    likes: 5,
    retweets: 1
  },
  {
    id: '1345551793112059904',
    created: 1609639521000,
    type: 'post',
    text: 'The workaround is to pass --allow-file-access-from-files when starting Chrome. But that\'s a very drastic fix that has substantial security implications.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1345551549880094722',
    created: 1609639463000,
    type: 'post',
    text: 'Chrome has totally broken SVG by refusing to load a relative path referenced by the &lt;use&gt; tag when the HTML page is accessed over the file protocol. This may seem highly specific, but breaking this case severely cripples SVG.',
    likes: 5,
    retweets: 0
  },
  {
    id: '1345507409955938304',
    created: 1609628939000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bk2204" rel="noopener noreferrer" target="_blank">@bk2204</a> I was thinking you could open in Word and re-save. But for full automation, direct to Word is certainly the better route. Another option would be to make Word XML using the same approach.<br><br>In reply to: <a href="https://x.com/bk2204/status/1345486485039030273" rel="noopener noreferrer" target="_blank">@bk2204</a> <span class="status">1345486485039030273</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1345484451258589185',
    created: 1609623465000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bk2204" rel="noopener noreferrer" target="_blank">@bk2204</a> You might also be interested in <a href="https://github.com/gregturn/asciidoctor-packt" rel="noopener noreferrer" target="_blank">github.com/gregturn/asciidoctor-packt</a><br><br>In reply to: <a href="https://x.com/bk2204/status/1345166589939875846" rel="noopener noreferrer" target="_blank">@bk2204</a> <span class="status">1345166589939875846</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1345201846827196418',
    created: 1609556087000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/SaraSoueidan" rel="noopener noreferrer" target="_blank">@SaraSoueidan</a> How do you feel about Chrome breaking SVG &lt;use&gt; with a relative path when the page is accessed using the file protocol (without the --allow-file-access-from-files switch)? Is this just a limitation of SVG or is Chrome being draconian?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1345150466804498432',
    created: 1609543837000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DuncanLock" rel="noopener noreferrer" target="_blank">@DuncanLock</a> Whoa, That looks pretty cool! Thanks for sharing.<br><br>In reply to: <a href="https://x.com/DuncanLock/status/1345130428059897857" rel="noopener noreferrer" target="_blank">@DuncanLock</a> <span class="status">1345130428059897857</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1344952786765504512',
    created: 1609496707000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/netskymusic" rel="noopener noreferrer" target="_blank">@netskymusic</a> Stellar set as always! You were flying under water like those manta rays around you. I\'m really liking the new record, btw.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1344922318712184832',
    created: 1609489442000,
    type: 'post',
    text: 'To everyone in the Asciidoctor &amp; Antora communities, I wish you a very happy new year! 🎉 I look forward to what we\'re going to accomplish together in what promises to be an important year for the two projects. We\'ll start off on the right foot with exciting news next week.',
    likes: 55,
    retweets: 3
  },
  {
    id: '1344902366508634114',
    created: 1609484685000,
    type: 'post',
    text: '2020, GOOD RIDDANCE!',
    likes: 3,
    retweets: 0
  }
])
