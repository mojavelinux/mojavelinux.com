'use strict'

inflateTweets([
  {
    id: '21089747988381696',
    created: 1293863162000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> @brianleathem Honored to be in the group. Let\'s keep the conversations going on into 2011.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/21045936755048448" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">21045936755048448</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21088952756731905',
    created: 1293862972000,
    type: 'post',
    text: 'Instead of resolutions for the New Year, my wife &amp; I are making plans.',
    likes: 0,
    retweets: 0
  },
  {
    id: '21088748347330560',
    created: 1293862924000,
    type: 'post',
    text: '2011 is going to be an off-centered year for off-centered people. Woot!',
    likes: 0,
    retweets: 0
  },
  {
    id: '21088332121382912',
    created: 1293862824000,
    type: 'post',
    text: 'Fireworks near the Ben Franklin bridge reign in 2011 for me, my wife &amp; our philly cronies. Cheers!',
    photos: ['<div class="entry"><img class="photo" src="media/21088332121382912.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '20988259790225408',
    created: 1293838965000,
    type: 'post',
    text: 'Downtown philly looks very festive! The bridge is doubling as an xmas tree.',
    likes: 0,
    retweets: 0
  },
  {
    id: '20987800622997504',
    created: 1293838856000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/codylerum" rel="noopener noreferrer" target="_blank">@codylerum</a> Oh wait, I forgot. We need flash to play ads too. What would we do without those ;)<br><br>In reply to: <a href="https://x.com/codylerum/status/20923641889824770" rel="noopener noreferrer" target="_blank">@codylerum</a> <span class="status">20923641889824770</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '20987356005797889',
    created: 1293838750000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> <a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Yep, I know you\'ve been around, but this will be more fun ;)<br><br>In reply to: <a href="https://x.com/JohnAment/status/20956493620715520" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">20956493620715520</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '20923286175088640',
    created: 1293823474000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/codylerum" rel="noopener noreferrer" target="_blank">@codylerum</a> Flash should really die anyway. All we needed it for was video. HTML5 baby.<br><br>In reply to: <a href="https://x.com/codylerum/status/20917796418363392" rel="noopener noreferrer" target="_blank">@codylerum</a> <span class="status">20917796418363392</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '20923166469656576',
    created: 1293823446000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/the_jamezp" rel="noopener noreferrer" target="_blank">@the_jamezp</a> I\'ve got a can\'t lose situation for evaluation. If I don\'t like it, my wife is all over it when she is due for an upgrade.<br><br>In reply to: <a href="https://x.com/the_jamezp/status/20919780231872513" rel="noopener noreferrer" target="_blank">@the_jamezp</a> <span class="status">20919780231872513</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '20918943103324160',
    created: 1293822439000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/codylerum" rel="noopener noreferrer" target="_blank">@codylerum</a> No problems with flash that I can tell. I\'m watching hulu just fine. Finally no sound conflicts in #maverick.<br><br>In reply to: <a href="https://x.com/codylerum/status/20917796418363392" rel="noopener noreferrer" target="_blank">@codylerum</a> <span class="status">20917796418363392</span>',
    likes: 0,
    retweets: 0,
    tags: ['maverick']
  },
  {
    id: '20916860501360640',
    created: 1293821942000,
    type: 'post',
    text: 'My HP Envy has the multi-touch capability. Got two-finger and circular scrolling setup. So freakin cool.',
    likes: 0,
    retweets: 0
  },
  {
    id: '20916626526310400',
    created: 1293821887000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> <a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Fantastic. We\'re thrilled to have your expertise. I look forward to hacking alongside you ;)<br><br>In reply to: <a href="https://x.com/JohnAment/status/20832992180051968" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">20832992180051968</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '20916405654261760',
    created: 1293821834000,
    type: 'post',
    text: 'I\'m finally upgrading to #Ubuntu 10.10. Also, my first time using 64 bit Linux. So far, excellent.',
    likes: 0,
    retweets: 1,
    tags: ['ubuntu']
  },
  {
    id: '20916148237238273',
    created: 1293821773000,
    type: 'post',
    text: 'I decided on the myTouch 4G over the G2. Nexus S wasn\'t an option since I\'ll still serving my two year contract sentence. #android #tmobile',
    likes: 0,
    retweets: 0,
    tags: ['android', 'tmobile']
  },
  {
    id: '20915897082322944',
    created: 1293821713000,
    type: 'post',
    text: '2011, there\'s no confusion how to pronounce you. So bring it on.',
    likes: 0,
    retweets: 0
  },
  {
    id: '20651727258648576',
    created: 1293758730000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> That\'s Sarah and my mom. The framed images are a gift from my mom and I. I can\'t wait to see them done!<br><br>In reply to: <a href="https://x.com/lightguardjp/status/20618001501921280" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">20618001501921280</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '20644465781776385',
    created: 1293756998000,
    type: 'post',
    text: 'Cool! #Ubuntu styled all the Linux man pages for web viewing <a href="http://manpages.ubuntu.com/manpages/maverick/man1/bash.1.html" rel="noopener noreferrer" target="_blank">manpages.ubuntu.com/manpages/maverick/man1/bash.1.html</a>',
    likes: 1,
    retweets: 2,
    tags: ['ubuntu']
  },
  {
    id: '20593889299660800',
    created: 1293744940000,
    type: 'post',
    text: 'The Hubble images are in, getting dressed up now.',
    photos: ['<div class="entry"><img class="photo" src="media/20593889299660800.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '20575848809635840',
    created: 1293740639000,
    type: 'post',
    text: 'My new #Panera take-out salad eating bowl. Mmmmm.',
    photos: ['<div class="entry"><img class="photo" src="media/20575848809635840.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['panera']
  },
  {
    id: '20324079215054849',
    created: 1293680612000,
    type: 'post',
    text: 'Two signs that Dan wrapped this gift.',
    photos: ['<div class="entry"><img class="photo" src="media/20324079215054849.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '19904191354904577',
    created: 1293580503000,
    type: 'post',
    text: 'Despite holiday break, big news from #JBoss. AS 6 is final <a href="https://in.relation.to/Bloggers/JBossAS600FinalReleased" rel="noopener noreferrer" target="_blank">in.relation.to/Bloggers/JBossAS600FinalReleased</a> Seam 3 modules "stocking stuffer" release <a href="https://in.relation.to/Bloggers/Seam3ModulesStockingStufferRelease" rel="noopener noreferrer" target="_blank">in.relation.to/Bloggers/Seam3ModulesStockingStufferRelease</a>',
    likes: 0,
    retweets: 13,
    tags: ['jboss']
  },
  {
    id: '19796284437168128',
    created: 1293554776000,
    type: 'post',
    text: 'I\'m *finally* getting around to setting up my HP Envy beats edition laptop. Black and red. Awesome.',
    likes: 0,
    retweets: 0
  },
  {
    id: '19777593112793088',
    created: 1293550320000,
    type: 'post',
    text: 'My wife found a UFO in the freezer. "Unidentified Food Object"',
    likes: 1,
    retweets: 0
  },
  {
    id: '19755505517858816',
    created: 1293545054000,
    type: 'post',
    text: 'Even better, when #github fails, it tells you to check twitter for status. Oh, right, like that\'s your more reliable option for news :)',
    likes: 0,
    retweets: 1,
    tags: ['github']
  },
  {
    id: '19755278165606400',
    created: 1293545000000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> It\'s orthogonal. An SPI in Java is activated when a services file is found w/ 1 or more implementations, no matter where.<br><br>In reply to: <a href="https://x.com/JohnAment/status/19529678964396032" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">19529678964396032</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '19754823045877760',
    created: 1293544891000,
    type: 'post',
    text: 'Apparently #github has an angry psychedelic pink #fail unicorn. They go down like rainbow bright.',
    photos: ['<div class="entry"><img class="photo" src="media/19754823045877760.png"></div>'],
    likes: 0,
    retweets: 2,
    tags: ['github', 'fail']
  },
  {
    id: '19753387172044800',
    created: 1293544549000,
    type: 'post',
    text: 'Outlet overload!',
    photos: ['<div class="entry"><img class="photo" src="media/19753387172044800.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '19736829771128832',
    created: 1293540601000,
    type: 'post',
    text: 'Happy Birthday to <a class="mention" href="https://x.com/plmuir" rel="noopener noreferrer" target="_blank">@plmuir</a>!',
    likes: 0,
    retweets: 3
  },
  {
    id: '19732109048414209',
    created: 1293539476000,
    type: 'post',
    text: 'Is it just me or does Sam Calagione (Dogfish Head, Brew Masters) look like Rob Morrow (Numbers)? #uncanny',
    likes: 0,
    retweets: 0,
    tags: ['uncanny']
  },
  {
    id: '18944326604165120',
    created: 1293351654000,
    type: 'post',
    text: 'Got everything I wanted for #xmas. I simply enjoyed &amp; appreciated what I already have (oh, and my new speaking boots) #cheers',
    likes: 0,
    retweets: 0,
    tags: ['xmas', 'cheers']
  },
  {
    id: '18873126511190017',
    created: 1293334678000,
    type: 'post',
    text: 'The frozen pond at dusk at my wife\'s family farm.',
    photos: ['<div class="entry"><img class="photo" src="media/18873126511190017.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '18796681206177792',
    created: 1293316452000,
    type: 'post',
    text: 'My wife is doing some regifting this year. Don\'t worry, it\'s not *your* gift. Happy Christmas!',
    likes: 0,
    retweets: 0
  },
  {
    id: '17750657817640962',
    created: 1293067061000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> My guess is what they really want is something like PrettyFaces...urlrewriting.<br><br>In reply to: <a href="https://x.com/jasondlee/status/17708260194783232" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">17708260194783232</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17750486157365248',
    created: 1293067020000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> They want education :) I think this is just a matter of addressing the question clearly. Then pointing them at JAX-RS.<br><br>In reply to: <a href="https://x.com/jasondlee/status/17708260194783232" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">17708260194783232</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17730172270678016',
    created: 1293062177000,
    type: 'reply',
    text: '@brianleathem <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Let\'s do it!<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17724169445707776',
    created: 1293060745000,
    type: 'reply',
    text: '@brianleathem Want to add that as a feature to Seam Faces? I think you can leverage the Seam Faces navigation event.<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17703683223453696',
    created: 1293055861000,
    type: 'post',
    text: 'Hopefully "The Cloud" is *one* of the themes for #JavaEE 7, not the only theme. Because there are other crux issues to address.',
    likes: 0,
    retweets: 1,
    tags: ['javaee']
  },
  {
    id: '17702217632972800',
    created: 1293055512000,
    type: 'post',
    text: 'More good news for #Seam 2 devs. #JSF 2 support is coming! To help, get in touch w/ Marek via the forums <a href="http://sfwk.org/Community/SeamUsers" rel="noopener noreferrer" target="_blank">sfwk.org/Community/SeamUsers</a>',
    likes: 0,
    retweets: 2,
    tags: ['seam', 'jsf']
  },
  {
    id: '17701567599747072',
    created: 1293055357000,
    type: 'post',
    text: 'Good news for #Seam 2 developers! #Seam 2 has a new lead (Marek Novotny) &amp; he\'s released 2.2.1.CR3! <a href="http://www.seamframework.org/Community/Seam221CR3IsOut" rel="noopener noreferrer" target="_blank">www.seamframework.org/Community/Seam221CR3IsOut</a>',
    likes: 0,
    retweets: 1,
    tags: ['seam', 'seam']
  },
  {
    id: '17701083832913921',
    created: 1293055241000,
    type: 'post',
    text: 'We are rolling out the #seam 3 modules to use #solder instead of #weld extensions. Keep an eye on <a href="http://sfwk.org/Seam3/Downloads" rel="noopener noreferrer" target="_blank">sfwk.org/Seam3/Downloads</a>',
    likes: 0,
    retweets: 0,
    tags: ['seam', 'solder', 'weld']
  },
  {
    id: '17700438270808064',
    created: 1293055087000,
    type: 'post',
    text: 'I noticed in the most recent #JIRA there are some really nice custom view features, and I\'ll definitely be making use of those.',
    likes: 0,
    retweets: 0,
    tags: ['jira']
  },
  {
    id: '17700244057751553',
    created: 1293055041000,
    type: 'post',
    text: 'Let me restate my #JIRA request, since there\'s some confusion. It should be easier to navigate to outstanding issues in the default view.',
    likes: 0,
    retweets: 0,
    tags: ['jira']
  },
  {
    id: '17390533412392960',
    created: 1292981200000,
    type: 'post',
    text: 'I was getting a little hot today. I better be careful, people might to think I\'m an angry guy :) Just need some rock band to chill out.',
    likes: 0,
    retweets: 0
  },
  {
    id: '17389474115756033',
    created: 1292980948000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/the_jamezp" rel="noopener noreferrer" target="_blank">@the_jamezp</a> Fantastic. If you\'re interested, #Solder is great opportunity to experiment w/ JBoss Logging in use, so maybe a 2nd project :)<br><br>In reply to: <a href="https://x.com/the_jamezp/status/17371761779671040" rel="noopener noreferrer" target="_blank">@the_jamezp</a> <span class="status">17371761779671040</span>',
    likes: 0,
    retweets: 0,
    tags: ['solder']
  },
  {
    id: '17371376620937218',
    created: 1292976633000,
    type: 'post',
    text: 'Got a bit stuck on JBoss Logging code generation, but thanks to the pros I worked through it. Looks like I need to tweak #Solder though.',
    likes: 0,
    retweets: 0,
    tags: ['solder']
  },
  {
    id: '17371022344851457',
    created: 1292976549000,
    type: 'post',
    text: 'There is a link to the outstanding issues for a project in #JIRA. It\'s just hidden. Summary &gt; Filters &gt; Outstanding. It should a main link.',
    likes: 1,
    retweets: 0,
    tags: ['jira']
  },
  {
    id: '17370600754380800',
    created: 1292976448000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tommoors" rel="noopener noreferrer" target="_blank">@tommoors</a> I\'m not saying #JIRA sucks as a whole. I\'m just saying that I have to click a lot to get to the open bug list by default.<br><br>In reply to: <a href="https://x.com/tommoors/status/17358664234958848" rel="noopener noreferrer" target="_blank">@tommoors</a> <span class="status">17358664234958848</span>',
    likes: 0,
    retweets: 0,
    tags: ['jira']
  },
  {
    id: '17370305081114625',
    created: 1292976378000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/the_jamezp" rel="noopener noreferrer" target="_blank">@the_jamezp</a> Thanks for working through it with me. The annotation processor is now working. Now that the stars align :)<br><br>In reply to: <a href="https://x.com/the_jamezp/status/17354138065047552" rel="noopener noreferrer" target="_blank">@the_jamezp</a> <span class="status">17354138065047552</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17353759151620097',
    created: 1292972433000,
    type: 'post',
    text: 'So let\'s see, #JIRA can make the user interface not suck, or 10,000 developers each have to create a filter. Clearly, that\'s efficient.',
    likes: 0,
    retweets: 1,
    tags: ['jira']
  },
  {
    id: '17353394465275904',
    created: 1292972346000,
    type: 'post',
    text: 'Annotation processing in #Maven is an embarrassment. Frankly, it\'s downright shabby. 4 hours &amp; not a single line of code is generated. #fail',
    likes: 0,
    retweets: 0,
    tags: ['maven', 'fail']
  },
  {
    id: '17326712454905856',
    created: 1292965984000,
    type: 'post',
    text: '#JIRA devs, please add a big button for "Outstanding Issues" front and center. That\'s what I\'m interested in most of the time.',
    likes: 0,
    retweets: 0,
    tags: ['jira']
  },
  {
    id: '17267404358811648',
    created: 1292951844000,
    type: 'post',
    text: 'I dig the Repower America t-shirt. <a href="https://secure.repoweramerica.org/page/contribute/2011shirt" rel="noopener noreferrer" target="_blank">secure.repoweramerica.org/page/contribute/2011shirt</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17135495356092416',
    created: 1292920395000,
    type: 'post',
    text: 'Late night updating the #Seam parent and bom poms to prepare for the next round of module releases. Stay tuned.',
    likes: 0,
    retweets: 2,
    tags: ['seam']
  },
  {
    id: '17069983129403392',
    created: 1292904775000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Finally. Lincoln can touch his toes. Now the #Seam project can move forward.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/17042911434645504" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">17042911434645504</span>',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '17068033281359873',
    created: 1292904310000,
    type: 'reply',
    text: '@netdance Yep. It\'s like if you a member of a store\'s club, the TV is $100. If you aren\'t, it\'s $1,000,000. That\'s fair.<br><br>In reply to: @netdance <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17067769606443010',
    created: 1292904248000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Swype is the sliding screen keyboard. You drag between letters and when you lift your finger, it types the word.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/17063583250653184" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">17063583250653184</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16987837022867456',
    created: 1292885190000,
    type: 'reply',
    text: '@brianleathem <a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> I was waiting for the Canadians to chime in. I was told by one doctor I have "private insurance". Free the source!<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16977540161732609',
    created: 1292882735000,
    type: 'post',
    text: 'Note to insurance companies. Giving me a list of names of in-network doctors is useless! Hmm, do I like the name Wendy? Ratings please!!!',
    likes: 0,
    retweets: 0
  },
  {
    id: '16976906159128576',
    created: 1292882584000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Right, I figured. But doesn\'t it just sound ridiculous. It\'s like you can\'t shop at a store if you don\'t have a VISA card.<br><br>In reply to: <a href="https://x.com/jasondlee/status/16973448924368896" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">16973448924368896</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16971851762769920',
    created: 1292881379000,
    type: 'post',
    text: 'I really don\'t understand the phrase "we don\'t accept that insurance" from doctors. There is a good doctor &amp; I can\'t see that doctor. #fail',
    likes: 0,
    retweets: 0,
    tags: ['fail']
  },
  {
    id: '16964927063326720',
    created: 1292879728000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> Bring it on!<br><br>In reply to: <a href="https://x.com/JohnAment/status/16924811901018112" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">16924811901018112</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16926328213213184',
    created: 1292870525000,
    type: 'post',
    text: 'Please don\'t jump to forlorn conclusions about #Seam Solder on #GlassFish. We first needed to get out a beta. Now we can address compliance.',
    likes: 0,
    retweets: 0,
    tags: ['seam', 'glassfish']
  },
  {
    id: '16914642395729920',
    created: 1292867739000,
    type: 'post',
    text: 'Version 3.0.0.Beta1 of #Seam Solder is now available. <a href="http://in.relation.to/17950.lace" rel="noopener noreferrer" target="_blank">in.relation.to/17950.lace</a> Solder is a #CDI extension writer\'s dream :)',
    likes: 0,
    retweets: 6,
    tags: ['seam', 'cdi']
  },
  {
    id: '16756822249574400',
    created: 1292830112000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> <a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Great idea. We are looking for ideas for an emblem for each #Seam module. For #solder, I like a circuitboard.<br><br>In reply to: <a href="https://x.com/JohnAment/status/16632581474754560" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">16632581474754560</span>',
    likes: 0,
    retweets: 0,
    tags: ['seam', 'solder']
  },
  {
    id: '16756592179421185',
    created: 1292830057000,
    type: 'reply',
    text: '@mwessendorf My plan is to get the myTouch 4G. If I don\'t like it, my wife will get the #G2 in 2 months and we\'ll trade.<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['g2']
  },
  {
    id: '16728790487011328',
    created: 1292823429000,
    type: 'post',
    text: 'OMG! #android *finally* has inline reply for the #gmail client. Boom! Now I am *definitely* Merry Christmas-ing myself w/ a new phone.',
    likes: 0,
    retweets: 0,
    tags: ['android', 'gmail']
  },
  {
    id: '16627136978751488',
    created: 1292799193000,
    type: 'post',
    text: 'Still struggling getting #Seam Solder working on #GlassFish. We need a release out now, so we\'ll get to the bottom of it for Beta2.',
    likes: 0,
    retweets: 0,
    tags: ['seam', 'glassfish']
  },
  {
    id: '16626666868576256',
    created: 1292799080000,
    type: 'post',
    text: 'Rolling out new logos for #Seam modules. See examples: <a href="http://sfwk.org/Seam3/MailModule" rel="noopener noreferrer" target="_blank">sfwk.org/Seam3/MailModule</a> <a href="http://sfwk.org/Seam3/CatchModule" rel="noopener noreferrer" target="_blank">sfwk.org/Seam3/CatchModule</a>',
    likes: 1,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '16626213015523328',
    created: 1292798972000,
    type: 'post',
    text: '#Seam Solder is just about ready for its first release. All tests pass. Just need to get the announcement together now. #progress',
    likes: 0,
    retweets: 1,
    tags: ['seam', 'progress']
  },
  {
    id: '15578262717603840',
    created: 1292549121000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> @brianleathem It\'s a bit different. AFAIK, it\'s active as long as you stay on the same view, even a different instance.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/15296616202240000" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">15296616202240000</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '15577951437328384',
    created: 1292549047000,
    type: 'post',
    text: 'Created a new tooling info page on the #Seam project site: <a href="http://sfwk.org/Seam3/Tooling" rel="noopener noreferrer" target="_blank">sfwk.org/Seam3/Tooling</a>',
    likes: 0,
    retweets: 1,
    tags: ['seam']
  },
  {
    id: '15483826809606144',
    created: 1292526606000,
    type: 'post',
    text: 'Spent last 48 hrs shopping for the most important gifts, my wife\'s. You know they\'re good when you can\'t wait to give them. theme: #envy',
    likes: 0,
    retweets: 0,
    tags: ['envy']
  },
  {
    id: '14921867907309568',
    created: 1292392625000,
    type: 'post',
    text: 'I just cleaned out my trash folder. 5.7G. Imagine what it would have looked like if it were real trash in my backyard. There\'s my view!',
    likes: 0,
    retweets: 0
  },
  {
    id: '14832856270381057',
    created: 1292371403000,
    type: 'post',
    text: 'The trouble is, with all archetypes, what\'s perfect for one is perfectly wrong for another. *sigh* Hence why we are going #SeamForge :)',
    likes: 0,
    retweets: 1,
    tags: ['seamforge']
  },
  {
    id: '14832556503470080',
    created: 1292371331000,
    type: 'post',
    text: 'I know this sounds crazy coming from me, but does anyone have a #Seam 2 #Maven archetype they\'ve used recently? (and is confident it works).',
    likes: 0,
    retweets: 0,
    tags: ['seam', 'maven']
  },
  {
    id: '14798228843143168',
    created: 1292363147000,
    type: 'post',
    text: 'Too many tweets, not enough blogs? Keep a Pascal quote in mind: "I wrote you a long letter because I didn’t have time to make it shorter."',
    likes: 1,
    retweets: 0
  },
  {
    id: '14719861146001408',
    created: 1292344463000,
    type: 'reply',
    text: '@brianleathem That\'s Mr. #Seam to you :) hahaha. Yes.<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '14718919331815424',
    created: 1292344238000,
    type: 'post',
    text: 'For those curious, yes, I\'m "staying put", as Pete puts it. In fact, we\'re moving rapidly on #Seam 3. Speaking of which, back to work...',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '14716238907314176',
    created: 1292343599000,
    type: 'post',
    text: 'Shane Bryzak (<a class="mention" href="https://x.com/SBryzak" rel="noopener noreferrer" target="_blank">@SBryzak</a>) is now lead of #Seam 3. Pete Muir (<a class="mention" href="https://x.com/plmuir" rel="noopener noreferrer" target="_blank">@plmuir</a>) returns to his first passion by joining #Infinispan. Congrats guys!',
    likes: 0,
    retweets: 1,
    tags: ['seam', 'infinispan']
  },
  {
    id: '14715006293643264',
    created: 1292343305000,
    type: 'post',
    text: 'I\'m not normally a fan of Gordon Biersch beer, but had their WinterBock last night...and it\'s stellar.',
    likes: 0,
    retweets: 0
  },
  {
    id: '14554399363825665',
    created: 1292305013000,
    type: 'post',
    text: 'I just saw src/main/assembly &amp; thought "someone actually wrote code in assembly?" In fact, it\'s the Maven assembly descriptor :)',
    likes: 0,
    retweets: 0
  },
  {
    id: '14552791607414785',
    created: 1292304630000,
    type: 'reply',
    text: '@brianleathem @chuggid That\'s one of the key reasons you have #Arquillian running tests cross-container. You catch \'em red handed :)<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '14552385296793600',
    created: 1292304533000,
    type: 'post',
    text: 'The #Ravens did everything they could to throw that game, except actually lose it. In true #Ravens style, they signed out w/ an INT TD.',
    likes: 0,
    retweets: 0,
    tags: ['ravens', 'ravens']
  },
  {
    id: '14550290300669952',
    created: 1292304034000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brockm" rel="noopener noreferrer" target="_blank">@brockm</a> Depends on the container, but I doubt the Weld EE container is since it uses internal APIs. Fix intended for Arquillian beta.<br><br>In reply to: <a href="https://x.com/brockm/status/14445098913763328" rel="noopener noreferrer" target="_blank">@brockm</a> <span class="status">14445098913763328</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14549573531869184',
    created: 1292303863000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> I believe the statement is in reference to the interim when Weld 1.1.0.Final is released, but before AS 6.0.0.Final.<br><br>In reply to: <a href="https://x.com/JohnAment/status/14483219785191424" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">14483219785191424</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14442232387993600',
    created: 1292278271000,
    type: 'reply',
    text: '@chuggid <a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Okay, in the case it makes sense.<br><br>In reply to: @chuggid <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14442099168509954',
    created: 1292278239000,
    type: 'post',
    text: 'The foundation of a portable injector API for Java EE 7 <a href="http://community.jboss.org/wiki/DesignofJBossInjection" rel="noopener noreferrer" target="_blank">community.jboss.org/wiki/DesignofJBossInjection</a> #javaee #inject',
    likes: 0,
    retweets: 2,
    tags: ['javaee', 'inject']
  },
  {
    id: '14439341799514112',
    created: 1292277582000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> Can you tweet the link to the injector API for AS?<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/14437859427622913" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">14437859427622913</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14438979206127616',
    created: 1292277495000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brockm" rel="noopener noreferrer" target="_blank">@brockm</a> Thanks for looking into it. If you find the issue, be sure to JIRA it ASAP. We need 1.1.0 to be a rock.<br><br>In reply to: <a href="https://x.com/brockm/status/14438224105574401" rel="noopener noreferrer" target="_blank">@brockm</a> <span class="status">14438224105574401</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14438565765185536',
    created: 1292277396000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Hahaha. Yes, we also need an injector API in EE 7 :) Let\'s get it done.<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/14437859427622913" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">14437859427622913</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14438013396320256',
    created: 1292277265000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Most excellent. And we should not let the failure of JSR-88 discourage us. This is a new ballgame :)<br><br>In reply to: <a href="https://x.com/jasondlee/status/14437403930398720" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">14437403930398720</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14437562353451009',
    created: 1292277157000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> @chuggid Yep, I\'ve been burned by assert one too many times. More effective to use an exception.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/14437158043521024" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">14437158043521024</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14437252612493312',
    created: 1292277083000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> I think we also need a spec for the container adapter API in #Arquillian. Goes hand-in-hand, though independent.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/14434991182188544" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">14434991182188544</span>',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '14436989067591680',
    created: 1292277021000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> @brianleathem Exactly! And it\'s not just the implementation. It\'s a common language for archive reading.<br><br>In reply to: <a href="https://x.com/jasondlee/status/14436493590269952" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">14436493590269952</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14436664306835456',
    created: 1292276943000,
    type: 'reply',
    text: '@brianleathem <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> #ShrinkWrap is an ideal candidate for a standard because it\'s essential that all containers vendors adopt it.<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 1,
    tags: ['shrinkwrap']
  },
  {
    id: '14436021957562369',
    created: 1292276790000,
    type: 'post',
    text: 'Yes!!! #Weld 1.1.0.CR1 is out! 40% better performance &amp; 20x faster startup! Also Jetty 8 support in Weld Servlet! <a href="https://in.relation.to/Bloggers/Weld110CR1AndCDITCK102" rel="noopener noreferrer" target="_blank">in.relation.to/Bloggers/Weld110CR1AndCDITCK102</a>',
    likes: 0,
    retweets: 6,
    tags: ['weld']
  },
  {
    id: '14413317384708096',
    created: 1292271377000,
    type: 'post',
    text: 'Oh look! Snow.',
    likes: 0,
    retweets: 0
  },
  {
    id: '14413067894923267',
    created: 1292271317000,
    type: 'post',
    text: 'Good news: Got a sweet new golf bag. Bad news: It\'s 0C. Can we skip winter this year? Oh wait, got a sweet snowboard too. Bring the snow!',
    likes: 0,
    retweets: 0
  },
  {
    id: '14411383332413440',
    created: 1292270916000,
    type: 'post',
    text: 'I agree! RT <a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I\'d like #github to have better java package traversal. Treat the package as one, not many folders.',
    likes: 0,
    retweets: 0,
    tags: ['github']
  },
  {
    id: '14398225498767360',
    created: 1292267779000,
    type: 'post',
    text: 'It takes a concerted effort to locate the source code repo for #Mojarra atm. Lots of broken links scattered around. I finally bookmarked it.',
    likes: 0,
    retweets: 0,
    tags: ['mojarra']
  },
  {
    id: '14139259606798336',
    created: 1292206036000,
    type: 'post',
    text: 'Awesome. Just got notified of a fix for cases when Mojarra logs an exception at level severe &amp; rethrows: <a href="http://java.net/jira/browse/JAVASERVERFACES-1891" rel="noopener noreferrer" target="_blank">java.net/jira/browse/JAVASERVERFACES-1891</a> #jsf',
    likes: 0,
    retweets: 1,
    tags: ['jsf']
  },
  {
    id: '14091622534877184',
    created: 1292194679000,
    type: 'post',
    text: '#JUDCon 2011 returns to #Boston immediately before #JBossWorld <a href="http://community.jboss.org/en/judcon/blog/2010/12/12/judcon-2011" rel="noopener noreferrer" target="_blank">community.jboss.org/en/judcon/blog/2010/12/12/judcon-2011</a>',
    likes: 0,
    retweets: 1,
    tags: ['judcon', 'boston', 'jbossworld']
  },
  {
    id: '14089028257189888',
    created: 1292194060000,
    type: 'post',
    text: 'Alright!!! Google Calendar now has a TimeZone selection for an event! Finally! Thanks for listening #Google!',
    likes: 0,
    retweets: 1,
    tags: ['google']
  },
  {
    id: '13451051726278657',
    created: 1292041955000,
    type: 'post',
    text: 'I would LOVE to see #github display JavaDoc just like it displays README files. (I\'m OK w/ clicking a button to render, or toggle on/off).',
    likes: 0,
    retweets: 1,
    tags: ['github']
  },
  {
    id: '13450691397816320',
    created: 1292041869000,
    type: 'post',
    text: 'The git wiki page system (gollum) looks reeeeally nice. I\'m totally tempted to suggest adopting it for #Seam module pages.',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '13388552914083841',
    created: 1292027054000,
    type: 'post',
    text: 'We discovered those #jsf issues while playing with exception handling with #seamcatch.',
    likes: 0,
    retweets: 0,
    tags: ['jsf', 'seamcatch']
  },
  {
    id: '13388466117148672',
    created: 1292027033000,
    type: 'post',
    text: 'In one case #Mojarra still swallows exceptions: <a href="http://java.net/jira/browse/JAVASERVERFACES-1892" rel="noopener noreferrer" target="_blank">java.net/jira/browse/JAVASERVERFACES-1892</a> In another it logs too verbosely: <a href="http://java.net/jira/browse/JAVASERVERFACES-1891" rel="noopener noreferrer" target="_blank">java.net/jira/browse/JAVASERVERFACES-1891</a> #jsf',
    likes: 1,
    retweets: 2,
    tags: ['mojarra', 'jsf']
  },
  {
    id: '13388217306841089',
    created: 1292026974000,
    type: 'post',
    text: 'Oops, let\'s try that again. Both links go to the same place.',
    likes: 0,
    retweets: 0
  },
  {
    id: '13387761876729856',
    created: 1292026865000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/codylerum" rel="noopener noreferrer" target="_blank">@codylerum</a> Yeah, seriously. I had to resort to goo.gl just to tweet it :)<br><br>In reply to: <a href="https://x.com/codylerum/status/13387081132802048" rel="noopener noreferrer" target="_blank">@codylerum</a> <span class="status">13387081132802048</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13385749160271874',
    created: 1292026385000,
    type: 'post',
    text: 'In one case #Mojarra still swallows exceptions: <a href="http://java.net/jira/browse/JAVASERVERFACES-1892" rel="noopener noreferrer" target="_blank">java.net/jira/browse/JAVASERVERFACES-1892</a> In another it logs too verbosely: <a href="http://java.net/jira/browse/JAVASERVERFACES-1892" rel="noopener noreferrer" target="_blank">java.net/jira/browse/JAVASERVERFACES-1892</a> #jsf',
    likes: 0,
    retweets: 0,
    tags: ['mojarra', 'jsf']
  },
  {
    id: '12922196292800512',
    created: 1291915866000,
    type: 'post',
    text: '<a href="http://issues.jboss.org" rel="noopener noreferrer" target="_blank">issues.jboss.org</a> is now live! Links to <a href="http://jira.jboss.org" rel="noopener noreferrer" target="_blank">jira.jboss.org</a> redirect appropriately. This gives the location better semantics.',
    likes: 0,
    retweets: 1
  },
  {
    id: '12652787494031361',
    created: 1291851634000,
    type: 'post',
    text: 'I don\'t understand why phones can\'t be purchased at a pro-rated price when renewing a contract early. It\'s like I have to serve jail time.',
    likes: 0,
    retweets: 0
  },
  {
    id: '12591559144898560',
    created: 1291837036000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Ah, but that\'s the beauty of it! Imagine if IDE didn\'t parse your Java files. It would be wreckage #SeamForge = smart<br><br>In reply to: <a href="https://x.com/kito99/status/12564773912711169" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">12564773912711169</span>',
    likes: 0,
    retweets: 0,
    tags: ['seamforge']
  },
  {
    id: '12584562152443905',
    created: 1291835368000,
    type: 'post',
    text: 'Wonder who contributes most to #Linux kernel? 20% unaffiliated. #RedHat at top of company list: 12.4% since 2.6.12, 12.0% since 2.6.30.',
    likes: 0,
    retweets: 1,
    tags: ['linux', 'redhat']
  },
  {
    id: '12397396684181504',
    created: 1291790744000,
    type: 'post',
    text: '#Seam Servlet 3.0.0.Alpha2 is out the door, featuring the initial integration with #seamcatch (exception handling). <a href="https://developer.jboss.org/people/dan.j.allen/blog/2010/12/08/seam-servlet-300alpha2-released-take-control-of-the-request" rel="noopener noreferrer" target="_blank">developer.jboss.org/people/dan.j.allen/blog/2010/12/08/seam-servlet-300alpha2-released-take-control-of-the-request</a>',
    likes: 0,
    retweets: 1,
    tags: ['seam', 'seamcatch']
  },
  {
    id: '12251892260274176',
    created: 1291756053000,
    type: 'reply',
    text: '@brianleathem Feel free to send me an e-mail w/ the patch. I don\'t have the archetypes in git yet, which would have made this simpler :)<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12250529426046976',
    created: 1291755728000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Stephan007" rel="noopener noreferrer" target="_blank">@Stephan007</a> <a class="mention" href="https://x.com/brianm" rel="noopener noreferrer" target="_blank">@brianm</a> That\'s exactly the trap we wanted to avoid by putting #JSR-299 &amp; #JSR-303 TCK under the Apache License. Hack away!<br><br>In reply to: <a href="https://x.com/Stephan007/status/12226294615908352" rel="noopener noreferrer" target="_blank">@Stephan007</a> <span class="status">12226294615908352</span>',
    likes: 0,
    retweets: 0,
    tags: ['jsr', 'jsr']
  },
  {
    id: '12198441400668160',
    created: 1291743309000,
    type: 'reply',
    text: '@brianleathem That reminds me I need to publish the new #Weld archetypes w/ bug fixes.<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['weld']
  },
  {
    id: '12197627122679808',
    created: 1291743115000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> You can also disable Bean Validation in JSF globally, per form, per input, or set the validation groups.<br><br>In reply to: <a href="https://x.com/JohnAment/status/12094598411190272" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">12094598411190272</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12197280799006721',
    created: 1291743033000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> Sounds like your problem is with JSF, when it decides to validate vs not. Same would happen if you had explicit validation tag.<br><br>In reply to: <a href="https://x.com/JohnAment/status/12094598411190272" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">12094598411190272</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12195686258835457',
    created: 1291742652000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> I\'d be interested in that too. Docbook resources in general are disappointing.<br><br>In reply to: <a href="https://x.com/jasondlee/status/12175942021545986" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">12175942021545986</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12194735892467712',
    created: 1291742426000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/struberg" rel="noopener noreferrer" target="_blank">@struberg</a> <a class="mention" href="https://x.com/michaelschuetz" rel="noopener noreferrer" target="_blank">@michaelschuetz</a> sure thing. Let\'s plan for early 2011 when we should have most modules in beta.<br><br>In reply to: <a href="https://x.com/struberg/status/11902677415890944" rel="noopener noreferrer" target="_blank">@struberg</a> <span class="status">11902677415890944</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11899435315363840',
    created: 1291672021000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ps4os" rel="noopener noreferrer" target="_blank">@ps4os</a> <a class="mention" href="https://x.com/michaelschuetz" rel="noopener noreferrer" target="_blank">@michaelschuetz</a> I plan on contacting Gerhard via e-mail. I don\'t have time for a debate today :) (plus I can\'t read German).<br><br>In reply to: <a href="https://x.com/ps4os/status/11893105003339776" rel="noopener noreferrer" target="_blank">@ps4os</a> <span class="status">11893105003339776</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11812231054368768',
    created: 1291651230000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/asermej" rel="noopener noreferrer" target="_blank">@asermej</a> <a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Yep, we really pushed for it hard. Not that we were the only ones to realize it. All we did was echo the demand ;)<br><br>In reply to: <a href="https://x.com/asermej/status/11807518858674176" rel="noopener noreferrer" target="_blank">@asermej</a> <span class="status">11807518858674176</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11799428822212608',
    created: 1291648177000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/michaelschuetz" rel="noopener noreferrer" target="_blank">@michaelschuetz</a> I don\'t mean to sound defensive. We\'ve still got some work to do. This month we\'ll be rolling out many more features. #seam<br><br>In reply to: <a href="https://x.com/michaelschuetz/status/11723077444444160" rel="noopener noreferrer" target="_blank">@michaelschuetz</a> <span class="status">11723077444444160</span>',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '11799004325089280',
    created: 1291648076000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/michaelschuetz" rel="noopener noreferrer" target="_blank">@michaelschuetz</a> A comparison between #CODI &amp; #Seam Faces isn\'t entirely fair. Some missing features are in other #Seam modules.<br><br>In reply to: <a href="https://x.com/michaelschuetz/status/11723077444444160" rel="noopener noreferrer" target="_blank">@michaelschuetz</a> <span class="status">11723077444444160</span>',
    likes: 0,
    retweets: 0,
    tags: ['codi', 'seam', 'seam']
  },
  {
    id: '11548108018155520',
    created: 1291588258000,
    type: 'post',
    text: 'I\'m thinking of choosing the mytouch 4G over the G2. Front camera over slide-out keyboard. Going to have to embrace slide typing.',
    likes: 0,
    retweets: 0
  },
  {
    id: '11546853015298048',
    created: 1291587958000,
    type: 'post',
    text: 'My wife decided house needed to be rearranged. Couch now in kitchen, arm chairs in guest bedroom, desk in living room. Nonconformity FTW.',
    likes: 0,
    retweets: 0
  },
  {
    id: '11545259557265408',
    created: 1291587579000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/andybosch" rel="noopener noreferrer" target="_blank">@andybosch</a> You are teasing us w/ no link ;) Look forward to seeing it (if it\'s english).<br><br>In reply to: <a href="https://x.com/andybosch/status/11502283908972544" rel="noopener noreferrer" target="_blank">@andybosch</a> <span class="status">11502283908972544</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11544361611304960',
    created: 1291587364000,
    type: 'post',
    text: 'Watched the Brewmasters episode last night about Dogfish Head concocting Bitches Brew. I love that brewery. A true pride of America.',
    likes: 0,
    retweets: 0
  },
  {
    id: '11543859662159872',
    created: 1291587245000,
    type: 'post',
    text: 'I\'ve really felt crappy all weekend. The sickness this year really wipes you out. I should be good by tommorrow. Got plenty of rest.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10905980879507457',
    created: 1291435163000,
    type: 'post',
    text: '#Seam mail is finally imported into the Seam git repo &amp; module page is setup. Next, up, alpha. <a href="http://sfwk.org/Seam3/MailModule" rel="noopener noreferrer" target="_blank">sfwk.org/Seam3/MailModule</a>',
    likes: 0,
    retweets: 3,
    tags: ['seam']
  },
  {
    id: '10789527249485824',
    created: 1291407398000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> The Galaxy tablet looked plenty big for me. It was smaller than I had thought it was, but I realized the size is quite perfect.<br><br>In reply to: <a href="https://x.com/rruss/status/10760376488566784" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">10760376488566784</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10759574936092672',
    created: 1291400257000,
    type: 'post',
    text: 'I just played around with the galaxy tablet. Wow that is seriously nice.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10746599902806018',
    created: 1291397163000,
    type: 'post',
    text: 'I\'m playing around with the SlideIT keyboard to see if its good enough for me to give up a device keyboard. It\'s pretty amazing.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10450498586939392',
    created: 1291326567000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cbredesen" rel="noopener noreferrer" target="_blank">@cbredesen</a> Maybe the logo for #weld should have been a needle ;)<br><br>In reply to: <a href="https://x.com/cbredesen/status/10449946952073216" rel="noopener noreferrer" target="_blank">@cbredesen</a> <span class="status">10449946952073216</span>',
    likes: 0,
    retweets: 0,
    tags: ['weld']
  },
  {
    id: '10449971950125056',
    created: 1291326442000,
    type: 'post',
    text: 'Focus on #seam has kept me away from #arquillian. Tough having to juggle 2 awesome projects. I think I need to write some tests (wink).',
    likes: 0,
    retweets: 0,
    tags: ['seam', 'arquillian']
  },
  {
    id: '10448823285448705',
    created: 1291326168000,
    type: 'post',
    text: '#cdi is going to make us all sound like a bunch of drug addicts (programmer humor ;))',
    likes: 0,
    retweets: 0,
    tags: ['cdi']
  },
  {
    id: '10420789274738688',
    created: 1291319484000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I want to order some golf balls with Ike on them :) #arquillian<br><br>In reply to: <a href="https://x.com/lightguardjp/status/9847536177119232" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">9847536177119232</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '10394683662401536',
    created: 1291313260000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/asermej" rel="noopener noreferrer" target="_blank">@asermej</a> The application framework is not currently slated for the initial #Seam 3 bundle. It\'s currently on the whiteboard for redesign.<br><br>In reply to: <a href="https://x.com/asermej/status/10328427785617409" rel="noopener noreferrer" target="_blank">@asermej</a> <span class="status">10328427785617409</span>',
    likes: 0,
    retweets: 1,
    tags: ['seam']
  },
  {
    id: '10394171865038848',
    created: 1291313138000,
    type: 'post',
    text: '#Seam Servlet release announcement: <a href="http://community.jboss.org/people/dan.j.allen/blog/2010/12/02/seam-servlet-alpha-1-brings-servlet-lifecycle-events-to-cdi" rel="noopener noreferrer" target="_blank">community.jboss.org/people/dan.j.allen/blog/2010/12/02/seam-servlet-alpha-1-brings-servlet-lifecycle-events-to-cdi</a>',
    likes: 0,
    retweets: 5,
    tags: ['seam']
  },
  {
    id: '10132465284816898',
    created: 1291250742000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/stuartwdouglas" rel="noopener noreferrer" target="_blank">@stuartwdouglas</a> <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> I decided on the i5 rather than the i7. A good balance of battery life &amp; performance. I can exchange if not.<br><br>In reply to: <a href="https://x.com/stuartwdouglas/status/10109018785714176" rel="noopener noreferrer" target="_blank">@stuartwdouglas</a> <span class="status">10109018785714176</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10132124325650432',
    created: 1291250661000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> <a class="mention" href="https://x.com/stuartwdouglas" rel="noopener noreferrer" target="_blank">@stuartwdouglas</a> <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> I just bought the damn thing. I\'ll deal with expenses later :)<br><br>In reply to: <a href="https://x.com/JohnAment/status/10124727150452736" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">10124727150452736</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10102111912075264',
    created: 1291243505000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/stuartwdouglas" rel="noopener noreferrer" target="_blank">@stuartwdouglas</a> That\'s the conclusion I finally came to. I\'m sick of moving in slow motion. Also, I\'m going to parallelize w/ 3 computers.<br><br>In reply to: <a href="https://x.com/stuartwdouglas/status/10101050346307584" rel="noopener noreferrer" target="_blank">@stuartwdouglas</a> <span class="status">10101050346307584</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10099386805649408',
    created: 1291242856000,
    type: 'post',
    text: 'I bought a new laptop on Monday, the HP Envy beats edition. It\'s more accurate to say I bought 8GB of RAM encased in a laptop shell ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '10099057179500544',
    created: 1291242777000,
    type: 'post',
    text: 'It feels like xmas today. UPS showered me w/ delivers from my Cyber Monday shopping spree ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '10098672150773761',
    created: 1291242685000,
    type: 'post',
    text: '#Seam Servlet is also the first module to be released under the Apache License, Version 2.0. It\'s a foreshadow of a broader announcement ;)',
    likes: 0,
    retweets: 2,
    tags: ['seam']
  },
  {
    id: '10098182595805184',
    created: 1291242569000,
    type: 'post',
    text: 'With #Seam Servlet, never write a Servlet listener again. Just observe the event using a #CDI observer method. So much more concise.',
    likes: 0,
    retweets: 3,
    tags: ['seam', 'cdi']
  },
  {
    id: '10097750121127936',
    created: 1291242465000,
    type: 'reply',
    text: '@mwessendorf I am also simulating a response initialized/destroyed event using an outer filter, something Servlet does not provide.<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10080327066193920',
    created: 1291238311000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/domdorn" rel="noopener noreferrer" target="_blank">@domdorn</a> brilliant :)<br><br>In reply to: <a href="https://x.com/domdorn/status/10004685071060993" rel="noopener noreferrer" target="_blank">@domdorn</a> <span class="status">10004685071060993</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10079806662115328',
    created: 1291238187000,
    type: 'post',
    text: 'The big feature in #Seam Servlet is the Servlet lifecycle to CDI event bridge. There are also producers for all the stuff you want ;)',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '10079633986822145',
    created: 1291238146000,
    type: 'post',
    text: 'I pushed out a #Seam Servlet Alpha 1 release. I still need to get the announcement written up. Fighting w/ Maven took all my energy :)',
    likes: 0,
    retweets: 1,
    tags: ['seam']
  },
  {
    id: '9361884595617792',
    created: 1291067021000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> Absolutely. Reuse is good, but forcing it is not. The important thing is to deliver a good API. Do what you must.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/9352086504345600" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">9352086504345600</span>',
    likes: 0,
    retweets: 1
  },
  {
    id: '9333345024479232',
    created: 1291060217000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> You\'ve been a great champion of openness at a time when Java EE really needed it. I pray your new gig is also public facing.<br><br>In reply to: @pelegri <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9311497222094848',
    created: 1291055008000,
    type: 'post',
    text: 'The Seam 3 module pages have new logos, which originated from the Seam State of the Union presentation. <a href="http://seamframework.org/Seam3" rel="noopener noreferrer" target="_blank">seamframework.org/Seam3</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9309767726006272',
    created: 1291054596000,
    type: 'post',
    text: 'Just registered my Panera card. Looking forward to racking up points. Wish I could make it retroactive. I practically invest in that place.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9105172592594944',
    created: 1291005816000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> We must have picked up something at #devoxx \'cause I\'ve been feeling sick too. Not sure Sarah was expecting that present :)<br><br>In reply to: <a href="https://x.com/lincolnthree/status/9081779449237504" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">9081779449237504</span>',
    likes: 0,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '9104866320322561',
    created: 1291005743000,
    type: 'post',
    text: 'My Swingers Soundtrack CD finally arrived this weekend. It\'s money, baby! I really had to hunt to find it since it\'s out of circulation.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9103951169323008',
    created: 1291005525000,
    type: 'post',
    text: 'Had a pretty lazy weekend, partly just to relax, partly recovering from a virus my wife &amp; I keep handing off to one other. Sick of coughing.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9100876597698560',
    created: 1291004792000,
    type: 'post',
    text: 'Catch is the exception handling utility in #Seam 3. We call it "Next generation exception handling" <a href="http://sfwk.org/Seam3/CatchModule" rel="noopener noreferrer" target="_blank">sfwk.org/Seam3/CatchModule</a>',
    likes: 0,
    retweets: 2,
    tags: ['seam']
  },
  {
    id: '8270970636734464',
    created: 1290806927000,
    type: 'post',
    text: 'All the info about #JAXLondon 2011 is up now! <a href="http://jaxlondon.com" rel="noopener noreferrer" target="_blank">jaxlondon.com</a>',
    likes: 0,
    retweets: 2,
    tags: ['jaxlondon']
  },
  {
    id: '7987047302172672',
    created: 1290739235000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/the_jamezp" rel="noopener noreferrer" target="_blank">@the_jamezp</a> I haven\'t gotten the G2 yet. That\'s a "Merry xmas to myself gift" this year. It does have tethering like my wife\'s myTouch.<br><br>In reply to: <a href="https://x.com/the_jamezp/status/7835599792766976" rel="noopener noreferrer" target="_blank">@the_jamezp</a> <span class="status">7835599792766976</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7854922565427200',
    created: 1290707734000,
    type: 'post',
    text: 'Found some money on the ground. I should be all set now.',
    photos: ['<div class="entry"><img class="photo" src="media/7854922565427200.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '7834052669214720',
    created: 1290702758000,
    type: 'post',
    text: 'Okay, time to go find some turkey &amp; visit family...somewhere in there is some manual kitchen labor too. Happy Thanksgiving!',
    likes: 0,
    retweets: 0
  },
  {
    id: '7833754001215489',
    created: 1290702687000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> And not having bugs is good for my sanity :)<br><br>In reply to: @pelegri <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7833649844060161',
    created: 1290702662000,
    type: 'post',
    text: 'I was psyched to discover that RESTEasy is finally JAX-RS compliant (w/ regard to configuration) in JBoss AS 6 RC1. Finally!',
    likes: 1,
    retweets: 1
  },
  {
    id: '7825195171577856',
    created: 1290700646000,
    type: 'post',
    text: 'I was psyched to discover that RESTEasy is finally JAX-RS compliant (w/ regard to configuration) in JBoss AS 6 RC1. Finally!',
    likes: 0,
    retweets: 1
  },
  {
    id: '7823455978258433',
    created: 1290700231000,
    type: 'post',
    text: 'I made the #CDI extensions bookmark group a lot simpler to remember: <a href="http://groups.diigo.com/group/cdi-extensions" rel="noopener noreferrer" target="_blank">groups.diigo.com/group/cdi-extensions</a>. Publicize your work!',
    likes: 0,
    retweets: 0,
    tags: ['cdi']
  },
  {
    id: '7811240973828096',
    created: 1290697319000,
    type: 'post',
    text: 'I\'m playing a little morning internet hooky before the Thanksgiving festivities begin. Gotta front-load before my turkey-day nap :)',
    likes: 0,
    retweets: 0
  },
  {
    id: '7810882599911425',
    created: 1290697234000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SKohler" rel="noopener noreferrer" target="_blank">@SKohler</a> <a class="mention" href="https://x.com/Dorpsidioot" rel="noopener noreferrer" target="_blank">@Dorpsidioot</a> Keepin\' it real :) Gotta show my American redneck now &amp; again :)<br><br>In reply to: @skohler <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7810624763469825',
    created: 1290697172000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> Hahaha. Yeah, I know. It was a Thanksgiving gift from my mom, so she got close. I made her laugh w/ the Ball jar :)<br><br>In reply to: <a href="https://x.com/metacosm/status/7788808753909760" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">7788808753909760</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7810130125000705',
    created: 1290697054000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Actually, that picture was Gimp\'ed :) But we really did bike the Golden Gate together. Great day thanks to <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a>.<br><br>In reply to: <a href="https://x.com/maxandersen/status/7702529638928384" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">7702529638928384</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7809636069548032',
    created: 1290696936000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iapazmino" rel="noopener noreferrer" target="_blank">@iapazmino</a> Cool! Great use of #Arquillian. Another great example of how you can use it to test previously untestable stuff :)<br><br>In reply to: <a href="https://x.com/iapazmino/status/7638027492917248" rel="noopener noreferrer" target="_blank">@iapazmino</a> <span class="status">7638027492917248</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '7809469165600769',
    created: 1290696897000,
    type: 'post',
    text: 'Just got the word that statistics are available for oss.sonatype.org in the #Nexus UI for artifact publishers.',
    likes: 0,
    retweets: 0,
    tags: ['nexus']
  },
  {
    id: '7808587069915136',
    created: 1290696686000,
    type: 'post',
    text: 'The best part about working with #CDI &amp; #Arquillian is checking out the the cool stuff developers do to extend it. Innovate away!',
    likes: 0,
    retweets: 0,
    tags: ['cdi', 'arquillian']
  },
  {
    id: '7806849160060928',
    created: 1290696272000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/domdorn" rel="noopener noreferrer" target="_blank">@domdorn</a> Great job! I can\'t wait to see it. Have you pushed it to github yet? I can help once back from Thanksgiving.<br><br>In reply to: <a href="https://x.com/domdorn/status/7155235654995968" rel="noopener noreferrer" target="_blank">@domdorn</a> <span class="status">7155235654995968</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7805400447459329',
    created: 1290695927000,
    type: 'post',
    text: 'I\'m tethering w/ my wife\'s mytouch #android phone. A dream has become a reality. Have #android, will travel :)',
    likes: 0,
    retweets: 0,
    tags: ['android', 'android']
  },
  {
    id: '7653857186287617',
    created: 1290659796000,
    type: 'post',
    text: 'And this is how we drink Belgian beer in the U.S.',
    photos: ['<div class="entry"><img class="photo" src="media/7653857186287617.jpg"></div>'],
    likes: 0,
    retweets: 2
  },
  {
    id: '7525504437460992',
    created: 1290629194000,
    type: 'reply',
    text: '@chuggid <a class="mention" href="https://x.com/myfear" rel="noopener noreferrer" target="_blank">@myfear</a> We are just caught in the middle of an integration construction area.<br><br>In reply to: @chuggid <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7525154057879553',
    created: 1290629111000,
    type: 'reply',
    text: '@chuggid <a class="mention" href="https://x.com/myfear" rel="noopener noreferrer" target="_blank">@myfear</a> I did some testing today with #Arquillian running tests on #GlassFish 3.1-b30. It\'s not ready for #Arquillian to use it.<br><br>In reply to: @chuggid <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'glassfish', 'arquillian']
  },
  {
    id: '7510165809004544',
    created: 1290625537000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Yep, the CDI injections still work, appears to be a problem only with resource injections.<br><br>In reply to: <a href="https://x.com/jasondlee/status/7457178868785152" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">7457178868785152</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7509937873752064',
    created: 1290625483000,
    type: 'post',
    text: 'Want tx managed beans (not EJBs)? Take a look at #Seam 3 persistence (<a href="http://sfwk.org/Seam3" rel="noopener noreferrer" target="_blank">sfwk.org/Seam3</a>), MyFaces CODI or SoftwareMill Commons.',
    likes: 0,
    retweets: 1,
    tags: ['seam']
  },
  {
    id: '7207645182169089',
    created: 1290553411000,
    type: 'reply',
    text: 'Thanks! RT <a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> <a class="mention" href="https://x.com/richsharples" rel="noopener noreferrer" target="_blank">@richsharples</a> Welcome to the JavaEE 6 party; you guys (JBoss) were instrumental in making it happen!',
    likes: 0,
    retweets: 0
  },
  {
    id: '7177009033646080',
    created: 1290546106000,
    type: 'post',
    text: 'Some great options for HTML5 preso tools. Thanks for the feedback! Can\'t wait to try. Just being able to add code easily would be a huge +.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7151305638281217',
    created: 1290539978000,
    type: 'post',
    text: 'OpenOffice Impress is dead to me. I\'m actively seeking an HTML5 replacement or, if I have to, keynote.',
    likes: 0,
    retweets: 2
  },
  {
    id: '7150546276327424',
    created: 1290539797000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/datafront" rel="noopener noreferrer" target="_blank">@datafront</a> Laurel by Russett :) If you want to meetup for lunch at Panera one day, just drop me an e-mail. December is looking pretty open.<br><br>In reply to: <a href="https://x.com/datafront/status/7140733811167232" rel="noopener noreferrer" target="_blank">@datafront</a> <span class="status">7140733811167232</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7075544935964673',
    created: 1290521916000,
    type: 'post',
    text: 'My local #Panera finally has a rewards card. I\'m going to rack up some serious credits now :) Free food. Woot!',
    likes: 0,
    retweets: 0,
    tags: ['panera']
  },
  {
    id: '7074325358186497',
    created: 1290521625000,
    type: 'post',
    text: 'Enjoyed a post-#devoxx hacking session w/ <a class="mention" href="https://x.com/domdorn" rel="noopener noreferrer" target="_blank">@domdorn</a> &amp; <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> near central station on Fri night. As promised, now following <a class="mention" href="https://x.com/domdorn" rel="noopener noreferrer" target="_blank">@domdorn</a> :)',
    likes: 0,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '7073229701124097',
    created: 1290521364000,
    type: 'post',
    text: 'If you\'re looking for #Seam Solder, it\'s presently known as #Weld Extensions. We\'re in the process of renaming it. Stay tuned.',
    likes: 0,
    retweets: 1,
    tags: ['seam', 'weld']
  },
  {
    id: '7072356514140160',
    created: 1290521155000,
    type: 'post',
    text: 'My next gig in Europe is #jfokus in Stockholm Sweden. Giving an #Arquillian talk w/ <a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> and detailed #CDI extensions talk.',
    likes: 0,
    retweets: 0,
    tags: ['jfokus', 'arquillian', 'cdi']
  },
  {
    id: '7072169691447296',
    created: 1290521111000,
    type: 'reply',
    text: '@JAXenterCOM Excellent. I\'m looking forward to the info about JAX London.<br><br>In reply to: <a href="https://x.com/devmio_official/status/7042805310423040" rel="noopener noreferrer" target="_blank">@devmio_official</a> <span class="status">7042805310423040</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7071711119806464',
    created: 1290521001000,
    type: 'post',
    text: 'JBoss Community Asylum is a superb resource for both community &amp; project members to keep in touch w/ what\'s going on <a href="https://in.relation.to/2010/11/22/podcast-15-jboss-application-server-7-is-looking-shockingly-good/" rel="noopener noreferrer" target="_blank">in.relation.to/2010/11/22/podcast-15-jboss-application-server-7-is-looking-shockingly-good/</a>',
    likes: 0,
    retweets: 1
  },
  {
    id: '6855808222498816',
    created: 1290469526000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/caniszczyk" rel="noopener noreferrer" target="_blank">@caniszczyk</a> I use #Eclipse almost exclusively for development. I just know that we have a multi-IDE user base :)<br><br>In reply to: <a href="https://x.com/cra/status/6839835218149376" rel="noopener noreferrer" target="_blank">@cra</a> <span class="status">6839835218149376</span>',
    likes: 0,
    retweets: 0,
    tags: ['eclipse']
  },
  {
    id: '6855604924583936',
    created: 1290469478000,
    type: 'post',
    text: 'Anyone know when #JAX London 2011 will be?',
    likes: 0,
    retweets: 0,
    tags: ['jax']
  },
  {
    id: '6839890675240960',
    created: 1290465731000,
    type: 'post',
    text: 'Looking back at #devoxx photos, realized I missed a lot of people. I wish I had felt better. See ya at the next gig! <a href="http://picasaweb.google.com/JavaPolis.com/Devoxx2010" rel="noopener noreferrer" target="_blank">picasaweb.google.com/JavaPolis.com/Devoxx2010</a>',
    likes: 0,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '6839143837466624',
    created: 1290465553000,
    type: 'post',
    text: 'Just received the IntelliJ license renewal for the Seam project. If you are a #seam contributor &amp; want to use IntelliJ, let me know.',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '6826289692413952',
    created: 1290462488000,
    type: 'post',
    text: 'Had great conversation w/ <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> about community: how to build it, sustain it, engage it. Looking forward to improving.',
    likes: 0,
    retweets: 1
  },
  {
    id: '6825709674692608',
    created: 1290462350000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> Yep, I\'m going to create reproducible steps and then figure out what fix to request. Stay tuned.<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/6822366231404544" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">6822366231404544</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6825550370840577',
    created: 1290462312000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pbakker" rel="noopener noreferrer" target="_blank">@pbakker</a> Great. Yeah, the #Arquillian lab provided great feedback for us too, helped us know what to work on. Look for good things to come.<br><br>In reply to: <a href="https://x.com/pbakker/status/6820333076094976" rel="noopener noreferrer" target="_blank">@pbakker</a> <span class="status">6820333076094976</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '6819494286598144',
    created: 1290460868000,
    type: 'post',
    text: 'Figured out why #Maven went haywire in #Arquillian lab. go-offline created _maven.repositories files, flagged artifacts as unresolved.',
    likes: 0,
    retweets: 1,
    tags: ['maven', 'arquillian']
  },
  {
    id: '6818096782249984',
    created: 1290460535000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fbricon" rel="noopener noreferrer" target="_blank">@fbricon</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> The #Weld archetypes are purely #javaee6 &amp; #CDI compliant. They\'re part of Weld because they help get started w/ CDI.<br><br>In reply to: <a href="https://x.com/fbricon/status/5936518136860672" rel="noopener noreferrer" target="_blank">@fbricon</a> <span class="status">5936518136860672</span>',
    likes: 0,
    retweets: 0,
    tags: ['weld', 'javaee6', 'cdi']
  },
  {
    id: '6432926438789121',
    created: 1290368703000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> Actually, it\'s the American music they play I find scary. I dig the Belgium DJs ;)<br><br>In reply to: <a href="https://x.com/metacosm/status/6024649217417216" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">6024649217417216</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6432389408497664',
    created: 1290368575000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/szimano" rel="noopener noreferrer" target="_blank">@szimano</a> You bet. Thanks for the good work. It\'s exciting to see what extension writers are creating!<br><br>In reply to: <a href="https://x.com/szimano/status/6044557888196608" rel="noopener noreferrer" target="_blank">@szimano</a> <span class="status">6044557888196608</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6429223388454912',
    created: 1290367820000,
    type: 'post',
    text: 'It\'s been quite an adventure, but I\'m really looking forward to seeing my wife, arriving home and finally getting some sleep!',
    likes: 0,
    retweets: 0
  },
  {
    id: '6429069457498112',
    created: 1290367784000,
    type: 'post',
    text: 'I watched <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> hack all the way home on the plane. Hard core. New pet project he\'ll unveil soon. Safe travels home lincoln!',
    likes: 0,
    retweets: 0
  },
  {
    id: '6428652006809600',
    created: 1290367684000,
    type: 'post',
    text: 'I wonder how many talks at #devoxx recommended using #Arquillian for testing. At least our uni talk, Adam Bien\'s talk and my last one.',
    likes: 0,
    retweets: 0,
    tags: ['devoxx', 'arquillian']
  },
  {
    id: '6428297617473537',
    created: 1290367600000,
    type: 'post',
    text: 'I assembled all the bits of energy I could muster &amp; put it into my last talk at #devoxx on #cdi extensions. I left it all on stage ;)',
    likes: 0,
    retweets: 0,
    tags: ['devoxx', 'cdi']
  },
  {
    id: '5984753400422402',
    created: 1290261851000,
    type: 'post',
    text: 'Love the beer in Belgium; the chocolate, heavenly; the food, amazing. The music, kill it. Horrible. Get rid of it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5983798265126912',
    created: 1290261623000,
    type: 'post',
    text: 'With <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> &amp; <a class="mention" href="https://x.com/SHOWBULL" rel="noopener noreferrer" target="_blank">@SHOWBULL</a> in Brugges.',
    photos: ['<div class="entry"><img class="photo" src="media/5983798265126912.jpg"></div>'],
    likes: 0,
    retweets: 1
  },
  {
    id: '5983173674541056',
    created: 1290261474000,
    type: 'post',
    text: 'Back at De Halve Maan brewery in Brugge with <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> &amp; <a class="mention" href="https://x.com/shobull" rel="noopener noreferrer" target="_blank">@shobull</a> (my bro).',
    photos: ['<div class="entry"><img class="photo" src="media/5983173674541056.jpg"></div>'],
    likes: 0,
    retweets: 1
  },
  {
    id: '5662976329646081',
    created: 1290185133000,
    type: 'post',
    text: 'Slides from my "The roots of Java EE 6" talk at #devoxx #cdi #jsr299 #weld #seam <a class="mention" href="https://x.com/mojavelinux" rel="noopener noreferrer" target="_blank">@mojavelinux</a> <a href="http://seamframework.org/service/File/143466" rel="noopener noreferrer" target="_blank">seamframework.org/service/File/143466</a>',
    likes: 4,
    retweets: 9,
    tags: ['devoxx', 'cdi', 'jsr299', 'weld', 'seam']
  },
  {
    id: '5379690923560960',
    created: 1290117592000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/stuartwdouglas" rel="noopener noreferrer" target="_blank">@stuartwdouglas</a> Hero.<br><br>In reply to: <a href="https://x.com/stuartwdouglas/status/4116812304220160" rel="noopener noreferrer" target="_blank">@stuartwdouglas</a> <span class="status">4116812304220160</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5379406323261440',
    created: 1290117525000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> <a class="mention" href="https://x.com/stuartwdouglas" rel="noopener noreferrer" target="_blank">@stuartwdouglas</a> #Weld 1.1.0.Beta2 (you can find the version in the pom.properties inside the embedded-glassfish-all JAR file)<br><br>In reply to: <a href="https://x.com/JohnAment/status/5354293716590592" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">5354293716590592</span>',
    likes: 0,
    retweets: 0,
    tags: ['weld']
  },
  {
    id: '5272832749608961',
    created: 1290092115000,
    type: 'post',
    text: 'This week is kicking my ass. Gotta keep it together.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4337932995395584',
    created: 1289869218000,
    type: 'post',
    text: 'A 2.5 hour presentation on #seam, 10 minutes on #seam forge and all people wanted to know more about was #forge. Great stuff <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a>!',
    likes: 0,
    retweets: 0,
    tags: ['seam', 'seam', 'forge']
  },
  {
    id: '4335907100426242',
    created: 1289868735000,
    type: 'post',
    text: '#Arquillian is our blessing &amp; our curse. Blessing because it gives us limitless testing. Curse because we uncover *every* container bug',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '4335142977933312',
    created: 1289868553000,
    type: 'post',
    text: '#seam catch got a huge spotlight at #devoxx during #seam university talk. Such a creative use of #cdi. Great stuff from <a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a>.',
    likes: 0,
    retweets: 1,
    tags: ['seam', 'devoxx', 'seam', 'cdi']
  },
  {
    id: '4334749594157056',
    created: 1289868459000,
    type: 'post',
    text: 'Awesome time with #seam &amp; #infinispan community at #kuliminator. Seriously, most quality bar ever. Note to self: next time have cash :)',
    likes: 0,
    retweets: 0,
    tags: ['seam', 'infinispan', 'kuliminator']
  },
  {
    id: '4334450334760961',
    created: 1289868388000,
    type: 'post',
    text: 'Got raked over the coals by Maven &amp; m2eclipse f\'ing up in #arquillian lab. However, beta1 isn\'t going to have these hangups. #progress',
    likes: 0,
    retweets: 1,
    tags: ['arquillian', 'progress']
  },
  {
    id: '4333277049520128',
    created: 1289868108000,
    type: 'post',
    text: 'That was an absolutely brutal day at #devoxx. All hardships erased, though, because I finally had a #Westvleteren. Jesper, you\'re my hero.',
    likes: 0,
    retweets: 0,
    tags: ['devoxx', 'westvleteren']
  },
  {
    id: '3750227148275712',
    created: 1289729098000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <a class="mention" href="https://x.com/mojavelinux" rel="noopener noreferrer" target="_blank">@mojavelinux</a> Now wheels down! Homeless until 3. Tapped wifi at trendy healthy place, one of few eateries actually open.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/3583533733912576" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">3583533733912576</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3551756696952832',
    created: 1289681779000,
    type: 'post',
    text: 'Didn\'t miss my flight to #devoxx this year! Starting off on the right foot. About to meet up w/ <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> at IAD before we fly out.',
    likes: 0,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '2824118672035840',
    created: 1289508297000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> <a class="mention" href="https://x.com/myfear" rel="noopener noreferrer" target="_blank">@myfear</a> Have you tried <a href="https://github.com/arquillian/arquillian-examples" rel="noopener noreferrer" target="_blank">github.com/arquillian/arquillian-examples</a> (jpalab)? It\'s on my list to review all examples.<br><br>In reply to: <a href="https://x.com/jasondlee/status/2796153087205376" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">2796153087205376</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2823063372566529',
    created: 1289508045000,
    type: 'post',
    text: 'Sneak peak at #Arquillian minicards making their debut at #Devoxx.',
    photos: ['<div class="entry"><img class="photo" src="media/2823063372566529.png"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'devoxx']
  },
  {
    id: '2785517208862720',
    created: 1289499093000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> Hahaha! I have totally been there before.<br><br>In reply to: <a href="https://x.com/aschwart/status/2781814858977281" rel="noopener noreferrer" target="_blank">@aschwart</a> <span class="status">2781814858977281</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2784993285775360',
    created: 1289498968000,
    type: 'post',
    text: 'The #jboss swag has arrived!',
    likes: 0,
    retweets: 0,
    tags: ['jboss']
  },
  {
    id: '2633776572137472',
    created: 1289462915000,
    type: 'post',
    text: 'Torn between working on presentation &amp; hacking. Too bad, gotta work before play.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2632589680582656',
    created: 1289462632000,
    type: 'post',
    text: 'I can\'t believe that OpenOffice Impress can\'t properly put a drop shadow on a transparent box. It\'s a wreck. Lame.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2632388492402688',
    created: 1289462584000,
    type: 'post',
    text: 'Wrestled to figure out simpler way to get highlighted code into slide. Finally nailed process: prettify -&gt; paste to wp -&gt; paste to slide.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2631856264577024',
    created: 1289462458000,
    type: 'post',
    text: 'Using slides to relay info about software is like using prose to describe a symphony. That\'s why my slides primarily have code. #devoxx',
    likes: 0,
    retweets: 1,
    tags: ['devoxx']
  },
  {
    id: '2462074860994560',
    created: 1289421979000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> That solved my layout problem too! I needed the extra row :)<br><br>In reply to: <a href="https://x.com/maxandersen/status/2460226020179968" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">2460226020179968</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2461040063287296',
    created: 1289421732000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Oops! Sorry about that...too many pages to switch between. Adding now. Sorry!<br><br>In reply to: <a href="https://x.com/maxandersen/status/2460226020179968" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">2460226020179968</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2424975470362624',
    created: 1289413133000,
    type: 'post',
    text: 'There are clocks that are 1 hour fast, some 1 hour slow, some right 2x a day. Then, there are clocks just on their own damn schedule.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2422756587077632',
    created: 1289412604000,
    type: 'post',
    text: 'Pete Muir (<a class="mention" href="https://x.com/plmuir" rel="noopener noreferrer" target="_blank">@plmuir</a>), #Weld project lead, sets the record straight over performance concerns <a href="https://in.relation.to/Bloggers/WeldsPerformance" rel="noopener noreferrer" target="_blank">in.relation.to/Bloggers/WeldsPerformance</a>',
    likes: 1,
    retweets: 2,
    tags: ['weld']
  },
  {
    id: '2421884461256704',
    created: 1289412396000,
    type: 'post',
    text: 'Planning your #Devoxx schedule? Here\'s a cheat sheet :) <a href="https://in.relation.to/Bloggers/SeamInfinispanAndArquillianSocialAtDevoxxPlusYourJBossPlanner" rel="noopener noreferrer" target="_blank">in.relation.to/Bloggers/SeamInfinispanAndArquillianSocialAtDevoxxPlusYourJBossPlanner</a>',
    likes: 1,
    retweets: 1,
    tags: ['devoxx']
  },
  {
    id: '2421502532124672',
    created: 1289412305000,
    type: 'post',
    text: '#JBoss at #Devoxx, including a #Seam, #Infinispan &amp; #Arquillian social <a href="https://in.relation.to/Bloggers/SeamInfinispanAndArquillianSocialAtDevoxxPlusYourJBossPlanner" rel="noopener noreferrer" target="_blank">in.relation.to/Bloggers/SeamInfinispanAndArquillianSocialAtDevoxxPlusYourJBossPlanner</a>',
    likes: 0,
    retweets: 4,
    tags: ['jboss', 'devoxx', 'seam', 'infinispan', 'arquillian']
  },
  {
    id: '2389814338916352',
    created: 1289404750000,
    type: 'post',
    text: 'The #Arquillian allied invasion is now. 4 talks, 4 events, 1 day. <a href="http://community.jboss.org/en/arquillian/blog/2010/10/28/were-published" rel="noopener noreferrer" target="_blank">community.jboss.org/en/arquillian/blog/2010/10/28/were-published</a> Just a warm-up to the main event, #devoxx',
    likes: 0,
    retweets: 4,
    tags: ['arquillian', 'devoxx']
  },
  {
    id: '1886160410378241',
    created: 1289284670000,
    type: 'post',
    text: 'Fantastic news! SIwpas CR5 passes the CDI TCK! Thanks to hard work &amp; an Apache-licensed test suite ;) <a href="http://tiny.cc/8w972" rel="noopener noreferrer" target="_blank">tiny.cc/8w972</a> #cdi #jsr299',
    likes: 0,
    retweets: 1,
    tags: ['cdi', 'jsr299']
  },
  {
    id: '1885116506832896',
    created: 1289284421000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexismp" rel="noopener noreferrer" target="_blank">@alexismp</a> <a class="mention" href="https://x.com/kevin_farnham" rel="noopener noreferrer" target="_blank">@kevin_farnham</a> No thanks. If I never see it again, the world will be a better place to me. Viva the new java.net!<br><br>In reply to: <a href="https://x.com/alexismp/status/1883395525181440" rel="noopener noreferrer" target="_blank">@alexismp</a> <span class="status">1883395525181440</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1787091008622592',
    created: 1289261050000,
    type: 'post',
    text: '"AS 7 is..treating administrators with the same love that we have always shown developers" Great objective! #jbossas',
    likes: 0,
    retweets: 4,
    tags: ['jbossas']
  },
  {
    id: '1773395372941312',
    created: 1289257785000,
    type: 'post',
    text: 'JIRA needs to add the feature from Gmail to update the conversation if a new comment comes in. Saves a lot of embarrassment.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1723766618984448',
    created: 1289245952000,
    type: 'post',
    text: 'This is going to be a hardware holiday for me. Both my phone and laptop are 2-year old tech. Really feeling the pain.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1538782956359680',
    created: 1289201849000,
    type: 'post',
    text: 'Just committed code for the #Seam Servlet module. Still some work to do, but learned a whole bunch playing around w/ #Weld extensions.',
    likes: 0,
    retweets: 0,
    tags: ['seam', 'weld']
  },
  {
    id: '1538604690046976',
    created: 1289201806000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jbandi" rel="noopener noreferrer" target="_blank">@jbandi</a> Stay tuned for info about a CDI/Weld book soon. The plan is that Seam in Action will become part of this new series.<br><br>In reply to: <a href="https://x.com/jbandi/status/1537074574393345" rel="noopener noreferrer" target="_blank">@jbandi</a> <span class="status">1537074574393345</span>',
    likes: 0,
    retweets: 1
  },
  {
    id: '1526426889093120',
    created: 1289198903000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jbandi" rel="noopener noreferrer" target="_blank">@jbandi</a> Like Java Persistence w/ Hibernate, Seam in Action offers critical info about overall tech. Bijection out, proxies, typesafe in.<br><br>In reply to: <a href="https://x.com/jbandi/status/918477225336832" rel="noopener noreferrer" target="_blank">@jbandi</a> <span class="status">918477225336832</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1525189460365312',
    created: 1289198608000,
    type: 'post',
    text: 'Where did the weekend go? Can I get it back? I need a lot more than just a free hour.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1524975198535680',
    created: 1289198557000,
    type: 'post',
    text: 'Today I patched in support for Jetty 7 &amp; 8 in #Weld Servlet. Also got injection into listeners working: <a href="https://github.com/weld/core/pulls" rel="noopener noreferrer" target="_blank">github.com/weld/core/pulls</a>',
    likes: 0,
    retweets: 0,
    tags: ['weld']
  },
  {
    id: '1512366311211008',
    created: 1289195550000,
    type: 'reply',
    text: '@chuggid And yet it would be such a shame to let that FUD rob you of experiencing such a powerful &amp; productive programming model. #jsr299<br><br>In reply to: @chuggid <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['jsr299']
  },
  {
    id: '1028673272549376',
    created: 1289080229000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bileblog" rel="noopener noreferrer" target="_blank">@bileblog</a> I added an explanation as to why there are no #Arquillian distribution downloads &amp; when you can expect them. Thanks.<br><br>In reply to: <a href="https://x.com/bileblog/status/352296877686784" rel="noopener noreferrer" target="_blank">@bileblog</a> <span class="status">352296877686784</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '1025730989260800',
    created: 1289079527000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/craschka" rel="noopener noreferrer" target="_blank">@craschka</a> Can you provide details as to what isn\'t working for you? We have an issue tracker -&gt; <a href="http://jira.jboss.org/browse/WELD" rel="noopener noreferrer" target="_blank">jira.jboss.org/browse/WELD</a><br><br>In reply to: <a href="https://x.com/craschka/status/29495492697" rel="noopener noreferrer" target="_blank">@craschka</a> <span class="status">29495492697</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1025354177183744',
    created: 1289079438000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/espenkl" rel="noopener noreferrer" target="_blank">@espenkl</a> Awesome. One of the key features of #Arquillian is that you can run it in the IDE just like a unit test.<br><br>In reply to: <a href="https://x.com/espenkl/status/480212223528960" rel="noopener noreferrer" target="_blank">@espenkl</a> <span class="status">480212223528960</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '1024507087163393',
    created: 1289079236000,
    type: 'reply',
    text: '@brianleathem <a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I\'m all about the feedback. Let\'s encourage folks to spend time on fixes instead of blog wars ;)<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1023802926436353',
    created: 1289079068000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> What matters is how you respond to the bugs ;) We strive to be vigilant. Our investment in #Arquillian proves this as well.<br><br>In reply to: <a href="https://x.com/JohnAment/status/1005159836553216" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">1005159836553216</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '1023379133956097',
    created: 1289078967000,
    type: 'reply',
    text: '@brianleathem Thank you. Exactly. We\'ll get the leaks ironed out if they\'re real &amp; aren\'t already resolved. Progress folks, progress ;)<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1004595505537024',
    created: 1289074488000,
    type: 'post',
    text: 'Spent 2 quiet days diving into #Weld Extensions - bad timing it seems. Bling! Absolutely a swiss army knife of #javaee. Will demo @ #devoxx',
    likes: 0,
    retweets: 0,
    tags: ['weld', 'javaee', 'devoxx']
  },
  {
    id: '1003172487237632',
    created: 1289074149000,
    type: 'post',
    text: 'It just happens that <a class="mention" href="https://x.com/plmuir" rel="noopener noreferrer" target="_blank">@plmuir</a> is out this week. He\'ll put these claims about poor quality to rest next week, no doubt. Cool the jets.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1001818192609282',
    created: 1289073826000,
    type: 'post',
    text: 'What\'s w/ the hatin\' on #Weld this week? A cornerstone of #javaee 6 available under #ASL, quick releases &amp; well tested; that deserves hate?',
    likes: 0,
    retweets: 0,
    tags: ['weld', 'javaee', 'asl']
  },
  {
    id: '29675999129',
    created: 1288884174000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jodastephen" rel="noopener noreferrer" target="_blank">@jodastephen</a> <a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> <a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> Now that\'s really starting to sound promising. Renewed faith in #javaee &amp; #jcp. Could be quite a year.<br><br>In reply to: <a href="https://x.com/jodastephen/status/29666809002" rel="noopener noreferrer" target="_blank">@jodastephen</a> <span class="status">29666809002</span>',
    likes: 0,
    retweets: 0,
    tags: ['javaee', 'jcp']
  },
  {
    id: '29666895383',
    created: 1288876265000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Rats. I was dreaming about work last night. Hence my blurry eyes.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/29643721089" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">29643721089</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '29666708329',
    created: 1288876106000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> <a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> #github allows renames, it just doesn\'t prompt for a custom name. Then you get a big disclaimer when you rename.<br><br>In reply to: <a href="https://x.com/JohnAment/status/29659365515" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">29659365515</span>',
    likes: 0,
    retweets: 0,
    tags: ['github']
  },
  {
    id: '29636590323',
    created: 1288841605000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kengkaj_s" rel="noopener noreferrer" target="_blank">@kengkaj_s</a> True, interfaces have methods. But these days annotations can indicate functionality as much as method signatures.<br><br>In reply to: <a href="https://x.com/kengkaj_s/status/29634536071" rel="noopener noreferrer" target="_blank">@kengkaj_s</a> <span class="status">29634536071</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '29634263031',
    created: 1288839619000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iapazmino" rel="noopener noreferrer" target="_blank">@iapazmino</a> Awesome! So glad you finally made Ike happy. Look forward to your feedback on how to make it simpler to get started. #arquillian<br><br>In reply to: <a href="https://x.com/iapazmino/status/29592024284" rel="noopener noreferrer" target="_blank">@iapazmino</a> <span class="status">29592024284</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '29634122154',
    created: 1288839502000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> Yep. Question is, should we rename projects or should #github address issue? I think a fork should prompt for project name.<br><br>In reply to: <a href="https://x.com/JohnAment/status/29607753829" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">29607753829</span>',
    likes: 0,
    retweets: 0,
    tags: ['github']
  },
  {
    id: '29605794714',
    created: 1288818254000,
    type: 'post',
    text: 'Not as serious as I thought. You can rename your fork. So #github should ask you for a fork name when you fork.',
    likes: 0,
    retweets: 0,
    tags: ['github']
  },
  {
    id: '29605628401',
    created: 1288818113000,
    type: 'post',
    text: 'Uh oh, you can\'t fork two projects on #github that have the same name but belong to different organizations. Serious problem.',
    likes: 0,
    retweets: 0,
    tags: ['github']
  },
  {
    id: '29605310888',
    created: 1288817847000,
    type: 'post',
    text: 'I noticed when I forked weld/core on #github, it becomes myusername/core. Is there a namespacing problem? Should projects use a prefix-?',
    likes: 0,
    retweets: 0,
    tags: ['github']
  },
  {
    id: '29596855859',
    created: 1288810479000,
    type: 'post',
    text: 'The link to my #cdi &amp; #javaee6 webinar replay seems to be difficult to track down. You can find it here <a href="http://jboss.com/promo/EE6" rel="noopener noreferrer" target="_blank">jboss.com/promo/EE6</a>',
    likes: 4,
    retweets: 2,
    tags: ['cdi', 'javaee6']
  },
  {
    id: '29593406565',
    created: 1288807437000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <a class="mention" href="https://x.com/AdamBien" rel="noopener noreferrer" target="_blank">@AdamBien</a> I have an example here: <a href="https://github.com/arquillian/arquillian-examples" rel="noopener noreferrer" target="_blank">github.com/arquillian/arquillian-examples</a> (see xa project)<br><br>In reply to: <a href="https://x.com/lightguardjp/status/29592407149" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">29592407149</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '29592888341',
    created: 1288806992000,
    type: 'post',
    text: 'Yes, you can grab all types &amp; check for the annotation, but native support would be nice: MyBean annotated Stateful &amp; generic &lt;<a class="mention" href="https://x.com/stateful" rel="noopener noreferrer" target="_blank">@stateful</a>&gt;',
    likes: 0,
    retweets: 0
  },
  {
    id: '29592684625',
    created: 1288806820000,
    type: 'post',
    text: 'It would be nice if Java supported the concept of matching a type that has an annotation, as a complement to matching based on interface.',
    likes: 0,
    retweets: 0
  },
  {
    id: '29584967644',
    created: 1288800640000,
    type: 'post',
    text: '"Make the doer the subject." Keen advice to avoid passive voice in writing from the Grammar Divas.',
    likes: 0,
    retweets: 0
  },
  {
    id: '29579836662',
    created: 1288797030000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AndroidDev" rel="noopener noreferrer" target="_blank">@AndroidDev</a> It\'s about freeakin\' time a changelog area is provided for market updates.<br><br>In reply to: <a href="https://x.com/AndroidDev/status/29568109123" rel="noopener noreferrer" target="_blank">@AndroidDev</a> <span class="status">29568109123</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '29542200375',
    created: 1288759861000,
    type: 'post',
    text: 'Nice to see we\'ve collected a few signatures on our open letter to the EC calling for #JCP reform <a href="https://mojavelinux.com/blog/archives/2010/11/open_letter_to_the_jcp_executive_committee_calling_for_jcp_reform/" rel="noopener noreferrer" target="_blank">mojavelinux.com/blog/archives/2010/11/open_letter_to_the_jcp_executive_committee_calling_for_jcp_reform/</a>',
    likes: 0,
    retweets: 1,
    tags: ['jcp']
  },
  {
    id: '29497306885',
    created: 1288724981000,
    type: 'post',
    text: 'Apache, Red Hat, Eclipse and Google on the #JCP EC. That\'s got to be our best chance for change yet.',
    likes: 0,
    retweets: 5,
    tags: ['jcp']
  },
  {
    id: '29492561210',
    created: 1288720887000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/evanchooly" rel="noopener noreferrer" target="_blank">@evanchooly</a> I should have clarified, one based on HTML (i.e., for the browsable web) :)<br><br>In reply to: <a href="https://x.com/evanchooly/status/29428464681" rel="noopener noreferrer" target="_blank">@evanchooly</a> <span class="status">29428464681</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '29492496183',
    created: 1288720833000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/heiko_braun" rel="noopener noreferrer" target="_blank">@heiko_braun</a> My point is that in the standard Java EE platform, there is no framework like GWT. Fair to say that JSF and GWT are different.<br><br>In reply to: <a href="https://x.com/heiko_braun/status/29447228576" rel="noopener noreferrer" target="_blank">@heiko_braun</a> <span class="status">29447228576</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '29416984074',
    created: 1288654269000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/TessiePorter" rel="noopener noreferrer" target="_blank">@TessiePorter</a> Hahaha. I\'ve been singing praises for SLC to my wife ever since I returned. But Santa Fe really captured our hearts.<br><br>In reply to: <a href="https://x.com/mrslightguardjp/status/29412016594" rel="noopener noreferrer" target="_blank">@mrslightguardjp</a> <span class="status">29412016594</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '29410905739',
    created: 1288649631000,
    type: 'post',
    text: 'Case in point, JSF is a server-side component web framework. What if you need a client-server web framework like GWT? There is a void.',
    likes: 0,
    retweets: 2
  },
  {
    id: '29410695407',
    created: 1288649462000,
    type: 'post',
    text: 'My open letter raises a very important point. #JSF is very compelling, but we shouldn\'t limit the platform to a single view technology.',
    likes: 1,
    retweets: 1,
    tags: ['jsf']
  },
  {
    id: '29409925220',
    created: 1288648852000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cagataycivici" rel="noopener noreferrer" target="_blank">@cagataycivici</a> Seam 2 core is compatible with JSF 2, but it\'s unlikely Seam 2 UI will work because of conflicts w/ the Facelets codebases.<br><br>In reply to: @cagataycivici <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '29409710744',
    created: 1288648679000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> Yep, I wanted to make sure that the changes I believe would save/strengthen the JCP are clearly stated &amp; not just in my head :)<br><br>In reply to: @pelegri <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '29405886335',
    created: 1288645456000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Now you can join the cause here: <a href="http://www.petitiononline.com/fixjcp/petition.html" rel="noopener noreferrer" target="_blank">www.petitiononline.com/fixjcp/petition.html</a><br><br>In reply to: <a href="https://x.com/ALRubinger/status/29404221013" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">29404221013</span>',
    likes: 0,
    retweets: 1
  },
  {
    id: '29405865945',
    created: 1288645438000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JCastroS" rel="noopener noreferrer" target="_blank">@JCastroS</a> Got it, thanks.<br><br>In reply to: <a href="https://x.com/JCastroS/status/29404603486" rel="noopener noreferrer" target="_blank">@JCastroS</a> <span class="status">29404603486</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '29405850748',
    created: 1288645424000,
    type: 'reply',
    text: '@mwessendorf <a class="mention" href="https://x.com/joesuf" rel="noopener noreferrer" target="_blank">@joesuf</a> Looks good, exception it\'s featuring a project which is now in the attic.<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '29405797250',
    created: 1288645377000,
    type: 'post',
    text: 'Sign the #JCP reform letter w/o a facebook login here <a href="http://www.petitiononline.com/fixjcp/petition.html" rel="noopener noreferrer" target="_blank">www.petitiononline.com/fixjcp/petition.html</a>',
    likes: 0,
    retweets: 2,
    tags: ['jcp']
  },
  {
    id: '29404411447',
    created: 1288644144000,
    type: 'post',
    text: 'What\'s a good open platform for hosting a petition? I didn\'t realize petitionspot required #facebook.',
    likes: 0,
    retweets: 1,
    tags: ['facebook']
  },
  {
    id: '29404331108',
    created: 1288644072000,
    type: 'reply',
    text: '@brianleathem <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> We put it up on petitionspot just so people could show support. We\'ll move it if there\'s a better choice.<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 1
  },
  {
    id: '29402811038',
    created: 1288642706000,
    type: 'post',
    text: 'I sincerely hope that the #JCP reform I\'ve proposed, if carried out, will bring back people like Doug Lea. <a href="https://mojavelinux.com/blog/archives/2010/11/open_letter_to_the_jcp_executive_committee_calling_for_jcp_reform/" rel="noopener noreferrer" target="_blank">mojavelinux.com/blog/archives/2010/11/open_letter_to_the_jcp_executive_committee_calling_for_jcp_reform/</a>',
    likes: 0,
    retweets: 1,
    tags: ['jcp']
  },
  {
    id: '29402448590',
    created: 1288642376000,
    type: 'post',
    text: 'Finally, I\'ve clearly voiced my opinion about the importance of open JSR mailinglists in the JCP: <a href="https://mojavelinux.com/blog/archives/2010/11/open_letter_to_the_jcp_executive_committee_calling_for_jcp_reform/" rel="noopener noreferrer" target="_blank">mojavelinux.com/blog/archives/2010/11/open_letter_to_the_jcp_executive_committee_calling_for_jcp_reform/</a>',
    likes: 0,
    retweets: 1
  },
  {
    id: '29402312573',
    created: 1288642252000,
    type: 'post',
    text: 'To join our cause for reforming the #JCP, you can add your signature here: <a href="http://www.petitionspot.com/petitions/jcpreform" rel="noopener noreferrer" target="_blank">www.petitionspot.com/petitions/jcpreform</a>',
    likes: 0,
    retweets: 0,
    tags: ['jcp']
  },
  {
    id: '29402205290',
    created: 1288642152000,
    type: 'post',
    text: '"Open Letter to the JCP Executive Committee calling for JCP reform" x 2 <a href="https://mojavelinux.com/blog/archives/2010/11/open_letter_to_the_jcp_executive_committee_calling_for_jcp_reform/" rel="noopener noreferrer" target="_blank">mojavelinux.com/blog/archives/2010/11/open_letter_to_the_jcp_executive_committee_calling_for_jcp_reform/</a> &amp; <a href="http://ocpsoft.com/?p=1667" rel="noopener noreferrer" target="_blank">ocpsoft.com/?p=1667</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '29399154847',
    created: 1288639351000,
    type: 'post',
    text: 'What the heck is going on w/ #gmail? I keep forgetting what I was going to say by the time the form appears.',
    likes: 0,
    retweets: 0,
    tags: ['gmail']
  },
  {
    id: '29154296962',
    created: 1288412879000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> I couldn\'t afford to buy you a car, but this tweet\'s for you. Put it on cruise control &amp; enjoy the b-day all weekend :)<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/29151733750" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">29151733750</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '29145297093',
    created: 1288405040000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> I think that AS 7 is right on the heels of AS 6, therefore worth promoting ahead of AS 6 as the commercial support candidate.<br><br>In reply to: <a href="https://x.com/JohnAment/status/29035422220" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">29035422220</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '29120033555',
    created: 1288383906000,
    type: 'post',
    text: 'Just had a nasty debug session trying to track down why #Weld Servlet won\'t work on Jetty 7. Low-level session problem. Works on Jetty 8.',
    likes: 0,
    retweets: 0,
    tags: ['weld']
  },
  {
    id: '29052769185',
    created: 1288322812000,
    type: 'post',
    text: 'haha "Hello, I\'m Little MOO, the bit of software that will manage your moo.com order. It will shortly be sent to Big MOO, our print machine"',
    likes: 1,
    retweets: 1
  },
  {
    id: '29031430763',
    created: 1288306809000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Haha, more like it just motivates me to write, and keep writing :)<br><br>In reply to: <a href="https://x.com/lightguardjp/status/29031279974" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">29031279974</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '29031373202',
    created: 1288306767000,
    type: 'post',
    text: 'Seam in Action isn\'t the only one having a revival. Java EE 6 is at the party to. From 2 to 8: Java EE 6 App Servers: <a href="http://blogs.sun.com/theaquarium/entry/from_2_to_8_java" rel="noopener noreferrer" target="_blank">blogs.sun.com/theaquarium/entry/from_2_to_8_java</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '29031192498',
    created: 1288306627000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/steveebersole" rel="noopener noreferrer" target="_blank">@steveebersole</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Cool! I love KDE goodness.<br><br>In reply to: <a href="https://x.com/steveebersole/status/29003700780" rel="noopener noreferrer" target="_blank">@steveebersole</a> <span class="status">29003700780</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '29031062059',
    created: 1288306526000,
    type: 'post',
    text: 'Wow, the week of Oct 27, Seam in Action climbed to the #2 bestseller spot on manning.com again. #revival',
    likes: 0,
    retweets: 0,
    tags: ['revival']
  },
  {
    id: '29025576402',
    created: 1288302085000,
    type: 'post',
    text: 'I finally got around to writing up the announcement for my #Arquillian article in #NFJS magazine <a href="http://community.jboss.org/en/arquillian/blog/2010/10/28/were-published" rel="noopener noreferrer" target="_blank">community.jboss.org/en/arquillian/blog/2010/10/28/were-published</a>',
    likes: 0,
    retweets: 3,
    tags: ['arquillian', 'nfjs']
  },
  {
    id: '28960109784',
    created: 1288242023000,
    type: 'post',
    text: 'If you carve Ike (#Arquillian logo) on a pumpkin, we\'ll post it on the project blog &amp; perhaps elsewhere <a href="http://jboss.org/arquillian/artwork" rel="noopener noreferrer" target="_blank">jboss.org/arquillian/artwork</a>',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '28959789239',
    created: 1288241667000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Bean Validation is a standalone programming model overlay that you can leverage internally. But better if it were in SE.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/28959252143" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">28959252143</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '28959698130',
    created: 1288241565000,
    type: 'post',
    text: 'I got tired of navigating JIRA to the outstanding issues in #Arquillian, so I made a convenient bookmark <a href="https://jira.jboss.org/secure/IssueNavigator.jspa?reset=true&amp;mode=hide&amp;jqlQuery=project+%3D+ARQ+AND+resolution+%3D+Unresolved+ORDER+BY+updated+DESC" rel="noopener noreferrer" target="_blank">jira.jboss.org/secure/IssueNavigator.jspa?reset=true&amp;mode=hide&amp;jqlQuery=project+%3D+ARQ+AND+resolution+%3D+Unresolved+ORDER+BY+updated+DESC</a>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '28958486252',
    created: 1288240258000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Exactly, which is another feature that #Weld Extensions will include, I think. We had that in #seam 2 as you recall.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/28958133083" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">28958133083</span>',
    likes: 0,
    retweets: 0,
    tags: ['weld', 'seam']
  },
  {
    id: '28958259922',
    created: 1288240024000,
    type: 'post',
    text: 'Bean Validation absolutely deserves to be in Java SE. It\'s no less important than XML parsers. Seriously.',
    likes: 1,
    retweets: 2
  },
  {
    id: '28958222028',
    created: 1288239985000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Dude, Bean Validation. I think perhaps a few beers with <a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> will help you out (or glasses of wine).<br><br>In reply to: <a href="https://x.com/ALRubinger/status/28808739471" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">28808739471</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '28958097254',
    created: 1288239858000,
    type: 'post',
    text: 'Finally got a chance to play with Froyo on my wife\'s phone. I\'m really liking it! Much better #gmail. Label selection still sucks though.',
    likes: 0,
    retweets: 0,
    tags: ['gmail']
  },
  {
    id: '28957935957',
    created: 1288239693000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> Shrug. If I\'m posting, I like it to be in my timeline. If I need a DM, I\'ll use a DM. Hey, at least I can control it w/ a hack.<br><br>In reply to: <a href="https://x.com/metacosm/status/28932680382" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">28932680382</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '28957846820',
    created: 1288239604000,
    type: 'post',
    text: 'Cool! My feature of fully-qualified bean names was accepted into #Weld Extensions. <a class="mention" href="https://x.com/FullyQualified" rel="noopener noreferrer" target="_blank">@FullyQualified</a> <a class="mention" href="https://x.com/Named" rel="noopener noreferrer" target="_blank">@Named</a> = package + lower class name.',
    likes: 0,
    retweets: 0,
    tags: ['weld']
  },
  {
    id: '28932257788',
    created: 1288219821000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jakobkorherr" rel="noopener noreferrer" target="_blank">@jakobkorherr</a> They might be stuck. The mirror requires a whitelist because of how it\'s setup. I\'ll update.<br><br>In reply to: <a href="https://x.com/jakobkorherr/status/28894047489" rel="noopener noreferrer" target="_blank">@jakobkorherr</a> <span class="status">28894047489</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '28932107051',
    created: 1288219711000,
    type: 'post',
    text: 'I was totally unaware of this reply-isn\'t-visible problem on twitter. Twitter, what the heck are you doing? Employing the leading . now.',
    likes: 0,
    retweets: 0
  },
  {
    id: '28918172760',
    created: 1288208022000,
    type: 'post',
    text: 'Just another day at the office.',
    photos: ['<div class="entry"><img class="photo" src="media/28918172760.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '28862390960',
    created: 1288156464000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Well, unless you use Bean Validation :)<br><br>In reply to: <a href="https://x.com/ALRubinger/status/28808127986" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">28808127986</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '28862252107',
    created: 1288156304000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> You could weave that into a custom EntityManager using a #JSR299 producer method. One idea.<br><br>In reply to: <a href="https://x.com/JohnAment/status/28841675667" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">28841675667</span>',
    likes: 0,
    retweets: 0,
    tags: ['jsr299']
  },
  {
    id: '28862126737',
    created: 1288156165000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SBryzak" rel="noopener noreferrer" target="_blank">@SBryzak</a> Thanks <a class="mention" href="https://x.com/SBryzak" rel="noopener noreferrer" target="_blank">@SBryzak</a> for posting! #JSR299 in #javaee5. I really need to get that Maven archetype published to central :)<br><br>In reply to: @sbryzak <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['jsr299', 'javaee5']
  },
  {
    id: '28861969911',
    created: 1288155989000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dzone" rel="noopener noreferrer" target="_blank">@dzone</a> Hahaha. This post originated in an underground restaurant in San Francisco during #javaone :) Ever read The Little Prince?<br><br>In reply to: <a href="https://x.com/DZoneInc/status/28861115547" rel="noopener noreferrer" target="_blank">@DZoneInc</a> <span class="status">28861115547</span>',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '28861870131',
    created: 1288155878000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> If that\'s the plan, super. If not, can we make it a plan to review the JCP charter? I promise changes we seek aren\'t outrageous.',
    likes: 0,
    retweets: 0
  },
  {
    id: '28861680180',
    created: 1288155687000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> I\'m still not equating an EC vote w/ the JCP being sorted out. Will this new EC actually review all the outcry &amp; take action?<br><br>In reply to: @pelegri <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '28837610334',
    created: 1288136605000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> <a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> And when will that be exactly? We can\'t wait forever. And we don\'t even know what we can do about it (other than complain).<br><br>In reply to: @pelegri <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '28837422624',
    created: 1288136477000,
    type: 'post',
    text: 'Just so my position is clear, I\'m a firm believer in open standards. I welcome a fair and functional #JCP, but atm it appears to be neither.',
    likes: 0,
    retweets: 1,
    tags: ['jcp']
  },
  {
    id: '28836806673',
    created: 1288136055000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matkar" rel="noopener noreferrer" target="_blank">@matkar</a> ...pay no attention to the departure of a really prominent figure who is deeply invested. He must have just had a bad day #wakeup<br><br>In reply to: <a href="https://x.com/matkar/status/28821111172" rel="noopener noreferrer" target="_blank">@matkar</a> <span class="status">28821111172</span>',
    likes: 0,
    retweets: 0,
    tags: ['wakeup']
  },
  {
    id: '28836592044',
    created: 1288135904000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matkar" rel="noopener noreferrer" target="_blank">@matkar</a> Eh, that doesn\'t give me any renewed confidence in the #jcp. I read they hope we ignore problems &amp; continue to use a broken process.<br><br>In reply to: <a href="https://x.com/matkar/status/28821111172" rel="noopener noreferrer" target="_blank">@matkar</a> <span class="status">28821111172</span>',
    likes: 0,
    retweets: 0,
    tags: ['jcp']
  },
  {
    id: '28835781799',
    created: 1288135331000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> How do we get #Arquillian added to that receipe? I guess we need the release out w/ Selenium integration ;)<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '28820352069',
    created: 1288122701000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cyberzac" rel="noopener noreferrer" target="_blank">@cyberzac</a> #Arquillian will be in beta very soon. And our alpha status does not mean it\'s incomplete, but rather sorting out the SPIs.<br><br>In reply to: <a href="https://x.com/cyberzac/status/28818417078" rel="noopener noreferrer" target="_blank">@cyberzac</a> <span class="status">28818417078</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '28765266106',
    created: 1288073870000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> We had a slight...um...communication breakdown. We seem to have forgotten to pay the hosting provider. Whoops!<br><br>In reply to: <a href="https://x.com/arungupta/status/28721580401" rel="noopener noreferrer" target="_blank">@arungupta</a> <span class="status">28721580401</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '28765200182',
    created: 1288073781000,
    type: 'post',
    text: 'The title of the #NFJS magazine that contains my #Arquillian article is "Control Your Destiny". Awesome. How fitting.',
    likes: 0,
    retweets: 0,
    tags: ['nfjs', 'arquillian']
  },
  {
    id: '28716814898',
    created: 1288033006000,
    type: 'post',
    text: 'Working from the beach this week. First, need to get there. Hitting the road.',
    likes: 0,
    retweets: 0
  },
  {
    id: '28713275241',
    created: 1288029815000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AdamBien" rel="noopener noreferrer" target="_blank">@AdamBien</a> How about at Devoxx? <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> won\'t be there, unfortunately, but <a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> will be there.<br><br>In reply to: <a href="https://x.com/AdamBien/status/28709029319" rel="noopener noreferrer" target="_blank">@AdamBien</a> <span class="status">28709029319</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '28713194667',
    created: 1288029741000,
    type: 'post',
    text: 'Do you care about the future of #JSF 2 (#JSR-314)? Then you may want to read this call for action by #JBoss: <a href="https://in.relation.to/Bloggers/WhenWillJSFHaveANewJSROfficialExpertGroup" rel="noopener noreferrer" target="_blank">in.relation.to/Bloggers/WhenWillJSFHaveANewJSROfficialExpertGroup</a>',
    likes: 0,
    retweets: 11,
    tags: ['jsf', 'jsr', 'jboss']
  },
  {
    id: '28707950258',
    created: 1288025371000,
    type: 'post',
    text: 'If you are able to private message someone on Twitter, you should be able to e-mail them too. Hasn\'t trust already been est at that point.',
    likes: 0,
    retweets: 0
  },
  {
    id: '28706301772',
    created: 1288024111000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> This bunny is recharged. Boom, boom, boom... :)<br><br>In reply to: <a href="https://x.com/lightguardjp/status/28701501033" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">28701501033</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '28703030405',
    created: 1288021731000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> &amp; I were invited to speak on #Arquillian @ #JavaOne China. Wow! But that might as well be on the moon. Too far on short notice.',
    likes: 0,
    retweets: 1,
    tags: ['arquillian', 'javaone']
  },
  {
    id: '28697098584',
    created: 1288017717000,
    type: 'post',
    text: 'I strongly recommend supporting this tech mag of rare quality. RT <a class="mention" href="https://x.com/NFJSMag" rel="noopener noreferrer" target="_blank">@NFJSMag</a> Latest issue is out! To subscribe, go here: <em>&lt;expired link&gt;</em>',
    likes: 1,
    retweets: 0
  },
  {
    id: '28666815528',
    created: 1287988546000,
    type: 'post',
    text: '"The #ShrinkWrap descriptors are as good as gold to us." <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> during his #Seam forge demo. Thanks!',
    likes: 0,
    retweets: 1,
    tags: ['shrinkwrap', 'seam']
  },
  {
    id: '28662546069',
    created: 1287983135000,
    type: 'post',
    text: 'Finally had a chance to dabble in #SeamForge. Super excited to see the progress &amp; also that it uses the #ShrinkWrap descriptors project!',
    likes: 0,
    retweets: 0,
    tags: ['seamforge', 'shrinkwrap']
  },
  {
    id: '28645642463',
    created: 1287968327000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/mojavelinux" rel="noopener noreferrer" target="_blank">@mojavelinux</a> To even top that, it was the 4th playoff hole &amp; the tournament was in Las Vegas. Someone lost big @ the roulette wheel #balance',
    likes: 0,
    retweets: 0,
    tags: ['balance']
  },
  {
    id: '28645319859',
    created: 1287968072000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/mojavelinux" rel="noopener noreferrer" target="_blank">@mojavelinux</a> And to top that, he didn\'t even see it go in the hole because it was so dark. Stranger than fiction.',
    likes: 0,
    retweets: 0
  },
  {
    id: '28645237545',
    created: 1287968008000,
    type: 'post',
    text: 'Wow. I just witness my first live hole in one (on TV). Jonathan Byrd just smoked it. On top of that, it gave him the win in a 3-way playoff.',
    likes: 0,
    retweets: 0
  },
  {
    id: '28631026569',
    created: 1287956572000,
    type: 'post',
    text: 'Just signed the Contribute the Apple JDK source to OpenJDK petition (#602) @ <a href="http://www.petitionspot.com/petitions/macjdk" rel="noopener noreferrer" target="_blank">www.petitionspot.com/petitions/macjdk</a>',
    likes: 0,
    retweets: 1
  },
  {
    id: '28630214022',
    created: 1287955840000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> <a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> In my talks, particularly #Arquillian, I need to demonstrate w/ all Java IDEs (not always possible due to time limits).<br><br>In reply to: <a href="https://x.com/arungupta/status/28629471695" rel="noopener noreferrer" target="_blank">@arungupta</a> <span class="status">28629471695</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '28630120165',
    created: 1287955758000,
    type: 'post',
    text: 'Are you a presenter? Do you demo w/ #Eclipse? Get #tarlog to control the font size w/ Ctrl+Shift++ &amp; Ctrl+- <a href="http://tarlogonjava.blogspot.com/2009/03/tarlog-plugins-13.html" rel="noopener noreferrer" target="_blank">tarlogonjava.blogspot.com/2009/03/tarlog-plugins-13.html</a>',
    likes: 5,
    retweets: 3,
    tags: ['eclipse', 'tarlog']
  },
  {
    id: '28629396966',
    created: 1287955107000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cmaki" rel="noopener noreferrer" target="_blank">@cmaki</a> I\'m glad you asked. I\'m just going to be ready for it next time :)<br><br>In reply to: <a href="https://x.com/cmaki/status/28626812923" rel="noopener noreferrer" target="_blank">@cmaki</a> <span class="status">28626812923</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '28629242925',
    created: 1287954967000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> You are my hero! I must have lost it in the comotion of the conference. Grabbing it now.<br><br>In reply to: <a href="https://x.com/maxandersen/status/28626355141" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">28626355141</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '28625556137',
    created: 1287951646000,
    type: 'post',
    text: 'The reason it\'s so hard to change the font size using the preferences mid-talk is that your brain cannot do both tasks at once.',
    likes: 0,
    retweets: 0
  },
  {
    id: '28625464125',
    created: 1287951563000,
    type: 'post',
    text: 'I found myself in the hot seat again @ the UJUG talk trying in increase the editor font size in #Eclipse mid-talk. A keybinding, please!!',
    likes: 0,
    retweets: 0,
    tags: ['eclipse']
  },
  {
    id: '28621719921',
    created: 1287948374000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> You know, when I was leaving I could have sworn I saw the peaks starting to turn white. So I wasn\'t just seeing things.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/28618968192" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">28618968192</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '28566548618',
    created: 1287896009000,
    type: 'post',
    text: 'My neighbor laughs like a hyena and it\'s making me go mad. No, seriously. It\'s insane. Even a hyena would be like "OMG, please shut up."',
    likes: 0,
    retweets: 0
  },
  {
    id: '28566311533',
    created: 1287895808000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Was it painted in jello?<br><br>In reply to: <a href="https://x.com/lightguardjp/status/28565253030" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">28565253030</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '28566280195',
    created: 1287895778000,
    type: 'post',
    text: '@bsdrum Programming Java EE/Seam, drumming and snowboarding. We have a lot in common it seems :)',
    likes: 0,
    retweets: 0
  },
  {
    id: '28566210717',
    created: 1287895711000,
    type: 'reply',
    text: '@bsdrum <a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Hahaha. Yeah, he\'s definitely a fan of the sure thing. I think we few more #seam modules out and he\'ll see the light.<br><br>In reply to: @bsdrum <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '28565995610',
    created: 1287895505000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I expected you\'d want to break after the intense 3 days of geekdom :) I just needed a puzzle to shorten my trip. Fit the bill.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/28565232040" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">28565232040</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '28564897920',
    created: 1287894512000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> See my previous post :)<br><br>In reply to: <a href="https://x.com/lightguardjp/status/28551671635" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">28551671635</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '28564869814',
    created: 1287894487000,
    type: 'post',
    text: 'Forked my first #Seam 3 module. Suprisingly, not faces. <a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> &amp; I were brainstorming some fluent APIs and I dove in shortly after :)',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '28550598796',
    created: 1287882760000,
    type: 'post',
    text: 'On a plane, it\'s always a race between how fast my mind can think of the right code to type &amp; how fast my battery is getting drained.',
    likes: 0,
    retweets: 0
  },
  {
    id: '28511247254',
    created: 1287848511000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/TessiePorter" rel="noopener noreferrer" target="_blank">@TessiePorter</a> <a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I had a pleasure meeting you &amp; your beautiful children. Certainly made every minute of trip whorthwhile ;)<br><br>In reply to: <a href="https://x.com/mrslightguardjp/status/28477830543" rel="noopener noreferrer" target="_blank">@mrslightguardjp</a> <span class="status">28477830543</span>',
    likes: 0,
    retweets: 1
  },
  {
    id: '28509795197',
    created: 1287847494000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> &amp; <a class="mention" href="https://x.com/TessiePorter" rel="noopener noreferrer" target="_blank">@TessiePorter</a> set a high bar for community hospitality. Home cooked meat &amp; potatoes, then family time &amp; conversation. Tnx!',
    likes: 0,
    retweets: 1
  },
  {
    id: '28453274046',
    created: 1287793117000,
    type: 'post',
    text: 'When you drive crazy, people behind you no loner say "This guy must be lost." Now they say "This guy must have a GPS."',
    likes: 0,
    retweets: 1
  },
  {
    id: '28452888168',
    created: 1287792797000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mojavelinux" rel="noopener noreferrer" target="_blank">@mojavelinux</a> <a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <a class="mention" href="https://x.com/zackerylee" rel="noopener noreferrer" target="_blank">@zackerylee</a> @bsdrum Add @rickgoosen to that list. #NFL was amongst the topics of discussion.<br><br>In reply to: <a href="#28446276043">28446276043</a>',
    likes: 0,
    retweets: 0,
    tags: ['nfl']
  },
  {
    id: '28447528971',
    created: 1287788372000,
    type: 'post',
    text: 'The organization icon (top right) on a #github org page (github.com/seam) should be a link. Otherwise, navigation is massively confusing.',
    likes: 0,
    retweets: 0,
    tags: ['github']
  },
  {
    id: '28447292429',
    created: 1287788168000,
    type: 'post',
    text: 'My message to #JUGs worldwide: You\'re a critical voice for the Java community. Stay strong, unite &amp; speak out! You can make a difference.',
    likes: 0,
    retweets: 2,
    tags: ['jugs']
  },
  {
    id: '28447001656',
    created: 1287787916000,
    type: 'reply',
    text: 'Agreed. It\'s a huge boost for progress &amp; community! RT <a class="mention" href="https://x.com/jganoff" rel="noopener noreferrer" target="_blank">@jganoff</a> Oh how I\'ve missed using git. Glad Seam 3 switched over! #git #jboss #seam<br><br>In reply to: <a href="https://x.com/jganoff/status/28398303302" rel="noopener noreferrer" target="_blank">@jganoff</a> <span class="status">28398303302</span>',
    likes: 0,
    retweets: 0,
    tags: ['git', 'jboss', 'seam']
  },
  {
    id: '28446606864',
    created: 1287787577000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <a class="mention" href="https://x.com/joshbloch" rel="noopener noreferrer" target="_blank">@joshbloch</a> Some parallels to our position on JSR-314 (#JSF 2 spec): <a href="http://lists.jboss.org/pipermail/jsr-314-open-mirror/2010-August/000324.html" rel="noopener noreferrer" target="_blank">lists.jboss.org/pipermail/jsr-314-open-mirror/2010-August/000324.html</a><br><br>In reply to: <a href="https://x.com/jeremynorris/status/28431782547" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">28431782547</span>',
    likes: 0,
    retweets: 0,
    tags: ['jsf']
  },
  {
    id: '28446276043',
    created: 1287787290000,
    type: 'post',
    text: 'Enjoyed day @ the office w/ <a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> &amp; colleagues <a class="mention" href="https://x.com/zackerylee" rel="noopener noreferrer" target="_blank">@zackerylee</a>, @bsdrum, Ray Van Eperen, +2 chatting about #javaee &amp; migrations.',
    likes: 0,
    retweets: 1,
    tags: ['javaee']
  },
  {
    id: '28446036188',
    created: 1287787082000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CharlieCollins" rel="noopener noreferrer" target="_blank">@CharlieCollins</a> As all G1s do, it probably lost itself. That\'s the only way we can separate from our nostalgia.<br><br>In reply to: <a href="https://x.com/CharlieCollins/status/28406326518" rel="noopener noreferrer" target="_blank">@CharlieCollins</a> <span class="status">28406326518</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '28445945085',
    created: 1287787002000,
    type: 'post',
    text: 'My wife e-mails yesterday to boast that her phone got the #Froyo upgrade. Next thing I know people will start calling me Gramps.',
    likes: 0,
    retweets: 0,
    tags: ['froyo']
  },
  {
    id: '28381834587',
    created: 1287731529000,
    type: 'post',
    text: 'There\'s no expiration date on the programming models #Arquillian can test. Not too late, either, I suppose. TDD EJB 2.1 <a href="http://skajotde.wordpress.com/2010/10/21/tdd-ejb21-with-arquillian-and-jboss/" rel="noopener noreferrer" target="_blank">skajotde.wordpress.com/2010/10/21/tdd-ejb21-with-arquillian-and-jboss/</a>',
    likes: 0,
    retweets: 2,
    tags: ['arquillian']
  },
  {
    id: '28381245758',
    created: 1287730765000,
    type: 'reply',
    text: '@nabeelalimemon Until I have time for a more complete response, you can start here: <a href="http://sfwk.org/Seam3/GetInvolved" rel="noopener noreferrer" target="_blank">sfwk.org/Seam3/GetInvolved</a><br><br>In reply to: <a href="https://x.com/base58ed/status/28001810177" rel="noopener noreferrer" target="_blank">@base58ed</a> <span class="status">28001810177</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '28380785340',
    created: 1287730144000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cmaki" rel="noopener noreferrer" target="_blank">@cmaki</a> I was glad to finally make it to UJUG, and was thrilled to see such a healthy attendance. Great city, great folks!<br><br>In reply to: <a href="https://x.com/cmaki/status/28357242588" rel="noopener noreferrer" target="_blank">@cmaki</a> <span class="status">28357242588</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '28380614219',
    created: 1287729913000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I thoroughly enjoyed meeting you and Tessie. Definitely made the epic travel day well worth the effort.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/28379612249" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">28379612249</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '28379465283',
    created: 1287728405000,
    type: 'post',
    text: 'While boarding plane, dude showed off his G2 when he saw my G1, then bantered guy w/ BlackBerry, asked if he knew they were going bankrupt.',
    likes: 0,
    retweets: 1
  },
  {
    id: '28037667256',
    created: 1287675571000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MobiWan" rel="noopener noreferrer" target="_blank">@MobiWan</a> Sort of. I wrote Zimbra and explained how broken it is. For instance, new messages in a thread don\'t inherit existing tags.<br><br>In reply to: <a href="https://x.com/MobiWan/status/28037040641" rel="noopener noreferrer" target="_blank">@MobiWan</a> <span class="status">28037040641</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '28037132395',
    created: 1287675183000,
    type: 'post',
    text: 'I got my exercise in ORD airport today. Where do you get yours?',
    likes: 0,
    retweets: 0
  },
  {
    id: '28036878518',
    created: 1287675004000,
    type: 'post',
    text: 'Linux Mag put out a Zimbra 2 vs Gmail comparison. I chuckled. Zimbra is worthless to me until they realize why Gmail has tags &amp; archive msg.',
    likes: 0,
    retweets: 0
  },
  {
    id: '28017351902',
    created: 1287660351000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> <a class="mention" href="https://x.com/alexismp" rel="noopener noreferrer" target="_blank">@alexismp</a> I read somewhere that pom order = classpath order is now by design.<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/28015730512" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">28015730512</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27999976443',
    created: 1287638927000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/johnsgreader" rel="noopener noreferrer" target="_blank">@johnsgreader</a> Gavin seems to have resurfaced, adding comments to this post about CDI &amp; Java EE 6. <a href="https://feeds.dzone.com/~r/dzone/frontpage/~3/U1GC_R8zWIE/views_on_cdi_and_whats_new_in_javaee6.html" rel="noopener noreferrer" target="_blank">feeds.dzone.com/~r/dzone/frontpage/~3/U1GC_R8zWIE/views_on_cdi_and_whats_new_in_javaee6.html</a><br><br>In reply to: <a href="https://x.com/johnsgreader/status/27908146028" rel="noopener noreferrer" target="_blank">@johnsgreader</a> <span class="status">27908146028</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27998287781',
    created: 1287636949000,
    type: 'post',
    text: 'Another approach for creating a managed bean that initialized on web app startup using #CDI &amp; #Servlet 3: <a href="http://gist.github.com/637959" rel="noopener noreferrer" target="_blank">gist.github.com/637959</a>',
    likes: 4,
    retweets: 1,
    tags: ['cdi', 'servlet']
  },
  {
    id: '27996125162',
    created: 1287634626000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/OpenEJB" rel="noopener noreferrer" target="_blank">@OpenEJB</a> Cool, that just fixed my #Arquillian demo. No more exception on undeploy :)<br><br>In reply to: <a href="https://x.com/OpenEJB/status/27990627558" rel="noopener noreferrer" target="_blank">@OpenEJB</a> <span class="status">27990627558</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '27995553206',
    created: 1287634058000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/OpenEJB" rel="noopener noreferrer" target="_blank">@OpenEJB</a> Congrats! In plenty of time for inclusion in the first #Arquillian beta.<br><br>In reply to: <a href="https://x.com/OpenEJB/status/27990627558" rel="noopener noreferrer" target="_blank">@OpenEJB</a> <span class="status">27990627558</span>',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '27965417231',
    created: 1287609766000,
    type: 'post',
    text: 'This announcement is seriously belated, but we have a public JSR-314 mirror at JBoss <a href="http://lists.jboss.org/pipermail/jsr-314-open-mirror" rel="noopener noreferrer" target="_blank">lists.jboss.org/pipermail/jsr-314-open-mirror</a>',
    likes: 0,
    retweets: 1
  },
  {
    id: '27964371516',
    created: 1287608487000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AdamBien" rel="noopener noreferrer" target="_blank">@AdamBien</a> JBoss-provided Java EE 6 spec pom: <a href="https://repository.jboss.org/nexus/content/groups/public/org/jboss/spec/jboss-javaee-6.0/1.0.0.Beta7/jboss-javaee-6.0-1.0.0.Beta7.pom" rel="noopener noreferrer" target="_blank">repository.jboss.org/nexus/content/groups/public/org/jboss/spec/jboss-javaee-6.0/1.0.0.Beta7/jboss-javaee-6.0-1.0.0.Beta7.pom</a> must specify &lt;type&gt;pom&lt;/type&gt;<br><br>In reply to: <a href="https://x.com/AdamBien/status/27960905428" rel="noopener noreferrer" target="_blank">@AdamBien</a> <span class="status">27960905428</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27962534803',
    created: 1287606457000,
    type: 'post',
    text: 'Why is there no "Open Image in this tab" option in Chromium? Perhaps a good extension.',
    likes: 0,
    retweets: 0
  },
  {
    id: '27959438465',
    created: 1287603683000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> I would include Wicket. I\'m not a big user of it, but it\'s an important contrast IMO.<br><br>In reply to: <a href="https://x.com/mraible/status/27903590022" rel="noopener noreferrer" target="_blank">@mraible</a> <span class="status">27903590022</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27959015810',
    created: 1287603310000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AdamBien" rel="noopener noreferrer" target="_blank">@AdamBien</a> It\'s best to have a real platform impl on test classpath, but there\'s no reason we should have to fight w/ toxic APIs.<br><br>In reply to: <a href="https://x.com/AdamBien/status/27914391896" rel="noopener noreferrer" target="_blank">@AdamBien</a> <span class="status">27914391896</span>',
    likes: 0,
    retweets: 1
  },
  {
    id: '27958977812',
    created: 1287603275000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AdamBien" rel="noopener noreferrer" target="_blank">@AdamBien</a> Amen. One of the reasons JBoss provides API pom org.jboss.spec.jboss-javaee-6.0 in JBoss nexus repo.<br><br>In reply to: <a href="https://x.com/AdamBien/status/27914391896" rel="noopener noreferrer" target="_blank">@AdamBien</a> <span class="status">27914391896</span>',
    likes: 0,
    retweets: 1
  },
  {
    id: '27958707201',
    created: 1287603025000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/steveebersole" rel="noopener noreferrer" target="_blank">@steveebersole</a> Added comment.<br><br>In reply to: <a href="https://x.com/steveebersole/status/27920556964" rel="noopener noreferrer" target="_blank">@steveebersole</a> <span class="status">27920556964</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27956552196',
    created: 1287601130000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/steveebersole" rel="noopener noreferrer" target="_blank">@steveebersole</a> Sort of a loaded question. You can only have #CDI w/ a certified impl. That said, Spring supports the JSR-330 annotations.<br><br>In reply to: <a href="https://x.com/steveebersole/status/27921327061" rel="noopener noreferrer" target="_blank">@steveebersole</a> <span class="status">27921327061</span>',
    likes: 0,
    retweets: 0,
    tags: ['cdi']
  },
  {
    id: '27956270371',
    created: 1287600879000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/evanchooly" rel="noopener noreferrer" target="_blank">@evanchooly</a> <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Another overloaded term that depends on "context" :)<br><br>In reply to: <a href="https://x.com/evanchooly/status/27943847827" rel="noopener noreferrer" target="_blank">@evanchooly</a> <span class="status">27943847827</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27956206616',
    created: 1287600822000,
    type: 'reply',
    text: '@brianleathem Same here. That\'s the great thing about webinars. You can watch them at your leisure :)<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27956126789',
    created: 1287600754000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MStine" rel="noopener noreferrer" target="_blank">@MStine</a> You\'re missing JSFUnit. Perhaps update the HtmlUnit to be HtmlUnit/HttpUnit/JSFUnit since they are of a similar breed.<br><br>In reply to: @mstine <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27943915199',
    created: 1287590501000,
    type: 'post',
    text: 'Today I\'m employing coffee as a service (#starbucks) rather than my own component model (#nespresso).',
    likes: 0,
    retweets: 1,
    tags: ['starbucks', 'nespresso']
  },
  {
    id: '27942715763',
    created: 1287589659000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> Ah, it was for author attribution.<br><br>In reply to: <a href="https://x.com/metacosm/status/27942531398" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">27942531398</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27942344624',
    created: 1287589396000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> The better part of that quote is "listen to their pain". That goes for more than just the software, but also quality of docs.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/27925661987" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">27925661987</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27942105179',
    created: 1287589230000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> That\'s what I was going for in my ad-hoc quoting syntax. Perhaps (via *) syntax is perhaps more "standard".<br><br>In reply to: <a href="https://x.com/metacosm/status/27912216891" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">27912216891</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27908208875',
    created: 1287557157000,
    type: 'post',
    text: 'Wrote up a blog entry about speaking at UJUG <a href="http://community.jboss.org/people/dan.j.allen/blog/2010/10/20/speaking-at-utah-jug" rel="noopener noreferrer" target="_blank">community.jboss.org/people/dan.j.allen/blog/2010/10/20/speaking-at-utah-jug</a> Signing off.',
    likes: 0,
    retweets: 1
  },
  {
    id: '27906345010',
    created: 1287554600000,
    type: 'post',
    text: 'We also moderate a social bookmarking group where anyone can link to a #CDI extension. <a href="http://groups.diigo.com/group/cdi-extensions" rel="noopener noreferrer" target="_blank">groups.diigo.com/group/cdi-extensions</a>',
    likes: 0,
    retweets: 1,
    tags: ['cdi']
  },
  {
    id: '27906321879',
    created: 1287554569000,
    type: 'post',
    text: 'We encourage contributions from anyone in the community who wants to develop a new module for #Seam 3 (just get in contact!)',
    likes: 0,
    retweets: 1,
    tags: ['seam']
  },
  {
    id: '27906092961',
    created: 1287554270000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/plmuir" rel="noopener noreferrer" target="_blank">@plmuir</a> "#Seam 3 is implemented as a set of portable extensions (modules) for JSR-299 that run in any environment which supports JSR-299"',
    likes: 0,
    retweets: 1,
    tags: ['seam']
  },
  {
    id: '27906037859',
    created: 1287554196000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/plmuir" rel="noopener noreferrer" target="_blank">@plmuir</a> "Think of #CDI as the core of #Seam 3 - it\'s the basic programming model for your application components"',
    likes: 0,
    retweets: 1,
    tags: ['cdi', 'seam']
  },
  {
    id: '27906008506',
    created: 1287554157000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/plmuir" rel="noopener noreferrer" target="_blank">@plmuir</a> "I\'m a great believer that what makes a good framework developer is really understanding the problems the framework solves." Amen.',
    likes: 0,
    retweets: 1
  },
  {
    id: '27905798253',
    created: 1287553879000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Give me a laptop &amp; and audience and I can make the demo anytime, any place ;) We\'ll have opportunity aplenty.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/27901848548" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">27901848548</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27900453908',
    created: 1287547847000,
    type: 'reply',
    text: '@chuggid <a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <a href="http://www.oracle.com/technetwork/java/javase/documentation/spec-136004.html" rel="noopener noreferrer" target="_blank">www.oracle.com/technetwork/java/javase/documentation/spec-136004.html</a><br><br>In reply to: @chuggid <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27900394271',
    created: 1287547791000,
    type: 'post',
    text: 'The info page about my talk at UJUG is online, just in time! <a href="http://ujug.org/" rel="noopener noreferrer" target="_blank">ujug.org/</a> I\'ll cover #CDI, #Seam 3 &amp; #Arquillian in a blink of an eye.',
    likes: 0,
    retweets: 1,
    tags: ['cdi', 'seam', 'arquillian']
  },
  {
    id: '27898597066',
    created: 1287546168000,
    type: 'post',
    text: 'One approach to creating application-scoped startup managed beans using a #CDI extension: <a href="http://gist.github.com/635719" rel="noopener noreferrer" target="_blank">gist.github.com/635719</a>',
    likes: 3,
    retweets: 0,
    tags: ['cdi']
  },
  {
    id: '27897477893',
    created: 1287545188000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/plmuir" rel="noopener noreferrer" target="_blank">@plmuir</a> giving webinar on "Seam in the Clouds " Wed Oct 20. See link for time: <em>&lt;expired link&gt;</em>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27896466753',
    created: 1287544327000,
    type: 'reply',
    text: '@chuggid I found the spec :) (Actually <a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> did).<br><br>In reply to: @chuggid <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27896393855',
    created: 1287544265000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> Actually, it\'s more OTFM :) I\'ll let you work out what the stands for before giving it away.<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/27894217008" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">27894217008</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27893119443',
    created: 1287541590000,
    type: 'post',
    text: 'Time-saving advice. If you work closely with Java specs or even the APIs, take the time down download the relevant spec PDFs. Quite handy.',
    likes: 0,
    retweets: 0
  },
  {
    id: '27878300852',
    created: 1287530447000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Jon_E" rel="noopener noreferrer" target="_blank">@Jon_E</a> Yep, a friend of mine finally tracked it down. Oracle seems to have broken many of the links to older Java resources.<br><br>In reply to: <a href="https://x.com/Jon_E/status/27873747214" rel="noopener noreferrer" target="_blank">@Jon_E</a> <span class="status">27873747214</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27873783007',
    created: 1287527012000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mojavelinux" rel="noopener noreferrer" target="_blank">@mojavelinux</a> <a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Apparently in 1997, foobar hadn\'t been discovered yet. They were still at foobah.<br><br>In reply to: <a href="#27873692530">27873692530</a>',
    likes: 0,
    retweets: 1
  },
  {
    id: '27873692530',
    created: 1287526941000,
    type: 'post',
    text: 'FooBah becomes fooBah, Z becomes z, URL becomes URL. Introspector.decapitalize follows these rules. Thanks <a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27872745485',
    created: 1287526184000,
    type: 'post',
    text: 'It appears the JavaBean spec no longer exists. Is there a definitive document that defines the rules for a JavaBean property name?',
    likes: 0,
    retweets: 0
  },
  {
    id: '27866331998',
    created: 1287520605000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Knock \'em dead!<br><br>In reply to: <a href="https://x.com/ALRubinger/status/27853706013" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">27853706013</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27866209711',
    created: 1287520507000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/desandro" rel="noopener noreferrer" target="_blank">@desandro</a> I just had to blow on a cartridge today. The git rebase game.<br><br>In reply to: <a href="https://x.com/desandro/status/27862815771" rel="noopener noreferrer" target="_blank">@desandro</a> <span class="status">27862815771</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27866126341',
    created: 1287520433000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> My wife and I just watched it too, and we agree! Excellent. Great more lesson and personalities.<br><br>In reply to: <a href="https://x.com/maxandersen/status/27864668507" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">27864668507</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27866063634',
    created: 1287520378000,
    type: 'post',
    text: 'Did a little hacking on Weld Extensions yesterday. Proposed a <a class="mention" href="https://x.com/FullyQualified" rel="noopener noreferrer" target="_blank">@FullyQualified</a> annotation for <a class="mention" href="https://x.com/Named" rel="noopener noreferrer" target="_blank">@Named</a> beans. WELDX-179',
    likes: 0,
    retweets: 0
  },
  {
    id: '27852105649',
    created: 1287508274000,
    type: 'post',
    text: 'I download a lot of refcards. Eventually I need to look at one of them :) The one I have referenced so far is the #Git refcard.',
    likes: 0,
    retweets: 0,
    tags: ['git']
  },
  {
    id: '27851800473',
    created: 1287508033000,
    type: 'post',
    text: 'Is my XM trying to start something? Such a dirty vocabulary it has. My Prius is saying: Don\'t shoot the messenger.',
    photos: ['<div class="entry"><img class="photo" src="media/27851800473.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '27762932020',
    created: 1287430315000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jbosstools" rel="noopener noreferrer" target="_blank">@jbosstools</a> Awesome. Just awesome.<br><br>In reply to: <a href="https://x.com/jbosstools/status/27754935365" rel="noopener noreferrer" target="_blank">@jbosstools</a> <span class="status">27754935365</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27750121078',
    created: 1287419343000,
    type: 'post',
    text: 'Correction: New interview about #CDI and #Seam 3 with <a class="mention" href="https://x.com/plmuir" rel="noopener noreferrer" target="_blank">@plmuir</a> at JUDCon published: <a href="http://jaxenter.com/seam-3-interview.1-32253.html" rel="noopener noreferrer" target="_blank">jaxenter.com/seam-3-interview.1-32253.html</a>',
    likes: 2,
    retweets: 3,
    tags: ['cdi', 'seam']
  },
  {
    id: '27746290778',
    created: 1287416617000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> I\'ll be speaking at the Utah JUG Thur, Oct 21 on #CDI, #Weld, #Seam 3 and #Arquillian. It\'s JUG week for Ike and #JBoss.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/27737525300" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">27737525300</span>',
    likes: 0,
    retweets: 1,
    tags: ['cdi', 'weld', 'seam', 'arquillian', 'jboss']
  },
  {
    id: '27746103243',
    created: 1287416490000,
    type: 'post',
    text: 'New interview about #CDI and #Seam 3 with <a class="mention" href="https://x.com/plmuir" rel="noopener noreferrer" target="_blank">@plmuir</a> at JAXLondon published: <a href="http://jaxenter.com/seam-3-interview.1-32253.html" rel="noopener noreferrer" target="_blank">jaxenter.com/seam-3-interview.1-32253.html</a>',
    likes: 1,
    retweets: 0,
    tags: ['cdi', 'seam']
  },
  {
    id: '27745805294',
    created: 1287416293000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <a class="mention" href="https://x.com/tlberglund" rel="noopener noreferrer" target="_blank">@tlberglund</a> I certainly can\'t be credited with the idea, but Hans shared w/ me a daemon that could be use for that purpose.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/27704300621" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">27704300621</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27697384663',
    created: 1287369564000,
    type: 'post',
    text: 'There\'s one thing more painful than doing expense reports. Having to extend it another day to track down a charge :(',
    likes: 0,
    retweets: 0
  },
  {
    id: '27697309054',
    created: 1287369502000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> My next two trips are on United (one you are on to Devoxx). Hopefully they\'ll live up to our expectations!<br><br>In reply to: <a href="https://x.com/lincolnthree/status/27690932735" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">27690932735</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27697243425',
    created: 1287369446000,
    type: 'post',
    text: 'Finally made it back out to the driving range today after a month hiatus. Relieved to discover I haven\'t lost my swing. Next up, tee time.',
    likes: 0,
    retweets: 0
  },
  {
    id: '27611488538',
    created: 1287294380000,
    type: 'post',
    text: 'Just finished reading through the #Weld Extensions reference guide. A must read if you use or build on #CDI. <a href="http://docs.jboss.org/weld/extensions/reference/latest/en-US/html/" rel="noopener noreferrer" target="_blank">docs.jboss.org/weld/extensions/reference/latest/en-US/html/</a>',
    likes: 4,
    retweets: 2,
    tags: ['weld', 'cdi']
  },
  {
    id: '27600963407',
    created: 1287285126000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/plmuir" rel="noopener noreferrer" target="_blank">@plmuir</a> will be speaking on Nov 10 at the JBoss User Group Belgium about #Seam &amp; #Arquillian <a href="http://www.brussels-jug.be/?p=1057" rel="noopener noreferrer" target="_blank">www.brussels-jug.be/?p=1057</a>',
    likes: 0,
    retweets: 1,
    tags: ['seam', 'arquillian']
  },
  {
    id: '27587964514',
    created: 1287274780000,
    type: 'reply',
    text: '@nabeelalimemon We\'ll keep you updated. Just be sure to follow the #Seam news <a href="http://seamframework.org/service/Feed/atom/Aggregate/Seam3News" rel="noopener noreferrer" target="_blank">seamframework.org/service/Feed/atom/Aggregate/Seam3News</a><br><br>In reply to: <a href="https://x.com/base58ed/status/27575324545" rel="noopener noreferrer" target="_blank">@base58ed</a> <span class="status">27575324545</span>',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '27587891226',
    created: 1287274717000,
    type: 'reply',
    text: '@nabeelalimemon By production ready, I\'ll assume you mean 3.0.0.Final community release. Some as early as year\'s end, others further out.<br><br>In reply to: <a href="https://x.com/base58ed/status/27575324545" rel="noopener noreferrer" target="_blank">@base58ed</a> <span class="status">27575324545</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27573721234',
    created: 1287261756000,
    type: 'post',
    text: 'Without much ado, #Seam3 is now switched over to Git <a href="http://github.com/seam" rel="noopener noreferrer" target="_blank">github.com/seam</a> Fork away!',
    likes: 0,
    retweets: 5,
    tags: ['seam3']
  },
  {
    id: '27567916578',
    created: 1287256348000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dzone" rel="noopener noreferrer" target="_blank">@dzone</a> Um, yes go straight to the article. Do you think we enjoy wasting time? (I don\'t mind the floating header so I can vote).<br><br>In reply to: <a href="https://x.com/DZoneInc/status/27472334513" rel="noopener noreferrer" target="_blank">@DZoneInc</a> <span class="status">27472334513</span>',
    likes: 0,
    retweets: 3
  },
  {
    id: '27501192511',
    created: 1287195417000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> Btw, I\'ve always believed that, even 5 years ago when I was using Spring &amp; JSF together. I didn\'t discover Seam by accident.<br><br>In reply to: <a href="https://x.com/agoncal/status/27470307226" rel="noopener noreferrer" target="_blank">@agoncal</a> <span class="status">27470307226</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27500554997',
    created: 1287194931000,
    type: 'post',
    text: 'For those unfamiliar, Seam XML is for defining and configuring #CDI beans using "type-safe" XML...but you\'d know that by reading the post.',
    likes: 0,
    retweets: 0,
    tags: ['cdi']
  },
  {
    id: '27500010886',
    created: 1287194514000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kenfinnigan" rel="noopener noreferrer" target="_blank">@kenfinnigan</a> Shoot! I knew I was leaving someone important out :) Beer on me.<br><br>In reply to: <a href="https://x.com/kenfinnigan/status/27476107350" rel="noopener noreferrer" target="_blank">@kenfinnigan</a> <span class="status">27476107350</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27499921923',
    created: 1287194446000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> If were up to me, #JSF Managed Beans would absolutely be pruned. JSF should not be in the business of managing beans.<br><br>In reply to: <a href="https://x.com/agoncal/status/27470307226" rel="noopener noreferrer" target="_blank">@agoncal</a> <span class="status">27470307226</span>',
    likes: 1,
    retweets: 0,
    tags: ['jsf']
  },
  {
    id: '27470149343',
    created: 1287169819000,
    type: 'post',
    text: 'Example of an #Arquillian test using JSFUnit executing on an embedded Servlet container: <a href="https://github.com/arquillian/arquillian-examples/tree/master/jsfunit-servlet/" rel="noopener noreferrer" target="_blank">github.com/arquillian/arquillian-examples/tree/master/jsfunit-servlet/</a> (simplification still in order).',
    likes: 2,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '27458253716',
    created: 1287160572000,
    type: 'post',
    text: 'Just created a list of #Seam core devs &amp; committers. <a href="https://x.com/#!/jbossseam/seam-dev" rel="noopener noreferrer" target="_blank">x.com/#!/jbossseam/seam-dev</a> Let me know if I left out anyone.',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '27456550509',
    created: 1287159431000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/mojavelinux" rel="noopener noreferrer" target="_blank">@mojavelinux</a> What\'s fascinating is that both Jersey &amp; RESTEasy leverage #CDI extensions to provide CDI support. Circular harmony.',
    likes: 0,
    retweets: 0,
    tags: ['cdi']
  },
  {
    id: '27455954570',
    created: 1287159045000,
    type: 'post',
    text: 'Jersey "pushes #CDI and #Weld to the limits" to provide constructor injection. Interesting case study in CDI extensions <a href="http://blogs.sun.com/sandoz/entry/jersey_1_4_was_released" rel="noopener noreferrer" target="_blank">blogs.sun.com/sandoz/entry/jersey_1_4_was_released</a>',
    likes: 2,
    retweets: 1,
    tags: ['cdi', 'weld']
  },
  {
    id: '27402337678',
    created: 1287110235000,
    type: 'post',
    text: '#Arquillian 3-hour lab @ #Devoxx on Monday, Nov 15th is now official. <a href="http://www.devoxx.com/display/Devoxx2K10/Labs+Day+1" rel="noopener noreferrer" target="_blank">www.devoxx.com/display/Devoxx2K10/Labs+Day+1</a> Directed by <a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <a class="mention" href="https://x.com/plmuir" rel="noopener noreferrer" target="_blank">@plmuir</a> &amp; yours truly.',
    likes: 1,
    retweets: 2,
    tags: ['arquillian', 'devoxx']
  },
  {
    id: '27402019246',
    created: 1287109999000,
    type: 'reply',
    text: 'RIP @swd847 Hello <a class="mention" href="https://x.com/stuartwdouglas" rel="noopener noreferrer" target="_blank">@stuartwdouglas</a> &lt;- Follow him if you are interested in #Seam3<br><br>In reply to: <a href="https://x.com/stuartwdouglas/status/27382788640" rel="noopener noreferrer" target="_blank">@stuartwdouglas</a> <span class="status">27382788640</span>',
    likes: 0,
    retweets: 0,
    tags: ['seam3']
  },
  {
    id: '27401855505',
    created: 1287109874000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> +1 I so wish there were a document format I could actually tolerate.<br><br>In reply to: <a href="https://x.com/aalmiray/status/27383868467" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">27383868467</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27401533772',
    created: 1287109634000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brockm" rel="noopener noreferrer" target="_blank">@brockm</a> It\'s for the best. The next upgrade will uninstall Maven and install Gradle. Kind of like elevators passing by.<br><br>In reply to: <a href="https://x.com/brockm/status/27393794827" rel="noopener noreferrer" target="_blank">@brockm</a> <span class="status">27393794827</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27401410695',
    created: 1287109545000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dzone" rel="noopener noreferrer" target="_blank">@dzone</a> There are websites that can\'t handle multiple users? Not much of a website then, is it? People write articles about that?<br><br>In reply to: <a href="https://x.com/DZoneInc/status/27397111831" rel="noopener noreferrer" target="_blank">@DZoneInc</a> <span class="status">27397111831</span>',
    likes: 0,
    retweets: 1
  },
  {
    id: '27375083659',
    created: 1287089255000,
    type: 'post',
    text: 'A few folks mentioned they wanted more than one slide on #javaee6 testing. How\'s a 3-hour lab at #devoxx work for you? Join us.',
    likes: 0,
    retweets: 0,
    tags: ['javaee6', 'devoxx']
  },
  {
    id: '27374868962',
    created: 1287089068000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jbossnews" rel="noopener noreferrer" target="_blank">@jbossnews</a> Thank you all for listening in. Wow, community FTW!<br><br>In reply to: <a href="https://x.com/RHMiddleware/status/27371309861" rel="noopener noreferrer" target="_blank">@RHMiddleware</a> <span class="status">27371309861</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27371921094',
    created: 1287086521000,
    type: 'post',
    text: 'Finally breaking away to eat after 7 hours of pre- and post-webinar activity. Hopefully I made it to food before passing out.',
    likes: 0,
    retweets: 0
  },
  {
    id: '27371774105',
    created: 1287086397000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/myfear" rel="noopener noreferrer" target="_blank">@myfear</a> Absolutely. I would be glad to review. We\'ll also have to see what Ike has to say about it. Gotta make Ike happy.<br><br>In reply to: <a href="https://x.com/myfear/status/27369817051" rel="noopener noreferrer" target="_blank">@myfear</a> <span class="status">27369817051</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27369436304',
    created: 1287084318000,
    type: 'post',
    text: 'Note from editor about my upcoming #Arquillian article. "You wrote a very convincing and well-written article." Excellent :)',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '27367043331',
    created: 1287082207000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/myfear" rel="noopener noreferrer" target="_blank">@myfear</a> Yep, he\'s going to hate me :) Hahaha.<br><br>In reply to: <a href="https://x.com/myfear/status/27366799439" rel="noopener noreferrer" target="_blank">@myfear</a> <span class="status">27366799439</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27365784747',
    created: 1287081103000,
    type: 'post',
    text: 'Wow! <a class="mention" href="https://x.com/plmuir" rel="noopener noreferrer" target="_blank">@plmuir</a> (Pete Muir, #Weld &amp; #Seam lead) is on twitter. I thought we\'d never be able to drag him into this mess. Welcome to the party!',
    likes: 0,
    retweets: 3,
    tags: ['weld', 'seam']
  },
  {
    id: '27365497359',
    created: 1287080846000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maeste" rel="noopener noreferrer" target="_blank">@maeste</a> Awesome. Once we get the JBoss Testing Laboratory page up, then we can list the individual projects that funnel to #jbosstesting<br><br>In reply to: <a href="https://x.com/maeste/status/27365411694" rel="noopener noreferrer" target="_blank">@maeste</a> <span class="status">27365411694</span>',
    likes: 0,
    retweets: 0,
    tags: ['jbosstesting']
  },
  {
    id: '27363965805',
    created: 1287079520000,
    type: 'post',
    text: 'Made my slidedeck from the #javaee6 webinar available <a href="http://slidesha.re/cvquiK" rel="noopener noreferrer" target="_blank">slidesha.re/cvquiK</a> Red Hat will also publish slides + audio soon.',
    likes: 5,
    retweets: 5,
    tags: ['javaee6']
  },
  {
    id: '27361998668',
    created: 1287077869000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> <a class="mention" href="https://x.com/inject" rel="noopener noreferrer" target="_blank">@inject</a> <a class="mention" href="https://x.com/baby" rel="noopener noreferrer" target="_blank">@baby</a> Bottle b;<br><br>In reply to: <a href="https://x.com/aschwart/status/27354587249" rel="noopener noreferrer" target="_blank">@aschwart</a> <span class="status">27354587249</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27361274100',
    created: 1287077289000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/steveebersole" rel="noopener noreferrer" target="_blank">@steveebersole</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Awesome! Definitely connect up with @swd847, chat about the #seam3 persistence module. (if you aren\'t already).<br><br>In reply to: <a href="https://x.com/steveebersole/status/27359533867" rel="noopener noreferrer" target="_blank">@steveebersole</a> <span class="status">27359533867</span>',
    likes: 0,
    retweets: 0,
    tags: ['seam3']
  },
  {
    id: '27361146146',
    created: 1287077186000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/stevedperkins" rel="noopener noreferrer" target="_blank">@stevedperkins</a> Thanks! I do certainly appreciate you taking the time to listen in. Like I recommended, explore it, see what you think.<br><br>In reply to: <a href="https://x.com/BadMoonRosin/status/27360979671" rel="noopener noreferrer" target="_blank">@BadMoonRosin</a> <span class="status">27360979671</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27361034528',
    created: 1287077098000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/stevedperkins" rel="noopener noreferrer" target="_blank">@stevedperkins</a> Yes, in the sense that you don\'t have to ship an entire component model with every one of your applications.<br><br>In reply to: <a href="https://x.com/BadMoonRosin/status/27359507148" rel="noopener noreferrer" target="_blank">@BadMoonRosin</a> <span class="status">27359507148</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27360851431',
    created: 1287076951000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <a class="mention" href="https://x.com/hanrahan" rel="noopener noreferrer" target="_blank">@hanrahan</a> <a class="mention" href="https://x.com/LeeTree" rel="noopener noreferrer" target="_blank">@LeeTree</a> More like Seam will offer that experience w/o forcing you to adopt a different model :)<br><br>In reply to: <a href="https://x.com/lincolnthree/status/27359309636" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">27359309636</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27360729587',
    created: 1287076855000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/stevedperkins" rel="noopener noreferrer" target="_blank">@stevedperkins</a> Ah, rats, I forgot to mention the other 2 CDI implementations: OpenWebBeans &amp; Resin. The TCK is ASL, so no impedance.<br><br>In reply to: <a href="https://x.com/BadMoonRosin/status/27359076843" rel="noopener noreferrer" target="_blank">@BadMoonRosin</a> <span class="status">27359076843</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27354266777',
    created: 1287072156000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rodrigouchoa" rel="noopener noreferrer" target="_blank">@rodrigouchoa</a> In a sense, Seam 2 strikes a careful balance between deep standards integration &amp; interim solutions now rolled into standards.<br><br>In reply to: <a href="https://x.com/rodrigouchoa/status/27353570327" rel="noopener noreferrer" target="_blank">@rodrigouchoa</a> <span class="status">27353570327</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27352819749',
    created: 1287071160000,
    type: 'post',
    text: 'The twitter hash tag for today\'s #javaee6 webinar is #jboss. So look for activity as we get going in 15 minutes.',
    likes: 0,
    retweets: 3,
    tags: ['javaee6', 'jboss']
  },
  {
    id: '27347862399',
    created: 1287067908000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Or is it?<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27343470661',
    created: 1287065051000,
    type: 'post',
    text: 'I\'ve got 78 slides (30 w/ code) for the #javaee &amp; #cdi webinar today at 12 EST. I won\'t let you down :) <a href="http://www.jboss.com/promo/EE6/" rel="noopener noreferrer" target="_blank">www.jboss.com/promo/EE6/</a>',
    likes: 0,
    retweets: 2,
    tags: ['javaee', 'cdi']
  },
  {
    id: '27342941692',
    created: 1287064708000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/maeste" rel="noopener noreferrer" target="_blank">@maeste</a> Do you want to pair up and have the chats on #jbosstesting instead, or perhaps shepard both?<br><br>In reply to: <a href="https://x.com/maxandersen/status/27331558837" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">27331558837</span>',
    likes: 0,
    retweets: 0,
    tags: ['jbosstesting']
  },
  {
    id: '27291047968',
    created: 1287015719000,
    type: 'post',
    text: 'The git migration script for #seam3 is ready. We just need to do some testing &amp; let everyone know. Good thing tomorrow is the IRC meeting.',
    likes: 0,
    retweets: 0,
    tags: ['seam3']
  },
  {
    id: '27290064191',
    created: 1287014993000,
    type: 'post',
    text: 'I just browsed the tags in an SVN repository and I swore I saw "before-drinking". Turns out I was seeing things, but that would be funny.',
    likes: 0,
    retweets: 0
  },
  {
    id: '27285944240',
    created: 1287011883000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> @brianleathem If you got to the page with the slides, I think that means you registered. (Rolls eyes)<br><br>In reply to: <a href="https://x.com/JohnAment/status/27274113938" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">27274113938</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27278577975',
    created: 1287005912000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> @brianleathem 1 hour. It will be a pretty multi-faceted talk, so the technical bits won\'t be super technical.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/27273205604" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">27273205604</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27259176391',
    created: 1286989078000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/derkdukker" rel="noopener noreferrer" target="_blank">@derkdukker</a> There\'s no #seam3 core. It\'s all based on #jsr299 (CDI). There\'s conversation support there, but JSF controls are in Seam Faces.<br><br>In reply to: <a href="https://x.com/derkdukker/status/27258139872" rel="noopener noreferrer" target="_blank">@derkdukker</a> <span class="status">27258139872</span>',
    likes: 0,
    retweets: 0,
    tags: ['seam3', 'jsr299']
  },
  {
    id: '27250304874',
    created: 1286982829000,
    type: 'post',
    text: 'I\'ll be giving a webinar on #JavaEE6 &amp; #CDI Thurs, Oct 14 @ UTC - 4 (12:00 EST). I\'ll also delve into #Seam3. Register: <a href="http://www-waa-akam.thomson-webcast.net/us/dispatching/?event_id=1a0c6f59bf1e233f7a388ce7995c903c&amp;portal_id=af9b227bf07c733390c2738ee0330646" rel="noopener noreferrer" target="_blank">www-waa-akam.thomson-webcast.net/us/dispatching/?event_id=1a0c6f59bf1e233f7a388ce7995c903c&amp;portal_id=af9b227bf07c733390c2738ee0330646</a>',
    likes: 0,
    retweets: 7,
    tags: ['javaee6', 'cdi', 'seam3']
  },
  {
    id: '27211670593',
    created: 1286944637000,
    type: 'post',
    text: 'Check out Sibilla, newest member of the JBoss Testing Laboratory. <a href="http://www.dzone.com/links/sibilla_first_release_experince_a_new_approach_to.html" rel="noopener noreferrer" target="_blank">www.dzone.com/links/sibilla_first_release_experince_a_new_approach_to.html</a> Experiments with #Arquilian integration underway.',
    likes: 0,
    retweets: 3,
    tags: ['arquilian']
  },
  {
    id: '27201831187',
    created: 1286937553000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/stuartwdouglas" rel="noopener noreferrer" target="_blank">@stuartwdouglas</a> I regret to say I\'ve been the backup in the system w/ this switch to git. We don\'t have much left to do, might ping you.<br><br>In reply to: <a href="https://x.com/stuartwdouglas/status/26829250423" rel="noopener noreferrer" target="_blank">@stuartwdouglas</a> <span class="status">26829250423</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27201588487',
    created: 1286937396000,
    type: 'reply',
    text: '@brianleathem <a class="mention" href="https://x.com/jbossasylum" rel="noopener noreferrer" target="_blank">@jbossasylum</a> Sure. In fact, we *want* to encourage a CDI extension ecosystem. #Seam is just our contribution to it.<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '27201338007',
    created: 1286937219000,
    type: 'reply',
    text: '@derekmdcom DBUnit man. There\'s an AbstractDBUnitSeamTest you can extend.<br><br>In reply to: @derekmdcom <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27201189736',
    created: 1286937116000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> @brianleathem <a class="mention" href="https://x.com/jbossasylum" rel="noopener noreferrer" target="_blank">@jbossasylum</a> Switching #Seam to git is eminent. I just need to git it done ;)<br><br>In reply to: <a href="https://x.com/lightguardjp/status/27191781782" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">27191781782</span>',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '27176356999',
    created: 1286919932000,
    type: 'post',
    text: '#Weld Extensions now has a sizable reference guide <a href="http://docs.jboss.org/weld/extensions/reference/latest/en-US/html/" rel="noopener noreferrer" target="_blank">docs.jboss.org/weld/extensions/reference/latest/en-US/html/</a> &amp; comprehensive API docs <a href="http://docs.jboss.org/weld/extensions/apidocs/latest/" rel="noopener noreferrer" target="_blank">docs.jboss.org/weld/extensions/apidocs/latest/</a> Check it out!',
    likes: 2,
    retweets: 1,
    tags: ['weld']
  },
  {
    id: '27176211721',
    created: 1286919802000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> @brianleathem Better yet, join in! Swing by #seam-dev if you need help building Seam 3 modules.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/27169875063" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">27169875063</span>',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '27176099540',
    created: 1286919702000,
    type: 'post',
    text: 'There were 18,000 downloads of #Weld from sf.net last month. That doesn\'t include Maven downloads, so safe to say ~50,000 total?',
    likes: 0,
    retweets: 1,
    tags: ['weld']
  },
  {
    id: '27176024504',
    created: 1286919636000,
    type: 'post',
    text: 'If you are reporting a bug in #Weld, please know the workflow as it related to the EE containers (GlassFish &amp; JBoss AS) <a href="http://seamframework.org/Weld/Home#H-ReportingBugsInWeld" rel="noopener noreferrer" target="_blank">seamframework.org/Weld/Home#H-ReportingBugsInWeld</a>',
    likes: 0,
    retweets: 2,
    tags: ['weld']
  },
  {
    id: '27168275148',
    created: 1286912701000,
    type: 'post',
    text: '#Weld Extensions is a portable library (ASL 2) that provides utilities &amp; common functionality for CDI applications and/or extensions.',
    likes: 1,
    retweets: 1,
    tags: ['weld']
  },
  {
    id: '27168186122',
    created: 1286912621000,
    type: 'post',
    text: 'Hold the music. #Weld Extensions 1.0.0.Beta1 is available! ...and there was much rejoicing. <a href="https://in.relation.to/Bloggers/WeldExtensions100Beta1Available" rel="noopener noreferrer" target="_blank">in.relation.to/Bloggers/WeldExtensions100Beta1Available</a>',
    likes: 0,
    retweets: 5,
    tags: ['weld']
  },
  {
    id: '27109154700',
    created: 1286858562000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/steveebersole" rel="noopener noreferrer" target="_blank">@steveebersole</a> Now that\'s something worth cheering about (as you are in your avatar).<br><br>In reply to: <a href="https://x.com/steveebersole/status/27072335433" rel="noopener noreferrer" target="_blank">@steveebersole</a> <span class="status">27072335433</span>',
    likes: 0,
    retweets: 1
  },
  {
    id: '27107915544',
    created: 1286857465000,
    type: 'post',
    text: 'The start of something beautiful - linking #Arquillian test cases to issue reports: <a href="https://jira.jboss.org/browse/WELD-662" rel="noopener noreferrer" target="_blank">jira.jboss.org/browse/WELD-662</a>',
    likes: 1,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '27074383837',
    created: 1286836084000,
    type: 'post',
    text: 'When I really need to get things done, I end up getting stuck on the first thing all day. Wheels stuck in mud.',
    likes: 1,
    retweets: 0
  },
  {
    id: '27004646188',
    created: 1286772419000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MStine" rel="noopener noreferrer" target="_blank">@MStine</a> +1000 Templates just mean transcribing my well-prepared presentations into something less well-prepared == Downgrade.<br><br>In reply to: @mstine <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '27004561976',
    created: 1286772339000,
    type: 'post',
    text: 'I decided on the #Oakley polarized lenses after spending a month w/ "trial pair". LCD screens look wacky, but the protection is essential.',
    likes: 0,
    retweets: 0,
    tags: ['oakley']
  },
  {
    id: '27004448566',
    created: 1286772232000,
    type: 'post',
    text: 'Just ordered custom #Oakley Straight Jackets. Color? Polished black w/ red logo. An extra surprise to be revealed once I get them.',
    likes: 0,
    retweets: 0,
    tags: ['oakley']
  },
  {
    id: '27003285484',
    created: 1286771157000,
    type: 'post',
    text: 'Congratulations to <a class="mention" href="https://x.com/AdamBien" rel="noopener noreferrer" target="_blank">@AdamBien</a> for being awarded Java Developer of the Year 2010 <a href="https://in.relation.to/2010/10/11/adam-bien-java-developer-of-the-year-2010/" rel="noopener noreferrer" target="_blank">in.relation.to/2010/10/11/adam-bien-java-developer-of-the-year-2010/</a>',
    likes: 0,
    retweets: 3
  },
  {
    id: '26864994882',
    created: 1286647724000,
    type: 'post',
    text: 'What\'s tough about being a speaker &amp; writer is that you don\'t have much time hack. So sometimes I just need to disappear to hack in peace.',
    likes: 0,
    retweets: 0
  },
  {
    id: '26714781767',
    created: 1286508902000,
    type: 'post',
    text: 'Sort of a strange feeling when you receive an e-mail announcing a webinar that you\'ll be giving. That\'s the other me :)',
    likes: 1,
    retweets: 0
  },
  {
    id: '26679056317',
    created: 1286480187000,
    type: 'post',
    text: 'Hint, to generate a site report with Maven 3, use Maven 2 :)',
    likes: 0,
    retweets: 0
  },
  {
    id: '26678801874',
    created: 1286479958000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/steveebersole" rel="noopener noreferrer" target="_blank">@steveebersole</a> Hahaha. Good point. Though, it\'s fair to appreciate the design team\'s work despite IT not keeping up w/ demands.<br><br>In reply to: <a href="https://x.com/steveebersole/status/26677517307" rel="noopener noreferrer" target="_blank">@steveebersole</a> <span class="status">26677517307</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '26670329744',
    created: 1286472333000,
    type: 'post',
    text: '#Arquillian was mentioned at two separate conferences on the same day. That has to be some sort of milestone for a project ;)',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '26662297251',
    created: 1286467883000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SanneGrinovero" rel="noopener noreferrer" target="_blank">@SanneGrinovero</a> More like clone and *send pull request" Probably what you intended to say :)<br><br>In reply to: <a href="https://x.com/SanneGrinovero/status/26657422400" rel="noopener noreferrer" target="_blank">@SanneGrinovero</a> <span class="status">26657422400</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '26629592508',
    created: 1286438192000,
    type: 'post',
    text: 'Created a diigo group to track CDI articles. We\'ll see where it goes. <a href="http://groups.diigo.com/group/cdi-tech-hub" rel="noopener noreferrer" target="_blank">groups.diigo.com/group/cdi-tech-hub</a>',
    likes: 3,
    retweets: 1
  },
  {
    id: '26629228804',
    created: 1286437674000,
    type: 'post',
    text: 'Great article on #EJB 3.1 &amp; #CDI synergy by <a class="mention" href="https://x.com/AdamBien" rel="noopener noreferrer" target="_blank">@AdamBien</a> <a href="http://www.oracle.com/technetwork/articles/java/ejb-3-1-175064.html" rel="noopener noreferrer" target="_blank">www.oracle.com/technetwork/articles/java/ejb-3-1-175064.html</a> Does excellent job showing CDI is clear &amp; simple.',
    likes: 4,
    retweets: 5,
    tags: ['ejb', 'cdi']
  },
  {
    id: '26624277400',
    created: 1286430980000,
    type: 'post',
    text: 'I\'m addicted to creating #Arquillian tests to explore various container features &amp; compare containers. If I had this 10 years ago...',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '26624178768',
    created: 1286430854000,
    type: 'post',
    text: 'I just wrote an #Arquillian test that demos using 2 JPA providers in the same application and as part of a 2 phase commit. So crazy.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '26594278372',
    created: 1286406739000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mrblog" rel="noopener noreferrer" target="_blank">@mrblog</a> Now that\'s a neat idea ;) It sends the message "we\'ll decide what security is best for us, thanks".<br><br>In reply to: <a href="https://x.com/mrblog/status/26589111700" rel="noopener noreferrer" target="_blank">@mrblog</a> <span class="status">26589111700</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '26584127624',
    created: 1286398452000,
    type: 'post',
    text: 'When you introduce a container-managed component type w/o a post-construct hook, it results in logic being performed in setter methods.',
    likes: 1,
    retweets: 0
  },
  {
    id: '26583655049',
    created: 1286398041000,
    type: 'post',
    text: '"As of Sep 30, customers can\'t use their <br>Bank of America credit card to pay merchants through online Bill Pay." Can\'t throw debt @ debt.',
    likes: 0,
    retweets: 0
  },
  {
    id: '26582115671',
    created: 1286396656000,
    type: 'post',
    text: 'jira.jboss.org gets a new splash image. I like it!',
    likes: 0,
    retweets: 1
  },
  {
    id: '26579214079',
    created: 1286393994000,
    type: 'reply',
    text: '@chuggid <a class="mention" href="https://x.com/glassfish" rel="noopener noreferrer" target="_blank">@glassfish</a> We certainly do strive be ubiquitous. Weld is the CDI impl in GlassFish after all. Do you mean #Seam module portability?<br><br>In reply to: @chuggid <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '26579075329',
    created: 1286393861000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Hmm. Well, I\'m glad I\'ve waited, at least until all the facts are sorted out. I\'m certainly against violation of openness.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '26578554695',
    created: 1286393373000,
    type: 'post',
    text: '"The CDI reference implementation is really becoming a water shed moment for enterprise developers." -- Andy Miller #Weld #jsr299',
    likes: 0,
    retweets: 2,
    tags: ['weld', 'jsr299']
  },
  {
    id: '26578053654',
    created: 1286392900000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> <a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Last I checked, you can\'t. I pushed everything aside and got my backup script together before I hit 3000.<br><br>In reply to: <a href="https://x.com/metacosm/status/26574172743" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">26574172743</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '26518137450',
    created: 1286335136000,
    type: 'post',
    text: '-rf is the command flag for #Maven\'s resume feature. In shell that\'s recursive &amp; force. Better not daydream &amp; lead w/ rm instead of mvn.',
    likes: 0,
    retweets: 0,
    tags: ['maven']
  },
  {
    id: '26472958097',
    created: 1286297308000,
    type: 'post',
    text: 'I\'m glad to see that blog comments are finally working on jboss.org. You wouldn\'t think that would be such a big deal. Believe it ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '26467704107',
    created: 1286293378000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> Oh good! One of my favorite topics in CDI ;)<br><br>In reply to: <a href="https://x.com/arungupta/status/26467514206" rel="noopener noreferrer" target="_blank">@arungupta</a> <span class="status">26467514206</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '26464725320',
    created: 1286291305000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> Cool! Another valuable TOTD!<br><br>In reply to: <a href="https://x.com/arungupta/status/26452110907" rel="noopener noreferrer" target="_blank">@arungupta</a> <span class="status">26452110907</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '26435162779',
    created: 1286261772000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/edburns" rel="noopener noreferrer" target="_blank">@edburns</a> Sucks when the end result is the status quo. If I had lazy days, I would actually improve the script.<br><br>In reply to: <a href="https://x.com/edburns/status/26435101405" rel="noopener noreferrer" target="_blank">@edburns</a> <span class="status">26435101405</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '26434698360',
    created: 1286261083000,
    type: 'post',
    text: 'Damn you twitter. You\'re forcing me to learn OAuth just to keep my hacky backup script running.',
    likes: 0,
    retweets: 0
  },
  {
    id: '26423914450',
    created: 1286248959000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> That may be one of the first recorded cases of JPA humor. And it\'s really bad :P',
    likes: 0,
    retweets: 0
  },
  {
    id: '26423844862',
    created: 1286248899000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Hahaha. He had to get those corner cases "managed", "merged" and "persisted". I needed a "refresher" myself.<br><br>In reply to: <a href="https://x.com/JohnAment/status/26403585303" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">26403585303</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '26403406985',
    created: 1286233357000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/germanescobar" rel="noopener noreferrer" target="_blank">@germanescobar</a> Thanks for the fix. Glad you like it. It\'s been brewing in my head for months. Finally, I threw it down!<br><br>In reply to: <a href="https://x.com/germanescobar/status/26401404962" rel="noopener noreferrer" target="_blank">@germanescobar</a> <span class="status">26401404962</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '26403102229',
    created: 1286233118000,
    type: 'post',
    text: 'Blogging using jboss.org makes me stabby (as TheBloggess would say).',
    likes: 0,
    retweets: 0
  },
  {
    id: '26402932666',
    created: 1286232983000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/crichardson" rel="noopener noreferrer" target="_blank">@crichardson</a> But yes, you are correct that we don\'t want to make things harder then they were ;) Recipes can be refined.<br><br>In reply to: <a href="https://x.com/crichardson/status/26401742482" rel="noopener noreferrer" target="_blank">@crichardson</a> <span class="status">26401742482</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '26402699806',
    created: 1286232799000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/crichardson" rel="noopener noreferrer" target="_blank">@crichardson</a> And the benefit of choosing to is to see how it behaves in the real container, with the real JPA provider ;)<br><br>In reply to: <a href="https://x.com/crichardson/status/26401742482" rel="noopener noreferrer" target="_blank">@crichardson</a> <span class="status">26401742482</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '26402626407',
    created: 1286232741000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/crichardson" rel="noopener noreferrer" target="_blank">@crichardson</a> You don\'t. You can just choose to. Check out the non-packaging variant: <a href="https://github.com/arquillian/arquillian-examples/tree/master/jpalab/" rel="noopener noreferrer" target="_blank">github.com/arquillian/arquillian-examples/tree/master/jpalab/</a> It\'s also about classpath control.<br><br>In reply to: <a href="https://x.com/crichardson/status/26401742482" rel="noopener noreferrer" target="_blank">@crichardson</a> <span class="status">26401742482</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '26402485583',
    created: 1286232631000,
    type: 'post',
    text: 'I created a JPA lab to help <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> master persistence context behavior. Now for anyone to study. <a href="https://github.com/arquillian/arquillian-examples/tree/master/jpalab/" rel="noopener noreferrer" target="_blank">github.com/arquillian/arquillian-examples/tree/master/jpalab/</a> #Arquillian',
    likes: 1,
    retweets: 6,
    tags: ['arquillian']
  },
  {
    id: '26399048958',
    created: 1286229831000,
    type: 'post',
    text: 'JBoss Developer Conference (Berlin Oct 7-8) <em>&lt;expired link&gt;</em> #JBoss #JUDCon #Arquillian Come spin code.',
    likes: 0,
    retweets: 0,
    tags: ['jboss', 'judcon', 'arquillian']
  },
  {
    id: '26396858400',
    created: 1286227945000,
    type: 'post',
    text: '"CDI: The Natural Complement of EJB 3.1" Indeed. <a href="http://www.oracle.com/technetwork/articles/java/ejb-3-1-175064.html" rel="noopener noreferrer" target="_blank">www.oracle.com/technetwork/articles/java/ejb-3-1-175064.html</a> #jsr299',
    likes: 1,
    retweets: 7,
    tags: ['jsr299']
  },
  {
    id: '26394890814',
    created: 1286226164000,
    type: 'post',
    text: 'Just published "The perfect recipe for testing JPA 2: revisited" <a href="https://developer.jboss.org/en/arquillian/blog/2010/10/04/the-perfect-recipe-for-testing-jpa-2-revisited" rel="noopener noreferrer" target="_blank">developer.jboss.org/en/arquillian/blog/2010/10/04/the-perfect-recipe-for-testing-jpa-2-revisited</a> #Arquillian',
    likes: 4,
    retweets: 5,
    tags: ['arquillian']
  },
  {
    id: '26382279277',
    created: 1286213508000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> I should mention the #JSR-303 (Bean Validation) spec is also available in HTML <a href="http://people.redhat.com/~ebernard/validation/" rel="noopener noreferrer" target="_blank">people.redhat.com/~ebernard/validation/</a><br><br>In reply to: <a href="https://x.com/jeremynorris/status/26378905702" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">26378905702</span>',
    likes: 0,
    retweets: 1,
    tags: ['jsr']
  },
  {
    id: '26381141958',
    created: 1286212558000,
    type: 'post',
    text: 'Just finished writing another a tutorial for using #Arquillian to test JPA code. It should be up soon.',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '26373659031',
    created: 1286206886000,
    type: 'post',
    text: 'Did you know you can read the #JSR-299 spec online in HTML format? <a href="http://docs.jboss.org/cdi/spec/1.0/html/" rel="noopener noreferrer" target="_blank">docs.jboss.org/cdi/spec/1.0/html/</a>',
    likes: 1,
    retweets: 2,
    tags: ['jsr']
  },
  {
    id: '26334660830',
    created: 1286167624000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Hahaha. I think those that fear me also fear Killer Bunnies ;)<br><br>In reply to: <a href="https://x.com/lincolnthree/status/26306119965" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">26306119965</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '26303776234',
    created: 1286142564000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> It\'s rated one of the best beers in the world. Perhaps inflated by the fact it\'s so hard to find.<br><br>In reply to: <a href="https://x.com/metacosm/status/26299871875" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">26299871875</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '26303529822',
    created: 1286142351000,
    type: 'post',
    text: 'Apparently I\'m an "archetypal angry guy". <a href="http://www.jroller.com/robwilliams/entry/spring_v_redhat_v_oracle" rel="noopener noreferrer" target="_blank">www.jroller.com/robwilliams/entry/spring_v_redhat_v_oracle</a> Hmm maybe he\'s using reverse psychology to toughen me up :)',
    likes: 0,
    retweets: 0
  },
  {
    id: '26299866314',
    created: 1286139161000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/marekgoldmann" rel="noopener noreferrer" target="_blank">@marekgoldmann</a> Make sure there are refreshments this time so the hackfest actually happens.<br><br>In reply to: <a href="https://x.com/maxandersen/status/26292302379" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">26292302379</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '26299751470',
    created: 1286139060000,
    type: 'post',
    text: 'With &gt;1 week in Belgium for #Devoxx, I have a chance to find my grail, #Westvleteren. If anyone can get their hands on it for me == Hero.',
    likes: 0,
    retweets: 1,
    tags: ['devoxx', 'westvleteren']
  },
  {
    id: '26299134349',
    created: 1286138515000,
    type: 'post',
    text: 'I must admit I\'m getting pretty spoiled by m2eclipse downloading &amp; attaching sources for me when I open a class file. Out of the darkness!',
    likes: 1,
    retweets: 2
  },
  {
    id: '26298681001',
    created: 1286138125000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jganoff" rel="noopener noreferrer" target="_blank">@jganoff</a> The question is, then, with #Swype do you really need the slide out keyboard? I use it for every tweet from my phone.<br><br>In reply to: <a href="https://x.com/jganoff/status/26261958702" rel="noopener noreferrer" target="_blank">@jganoff</a> <span class="status">26261958702</span>',
    likes: 0,
    retweets: 0,
    tags: ['swype']
  },
  {
    id: '26244124475',
    created: 1286087124000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dzone" rel="noopener noreferrer" target="_blank">@dzone</a> You need to update that guide to include the #G2. It\'s going to be a player.<br><br>In reply to: <a href="https://x.com/DZoneInc/status/26236517904" rel="noopener noreferrer" target="_blank">@DZoneInc</a> <span class="status">26236517904</span>',
    likes: 0,
    retweets: 0,
    tags: ['g2']
  },
  {
    id: '26244089313',
    created: 1286087085000,
    type: 'post',
    text: 'I played with the Swipe UI keyboard for #Android for the first time. Amazing concept. My inner productivity is craving it.',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '26243953794',
    created: 1286086932000,
    type: 'post',
    text: 'I had a chance to toy around w/ an early release #G2 tonight at a #t-mobile kiosk at the mall. Where have you been all my life? Sweet.',
    likes: 0,
    retweets: 0,
    tags: ['g2', 't']
  },
  {
    id: '26209340772',
    created: 1286055754000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> Now you can pit the JPA providers against one another and see whose bugs are showing :)<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/26186254188" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">26186254188</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '26120349081',
    created: 1285971806000,
    type: 'post',
    text: 'There\'s at least 1 thing #JavaOne did very right. Not allowing slides @ BOFs. Now need to put speakers down on the floor w/ the people.',
    likes: 1,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '26120093593',
    created: 1285971580000,
    type: 'post',
    text: 'Plans all set for #Devoxx. I\'m going to be there from start to finish. What can I say, it\'s the best time of year (professionally).',
    likes: 0,
    retweets: 3,
    tags: ['devoxx']
  },
  {
    id: '26023478216',
    created: 1285887976000,
    type: 'post',
    text: 'Post-JavaOne 2010 blog: Throwing complexity over the Pacific Coast <a href="http://community.jboss.org/en/arquillian/blog/2010/09/30/throwing-complexity-over-the-pacific-coast" rel="noopener noreferrer" target="_blank">community.jboss.org/en/arquillian/blog/2010/09/30/throwing-complexity-over-the-pacific-coast</a> #Arquillian #ShrinkWrap #JBoss',
    likes: 1,
    retweets: 0,
    tags: ['arquillian', 'shrinkwrap', 'jboss']
  },
  {
    id: '26018056602',
    created: 1285883612000,
    type: 'post',
    text: 'A picture of my cousin and son won a family vacation photo contest. <a href="http://photos.parents.com/category/vote/id/15/w/38/y/2010?page=1" rel="noopener noreferrer" target="_blank">photos.parents.com/category/vote/id/15/w/38/y/2010?page=1</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '26004513177',
    created: 1285871704000,
    type: 'post',
    text: 'I\'ve discovered a passion for blues. Sing it, yeah.',
    likes: 0,
    retweets: 0
  },
  {
    id: '25996842794',
    created: 1285865412000,
    type: 'reply',
    text: '@mwessendorf No annoyed, but when I look up, the day is over. So yes, I get stuff that is neglected done w/o e-mail coming in.<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25991146619',
    created: 1285861406000,
    type: 'post',
    text: '@brianleathem Are you kidding? Writing tests with #Arquillian is the fun part!',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '25991051177',
    created: 1285861342000,
    type: 'reply',
    text: '@mwessendorf That would actually solve some of my problems :)<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25983534685',
    created: 1285856435000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mertcal" rel="noopener noreferrer" target="_blank">@mertcal</a> Great! I\'m so glad to hear that. It\'s like a little slice of heaven just outside of a great city ;) Looking forward to the pics.<br><br>In reply to: @mertcal <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25950807537',
    created: 1285821585000,
    type: 'post',
    text: 'Sweet, the twitter web UI now has autocomplete for screen names! No more mis-mentions ;)',
    likes: 0,
    retweets: 1
  },
  {
    id: '25950751735',
    created: 1285821526000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brockm" rel="noopener noreferrer" target="_blank">@brockm</a> My idea was to have <a class="mention" href="https://x.com/runwith" rel="noopener noreferrer" target="_blank">@runwith</a>(ARQUILLIAN) rather than <a class="mention" href="https://x.com/runwith" rel="noopener noreferrer" target="_blank">@runwith</a>(Arquillian.class). Oh well.<br><br>In reply to: <a href="https://x.com/brockm/status/25945559219" rel="noopener noreferrer" target="_blank">@brockm</a> <span class="status">25945559219</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25950730625',
    created: 1285821504000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brockm" rel="noopener noreferrer" target="_blank">@brockm</a> Ah, so the only reason the primitive types work in annotations is because the values are literals.<br><br>In reply to: <a href="https://x.com/brockm/status/25945559219" rel="noopener noreferrer" target="_blank">@brockm</a> <span class="status">25945559219</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25947751274',
    created: 1285818628000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Two integrations are better than one :)<br><br>In reply to: <a href="https://x.com/lightguardjp/status/25942359465" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">25942359465</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25943898736',
    created: 1285815423000,
    type: 'post',
    text: 'Is it true that a Java annotation attribute of type Class cannot accept a static final variable? (All other types work)',
    likes: 0,
    retweets: 0
  },
  {
    id: '25942045282',
    created: 1285813986000,
    type: 'post',
    text: 'I\'m introducing Pete and David in hopes we can get an OpenEJB + Weld integration. That would be a powerful combo for #Arquillian users.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '25927968102',
    created: 1285803408000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dzone" rel="noopener noreferrer" target="_blank">@dzone</a> You can see an example of Embedded Jetty in the #Arquillian container implementation.<br><br>In reply to: <a href="https://x.com/DZoneInc/status/25919788863" rel="noopener noreferrer" target="_blank">@DZoneInc</a> <span class="status">25919788863</span>',
    likes: 1,
    retweets: 2,
    tags: ['arquillian']
  },
  {
    id: '25927787756',
    created: 1285803272000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Stephan007" rel="noopener noreferrer" target="_blank">@Stephan007</a> Fascinating. A timely sequel to Brian Cox\'s Wonders of the Universe Series.<br><br>In reply to: <a href="https://x.com/Stephan007/status/25919982403" rel="noopener noreferrer" target="_blank">@Stephan007</a> <span class="status">25919982403</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25923285621',
    created: 1285799664000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jankeesvanandel" rel="noopener noreferrer" target="_blank">@jankeesvanandel</a> If things go as planned, JBoss folk will be primarily in the Astoria hotel short walk from central station.<br><br>In reply to: <a href="https://x.com/jankeesvanandel/status/25914915979" rel="noopener noreferrer" target="_blank">@jankeesvanandel</a> <span class="status">25914915979</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25918115233',
    created: 1285795195000,
    type: 'reply',
    text: '@brianleathem Yes, this is a critical refactoring that\'s in the works. Containers will use a protocol abstraction instead of assuming.<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25918016893',
    created: 1285795107000,
    type: 'post',
    text: 'Please don\'t put http:// in a text field where you want me to enter a URL. It doesn\'t help me. It\'s just something I have to delete.',
    likes: 0,
    retweets: 0
  },
  {
    id: '25915069379',
    created: 1285792453000,
    type: 'post',
    text: 'Hudson Blues posted to DZone. Me passing the time on a long plane ride. <a href="http://www.dzone.com/links/hudson_blues_every_test_is_broken.html" rel="noopener noreferrer" target="_blank">www.dzone.com/links/hudson_blues_every_test_is_broken.html</a>',
    likes: 0,
    retweets: 1
  },
  {
    id: '25914547342',
    created: 1285791984000,
    type: 'post',
    text: 'devtownstation.com is a great start for a conference calendar, except it\'s missing most major conferences.',
    likes: 0,
    retweets: 0
  },
  {
    id: '25914087544',
    created: 1285791577000,
    type: 'post',
    text: 'I would be totally interested in subscribing to a mailinglist announcing call for papers for conferences rather than e-mail blasts.',
    likes: 0,
    retweets: 0
  },
  {
    id: '25907937270',
    created: 1285785995000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jankeesvanandel" rel="noopener noreferrer" target="_blank">@jankeesvanandel</a> I look forward to seeing you there. It should be easier to chat than 50 yds from the stage at a Steve Miller concert.<br><br>In reply to: <a href="https://x.com/jankeesvanandel/status/25785707332" rel="noopener noreferrer" target="_blank">@jankeesvanandel</a> <span class="status">25785707332</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25907648686',
    created: 1285785729000,
    type: 'post',
    text: 'I just noticed the new twitter web UI shows the real name next to the screen name. Less cryptic, but less anonymous.',
    likes: 0,
    retweets: 0
  },
  {
    id: '25821630119',
    created: 1285709283000,
    type: 'post',
    text: 'It appears many of the presentations are not linked correctly in the OpenWorld portal. Getting some garbage downloads.',
    likes: 0,
    retweets: 0
  },
  {
    id: '25821290083',
    created: 1285709013000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AdamBien" rel="noopener noreferrer" target="_blank">@AdamBien</a> Haha, that\'s because the purpose of the account is to report outage. Perhaps not so aptly named ;)<br><br>In reply to: <a href="https://x.com/AdamBien/status/25820172115" rel="noopener noreferrer" target="_blank">@AdamBien</a> <span class="status">25820172115</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25821205404',
    created: 1285708947000,
    type: 'post',
    text: '#Facebook, I am going to break your neck if you ask me the name of my computer one more time.',
    likes: 0,
    retweets: 1,
    tags: ['facebook']
  },
  {
    id: '25818713104',
    created: 1285706867000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/javajuneau" rel="noopener noreferrer" target="_blank">@javajuneau</a> Super. I\'ll have to check that out since the audio is available.<br><br>In reply to: <a href="https://x.com/javajuneau/status/25814395200" rel="noopener noreferrer" target="_blank">@javajuneau</a> <span class="status">25814395200</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25818674230',
    created: 1285706832000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matkar" rel="noopener noreferrer" target="_blank">@matkar</a> I finally submitted my proposals for jFokus. #Arquillian, #Seam, #JSR299, #RichFaces, #Metawidget<br><br>In reply to: <a href="https://x.com/matkar/status/25799872553" rel="noopener noreferrer" target="_blank">@matkar</a> <span class="status">25799872553</span>',
    likes: 0,
    retweets: 2,
    tags: ['arquillian', 'seam', 'jsr299', 'richfaces', 'metawidget']
  },
  {
    id: '25814075788',
    created: 1285702950000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexismp" rel="noopener noreferrer" target="_blank">@alexismp</a> <a class="mention" href="https://x.com/javajuneau" rel="noopener noreferrer" target="_blank">@javajuneau</a> Surely CDI would be a key factor in that list, especially if you use EJB and JSF in the same sentence.<br><br>In reply to: <a href="https://x.com/alexismp/status/25811564401" rel="noopener noreferrer" target="_blank">@alexismp</a> <span class="status">25811564401</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25813634884',
    created: 1285702568000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/github" rel="noopener noreferrer" target="_blank">@github</a> What\'s funny is that few people probably noticed it. That\'s the beauty of distributed VCS :)<br><br>In reply to: <a href="https://x.com/github/status/25806818349" rel="noopener noreferrer" target="_blank">@github</a> <span class="status">25806818349</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25809554863',
    created: 1285699107000,
    type: 'post',
    text: 'The only time I look at my desktop is after my browser crashes. Hello desktop.',
    likes: 0,
    retweets: 1
  },
  {
    id: '25806875556',
    created: 1285696743000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matkar" rel="noopener noreferrer" target="_blank">@matkar</a> Doing it right now (finally).<br><br>In reply to: <a href="https://x.com/matkar/status/25799872553" rel="noopener noreferrer" target="_blank">@matkar</a> <span class="status">25799872553</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25795352498',
    created: 1285688077000,
    type: 'post',
    text: 'As if we didn\'t get enough IronMan 2 at #javaone, you can rent it at redbox. Hmm, I might defer that one :)',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '25757138439',
    created: 1285649433000,
    type: 'post',
    text: 'Follow <a class="mention" href="https://x.com/NFJSMag" rel="noopener noreferrer" target="_blank">@NFJSMag</a> for news about #NFJS magazine. All tour authors.',
    likes: 0,
    retweets: 0,
    tags: ['nfjs']
  },
  {
    id: '25757012592',
    created: 1285649298000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexismp" rel="noopener noreferrer" target="_blank">@alexismp</a> I\'m never worried about the quality of content at #javaone. Quality will be somewhere. The challenge is being in the right place.<br><br>In reply to: <a href="https://x.com/alexismp/status/25682375428" rel="noopener noreferrer" target="_blank">@alexismp</a> <span class="status">25682375428</span>',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '25756908131',
    created: 1285649187000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexismp" rel="noopener noreferrer" target="_blank">@alexismp</a> Though search queries fit more with a conversation than does a cart.<br><br>In reply to: <a href="https://x.com/alexismp/status/25618671122" rel="noopener noreferrer" target="_blank">@alexismp</a> <span class="status">25618671122</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25756884472',
    created: 1285649162000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexismp" rel="noopener noreferrer" target="_blank">@alexismp</a> It really depends on the requirements. If you expect to be able to manage multiple carts in parallel, then conversation fits.<br><br>In reply to: <a href="https://x.com/alexismp/status/25618671122" rel="noopener noreferrer" target="_blank">@alexismp</a> <span class="status">25618671122</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25755111766',
    created: 1285647373000,
    type: 'post',
    text: 'Hudson\'s down, and it\'s singin\' the blues! <a href="http://community.jboss.org/en/arquillian/blog/2010/09/28/hudson-blues" rel="noopener noreferrer" target="_blank">community.jboss.org/en/arquillian/blog/2010/09/28/hudson-blues</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25749714593',
    created: 1285642856000,
    type: 'post',
    text: 'If you attended #javaone, you can get the audio for just about any session. <a href="http://openworld.vportal.net" rel="noopener noreferrer" target="_blank">openworld.vportal.net</a> Go learn!',
    likes: 0,
    retweets: 2,
    tags: ['javaone']
  },
  {
    id: '25735180516',
    created: 1285632195000,
    type: 'post',
    text: '#NFJS magazine is worth reading. Sept 2010 issue is available, includes abstract for #Arquillian article coming in Oct 2010 issue.',
    likes: 0,
    retweets: 0,
    tags: ['nfjs', 'arquillian']
  },
  {
    id: '25725584923',
    created: 1285625384000,
    type: 'reply',
    text: '@mwessendorf It\'s a miracle <a class="mention" href="https://x.com/aliok_tr" rel="noopener noreferrer" target="_blank">@aliok_tr</a> made it to the talk after a late (fun) night at TI :)<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25725475482',
    created: 1285625295000,
    type: 'post',
    text: 'Hello airline. I\'d like to use my "0 miles upgrade". Is that possible?',
    likes: 0,
    retweets: 0
  },
  {
    id: '25705871273',
    created: 1285608206000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maiko_rocha" rel="noopener noreferrer" target="_blank">@maiko_rocha</a> It\'s mostly the moaning from non-tech folks that bother me. They quickly jump to conspiracy theory talk.<br><br>In reply to: <a href="https://x.com/maiko_rocha/status/25696977816" rel="noopener noreferrer" target="_blank">@maiko_rocha</a> <span class="status">25696977816</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25705711992',
    created: 1285608072000,
    type: 'post',
    text: 'I wish #Oracle would show the Duke\'s Choice Award nominees. And seriously, that log appender pick was lame. Heck, it\'s a year out of date.',
    likes: 0,
    retweets: 0,
    tags: ['oracle']
  },
  {
    id: '25705562966',
    created: 1285607949000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MStine" rel="noopener noreferrer" target="_blank">@MStine</a> I heard about it on the #nfjs tour. You were bound to hear about it soon enough :) Would make a good #Arquillian integration.<br><br>In reply to: @mstine <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['nfjs', 'arquillian']
  },
  {
    id: '25695687641',
    created: 1285600704000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> But we do need to get a piano in Moscone for <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> and other keyboard talents.',
    likes: 0,
    retweets: 0
  },
  {
    id: '25695585087',
    created: 1285600635000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> It was for the whole kaboddle. I\'d like to see it back in Moscone. Not a fan of the hotel venue. Too many corners.<br><br>In reply to: @pelegri <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25695401160',
    created: 1285600510000,
    type: 'post',
    text: 'I hate listening to people complain about virus software screwing up their Windows box. Please make it stop. Isn\'t the weather nice?',
    likes: 0,
    retweets: 1
  },
  {
    id: '25688411315',
    created: 1285595788000,
    type: 'post',
    text: 'Just submitted by post-JavaOne survey. I\'ve always said, the event is about the networking and just a carefully selected handful of talks.',
    likes: 0,
    retweets: 0
  },
  {
    id: '25687489139',
    created: 1285595122000,
    type: 'post',
    text: 'Time to go grab JBoss AS 6.0.0.M5. Look for it in the JBoss Nexus repo for easy download org.jboss.jbossas:jbossas-distribution',
    likes: 0,
    retweets: 0
  },
  {
    id: '25687316157',
    created: 1285594996000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Absolutely. You think about it for just a couple minutes only to realize that the problem is inherent in the system.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/25652450524" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">25652450524</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25682700760',
    created: 1285591456000,
    type: 'post',
    text: 'Wow, #SouthWest just announced an intent to acquire #AirTran. That\'s great news since I ♥ SouthWest. And finally a flight to ATL!',
    likes: 0,
    retweets: 1,
    tags: ['southwest', 'airtran']
  },
  {
    id: '25648981795',
    created: 1285555118000,
    type: 'post',
    text: 'Dang it, why don\'t frameworks isolate class and resource loading cleanly from the thread classloader so we can control what they find?',
    likes: 0,
    retweets: 1
  },
  {
    id: '25641282910',
    created: 1285549420000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I also made enhancements that should benefit other embedded containers. Still room for improvement.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/25631914951" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">25631914951</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25641230300',
    created: 1285549381000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> The classpath is what you put in the ShrinkWrap archive. That\'s one benefit of testing a Spring app w/ Arquillian.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/25631914951" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">25631914951</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25641167223',
    created: 1285549335000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Actually, the #Spring integration was courtesy of a long plane ride home. Then I had my conference hangover ;)<br><br>In reply to: <a href="https://x.com/ALRubinger/status/25634176080" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">25634176080</span>',
    likes: 0,
    retweets: 0,
    tags: ['spring']
  },
  {
    id: '25631843376',
    created: 1285542150000,
    type: 'post',
    text: 'The fascinating part about the #Arquillian #Spring integration is the classpath control. It\'s not perfect, but a good start.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'spring']
  },
  {
    id: '25626063206',
    created: 1285537131000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I agree though, while working on the integration, I\'m reminded how overly complex Spring has become.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/25625770425" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">25625770425</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25626040078',
    created: 1285537109000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> It\'s good for #Arquillian as it challenges assumptions/proves extensibility. +, 1st programming model container doesn\'t have.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/25625770425" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">25625770425</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '25625709413',
    created: 1285536807000,
    type: 'post',
    text: 'Just dropped a prototype of the Spring integration for #Arquillian. <a href="http://community.jboss.org/message/563691" rel="noopener noreferrer" target="_blank">community.jboss.org/message/563691</a>',
    likes: 1,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '25621417165',
    created: 1285532963000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jbandi" rel="noopener noreferrer" target="_blank">@jbandi</a> I\'ve got a fix for that. The project grew quickly and we need partitioned builds that focus on usage. Stay tuned.<br><br>In reply to: <a href="https://x.com/jbandi/status/25405442135" rel="noopener noreferrer" target="_blank">@jbandi</a> <span class="status">25405442135</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25528514524',
    created: 1285446211000,
    type: 'post',
    text: 'I regret not seeing more community members @ #javaone. Balanced it w/ some in-depth conversations &amp; getting to know teammate <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a>.',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '25528101027',
    created: 1285445801000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/evanchooly" rel="noopener noreferrer" target="_blank">@evanchooly</a> You\'ve built a bridge to the JBoss community, so hopefully an offset no matter how small. Thanks for hanging w/ us!<br><br>In reply to: <a href="https://x.com/evanchooly/status/25514362108" rel="noopener noreferrer" target="_blank">@evanchooly</a> <span class="status">25514362108</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25527902603',
    created: 1285445606000,
    type: 'reply',
    text: '@mwessendorf Ah, that explains the finish line banner <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> saw before crossing the bridge. It wasn\'t our finish line though.<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25527775346',
    created: 1285445478000,
    type: 'post',
    text: '#Kindle for #Android finally has notes, highlight and search.',
    likes: 0,
    retweets: 0,
    tags: ['kindle', 'android']
  },
  {
    id: '25448963854',
    created: 1285371388000,
    type: 'reply',
    text: '@mwessendorf Shhh, the flight attendent might hear you and I\'ll be busted. hehehe ;)<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25428609102',
    created: 1285353141000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kengkaj_s" rel="noopener noreferrer" target="_blank">@kengkaj_s</a> Don\'t mistake that quote as mine. That\'s a quote from the JCP. I think the statement about JSR-331 is a load of horseshit.<br><br>In reply to: <a href="https://x.com/kengkaj_s/status/25416456280" rel="noopener noreferrer" target="_blank">@kengkaj_s</a> <span class="status">25416456280</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25424081526',
    created: 1285349334000,
    type: 'post',
    text: '"Colossal Pull" - new term for large community contributions (idea from LA Times headline)',
    photos: ['<div class="entry"><img class="photo" src="media/25424081526.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '25415092687',
    created: 1285342838000,
    type: 'post',
    text: 'Just got the pre-order e-mail for the G2 from T-mobile. Excited, but I\'m thinking I want to at least hold a demo of it first.',
    likes: 0,
    retweets: 0
  },
  {
    id: '25413240531',
    created: 1285341577000,
    type: 'post',
    text: 'Proud of the fact that I always use BART to get to and from SFO. Honestly, it\'s also the easiest option.',
    likes: 0,
    retweets: 0
  },
  {
    id: '25412223701',
    created: 1285340898000,
    type: 'post',
    text: 'Free WIFI at SFO, FTW. Likin\' these progressive cities. All we ask for is access.',
    photos: ['<div class="entry"><img class="photo" src="media/25412223701.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '25409685164',
    created: 1285339188000,
    type: 'post',
    text: 'You\'re singing <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a>\'s tune RT <a class="mention" href="https://x.com/apemberton" rel="noopener noreferrer" target="_blank">@apemberton</a> all web site URLs should end in HTML or no extension. What\'s the value in revealing impl?',
    likes: 0,
    retweets: 2
  },
  {
    id: '25409492300',
    created: 1285339054000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/codylerum" rel="noopener noreferrer" target="_blank">@codylerum</a> Cool! @emmanualbernard will be happy about your first pick, and you will be happy with the search results you\'ll unlock.<br><br>In reply to: <a href="https://x.com/codylerum/status/25408118846" rel="noopener noreferrer" target="_blank">@codylerum</a> <span class="status">25408118846</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25385733101',
    created: 1285315840000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> We\'ve invited them and I think it\'s a foundation. Reza spoke about #Arquillian in his #javaone talk.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/25246475673" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">25246475673</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'javaone']
  },
  {
    id: '25384262065',
    created: 1285313777000,
    type: 'post',
    text: 'Here you go <a class="mention" href="https://x.com/scottdavis99" rel="noopener noreferrer" target="_blank">@scottdavis99</a>. If you are Groovy, Kenny Lee Lewis is Funky ;)',
    photos: ['<div class="entry"><img class="photo" src="media/25384262065.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '25383608024',
    created: 1285312868000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Stephan007" rel="noopener noreferrer" target="_blank">@Stephan007</a> +1 with a rim shot.<br><br>In reply to: <a href="https://x.com/Stephan007/status/25334425259" rel="noopener noreferrer" target="_blank">@Stephan007</a> <span class="status">25334425259</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25383574363',
    created: 1285312821000,
    type: 'post',
    text: 'A little birdy told me #Devoxx might open up another theater. Perhaps in preparation for an #Arquillian invasion? (The sky darkens...)',
    likes: 0,
    retweets: 0,
    tags: ['devoxx', 'arquillian']
  },
  {
    id: '25383440936',
    created: 1285312636000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> +1<br><br>In reply to: <a href="https://x.com/jeremynorris/status/25380672874" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">25380672874</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25383414922',
    created: 1285312599000,
    type: 'post',
    text: 'A log4j audio appender wins a Duke\'s Choice award. Really? That\'s what we need in enterprise development? Seems like a strange pick.',
    likes: 0,
    retweets: 0
  },
  {
    id: '25380407582',
    created: 1285308622000,
    type: 'post',
    text: 'Great discussions tonight about standards and leadership w/ <a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a>, Manik, Greg Luck, Terracotta guy #2 (forget name, sorry) &amp; <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a>.',
    likes: 0,
    retweets: 1
  },
  {
    id: '25380265985',
    created: 1285308456000,
    type: 'post',
    text: '40% off many Manning books, including Seam in Action w/ code javaone2010cc until Sept 27.',
    likes: 0,
    retweets: 2
  },
  {
    id: '25374258433',
    created: 1285301788000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/mojavelinux" rel="noopener noreferrer" target="_blank">@mojavelinux</a> I\'m just wondering how many people really believe that statement is true.',
    likes: 0,
    retweets: 0
  },
  {
    id: '25370794567',
    created: 1285298742000,
    type: 'post',
    text: '"JSR 331 represents the real innovation in SE/EE since JSR-300; every other JSR is just an upgrade to existing technologies" - #jcp',
    likes: 1,
    retweets: 1,
    tags: ['jcp']
  },
  {
    id: '25368218417',
    created: 1285296591000,
    type: 'post',
    text: 'Just before crossing the Golden Gate Bridge on bike. Another rite of passage down. Quite a 2 weeks.',
    photos: ['<div class="entry"><img class="photo" src="media/25368218417.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '25367719297',
    created: 1285296185000,
    type: 'post',
    text: 'Downtown San Francisco at sunset from bay.',
    photos: ['<div class="entry"><img class="photo" src="media/25367719297.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '25367611224',
    created: 1285296096000,
    type: 'post',
    text: 'Bay front park in Sausalito.',
    photos: ['<div class="entry"><img class="photo" src="media/25367611224.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '25367265919',
    created: 1285295812000,
    type: 'post',
    text: 'Golden Gate Bridge and exposed Serpentine rock.',
    photos: ['<div class="entry"><img class="photo" src="media/25367265919.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '25367113975',
    created: 1285295689000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> and I enroute to the Golden Gate Bridge. #javaone',
    photos: ['<div class="entry"><img class="photo" src="media/25367113975.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '25349528218',
    created: 1285282167000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> at Ocean Beach on bike trip. Beautiful day. Good times.',
    photos: ['<div class="entry"><img class="photo" src="media/25349528218.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '25345511730',
    created: 1285278812000,
    type: 'post',
    text: 'Washington DC street car in San Francisco. Holler.',
    photos: ['<div class="entry"><img class="photo" src="media/25345511730.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '25294887889',
    created: 1285237569000,
    type: 'post',
    text: 'The only unperfect part of today was that Red Hat went 0/3 in JCP awards.',
    likes: 0,
    retweets: 0
  },
  {
    id: '25294331979',
    created: 1285236899000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aliok_tr" rel="noopener noreferrer" target="_blank">@aliok_tr</a> <a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> I\'m really glad we could hang out. Definitely a one-of-a-kind night. Community FTW!<br><br>In reply to: <a href="https://x.com/aliok_tr/status/25291974571" rel="noopener noreferrer" target="_blank">@aliok_tr</a> <span class="status">25291974571</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25294228989',
    created: 1285236771000,
    type: 'post',
    text: 'Missed last call back in downtown after great night at TI. Then, <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> came through w/ a brown bag present. Hero.',
    likes: 0,
    retweets: 0
  },
  {
    id: '25287347594',
    created: 1285227437000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/scottdavis99" rel="noopener noreferrer" target="_blank">@scottdavis99</a> I didn\'t know you played bass for Steve Miller. You sure are Grooooovy! #JavaOne',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '25287216635',
    created: 1285227248000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> True that. I want the alien space suit for our #Arquillian invasion ;)<br><br>In reply to: <a href="https://x.com/aalmiray/status/25287060258" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">25287060258</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '25287076774',
    created: 1285227051000,
    type: 'post',
    text: 'Steve Miller rockin the funk at Treasure Island #javaone',
    photos: ['<div class="entry"><img class="photo" src="media/25287076774.jpg"></div>'],
    likes: 0,
    retweets: 1,
    tags: ['javaone']
  },
  {
    id: '25284060023',
    created: 1285222855000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/the_f__word" rel="noopener noreferrer" target="_blank">@the_f__word</a> I didn\'t say it, I quoted it. There\'s a difference.<br><br>In reply to: <a href="https://x.com/the_f__word/status/25283347084" rel="noopener noreferrer" target="_blank">@the_f__word</a> <span class="status">25283347084</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25283332031',
    created: 1285221891000,
    type: 'post',
    text: '"I love the code writers and shit...I want to write code and shit, why the fuck not?" - Black Eyed Peas lead. Hero.',
    likes: 0,
    retweets: 0
  },
  {
    id: '25283153356',
    created: 1285221661000,
    type: 'post',
    text: 'I want one of the LED screens. Slick. Real slick.',
    likes: 0,
    retweets: 0
  },
  {
    id: '25283011725',
    created: 1285221479000,
    type: 'post',
    text: 'Boom Boom Pow!',
    photos: ['<div class="entry"><img class="photo" src="media/25283011725.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '25281484732',
    created: 1285219588000,
    type: 'post',
    text: 'Totally packed house at the #Arquillian talk! I love this project. This is big, real big.',
    likes: 1,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '25281308947',
    created: 1285219383000,
    type: 'post',
    text: '"This is like a festival. Not some lame company event." -Black Eyed Peas',
    likes: 0,
    retweets: 2
  },
  {
    id: '25281251507',
    created: 1285219317000,
    type: 'post',
    text: 'A freraky band playing for a geeky group. Black Eyed Peas at #javaone. Yes, love, I suck ;)',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '25238637248',
    created: 1285183325000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/mojavelinux" rel="noopener noreferrer" target="_blank">@mojavelinux</a> Just thought the proximity of those projects was fitting.',
    likes: 0,
    retweets: 0
  },
  {
    id: '25228256485',
    created: 1285174499000,
    type: 'post',
    text: 'Application Server, #Arquillian, Framework Choice (via JBoss booth display)',
    photos: ['<div class="entry"><img class="photo" src="media/25228256485.jpg"></div>'],
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '25192548223',
    created: 1285141340000,
    type: 'post',
    text: 'Slidedeck from #javaone JSR-299 (CDI), Weld &amp; the Future of Seam presentation <a href="http://slidesha.re/cqh7W2" rel="noopener noreferrer" target="_blank">slidesha.re/cqh7W2</a> #weld #jsr299 #seam',
    likes: 2,
    retweets: 6,
    tags: ['javaone', 'weld', 'jsr299', 'seam']
  },
  {
    id: '25188423178',
    created: 1285135366000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> I sang your praises to Barbara Lewis today about your consistent blogging about Java EE 6. Way to be.',
    likes: 0,
    retweets: 0
  },
  {
    id: '25187692388',
    created: 1285134373000,
    type: 'post',
    text: 'Booth talks tomorrow at 10 and 11, then #Arquillian talk at 4:45, jcp event @ 6, then beers the rest of the night ;)',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '25178130300',
    created: 1285124532000,
    type: 'post',
    text: 'Launch your party into the cloud at #javaone #jboss',
    photos: ['<div class="entry"><img class="photo" src="media/25178130300.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['javaone', 'jboss']
  },
  {
    id: '25161506702',
    created: 1285111593000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/glen_a_smith" rel="noopener noreferrer" target="_blank">@glen_a_smith</a> With #Arquillian, alternatives are unnecessary since you can just put the classes you want into the archive.<br><br>In reply to: <a href="https://x.com/glen_a_smith/status/25144083924" rel="noopener noreferrer" target="_blank">@glen_a_smith</a> <span class="status">25144083924</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '25161394627',
    created: 1285111504000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/glen_a_smith" rel="noopener noreferrer" target="_blank">@glen_a_smith</a> <a class="mention" href="https://x.com/Alternative" rel="noopener noreferrer" target="_blank">@Alternative</a> is indeed ideal for mocks. In fact, annotate a stereotype annotation named <a class="mention" href="https://x.com/mock" rel="noopener noreferrer" target="_blank">@mock</a>, then activate it.<br><br>In reply to: <a href="https://x.com/glen_a_smith/status/25144083924" rel="noopener noreferrer" target="_blank">@glen_a_smith</a> <span class="status">25144083924</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25161279990',
    created: 1285111413000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/glen_a_smith" rel="noopener noreferrer" target="_blank">@glen_a_smith</a> A much better link is <a href="http://jboss.org/arquillian" rel="noopener noreferrer" target="_blank">jboss.org/arquillian</a>. It\'s on my list to kill that wiki page as it was the home page draft.<br><br>In reply to: <a href="https://x.com/glen_a_smith/status/25143025790" rel="noopener noreferrer" target="_blank">@glen_a_smith</a> <span class="status">25143025790</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25161132769',
    created: 1285111296000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> You guessed it!<br><br>In reply to: <a href="https://x.com/jasondlee/status/24999241935" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">24999241935</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25160962936',
    created: 1285111165000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> I will get one personally from Chris Maki himself :)<br><br>In reply to: <a href="https://x.com/rruss/status/25043025513" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">25043025513</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25160710013',
    created: 1285110963000,
    type: 'post',
    text: 'I think the tag #javaone2010 is unnecessary (vs #javaone). Updates are timestamped. What other JavaOne could it possibly be?',
    likes: 0,
    retweets: 0,
    tags: ['javaone2010', 'javaone']
  },
  {
    id: '25160650059',
    created: 1285110916000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matkar" rel="noopener noreferrer" target="_blank">@matkar</a> I\'ll be at the JBoss party starting @ 5. Then likely sneaking to a BOF @ 7 (maybe 6). After that, perhaps a drink @ Thirsty Bear.<br><br>In reply to: <a href="https://x.com/matkar/status/25160141627" rel="noopener noreferrer" target="_blank">@matkar</a> <span class="status">25160141627</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '25160525175',
    created: 1285110817000,
    type: 'post',
    text: 'Got trapped by a random person in the lobby when I sat down to drink coffee. She told to stop ruining my life doing software development.',
    likes: 0,
    retweets: 0
  },
  {
    id: '25159636952',
    created: 1285110117000,
    type: 'post',
    text: 'Finally opened up the program for #javaone, saw Duke rocking it out next to my talk.',
    photos: ['<div class="entry"><img class="photo" src="media/25159636952.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '25147388098',
    created: 1285099288000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Yeah man, sorry I missed you. The week just got too nuts w/ house hunting, writing &amp; conf prep. Love NM! 80% sure we\'ll pick it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '25139882261',
    created: 1285092292000,
    type: 'post',
    text: '30 minutes until my #jsr299 and #seam 3 talk at #javaone. Note there is a room change. Hilton - Golden Gate 6/7. Hope to see you there!',
    likes: 0,
    retweets: 1,
    tags: ['jsr299', 'seam', 'javaone']
  },
  {
    id: '25101629122',
    created: 1285059296000,
    type: 'post',
    text: 'OMG, it\'s 2 in the morning and I have a huge talk tomorrow. Time to call it quits! #javaone #jsr299 #cdi #seam Hilton, Golden Gate 6/7.',
    likes: 0,
    retweets: 0,
    tags: ['javaone', 'jsr299', 'cdi', 'seam']
  },
  {
    id: '25101338309',
    created: 1285058873000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> Hero, as <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> would say. The timing is perfect. So perfect.',
    likes: 0,
    retweets: 0
  },
  {
    id: '25101281659',
    created: 1285058788000,
    type: 'post',
    text: 'If you are looking for a great software architect/consultant, hire Adam Bien. Seriously, he radiates success through levelheadedness.',
    likes: 0,
    retweets: 1
  },
  {
    id: '25101138489',
    created: 1285058577000,
    type: 'post',
    text: 'Had fantastic conversation w/ Adam Bien and David Blevins tonight at Thirsty Bear. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '25101087238',
    created: 1285058501000,
    type: 'post',
    text: 'Made it just in the nick of time to the #CDI BOF w/ David Belvins &amp; Reza Rahman. Most successful BOF I\'ve ever participated in #javaone',
    likes: 0,
    retweets: 0,
    tags: ['cdi', 'javaone']
  },
  {
    id: '25057734674',
    created: 1285016769000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> speaking about #Arquillian with examples included in his EJB book.',
    photos: ['<div class="entry"><img class="photo" src="media/25057734674.jpg"></div>'],
    likes: 0,
    retweets: 2,
    tags: ['arquillian']
  },
  {
    id: '25057547050',
    created: 1285016592000,
    type: 'post',
    text: '#Arquillian has a slot in the rack @ the JBoss booth. I suppose that we did a good job communicating its goals.',
    photos: ['<div class="entry"><img class="photo" src="media/25057547050.jpg"></div>'],
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '24999186060',
    created: 1284958977000,
    type: 'post',
    text: 'Went through an American rite of passage while in New Mexico. Finally drove on The Mother Road. Unfortunately, just a snippet of it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '24998862259',
    created: 1284958632000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maiko_rocha" rel="noopener noreferrer" target="_blank">@maiko_rocha</a> I\'m all for it, just a lot late for the party ;)<br><br>In reply to: <a href="https://x.com/maiko_rocha/status/24997929015" rel="noopener noreferrer" target="_blank">@maiko_rocha</a> <span class="status">24997929015</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '24998791039',
    created: 1284958554000,
    type: 'post',
    text: '$110 to get to and from Redmond to Seattle airport. Sick.',
    likes: 0,
    retweets: 0
  },
  {
    id: '24997106274',
    created: 1284956784000,
    type: 'post',
    text: 'The return value is written on the wall.',
    photos: ['<div class="entry"><img class="photo" src="media/24997106274.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '24996575039',
    created: 1284956251000,
    type: 'post',
    text: 'Who starts a conference on Sunday night anyway?',
    likes: 0,
    retweets: 0
  },
  {
    id: '24996500249',
    created: 1284956177000,
    type: 'post',
    text: 'An 8 hour flight is nothing. 30 minutes of circling over a city &amp; I\'m ready to throw myself off the plane. Ahhhhh. Now I need beer.',
    likes: 0,
    retweets: 0
  },
  {
    id: '24984604859',
    created: 1284946148000,
    type: 'post',
    text: 'Poor timing on my part to catch Sun events. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '24984443296',
    created: 1284946017000,
    type: 'post',
    text: 'Taking off from Seattle, next stop #javaone!',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '24916541339',
    created: 1284880626000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mariusbogoevici" rel="noopener noreferrer" target="_blank">@mariusbogoevici</a> The West is something spectacular. If I lived here, sunset would be a daily show. I\'d ask "Where were you for the sunset?"<br><br>In reply to: <a href="https://x.com/mariusbogoevici/status/24915569343" rel="noopener noreferrer" target="_blank">@mariusbogoevici</a> <span class="status">24915569343</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '24916064167',
    created: 1284880010000,
    type: 'post',
    text: 'Time to get some zzzzs. Finishing up rough draft of #Arquillian article and presenting #Arquillian, both for #NFJS.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'arquillian', 'nfjs']
  },
  {
    id: '24916023568',
    created: 1284879958000,
    type: 'post',
    text: 'It was actually sunny in Seattle today. Definitely made for a warm welcome.',
    likes: 0,
    retweets: 0
  },
  {
    id: '24915979580',
    created: 1284879902000,
    type: 'post',
    text: '#Andiamo isn\'t just a JBoss initiative. It\'s also the best Italian in Santa Fe, apparently.',
    photos: ['<div class="entry"><img class="photo" src="media/24915979580.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['andiamo']
  },
  {
    id: '24915794692',
    created: 1284879668000,
    type: 'post',
    text: 'Santa Fe at dusk from above town.',
    photos: ['<div class="entry"><img class="photo" src="media/24915794692.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '24915495592',
    created: 1284879290000,
    type: 'post',
    text: 'Parking meter #fail (Santa Fe)',
    photos: ['<div class="entry"><img class="photo" src="media/24915495592.jpg"></div>'],
    likes: 0,
    retweets: 2,
    tags: ['fail']
  },
  {
    id: '24915389300',
    created: 1284879157000,
    type: 'post',
    text: 'I\'ve developed a love affair with Aspens. They are official my favorite tree.',
    photos: ['<div class="entry"><img class="photo" src="media/24915389300.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '24915166476',
    created: 1284878873000,
    type: 'post',
    text: 'You have not seen blue until you\'ve seen the Santa Fe sky. No monitor in the world could display all the colors in a Santa Fe sunset. Wow.',
    likes: 0,
    retweets: 0
  },
  {
    id: '24915007506',
    created: 1284878672000,
    type: 'post',
    text: 'I think is #Ike is very excited about #JavaOne',
    photos: ['<div class="entry"><img class="photo" src="media/24915007506.png"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['ike', 'javaone']
  },
  {
    id: '24914952202',
    created: 1284878602000,
    type: 'post',
    text: 'Of course, I had to boast about our awesome Norwegian project lead, <a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> #Arquillian.',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '24914902081',
    created: 1284878539000,
    type: 'post',
    text: 'Was listening to <a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> talk about his amazing hike in Norway &amp; about the good fortune of the country\'s citizens.',
    likes: 0,
    retweets: 0
  },
  {
    id: '24914024992',
    created: 1284877459000,
    type: 'reply',
    text: '@jethrobakker #OpenWebBeans is another JSR-299 impl, developed at Apache. Fun to see how they are extending it too.<br><br>In reply to: @jethrobakker <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['openwebbeans']
  },
  {
    id: '24913934065',
    created: 1284877350000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jakobkorherr" rel="noopener noreferrer" target="_blank">@jakobkorherr</a> It\'s really trivial in Gradle: <a href="https://github.com/arquillian/arquillian-showcase/blob/master/cdi/build.gradle" rel="noopener noreferrer" target="_blank">github.com/arquillian/arquillian-showcase/blob/master/cdi/build.gradle</a><br><br>In reply to: <a href="https://x.com/jakobkorherr/status/24517594081" rel="noopener noreferrer" target="_blank">@jakobkorherr</a> <span class="status">24517594081</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '24913840728',
    created: 1284877239000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MStine" rel="noopener noreferrer" target="_blank">@MStine</a> Remember to check out github.com/arquillian. I\'m positive you\'ll find it useful &amp; we\'d value your expertise (whatever time permits).<br><br>In reply to: @mstine <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '24913746576',
    created: 1284877127000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MStine" rel="noopener noreferrer" target="_blank">@MStine</a> Indeed! And a good time for shop talk, despite being called out by the headmaster. Shop talk timeout is back at home ;)<br><br>In reply to: @mstine <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '24913676441',
    created: 1284877043000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> <a class="mention" href="https://x.com/jponge" rel="noopener noreferrer" target="_blank">@jponge</a> beans.xml is an optimization; limits scanning/discovery to select jars instead of entire classpath. Will cover @ #javaone<br><br>In reply to: <a href="https://x.com/arungupta/status/24661359442" rel="noopener noreferrer" target="_blank">@arungupta</a> <span class="status">24661359442</span>',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '24913503083',
    created: 1284876842000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matkar" rel="noopener noreferrer" target="_blank">@matkar</a> Shoot, it just occurred to me tonight when the conversation came up about European conferences. I\'ll get it done this week.<br><br>In reply to: <a href="https://x.com/matkar/status/24739519397" rel="noopener noreferrer" target="_blank">@matkar</a> <span class="status">24739519397</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '24913430545',
    created: 1284876756000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> I can\'t wait! I\'ll be getting in late tomorrow night, so I might just see you at the hotel.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/24912754191" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">24912754191</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '24849614886',
    created: 1284817912000,
    type: 'post',
    text: 'Btw, hello everyone! Just wrapped up my New Mexico visit. Now on to Seattle 4 #NFJS &amp; #JavaOne after the weekend. Look forward to geek out!',
    likes: 0,
    retweets: 0,
    tags: ['nfjs', 'javaone']
  },
  {
    id: '24849478222',
    created: 1284817795000,
    type: 'post',
    text: 'Just used a mobile boarding pass all the way from counter to gate at ABQ. Felt a little nervous about it, but all scanners we a go.',
    likes: 0,
    retweets: 0
  },
  {
    id: '24138347375',
    created: 1284153726000,
    type: 'post',
    text: 'At the Palace of the Governors during Fiesta in Santa Fe.',
    photos: ['<div class="entry"><img class="photo" src="media/24138347375.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '24078809857',
    created: 1284095476000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tedneward" rel="noopener noreferrer" target="_blank">@tedneward</a> Just do it.<br><br>In reply to: <a href="https://x.com/tedneward/status/24078350897" rel="noopener noreferrer" target="_blank">@tedneward</a> <span class="status">24078350897</span>',
    likes: 0,
    retweets: 1
  },
  {
    id: '24078645008',
    created: 1284095300000,
    type: 'post',
    text: '#Zozobra burning is sort of like 4th of July cut with #Cornell slope day.',
    likes: 0,
    retweets: 0,
    tags: ['zozobra', 'cornell']
  },
  {
    id: '24078541314',
    created: 1284095189000,
    type: 'post',
    text: 'Arrived in Santa Fe tonight to find ourselves @ epicenter of the Zozobra burning ceremony &amp; opening of Fiesta. Pure drunken madness.',
    likes: 0,
    retweets: 0
  },
  {
    id: '23984050794',
    created: 1284011040000,
    type: 'post',
    text: 'Enjoyed speaking to serious test automation experts @ NOVATAIG about #Arquillian. Many already heard of it! It\'s crack &amp; getting better',
    likes: 1,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '23983973970',
    created: 1284010953000,
    type: 'post',
    text: 'Thanks <a class="mention" href="https://x.com/cwash" rel="noopener noreferrer" target="_blank">@cwash</a> for traveling from Richmond to see my #Arquillian presentation at NOVATAIG. Good times ahead, indeed!',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '23983937050',
    created: 1284010912000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cwash" rel="noopener noreferrer" target="_blank">@cwash</a> It\'s on :) IHookable and IConfigurable <a href="http://code.google.com/p/testng/source/detail?r=971" rel="noopener noreferrer" target="_blank">code.google.com/p/testng/source/detail?r=971</a> FTW!<br><br>In reply to: <a href="https://x.com/cwash/status/22851924265" rel="noopener noreferrer" target="_blank">@cwash</a> <span class="status">22851924265</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '23983780319',
    created: 1284010739000,
    type: 'post',
    text: '#Arquillian makes waves during #GSoC <a href="http://www.jakobk.com/2010/09/myfaces-webapptest-meets-apache-svn/" rel="noopener noreferrer" target="_blank">www.jakobk.com/2010/09/myfaces-webapptest-meets-apache-svn/</a>',
    likes: 0,
    retweets: 3,
    tags: ['arquillian', 'gsoc']
  },
  {
    id: '23983588310',
    created: 1284010539000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jakobkorherr" rel="noopener noreferrer" target="_blank">@jakobkorherr</a> Fantastic!<br><br>In reply to: <a href="https://x.com/jakobkorherr/status/23961429264" rel="noopener noreferrer" target="_blank">@jakobkorherr</a> <span class="status">23961429264</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '23983485408',
    created: 1284010421000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jankeesvanandel" rel="noopener noreferrer" target="_blank">@jankeesvanandel</a> Yep, it\'s the blog\'s layout that\'s confused ;)<br><br>In reply to: <a href="https://x.com/jankeesvanandel/status/23937574850" rel="noopener noreferrer" target="_blank">@jankeesvanandel</a> <span class="status">23937574850</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '23983470207',
    created: 1284010403000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> Oops, my mistake. The attribution sort of floats between two paragraphs, and I got it mixed up.<br><br>In reply to: @pelegri <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '23983311512',
    created: 1284010225000,
    type: 'post',
    text: 'I\'ve been using #git to manage my the versions of my presentations. I lose the diff functionality, but still much better organized.',
    likes: 0,
    retweets: 0,
    tags: ['git']
  },
  {
    id: '23936191017',
    created: 1283969428000,
    type: 'post',
    text: 'Interesting to see Brian Goetz chime in w/ disappointment over that schedule given his position.',
    likes: 0,
    retweets: 0
  },
  {
    id: '23928246333',
    created: 1283963014000,
    type: 'post',
    text: 'I\'ll be giving a presentation tonight on #Arquillian to Northern Virginia Test Automation Group - 6:30pm @ Tortilla Factory, Herndon, VA.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '23927103613',
    created: 1283962194000,
    type: 'post',
    text: 'Is there a way to collaboratively edit and view docbook source in a project? We need a Google Docs interface on github or something.',
    likes: 0,
    retweets: 0
  },
  {
    id: '23891060949',
    created: 1283926972000,
    type: 'reply',
    text: '@JAXenterCOM More like "what can build tools learn from #Gradle?" :) Gradle is the virtuoso of automation tools.<br><br>In reply to: <a href="https://x.com/devmio_official/status/23222921900" rel="noopener noreferrer" target="_blank">@devmio_official</a> <span class="status">23222921900</span>',
    likes: 0,
    retweets: 0,
    tags: ['gradle']
  },
  {
    id: '23870456881',
    created: 1283908782000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> Prepare to be amazed ;) A tutorial is waiting in the wings.<br><br>In reply to: <a href="https://x.com/JohnAment/status/23862295538" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">23862295538</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '23861939000',
    created: 1283901837000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> Haha. Not necessarily, though I\'m certainly liking #Gradle. Right now my goal is to demo how to use #Arquillian w/o #Maven.<br><br>In reply to: <a href="https://x.com/JohnAment/status/23859681865" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">23859681865</span>',
    likes: 0,
    retweets: 0,
    tags: ['gradle', 'arquillian', 'maven']
  },
  {
    id: '23272019433',
    created: 1283894821000,
    type: 'post',
    text: 'I\'ve requested additional named repositories in #Gradle for the major public artifact repos. <a href="http://jira.codehaus.org/browse/GRADLE-1145" rel="noopener noreferrer" target="_blank">jira.codehaus.org/browse/GRADLE-1145</a>',
    likes: 0,
    retweets: 1,
    tags: ['gradle']
  },
  {
    id: '23264755274',
    created: 1283887978000,
    type: 'post',
    text: 'Any major repositories missing from this list? JBoss (Nexus and legacy), Codehaus, Apache, Spring, Java.net, Google Code, GlassFish.',
    likes: 0,
    retweets: 0
  },
  {
    id: '23264503502',
    created: 1283887737000,
    type: 'post',
    text: 'What\'s your opinion, polarized or non-polarized sunglasses? I\'m really hesitant on polarized since I use LCD screens *a lot* of the time.',
    likes: 0,
    retweets: 0
  },
  {
    id: '23255580239',
    created: 1283879855000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/jakobkorherr" rel="noopener noreferrer" target="_blank">@jakobkorherr</a> Got a link or a blog entry? Actually, just a quick blog entry would go a long way.',
    likes: 0,
    retweets: 0
  },
  {
    id: '23255468626',
    created: 1283879778000,
    type: 'post',
    text: 'Had a great chat w/ Cedric Beust of #TestNG. Result: new hooks in #TestNG that will make the #Arquillian integration strictly declarative.',
    likes: 0,
    retweets: 3,
    tags: ['testng', 'testng', 'arquillian']
  },
  {
    id: '23152759250',
    created: 1283785945000,
    type: 'post',
    text: 'We haven\'t officially announced it, but the updated #Weld archetype for Java EE 6 (named jboss-javaee6-webapp) is now in #Maven central.',
    likes: 0,
    retweets: 2,
    tags: ['weld', 'maven']
  },
  {
    id: '23150897905',
    created: 1283784629000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/antoine_sd" rel="noopener noreferrer" target="_blank">@antoine_sd</a> It\'s taken a while to adjust to developing in a distributed, modular way. Your best bet now is to play w/ individual modules.<br><br>In reply to: <a href="https://x.com/antoine_sd/status/23003465618" rel="noopener noreferrer" target="_blank">@antoine_sd</a> <span class="status">23003465618</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '23150426790',
    created: 1283784286000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> From the teaser trailers, it sounds like it\'s going to be a blast. Spread the good word, man.<br><br>In reply to: <a href="https://x.com/matthewmccull/status/23149813136" rel="noopener noreferrer" target="_blank">@matthewmccull</a> <span class="status">23149813136</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '23148608521',
    created: 1283782932000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> "Myfaces Test Framework provides mock object libraries, plus base classes for creating your own JUnit TestCases for JSF."<br><br>In reply to: <a href="https://x.com/lightguardjp/status/23147011690" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">23147011690</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '23041800265',
    created: 1283672419000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cbeust" rel="noopener noreferrer" target="_blank">@cbeust</a> E-mail sent. I\'m excited to see how we can better integration #Arquillian and #TestNG. I want TestNG groups and my cake too :)<br><br>In reply to: <a href="https://x.com/cbeust/status/22999368839" rel="noopener noreferrer" target="_blank">@cbeust</a> <span class="status">22999368839</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'testng']
  },
  {
    id: '22949845929',
    created: 1283575142000,
    type: 'post',
    text: '"iTunes 10: Free for Mac+PC" Too bad, you can only have it if you computer is both a Mac &amp; a PC.',
    likes: 0,
    retweets: 0
  },
  {
    id: '22949674346',
    created: 1283574973000,
    type: 'post',
    text: '#Jfokus call for paper link: <a href="http://www.jfokus.se/jfokus/proposals.jsp?lang=en" rel="noopener noreferrer" target="_blank">www.jfokus.se/jfokus/proposals.jsp?lang=en</a>',
    likes: 2,
    retweets: 0,
    tags: ['jfokus']
  },
  {
    id: '22947187390',
    created: 1283572650000,
    type: 'post',
    text: 'Yes, #Arquillian can even be used to test #ruby <a href="http://torquebox.org/news/2010/08/30/end-to-end-testing" rel="noopener noreferrer" target="_blank">torquebox.org/news/2010/08/30/end-to-end-testing</a>',
    likes: 1,
    retweets: 1,
    tags: ['arquillian', 'ruby']
  },
  {
    id: '22931821176',
    created: 1283558917000,
    type: 'post',
    text: 'The Ant+Ivy &amp; Gradle builds for #Arquillian #CDI demo are now all working. Finally!!! <a href="http://github.com/mojavelinux/arquillian-showcase" rel="noopener noreferrer" target="_blank">github.com/mojavelinux/arquillian-showcase</a>',
    likes: 0,
    retweets: 2,
    tags: ['arquillian', 'cdi']
  },
  {
    id: '22906301164',
    created: 1283534996000,
    type: 'reply',
    text: '@GradleOrg Works for me now too! Yeah!<br><br>In reply to: <a href="https://x.com/gradle/status/22900233618" rel="noopener noreferrer" target="_blank">@gradle</a> <span class="status">22900233618</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22906247614',
    created: 1283534951000,
    type: 'reply',
    text: '@GradleOrg Aha! It seems I had a corrupt $HOME/.gradle directory, perhaps from usage way back. Nuked it and now see activity.<br><br>In reply to: <a href="https://x.com/gradle/status/22900233618" rel="noopener noreferrer" target="_blank">@gradle</a> <span class="status">22900233618</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22898529476',
    created: 1283529084000,
    type: 'post',
    text: 'What is wrong with this super basic #gradle build? <a href="http://gradle.1045684.n5.nabble.com/Resolving-transitive-dependencies-of-pom-artifact-tp2801806p2801806.html" rel="noopener noreferrer" target="_blank">gradle.1045684.n5.nabble.com/Resolving-transitive-dependencies-of-pom-artifact-tp2801806p2801806.html</a> Gotta get over this small hurdle.',
    likes: 0,
    retweets: 0,
    tags: ['gradle']
  },
  {
    id: '22898357576',
    created: 1283528960000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> I imagine it\'d be faster &amp; the impl simpler. Plus, familiar output. But I do still have issue w/ Maven storage strategy. Toss up.<br><br>In reply to: <a href="https://x.com/kito99/status/22897645280" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">22897645280</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22897702999',
    created: 1283528489000,
    type: 'post',
    text: 'I don\'t get why a doctors\' office is on facebook. Clearly the internet failed at some point. How about just an Atom feed (blog or twitter)?',
    likes: 0,
    retweets: 0
  },
  {
    id: '22897510397',
    created: 1283528353000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> That\'s my everyday it seems. Which causes me to go backward more than forward.<br><br>In reply to: <a href="https://x.com/aalmiray/status/22882089206" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">22882089206</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22897339809',
    created: 1283528230000,
    type: 'post',
    text: 'Lack of follower name complete #fail. That thanks should have been attributed to <a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a>. Amazing I know most handles from memory.',
    likes: 0,
    retweets: 0,
    tags: ['fail']
  },
  {
    id: '22897034030',
    created: 1283528012000,
    type: 'post',
    text: 'Btw, my heart is now owned by Gradle and Git. Oh Ike (#Arquillian), and you too ;)',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '22896838758',
    created: 1283527874000,
    type: 'post',
    text: 'Aether seems a good fit for Gradle.',
    likes: 0,
    retweets: 1
  },
  {
    id: '22896740823',
    created: 1283527805000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jvanzyl" rel="noopener noreferrer" target="_blank">@jvanzyl</a> Didn\'t realize Aether had Ant tasks. Still, we were requested to show an Ant Ivy example as proof of concept.<br><br>In reply to: <a href="https://x.com/jvanzyl/status/22886276936" rel="noopener noreferrer" target="_blank">@jvanzyl</a> <span class="status">22886276936</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22896406916',
    created: 1283527569000,
    type: 'post',
    text: 'Why do we call smartphones phones? A better term would be mobile communication device, or #mcd.',
    likes: 0,
    retweets: 0,
    tags: ['mcd']
  },
  {
    id: '22888889622',
    created: 1283522140000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/mojavelinux" rel="noopener noreferrer" target="_blank">@mojavelinux</a> Thanks @lightguard_jp for the late night support and helping me grok Ivy config mappings.',
    likes: 0,
    retweets: 0
  },
  {
    id: '22886850042',
    created: 1283520563000,
    type: 'post',
    text: 'Requirements for doctor\'s office new website. Internet Explorer 6 and up. Really? y2k?',
    photos: ['<div class="entry"><img class="photo" src="media/22886850042.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '22867170681',
    created: 1283497170000,
    type: 'post',
    text: 'The Ant + Ivy build took me about half a day to put together. If transitive dependencies had worked for me, the Gradle build &lt; 10 mins.',
    likes: 0,
    retweets: 1
  },
  {
    id: '22867071499',
    created: 1283497035000,
    type: 'post',
    text: 'We now have an Ant build for the #Arquillian #cdi showcase sub-project. Yeah! <a href="http://github.com/mojavelinux/arquillian-showcase" rel="noopener noreferrer" target="_blank">github.com/mojavelinux/arquillian-showcase</a>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'cdi']
  },
  {
    id: '22857308816',
    created: 1283485685000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I was able to work something out, but I still don\'t know exactly why it works.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/22857078597" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">22857078597</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22857271257',
    created: 1283485650000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I\'ll leave the benefit of the doubt open that it\'s the documentation. But using A-&gt;B syntax doesn\'t mean anything to me.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/22857078597" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">22857078597</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22856431092',
    created: 1283484887000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lazarotti" rel="noopener noreferrer" target="_blank">@lazarotti</a> Yep, which Eclipse doesn\'t always like to run :)<br><br>In reply to: <a href="https://x.com/lazarotti/status/22855421115" rel="noopener noreferrer" target="_blank">@lazarotti</a> <span class="status">22855421115</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22856385850',
    created: 1283484845000,
    type: 'post',
    text: 'Now Ivy is downloading the internet. Maven, you are not alone. But your line might go dead while Ivy is at work.',
    likes: 0,
    retweets: 0
  },
  {
    id: '22856234348',
    created: 1283484709000,
    type: 'post',
    text: 'I\'m torturing myself with Ivy for you so that you can see #Arquillian working without Maven. Just...about...have it.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '22856179357',
    created: 1283484662000,
    type: 'post',
    text: 'Maybe I\'m just ignorant, but Ivy scopes make absolutely no sense to me. Finding a basic Ivy setup is like looking for needle-&gt;haystack(*).',
    likes: 0,
    retweets: 0
  },
  {
    id: '22840624465',
    created: 1283471298000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Entries in calendar do not show the time zone of event. So I see 1PM for a talk in SF, but that is really 10AM when I get there.<br><br>In reply to: <a href="https://x.com/kito99/status/22840260678" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">22840260678</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22837427562',
    created: 1283468476000,
    type: 'post',
    text: 'The lack of time zone awareness in Google Calendar is really ticking me off. It\'s not enough to just display the time, unless it\'s UTC.',
    likes: 1,
    retweets: 0
  },
  {
    id: '22832281622',
    created: 1283463829000,
    type: 'post',
    text: 'I really want to demo #Arquillian TestNG support, but only if I can get a <a class="mention" href="https://x.com/runwith" rel="noopener noreferrer" target="_blank">@runwith</a> equivalent in #TestNG. Audiences hate test subclassing.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'testng']
  },
  {
    id: '22829548251',
    created: 1283461284000,
    type: 'post',
    text: 'Finally finished booking my trip for #JavaOne. It\'s going to be multi-part trip Home -&gt; Albuquerque/Santa Fe -&gt; Seattle -&gt; SF -&gt; Home',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '22826697122',
    created: 1283458420000,
    type: 'post',
    text: 'Reported Google Calendar birthday bug: <a href="https://www.google.com/support/forum/p/Calendar/thread?tid=756d519a5d43df03&amp;hl=en" rel="noopener noreferrer" target="_blank">www.google.com/support/forum/p/Calendar/thread?tid=756d519a5d43df03&amp;hl=en</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22826029645',
    created: 1283457746000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> It does.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/22823392407" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">22823392407</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22825910328',
    created: 1283457629000,
    type: 'post',
    text: 'Just discovered a bug in #Google Calendar. If 2 people have the same home e-mail, the calendar may attribute a birthday to the wrong person.',
    likes: 0,
    retweets: 0,
    tags: ['google']
  },
  {
    id: '22825836219',
    created: 1283457558000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jennifercord" rel="noopener noreferrer" target="_blank">@jennifercord</a> Oops, Happy Birthday to your husband ;)<br><br>In reply to: <a href="https://x.com/jennifercord/status/22824338741" rel="noopener noreferrer" target="_blank">@jennifercord</a> <span class="status">22824338741</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22823098757',
    created: 1283454763000,
    type: 'post',
    text: 'Happy Birthday <a class="mention" href="https://x.com/jennifercord" rel="noopener noreferrer" target="_blank">@jennifercord</a>!',
    likes: 0,
    retweets: 0
  },
  {
    id: '22822931507',
    created: 1283454595000,
    type: 'post',
    text: 'Async #EJB test using Arquillian <a href="https://github.com/mojavelinux/arquillian-showcase/blob/async-ejb-cdi/ejb/src/test/java/com/acme/ejb/async/FireAndForgetTestCase.java" rel="noopener noreferrer" target="_blank">github.com/mojavelinux/arquillian-showcase/blob/async-ejb-cdi/ejb/src/test/java/com/acme/ejb/async/FireAndForgetTestCase.java</a>',
    likes: 2,
    retweets: 1,
    tags: ['ejb']
  },
  {
    id: '22820695256',
    created: 1283452367000,
    type: 'post',
    text: 'I love "git commit -v" I almost always need to see the diff as I\'m writing my commit statement, and I get that for free ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '22819632870',
    created: 1283451308000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maeste" rel="noopener noreferrer" target="_blank">@maeste</a> Cool! I love having a design team, especially ours.<br><br>In reply to: <a href="https://x.com/maeste/status/22818964794" rel="noopener noreferrer" target="_blank">@maeste</a> <span class="status">22818964794</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22809126742',
    created: 1283442534000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> <a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> The only reasonable way to go is to fly into Brussels (or Amsterdam) then take the train. Easy commute from there by train.<br><br>In reply to: <a href="https://x.com/rruss/status/22806381823" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">22806381823</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22805480879',
    created: 1283439841000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/msreekanth" rel="noopener noreferrer" target="_blank">@msreekanth</a> Welcome to the #Arquillian forums. Thanks for your feedback and patience. Helps us know where to improve.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '22805265893',
    created: 1283439692000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/antoine_sd" rel="noopener noreferrer" target="_blank">@antoine_sd</a> Those are the only 3 #jsr299 implementations atm, afaik. All 3 are co-hosting a BOF together a #JavaOne to share stories.<br><br>In reply to: <a href="https://x.com/antoine_sd/status/22524445240" rel="noopener noreferrer" target="_blank">@antoine_sd</a> <span class="status">22524445240</span>',
    likes: 0,
    retweets: 0,
    tags: ['jsr299', 'javaone']
  },
  {
    id: '22759644872',
    created: 1283391526000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/evanchooly" rel="noopener noreferrer" target="_blank">@evanchooly</a> A public statement is certainly in order. But, since this is a community process, shouldn\'t we somehow be involved? Just saying.<br><br>In reply to: <a href="https://x.com/evanchooly/status/22740528476" rel="noopener noreferrer" target="_blank">@evanchooly</a> <span class="status">22740528476</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22734629030',
    created: 1283369490000,
    type: 'post',
    text: 'It\'s scary to see zero JSRs in the pipeline: <a href="http://jcp.org/en/jsr/stage?listBy=jsr" rel="noopener noreferrer" target="_blank">jcp.org/en/jsr/stage?listBy=jsr</a>',
    likes: 1,
    retweets: 4
  },
  {
    id: '22734429787',
    created: 1283369299000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> I\'d like to start with the question "does the JCP still exist?"<br><br>In reply to: <a href="https://x.com/jasondlee/status/22727977707" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">22727977707</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22728951162',
    created: 1283364268000,
    type: 'post',
    text: 'Gmail tip: To recreate a double quoted section, 1) highlight the text 2) click quote 3) click right indent 4) click quote again.',
    likes: 0,
    retweets: 1
  },
  {
    id: '22727523939',
    created: 1283363074000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Yeah, it didn\'t play out correctly b/c inter-spec communication happened too little too late on that topic ;)<br><br>In reply to: <a href="https://x.com/jasondlee/status/22727310861" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">22727310861</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22727401364',
    created: 1283362976000,
    type: 'post',
    text: 'Nominations for star spec lead (Gavin &amp; <a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a>) &amp; most innovative JSR #jsr299 &amp; #jsr303 got accepted! <a href="http://blogs.sun.com/jcp/" rel="noopener noreferrer" target="_blank">blogs.sun.com/jcp/</a>',
    likes: 0,
    retweets: 0,
    tags: ['jsr299', 'jsr303']
  },
  {
    id: '22726886901',
    created: 1283362567000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Putting conversation scope solely in JSF would have been the wrong approach, IMHO. Where it belongs is the Servlet spec + JSF tags.<br><br>In reply to: <a href="https://x.com/kito99/status/22626463056" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">22626463056</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22726641178',
    created: 1283362372000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> Congratulations! Tobias is lucky to have such great parents :) And I\'m sure you feel equally fortunate to have him in your life.<br><br>In reply to: <a href="https://x.com/aschwart/status/22702488210" rel="noopener noreferrer" target="_blank">@aschwart</a> <span class="status">22702488210</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22686310007',
    created: 1283324214000,
    type: 'post',
    text: 'Yes, #Arquillian has already made it into a book! <a href="http://oreilly.com/catalog/9780596158033" rel="noopener noreferrer" target="_blank">oreilly.com/catalog/9780596158033</a>',
    likes: 0,
    retweets: 2,
    tags: ['arquillian']
  },
  {
    id: '22686279799',
    created: 1283324173000,
    type: 'post',
    text: 'Just finished reading (most) of a draft copy of the EJB 3.1 O\'Reilly book by <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> (&amp; predecessors). Good stuff! Yeah #Arquillian!',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '22686068379',
    created: 1283323895000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/hannelita" rel="noopener noreferrer" target="_blank">@hannelita</a> Thanks!<br><br>In reply to: <a href="https://x.com/hannelita/status/22670440805" rel="noopener noreferrer" target="_blank">@hannelita</a> <span class="status">22670440805</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22681925143',
    created: 1283318846000,
    type: 'post',
    text: '"Tonight marks the end of the American combat mission in Iraq." -- Obama',
    likes: 0,
    retweets: 0
  },
  {
    id: '22670048215',
    created: 1283306808000,
    type: 'post',
    text: 'Psyched that Red Hat is using Seam in Action as courseware for Seam training &amp; likely Seam certification <a href="http://mojavelinux.com/seaminaction" rel="noopener noreferrer" target="_blank">mojavelinux.com/seaminaction</a>',
    likes: 0,
    retweets: 4
  },
  {
    id: '22653354772',
    created: 1283292865000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> Yep, that\'s where I was going with that. I realized when looking at my presentation that I had done it subconsciously.<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/22650429194" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">22650429194</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22649776830',
    created: 1283289581000,
    type: 'post',
    text: '#Arquillian in a nutshell',
    photos: ['<div class="entry"><img class="photo" src="media/22649776830.png"></div>'],
    likes: 0,
    retweets: 2,
    tags: ['arquillian']
  },
  {
    id: '22649756353',
    created: 1283289563000,
    type: 'post',
    text: 'ShrinkWrap in a nutshell',
    photos: ['<div class="entry"><img class="photo" src="media/22649756353.png"></div>'],
    likes: 0,
    retweets: 2
  },
  {
    id: '22627878427',
    created: 1283269923000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> This is freakin\' brilliant. When you thought #Arquillian couldn\'t get better...um, what were you thinking. Of course it can!<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/22627668929" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">22627668929</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '22627823927',
    created: 1283269882000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RKela" rel="noopener noreferrer" target="_blank">@RKela</a> It was great to meet you! I\'ll be in touch. Remember, attendees can get the latest version of my slides on the #NFJS event site.<br><br>In reply to: <a href="https://x.com/RKela/status/22463707118" rel="noopener noreferrer" target="_blank">@RKela</a> <span class="status">22463707118</span>',
    likes: 0,
    retweets: 0,
    tags: ['nfjs']
  },
  {
    id: '22489317391',
    created: 1283135791000,
    type: 'post',
    text: 'I think my phone senses it\'s going to be replaced w/ a newer model &amp; is trying to lose itself. Another "is this your phone?" incident today.',
    likes: 0,
    retweets: 0
  },
  {
    id: '22488011128',
    created: 1283134673000,
    type: 'post',
    text: 'Had the cheeriest flight attendent ever on flight from CLT-&gt;BWI. Her name: Hope. Born the optimistic type FTW. "Nighty night" she signs off.',
    likes: 0,
    retweets: 0
  },
  {
    id: '22476728867',
    created: 1283124814000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Nope, not this time. I\'m totally seeing the doors this tool can open. I\'m looking forward to get hacking on it.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/22474601182" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">22474601182</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22464279479',
    created: 1283112871000,
    type: 'post',
    text: 'Enjoyed bringing a unique perspective on #javaee to the #NFJS audience. But I fear I might get labeled Java EE lover by fellow speakers.',
    likes: 1,
    retweets: 0,
    tags: ['javaee', 'nfjs']
  },
  {
    id: '22464058764',
    created: 1283112631000,
    type: 'post',
    text: 'Surprised that even after some time off, all the info in my head about Java EE, #jsr299, and #arquillian is still fresh as ever.',
    likes: 0,
    retweets: 0,
    tags: ['jsr299', 'arquillian']
  },
  {
    id: '22448819487',
    created: 1283098087000,
    type: 'post',
    text: 'Our requirement to run our #Arquillian tests against a sequence of containers in the same build run would totally be doable in #Gradle.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'gradle']
  },
  {
    id: '22447500043',
    created: 1283097028000,
    type: 'post',
    text: 'It\'s clear that #Gradle could be used for a general automation script tool. Really puts bash to shame, even more than vanilla #groovy.',
    likes: 0,
    retweets: 1,
    tags: ['gradle', 'groovy']
  },
  {
    id: '22445738787',
    created: 1283095603000,
    type: 'post',
    text: 'In Ken Sipe\'s #Gradle talk at #NFJS. First time I\'ve had the chance to see Ken speak. Tremendous amount of energy!',
    photos: ['<div class="entry"><img class="photo" src="media/22445738787.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['gradle', 'nfjs']
  },
  {
    id: '22444645584',
    created: 1283094738000,
    type: 'post',
    text: 'Just finished the #Arquillian talk at #NFJS. Great group that seemed to really connect w/ the material. I get so pumped talking about it.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'nfjs']
  },
  {
    id: '22397090372',
    created: 1283043709000,
    type: 'post',
    text: 'Just got back from a BBQ dinner at the Relevance office with the NFJS speakers. Less of an office and more of just a cool place.',
    likes: 0,
    retweets: 0
  },
  {
    id: '22385070346',
    created: 1283031579000,
    type: 'post',
    text: 'Landed at RDU to marathon speak at the last day of NFJS Raleigh.',
    likes: 0,
    retweets: 0
  },
  {
    id: '22356094346',
    created: 1283005114000,
    type: 'post',
    text: 'DJTunes lets you select a 30 sec region of a song to preview. #Amazon needs that feature so you can evaluate the song better.',
    likes: 0,
    retweets: 0,
    tags: ['amazon']
  },
  {
    id: '22307734861',
    created: 1282952280000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Awesome! Somewhere my former coach is standing proud ;)<br><br>In reply to: <a href="https://x.com/lincolnthree/status/22306231339" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">22306231339</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22302663574',
    created: 1282947263000,
    type: 'post',
    text: 'Several members of the Seam development team will be hosting a Seam Gathering BOF at #Devoxx. Time and date TBD.',
    likes: 0,
    retweets: 1,
    tags: ['devoxx']
  },
  {
    id: '22301941327',
    created: 1282946609000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gmaggess" rel="noopener noreferrer" target="_blank">@gmaggess</a> I\'m not complaining, I\'m making free suggestions :) If I could hack the code, I might.<br><br>In reply to: <a href="https://x.com/gmaggess/status/22299863358" rel="noopener noreferrer" target="_blank">@gmaggess</a> <span class="status">22299863358</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22299873865',
    created: 1282944724000,
    type: 'post',
    text: '#Google calendar should have a picklist of frequent places where you have events, so you don\'t have to type it over &amp; over. #idea',
    likes: 0,
    retweets: 0,
    tags: ['google', 'idea']
  },
  {
    id: '22299527439',
    created: 1282944406000,
    type: 'post',
    text: 'I wish #gmail let you set the preferred e-mail address for a contact so that you know what the hell one to select in autocomplete.',
    likes: 0,
    retweets: 0,
    tags: ['gmail']
  },
  {
    id: '22298669770',
    created: 1282943612000,
    type: 'post',
    text: 'Fixed Seam 3 booking example: <a href="http://anonsvn.jboss.org/repos/seam/examples/trunk/javaee-booking" rel="noopener noreferrer" target="_blank">anonsvn.jboss.org/repos/seam/examples/trunk/javaee-booking</a>',
    likes: 0,
    retweets: 1
  },
  {
    id: '22298275913',
    created: 1282943237000,
    type: 'post',
    text: 'Prepping my presentations for #NFJS Raleigh. Gonna be a quick trip for me. I\'m only there to sleep &amp; present. That means lots of energy :)',
    likes: 0,
    retweets: 0,
    tags: ['nfjs']
  },
  {
    id: '22297662481',
    created: 1282942663000,
    type: 'post',
    text: '#Google to #JavaOne crowd: "We out" <em>&lt;expired link&gt;</em> Glad to see them defending their open source principles.',
    likes: 0,
    retweets: 4,
    tags: ['google', 'javaone']
  },
  {
    id: '22297465693',
    created: 1282942471000,
    type: 'post',
    text: 'Is it CSS naked day at twitter.com or something? Put your stylesheet back on, man!',
    likes: 0,
    retweets: 0
  },
  {
    id: '22281249910',
    created: 1282927440000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> I\'m just glad neither the ball nor the club maimed anyone. I crushed a few 3 woods after that, so it will be my spare tire ;)<br><br>In reply to: <a href="https://x.com/rruss/status/22250530593" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">22250530593</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22242408328',
    created: 1282887036000,
    type: 'post',
    text: 'When I swung my driver on the driving range today, the head came off and went flying out into the range. Scared the heck out of me.',
    likes: 0,
    retweets: 0
  },
  {
    id: '22210405152',
    created: 1282857868000,
    type: 'post',
    text: 'JBoss will be at Java One again this year: <a href="http://www.jboss.org/events/javaone.html" rel="noopener noreferrer" target="_blank">www.jboss.org/events/javaone.html</a>',
    likes: 0,
    retweets: 4
  },
  {
    id: '22113963030',
    created: 1282765234000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/simpligility" rel="noopener noreferrer" target="_blank">@simpligility</a> Nope, Prius. The clearance isn\'t bad, but not good enough to be totally clear.<br><br>In reply to: <a href="https://x.com/simpligility/status/22104944750" rel="noopener noreferrer" target="_blank">@simpligility</a> <span class="status">22104944750</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22113939635',
    created: 1282765211000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marekgoldmann" rel="noopener noreferrer" target="_blank">@marekgoldmann</a> Good to hear. I like the possibility then :)<br><br>In reply to: <a href="https://x.com/marekgoldmann/status/22109964911" rel="noopener noreferrer" target="_blank">@marekgoldmann</a> <span class="status">22109964911</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22104897539',
    created: 1282756381000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marekgoldmann" rel="noopener noreferrer" target="_blank">@marekgoldmann</a> My hestitation to submodules is that I want to avoid tripping people up. Submodules seem to make pull/push more complex.<br><br>In reply to: <a href="https://x.com/marekgoldmann/status/22100573524" rel="noopener noreferrer" target="_blank">@marekgoldmann</a> <span class="status">22100573524</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22104677106',
    created: 1282756176000,
    type: 'post',
    text: 'I can\'t stand ramps that bottom out the front of your car. Grr! Like getting stung.',
    likes: 0,
    retweets: 0
  },
  {
    id: '22101740266',
    created: 1282753653000,
    type: 'post',
    text: 'Just picked up two #Cleveland CG14 wedges last night, 52 and 58. Can\'t wait to see how the zip grooves grab the ball. Practice, practice.',
    likes: 0,
    retweets: 0,
    tags: ['cleveland']
  },
  {
    id: '22101337017',
    created: 1282753317000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dzone" rel="noopener noreferrer" target="_blank">@dzone</a> But then there are influential leaders in VM ecosystem but not necessarily Java like Bob McWhirter (<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a>)<br><br>In reply to: <a href="https://x.com/DZoneInc/status/22099809075" rel="noopener noreferrer" target="_blank">@DZoneInc</a> <span class="status">22099809075</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22101224007',
    created: 1282753223000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dzone" rel="noopener noreferrer" target="_blank">@dzone</a> I\'d pick Mike Brock (<a class="mention" href="https://x.com/brockm" rel="noopener noreferrer" target="_blank">@brockm</a>), Matthew McCullough (<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a>), Joshua Block, Brian Goetz, Andy Glover, Scott Davis among others<br><br>In reply to: <a href="https://x.com/DZoneInc/status/22099809075" rel="noopener noreferrer" target="_blank">@DZoneInc</a> <span class="status">22099809075</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22100566651',
    created: 1282752685000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dzone" rel="noopener noreferrer" target="_blank">@dzone</a> This would be a fun question on twitter. Who are the top 8 Java people you should know? And really know, not just for fame.<br><br>In reply to: <a href="https://x.com/DZoneInc/status/22099809075" rel="noopener noreferrer" target="_blank">@DZoneInc</a> <span class="status">22099809075</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22100408642',
    created: 1282752557000,
    type: 'post',
    text: 'We\'re planning on having a #git repo for each #Seam module. Is there a way to provide a consolidated url to clone? (submodules? script?)',
    likes: 0,
    retweets: 0,
    tags: ['git', 'seam']
  },
  {
    id: '22099652741',
    created: 1282751956000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emacsen" rel="noopener noreferrer" target="_blank">@emacsen</a> Sometimes, a very large one, unfortunately. I\'ve followed it since the beginning, but our paths just took a while to cross :)<br><br>In reply to: <a href="https://x.com/emacsen/status/22077295697" rel="noopener noreferrer" target="_blank">@emacsen</a> <span class="status">22077295697</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '22099594894',
    created: 1282751909000,
    type: 'post',
    text: 'Happy to say I was one of many that proposed &amp; voted for regional formatting control in #Eclipse <a href="http://download.eclipse.org/eclipse/downloads/drops/R-3.6-201006080911/eclipse-news-part2.html#JavaFormatter" rel="noopener noreferrer" target="_blank">download.eclipse.org/eclipse/downloads/drops/R-3.6-201006080911/eclipse-news-part2.html#JavaFormatter</a>',
    likes: 0,
    retweets: 0,
    tags: ['eclipse']
  },
  {
    id: '22099030839',
    created: 1282751454000,
    type: 'post',
    text: '#Deltacloud provides one of the only open cloud APIs and brings the principles of OSS to the cloud. <a href="http://www.redhat.com/about/news/prarchive/2010/DMTF.html" rel="noopener noreferrer" target="_blank">www.redhat.com/about/news/prarchive/2010/DMTF.html</a>',
    likes: 0,
    retweets: 1,
    tags: ['deltacloud']
  },
  {
    id: '22031625291',
    created: 1282684554000,
    type: 'post',
    text: 'Feels like a fall day in Maryland.',
    likes: 0,
    retweets: 0
  },
  {
    id: '22028913008',
    created: 1282681951000,
    type: 'post',
    text: '#git has me ecstatic. I can\'t believe the visibility it gives me. I can examine the whole history of a repo while waiting for a build.',
    likes: 0,
    retweets: 2,
    tags: ['git']
  },
  {
    id: '22012706264',
    created: 1282666528000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ShervinAsgari" rel="noopener noreferrer" target="_blank">@ShervinAsgari</a> Yes, #Seam 3 is switching from SVN to Git. The migration scripts are being prepared at the moment. Stay tuned.<br><br>In reply to: <a href="https://x.com/ShervinAsgari/status/21987196424" rel="noopener noreferrer" target="_blank">@ShervinAsgari</a> <span class="status">21987196424</span>',
    likes: 0,
    retweets: 1,
    tags: ['seam']
  },
  {
    id: '21941582515',
    created: 1282596176000,
    type: 'post',
    text: 'I often see the letter J at the end of lines of a message when reading it in gmail. Apparently, that\'s a smiley lost in translation.',
    likes: 0,
    retweets: 0
  },
  {
    id: '21939371256',
    created: 1282593964000,
    type: 'post',
    text: 'Currently working on script to migrate #seam 3 modules to github.com. Getting pretty close.',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '21939187342',
    created: 1282593782000,
    type: 'post',
    text: 'Concluded over drink w/ fellow techy that phone &amp; voicemail is least advanced part of "smart phone". More like "dumb phone, smart computer"',
    likes: 0,
    retweets: 1
  },
  {
    id: '21934186753',
    created: 1282588637000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/richsharples" rel="noopener noreferrer" target="_blank">@richsharples</a> <a class="mention" href="https://x.com/caniszczyk" rel="noopener noreferrer" target="_blank">@caniszczyk</a> I know about the e-mail feature, and I like it, but there is still effort that goes into tweaking it.<br><br>In reply to: <a href="https://x.com/richsharples/status/21931919739" rel="noopener noreferrer" target="_blank">@richsharples</a> <span class="status">21931919739</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21933262216',
    created: 1282587675000,
    type: 'reply',
    text: '@mwessendorf Was there ever a question? Hahaha.<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21932570225',
    created: 1282586969000,
    type: 'post',
    text: 'Thin &amp; light w/ a slick shell. Very nice. Can\'t play w/ it now though, so it won\'t give up it\'s inner secrets yet.',
    photos: ['<div class="entry"><img class="photo" src="media/21932570225.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '21932224199',
    created: 1282586623000,
    type: 'post',
    text: 'My wife\'s Lenovo ideapad has arrived! I\'ll be putting on my sysadmin hat tonight. Toys!',
    photos: ['<div class="entry"><img class="photo" src="media/21932224199.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '21931775561',
    created: 1282586177000,
    type: 'post',
    text: 'If the travel agent entered my itinerary into tripit, that would save me the most time. Otherwise, I\'m still doing a lot of work.',
    likes: 0,
    retweets: 0
  },
  {
    id: '21929603938',
    created: 1282584014000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> Good point! Sounds like we need a new JIRA.<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/21929527032" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">21929527032</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21929452564',
    created: 1282583868000,
    type: 'post',
    text: '#Arquillian and #Shrinkwrap are finally listed in both the projects menu and matrix on jboss.org <a href="http://www.jboss.org/projects/matrix" rel="noopener noreferrer" target="_blank">www.jboss.org/projects/matrix</a>',
    likes: 1,
    retweets: 1,
    tags: ['arquillian', 'shrinkwrap']
  },
  {
    id: '21855477689',
    created: 1282508206000,
    type: 'post',
    text: 'I\'m irritated that my company won\'t whitelist my gmail address for communication with redhat group e-mail aliases. Scream.',
    likes: 0,
    retweets: 0
  },
  {
    id: '21846030547',
    created: 1282498448000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <a class="mention" href="https://x.com/pniederw" rel="noopener noreferrer" target="_blank">@pniederw</a> We would be glad to get them in the JBoss repo, at least JUnit for sure. Direct benefit to #Arquillian users.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/21722043168" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">21722043168</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '21845945833',
    created: 1282498364000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <a class="mention" href="https://x.com/neal4d" rel="noopener noreferrer" target="_blank">@neal4d</a> <a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> I\'ve tried to switch to zsh for years. It\'s my loss because from what I\'ve read it\'s solid.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/21722657511" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">21722657511</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21845215178',
    created: 1282497635000,
    type: 'post',
    text: '#Android browser history options: Today, yesterday, 5 days ago, ... what? Hello, I think you skipped some days. And they just reappear?',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '21845004382',
    created: 1282497426000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> Such a superstar! You &amp; Ike ;)<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/21844548481" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">21844548481</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21844903778',
    created: 1282497327000,
    type: 'post',
    text: 'Signatures shouldn\'t whine "excuse the brevity of this e-mail, it was sent from a phone", but rather, "I have a phone w/ a crappy keypad."',
    likes: 0,
    retweets: 0
  },
  {
    id: '21844209805',
    created: 1282496665000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> We are proud of you too! Did you share stories about you conference limelight? I hope so, big accomplishment.<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/21832445540" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">21832445540</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21844041163',
    created: 1282496510000,
    type: 'post',
    text: 'I prefer to read my tweets &amp; e-mail in reverse chronological order. I guess I want a current read on things, then look at the history.',
    likes: 0,
    retweets: 0
  },
  {
    id: '21799523461',
    created: 1282447610000,
    type: 'post',
    text: 'Driving ranges should be able to give you digital readout of how far you hit the ball, rather than vague markers a la tin cup.',
    likes: 0,
    retweets: 0
  },
  {
    id: '21781389842',
    created: 1282429136000,
    type: 'post',
    text: 'Just finished up a round of golf @ West Winds Golf Club w/ <a class="mention" href="https://x.com/gscottshaw" rel="noopener noreferrer" target="_blank">@gscottshaw</a>. Decent course, decent round, great company &amp; beautiful weather!',
    likes: 0,
    retweets: 0
  },
  {
    id: '21781175210',
    created: 1282428898000,
    type: 'post',
    text: 'Google maps should let you enter a speed for a segment of a trip so you can get an accurate time.',
    likes: 0,
    retweets: 0
  },
  {
    id: '21728572144',
    created: 1282371522000,
    type: 'post',
    text: 'Just added West Winds Golf Club in New Market, MD to skydroid.net for tomorrow\'s round. Time to get some sleep!',
    likes: 0,
    retweets: 0
  },
  {
    id: '21722822747',
    created: 1282365415000,
    type: 'post',
    text: 'I\'m working w/ <a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> to prepare the migration of #Seam 3 modules to github.com. Finally getting to our original plans for Seam 3.',
    likes: 0,
    retweets: 1,
    tags: ['seam']
  },
  {
    id: '21702331009',
    created: 1282345710000,
    type: 'post',
    text: 'To clarify, you shouldn\'t have to nuke remote repo cache if you trust the repo to be stable, naturally. General case is to trust it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '21698330913',
    created: 1282341684000,
    type: 'post',
    text: 'You should never have to nuke the files mirrored from a remote repository. Instead, you just nuke the file links in the local #Maven repo.',
    likes: 0,
    retweets: 0,
    tags: ['maven']
  },
  {
    id: '21698219316',
    created: 1282341587000,
    type: 'post',
    text: 'The local #Maven repository is all wrong. It should create a local cache for each remote repo, then make local repo artifacts file links.',
    likes: 0,
    retweets: 0,
    tags: ['maven']
  },
  {
    id: '21696144220',
    created: 1282339617000,
    type: 'post',
    text: 'appbrain.com just announced sorting #Android apps by rating. Finally! Most annoying part of #Android Market is lack of this feature.',
    likes: 0,
    retweets: 1,
    tags: ['android', 'android']
  },
  {
    id: '21689459685',
    created: 1282332859000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Good job, both on the write-up &amp; impl. I was attracted to GlassFish for the same reason, so glad to see it advance in that area.<br><br>In reply to: <a href="https://x.com/jasondlee/status/21679481171" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">21679481171</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21683530663',
    created: 1282326987000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Exactly. Thanks to great work lead by Paul Gier &amp; patient communication of vision by Max Andersen.<br><br>In reply to: <a href="https://x.com/maxandersen/status/21681901817" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">21681901817</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21639598190',
    created: 1282281718000,
    type: 'post',
    text: '20 yrs of Perl history available in a single git repository. Value is git focuses on recognition. Git recognized. via <a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21639134700',
    created: 1282281231000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> quoted as saying about git: "every time I touch it, the experience continues to get better"',
    likes: 0,
    retweets: 0
  },
  {
    id: '21638666609',
    created: 1282280709000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> A must listen podcast! You describe git as so blazing fast you assume it\'s broken at 1st. I said the same to someone today!<br><br>In reply to: <a href="https://x.com/matthewmccull/status/21631881576" rel="noopener noreferrer" target="_blank">@matthewmccull</a> <span class="status">21631881576</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21637334568',
    created: 1282279310000,
    type: 'post',
    text: 'Git was clearly designed by someone who has to maintain a collaborative project, a key factor of its success thus far.',
    likes: 0,
    retweets: 0
  },
  {
    id: '21606090108',
    created: 1282250315000,
    type: 'post',
    text: 'Got a random auto-reply "I\'m in a land far, far away where there are no phones, email or internet :-) I may return if I lose all my senses."',
    likes: 0,
    retweets: 0
  },
  {
    id: '21598591011',
    created: 1282242599000,
    type: 'post',
    text: 'Just got an e-mail announcing new jsf component library built on composite components: smartfaces.org Not sure how approach will pan out.',
    likes: 0,
    retweets: 0
  },
  {
    id: '21581261825',
    created: 1282227920000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Congrats!<br><br>In reply to: <a href="https://x.com/lincolnthree/status/21576014284" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">21576014284</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21552491686',
    created: 1282195253000,
    type: 'post',
    text: 'What\'s so great about #git (&amp; github) is that you don\'t need permission from a project lead in order to share your project mods. OSS+',
    likes: 0,
    retweets: 1,
    tags: ['git']
  },
  {
    id: '21527718069',
    created: 1282172538000,
    type: 'post',
    text: 'Make sure to follow the build blog to keep up with what\'s going on w/ the JBoss repositories <a href="http://community.jboss.org/en/build?view=blog" rel="noopener noreferrer" target="_blank">community.jboss.org/en/build?view=blog</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21526131077',
    created: 1282171067000,
    type: 'reply',
    text: '@techmpegram <a class="mention" href="https://x.com/rdifrango" rel="noopener noreferrer" target="_blank">@rdifrango</a> <a class="mention" href="https://x.com/cwash" rel="noopener noreferrer" target="_blank">@cwash</a> Yeah, that\'s what I always think of first when I consider where #Arquillian applies.<br><br>In reply to: @techmpegram <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '21517969281',
    created: 1282163236000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> <a class="mention" href="https://x.com/simpligility" rel="noopener noreferrer" target="_blank">@simpligility</a> True, I can do it with the Android SDK kit, but not so useful when riding in car or at the sandwich shop ;)<br><br>In reply to: <a href="https://x.com/rruss/status/21511212713" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">21511212713</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21517884935',
    created: 1282163146000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> It\'s as good as sold as long as I don\'t discover any showstopper when I get a chance to demo it.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21516180084',
    created: 1282161373000,
    type: 'post',
    text: 'Check out #Arquillian presentation slides by <a class="mention" href="https://x.com/cwash" rel="noopener noreferrer" target="_blank">@cwash</a> <a href="http://www.slideshare.net/nukeevry1/arquillian" rel="noopener noreferrer" target="_blank">www.slideshare.net/nukeevry1/arquillian</a> Good stuff.',
    likes: 3,
    retweets: 3,
    tags: ['arquillian']
  },
  {
    id: '21515140464',
    created: 1282160284000,
    type: 'post',
    text: 'DZone on #Android. Survey says: useless. Notice rising link: "We want a mobile DZone"',
    photos: ['<div class="entry"><img class="photo" src="media/21515140464.png"></div>'],
    likes: 0,
    retweets: 1,
    tags: ['android']
  },
  {
    id: '21509944967',
    created: 1282155344000,
    type: 'post',
    text: 'Why does taking a screenshot in #Android require a rooted phone?? Definitely an overlooked marketing opportunity. Same story w/ 2.2?',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '21509181595',
    created: 1282154597000,
    type: 'post',
    text: 'Mobile RSS feed readers should eat through facade entries (like on DZone) &amp; pull the full content. Otherwise, I will only read 1 paragraph.',
    likes: 0,
    retweets: 0
  },
  {
    id: '21504626473',
    created: 1282150364000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gmaggess" rel="noopener noreferrer" target="_blank">@gmaggess</a> Nope, never shows me the details. At least, not the description, where is where my details are.<br><br>In reply to: <a href="https://x.com/gmaggess/status/21503737572" rel="noopener noreferrer" target="_blank">@gmaggess</a> <span class="status">21503737572</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21504583342',
    created: 1282150325000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sten_aksel" rel="noopener noreferrer" target="_blank">@sten_aksel</a> Yep, I believe that is work that is being done on github now. Having switched to github makes it easier for experimentation.<br><br>In reply to: <a href="https://x.com/sten_aksel/status/21504387194" rel="noopener noreferrer" target="_blank">@sten_aksel</a> <span class="status">21504387194</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21504056551',
    created: 1282149842000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> There is a PDF <a href="http://docs.jboss.org/weld/reference/latest/en-US/pdf/weld-reference.pdf" rel="noopener noreferrer" target="_blank">docs.jboss.org/weld/reference/latest/en-US/pdf/weld-reference.pdf</a><br><br>In reply to: <a href="https://x.com/arungupta/status/21502915378" rel="noopener noreferrer" target="_blank">@arungupta</a> <span class="status">21502915378</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21502157650',
    created: 1282148172000,
    type: 'post',
    text: 'Let\'s make a pact: "We\'ll not waste time developing a custom test harness." -- ALR, #Arquillian webinar',
    likes: 0,
    retweets: 4,
    tags: ['arquillian']
  },
  {
    id: '21499389724',
    created: 1282145844000,
    type: 'post',
    text: 'Why don\'t Google Calendar entries have "Show details". There is only "Edit details". I don\'t want to edit, I just want to see them.',
    likes: 0,
    retweets: 0
  },
  {
    id: '21499135616',
    created: 1282145619000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> Hahaha. Good thing that wasn\'t a pic of my phone :)<br><br>In reply to: <a href="https://x.com/rruss/status/21497914642" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">21497914642</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21495447912',
    created: 1282142579000,
    type: 'post',
    text: 'I believe that the #G2 is the HTC Vision. Pictures indicate that it does have a 3-row slide-out keyboard, a must have for a tech geek.',
    likes: 0,
    retweets: 1,
    tags: ['g2']
  },
  {
    id: '21495368516',
    created: 1282142514000,
    type: 'post',
    text: 'Here\'s a pic of the text message we got from #T-mobile this morning announcing the #G2: <a href="http://twitgoo.com/1imuef" rel="noopener noreferrer" target="_blank">twitgoo.com/1imuef</a>',
    likes: 0,
    retweets: 0,
    tags: ['t', 'g2']
  },
  {
    id: '21490847243',
    created: 1282138740000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tmobile_usa" rel="noopener noreferrer" target="_blank">@tmobile_usa</a> Thank you! Finally, the loyal #G1 customers are getting some love! Much appreciated!<br><br>In reply to: <a href="https://x.com/TMobile/status/21484695799" rel="noopener noreferrer" target="_blank">@TMobile</a> <span class="status">21484695799</span>',
    likes: 0,
    retweets: 0,
    tags: ['g1']
  },
  {
    id: '21490667865',
    created: 1282138583000,
    type: 'post',
    text: 'The #Android revolution continues at <a class="mention" href="https://x.com/tmobile_usa" rel="noopener noreferrer" target="_blank">@tmobile_usa</a> with the #G2. <a href="http://androidspin.com/2010/08/18/official-t-mobile-g2-coming-as-first-hspa-device/" rel="noopener noreferrer" target="_blank">androidspin.com/2010/08/18/official-t-mobile-g2-coming-as-first-hspa-device/</a>',
    likes: 0,
    retweets: 1,
    tags: ['android', 'g2']
  },
  {
    id: '21490609497',
    created: 1282138530000,
    type: 'post',
    text: 'What an unexpected surprise! I woke up this morning w/ a txt message from #T-mobile announcing #G2, the 4G successor to the G1. It\'s real.',
    likes: 0,
    retweets: 0,
    tags: ['t', 'g2']
  },
  {
    id: '21490367236',
    created: 1282138317000,
    type: 'post',
    text: 'It\'s not too late! Sign up for the #JBoss Developer Webinar covering #Arquillian today <a href="https://community.jboss.org/en/arquillian/blog/2010/08/18/jboss-developer-webinar-on-enterprise-testing-with-arquillian" rel="noopener noreferrer" target="_blank">community.jboss.org/en/arquillian/blog/2010/08/18/jboss-developer-webinar-on-enterprise-testing-with-arquillian</a>',
    likes: 0,
    retweets: 1,
    tags: ['jboss', 'arquillian']
  },
  {
    id: '21489209928',
    created: 1282137317000,
    type: 'post',
    text: 'Don\'t miss the JBoss Developer Webinar on #Arquillian by <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> today! (Aug 18) at 12PM EST / 9AM PST <a href="http://jboss.org/webinars" rel="noopener noreferrer" target="_blank">jboss.org/webinars</a>',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '21464401875',
    created: 1282107759000,
    type: 'post',
    text: 'Of course, after I upgraded, I immediately moved the window buttons back to the right. Sorry, I don\'t do close buttons on the left.',
    likes: 0,
    retweets: 0
  },
  {
    id: '21464341300',
    created: 1282107693000,
    type: 'post',
    text: 'Just upgraded one of my computers to Ubuntu Lucid 10.04, finally. Flawless as usual ;) Having fun toying with Gnome Do.',
    likes: 0,
    retweets: 0
  },
  {
    id: '21440360394',
    created: 1282085976000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> What\'s the view layer?',
    likes: 0,
    retweets: 0
  },
  {
    id: '21440254428',
    created: 1282085877000,
    type: 'post',
    text: 'Look at the JSR-299 TCK coverage report (<a href="https://repository.jboss.org/nexus/content/groups/public-jboss/org/jboss/jsr299/tck/jsr299-tck-impl/1.1.0-SNAPSHOT/jsr299-tck-impl-1.1.0-20100811.040129-1-coverage-cdi.html" rel="noopener noreferrer" target="_blank">repository.jboss.org/nexus/content/groups/public-jboss/org/jboss/jsr299/tck/jsr299-tck-impl/1.1.0-SNAPSHOT/jsr299-tck-impl-1.1.0-20100811.040129-1-coverage-cdi.html</a>), described at <a href="http://docs.jboss.org/cdi/tck/reference/1.0.2.CR2/en-US/html/reporting.html#d0e791" rel="noopener noreferrer" target="_blank">docs.jboss.org/cdi/tck/reference/1.0.2.CR2/en-US/html/reporting.html#d0e791</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21435219808',
    created: 1282081222000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> There\'s a discussion going on right now about UIData on the JSR-314 mailinglist. You could read it if it were open :)<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21414835425',
    created: 1282061620000,
    type: 'post',
    text: 'A meeting of the minds is going on right now in preparation for the #Arquillian &amp; #ShrinkWrap webinar tomorrow <a href="http://jboss.org/webinars" rel="noopener noreferrer" target="_blank">jboss.org/webinars</a>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'shrinkwrap']
  },
  {
    id: '21413202948',
    created: 1282060270000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Stephan007" rel="noopener noreferrer" target="_blank">@Stephan007</a> Ike is hoping for the thumbs up:<br><br>In reply to: <a href="https://x.com/Stephan007/status/21404518266" rel="noopener noreferrer" target="_blank">@Stephan007</a> <span class="status">21404518266</span>',
    photos: ['<div class="entry"><img class="photo" src="media/24915007506.png"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '21412774988',
    created: 1282059915000,
    type: 'post',
    text: 'Had a close encounter w/ burnout at the turn of the month. Nothing a little beach and family time couldn\'t sort out. Finding my legs again.',
    likes: 0,
    retweets: 0
  },
  {
    id: '21409806892',
    created: 1282057439000,
    type: 'post',
    text: 'If you want a convenient parent POM for publishing to Maven central, check out <a href="https://docs.sonatype.org/display/Repository/Central+Repository+FAQ" rel="noopener noreferrer" target="_blank">docs.sonatype.org/display/Repository/Central+Repository+FAQ</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '21352602353',
    created: 1281999844000,
    type: 'post',
    text: 'I feel like I got taken by the arms and sucker-punched by #Oracle mobsters when they decided to attack Android &amp; Java OSS community.',
    likes: 0,
    retweets: 0,
    tags: ['oracle']
  },
  {
    id: '21352420019',
    created: 1281999680000,
    type: 'post',
    text: 'Good insight by <a class="mention" href="https://x.com/SachaLabourey" rel="noopener noreferrer" target="_blank">@SachaLabourey</a> about how #Oracle missed the real opportunity to show leadership &amp; strength <a href="http://sacha.labourey.com/2010/08/16/orcl-vs-goog-hopefully-just-a-bad-timing/" rel="noopener noreferrer" target="_blank">sacha.labourey.com/2010/08/16/orcl-vs-goog-hopefully-just-a-bad-timing/</a>',
    likes: 1,
    retweets: 1,
    tags: ['oracle']
  },
  {
    id: '21351996640',
    created: 1281999306000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tedneward" rel="noopener noreferrer" target="_blank">@tedneward</a> It probably prepares them to be great actors/actresses though. Think about it, you have to interact w/ imaginary characters.<br><br>In reply to: <a href="https://x.com/tedneward/status/21350912353" rel="noopener noreferrer" target="_blank">@tedneward</a> <span class="status">21350912353</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21343752942',
    created: 1281991782000,
    type: 'post',
    text: 'First page of my interview about Seam 3 for JavaMagazin translated into German.',
    photos: ['<div class="entry"><img class="photo" src="media/21343752942.png"></div>'],
    likes: 0,
    retweets: 1
  },
  {
    id: '21337049141',
    created: 1281984847000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/timoreilly" rel="noopener noreferrer" target="_blank">@timoreilly</a> Good quote, though I hope that Google does not become numb to the discomfort of making such a compromise.<br><br>In reply to: <a href="https://x.com/timoreilly/status/21335924363" rel="noopener noreferrer" target="_blank">@timoreilly</a> <span class="status">21335924363</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21334974931',
    created: 1281982749000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/codylerum" rel="noopener noreferrer" target="_blank">@codylerum</a> For jboss.org, you could create a JIRA issue at <a href="http://jira.jboss.org/browse/ORG" rel="noopener noreferrer" target="_blank">jira.jboss.org/browse/ORG</a><br><br>In reply to: <a href="https://x.com/codylerum/status/21284250248" rel="noopener noreferrer" target="_blank">@codylerum</a> <span class="status">21284250248</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21279520294',
    created: 1281925569000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/codylerum" rel="noopener noreferrer" target="_blank">@codylerum</a> I\'m linked into gravatar. Agree, good central identity. I wish I could hook it up to my Google contacts.<br><br>In reply to: <a href="https://x.com/codylerum/status/21279244908" rel="noopener noreferrer" target="_blank">@codylerum</a> <span class="status">21279244908</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21279401943',
    created: 1281925460000,
    type: 'post',
    text: 'I was hitting on this hot bridesmaid at the wedding. I happen to be married to her, but that doesn\'t stop me from wooing her ;)',
    likes: 0,
    retweets: 1
  },
  {
    id: '21278938060',
    created: 1281925039000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jennifercord" rel="noopener noreferrer" target="_blank">@jennifercord</a> Only when I\'m throwing the attitude around :) Mostly, though, I appreciate his enthusiasm to make it as an artist.<br><br>In reply to: <a href="https://x.com/jennifercord/status/21004458696" rel="noopener noreferrer" target="_blank">@jennifercord</a> <span class="status">21004458696</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21176662558',
    created: 1281818581000,
    type: 'post',
    text: 'My sister on the alter saying "I do!"',
    photos: ['<div class="entry"><img class="photo" src="media/21176662558.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '21087981183',
    created: 1281726145000,
    type: 'post',
    text: 'I was high most of my childhood on these',
    photos: ['<div class="entry"><img class="photo" src="media/21087981183.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '21004483105',
    created: 1281646296000,
    type: 'post',
    text: 'You can also listen to the Jump of the Wall single on youtube: <a href="https://youtu.be/RHY9bKTAvQk" rel="noopener noreferrer" target="_blank">youtu.be/RHY9bKTAvQk</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '21004267884',
    created: 1281646081000,
    type: 'post',
    text: 'A local buddy has published a freestyle rap track titled Jump of the Wall. Listen to it on his myspace page <a href="http://www.myspace.com/nd200x" rel="noopener noreferrer" target="_blank">www.myspace.com/nd200x</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '20996246115',
    created: 1281637746000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lazarotti" rel="noopener noreferrer" target="_blank">@lazarotti</a> Yep, I didn\'t find it until just two days ago either. I\'ve asked the design team to start a blog for announcements.<br><br>In reply to: <a href="https://x.com/lazarotti/status/20977019798" rel="noopener noreferrer" target="_blank">@lazarotti</a> <span class="status">20977019798</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '20949084606',
    created: 1281589676000,
    type: 'post',
    text: 'I\'ve had a lot of success pulling my golf swing together following <a href="http://www.amazon.com/Two-Steps-Perfect-Golf-Swing/dp/0071435220" rel="noopener noreferrer" target="_blank">www.amazon.com/Two-Steps-Perfect-Golf-Swing/dp/0071435220</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '20948771614',
    created: 1281589369000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lazarotti" rel="noopener noreferrer" target="_blank">@lazarotti</a> All JBoss Community wallpapers can be found here: <a href="http://jboss.org/coolstuff/desktopwallpapers.html" rel="noopener noreferrer" target="_blank">jboss.org/coolstuff/desktopwallpapers.html</a><br><br>In reply to: <a href="https://x.com/lazarotti/status/20914889004" rel="noopener noreferrer" target="_blank">@lazarotti</a> <span class="status">20914889004</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '20948725635',
    created: 1281589324000,
    type: 'post',
    text: 'Just watched Wonders of the Solar System with Prof Brian Cox about Saturn\'s rings. If that isn\'t beauty in physics, I don\'t know what is.',
    likes: 0,
    retweets: 0
  },
  {
    id: '20909638317',
    created: 1281554118000,
    type: 'post',
    text: '9-part series by <a class="mention" href="https://x.com/kennardconsult" rel="noopener noreferrer" target="_blank">@kennardconsult</a> showing ways to control ordering of fields in a #MetaWidget <a href="http://kennardconsulting.blogspot.com/2010/08/customizing-which-form-fields-are_04.html" rel="noopener noreferrer" target="_blank">kennardconsulting.blogspot.com/2010/08/customizing-which-form-fields-are_04.html</a>',
    likes: 0,
    retweets: 0,
    tags: ['metawidget']
  },
  {
    id: '20905270154',
    created: 1281549867000,
    type: 'post',
    text: 'My sister is getting married this weekend just steps away from the US Capitol. It\'s going to be quite an event!',
    likes: 0,
    retweets: 0
  },
  {
    id: '20903893758',
    created: 1281548558000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> Speaking of sending, I wish it had a scheduled send too. I know there are proxies I can use for that, but why not native?<br><br>In reply to: <a href="https://x.com/jeremynorris/status/20903789436" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">20903789436</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '20903597292',
    created: 1281548286000,
    type: 'post',
    text: 'JBoss Community project collage as the iGoogle background. The home page is the new desktop.',
    photos: ['<div class="entry"><img class="photo" src="media/20903597292.png"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '20902013741',
    created: 1281546846000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> I agree with that assessment.<br><br>In reply to: <a href="https://x.com/jasondlee/status/20901953011" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">20901953011</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '20901946454',
    created: 1281546786000,
    type: 'post',
    text: '#Gmail upgraded the UI to make contacts view use the same layout as the mail view and a better use of space overall. I ♥ Gmail.',
    likes: 0,
    retweets: 0,
    tags: ['gmail']
  },
  {
    id: '20822678470',
    created: 1281471894000,
    type: 'post',
    text: 'Allergies are on the rise. I conclude the solution is: more real food, less anti-bacterial fanaticism...or, eat dirt and let dogs lick you.',
    likes: 0,
    retweets: 1
  },
  {
    id: '20821587638',
    created: 1281470882000,
    type: 'post',
    text: 'A new Weld release is in the works that uses #Arquillian for all the integration tests. This is #Arquillian\'s first big gig within JBoss.',
    likes: 0,
    retweets: 2,
    tags: ['arquillian', 'arquillian']
  },
  {
    id: '20820921235',
    created: 1281470246000,
    type: 'post',
    text: 'Here\'s a feature idea for IM clients/protocols. Reply to a line, sort of like in Twitter replies. That way, you don\'t get misunderstood.',
    likes: 0,
    retweets: 0
  },
  {
    id: '20820690072',
    created: 1281470011000,
    type: 'post',
    text: 'I don\'t like the IM status "Available". I\'m really not available. I\'m married. My IM status is "in front of my computer, taking on work."',
    likes: 0,
    retweets: 1
  },
  {
    id: '20817113512',
    created: 1281466473000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/unitygirl" rel="noopener noreferrer" target="_blank">@unitygirl</a> Congrats! We are lucky to have you in that position!<br><br>In reply to: <a href="https://x.com/wow3community/status/20816844075" rel="noopener noreferrer" target="_blank">@wow3community</a> <span class="status">20816844075</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '20811662067',
    created: 1281461243000,
    type: 'post',
    text: 'Exciting to see Drama show off his pride for #Triumph, my soon-to-be brother-in-law\'s employer, sporting the t-shirt on #Entourage.',
    likes: 0,
    retweets: 0,
    tags: ['triumph', 'entourage']
  },
  {
    id: '20811421503',
    created: 1281461020000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/papa_z" rel="noopener noreferrer" target="_blank">@papa_z</a> I don\'t mind having to buy a phone. #t-mobile has to make money like everyone else. I just want honesty and transparency!<br><br>In reply to: @Papa_Z <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['t']
  },
  {
    id: '20811363717',
    created: 1281460964000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> I have a lot of confidence in Lenevo hardware. True, I\'ve gotten some duds, but overall quite happy. Gotta run Linux :)<br><br>In reply to: <a href="https://x.com/jeremynorris/status/20732361322" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">20732361322</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '20811268768',
    created: 1281460879000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Yeah, it\'s a tough call because you want to be frugal, yet I\'ve been too frugal in the past and gotten unstable hardware.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '20759045053',
    created: 1281407537000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SBryzak" rel="noopener noreferrer" target="_blank">@SBryzak</a> That will be on my Kindle Android app very soon.<br><br>In reply to: @sbryzak <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '20730774977',
    created: 1281381550000,
    type: 'post',
    text: 'My wife needs a laptop to replace her battered &amp; beaten one. Dell Inspiron 15n vs Lenovo IdeaPad Y series. Worth paying for ATI gfx card?',
    likes: 0,
    retweets: 0
  },
  {
    id: '20729256729',
    created: 1281379919000,
    type: 'post',
    text: '#Seam 2.2.1.CR2 is available, bringing<br>security and bug fixes found in CR1. <a href="https://sourceforge.net/projects/jboss/files/JBoss%20Seam/2.2.1.CR2" rel="noopener noreferrer" target="_blank">sourceforge.net/projects/jboss/files/JBoss%20Seam/2.2.1.CR2</a>',
    likes: 0,
    retweets: 3,
    tags: ['seam']
  },
  {
    id: '20728536959',
    created: 1281379142000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> myTouch 3G is my wife\'s phone, which she got at the end of last summer. I just assumed she was getting the upgrade.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '20728103686',
    created: 1281378680000,
    type: 'post',
    text: 'I\'ll be talking about the design of #Arquillian at the NoVa Testing Automation Group Sept 8 <a href="http://novataigsept2010.eventbrite.com" rel="noopener noreferrer" target="_blank">novataigsept2010.eventbrite.com</a>',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '20718094731',
    created: 1281369191000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SanneGrinovero" rel="noopener noreferrer" target="_blank">@SanneGrinovero</a> I figure the wifi will be supported in the final release, right now just an RC. I\'ll try it when everything is functional.<br><br>In reply to: <a href="https://x.com/SanneGrinovero/status/20685736457" rel="noopener noreferrer" target="_blank">@SanneGrinovero</a> <span class="status">20685736457</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '20718003233',
    created: 1281369111000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/papa_z" rel="noopener noreferrer" target="_blank">@papa_z</a> I\'ve had G1 for just over a year, my wife the myTouch 3G for a year. Now trying to figure out how to get to Android 2.2 w/o paying.<br><br>In reply to: @Papa_Z <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '20657906383',
    created: 1281305319000,
    type: 'post',
    text: 'I already planned to root my G1 with a cyanogenmod built to get Android 2.2, but now I need to do the same with my wife\'s myTouch 3G.',
    likes: 0,
    retweets: 0
  },
  {
    id: '20657848508',
    created: 1281305253000,
    type: 'post',
    text: 'If I had an office at a cool internet/startup company, I would definitely put this couch in it.',
    photos: ['<div class="entry"><img class="photo" src="media/20657848508.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '20649599995',
    created: 1281295708000,
    type: 'post',
    text: 'I\'m so pissed at <a class="mention" href="https://x.com/tmobile_usa" rel="noopener noreferrer" target="_blank">@tmobile_usa</a> for being so opaque about the Android 2.2 updates. Customer service just told me no update for myTouch 3G.',
    likes: 0,
    retweets: 0
  },
  {
    id: '20441827891',
    created: 1281069455000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> Then it wouldn\'t really be request-scoped. Sounds like a good experiment for a custom scope. Perhaps a variation on view-scoped.<br><br>In reply to: <a href="https://x.com/JohnAment/status/20319711469" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">20319711469</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '20412802916',
    created: 1281038365000,
    type: 'post',
    text: 'Weld core, servlet, docs, and examples are migrating to Git NOW (as announced by Pete on weld-dev).',
    likes: 0,
    retweets: 0
  },
  {
    id: '20412327567',
    created: 1281037859000,
    type: 'post',
    text: 'Another showing for #Arquillian at #JavaOne10 - Testing Java EE 6 Applications: Tools and Techniques - <a href="http://www.eventreg.com/cc250/sessionDetail.jsp?SID=313039" rel="noopener noreferrer" target="_blank">www.eventreg.com/cc250/sessionDetail.jsp?SID=313039</a>',
    likes: 1,
    retweets: 2,
    tags: ['arquillian', 'javaone10']
  },
  {
    id: '20412136341',
    created: 1281037669000,
    type: 'post',
    text: 'I\'ll be speaking at the NFJS in Raleigh, NC at the end of August. <a href="http://www.nofluffjuststuff.com/conference/raleigh/2010/08/home" rel="noopener noreferrer" target="_blank">www.nofluffjuststuff.com/conference/raleigh/2010/08/home</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '20411264202',
    created: 1281036683000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> Does #Android 2.2 have the option to send a contact via e-mail or SMS?<br><br>In reply to: <a href="https://x.com/rruss/status/20406995872" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">20406995872</span>',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '20411221993',
    created: 1281036638000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> Sweeeeeeeeet. I can\'t wait to get my hands on #Android 2.2 one way or another.<br><br>In reply to: <a href="https://x.com/rruss/status/20406995872" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">20406995872</span>',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '20411153182',
    created: 1281036567000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> It means it must be summertime ;)<br><br>In reply to: <a href="https://x.com/lincolnthree/status/20410569400" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">20410569400</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '20411142338',
    created: 1281036556000,
    type: 'post',
    text: '#MyFaces is likely to be a built-in #JSF provider in JBoss AS 6. Get the story here: <a href="http://markmail.org/search/?q=list:org.apache.myfaces.dev#query:list%3Aorg.apache.myfaces.dev+page:1+mid:2ynnvcwgbepvywj3+state:results" rel="noopener noreferrer" target="_blank">markmail.org/search/?q=list:org.apache.myfaces.dev#query:list%3Aorg.apache.myfaces.dev+page:1+mid:2ynnvcwgbepvywj3+state:results</a>',
    likes: 0,
    retweets: 2,
    tags: ['myfaces', 'jsf']
  },
  {
    id: '20321486355',
    created: 1280944026000,
    type: 'post',
    text: 'Having lunch with my bro, who is home from Italy.',
    likes: 0,
    retweets: 0
  },
  {
    id: '20317658916',
    created: 1280940380000,
    type: 'post',
    text: 'President Obama\'s birthday is Aug. 4. Sign his birthday card alongside the First Lady. <a href="http://my.barackobama.com/page/content/barackbirthday" rel="noopener noreferrer" target="_blank">my.barackobama.com/page/content/barackbirthday</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '20251595400',
    created: 1280871157000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jganoff" rel="noopener noreferrer" target="_blank">@jganoff</a> Good work!<br><br>In reply to: <a href="https://x.com/jganoff/status/20192040650" rel="noopener noreferrer" target="_blank">@jganoff</a> <span class="status">20192040650</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '19861645230',
    created: 1280444193000,
    type: 'reply',
    text: '@brianleathem How about if they submit #Arquillian test cases? That would be a good trade-off. Mini test apps so to speak.<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '19851471715',
    created: 1280433827000,
    type: 'post',
    text: 'Discovered the game #KanJam the week before vacation. Was a huge hit, esp on the beach (once I got folks to agree to play) <a href="http://kanjam.com" rel="noopener noreferrer" target="_blank">kanjam.com</a>',
    likes: 1,
    retweets: 0,
    tags: ['kanjam']
  },
  {
    id: '19845252993',
    created: 1280427366000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jvassbo" rel="noopener noreferrer" target="_blank">@jvassbo</a> Here\'s Ike\'s reaction: <a href="http://design.jboss.org/arquillian/logo/ui/images/success/arquillian_ui_success_256px.png" rel="noopener noreferrer" target="_blank">design.jboss.org/arquillian/logo/ui/images/success/arquillian_ui_success_256px.png</a><br><br>In reply to: <a href="https://x.com/jvassbo/status/19746019697" rel="noopener noreferrer" target="_blank">@jvassbo</a> <span class="status">19746019697</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '19845147524',
    created: 1280427264000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/evanchooly" rel="noopener noreferrer" target="_blank">@evanchooly</a> I talk about aliens, astronomy and robots ;)<br><br>In reply to: <a href="https://x.com/evanchooly/status/19831268894" rel="noopener noreferrer" target="_blank">@evanchooly</a> <span class="status">19831268894</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '19840230217',
    created: 1280422551000,
    type: 'post',
    text: 'I can\'t live without Places Directory on #Android. But it\'s being constrained because it can\'t paginate beyond 20 results. Why???',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '19839941785',
    created: 1280422286000,
    type: 'post',
    text: 'I\'m really starting to get disappointed about the lack of improvement to the #Android Gmail client. The shortcomings are piling up.',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '19835483111',
    created: 1280418379000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Amen! We are really slamming into the limitation lately at #JBoss.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/19834046291" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">19834046291</span>',
    likes: 0,
    retweets: 0,
    tags: ['jboss']
  },
  {
    id: '19835388868',
    created: 1280418299000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> is currently converting #Weld tests over to #Arquillian. The revolution is happening. Pardon the dust.',
    likes: 0,
    retweets: 0,
    tags: ['weld', 'arquillian']
  },
  {
    id: '19830994903',
    created: 1280414666000,
    type: 'post',
    text: 'Message from my family member followers: "Make more sense." Hahaha.',
    likes: 0,
    retweets: 0
  },
  {
    id: '19830828282',
    created: 1280414525000,
    type: 'post',
    text: 'Gradually getting back into the swing of things after a vacation and some personal days.',
    likes: 0,
    retweets: 0
  },
  {
    id: '19685634812',
    created: 1280264343000,
    type: 'post',
    text: 'Just saw the sequential turn light on the new Mustang. Night rider style. Cool.',
    likes: 0,
    retweets: 0
  },
  {
    id: '19591666879',
    created: 1280167275000,
    type: 'post',
    text: 'Just discovered <a href="http://www.appbrain.com" rel="noopener noreferrer" target="_blank">www.appbrain.com</a> to browse and install #Android apps. Sweet.',
    likes: 0,
    retweets: 1,
    tags: ['android']
  },
  {
    id: '19587799832',
    created: 1280163475000,
    type: 'reply',
    text: '@brianleathem It\'s for the best because it enforces best practices w/ Maven. Think of it as running Perl in strict mode ;)<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '19587739565',
    created: 1280163417000,
    type: 'post',
    text: 'Ah, a restart of my phone solved my font problem. Must have been a transient side-effect of installing some app. Moving on...',
    likes: 0,
    retweets: 0
  },
  {
    id: '19587377631',
    created: 1280163081000,
    type: 'post',
    text: 'For some reason, the font in my Google &amp; Market search box on #Android G1 changed to a courier-like monospace. Can\'t figure out why.',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '19585841680',
    created: 1280161669000,
    type: 'post',
    text: 'Back from vacation, sort of. I had a lot of decompressing to do. Still need more sand &amp; sun. I did managed to squeeze out a golf round of 92',
    likes: 0,
    retweets: 0
  },
  {
    id: '19585665413',
    created: 1280161507000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sten_aksel" rel="noopener noreferrer" target="_blank">@sten_aksel</a> You know it. We definitely work to please. I tried to push out a release before vacation, but got snagged in QA. Back at it.<br><br>In reply to: <a href="https://x.com/sten_aksel/status/18102208740" rel="noopener noreferrer" target="_blank">@sten_aksel</a> <span class="status">18102208740</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '19585466809',
    created: 1280161325000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joeltosi" rel="noopener noreferrer" target="_blank">@joeltosi</a> <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> I love talking tech at 12000 m (35000 ft for you non-metric folks). Makes for a quick flight and fun brainstorming.<br><br>In reply to: <a href="https://x.com/joeltosi/status/19572000799" rel="noopener noreferrer" target="_blank">@joeltosi</a> <span class="status">19572000799</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '19585328077',
    created: 1280161200000,
    type: 'post',
    text: 'We are taking the spirit of continuous integration to a new level. RT <a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> Ike goes emotional! <a href="http://twurl.nl/8wxks6" rel="noopener noreferrer" target="_blank">twurl.nl/8wxks6</a> #Arquillian',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '19585164427',
    created: 1280161049000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Awesome!<br><br>In reply to: <a href="https://x.com/ALRubinger/status/19311288070" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">19311288070</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '19585120489',
    created: 1280161010000,
    type: 'reply',
    text: '@brianleathem Join the #jbosstesting IRC channel and we\'ll give you a personal lesson :) It\'s really much easier than it looks, I promise.<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['jbosstesting']
  },
  {
    id: '19584447549',
    created: 1280160413000,
    type: 'post',
    text: 'I\'m psyched, there\'s now a CyanogenMod RC of #Android 2.2 for G1 (HTC Dream). With no hope in sight for t-mobile upgrade, I\'m in when final.',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '19523272420',
    created: 1280094325000,
    type: 'post',
    text: 'I picked up the Kindle app for #Android while on the beach. Love It. Hello books.',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '19521079426',
    created: 1280091719000,
    type: 'post',
    text: 'Hazards do not give you license to park whereever you please.',
    likes: 0,
    retweets: 0
  },
  {
    id: '19517204129',
    created: 1280087046000,
    type: 'post',
    text: 'The sky just broke and the rain is pouring down in sheets. Guess I\'m hanging a Panera a little longer ;)',
    photos: ['<div class="entry"><img class="photo" src="media/19517204129.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '19451648294',
    created: 1280011836000,
    type: 'post',
    text: 'Why do bagel shops make it so freakin\' difficult to locate the list of cream cheeses? As if everyone knows what flavors there are.',
    likes: 0,
    retweets: 0
  },
  {
    id: '19305075428',
    created: 1279853284000,
    type: 'post',
    text: 'In Virginia Beach drinkin it up at bachelor party w/ my future brother-in-law and cousins at some freaky Irish/country karoke night bar.',
    likes: 0,
    retweets: 0
  },
  {
    id: '19221738163',
    created: 1279767207000,
    type: 'post',
    text: 'On the beach, my cousin told her son he could pee in the ocean. So he walks up to the edge of the surf, drops his drawers &amp; fires away.',
    likes: 1,
    retweets: 0
  },
  {
    id: '19016689674',
    created: 1279653757000,
    type: 'post',
    text: 'Hard at work',
    photos: ['<div class="entry"><img class="photo" src="media/19016689674.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '18783287724',
    created: 1279390587000,
    type: 'reply',
    text: '@iiamhydrogen Definitely a whore. Haha, j/k. <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> is a brilliant visionary the the utmost way.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/18748653102" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">18748653102</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18738700913',
    created: 1279337995000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> Hehe, that was out of context. I should have said "a single blog entry is not sufficient for documenting a whole project" :)<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/18717306119" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">18717306119</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18717166079',
    created: 1279316017000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Thanks! The plugin would be even more useful if it could execute asadmin commands after startup (e.g., to add a datasource)<br><br>In reply to: <a href="https://x.com/jasondlee/status/18717024991" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">18717024991</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18716480047',
    created: 1279315236000,
    type: 'post',
    text: 'A blog entry is not documentation.',
    likes: 0,
    retweets: 1
  },
  {
    id: '18716419370',
    created: 1279315169000,
    type: 'post',
    text: 'Hey #GlassFish team, would you mind creating a project page for the Embedded GlassFish Maven plugin w/ documentation?',
    likes: 0,
    retweets: 0,
    tags: ['glassfish']
  },
  {
    id: '18714872051',
    created: 1279313428000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RKela" rel="noopener noreferrer" target="_blank">@RKela</a> The interview by itself is here: <a href="http://community.jboss.org/people/dan.j.allen/blog/2010/06/10/dont-overlook-this-framework" rel="noopener noreferrer" target="_blank">community.jboss.org/people/dan.j.allen/blog/2010/06/10/dont-overlook-this-framework</a> #Seam #JavaEE<br><br>In reply to: <a href="https://x.com/RKela/status/18714101969" rel="noopener noreferrer" target="_blank">@RKela</a> <span class="status">18714101969</span>',
    likes: 0,
    retweets: 0,
    tags: ['seam', 'javaee']
  },
  {
    id: '18713850063',
    created: 1279312280000,
    type: 'post',
    text: 'I have an idea. Let\'s put the people in the US losing their house into the empty spaces in the ghost city of Ordos, China.',
    likes: 0,
    retweets: 1
  },
  {
    id: '18712563426',
    created: 1279310878000,
    type: 'post',
    text: '#Seam 3 got 11+ pages in the German JavaMagazin this month: <a href="http://it-republik.de/jaxenter/java-magazin-ausgaben/Java-EE-6-und-Seam-000405.html" rel="noopener noreferrer" target="_blank">it-republik.de/jaxenter/java-magazin-ausgaben/Java-EE-6-und-Seam-000405.html</a> including my interview in German',
    likes: 0,
    retweets: 1,
    tags: ['seam']
  },
  {
    id: '18711486389',
    created: 1279309727000,
    type: 'post',
    text: 'If you were going to donate money to promote open source and education, where would that money go?',
    likes: 0,
    retweets: 0
  },
  {
    id: '18708726539',
    created: 1279306866000,
    type: 'post',
    text: 'I got in trouble last night plotting with <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> without our other leader, <a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a>. There\'s gonna be a hangin\'. #Arquillian',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '18708530836',
    created: 1279306631000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iamroberthanson" rel="noopener noreferrer" target="_blank">@iamroberthanson</a> Haha. Actually, that was 2 years ago, my last real vacation. This year it\'s all fun and games ;)<br><br>In reply to: <a href="https://x.com/iamroberthanson/status/18692117894" rel="noopener noreferrer" target="_blank">@iamroberthanson</a> <span class="status">18692117894</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18658271026',
    created: 1279253977000,
    type: 'post',
    text: 'I saw Santa Claus in Panera Bread today. He apparently believed he was from the North Pole, but the rest doubted he was on this planet.',
    likes: 0,
    retweets: 0
  },
  {
    id: '18658146590',
    created: 1279253853000,
    type: 'post',
    text: 'One more work day until vacation starts. So if you are wondering next week why my tweets have sand in them, well, you\'ll know ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '18628967661',
    created: 1279225523000,
    type: 'post',
    text: 'I\'d say the main reason I miss submitting for conferences is because I don\'t hear about them in time (travel funding is a separate issue).',
    likes: 0,
    retweets: 0
  },
  {
    id: '18628448912',
    created: 1279224971000,
    type: 'post',
    text: 'It\'s fun to read a retweet, guess who retweeted it, then see if you are right ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '18628385627',
    created: 1279224906000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cbredesen" rel="noopener noreferrer" target="_blank">@cbredesen</a> <a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Ah, right. Just off by default. Preferences &gt; General &gt; Workspace &gt; Refresh automatically<br><br>In reply to: <a href="https://x.com/cbredesen/status/18595805835" rel="noopener noreferrer" target="_blank">@cbredesen</a> <span class="status">18595805835</span>',
    likes: 0,
    retweets: 1
  },
  {
    id: '18628093896',
    created: 1279224595000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> The way that NetBeans keeps in sync with the filesystem and changes to the Maven POM perfectly matches my expectations.<br><br>In reply to: <a href="https://x.com/jasondlee/status/18600314056" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">18600314056</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18625451590',
    created: 1279221801000,
    type: 'post',
    text: 'I have a great browser/browser plugin idea. Right-click, open link to closest anchor. I always have to view the source to find an anchor.',
    likes: 0,
    retweets: 0
  },
  {
    id: '18623710411',
    created: 1279220013000,
    type: 'post',
    text: 'Best time of year! RT <a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a>: Spotted in Cambridge, MA this morning. <a class="mention" href="https://x.com/mojavelinux" rel="noopener noreferrer" target="_blank">@mojavelinux</a> #devoxx',
    photos: ['<div class="entry"><img class="photo" src="media/18623710411.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '18623234713',
    created: 1279219518000,
    type: 'post',
    text: 'I wish there were a way to link a contact between two Google accounts. For instance, if my wife and I have same accountant, doctor, etc.',
    likes: 0,
    retweets: 0
  },
  {
    id: '18581852732',
    created: 1279175003000,
    type: 'post',
    text: '#Eclipse, yes I want you to refresh the file. That\'s more useful than showing me a blank view and making me click around like your bitch.',
    likes: 0,
    retweets: 0,
    tags: ['eclipse']
  },
  {
    id: '18541479104',
    created: 1279134122000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> That JDK 6 change affected so many projects. Makes you wonder if it would be better if non-ordered sets were really random.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/18539566012" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">18539566012</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18536192618',
    created: 1279128913000,
    type: 'post',
    text: 'Want to understand what\'s going on in Seam SVN? <a href="http://seamframework.org/Seam3/BuildSystemArchitecture" rel="noopener noreferrer" target="_blank">seamframework.org/Seam3/BuildSystemArchitecture</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18535965945',
    created: 1279128700000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a>, @brianleathem, <a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> get a forum, as in "get a room" :) Sounds like you have a lot to discuss.',
    likes: 0,
    retweets: 0
  },
  {
    id: '18535794954',
    created: 1279128544000,
    type: 'reply',
    text: '@brianleathem <a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> JBoss is all in on Maven3 (where we had Maven2; whether projects are eval\'ing other build tools is another story)<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18535686399',
    created: 1279128442000,
    type: 'reply',
    text: '@brianleathem <a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> According to Maven experts (<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a>), Maven3 is just as stable &amp; complete (maybe more stable) than Maven2.<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18524451728',
    created: 1279118727000,
    type: 'post',
    text: 'Central Maryland JUG talk tonight on #Arquillian at Johns Hopkins APL at 6pm. Come learn about container-oriented testing in Java.',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '18481639434',
    created: 1279070640000,
    type: 'post',
    text: 'Finally, #Ecllipse Helios supports static imports without a hassle.',
    likes: 0,
    retweets: 1,
    tags: ['ecllipse']
  },
  {
    id: '18469514679',
    created: 1279058643000,
    type: 'post',
    text: 'Btw, Subclipse is the top #Eclipse plugin in the Popular tab. So how about just making it part of the #Eclipse distribution :)',
    likes: 0,
    retweets: 0,
    tags: ['eclipse', 'eclipse']
  },
  {
    id: '18469269331',
    created: 1279058405000,
    type: 'post',
    text: 'Tried out the #Eclipse Marketplace from with Eclipse for the first time. Now that\'s what I\'m talking about. Easy Subclipse install FTW!',
    likes: 1,
    retweets: 0,
    tags: ['eclipse']
  },
  {
    id: '18462341225',
    created: 1279051074000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> The problem w/ .xhtml is that the markup isn\'t XHTML. It\'s an XML-based template language that *may* produce HTML.<br><br>In reply to: <a href="https://x.com/maxandersen/status/18454734357" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">18454734357</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18457697650',
    created: 1279046291000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> .jsf is the common servlet mapping so I\'m not sure that would work. I\'d be happy with .face. Can it be customized in jbt?<br><br>In reply to: <a href="https://x.com/maxandersen/status/18454734357" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">18454734357</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18457516243',
    created: 1279046105000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Either that or watch the user guide disappear ;)<br><br>In reply to: <a href="https://x.com/ALRubinger/status/18456356801" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">18456356801</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18457352777',
    created: 1279045937000,
    type: 'post',
    text: 'Can you type your password? Can you type your password on your phone? Just something to keep in mind. Doesn\'t always translate well.',
    likes: 0,
    retweets: 0
  },
  {
    id: '18454049150',
    created: 1279042703000,
    type: 'post',
    text: 'Google Voice should require that the person calling state their name or hang up on them. I don\'t want a blank name. Then I just hang up.',
    likes: 0,
    retweets: 0
  },
  {
    id: '18452466997',
    created: 1279041223000,
    type: 'post',
    text: 'I really which in #JSF 2.0, the file extension for Facelets templates defaulted to .view.xml rather than .xhtml.',
    likes: 0,
    retweets: 0,
    tags: ['jsf']
  },
  {
    id: '18452287869',
    created: 1279041056000,
    type: 'post',
    text: 'If you\'re interested in a community.jboss.org, join the space &amp; enable email notifications. It\'s one click to turn notifications on or off.',
    likes: 0,
    retweets: 1
  },
  {
    id: '18451557357',
    created: 1279040401000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JavaOneConf" rel="noopener noreferrer" target="_blank">@JavaOneConf</a> Great!<br><br>In reply to: <a href="https://x.com/JavaOne/status/18450920983" rel="noopener noreferrer" target="_blank">@JavaOne</a> <span class="status">18450920983</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18449626561',
    created: 1279038672000,
    type: 'post',
    text: 'Congrats to Nicklas Karlsson, an esteemed Seam community member! RT <a class="mention" href="https://x.com/icefaces" rel="noopener noreferrer" target="_blank">@icefaces</a> 2010 ICEfaces Blog Contest Winners! <a href="http://www.icefaces.org/pages/blog_contest_winners-2010.html" rel="noopener noreferrer" target="_blank">www.icefaces.org/pages/blog_contest_winners-2010.html</a>',
    likes: 0,
    retweets: 2
  },
  {
    id: '18443307140',
    created: 1279033294000,
    type: 'post',
    text: 'Red Hat IT finally granted by request to enlarge the font size in our document management system. Story of vanity vs accessibility.',
    likes: 0,
    retweets: 0
  },
  {
    id: '18404498822',
    created: 1278989678000,
    type: 'post',
    text: 'Don\'t miss out on Early Bird Rates for #NFJS in MD, Aug 13 - 15. Prices increase after Monday, July 19. <a href="http://www.nofluffjuststuff.com/conference/columbia/2010/08" rel="noopener noreferrer" target="_blank">www.nofluffjuststuff.com/conference/columbia/2010/08</a>',
    likes: 0,
    retweets: 0,
    tags: ['nfjs']
  },
  {
    id: '18390145149',
    created: 1278976869000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> Embrace it ;)<br><br>In reply to: <a href="https://x.com/matthewmccull/status/18296610695" rel="noopener noreferrer" target="_blank">@matthewmccull</a> <span class="status">18296610695</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18390092821',
    created: 1278976822000,
    type: 'post',
    text: 'Hey <a class="mention" href="https://x.com/JavaOneConf" rel="noopener noreferrer" target="_blank">@JavaOneConf</a>, you aren\'t deep linking to the actual presentation summaries when you tweet. Kind of useless. #javaone10',
    likes: 0,
    retweets: 0,
    tags: ['javaone10']
  },
  {
    id: '18382284457',
    created: 1278968930000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/paulojeronimo" rel="noopener noreferrer" target="_blank">@paulojeronimo</a> Can you post on the #Arquillian forum asking how to contribute a translation? Then we\'ll build out a guide from there.<br><br>In reply to: <a href="https://x.com/paulojeronimo/status/18289044864" rel="noopener noreferrer" target="_blank">@paulojeronimo</a> <span class="status">18289044864</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '18382057489',
    created: 1278968679000,
    type: 'reply',
    text: '@translucent_eye A link would be helpful, wouldn\'t it? <a href="http://cmjugmaryland.eventbrite.com" rel="noopener noreferrer" target="_blank">cmjugmaryland.eventbrite.com</a><br><br>In reply to: <a href="https://x.com/davidsachdev/status/18352291843" rel="noopener noreferrer" target="_blank">@davidsachdev</a> <span class="status">18352291843</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18381967046',
    created: 1278968581000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> Yeah, that would be next recommendation. gtk-recordMyDesktop works great on Linux. We are going to use it for the #Arquillian demo.<br><br>In reply to: <a href="https://x.com/rruss/status/18381306743" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">18381306743</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '18381925130',
    created: 1278968536000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AndroidDev" rel="noopener noreferrer" target="_blank">@AndroidDev</a> Cool. #Android and #Seam sitting in a tree.<br><br>In reply to: <a href="https://x.com/AndroidDev/status/18381042208" rel="noopener noreferrer" target="_blank">@AndroidDev</a> <span class="status">18381042208</span>',
    likes: 0,
    retweets: 1,
    tags: ['android', 'seam']
  },
  {
    id: '18380869435',
    created: 1278967377000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> Yeah, let me get my video production team on that ;) I\'m amazed I even worked out how to get the slides in there.<br><br>In reply to: <a href="https://x.com/rruss/status/18379299076" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">18379299076</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18375030461',
    created: 1278960995000,
    type: 'post',
    text: 'Here\'s the video of <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a>\'s talk on testing Java EE w/ #ShrinkWrap #Arquillian that I created in kdenlive <a href="http://vimeo.com/13193802" rel="noopener noreferrer" target="_blank">vimeo.com/13193802</a>',
    likes: 2,
    retweets: 1,
    tags: ['shrinkwrap', 'arquillian']
  },
  {
    id: '18336078876',
    created: 1278917516000,
    type: 'post',
    text: 'We document the #Arquillian roadmap in JIRA: <a href="https://jira.jboss.org/browse/ARQ" rel="noopener noreferrer" target="_blank">jira.jboss.org/browse/ARQ</a>',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '18336038971',
    created: 1278917459000,
    type: 'post',
    text: 'Only two days until my talk on #Arquillian at my hometown JUG in Maryland (Wed). I\'ve got to fire up those demos to get ready! #cmjug',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'cmjug']
  },
  {
    id: '18334021054',
    created: 1278914695000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Doh! You guys wouldn\'t make it very far as criminals ;)<br><br>In reply to: <a href="https://x.com/ALRubinger/status/18330843409" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">18330843409</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18324678634',
    created: 1278904396000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/evanchooly" rel="noopener noreferrer" target="_blank">@evanchooly</a> Nope, instead I just decided to work on #Arquillian and play some golf ;)<br><br>In reply to: <a href="https://x.com/evanchooly/status/18295268422" rel="noopener noreferrer" target="_blank">@evanchooly</a> <span class="status">18295268422</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '18305311298',
    created: 1278884298000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sten_aksel" rel="noopener noreferrer" target="_blank">@sten_aksel</a> Of course, as soon as we get Alpha3 out, then it will really get easier :)<br><br>In reply to: <a href="https://x.com/sten_aksel/status/18304888137" rel="noopener noreferrer" target="_blank">@sten_aksel</a> <span class="status">18304888137</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18294449955',
    created: 1278876726000,
    type: 'post',
    text: 'I want to do my expense reports, but my VPN token isn\'t working and I can\'t answer my own questions to reset it. #frustrating',
    likes: 0,
    retweets: 0,
    tags: ['frustrating']
  },
  {
    id: '18288641430',
    created: 1278871807000,
    type: 'post',
    text: 'I\'m definitely a Javaholic. <a href="http://devoxx.com/display/Devoxx2K10/Javaholics" rel="noopener noreferrer" target="_blank">devoxx.com/display/Devoxx2K10/Javaholics</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18288386842',
    created: 1278871563000,
    type: 'post',
    text: 'Never send a save the date card and leave off the date. I\'m just saying.',
    likes: 0,
    retweets: 0
  },
  {
    id: '18287254701',
    created: 1278870473000,
    type: 'post',
    text: 'As part of the work we do on #Arquillian, I\'d like to reinvest in the unit testing frameworks. <a href="http://community.jboss.org/docs/DOC-15562" rel="noopener noreferrer" target="_blank">community.jboss.org/docs/DOC-15562</a>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '18286865949',
    created: 1278870092000,
    type: 'post',
    text: 'I just updated the #Arquillian downloads page to make it easier to get started. <a href="http://jboss.org/arquillian/downloads.html" rel="noopener noreferrer" target="_blank">jboss.org/arquillian/downloads.html</a>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '18285478393',
    created: 1278868754000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/paulojeronimo" rel="noopener noreferrer" target="_blank">@paulojeronimo</a> Any interest in translating the Arquillian reference guide? Perhaps wait until beta, though.<br><br>In reply to: <a href="https://x.com/paulojeronimo/status/18149677923" rel="noopener noreferrer" target="_blank">@paulojeronimo</a> <span class="status">18149677923</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18285443052',
    created: 1278868721000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/paulojeronimo" rel="noopener noreferrer" target="_blank">@paulojeronimo</a> Here\'s a poor man\'s translation courtesy Google Translate <a href="http://translate.google.com/translate?js=y&amp;prev=_t&amp;hl=en&amp;ie=UTF-8&amp;layout=1&amp;eotf=1&amp;u=http://a.ladoservidor.com/tutoriais/arquillian-shrinkwrap/index.html&amp;sl=pt&amp;tl=en" rel="noopener noreferrer" target="_blank">translate.google.com/translate?js=y&amp;prev=_t&amp;hl=en&amp;ie=UTF-8&amp;layout=1&amp;eotf=1&amp;u=http://a.ladoservidor.com/tutoriais/arquillian-shrinkwrap/index.html&amp;sl=pt&amp;tl=en</a><br><br>In reply to: <a href="https://x.com/paulojeronimo/status/18149677923" rel="noopener noreferrer" target="_blank">@paulojeronimo</a> <span class="status">18149677923</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18285168082',
    created: 1278868466000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/paulojeronimo" rel="noopener noreferrer" target="_blank">@paulojeronimo</a> This is brilliant. Any chance someone could make a translation available as well (at least the article part).<br><br>In reply to: <a href="https://x.com/paulojeronimo/status/18150760173" rel="noopener noreferrer" target="_blank">@paulojeronimo</a> <span class="status">18150760173</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18285120210',
    created: 1278868422000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/sten_aksel" rel="noopener noreferrer" target="_blank">@sten_aksel</a> You can build the Weld archetype for Java EE 6 from trunk in the meantime: <a href="http://anonsvn.jboss.org/repos/weld/archetypes/trunk/javaee6-webapp-src/" rel="noopener noreferrer" target="_blank">anonsvn.jboss.org/repos/weld/archetypes/trunk/javaee6-webapp-src/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18285008741',
    created: 1278868319000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sten_aksel" rel="noopener noreferrer" target="_blank">@sten_aksel</a> You know it. Just have to resolve some repository layout issues and holding out for the #Arquillian 1.0.0.Alpha3 release. Soon!<br><br>In reply to: <a href="https://x.com/sten_aksel/status/18102208740" rel="noopener noreferrer" target="_blank">@sten_aksel</a> <span class="status">18102208740</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '18284910015',
    created: 1278868225000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sten_aksel" rel="noopener noreferrer" target="_blank">@sten_aksel</a> Hahaha. Ike, he\'s such a dictator :)<br><br>In reply to: <a href="https://x.com/sten_aksel/status/18105004191" rel="noopener noreferrer" target="_blank">@sten_aksel</a> <span class="status">18105004191</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18284804681',
    created: 1278868126000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Awesome. Moving to #cdi should enable you to uncover some ideas from the field to put into #seam 3. Nice.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/18244550413" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">18244550413</span>',
    likes: 0,
    retweets: 0,
    tags: ['cdi', 'seam']
  },
  {
    id: '18284646030',
    created: 1278867973000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Haha My wife asked me that yesterday :) Even as a kid, I dreaded that question. It\'s a euphemism for "why aren\'t you doing chores?"<br><br>In reply to: <a href="https://x.com/kito99/status/18256693762" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">18256693762</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18255818852',
    created: 1278834047000,
    type: 'post',
    text: 'The half of the day I wasn\'t relaxing and watching some baseball, I was tearing my hair out debugging Tomcat. In the end, I won the fight ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '18242662968',
    created: 1278818859000,
    type: 'post',
    text: 'When you\'re in the groove, you\'re in the groove. Take Steve Stricker\'s 54-hole PGA tour record weekend. Who\'s Tiger Woods again?',
    likes: 0,
    retweets: 0
  },
  {
    id: '18242334999',
    created: 1278818516000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Fight for what you believe in :)<br><br>In reply to: <a href="https://x.com/lightguardjp/status/18233128124" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">18233128124</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18242304150',
    created: 1278818485000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Haha. Sounds like my wife &amp; I :) Perhaps for every tech book, someone needs to read an actual story to keep world in balance.<br><br>In reply to: <a href="https://x.com/jasondlee/status/18239845832" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">18239845832</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18228855116',
    created: 1278803273000,
    type: 'post',
    text: 'It\'s that time of year. Nats games with mom!',
    photos: ['<div class="entry"><img class="photo" src="media/18228855116.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '18199386913',
    created: 1278771955000,
    type: 'post',
    text: '@swd847 You can setup #Arquillian to write artifacts to disk using deploymentExportPath in arquillian.xml, though it has issues.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '18198900206',
    created: 1278771469000,
    type: 'post',
    text: '@swd847 You are a superhero.',
    likes: 0,
    retweets: 0
  },
  {
    id: '18198819086',
    created: 1278771386000,
    type: 'post',
    text: 'Happy Birthday to <a class="mention" href="https://x.com/codylerum" rel="noopener noreferrer" target="_blank">@codylerum</a>, one of our newest Seam module contributors (mail, reporting)!',
    likes: 0,
    retweets: 1
  },
  {
    id: '18098299783',
    created: 1278657649000,
    type: 'post',
    text: 'Spent some more time tonight getting the next Weld archetypes worked out, soon to be named JBoss Maven Archetypes for Java EE 6 and CDI.',
    likes: 1,
    retweets: 0
  },
  {
    id: '18098186200',
    created: 1278657501000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> Oh goodness, I\'m starting a trend! Actually, I was very surprised that more repos don\'t do this. I looked around for examples.<br><br>In reply to: <a href="https://x.com/tech4j/status/18077150507" rel="noopener noreferrer" target="_blank">@tech4j</a> <span class="status">18077150507</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18098120582',
    created: 1278657415000,
    type: 'post',
    text: 'I worked out how to create a montage video w/ speaker on left and slides on right, in sync w/ the delivery using kdenlive. Link coming.',
    likes: 0,
    retweets: 0
  },
  {
    id: '18070923798',
    created: 1278631637000,
    type: 'post',
    text: 'I cleaned up the Seam SVN repo yesterday so that it\'s clear where stuff is: <a href="http://anonsvn.jboss.org/repos/seam/about.txt" rel="noopener noreferrer" target="_blank">anonsvn.jboss.org/repos/seam/about.txt</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '18060143657',
    created: 1278620103000,
    type: 'post',
    text: 'A feed for all my JBoss Community blog posts combined: <a href="http://community.jboss.org/blogs/feeds/users/dan.j.allen" rel="noopener noreferrer" target="_blank">community.jboss.org/blogs/feeds/users/dan.j.allen</a> Same for any member.',
    likes: 0,
    retweets: 1
  },
  {
    id: '18015590766',
    created: 1278571065000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maeste" rel="noopener noreferrer" target="_blank">@maeste</a> You look ready to change the world. And you make the hat look good. Shadowman would be jealous ;)<br><br>In reply to: <a href="https://x.com/maeste/status/17931148577" rel="noopener noreferrer" target="_blank">@maeste</a> <span class="status">17931148577</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17989210182',
    created: 1278544912000,
    type: 'post',
    text: 'Silicon Valley developers, come listen to Caucho talk about #JSR299 #CDI on Jul 21 @ the Googleplex <a href="https://www.meetup.com/sv-jug/events/12987711/" rel="noopener noreferrer" target="_blank">www.meetup.com/sv-jug/events/12987711/</a>',
    likes: 0,
    retweets: 2,
    tags: ['jsr299', 'cdi']
  },
  {
    id: '17988902659',
    created: 1278544594000,
    type: 'post',
    text: 'Border\'s just announced ebook store including an app for #Android. Sweet.',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '17986944496',
    created: 1278542561000,
    type: 'post',
    text: 'I\'m boiling in my house. Damn A/C kicked the bucket.',
    likes: 0,
    retweets: 0
  },
  {
    id: '17986637878',
    created: 1278542243000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sten_aksel" rel="noopener noreferrer" target="_blank">@sten_aksel</a> Sorry, we are behind in pushing out a new version. I\'ll put it at the top of my list to get done by next week.<br><br>In reply to: <a href="https://x.com/sten_aksel/status/17865545190" rel="noopener noreferrer" target="_blank">@sten_aksel</a> <span class="status">17865545190</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17986513124',
    created: 1278542113000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Actually, works well. I just didn\'t realize it was there.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/17953222224" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">17953222224</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17930322552',
    created: 1278485688000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> I second that! Very cool!<br><br>In reply to: <a href="https://x.com/maxandersen/status/17928383020" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">17928383020</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17913241498',
    created: 1278468093000,
    type: 'post',
    text: 'Why are the Rock Band 2 drums so expensive when they\'re so easy to break? I want the Drum Rocker set 4 Wii, but I\'m still waiting for it :(',
    likes: 0,
    retweets: 0
  },
  {
    id: '17897305568',
    created: 1278451416000,
    type: 'post',
    text: 'Doh! I spoke too soon. Google Docs does have a "Compare Checked" in the revision history. Works for me.',
    likes: 0,
    retweets: 0
  },
  {
    id: '17897156401',
    created: 1278451239000,
    type: 'post',
    text: 'Google Docs really needs some sort of track changes feature. Just a side-by-side diff would work for me.',
    likes: 0,
    retweets: 0
  },
  {
    id: '17886231485',
    created: 1278440528000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> And his/her twitter handle is...?<br><br>In reply to: <a href="https://x.com/maxandersen/status/17880263406" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">17880263406</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17886040846',
    created: 1278440335000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/datafront" rel="noopener noreferrer" target="_blank">@datafront</a> It\'s only if you sweat a ton. The microfibers can hold onto the bacteria. ~ Every year you need the detergent, but it works.<br><br>In reply to: <a href="https://x.com/datafront/status/17871728373" rel="noopener noreferrer" target="_blank">@datafront</a> <span class="status">17871728373</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17885363828',
    created: 1278439641000,
    type: 'post',
    text: 'Cool! Quickly search code in Maven central, JBoss releases and more, including #Arquillian and #ShrinkWrap <a href="http://grepcode.com" rel="noopener noreferrer" target="_blank">grepcode.com</a>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'shrinkwrap']
  },
  {
    id: '17880979834',
    created: 1278435395000,
    type: 'post',
    text: 'Ike, a developer magnet at JUDCon and JBoss World <a href="http://community.jboss.org/en/arquillian/blog/2010/07/03/ike-a-developer-magnet-at-judcon-jboss-world" rel="noopener noreferrer" target="_blank">community.jboss.org/en/arquillian/blog/2010/07/03/ike-a-developer-magnet-at-judcon-jboss-world</a> #Arquillian (retweet for those who were on vacation)',
    likes: 0,
    retweets: 2,
    tags: ['arquillian']
  },
  {
    id: '17834702820',
    created: 1278382884000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Hehehe. I guess it depends on where the wind blows.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/17833976965" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">17833976965</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17833148546',
    created: 1278381375000,
    type: 'post',
    text: 'I\'m really liking Under Armour clothing lately. Picked up a heatgear golf shirt the other day w/ 30 spf protection, and it worked.',
    likes: 0,
    retweets: 0
  },
  {
    id: '17823496263',
    created: 1278371158000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/mojavelinux" rel="noopener noreferrer" target="_blank">@mojavelinux</a> Or they filtered some of the extraneous content out for mobile clients.',
    likes: 0,
    retweets: 0
  },
  {
    id: '17823341053',
    created: 1278370992000,
    type: 'post',
    text: 'We wouldn\'t need a mobile app for every website if web developers/designers didn\'t put so much $#!% on the page.',
    likes: 0,
    retweets: 4
  },
  {
    id: '17814049706',
    created: 1278360369000,
    type: 'post',
    text: 'Jetty 7 container for #ShrinkWrap &amp; #Arquillian now committed. Hang tight while the Arquillian trunk is refactored gearing up for Alpha3.',
    likes: 0,
    retweets: 0,
    tags: ['shrinkwrap', 'arquillian']
  },
  {
    id: '17813952301',
    created: 1278360255000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> Sorry to hear that man! Perhaps today needs to be a sick day and tomorrow a vacation day. Or vice versa.<br><br>In reply to: <a href="https://x.com/tech4j/status/17800502596" rel="noopener noreferrer" target="_blank">@tech4j</a> <span class="status">17800502596</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17811489611',
    created: 1278357380000,
    type: 'post',
    text: 'Pic of Macy\'s fireworks spectacular: <a href="http://www.thingstoseenyc.com/wp-content/uploads/2009/06/macys-fireworks.jpg" rel="noopener noreferrer" target="_blank">www.thingstoseenyc.com/wp-content/uploads/2009/06/macys-fireworks.jpg</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17811413062',
    created: 1278357295000,
    type: 'post',
    text: 'The Macy\'s fireworks in NY were sick. Fired off from 6 ships in the middle of the Hudson river. Wish I could have seen that live.',
    likes: 0,
    retweets: 0
  },
  {
    id: '17810668417',
    created: 1278356450000,
    type: 'post',
    text: 'Internet at house is out of commission (phone of course, reliable as ever). More golf perhaps?',
    likes: 0,
    retweets: 0
  },
  {
    id: '17810609994',
    created: 1278356395000,
    type: 'post',
    text: 'Enjoyed a nice long, relaxing yesterday playing round of golf w/ my wife. She is doing awesome, my game needs work. But kept it \'net free.',
    likes: 0,
    retweets: 0
  },
  {
    id: '17810403444',
    created: 1278356156000,
    type: 'post',
    text: 'I love bringing together great minds. RT @swd847: New release of #fakereplace, now with MetaWidget support',
    likes: 0,
    retweets: 0,
    tags: ['fakereplace']
  },
  {
    id: '17698638117',
    created: 1278218338000,
    type: 'post',
    text: 'Ike, a developer magnet at JUDCon and JBoss World <a href="http://community.jboss.org/en/arquillian/blog/2010/07/03/ike-a-developer-magnet-at-judcon-jboss-world" rel="noopener noreferrer" target="_blank">community.jboss.org/en/arquillian/blog/2010/07/03/ike-a-developer-magnet-at-judcon-jboss-world</a> #Arquillian',
    likes: 0,
    retweets: 3,
    tags: ['arquillian']
  },
  {
    id: '17675262188',
    created: 1278188914000,
    type: 'post',
    text: '#Andiamo (the JBoss usability initiative) now has an official space in the JBoss Community <a href="http://community.jboss.org/groups/usability" rel="noopener noreferrer" target="_blank">community.jboss.org/groups/usability</a>',
    likes: 0,
    retweets: 5,
    tags: ['andiamo']
  },
  {
    id: '17630606688',
    created: 1278138100000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> You need to talk to <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> (or perhaps he needs to talk to you). Either way, get together.<br><br>In reply to: <a href="https://x.com/jeremynorris/status/17626530872" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">17626530872</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17604071996',
    created: 1278108723000,
    type: 'post',
    text: 'I created a slidecast from my #CDI, #Seam &amp; #RESTEasy presentation at Jazoon: <a href="http://slidesha.re/abwoes" rel="noopener noreferrer" target="_blank">slidesha.re/abwoes</a>',
    likes: 3,
    retweets: 4,
    tags: ['cdi', 'seam', 'resteasy']
  },
  {
    id: '17603370713',
    created: 1278107943000,
    type: 'post',
    text: 'Hey slideshare.net, why isn\'t the audio on a slidecast not working? Are you having some sort of outage?',
    likes: 0,
    retweets: 0
  },
  {
    id: '17596698868',
    created: 1278101122000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Absolutely. It is a JVM-level thing.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/17575356519" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">17575356519</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17555095298',
    created: 1278055796000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> If you report your finding on the Hot Deployment discussion forums, we\'ll owe you :)<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17555054095',
    created: 1278055736000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> That\'s a low fastball for #Arquillian ;)<br><br>In reply to: <a href="https://x.com/kito99/status/17549272143" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">17549272143</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '17526749449',
    created: 1278025319000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Talk to <a class="mention" href="https://x.com/kenglxn" rel="noopener noreferrer" target="_blank">@kenglxn</a> about rewards. I just pass on the information :)<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17524528664',
    created: 1278022871000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> I\'d reference what the KnowIT Objectnet devs did, cause they did a ton of reasearch <a href="http://code.google.com/p/seam-maven-refimpl/" rel="noopener noreferrer" target="_blank">code.google.com/p/seam-maven-refimpl/</a><br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17522488860',
    created: 1278020582000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/Google" rel="noopener noreferrer" target="_blank">@Google</a>, why do we have to submit source code to Google Code Search manually? Isn\'t that sort of a 90s approach? Can\'t you just find it?',
    likes: 0,
    retweets: 0
  },
  {
    id: '17518507300',
    created: 1278015968000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dzone" rel="noopener noreferrer" target="_blank">@dzone</a> Yawn. Everyone knows that vi is the faster editor in the world :) vi eats 2GB files for lunch.<br><br>In reply to: <a href="https://x.com/DZoneInc/status/17513524340" rel="noopener noreferrer" target="_blank">@DZoneInc</a> <span class="status">17513524340</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17518398211',
    created: 1278015842000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> Yeah, that was like "it only works on Pete\'s machine" deals. Hehehe, Pete\'s machine is magic.<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/17517024619" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">17517024619</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17509612951',
    created: 1278006516000,
    type: 'post',
    text: '#Arquillian alpha debuted last year at #Devoxx. We have our fingers crossed we can debut a far more mature beta (or release) this year.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'devoxx']
  },
  {
    id: '17507108814',
    created: 1278004225000,
    type: 'post',
    text: 'Ah. Now I\'ve got it. The #Arquillian ambassador is Ike.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '17507019157',
    created: 1278004146000,
    type: 'post',
    text: 'In just about every JUDCon/JBossWorld picture we took, #Arquillian is on a shirt, button or sticker. Ike is hot. The traveling gnome is not.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '17506451405',
    created: 1278003648000,
    type: 'post',
    text: 'I love how the gallery for JUDCon is my wife\'s picasa account. She\'s now an official community member <a href="http://jboss.org/events/JUDCon.html" rel="noopener noreferrer" target="_blank">jboss.org/events/JUDCon.html</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17505152179',
    created: 1278002514000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> The other thing would be to stop calling them slides, and start calling them activities. Oh no, I\'m an Android now!',
    likes: 0,
    retweets: 0
  },
  {
    id: '17492892476',
    created: 1277991545000,
    type: 'post',
    text: 'We\'re on for the #seam-dev IRC meeting today. See ya in 5m.',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '17472944050',
    created: 1277964177000,
    type: 'post',
    text: 'One of the things we\'re working on is nominating #Arquillian for a Duke\'s Choice Award. Who\'s with us?',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '17472863285',
    created: 1277964078000,
    type: 'post',
    text: 'I just got a crapload of things done. I\'ll call it the mid-week scramble. #gtd',
    likes: 0,
    retweets: 0,
    tags: ['gtd']
  },
  {
    id: '17472670898',
    created: 1277963842000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> babeswap is something <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> needs right about now ;)<br><br>In reply to: <a href="https://x.com/lightguardjp/status/17468646144" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">17468646144</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17467058231',
    created: 1277957642000,
    type: 'post',
    text: 'Btw, we couldn\'t deliver on the promise of more hot deployment in Seam 2 because the whole approach was flawed. We have a fresh approach ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '17466858043',
    created: 1277957448000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> 41 now! Thanks!<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17466442774',
    created: 1277957053000,
    type: 'post',
    text: 'Lots of talk about hot deployment starting at 40:00 in the JBoss Asylum podcast from JUDCon: <a href="http://asylum.libsyn.com/index.php?post_id=628137" rel="noopener noreferrer" target="_blank">asylum.libsyn.com/index.php?post_id=628137</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17466200299',
    created: 1277956836000,
    type: 'post',
    text: 'At the end of the last JBoss Asylum podcast, I announced the new JBoss Community Hot Deployment space: <a href="http://community.jboss.org/en/jbossas/hotdeployment" rel="noopener noreferrer" target="_blank">community.jboss.org/en/jbossas/hotdeployment</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17465659749',
    created: 1277956338000,
    type: 'post',
    text: 'I\'m up to 40 friends on community.jboss.org. Friends rule ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '17463912714',
    created: 1277954698000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/derkdukker" rel="noopener noreferrer" target="_blank">@derkdukker</a> Also, this is a good question/FAQ for the RichFaces space <a href="http://community.jboss.org/en/richfaces" rel="noopener noreferrer" target="_blank">community.jboss.org/en/richfaces</a><br><br>In reply to: <a href="https://x.com/derkdukker/status/17440829402" rel="noopener noreferrer" target="_blank">@derkdukker</a> <span class="status">17440829402</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17463808047',
    created: 1277954600000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/derkdukker" rel="noopener noreferrer" target="_blank">@derkdukker</a> You don\'t need dynamic form fields. Just binding inputs in each row to properties of object in a collection iterated by UIData.<br><br>In reply to: <a href="https://x.com/derkdukker/status/17440829402" rel="noopener noreferrer" target="_blank">@derkdukker</a> <span class="status">17440829402</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17460704557',
    created: 1277951706000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/deruelle" rel="noopener noreferrer" target="_blank">@deruelle</a> I love that though. The best part of the conference is the possibilities it unlocks ;) It\'s like finishing a level in a game.<br><br>In reply to: <a href="https://x.com/deruelle/status/17435276007" rel="noopener noreferrer" target="_blank">@deruelle</a> <span class="status">17435276007</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17460594127',
    created: 1277951602000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jganoff" rel="noopener noreferrer" target="_blank">@jganoff</a> That would be great. With all this archetype work, I sense a lessons learned/best practices wiki page formulating.<br><br>In reply to: <a href="https://x.com/jganoff/status/17440644764" rel="noopener noreferrer" target="_blank">@jganoff</a> <span class="status">17440644764</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17459618054',
    created: 1277950680000,
    type: 'post',
    text: 'Haha. Just saw this on the Infinispan project page: "Why is Infinispan sexy?" Those guys just don\'t hold back, do they :)',
    likes: 0,
    retweets: 0
  },
  {
    id: '17459525423',
    created: 1277950592000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> Super true. And using your teeth, it ain\'t pretty. It also leaves ruts. That\'s probably why the show the licking ;)<br><br>In reply to: <a href="https://x.com/adamcoomes/status/17450146403" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <span class="status">17450146403</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17459466315',
    created: 1277950536000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> You are single, but much more famous now. I\'m sure your admirers are welcoming you back to the ocean :)<br><br>In reply to: <a href="https://x.com/lincolnthree/status/17440702570" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">17440702570</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17454051822',
    created: 1277945306000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> I\'ll send an e-mail your way 2nite.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17449717877',
    created: 1277940881000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Talk to me man, I\'ve got the ear of the creator. What\'s buggin\' you?<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17438341213',
    created: 1277928259000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> I\'ve got Jetty 7 awaiting check-in and Tomcat 7 shouldn\'t be far behind. Jetty 7 is a better choice anyway than Jetty 6.',
    likes: 0,
    retweets: 0
  },
  {
    id: '17433016201',
    created: 1277922219000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> I just did a presentation for a customer and the first thing they asked was "where are you?" Hahaha. In Red Hat\'s "Laurel" office ;)<br><br>In reply to: <a href="https://x.com/rruss/status/17426344592" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">17426344592</span>',
    likes: 0,
    retweets: 1
  },
  {
    id: '17432309373',
    created: 1277921463000,
    type: 'post',
    text: 'I\'m surprised there is no Hitler parody for Maven. Come on, that\'s just asking for a rant ;)',
    likes: 0,
    retweets: 1
  },
  {
    id: '17424112680',
    created: 1277913554000,
    type: 'post',
    text: 'While at #summitjbw, I finally got to meet Will &amp; Will, the law-firm, er.. I mean the Seam trainers at Red Hat. Super guys.',
    likes: 1,
    retweets: 0,
    tags: ['summitjbw']
  },
  {
    id: '17423821510',
    created: 1277913294000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> What I\'ve learned is that we have a lot more to talk about than we think. you =&gt; JSFTemplating, FacesTester, Admin Console, etc<br><br>In reply to: <a href="https://x.com/jasondlee/status/17423043941" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">17423043941</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17423660724',
    created: 1277913149000,
    type: 'post',
    text: 'It appears that Hightide is the Jetty variant of a Java EE server. I would love to see it become a certified Web profile implementation.',
    likes: 0,
    retweets: 0
  },
  {
    id: '17423238000',
    created: 1277912771000,
    type: 'post',
    text: 'Does anyone know where I can download the whole Jetty 7 distribution with source? I hate when sources are split into 2 dozen jars only.',
    likes: 0,
    retweets: 0
  },
  {
    id: '17423176286',
    created: 1277912717000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> Doh! Well, he got enough of a lashing already from his project lead. He was probably just nervous ;)<br><br>In reply to: <a href="https://x.com/rruss/status/17420764225" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">17420764225</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17422718450',
    created: 1277912315000,
    type: 'post',
    text: 'Did I mention I love #Devoxx?',
    likes: 0,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '17422481138',
    created: 1277912104000,
    type: 'post',
    text: 'Woot! Seam 3: State of the Union University talk for #Devoxx approved. Glad to be back talking about Seam for 3hr, this time w/ Pete Muir.',
    likes: 0,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '17395092755',
    created: 1277879405000,
    type: 'post',
    text: 'The Java4Ever video is listed in the YouTube most popular videos, staged as Java vs .Net. <a href="https://youtu.be/fzza-ZbEY70" rel="noopener noreferrer" target="_blank">youtu.be/fzza-ZbEY70</a>',
    likes: 0,
    retweets: 1
  },
  {
    id: '17394913766',
    created: 1277879170000,
    type: 'post',
    text: 'I\'m hoping we can squeeze an #Arquillian talk into #JavaZone.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'javazone']
  },
  {
    id: '17394393784',
    created: 1277878484000,
    type: 'post',
    text: 'A parody of Hitler reacting to #Arquillian uncovering bugs in his container <a href="http://vimeo.com/12953852" rel="noopener noreferrer" target="_blank">vimeo.com/12953852</a> (all content is fictional)',
    likes: 1,
    retweets: 5,
    tags: ['arquillian']
  },
  {
    id: '17386835469',
    created: 1277870035000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> I begin and end every day in a terminal.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/17368246682" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">17368246682</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17386184998',
    created: 1277869416000,
    type: 'post',
    text: 'A detailed article on the #RichFaces Ajax Queue, an important future JSF feature: <a href="http://www.jsfcentral.com/articles/richfaces_queue.html" rel="noopener noreferrer" target="_blank">www.jsfcentral.com/articles/richfaces_queue.html</a>',
    likes: 1,
    retweets: 3,
    tags: ['richfaces']
  },
  {
    id: '17368302278',
    created: 1277851775000,
    type: 'post',
    text: 'Waiting for Tomcat 7 beta to appear in a Maven repository. I found a fork: <a href="http://repo2.maven.org/maven2/org/apache/geronimo/ext/tomcat/" rel="noopener noreferrer" target="_blank">repo2.maven.org/maven2/org/apache/geronimo/ext/tomcat/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17367141347',
    created: 1277850460000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jvanzyl" rel="noopener noreferrer" target="_blank">@jvanzyl</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Yeah, that\'s because we are hogging all of the monkey\'s bandwidth.<br><br>In reply to: <a href="https://x.com/jvanzyl/status/17363584864" rel="noopener noreferrer" target="_blank">@jvanzyl</a> <span class="status">17363584864</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17358279901',
    created: 1277840392000,
    type: 'post',
    text: '@anilsaldhana Sounds like a good time to create an #Arquillian container for it. With support for Servlet 3.0 it\'s a lot simpler.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '17298282697',
    created: 1277777886000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Way behind we are.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/17296944040" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">17296944040</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17298168297',
    created: 1277777775000,
    type: 'post',
    text: 'Twitter apps for #Android are way better than the feed apps (RSS, Atom, etc). In fact, the feed apps are downright abysmal.',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '17296714162',
    created: 1277776403000,
    type: 'post',
    text: 'ditto RT <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a>: After an awesome week at #summitjbw, it\'s time to clean a few inboxes and continue revolutionizing OSS',
    likes: 0,
    retweets: 0,
    tags: ['summitjbw']
  },
  {
    id: '17282982058',
    created: 1277762461000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Yeah, I\'ve always known about the directory stuff and gzipped files, but never realized it included archives too!<br><br>In reply to: <a href="https://x.com/jasondlee/status/17281029990" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">17281029990</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17280938821',
    created: 1277760218000,
    type: 'post',
    text: 'I just realized that vim has a zip browser built-in. Wow, that is going to be a huge time saver.',
    likes: 0,
    retweets: 0
  },
  {
    id: '17277774482',
    created: 1277756842000,
    type: 'post',
    text: '#ShrinkWrap and #Arquillian are neck &amp; neck on issue numbers, with #Arquillian (201) slightly edging out #ShrinkWrap (198).',
    likes: 0,
    retweets: 0,
    tags: ['shrinkwrap', 'arquillian', 'arquillian', 'shrinkwrap']
  },
  {
    id: '17277645014',
    created: 1277756719000,
    type: 'post',
    text: 'I completed the Tomcat extension for #ShrinkWrap. Still have some open questions though: <a href="https://jira.jboss.org/browse/SHRINKWRAP-198" rel="noopener noreferrer" target="_blank">jira.jboss.org/browse/SHRINKWRAP-198</a>',
    likes: 0,
    retweets: 0,
    tags: ['shrinkwrap']
  },
  {
    id: '17277559447',
    created: 1277756642000,
    type: 'post',
    text: 'How do you want the supported version to be represented in the #Arquillian container artifactIds? <a href="https://jira.jboss.org/browse/ARQ-200" rel="noopener noreferrer" target="_blank">jira.jboss.org/browse/ARQ-200</a>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '17273530542',
    created: 1277752742000,
    type: 'post',
    text: 'I really need a couple days of writing zeros to my brain\'s harddrive == days at the beach.',
    likes: 0,
    retweets: 0
  },
  {
    id: '17273310341',
    created: 1277752540000,
    type: 'post',
    text: 'Communication of information over the phone is soooo primitive. "P as in Paul. T as in Thomas." The chance for error is huge.',
    likes: 0,
    retweets: 0
  },
  {
    id: '17271634737',
    created: 1277750948000,
    type: 'post',
    text: 'Implemented a Tomcat ext for #ShrinkWrap yesterday. That makes 2/2 days of work this weekend, but I\'m so fired up to add new containers!',
    likes: 0,
    retweets: 1,
    tags: ['shrinkwrap']
  },
  {
    id: '17271532568',
    created: 1277750835000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> And I absolutely agree. This new generation of #JBoss has brought with it some really great people, not just great minds.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/17044147360" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">17044147360</span>',
    likes: 0,
    retweets: 0,
    tags: ['jboss']
  },
  {
    id: '17271334747',
    created: 1277750624000,
    type: 'post',
    text: 'My theory is w/ respect comes productivity we can be proud of. Cheers! RT <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Hard to find a #JBoss colleague I don\'t respect ...',
    likes: 0,
    retweets: 0,
    tags: ['jboss']
  },
  {
    id: '17270870736',
    created: 1277750138000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I was speaking of Jetty\'s API, which theoretically would allow you to escape XML.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/17252932565" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">17252932565</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17269974670',
    created: 1277749263000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> Ah, that\'s a Biscotti. My favorite coffee companion.<br><br>In reply to: <a href="https://x.com/rruss/status/17269816623" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">17269816623</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17269873489',
    created: 1277749161000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kenglxn" rel="noopener noreferrer" target="_blank">@kenglxn</a> <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> I just noticed that functionality last night. I like how the file extension is abstracted out.<br><br>In reply to: <a href="https://x.com/kenglxn/status/17269120945" rel="noopener noreferrer" target="_blank">@kenglxn</a> <span class="status">17269120945</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17269615685',
    created: 1277748900000,
    type: 'post',
    text: '#Arquillian coffee break.',
    photos: ['<div class="entry"><img class="photo" src="media/17269615685.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '17228448571',
    created: 1277701607000,
    type: 'post',
    text: 'The #Arquillian hackfest turned out to be very fruitful. We got a Jetty container, partial Tomcat container &amp; TAR ShrinkWrap archives.',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '17228379936',
    created: 1277701528000,
    type: 'post',
    text: 'Indeed, I found bugs in Jetty while writing the #Arquillian container for it. You just can\'t use #Arquillian and not discover a bug.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'arquillian']
  },
  {
    id: '17228315458',
    created: 1277701456000,
    type: 'post',
    text: 'Jetty is awesome because you can swap out many aspects of it to override its behavior. Sadly, figuring out how to do that is trial &amp; error.',
    likes: 0,
    retweets: 0
  },
  {
    id: '17228259744',
    created: 1277701394000,
    type: 'post',
    text: 'Finally got the Jetty 6.1.x container for #Arquillian working! Supports both client and in-container tests. See SVN trunk.',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '17228225679',
    created: 1277701356000,
    type: 'post',
    text: 'I really beat my head against the wall this weekend trying to get objects bound to JNDI in Jetty 6. JNDI is more complex than it has to be.',
    likes: 0,
    retweets: 1
  },
  {
    id: '17120682273',
    created: 1277589912000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> It\'s about context and when you have limited time for scanning updates. We\'ll see how it works out.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/17117103294" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">17117103294</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17120537904',
    created: 1277589730000,
    type: 'post',
    text: 'I\'m positive we\'ll see a big increase in activity on JBoss Community when it\'s easier to post via a phone (i.e. via e-mail or app)',
    likes: 0,
    retweets: 0
  },
  {
    id: '17117071204',
    created: 1277585533000,
    type: 'post',
    text: 'Pete Muir: "If you are in a CDI enabled archive, then why would be using JSF managed beans?" Exactly #jsr299 #jsf',
    likes: 1,
    retweets: 0,
    tags: ['jsr299', 'jsf']
  },
  {
    id: '17111529541',
    created: 1277579036000,
    type: 'post',
    text: 'A must-see trailer that\'s turning heads in Hollywood &amp; making M$ wet itself. Java 4Ever: <a href="https://youtu.be/KrfpnbGXL70" rel="noopener noreferrer" target="_blank">youtu.be/KrfpnbGXL70</a> #javazone',
    likes: 0,
    retweets: 4,
    tags: ['javazone']
  },
  {
    id: '17110854868',
    created: 1277578255000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brockm" rel="noopener noreferrer" target="_blank">@brockm</a> That would be a huge win for Android, both in terms of advocacy and benefitting from your involvement &amp; feedback.<br><br>In reply to: <a href="https://x.com/brockm/status/17107624336" rel="noopener noreferrer" target="_blank">@brockm</a> <span class="status">17107624336</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17068919444',
    created: 1277527375000,
    type: 'post',
    text: 'Lesson learned for non-US citizens going out on the town. Bring your passport. Bouncers aren\'t down w/ country ID cards #Boston #summitjbw',
    likes: 0,
    retweets: 0,
    tags: ['boston', 'summitjbw']
  },
  {
    id: '17068120394',
    created: 1277526496000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MertCaliskan" rel="noopener noreferrer" target="_blank">@MertCaliskan</a> The #Arquillian invasion is going to get heavy at #JavaOne. The earth will have no choice but to surrender to testing.<br><br>In reply to: @mertcaliskan <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 2,
    tags: ['arquillian', 'javaone']
  },
  {
    id: '17068018142',
    created: 1277526383000,
    type: 'post',
    text: 'Now that I\'ve crossed the 100 mark for the # of feeds I follow, I\'ve finally decided to create my first list to throttle the fire hose.',
    likes: 0,
    retweets: 0
  },
  {
    id: '17067260696',
    created: 1277525568000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I absolutely agree that we don\'t want all virtual. But we can extend our reach to get far away speakers doing just 1 talk.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/17065016868" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">17065016868</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17061873728',
    created: 1277520050000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/the_jamezp" rel="noopener noreferrer" target="_blank">@the_jamezp</a> Same here with my G1. This newsletter promises the myTouch is being upgraded "soon". No mention of the G1.<br><br>In reply to: <a href="https://x.com/the_jamezp/status/17060624366" rel="noopener noreferrer" target="_blank">@the_jamezp</a> <span class="status">17060624366</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17061669073',
    created: 1277519839000,
    type: 'post',
    text: 'Learn. Network. Experience Open Source. #summitjbw Like everyday @ JBoss Community, except the banners are PNGs',
    photos: ['<div class="entry"><img class="photo" src="media/17061669073.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['summitjbw']
  },
  {
    id: '17061408409',
    created: 1277519574000,
    type: 'post',
    text: 'World Trade Center #Boston, site of #summitjbw',
    photos: ['<div class="entry"><img class="photo" src="media/17061408409.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['boston', 'summitjbw']
  },
  {
    id: '17061334076',
    created: 1277519499000,
    type: 'post',
    text: '#ShrinkWrap lead <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> wiped out after throwing everyone\'s complexity over the wall @ #summitjbw',
    photos: ['<div class="entry"><img class="photo" src="media/17061334076.jpg"></div>'],
    likes: 0,
    retweets: 1,
    tags: ['shrinkwrap', 'summitjbw']
  },
  {
    id: '17061109947',
    created: 1277519275000,
    type: 'post',
    text: 'View from hotel room hallway @ Logan Hilton. Yes, I was that far from downtown &amp; conference site. #summitjbw',
    photos: ['<div class="entry"><img class="photo" src="media/17061109947.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['summitjbw']
  },
  {
    id: '17060902662',
    created: 1277519064000,
    type: 'post',
    text: 'Carbon neutal parking?',
    photos: ['<div class="entry"><img class="photo" src="media/17060902662.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '17059490031',
    created: 1277517625000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brockm" rel="noopener noreferrer" target="_blank">@brockm</a> And what\'s more, that was just a shot in the dark. With some practice we could make the slide/screen swapping a lot smoother.<br><br>In reply to: <a href="https://x.com/brockm/status/17059194712" rel="noopener noreferrer" target="_blank">@brockm</a> <span class="status">17059194712</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17059203312',
    created: 1277517328000,
    type: 'post',
    text: '#T-mobile: "No phones left behind. That\'s our pledge." Hmm, we\'ll see about that.',
    likes: 0,
    retweets: 0,
    tags: ['t']
  },
  {
    id: '17059059335',
    created: 1277517176000,
    type: 'post',
    text: 'Red Hat should note the success of the MetaWidget campground delivered over Skype @ #summiitjbw TV+laptop+skype = more talks, less money',
    likes: 0,
    retweets: 0,
    tags: ['summiitjbw']
  },
  {
    id: '17057905534',
    created: 1277515958000,
    type: 'post',
    text: 'Red Hat to Conduct, "Sorry we stole <a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> from you, but it\'s better for the world this way :)" #arquillian',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '17057785117',
    created: 1277515831000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/unitygirl" rel="noopener noreferrer" target="_blank">@unitygirl</a> It was indeed fun to meet/reconnect w/ many friends, colleagues &amp; partners. We certainly geeked out plenty. #summitjbw<br><br>In reply to: <a href="https://x.com/wow3community/status/17056467872" rel="noopener noreferrer" target="_blank">@wow3community</a> <span class="status">17056467872</span>',
    likes: 0,
    retweets: 0,
    tags: ['summitjbw']
  },
  {
    id: '17047477227',
    created: 1277504314000,
    type: 'post',
    text: 'Regular salty peanuts FTW!',
    photos: ['<div class="entry"><img class="photo" src="media/17047477227.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '17047421565',
    created: 1277504249000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a>, I saw your pizza on my flight ;) Where will it be seen next?',
    photos: ['<div class="entry"><img class="photo" src="media/17047421565.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '17042732388',
    created: 1277498840000,
    type: 'reply',
    text: '@brianleathem Great use of a new flag in JSF 2. We\'ll add that to booking, perhaps (though it\'s there in a slightly modified form already).<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '17042551667',
    created: 1277498636000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Definitely looking forward to getting back on #jbosstesting and #seam-dev to discuss all the new developments.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/17041418892" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">17041418892</span>',
    likes: 0,
    retweets: 0,
    tags: ['jbosstesting', 'seam']
  },
  {
    id: '17042399439',
    created: 1277498464000,
    type: 'post',
    text: 'Lots of familiar faces in the airport. Just crossed paths with Jason Greene, David Lloyd, <a class="mention" href="https://x.com/brockm" rel="noopener noreferrer" target="_blank">@brockm</a> and others. Bye guys!',
    likes: 0,
    retweets: 1
  },
  {
    id: '17040498845',
    created: 1277496429000,
    type: 'post',
    text: 'Just had a fantastic lunch outside w/ <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a>, <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> &amp; my wife. Sad to see the week end, but glad that rest is ahead #summitjbw',
    likes: 0,
    retweets: 0,
    tags: ['summitjbw']
  },
  {
    id: '17027349264',
    created: 1277483726000,
    type: 'post',
    text: '#Arquillian OSGi support is on fire right now thanks to Thomas Diesler. It\'s really interesting to watch it branch out from Java EE.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '16995370944',
    created: 1277448647000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> and <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> outside the Historic Summit Party in Quincy Market at #summitjbw',
    photos: ['<div class="entry"><img class="photo" src="media/16995370944.jpg"></div>'],
    likes: 0,
    retweets: 2,
    tags: ['summitjbw']
  },
  {
    id: '16994244181',
    created: 1277447165000,
    type: 'post',
    text: 'After a bunch of false starts, finally got the JBoss Community band together at Drinks, an international id-friendly bar. #summitjbw',
    likes: 0,
    retweets: 0,
    tags: ['summitjbw']
  },
  {
    id: '16961613328',
    created: 1277413696000,
    type: 'post',
    text: 'Richard Kennard skyped in from Australia at #summitjbw to present MetaWidget at a campground talk.',
    photos: ['<div class="entry"><img class="photo" src="media/16961613328.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['summitjbw']
  },
  {
    id: '16958203464',
    created: 1277410702000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> The JBoss nexus repos are listed in this pom: <a href="http://anonsvn.jboss.org/repos/common/arquillian/trunk/build/pom.xml" rel="noopener noreferrer" target="_blank">anonsvn.jboss.org/repos/common/arquillian/trunk/build/pom.xml</a><br><br>In reply to: <a href="https://x.com/jasondlee/status/16952023811" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">16952023811</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16957127850',
    created: 1277409943000,
    type: 'post',
    text: 'I regret having to miss <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a>\'s talk on JBT tips and tricks as it conflicted with the RichFaces session.',
    likes: 0,
    retweets: 0
  },
  {
    id: '16955601292',
    created: 1277408552000,
    type: 'reply',
    text: '@brianleathem +1 for open and accessible sources<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16950365774',
    created: 1277404266000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Cool. Also, you can check out Michael\'s examples on github for reference <a href="https://github.com/arquillian-sandbox/arquillian-examples" rel="noopener noreferrer" target="_blank">github.com/arquillian-sandbox/arquillian-examples</a><br><br>In reply to: <a href="https://x.com/jasondlee/status/16950222040" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">16950222040</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16950122175',
    created: 1277404068000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> We\'d be glad to help you on the forums (if you already posted, I should mention we are running around a conference atm)<br><br>In reply to: <a href="https://x.com/majson/status/16922808993" rel="noopener noreferrer" target="_blank">@majson</a> <span class="status">16922808993</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16949930014',
    created: 1277403901000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> That\'s a port issue (and a good FAQ candidate). Can you post on the forum and list your arquillian config, etc.<br><br>In reply to: <a href="https://x.com/jasondlee/status/16947179278" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">16947179278</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16949813156',
    created: 1277403797000,
    type: 'post',
    text: 'Watching <a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> and <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> present on JSF 2.0, RichFaces 4.0 and Seam Faces 3.0 at #summitjbw.',
    likes: 0,
    retweets: 2,
    tags: ['summitjbw']
  },
  {
    id: '16946831985',
    created: 1277401011000,
    type: 'post',
    text: 'Find a quickstart #Arquillian project here: <a href="http://github.com/arquillian-sandbox/arquillian-examples" rel="noopener noreferrer" target="_blank">github.com/arquillian-sandbox/arquillian-examples</a> We\'ve also got an archetype brewing.',
    likes: 0,
    retweets: 2,
    tags: ['arquillian']
  },
  {
    id: '16939670380',
    created: 1277394953000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> As I often say, better to be busy than bored. That is, if the work isn\'t mind numbing &amp; you get your toes in the sand now &amp; then.<br><br>In reply to: <a href="https://x.com/JohnAment/status/16893316496" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">16893316496</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16938883043',
    created: 1277394455000,
    type: 'post',
    text: 'Summer seems to be making up for lost time here in #Boston. It\'s cooking. But hey, I\'ll take it over chilly and rainy any day.',
    likes: 0,
    retweets: 0,
    tags: ['boston']
  },
  {
    id: '16937143727',
    created: 1277393200000,
    type: 'post',
    text: 'Excited to have linked up <a class="mention" href="https://x.com/jganoff" rel="noopener noreferrer" target="_blank">@jganoff</a> &amp; Mike Brook last night at the #summitjbw party. The partnership should be "eventful" #seam #errai',
    likes: 0,
    retweets: 0,
    tags: ['summitjbw', 'seam', 'errai']
  },
  {
    id: '16856550231',
    created: 1277306026000,
    type: 'post',
    text: 'All examples in the upcoming O\'Reilly EJB 3.1 book by <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> are powered by Arquillian.',
    likes: 0,
    retweets: 1
  },
  {
    id: '16855607260',
    created: 1277305089000,
    type: 'post',
    text: 'JBoss is involved in a lot more standards organizations than just the JCP.',
    likes: 0,
    retweets: 0
  },
  {
    id: '16855081323',
    created: 1277304559000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/galderz" rel="noopener noreferrer" target="_blank">@galderz</a> Yes, but not just places. I want to know what friends/role models select for products, airlines, books, and anything else.<br><br>In reply to: <a href="https://x.com/galderz/status/16854706560" rel="noopener noreferrer" target="_blank">@galderz</a> <span class="status">16854706560</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16854961807',
    created: 1277304443000,
    type: 'post',
    text: 'I\'d really like JBoss Tools to not just be limited to Eclipse plugins. There are other ways/platforms for tooling.',
    likes: 0,
    retweets: 0
  },
  {
    id: '16854814750',
    created: 1277304308000,
    type: 'post',
    text: 'Mark Little: JBoss has a new committment to simplicity. Manage, configure &amp; perform.',
    likes: 0,
    retweets: 0
  },
  {
    id: '16854691257',
    created: 1277304192000,
    type: 'post',
    text: 'Mark Little: Tooling is important to everything you do. We can\'t expect VI &amp; emacs to be suffient. We want to match developer w/ solution.',
    likes: 0,
    retweets: 0
  },
  {
    id: '16854233118',
    created: 1277303767000,
    type: 'post',
    text: 'Mark Little: #Arquillian central to the evolution of platforms &amp; "relevant" for development. "You need to test the hell out of things" #jbw',
    likes: 0,
    retweets: 1,
    tags: ['arquillian', 'jbw']
  },
  {
    id: '16853891579',
    created: 1277303453000,
    type: 'post',
    text: 'Mark Little: Community involvement is critical and welcome for projects and platforms/products. We don\'t just do code drops. #jbw',
    likes: 0,
    retweets: 0,
    tags: ['jbw']
  },
  {
    id: '16853690933',
    created: 1277303283000,
    type: 'reply',
    text: '@brianleathem <a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> Could you create an Arquillian test for this problem and package it up? Perhaps start w/ archetype from trunk.<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16853402077',
    created: 1277303038000,
    type: 'post',
    text: 'Mark Little: Community and customers help shape projects.',
    likes: 0,
    retweets: 0
  },
  {
    id: '16851565546',
    created: 1277301420000,
    type: 'post',
    text: 'I made a tinyurl for grabbing a JBoss AS 6.0.0 nightly: <a href="http://hudson.jboss.org/hudson/view/JBoss%20AS/job/JBoss-AS-6.0.x/lastSuccessfulBuild/artifact/JBossAS_6_0/build/target/jboss-6.0.x.zip" rel="noopener noreferrer" target="_blank">hudson.jboss.org/hudson/view/JBoss%20AS/job/JBoss-AS-6.0.x/lastSuccessfulBuild/artifact/JBossAS_6_0/build/target/jboss-6.0.x.zip</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16850502930',
    created: 1277300430000,
    type: 'post',
    text: 'Nexus is an overused work folks.',
    likes: 0,
    retweets: 0
  },
  {
    id: '16850467837',
    created: 1277300399000,
    type: 'post',
    text: 'I\'m out of #Arquillian buttons and shirts. But I have stickers! Find me to put Ike on your laptop or notepad. Get invaded.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '16850401049',
    created: 1277300337000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> However, we still have to address packaging for in-container testing in servlet containers. #Arquillian<br><br>In reply to: <a href="https://x.com/ALRubinger/status/16778415568" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">16778415568</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '16850319946',
    created: 1277300260000,
    type: 'post',
    text: 'Talked last night about location-based app that shows you not where your friends are, but where they have been. i.e. restuarant selection.',
    likes: 0,
    retweets: 0
  },
  {
    id: '16849920306',
    created: 1277299885000,
    type: 'post',
    text: 'Msg from Red Hat at #rhsummit. We\'ve got you covered in the cloud. Be open on the cloud. Most comprehensive solutions both public &amp; private.',
    likes: 0,
    retweets: 0,
    tags: ['rhsummit']
  },
  {
    id: '16849620661',
    created: 1277299602000,
    type: 'post',
    text: 'I\'m really anxious to get back online and respond to forum posts &amp; hack. The #Arquillian forums are really heating up! Keep it up folks!',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '16849487662',
    created: 1277299473000,
    type: 'post',
    text: 'Holy crap there are a lot of people at #rhsummit! This keynote feels like the size of a JavaOne keynote, if not larger.',
    likes: 0,
    retweets: 0,
    tags: ['rhsummit']
  },
  {
    id: '16848112210',
    created: 1277298136000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> FacesContext#getViewRoot().getViewId()<br><br>In reply to: <a href="https://x.com/JohnAment/status/16845950038" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">16845950038</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16847848078',
    created: 1277297885000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RedHatSummit" rel="noopener noreferrer" target="_blank">@RedHatSummit</a> +1 The mobile site is way more user-friendly than the regular site.<br><br>In reply to: <a href="https://x.com/RedHatSummit/status/16843832135" rel="noopener noreferrer" target="_blank">@RedHatSummit</a> <span class="status">16843832135</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16828725843',
    created: 1277271760000,
    type: 'post',
    text: 'A bridge closure nearly foiled my cab ride. Be up early tomorrow to find out state of JBoss union and how to throw complexity over the wall.',
    likes: 0,
    retweets: 0
  },
  {
    id: '16828629068',
    created: 1277271637000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Swimming is out too, I saw the size of those jelly fish today, not a chance.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/16828040767" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">16828040767</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16826565476',
    created: 1277269169000,
    type: 'post',
    text: 'Staying on the other side of the water presents a problem at night. You can\'t just walk it. Part of this trip will be in a cab :(',
    likes: 0,
    retweets: 0
  },
  {
    id: '16826447090',
    created: 1277269032000,
    type: 'post',
    text: 'Had a great time tonight talking to colleagues, community and customers. Too much fun, in fact. Now I can\'t seem to catch a train back.',
    likes: 0,
    retweets: 0
  },
  {
    id: '16826041390',
    created: 1277268572000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> Actually, you could say you can only find Italian restaurants in the north end and not be that far off. #Boston<br><br>In reply to: <a href="https://x.com/rruss/status/16791505104" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">16791505104</span>',
    likes: 0,
    retweets: 0,
    tags: ['boston']
  },
  {
    id: '16801747224',
    created: 1277244230000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> I\'ll be at the #jbw booth in about 15 minutes. Working through the transit system has taken longer than anticipated.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/16794301928" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">16794301928</span>',
    likes: 0,
    retweets: 0,
    tags: ['jbw']
  },
  {
    id: '16799946530',
    created: 1277242248000,
    type: 'post',
    text: 'Heading over to #jbw now for some booth time.',
    likes: 0,
    retweets: 0,
    tags: ['jbw']
  },
  {
    id: '16799696146',
    created: 1277241975000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/caniszczyk" rel="noopener noreferrer" target="_blank">@caniszczyk</a> I\'m all over it for sure. Good luck with the release.<br><br>In reply to: <a href="https://x.com/cra/status/16790956613" rel="noopener noreferrer" target="_blank">@cra</a> <span class="status">16790956613</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16795399821',
    created: 1277237378000,
    type: 'post',
    text: 'Aboard the USS Constitution. Each canon weighs as much as an Escalade.',
    photos: ['<div class="entry"><img class="photo" src="media/16795399821.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '16790643183',
    created: 1277232368000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> I\'ll be there.<br><br>In reply to: <a href="https://x.com/maxandersen/status/16709962840" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">16709962840</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16790612980',
    created: 1277232336000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/caniszczyk" rel="noopener noreferrer" target="_blank">@caniszczyk</a> I\'m on Ganymede I believe (3.5?) on Linux. Ctrl++ doesn\'t work for me. But that would certainly get the job done if it did.<br><br>In reply to: <a href="https://x.com/cra/status/16710098534" rel="noopener noreferrer" target="_blank">@cra</a> <span class="status">16710098534</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16790453169',
    created: 1277232172000,
    type: 'post',
    text: 'Just made it to Bunker Hill.',
    photos: ['<div class="entry"><img class="photo" src="media/16790453169.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '16790369268',
    created: 1277232088000,
    type: 'post',
    text: 'Strolling Freedom Trail in #Boston.',
    likes: 0,
    retweets: 0,
    tags: ['boston']
  },
  {
    id: '16777508217',
    created: 1277219016000,
    type: 'post',
    text: 'We got the #Arquillian Tomcat and Jetty containers started last night. Need to think about to get test runner in war, rather than ear.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '16718758427',
    created: 1277151321000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/kenglxn" rel="noopener noreferrer" target="_blank">@kenglxn</a> JBoss AS supports tx in <a class="mention" href="https://x.com/PostConstruct" rel="noopener noreferrer" target="_blank">@PostConstruct</a> and initializer, GlassFish supports in neither. Another mystery solved by #Arquillian.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '16711690070',
    created: 1277143994000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/mojavelinux" rel="noopener noreferrer" target="_blank">@mojavelinux</a> Great, <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> said we can get a font increase command. JIRA on the way.',
    likes: 0,
    retweets: 0
  },
  {
    id: '16710894701',
    created: 1277143191000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kenglxn" rel="noopener noreferrer" target="_blank">@kenglxn</a> CDI initializer method (<a class="mention" href="https://x.com/inject" rel="noopener noreferrer" target="_blank">@inject</a>) is preferred over <a class="mention" href="https://x.com/PostConstruct" rel="noopener noreferrer" target="_blank">@PostConstruct</a>. I think it falls within a tx on an EJB. Test w/ Arquillian.<br><br>In reply to: <a href="https://x.com/kenglxn/status/16710510621" rel="noopener noreferrer" target="_blank">@kenglxn</a> <span class="status">16710510621</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16710770380',
    created: 1277143062000,
    type: 'post',
    text: 'You can find the latest Weld PasteCode example here: <a href="http://anonsvn.jboss.org/repos/weld/examples/trunk/jsf/pastecode/" rel="noopener noreferrer" target="_blank">anonsvn.jboss.org/repos/weld/examples/trunk/jsf/pastecode/</a> Lot\'s of good examples of how to use EE 6.',
    likes: 0,
    retweets: 2
  },
  {
    id: '16710045947',
    created: 1277142320000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kenglxn" rel="noopener noreferrer" target="_blank">@kenglxn</a> Thank you for your contributions past and present. Wear it proud ;)<br><br>In reply to: <a href="https://x.com/kenglxn/status/16704391977" rel="noopener noreferrer" target="_blank">@kenglxn</a> <span class="status">16704391977</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16709846918',
    created: 1277142121000,
    type: 'post',
    text: 'Eclipse desperately needs a one click "make editor text larger" button. How much more presentation time are we going to waste?',
    likes: 0,
    retweets: 0
  },
  {
    id: '16709744836',
    created: 1277142019000,
    type: 'post',
    text: 'Watching people use Eclipse makes it very clear to me that I\'m not the only one having problems with it\'s annoying hangups.',
    likes: 0,
    retweets: 0
  },
  {
    id: '16709060012',
    created: 1277141346000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a>, Weld pastecode is calling, it needs #PrettyTime.',
    likes: 0,
    retweets: 0,
    tags: ['prettytime']
  },
  {
    id: '16708433560',
    created: 1277140733000,
    type: 'post',
    text: 'Pete Muir and <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> demoing the Weld PasteCode example at #JUDCon. According to Pere, it was a late nighting getting this to run ;)',
    likes: 0,
    retweets: 0,
    tags: ['judcon']
  },
  {
    id: '16702472119',
    created: 1277135196000,
    type: 'post',
    text: 'Listing to Ales Justin talk about testing MC in Maven, it\'s clear that classpath hell in surefire is a universal problem.',
    likes: 0,
    retweets: 0
  },
  {
    id: '16702365063',
    created: 1277135102000,
    type: 'post',
    text: 'If you want to see examples of #Arquillian in use, browse the Seam 3 or Weld source code.',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '16702167578',
    created: 1277134926000,
    type: 'post',
    text: 'New blog post about #Arquillian JUDCon hackfest <a href="http://community.jboss.org/en/arquillian/blog" rel="noopener noreferrer" target="_blank">community.jboss.org/en/arquillian/blog</a>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '16701874348',
    created: 1277134658000,
    type: 'post',
    text: 'Tomcat #Arquillian container to be implemented tonight at the hackfest. I\'m excited. We will also work on an Ant example.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '16701800686',
    created: 1277134591000,
    type: 'post',
    text: '#Arquillian talk done, shoert and sweet. We captured video, audio and screen, so we\'ll see what we can make of it.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '16695380088',
    created: 1277128352000,
    type: 'post',
    text: 'About to start our talk on #Arquillian at JUDcon. If you are around, come check us out! It\'s chock full of demos.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '16666669050',
    created: 1277092148000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kenglxn" rel="noopener noreferrer" target="_blank">@kenglxn</a> I look forward to meeting you. Welcome!<br><br>In reply to: <a href="https://x.com/kenglxn/status/16654189838" rel="noopener noreferrer" target="_blank">@kenglxn</a> <span class="status">16654189838</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16666533482',
    created: 1277092010000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> That would be a great read.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16666177505',
    created: 1277091653000,
    type: 'post',
    text: 'Guilt in a box. Not the wedding cakes and definitely not the Ricotta pie. Our vice was the cannoli.',
    photos: ['<div class="entry"><img class="photo" src="media/16666177505.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '16666055384',
    created: 1277091531000,
    type: 'post',
    text: 'Indulged in a cannoli @ Mike\'s Pastry in #boston with <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a>, <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> and my wife. Yum. Then guilt.',
    photos: ['<div class="entry"><img class="photo" src="media/16666055384.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['boston']
  },
  {
    id: '16638243226',
    created: 1277061374000,
    type: 'post',
    text: 'View of Quincy Market eating some clam chowder at the Chat House. #boston.',
    photos: ['<div class="entry"><img class="photo" src="media/16638243226.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['boston']
  },
  {
    id: '16627409368',
    created: 1277050161000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/derkdukker" rel="noopener noreferrer" target="_blank">@derkdukker</a> It\'s in the works, stay tuned!<br><br>In reply to: <a href="https://x.com/derkdukker/status/16419087234" rel="noopener noreferrer" target="_blank">@derkdukker</a> <span class="status">16419087234</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16627257461',
    created: 1277050027000,
    type: 'post',
    text: 'What\'s the dilly. Why is there no share option for a contact in #Android (1.6)?',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '16619101724',
    created: 1277043059000,
    type: 'post',
    text: 'Just saw the band LA Guns at #BWI airport.',
    likes: 0,
    retweets: 0,
    tags: ['bwi']
  },
  {
    id: '16617576958',
    created: 1277041633000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gscottshaw" rel="noopener noreferrer" target="_blank">@gscottshaw</a> Are you sure it wasn\'t just caused by the 3D glasses themselves? Those movies make my eyes gush by the end of them.<br><br>In reply to: <a href="https://x.com/gscottshaw/status/16538856883" rel="noopener noreferrer" target="_blank">@gscottshaw</a> <span class="status">16538856883</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16616785128',
    created: 1277040808000,
    type: 'post',
    text: 'It feels like a golf morning followed by a day at the beach. Sadly that\'s not the case. The upside is that I\'m off to see my buddies!',
    likes: 0,
    retweets: 0
  },
  {
    id: '16574479300',
    created: 1276985927000,
    type: 'reply',
    text: '@brianleathem Hibernate was very pleasant to work with in the Seam 3 booking example we put together for JBW.<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16574420946',
    created: 1276985845000,
    type: 'post',
    text: '@brianleathem That\'s why we are going full in on JodaTime in Seam 3 international ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '16418252409',
    created: 1276809298000,
    type: 'post',
    text: 'I\'ve been heads down for 2 days actually using Java EE 6 and #Seam 3. It\'s incredible how many ideas you get going through that exercise.',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '16287054412',
    created: 1276669462000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lionelg3" rel="noopener noreferrer" target="_blank">@lionelg3</a> Of course not. We welcome you to check out all JSR-299 has to offer. I guarantee you\'ll enjoy it.<br><br>In reply to: <a href="https://x.com/lionelg3/status/16237010918" rel="noopener noreferrer" target="_blank">@lionelg3</a> <span class="status">16237010918</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16286951926',
    created: 1276669318000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> I\'m of the opinion that the <a class="mention" href="https://x.com/ejb" rel="noopener noreferrer" target="_blank">@ejb</a> annotation is finished. The <a class="mention" href="https://x.com/Resource" rel="noopener noreferrer" target="_blank">@Resource</a> annotation is a bridge and hasn\'t come full circle yet.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16286873257',
    created: 1276669208000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Haha. I meant you don\'t have the second edition, cause it hasn\'t been drafted yet :)<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16286822886',
    created: 1276669138000,
    type: 'post',
    text: 'Finally, #Maven is going to address the need to have one local repository per remote repository: <a href="http://jira.codehaus.org/browse/MNG-4709" rel="noopener noreferrer" target="_blank">jira.codehaus.org/browse/MNG-4709</a>',
    likes: 0,
    retweets: 0,
    tags: ['maven']
  },
  {
    id: '16281831472',
    created: 1276662820000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> It\'s cool. I forgive you. You don\'t have a bible like Seam in Action yet to sort it all out ;)<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16281768707',
    created: 1276662746000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joshiaangela" rel="noopener noreferrer" target="_blank">@joshiaangela</a> Yes, my computer was affected by Eclipse.<br><br>In reply to: <a href="https://x.com/joshiaangela/status/16274190524" rel="noopener noreferrer" target="_blank">@joshiaangela</a> <span class="status">16274190524</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16281745975',
    created: 1276662720000,
    type: 'post',
    text: 'Can\'t wait for mid-July, my 1st day @ the beach since bailing on a vacation 2 years ago to go discuss Seam for a week @ farmhouse in Italy',
    likes: 0,
    retweets: 0
  },
  {
    id: '16279460702',
    created: 1276660299000,
    type: 'post',
    text: 'My doctor told me today she\'s resigning to start her own practice. The interesting part is the reason. She wants EMR &amp; automated coding.',
    likes: 0,
    retweets: 0
  },
  {
    id: '16279296053',
    created: 1276660135000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> We are working on the booking example now. So only a matter of time before we\'ve got the anthology back together.<br><br>In reply to: <a href="https://x.com/tech4j/status/16270856601" rel="noopener noreferrer" target="_blank">@tech4j</a> <span class="status">16270856601</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16272589112',
    created: 1276654038000,
    type: 'post',
    text: 'Great replies to the kill command comment. +1 for kill -9, but that\'s just implied. My computer was being bad today, had to slap it around.',
    likes: 0,
    retweets: 0
  },
  {
    id: '16248453131',
    created: 1276630154000,
    type: 'post',
    text: 'kill is the best command in Linux. It\'s the very essence of how the world should be. Stop when I say stop, bitch.',
    likes: 0,
    retweets: 0
  },
  {
    id: '16242674549',
    created: 1276624628000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> You shouldn\'t be confused. <a class="mention" href="https://x.com/Resource" rel="noopener noreferrer" target="_blank">@Resource</a> is for getting a JNDI resource (including DataSource). <a class="mention" href="https://x.com/inject" rel="noopener noreferrer" target="_blank">@inject</a> is everything else.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16234646783',
    created: 1276617583000,
    type: 'post',
    text: 'github slogan: don\'t think, just commit',
    likes: 1,
    retweets: 1
  },
  {
    id: '16210013123',
    created: 1276590618000,
    type: 'post',
    text: 'Can anyone point me to an article on how to programmatically resolve an artifact using #Maven, beginning with reading in a pom.xml.',
    likes: 0,
    retweets: 0,
    tags: ['maven']
  },
  {
    id: '16203826173',
    created: 1276584370000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/dzone" rel="noopener noreferrer" target="_blank">@dzone</a> your script is out of control :0',
    likes: 0,
    retweets: 0
  },
  {
    id: '16203345306',
    created: 1276584059000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> You should blog about the hackfest for #Arquillian and #ShrinkWrap at JUDCon.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'shrinkwrap']
  },
  {
    id: '16202339170',
    created: 1276583453000,
    type: 'post',
    text: 'Does anyone know how to add new code template files in #Eclipse? Not just inline templates, but a template that becomes a whole file.',
    likes: 0,
    retweets: 0,
    tags: ['eclipse']
  },
  {
    id: '16196191083',
    created: 1276570261000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> Same here. Good thing I\'m backing up ;)<br><br>In reply to: <a href="https://x.com/JohnAment/status/16191785199" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">16191785199</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16179264244',
    created: 1276553134000,
    type: 'post',
    text: 'When my wife goes to play golf, she asks "Can I play the course w/o these clubs?" Smart thinking. Men try to sneak extra clubs into the bag.',
    likes: 0,
    retweets: 0
  },
  {
    id: '16178931050',
    created: 1276552773000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/deruelle" rel="noopener noreferrer" target="_blank">@deruelle</a> Hahaha. Yeah, I don\'t get in trouble enough, apparently ;)<br><br>In reply to: <a href="https://x.com/deruelle/status/16167824744" rel="noopener noreferrer" target="_blank">@deruelle</a> <span class="status">16167824744</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16167551017',
    created: 1276540440000,
    type: 'post',
    text: 'The first question my wife always asks me after leaving me at home for a weekend: "Did you move?"',
    likes: 0,
    retweets: 2
  },
  {
    id: '16163186855',
    created: 1276535931000,
    type: 'post',
    text: 'While toying with Java EE 6 lately, the implementation that has yet to let me down is #Hibernate. It\'s back on top, baby!',
    likes: 0,
    retweets: 1,
    tags: ['hibernate']
  },
  {
    id: '16161695978',
    created: 1276534452000,
    type: 'reply',
    text: '@chuggid The users list probably doesn\'t offer the insight you\'re looking for. So go w/ the dev list ;) <a href="http://sourceforge.net/mailarchive/forum.php?forum_name=resteasy-developers" rel="noopener noreferrer" target="_blank">sourceforge.net/mailarchive/forum.php?forum_name=resteasy-developers</a><br><br>In reply to: @chuggid <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16124742821',
    created: 1276491996000,
    type: 'reply',
    text: '@chuggid This feature is currently only in trunk, not yet in a release. It\'s being worked on. See the resteasy-developers mailinglist.<br><br>In reply to: @chuggid <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16120721158',
    created: 1276487232000,
    type: 'post',
    text: '#Arquillian had its world premier at #Devoxx 2009.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'devoxx']
  },
  {
    id: '16120641640',
    created: 1276487145000,
    type: 'post',
    text: 'If we are lucky, we\'ll get to show #Arquillian at #devoxx ... for the second time. This time with a lot more meat on the bones.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'devoxx']
  },
  {
    id: '16120592352',
    created: 1276487093000,
    type: 'post',
    text: 'I really screwed up by forgetting to get my #devoxx submissions in on time. Hopefully <a class="mention" href="https://x.com/Stephan007" rel="noopener noreferrer" target="_blank">@Stephan007</a> will grant me amnesty. Sending now.',
    likes: 0,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '16119124836',
    created: 1276485575000,
    type: 'post',
    text: 'Still trying to think of a superpower for my Google profile. In the past, I\'ve said the ability to retrieve information anytime, anywhere.',
    likes: 0,
    retweets: 0
  },
  {
    id: '16101385553',
    created: 1276467733000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> I can\'t believe you haven\'t seen home since I last saw you in Switzerland. You are a warrior ;)<br><br>In reply to: <a href="https://x.com/matthewmccull/status/16100925953" rel="noopener noreferrer" target="_blank">@matthewmccull</a> <span class="status">16100925953</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16081766584',
    created: 1276445888000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> Hmm, on second thought. Maybe they should worry less about banning operating systems and focus more on the drugs ;)<br><br>In reply to: <a href="https://x.com/matthewmccull/status/16056506896" rel="noopener noreferrer" target="_blank">@matthewmccull</a> <span class="status">16056506896</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16049047596',
    created: 1276400702000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> Haha. What I mean was, NetBeans is adding them to svn:ignore so exclude in that sense ;)<br><br>In reply to: <a href="https://x.com/jeremynorris/status/16048279212" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">16048279212</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16045908835',
    created: 1276397374000,
    type: 'post',
    text: 'm2eclipse does not honor dependency scopes, whereas NetBeans does. So Eclipse developers end up committing builds that don\'t work.',
    likes: 0,
    retweets: 0
  },
  {
    id: '16043954935',
    created: 1276395184000,
    type: 'post',
    text: 'NetBeans, stop excluding my VIM swap files from SVN. I know you are jealous, but deal with it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '16041956911',
    created: 1276392866000,
    type: 'post',
    text: '#Maven profile selector for #m2eclipse <a href="https://issues.sonatype.org/browse/MNGECLIPSE-2248" rel="noopener noreferrer" target="_blank">issues.sonatype.org/browse/MNGECLIPSE-2248</a>',
    likes: 0,
    retweets: 0,
    tags: ['maven', 'm2eclipse']
  },
  {
    id: '16040491636',
    created: 1276391099000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> is converting #weld tests to #Arquillian like a champ ;)',
    likes: 0,
    retweets: 0,
    tags: ['weld', 'arquillian']
  },
  {
    id: '16037771455',
    created: 1276387673000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> Now that would be a TSA ban I could side with ;)<br><br>In reply to: <a href="https://x.com/matthewmccull/status/16029064020" rel="noopener noreferrer" target="_blank">@matthewmccull</a> <span class="status">16029064020</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16025934658',
    created: 1276372254000,
    type: 'post',
    text: 'I can now confidently say that I have learned JSR 88 (deployment API). Just in time for it to be pruned from the platform :( Too bad.',
    likes: 0,
    retweets: 0
  },
  {
    id: '16022889715',
    created: 1276369218000,
    type: 'post',
    text: 'Just checked in a whole bunch of #Arquillian code, including the remote glassfish container and an archetype.',
    likes: 0,
    retweets: 2,
    tags: ['arquillian']
  },
  {
    id: '16018647452',
    created: 1276364958000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> Hmm, no love for Android phones then. Perhaps I need to spoof my user agent ;0<br><br>In reply to: <a href="https://x.com/aschwart/status/16018290111" rel="noopener noreferrer" target="_blank">@aschwart</a> <span class="status">16018290111</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '16018630930',
    created: 1276364941000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Woot! Now you\'ve one-upped me in the "red" department.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/16015727619" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">16015727619</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '15973716404',
    created: 1276310938000,
    type: 'post',
    text: 'Does nobody use <a class="mention" href="https://x.com/dzone" rel="noopener noreferrer" target="_blank">@dzone</a> on a mobile phone? Because it\'s completely unreadable. #fail',
    likes: 0,
    retweets: 0,
    tags: ['fail']
  },
  {
    id: '15970661802',
    created: 1276307744000,
    type: 'post',
    text: 'Day didn\'t feel good until I started slamming some drives at the #golf range. Even got some tips from an old pro.',
    likes: 0,
    retweets: 0,
    tags: ['golf']
  },
  {
    id: '15970529089',
    created: 1276307601000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> @chuggid That was the story of my day :(<br><br>In reply to: <a href="https://x.com/ALRubinger/status/15941386476" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">15941386476</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '15927171950',
    created: 1276262266000,
    type: 'post',
    text: 'If you are in Maryland, DC or Virginia...I\'ll be a CMJUG on July 14th, likely showing #Arquillian. <a href="http://www.cmjug.org" rel="noopener noreferrer" target="_blank">www.cmjug.org</a> Details to come.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '15878609017',
    created: 1276204060000,
    type: 'reply',
    text: '@mwessendorf Yeah, the one country in the world that finds golf more interesting than soccer.<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '15878549633',
    created: 1276203989000,
    type: 'post',
    text: 'It\'s bad when you have so many tabs open you can\'t even see the favicon :(',
    likes: 0,
    retweets: 0
  },
  {
    id: '15860172836',
    created: 1276185788000,
    type: 'post',
    text: 'Not one of Seam 2\'s best moments, from the message API: Messages#addToControlFromResourceBundleOrDefault()',
    likes: 0,
    retweets: 0
  },
  {
    id: '15848707423',
    created: 1276174935000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dzone" rel="noopener noreferrer" target="_blank">@dzone</a> No.<br><br>In reply to: <a href="https://x.com/DZoneInc/status/15848370125" rel="noopener noreferrer" target="_blank">@DZoneInc</a> <span class="status">15848370125</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '15807274393',
    created: 1276124356000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> Same here. The hibernate generator gets me half way because it at least generates the sources, but in the wrong location.<br><br>In reply to: <a href="https://x.com/jeremynorris/status/15803677469" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">15803677469</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '15801596177',
    created: 1276116724000,
    type: 'post',
    text: 'JPA 2 is suffering because tooling can\'t get it\'s act together to support the metamodel generation.',
    likes: 0,
    retweets: 0
  },
  {
    id: '15797825155',
    created: 1276112539000,
    type: 'post',
    text: '#Arquillian goes green <a href="http://community.jboss.org/en/arquillian/blog/2010/06/09/arquillian-goes-green" rel="noopener noreferrer" target="_blank">community.jboss.org/en/arquillian/blog/2010/06/09/arquillian-goes-green</a>',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '15790669138',
    created: 1276104025000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> That must be why it\'s failing miserably today ;)<br><br>In reply to: <a href="https://x.com/adamcoomes/status/15767103620" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <span class="status">15767103620</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '15790099794',
    created: 1276103199000,
    type: 'post',
    text: 'The mailinglist archives at sourceforge.net suuuuuuuuuuuuuuuuuck. It seems you can\'t view a full thread across months. wtf?',
    likes: 0,
    retweets: 0
  },
  {
    id: '15789428287',
    created: 1276102294000,
    type: 'post',
    text: 'The DNS records for in.relation.to inside of Red Hat never got updated. That\'s why it seemed down. The DNS is being updated now.',
    likes: 0,
    retweets: 0
  },
  {
    id: '15754940181',
    created: 1276054547000,
    type: 'post',
    text: 'Which slogan would you like to see on the #Arquillian shirts? <a href="http://community.jboss.org/poll.jspa?poll=1047" rel="noopener noreferrer" target="_blank">community.jboss.org/poll.jspa?poll=1047</a> (suggestions welcome)',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '15738948244',
    created: 1276038447000,
    type: 'post',
    text: 'I\'ve said it before. I\'ll say it again. I love how easy it is to change flights w/ #SouthWest. I ♥ SouthWest. Can I have free wifi now?',
    likes: 0,
    retweets: 0,
    tags: ['southwest']
  },
  {
    id: '15734696930',
    created: 1276033843000,
    type: 'post',
    text: 'Future blogs by <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> related to JBoss Tools &amp; Developer Studio will be done at <a href="http://community.jboss.org/en/jbosstools/blog/" rel="noopener noreferrer" target="_blank">community.jboss.org/en/jbosstools/blog/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '15732858346',
    created: 1276031763000,
    type: 'post',
    text: 'Every great golf season starts with a new club, I like to think. This is the year of the putter. Got a Nike OZ #1. Such a nice feel!',
    likes: 0,
    retweets: 0
  },
  {
    id: '15732571866',
    created: 1276031444000,
    type: 'post',
    text: 'As a workaround to avoid forgetting another birthday, I\'ll go early. Happy Birthday to <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> tomorrow!',
    likes: 0,
    retweets: 0
  },
  {
    id: '15726906251',
    created: 1276025033000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <a class="mention" href="https://x.com/cwash" rel="noopener noreferrer" target="_blank">@cwash</a> Yep, it\'s on the list ;)<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/15723109117" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">15723109117</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '15714224657',
    created: 1276011337000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jakobkorherr" rel="noopener noreferrer" target="_blank">@jakobkorherr</a> Please fall towards the forums. We\'ll be there to catch you ;)<br><br>In reply to: <a href="https://x.com/jakobkorherr/status/15698095925" rel="noopener noreferrer" target="_blank">@jakobkorherr</a> <span class="status">15698095925</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '15712374435',
    created: 1276009462000,
    type: 'post',
    text: 'I\'m no rockstar to my wife until I speak at a conference in Dublin. Seems I missed it once again this year. I need a personal invitation.',
    likes: 0,
    retweets: 0
  },
  {
    id: '15682506895',
    created: 1275971335000,
    type: 'post',
    text: 'Hot deployment is brewing in the JBoss Community. <a href="http://community.jboss.org/message/546608" rel="noopener noreferrer" target="_blank">community.jboss.org/message/546608</a>',
    likes: 0,
    retweets: 1
  },
  {
    id: '15675041597',
    created: 1275964003000,
    type: 'post',
    text: 'Happy Belated Birthday to <a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a>! I blame my tardiness on the jetlag. Hope it was a day of successful builds and smooth deployments.',
    likes: 0,
    retweets: 1
  },
  {
    id: '15671095768',
    created: 1275960608000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> I\'m working on a post that builds on a TODT by <a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> to show how #Arquillian simplifies Java EE testing &amp; makes it portable.<br><br>In reply to: @pelegri <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '15654740467',
    created: 1275943235000,
    type: 'post',
    text: 'Keep your eye on blogs.jboss.org. There are new bloggers coming online everyday, including Stuart Douglas and Ken Finnigan.',
    likes: 0,
    retweets: 0
  },
  {
    id: '15654425375',
    created: 1275942859000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> If I had a lawn, I would definitely mow drunk, to take the pain away from my allergies going nuts ;)<br><br>In reply to: <a href="https://x.com/ALRubinger/status/15579053167" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">15579053167</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '15654270067',
    created: 1275942673000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> I\'ll raise my coffee mug to that!<br><br>In reply to: <a href="https://x.com/ALRubinger/status/15604650574" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">15604650574</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '15653557535',
    created: 1275941857000,
    type: 'post',
    text: 'Any container that supports JSR 88 should be able to piggy back on the JSR 88 container implementation <a href="https://jira.jboss.org/browse/ARQ-156" rel="noopener noreferrer" target="_blank">jira.jboss.org/browse/ARQ-156</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '15653476701',
    created: 1275941764000,
    type: 'post',
    text: 'I implemented a JSR 88 container for #Arquillian this weekend. From that I created a GlassFish remote container in a few lines.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '15639298411',
    created: 1275926916000,
    type: 'post',
    text: 'I\'m amazed at how many container or spec compliance bugs I\'ve uncovered using #Arquillian...without even really looking for them.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '15621920033',
    created: 1275909077000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> did an awesome job presenting the #Arquillian lightening talk at #jazoon. He kept the delivery relaxed &amp; still got it all in.',
    likes: 0,
    retweets: 2,
    tags: ['arquillian', 'jazoon']
  },
  {
    id: '15620352457',
    created: 1275906782000,
    type: 'post',
    text: 'Up early for e-mail Monday.',
    likes: 0,
    retweets: 0
  },
  {
    id: '15503705455',
    created: 1275762040000,
    type: 'post',
    text: 'The #cdi hashtag is a wasteland of spam tweets. If you are tweeting about JSR-299 (CDI), I recommend using #jsr299.',
    likes: 0,
    retweets: 0,
    tags: ['cdi', 'jsr299']
  },
  {
    id: '15503646530',
    created: 1275761971000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/TheASF" rel="noopener noreferrer" target="_blank">@TheASF</a> Did you know that #Weld is an Apache-licensed implementation of the #jsr299 specification too ;) <a href="http://seamframework.org/Weld" rel="noopener noreferrer" target="_blank">seamframework.org/Weld</a>',
    likes: 0,
    retweets: 0,
    tags: ['weld', 'jsr299']
  },
  {
    id: '15503114850',
    created: 1275761352000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> I\'ve hidden the names to protect the, well, certainly not innocent...just friendly. If you feel guilty, you are ;)<br><br>In reply to: <a href="https://x.com/jeremynorris/status/15502835220" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">15502835220</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '15502082764',
    created: 1275760165000,
    type: 'post',
    text: 'It\'s a bad thing if you have to look at the source code of an API to know how to use it. #javadoc #fail',
    likes: 0,
    retweets: 1,
    tags: ['javadoc', 'fail']
  },
  {
    id: '15452113672',
    created: 1275697167000,
    type: 'post',
    text: 'What do you want to pick up for dinner? Chinese? Mexican? Pizza? Hmm, I was thinking more like Apple.',
    photos: ['<div class="entry"><img class="photo" src="media/15452113672.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '15399968157',
    created: 1275636745000,
    type: 'post',
    text: 'I\'m counting down to my vacation by the number of conferences I have left before then :)',
    likes: 0,
    retweets: 0
  },
  {
    id: '15399913501',
    created: 1275636654000,
    type: 'post',
    text: 'I have so much to write, share and talk about it\'s making my head explode ;) One thing at a time. First thing, get home from Zurich.',
    likes: 0,
    retweets: 0
  },
  {
    id: '15396545175',
    created: 1275631347000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremyg484" rel="noopener noreferrer" target="_blank">@jeremyg484</a> Haha. I\'m with you about the beer. Life\'s to short to drink cheap beer.<br><br>In reply to: <a href="https://x.com/jeremyg484/status/15357393108" rel="noopener noreferrer" target="_blank">@jeremyg484</a> <span class="status">15357393108</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '15351175515',
    created: 1275598655000,
    type: 'post',
    text: 'Zurich transit reminds you "Please don\'t saw the tram seats."',
    photos: ['<div class="entry"><img class="photo" src="media/15351175515.jpg"></div>'],
    likes: 0,
    retweets: 2
  },
  {
    id: '15351058325',
    created: 1275598513000,
    type: 'post',
    text: 'The Limmat river in Zurich...boy that current is fast!',
    photos: ['<div class="entry"><img class="photo" src="media/15351058325.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '15350987073',
    created: 1275598428000,
    type: 'post',
    text: 'Zurich center city.',
    photos: ['<div class="entry"><img class="photo" src="media/15350987073.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '15348681718',
    created: 1275595685000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> I talked to Mark Newton about this the other night. We need to pull IRC logs into jboss.org site &amp; make them searchable.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/15340931118" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">15340931118</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '15348543995',
    created: 1275595522000,
    type: 'post',
    text: 'That\'s a wrap on a successful and productive #jazoon, rounded it off by getting a Paulaner beer w/ <a class="mention" href="https://x.com/wesleyhales" rel="noopener noreferrer" target="_blank">@wesleyhales</a>. Now, I\'m just beat.',
    likes: 0,
    retweets: 0,
    tags: ['jazoon']
  },
  {
    id: '15348332002',
    created: 1275595270000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matkar" rel="noopener noreferrer" target="_blank">@matkar</a> Indeed. I\'ve been meaning to get back to you. I think 2011 is my year for jfokus, finally :)<br><br>In reply to: <a href="https://x.com/matkar/status/15342074753" rel="noopener noreferrer" target="_blank">@matkar</a> <span class="status">15342074753</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '15339611770',
    created: 1275585496000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/datafront" rel="noopener noreferrer" target="_blank">@datafront</a> We\'ll have audio and a screencast of the #Arquillian talk up in a few days.<br><br>In reply to: <a href="https://x.com/datafront/status/15326931178" rel="noopener noreferrer" target="_blank">@datafront</a> <span class="status">15326931178</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '15339518687',
    created: 1275585400000,
    type: 'post',
    text: 'Real Java EE testing w/ #Arquillian and #ShrinkWrap by <a class="mention" href="https://x.com/mojavelinux" rel="noopener noreferrer" target="_blank">@mojavelinux</a> &amp; <a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> was well attended #jazoon',
    photos: ['<div class="entry"><img class="photo" src="media/15339518687.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'shrinkwrap', 'jazoon']
  },
  {
    id: '15339295773',
    created: 1275585175000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> That\'s impact. Go to talk on Maven 3 one day. Get impressed. Use it in your talk the next day ;)',
    likes: 1,
    retweets: 0
  },
  {
    id: '15338682041',
    created: 1275584560000,
    type: 'post',
    text: 'The Java EE 6 web profile hits a happy medium; devs can be productive and have all key technologies they need to create enterprise apps.',
    likes: 0,
    retweets: 0
  },
  {
    id: '15338600038',
    created: 1275584480000,
    type: 'post',
    text: 'I like to say it, "Don\'t settle for a Servlet container." I say that because it\'s like having to bring your own beer to the party.',
    likes: 0,
    retweets: 2
  },
  {
    id: '15326581416',
    created: 1275573363000,
    type: 'post',
    text: 'Aslak and I are about to demo how to do real Java EE testing with #Arquillian.',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '15308867021',
    created: 1275548633000,
    type: 'post',
    text: '#Arquillian talk today at #jazoon, 16:00. Get invaded. (Free shirts too ;))',
    likes: 0,
    retweets: 2,
    tags: ['arquillian', 'jazoon']
  },
  {
    id: '15278332612',
    created: 1275514490000,
    type: 'post',
    text: 'Had my first hemp beer tonight. <a href="http://www.time.com/time/magazine/article/0,9171,407293,00.html" rel="noopener noreferrer" target="_blank">www.time.com/time/magazine/article/0,9171,407293,00.html</a> (Time magazine article) That could explain some of my typos ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '15278166895',
    created: 1275514288000,
    type: 'post',
    text: '...that would be on the topic of "if humans were computing systems"',
    likes: 0,
    retweets: 0
  },
  {
    id: '15278117305',
    created: 1275514229000,
    type: 'post',
    text: 'Quote from Brazilian colleague at dinner "Women\'s brains have a different engine" I\'d throw in they have a different interpretor too ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '15277459816',
    created: 1275513444000,
    type: 'post',
    text: 'Speaking of soundtracks, I\'ve suggested that we add startup music to JBoss AS so that you can enjoy your time while it starts up ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '15277410364',
    created: 1275513384000,
    type: 'post',
    text: 'The Eclipse soundtrack is trending in Google. Now that is something that would motivate me to use an IDE...if it had a soundtrack ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '15277352042',
    created: 1275513313000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> I\'m interested to hear why. Then, I\'d like to show you an #Arquillian test that can test it :) I\'ve been playing w/ the combo.<br><br>In reply to: <a href="https://x.com/JohnAment/status/15273887108" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">15273887108</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '15277228353',
    created: 1275513163000,
    type: 'reply',
    text: '@translucent_eye I wasn\'t interested in Maven 3; wasn\'t on my radar. <a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> ignited my interested. Now, mvn command == Maven 3 :)<br><br>In reply to: <a href="https://x.com/davidsachdev/status/15259021994" rel="noopener noreferrer" target="_blank">@davidsachdev</a> <span class="status">15259021994</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '15276738140',
    created: 1275512567000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> No worries. We\'ve always tomorrow. Glad to hear you had some great discussions. Ours were a little more disturbing ;)<br><br>In reply to: <a href="https://x.com/matthewmccull/status/15270665716" rel="noopener noreferrer" target="_blank">@matthewmccull</a> <span class="status">15270665716</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '15254735184',
    created: 1275488711000,
    type: 'post',
    text: 'Maven 3 talk by <a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> was incredible. Best talk I\'ve seen in a long time, and I\'ve seen some impressive speakers. #jazoon',
    likes: 1,
    retweets: 0,
    tags: ['jazoon']
  },
  {
    id: '15241475032',
    created: 1275473108000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ciurana" rel="noopener noreferrer" target="_blank">@ciurana</a> Truth be told, Maven 3 is a far better citizen of the internet. They are finally getting things under control.<br><br>In reply to: <a href="https://x.com/ciurana/status/15240470733" rel="noopener noreferrer" target="_blank">@ciurana</a> <span class="status">15240470733</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '15241280969',
    created: 1275472806000,
    type: 'post',
    text: 'Maven 3 is a library. This fact opens doors to things like shell, IDE integration and polyglot. #jazoon',
    likes: 0,
    retweets: 1,
    tags: ['jazoon']
  },
  {
    id: '15241223630',
    created: 1275472717000,
    type: 'post',
    text: 'If you have used the maven-cli-plugin, make the move to Maven 3 shell today. It\'s the future for using a warmed up Maven instance.',
    likes: 0,
    retweets: 1
  },
  {
    id: '15240414765',
    created: 1275471369000,
    type: 'post',
    text: '"Maven 3 beta is very robust, easily RC quality." #jazoon',
    likes: 0,
    retweets: 0,
    tags: ['jazoon']
  },
  {
    id: '15240335390',
    created: 1275471236000,
    type: 'post',
    text: '"Sonatype is in the same vein as Red Hat for the Maven community" Matthew McCullough at #jazoon',
    likes: 0,
    retweets: 0,
    tags: ['jazoon']
  },
  {
    id: '15236820236',
    created: 1275465167000,
    type: 'post',
    text: 'The secret to a stress free trip: Have someone at home nuking your e-mail (We\'ll, not the important ones of course).',
    likes: 0,
    retweets: 0
  },
  {
    id: '15236332549',
    created: 1275464326000,
    type: 'post',
    text: 'Neal Ford asks "At what point will the Maven traffic on the internet surpass the spam traffic?"',
    likes: 0,
    retweets: 2
  },
  {
    id: '15208172569',
    created: 1275431940000,
    type: 'post',
    text: 'In trunk, ShrinkWrap.create("test.jar", JavaArchive.class) can now be written as ShrinkWrap.create(JavaArchive.class). Awesome.',
    likes: 0,
    retweets: 1
  },
  {
    id: '15208007549',
    created: 1275431755000,
    type: 'post',
    text: 'Here\'s a crazy idea that just might take off. Use #Arquillian to bundle tests as apk and run them inside the #Android phone emulator.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'android']
  },
  {
    id: '15207899776',
    created: 1275431640000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> Uh oh. After I did that, I didn\'t come up for air for nearly a week. It\'s addictive.<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/15206639696" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">15206639696</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '15206133964',
    created: 1275429637000,
    type: 'post',
    text: 'Officially feeling jet lag at the end of #jazoon day #1. Or was it the 5 hour conversation about JBoss Community. It\'s a toss up :)',
    likes: 0,
    retweets: 0,
    tags: ['jazoon']
  },
  {
    id: '15130103762',
    created: 1275337444000,
    type: 'post',
    text: 'NetBeans 6.9 RC1 is too unstable for my taste. I lost focus in X 3 times in a row. Back to 6.8. Not all is lost, it has the CDI templates.',
    likes: 0,
    retweets: 0
  },
  {
    id: '15128338935',
    created: 1275335123000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> I\'d give you the shirt off my back ;)<br><br>In reply to: <a href="https://x.com/matthewmccull/status/15127519762" rel="noopener noreferrer" target="_blank">@matthewmccull</a> <span class="status">15127519762</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '15127301001',
    created: 1275333790000,
    type: 'post',
    text: 'Just had the chef\'s special plate at Coco w/ <a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> &amp; <a class="mention" href="https://x.com/wesleyhales" rel="noopener noreferrer" target="_blank">@wesleyhales</a>. Wesley got on us for bringing too much #Arquillian to the table.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '15127176244',
    created: 1275333630000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> I\'m sure glad you made it! We drank your beer and ate your steak for you so it didn\'t go to waste.<br><br>In reply to: <a href="https://x.com/matthewmccull/status/15114961431" rel="noopener noreferrer" target="_blank">@matthewmccull</a> <span class="status">15114961431</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '15127104594',
    created: 1275333540000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Absolutely. Coming your way soon (and likely a blog entry as well).<br><br>In reply to: <a href="https://x.com/ALRubinger/status/15119935739" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">15119935739</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '15112507072',
    created: 1275317784000,
    type: 'post',
    text: 'Just created JPA 2-driven JAX-RS service and tested it running in embedded GlassFish w/ #Arquillian <a class="mention" href="https://x.com/run" rel="noopener noreferrer" target="_blank">@run</a>(AS_CLIENT) &amp; RESTEasy client API.',
    likes: 1,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '15096844576',
    created: 1275297865000,
    type: 'post',
    text: 'The template manager in NetBeans is barely usable. I figured out what files it expects and I just created them outside of NetBeans.',
    likes: 0,
    retweets: 0
  },
  {
    id: '15096561807',
    created: 1275297397000,
    type: 'reply',
    text: '@riacowboy Oh shoot, I didn\'t check my tweets before I shutdown. Hope it was an enjoyable flight! I was cramped back in coach ;)<br><br>In reply to: <a href="https://x.com/_JamesWard/status/15064850465" rel="noopener noreferrer" target="_blank">@_JamesWard</a> <span class="status">15064850465</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '15096515240',
    created: 1275297319000,
    type: 'post',
    text: 'Long way to go still. I got extremely frustrated over the weekend when three different tools I tried to use had documentation #fail.',
    likes: 0,
    retweets: 0,
    tags: ['fail']
  },
  {
    id: '15096482640',
    created: 1275297265000,
    type: 'post',
    text: 'Just had a warm-up ideas session with <a class="mention" href="https://x.com/wesleyhales" rel="noopener noreferrer" target="_blank">@wesleyhales</a> about various tech topics and the JBoss developer experience revolution.',
    likes: 0,
    retweets: 0
  },
  {
    id: '15096439886',
    created: 1275297189000,
    type: 'post',
    text: 'Arrived in Zurich bright an early, met with <a class="mention" href="https://x.com/wesleyhales" rel="noopener noreferrer" target="_blank">@wesleyhales</a> at the airport and got our morning workout on the way to the hotel.',
    likes: 0,
    retweets: 0
  },
  {
    id: '15064447839',
    created: 1275253635000,
    type: 'post',
    text: 'About to board my flight out of IAD to Zurich for #Jazoon.',
    likes: 0,
    retweets: 0,
    tags: ['jazoon']
  },
  {
    id: '15063064554',
    created: 1275251749000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> I was disappointed that none of the instructions in the Hibernate docs or blog post worked for me.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/15052213251" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">15052213251</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '15023950888',
    created: 1275199155000,
    type: 'post',
    text: 'Lemma turns software documentation inside out by making the example code the first class construct. <a href="http://www.teleal.org/projects/lemma/" rel="noopener noreferrer" target="_blank">www.teleal.org/projects/lemma/</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '15009907039',
    created: 1275182181000,
    type: 'post',
    text: 'Hibernate\'s JPA 2 annotation processor puts the *_.java meta model files in target/classes &amp; they never get compiled. wtf?',
    likes: 0,
    retweets: 0
  },
  {
    id: '15005962272',
    created: 1275177093000,
    type: 'post',
    text: 'Alpha2 release of #Seam 3 XML Configuration module is available <a href="http://community.jboss.org/blogs/stuartdouglas/2010/05/29/seam-xml-configuration-alpha2-released" rel="noopener noreferrer" target="_blank">community.jboss.org/blogs/stuartdouglas/2010/05/29/seam-xml-configuration-alpha2-released</a>',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '15005298589',
    created: 1275176192000,
    type: 'post',
    text: 'I rave about the Art of Community &amp; how it has guided me as Seam Community Liaison in this interview: <a href="http://jaxenter.com/i-didn-t-want-others-to-overlook-this-framework-11947.html" rel="noopener noreferrer" target="_blank">jaxenter.com/i-didn-t-want-others-to-overlook-this-framework-11947.html</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '15005088932',
    created: 1275175902000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> will appreciate this one. "Your contract with Open Source never runs out" <a href="https://community.jboss.org/people/dan.j.allen/blog/2010/05/29/your-contract-with-open-source-never-runs-out" rel="noopener noreferrer" target="_blank">community.jboss.org/people/dan.j.allen/blog/2010/05/29/your-contract-with-open-source-never-runs-out</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14995291246',
    created: 1275163025000,
    type: 'post',
    text: 'Does anyone know how to add an Undeploy target to a #NetBeans Maven project? I hate having to switch to the Servers node to undeploy.',
    likes: 0,
    retweets: 0,
    tags: ['netbeans']
  },
  {
    id: '14992882586',
    created: 1275159985000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> There was a communication breakdown in getting the DNS record updated after migration to a new server on Friday.<br><br>In reply to: <a href="https://x.com/majson/status/14991090618" rel="noopener noreferrer" target="_blank">@majson</a> <span class="status">14991090618</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14992846198',
    created: 1275159937000,
    type: 'post',
    text: 'We finally got through to Red Hat IT. seamframework.org is back. Sorry for the inconvenience. I really hate having to say that.',
    likes: 0,
    retweets: 0
  },
  {
    id: '14980034928',
    created: 1275145736000,
    type: 'post',
    text: 'Add these lines to /etc/hosts to access seamframework.org:<br>216.154.211.218 seamframework.org<br>216.154.211.218 www.seamframework.org',
    likes: 0,
    retweets: 0
  },
  {
    id: '14979911551',
    created: 1275145612000,
    type: 'post',
    text: 'seamframework.org will continue to be down until Red Hat IT picks up the phone and changes the DNS record.',
    likes: 0,
    retweets: 0
  },
  {
    id: '14956510427',
    created: 1275109778000,
    type: 'post',
    text: 'Once again, Pete Muir is working his Maven wizardry refining the #Seam 3 POMs. We have tight guidelines to keep everything organized.',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '14949050865',
    created: 1275100960000,
    type: 'post',
    text: 'The communication about the DNS entry for seamframework.org got f*ed up big time. The site is offline and the phone is dead. Frustrating.',
    likes: 0,
    retweets: 0
  },
  {
    id: '14925716725',
    created: 1275073049000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jakobkorherr" rel="noopener noreferrer" target="_blank">@jakobkorherr</a> It hasn\'t been announced yet, but once again my Twitter VIPs get inside info: let\'s just say the MyFaces guys will be happy ;)<br><br>In reply to: <a href="https://x.com/jakobkorherr/status/14922758755" rel="noopener noreferrer" target="_blank">@jakobkorherr</a> <span class="status">14922758755</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14925576948',
    created: 1275072887000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dzone" rel="noopener noreferrer" target="_blank">@dzone</a> Congrats to <a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> for earning the #3 spot with the Git refcard.<br><br>In reply to: <a href="https://x.com/DZoneInc/status/14922605465" rel="noopener noreferrer" target="_blank">@DZoneInc</a> <span class="status">14922605465</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14921297922',
    created: 1275068121000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jakobkorherr" rel="noopener noreferrer" target="_blank">@jakobkorherr</a> Yep. We\'re actually trying to get the MyFaces guys involved in Seam Faces to put our heads together, create something better.<br><br>In reply to: <a href="https://x.com/jakobkorherr/status/14894198153" rel="noopener noreferrer" target="_blank">@jakobkorherr</a> <span class="status">14894198153</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14921125991',
    created: 1275067941000,
    type: 'post',
    text: 'This information is a bit belated, but we are migrating seamframework.org and in.relation.to to a different server host today. #seam',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '14914968599',
    created: 1275061914000,
    type: 'post',
    text: 'Welcome <a class="mention" href="https://x.com/sten_aksel" rel="noopener noreferrer" target="_blank">@sten_aksel</a> as a contributor to #ShrinkWrap!',
    likes: 0,
    retweets: 0,
    tags: ['shrinkwrap']
  },
  {
    id: '14884341253',
    created: 1275021533000,
    type: 'post',
    text: 'And by alternate energy sources, I mean ones that don\'t require extraction.',
    likes: 0,
    retweets: 0
  },
  {
    id: '14884290210',
    created: 1275021470000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> Point well taken. I support alternative energy sources because I know that money doesn\'t grow on trees, nor does oil.<br><br>In reply to: <a href="https://x.com/metacosm/status/14863905422" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">14863905422</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14863474595',
    created: 1274999426000,
    type: 'post',
    text: 'It makes me sick to see people pumping gas at a BP station.',
    likes: 0,
    retweets: 0
  },
  {
    id: '14846360221',
    created: 1274978970000,
    type: 'post',
    text: 'Alright! I made a test screencast with audio (from a mic) in Ubuntu using gtk-recordmydesktop. Now I\'m ready to create a real one.',
    likes: 0,
    retweets: 0
  },
  {
    id: '14842150166',
    created: 1274974778000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/derkdukker" rel="noopener noreferrer" target="_blank">@derkdukker</a> I should clarify that I need both Eclipse and NetBeans at the same time. NetBeans is my Maven IDE &amp; Eclipse is my Java editor.<br><br>In reply to: <a href="https://x.com/derkdukker/status/14776260440" rel="noopener noreferrer" target="_blank">@derkdukker</a> <span class="status">14776260440</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14842090201',
    created: 1274974720000,
    type: 'post',
    text: 'The only thing that bugs me about Chromium on Ubuntu is that eventually sound in flash stops working and I have to restart it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '14835354409',
    created: 1274967943000,
    type: 'post',
    text: 'About to start the weekly #Seam community meeting in #seam-dev on Freenode IRC.',
    likes: 0,
    retweets: 0,
    tags: ['seam', 'seam']
  },
  {
    id: '14814294340',
    created: 1274936778000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> I hear you. But hopefully, ASL-licensed #Arquillian will eventually help you test GlassFish ;) #pathsmerge<br><br>In reply to: <a href="https://x.com/jasondlee/status/14814184258" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">14814184258</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'pathsmerge']
  },
  {
    id: '14814169672',
    created: 1274936608000,
    type: 'post',
    text: 'Just added an FAQ: How do I setup a DataSource in Embedded GlassFish w/ Arquillian. <a href="http://community.jboss.org/wiki/HowdoIsetupaDataSourceinEmbeddedGlassFishwhenusingArquillian" rel="noopener noreferrer" target="_blank">community.jboss.org/wiki/HowdoIsetupaDataSourceinEmbeddedGlassFishwhenusingArquillian</a>',
    likes: 1,
    retweets: 1
  },
  {
    id: '14813477708',
    created: 1274935681000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Why don\'t you join in the #Arquillian forums and perhaps we can help you get CDI support and #Arquillian integration.<br><br>In reply to: <a href="https://x.com/jasondlee/status/14811114241" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">14811114241</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'arquillian']
  },
  {
    id: '14810364241',
    created: 1274931866000,
    type: 'post',
    text: 'The #Arquillian shirts from cafepress have arrived. Awesome. 10 lucky winners will get abducted and fitted with them at #Jazoon.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'jazoon']
  },
  {
    id: '14794768116',
    created: 1274915684000,
    type: 'post',
    text: 'Java EE Testing "Gets Real" is still holding the #2 spot on #DZone <a href="http://www.dzone.com/links/java_ee_testing_gets_real.html" rel="noopener noreferrer" target="_blank">www.dzone.com/links/java_ee_testing_gets_real.html</a> #Arquillian',
    likes: 0,
    retweets: 1,
    tags: ['dzone', 'arquillian']
  },
  {
    id: '14743327214',
    created: 1274850675000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> I\'ve suggested the ThreadLocal scope on several occasions, but haven\'t followed up. Want to help work on it in #Seam?<br><br>In reply to: <a href="https://x.com/jeremynorris/status/14731379938" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">14731379938</span>',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '14743137402',
    created: 1274850412000,
    type: 'post',
    text: 'I\'m still a Lost virgin. Wait, that didn\'t come out right.',
    likes: 0,
    retweets: 0
  },
  {
    id: '14730949306',
    created: 1274837082000,
    type: 'post',
    text: '"Your cashier today was ACM lane #2" #personaltouch #fail',
    likes: 0,
    retweets: 0,
    tags: ['personaltouch', 'fail']
  },
  {
    id: '14719678746',
    created: 1274824401000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dzone" rel="noopener noreferrer" target="_blank">@dzone</a> I can\'t wait to use any Android 2 feature. #tmobile #upgrade #waiting<br><br>In reply to: <a href="https://x.com/DZoneInc/status/14717508775" rel="noopener noreferrer" target="_blank">@DZoneInc</a> <span class="status">14717508775</span>',
    likes: 0,
    retweets: 0,
    tags: ['tmobile', 'upgrade', 'waiting']
  },
  {
    id: '14719627787',
    created: 1274824338000,
    type: 'post',
    text: '#NetBeans 6.9 RC is out. Time to give it a try. I\'d like to enjoy some of those JSF and CDI features. (btw, I use both Eclipse &amp; NetBeans).',
    likes: 0,
    retweets: 1,
    tags: ['netbeans']
  },
  {
    id: '14713921001',
    created: 1274817114000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> But if you have a new system, start with unit tests and work upwards.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/14706891559" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">14706891559</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14713906081',
    created: 1274817095000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Jared Richardson explains situation well. If you\'ve got big app w/o tests, start w/ UI tests. Ultimately, you want unit tests.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/14706891559" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">14706891559</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14651946012',
    created: 1274740825000,
    type: 'post',
    text: 'If you are going to be at Jazoon, and you are a long-distance runner, catch up with <a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a>.',
    likes: 0,
    retweets: 0
  },
  {
    id: '14650103786',
    created: 1274738655000,
    type: 'post',
    text: 'Come close, I need to whisper something to you. A new release of #Arquillian is about to be announced.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '14644593359',
    created: 1274731897000,
    type: 'post',
    text: 'Just ordered #Arquillian swag for #Jazoon. We\'ll have 10 t-shirts to give away.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'jazoon']
  },
  {
    id: '14638229638',
    created: 1274723994000,
    type: 'post',
    text: 'Evidence of a poor design. Yes, that socket used to house a heat lamp.',
    photos: ['<div class="entry"><img class="photo" src="media/14638229638.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '14579783582',
    created: 1274650101000,
    type: 'post',
    text: 'Came across a plaque: "Let no man imagine that he has no influence." The key is discovering that influence and make the most of it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '14579709492',
    created: 1274650005000,
    type: 'post',
    text: 'Had to pick through my belongings after my parents swept everything out of my old bedroom. The bedroom is no longer suspended in time.',
    likes: 0,
    retweets: 0
  },
  {
    id: '14579528697',
    created: 1274649756000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> Sweet.<br><br>In reply to: <a href="https://x.com/dhanji/status/14577071830" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">14577071830</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14576417955',
    created: 1274645523000,
    type: 'post',
    text: 'Navigating FIOS On Demand to get to the next episode of a show I watch every week is tedious as hell. I think to myself stupid, stupid box.',
    likes: 0,
    retweets: 0
  },
  {
    id: '14576368228',
    created: 1274645457000,
    type: 'post',
    text: 'Google TV, you had me at "Spend less time finding what you want and more time watching what you want." <a href="http://www.vineetmanohar.com/2010/05/what-is-google-tv-in-2-minutes/" rel="noopener noreferrer" target="_blank">www.vineetmanohar.com/2010/05/what-is-google-tv-in-2-minutes/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14536744196',
    created: 1274589321000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> My sentiments exactly.<br><br>In reply to: <a href="https://x.com/kito99/status/14518643146" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">14518643146</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14509132838',
    created: 1274552658000,
    type: 'post',
    text: 'My advice for staying connected as a remote employee: <a href="http://community.jboss.org/people/dan.j.allen/blog/2010/05/22/staying-connected-as-a-remote-employee" rel="noopener noreferrer" target="_blank">community.jboss.org/people/dan.j.allen/blog/2010/05/22/staying-connected-as-a-remote-employee</a> #jboss #redhat',
    likes: 3,
    retweets: 2,
    tags: ['jboss', 'redhat']
  },
  {
    id: '14458553260',
    created: 1274483796000,
    type: 'post',
    text: 'Vote for openness, now on the jboss.org homepage.',
    likes: 1,
    retweets: 1
  },
  {
    id: '14458475109',
    created: 1274483700000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> I can send you a monitor if you want one. Just say the word. Viewsonic VA2226w (this is an internal Red Hat offer ;))<br><br>In reply to: <a href="https://x.com/lincolnthree/status/14450990268" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">14450990268</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14447974608',
    created: 1274469915000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> I agree and back that strategy. Context switching was a killer for me when writing Seam in Action.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/14383935077" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">14383935077</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14408180533',
    created: 1274418082000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> has important things to tell you about Java EE. That\'s why I encourage you to vote him into JUDCon: <a href="http://ocpsoft.com/opensource/vote-for-prettyfaces-at-judcon-jboss-world-red-hat-summit/" rel="noopener noreferrer" target="_blank">ocpsoft.com/opensource/vote-for-prettyfaces-at-judcon-jboss-world-red-hat-summit/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14407666773',
    created: 1274417346000,
    type: 'post',
    text: 'Wow! Chromium uses gutter indicators in the scrollbar to show offsets in page that match your find query.',
    photos: ['<div class="entry"><img class="photo" src="media/14407666773.png"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '14407010755',
    created: 1274416427000,
    type: 'post',
    text: 'My favorite bit is our dashboard with Hudson lights: <a href="http://sfwk.org/Seam3/ProjectStatusAndDirection" rel="noopener noreferrer" target="_blank">sfwk.org/Seam3/ProjectStatusAndDirection</a> #seam3',
    likes: 0,
    retweets: 0,
    tags: ['seam3']
  },
  {
    id: '14406967418',
    created: 1274416368000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> We\'ve been hard at work lately ;) What we were really missing was some project pages, which really got things rolling.<br><br>In reply to: <a href="https://x.com/jeremynorris/status/14406046817" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">14406046817</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14404832173',
    created: 1274413656000,
    type: 'post',
    text: 'A new ode to my alma mater <a href="http://www.huffingtonpost.com/2010/05/18/cornell-graduation-song-s_n_580919.html" rel="noopener noreferrer" target="_blank">www.huffingtonpost.com/2010/05/18/cornell-graduation-song-s_n_580919.html</a> #cornell',
    likes: 0,
    retweets: 0,
    tags: ['cornell']
  },
  {
    id: '14403015651',
    created: 1274411605000,
    type: 'post',
    text: 'I just discovered stealth color mode in #Ubuntu last night. Super (Windows key) + m toggles all windows and Super + n the current one.',
    likes: 2,
    retweets: 0,
    tags: ['ubuntu']
  },
  {
    id: '14402815446',
    created: 1274411382000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> So larger goal here is to be able to use Up and Down w/ a home position keybinding in Eclipse (or any IDE)<br><br>In reply to: <a href="https://x.com/lightguardjp/status/14395047212" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">14395047212</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14402761542',
    created: 1274411324000,
    type: 'post',
    text: '#JBoss community is reaching out to developers: Follow @jbossdeveloper <a href="http://jboss.org/webinars" rel="noopener noreferrer" target="_blank">jboss.org/webinars</a> #arquillian #maven #infinispan',
    likes: 0,
    retweets: 0,
    tags: ['jboss', 'arquillian', 'maven', 'infinispan']
  },
  {
    id: '14385643340',
    created: 1274389977000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I\'m trying to find a way to map a key combination to Up and Down globally in Linux.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/14380764400" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">14380764400</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14377631826',
    created: 1274379404000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> No way my finger can make that reach on a desktop keyboard. Maybe on laptop, but the pinky is a goofy finger.<br><br>In reply to: <a href="https://x.com/maxandersen/status/14375295425" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">14375295425</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14374489968',
    created: 1274375731000,
    type: 'post',
    text: 'Frankly, I want to remove the arrow keys from my keyboard because it\'s a hand movement. Might as well go to the mouse at that point.',
    likes: 0,
    retweets: 0
  },
  {
    id: '14374321434',
    created: 1274375551000,
    type: 'post',
    text: 'I give up. I can\'t figure out how to map Ctrl+&lt;some key&gt; to Up or Down in Linux.',
    likes: 0,
    retweets: 0
  },
  {
    id: '14362364349',
    created: 1274363458000,
    type: 'post',
    text: '"From now on, I’m a huge fan of any conference or event that uses movie screens." (via <a class="mention" href="https://x.com/caniszczyk" rel="noopener noreferrer" target="_blank">@caniszczyk</a>) I\'m with you! #devoxx #jazoon + #jfokus',
    likes: 0,
    retweets: 0,
    tags: ['devoxx', 'jazoon', 'jfokus']
  },
  {
    id: '14362329165',
    created: 1274363423000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cagataycivici" rel="noopener noreferrer" target="_blank">@cagataycivici</a> Has happened to me many times too. Freaky.<br><br>In reply to: @cagataycivici <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14346882907',
    created: 1274340964000,
    type: 'post',
    text: '"From now on, I’m a huge fan of any conference or event that uses movie screens." (via <a class="mention" href="https://x.com/caniszczyk" rel="noopener noreferrer" target="_blank">@caniszczyk</a>) I\'m with you! #devoxx #jazoon',
    likes: 0,
    retweets: 2,
    tags: ['devoxx', 'jazoon']
  },
  {
    id: '14345874560',
    created: 1274339123000,
    type: 'post',
    text: 'I gotta hand it to Google for jump starting stalled initiatives. 1 word: fonts! Finally, something besides Sans, Serif &amp; friends! Amen.',
    likes: 0,
    retweets: 0
  },
  {
    id: '14345583828',
    created: 1274338613000,
    type: 'post',
    text: 'Of course, I like VP8 for the fact that it is open, but that goes without saying with me ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '14345442245',
    created: 1274338353000,
    type: 'post',
    text: 'I like VP8 for one simple reason. I can remember it. Like mp3 and ogg. A simple name I can use in a conversation. Not H.234542. #googleio',
    likes: 0,
    retweets: 0,
    tags: ['googleio']
  },
  {
    id: '14343440371',
    created: 1274334922000,
    type: 'post',
    text: 'Smartphones are the thumb\'s big break. "See, I don\'t need the biggest key on the keyboard or to share the load w/ my sibling to be useful!"',
    likes: 0,
    retweets: 0
  },
  {
    id: '14343280639',
    created: 1274334654000,
    type: 'post',
    text: 'Does anyone know how to remap the up and down arrow keys to Ctrl-P and Ctrl-N globally in #eclipse? I can only do Line Up and Down.',
    likes: 0,
    retweets: 0,
    tags: ['eclipse']
  },
  {
    id: '14328897920',
    created: 1274317730000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/datafront" rel="noopener noreferrer" target="_blank">@datafront</a> I\'ll be speaking at the July meeting, so hopefully you can make that one. Follow the mailinglist to vote on the talk.<br><br>In reply to: <a href="https://x.com/datafront/status/14326167284" rel="noopener noreferrer" target="_blank">@datafront</a> <span class="status">14326167284</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14328702353',
    created: 1274317525000,
    type: 'post',
    text: 'TweetsRide for Android now has a multiple username reply feature. Reply super power.',
    likes: 0,
    retweets: 0
  },
  {
    id: '14328598660',
    created: 1274317419000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <a class="mention" href="https://x.com/germanescobar" rel="noopener noreferrer" target="_blank">@germanescobar</a> Want to turn that into the Seam 3 JMX module and lead it?<br><br>In reply to: <a href="https://x.com/jeremynorris/status/14325604223" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">14325604223</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14325346553',
    created: 1274313854000,
    type: 'post',
    text: 'At the Central Maryland JUG watching javadude.com show how to use #Eclipse effectively. Always good to pick up productivity tips.',
    likes: 0,
    retweets: 0,
    tags: ['eclipse']
  },
  {
    id: '14308041351',
    created: 1274292058000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> That means Arquillian will be equally beautiful ;)<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/14296161991" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">14296161991</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14307684256',
    created: 1274291602000,
    type: 'post',
    text: 'Is it just me, or do the Appfuse and Google App Engine logos and names appear very similar?',
    likes: 0,
    retweets: 0
  },
  {
    id: '14257577083',
    created: 1274225768000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/jbossorg" rel="noopener noreferrer" target="_blank">@jbossorg</a> I think the site is trying to tell you something ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '14257512572',
    created: 1274225698000,
    type: 'post',
    text: 'I nominate Gavin and Emmanuel for star spec lead: <a href="http://community.jboss.org/wiki/2010StarSpecLeadNominations" rel="noopener noreferrer" target="_blank">community.jboss.org/wiki/2010StarSpecLeadNominations</a> Will you?',
    likes: 2,
    retweets: 2
  },
  {
    id: '14246306447',
    created: 1274211828000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Although subtle, entities (entity classes) are distinct from entity beans. The old retrieval mechanism is what\'s deprecated.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/14243764128" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">14243764128</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14206759813',
    created: 1274156569000,
    type: 'post',
    text: 'Did you know you can still submit a nomination for the star spec lead award? <a href="http://blogs.sun.com/jcp/entry/2010_star_spec_lead_input" rel="noopener noreferrer" target="_blank">blogs.sun.com/jcp/entry/2010_star_spec_lead_input</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14204221882',
    created: 1274153564000,
    type: 'post',
    text: 'issues.jboss.org Who\'s with me? <a href="https://jira.jboss.org/browse/ORG-598" rel="noopener noreferrer" target="_blank">jira.jboss.org/browse/ORG-598</a> #jboss',
    likes: 0,
    retweets: 0,
    tags: ['jboss']
  },
  {
    id: '14187540241',
    created: 1274135480000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> I\'ll send you an e-mail ;)<br><br>In reply to: <a href="https://x.com/arungupta/status/14187453758" rel="noopener noreferrer" target="_blank">@arungupta</a> <span class="status">14187453758</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14187414766',
    created: 1274135337000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> If you contact Bela Ban, he would be thrilled to go running with you. He is local and will be at the conference on some days.<br><br>In reply to: <a href="https://x.com/arungupta/status/14185404868" rel="noopener noreferrer" target="_blank">@arungupta</a> <span class="status">14185404868</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14109112811',
    created: 1274029503000,
    type: 'post',
    text: 'While I\'m on the subject, #Panera, I love you and your #freewifi.',
    likes: 0,
    retweets: 0,
    tags: ['panera', 'freewifi']
  },
  {
    id: '14109014945',
    created: 1274029382000,
    type: 'post',
    text: 'I am sick of paying internet fees at hotels. If you charge for internet, I\'m not staying at your hotel anymore. #freewifi',
    likes: 0,
    retweets: 0,
    tags: ['freewifi']
  },
  {
    id: '14070889270',
    created: 1273975344000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Sexy! I particularly like the categories, such as Tools &amp; Testing <a href="https://jira.jboss.org/secure/BrowseProjects.jspa#10099" rel="noopener noreferrer" target="_blank">jira.jboss.org/secure/BrowseProjects.jspa#10099</a><br><br>In reply to: <a href="https://x.com/ALRubinger/status/14060269375" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">14060269375</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14023730559',
    created: 1273906193000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Not drug test, a blood test silly ;) Just routine stuff, and to clear me for more international excursions.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/14023457191" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">14023457191</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14013670064',
    created: 1273891877000,
    type: 'post',
    text: 'OSS == many disparate, at times seemingly incompatible, minds join to create something that works, inspires &amp; helps make the world better.',
    likes: 0,
    retweets: 1
  },
  {
    id: '14003116398',
    created: 1273878830000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/unitygirl" rel="noopener noreferrer" target="_blank">@unitygirl</a> You bet. I\'ll be at JBW and JUDCon.<br><br>In reply to: <a href="https://x.com/wow3community/status/13999194314" rel="noopener noreferrer" target="_blank">@wow3community</a> <span class="status">13999194314</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '14003102197',
    created: 1273878811000,
    type: 'post',
    text: 'Pizza and beer night. Mmmmm.',
    likes: 0,
    retweets: 0
  },
  {
    id: '13997735237',
    created: 1273871545000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CharlieCollins" rel="noopener noreferrer" target="_blank">@CharlieCollins</a> But as most people who know me will tell you, most of my talks occur in the common area and bars ;)<br><br>In reply to: <a href="https://x.com/CharlieCollins/status/13993662771" rel="noopener noreferrer" target="_blank">@CharlieCollins</a> <span class="status">13993662771</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13997712614',
    created: 1273871514000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CharlieCollins" rel="noopener noreferrer" target="_blank">@CharlieCollins</a> Yes, I\'ll be speaking on Seam and RESTEasy and our new testing framework, #Arquillian.<br><br>In reply to: <a href="https://x.com/CharlieCollins/status/13993662771" rel="noopener noreferrer" target="_blank">@CharlieCollins</a> <span class="status">13993662771</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '13997013821',
    created: 1273870561000,
    type: 'post',
    text: 'Talks accepted and registration done for #JavaOne. Call me an early bird ;)',
    likes: 0,
    retweets: 1,
    tags: ['javaone']
  },
  {
    id: '13995787471',
    created: 1273868857000,
    type: 'post',
    text: 'That\'s cool! Quest Diagnostics lets you schedule an appt online then adds it to your Google calendar. #ehealth FTW!',
    likes: 0,
    retweets: 0,
    tags: ['ehealth']
  },
  {
    id: '13994625143',
    created: 1273867279000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <a class="mention" href="https://x.com/jbossnews" rel="noopener noreferrer" target="_blank">@jbossnews</a> Damn, I had to go with Sierra Nevada Pale Ale. Please reconsider by adding Harpoon IPA. Local and better.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/13992386754" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">13992386754</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13993553556',
    created: 1273865829000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jbossnews" rel="noopener noreferrer" target="_blank">@jbossnews</a> Now that is my kind of poll! <a href="http://www.redhat.com/promo/summit/2010/" rel="noopener noreferrer" target="_blank">www.redhat.com/promo/summit/2010/</a> #beer<br><br>In reply to: <a href="https://x.com/RHMiddleware/status/13933303913" rel="noopener noreferrer" target="_blank">@RHMiddleware</a> <span class="status">13933303913</span>',
    likes: 0,
    retweets: 0,
    tags: ['beer']
  },
  {
    id: '13993512392',
    created: 1273865771000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/unitygirl" rel="noopener noreferrer" target="_blank">@unitygirl</a> You should touch base with <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a>, who has been championing quickstarts for developers since his first day.<br><br>In reply to: <a href="https://x.com/wow3community/status/13992489716" rel="noopener noreferrer" target="_blank">@wow3community</a> <span class="status">13992489716</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13993108741',
    created: 1273865221000,
    type: 'post',
    text: 'All booked for #Jazoon. Excited to be back in Zurich.',
    likes: 0,
    retweets: 0,
    tags: ['jazoon']
  },
  {
    id: '13983301522',
    created: 1273852477000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pavlobaron" rel="noopener noreferrer" target="_blank">@pavlobaron</a> Thanks!<br><br>In reply to: <a href="https://x.com/pavlobaron/status/13602127293" rel="noopener noreferrer" target="_blank">@pavlobaron</a> <span class="status">13602127293</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13928336973',
    created: 1273775890000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iamroberthanson" rel="noopener noreferrer" target="_blank">@iamroberthanson</a> Sadly, I barely had any German beer cause I started feeling bad the second day. I figured beer and dehydration don\'t mix.<br><br>In reply to: <a href="https://x.com/iamroberthanson/status/13924278913" rel="noopener noreferrer" target="_blank">@iamroberthanson</a> <span class="status">13924278913</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13924724398',
    created: 1273770941000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Hey, that\'s exactly what I\'m doing. So fun.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/13924538957" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">13924538957</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13923272759',
    created: 1273769042000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> It finally got unblocked because they got permission to publish the dependent artifacts, such as EL and Servlet APIs.<br><br>In reply to: <a href="https://x.com/tech4j/status/13909610204" rel="noopener noreferrer" target="_blank">@tech4j</a> <span class="status">13909610204</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13923035283',
    created: 1273768737000,
    type: 'post',
    text: 'Downloading JBoss AS 6.0.0 M3. Noticed that the download is finally named jboss-as rather than just jboss.',
    likes: 0,
    retweets: 0
  },
  {
    id: '13923008852',
    created: 1273768703000,
    type: 'post',
    text: 'Finally feeling better. Been feeling dehydrated and exhausted ever since getting back from Germany.',
    likes: 0,
    retweets: 0
  },
  {
    id: '13788526270',
    created: 1273584508000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kenglxn" rel="noopener noreferrer" target="_blank">@kenglxn</a> Indeed it does. Blog post announcing an alpha release is to follow, so stay tuned.<br><br>In reply to: <a href="https://x.com/kenglxn/status/13787956722" rel="noopener noreferrer" target="_blank">@kenglxn</a> <span class="status">13787956722</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13787640160',
    created: 1273583377000,
    type: 'post',
    text: 'Pete Muir presented #Arquillian to an audience in São Paulo and made some updates to the slides: <a href="https://in.relation.to/2010/05/11/seam-3-slides-arquillian-slides-and-seam-whitepaper/" rel="noopener noreferrer" target="_blank">in.relation.to/2010/05/11/seam-3-slides-arquillian-slides-and-seam-whitepaper/</a>',
    likes: 1,
    retweets: 2,
    tags: ['arquillian']
  },
  {
    id: '13786884664',
    created: 1273582412000,
    type: 'post',
    text: 'Cool! <a class="mention" href="https://x.com/jganoff" rel="noopener noreferrer" target="_blank">@jganoff</a> lists his role as Seam 3 module lead in his LinkedIn profile. #autonomy #success <a href="http://www.linkedin.com/in/jganoff" rel="noopener noreferrer" target="_blank">www.linkedin.com/in/jganoff</a>',
    likes: 0,
    retweets: 0,
    tags: ['autonomy', 'success']
  },
  {
    id: '13786433318',
    created: 1273581805000,
    type: 'post',
    text: '2 #JavaOne talks accepted + 1 BOF. We\'ll throw complexity over the wall w/ #Arquillian / #ShrinkWrap, present road to #Seam3 and talk #CDI.',
    likes: 0,
    retweets: 1,
    tags: ['javaone', 'arquillian', 'shrinkwrap', 'seam3', 'cdi']
  },
  {
    id: '13768773166',
    created: 1273551151000,
    type: 'post',
    text: 'I am chronically behind in e-mail, one of the prices to pay for making it out to meet the community. Tomorrow morning: coffee + replies.',
    likes: 0,
    retweets: 0
  },
  {
    id: '13745617788',
    created: 1273522481000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bileblog" rel="noopener noreferrer" target="_blank">@bileblog</a> I never said I was proud of it. I said "that\'s a start". The numbers are now going in the right direction, down ;)<br><br>In reply to: <a href="https://x.com/bileblog/status/13745341564" rel="noopener noreferrer" target="_blank">@bileblog</a> <span class="status">13745341564</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13745555257',
    created: 1273522389000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/jeresig" rel="noopener noreferrer" target="_blank">@jeresig</a> To clarify, standards will prevail if they are truly open and vendor-neutral. Java ME lost to Android. Lesson learned?',
    likes: 0,
    retweets: 0
  },
  {
    id: '13745304084',
    created: 1273522034000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/jeresig" rel="noopener noreferrer" target="_blank">@jeresig</a> This should motivate the JCP to get reorganized. If Google decides it has a better way to do Java EE, Java EE will get replaced.',
    likes: 0,
    retweets: 0
  },
  {
    id: '13745184175',
    created: 1273521857000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cwash" rel="noopener noreferrer" target="_blank">@cwash</a> A volunteer has stepped forward to create the Weblogic container for #Arquillian. We just need a volunteer for Websphere ;)<br><br>In reply to: <a href="https://x.com/cwash/status/13738334427" rel="noopener noreferrer" target="_blank">@cwash</a> <span class="status">13738334427</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '13745043605',
    created: 1273521653000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Agreed, the extra control in Surefire would be extremely helpful as well.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/13743724000" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">13743724000</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13736922270',
    created: 1273511151000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/caniszczyk" rel="noopener noreferrer" target="_blank">@caniszczyk</a> I started a thread in the #Arquillian forums about #OSGi support. <a href="http://community.jboss.org/thread/151749" rel="noopener noreferrer" target="_blank">community.jboss.org/thread/151749</a> I\'ll send a mail too.<br><br>In reply to: <a href="https://x.com/cra/status/13689783778" rel="noopener noreferrer" target="_blank">@cra</a> <span class="status">13689783778</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'osgi']
  },
  {
    id: '13736842451',
    created: 1273511076000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/caniszczyk" rel="noopener noreferrer" target="_blank">@caniszczyk</a> I\'m thrilled that we ran into each other at #jax2010. I wish I could have talked to you longer, but presentation work called.<br><br>In reply to: <a href="https://x.com/cra/status/13689783778" rel="noopener noreferrer" target="_blank">@cra</a> <span class="status">13689783778</span>',
    likes: 0,
    retweets: 0,
    tags: ['jax2010']
  },
  {
    id: '13735074889',
    created: 1273508955000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Exactly! That\'s why we need a compile-only scope in #Maven. It\'s needed in general when compiling against a platform.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/13688419111" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">13688419111</span>',
    likes: 0,
    retweets: 0,
    tags: ['maven']
  },
  {
    id: '13734418960',
    created: 1273508084000,
    type: 'post',
    text: '#Arquillian may be getting #Weblogic support soon. Feel free to help out if interested <a href="https://community.jboss.org/message/542079" rel="noopener noreferrer" target="_blank">community.jboss.org/message/542079</a>',
    likes: 0,
    retweets: 1,
    tags: ['arquillian', 'weblogic']
  },
  {
    id: '13734342513',
    created: 1273507978000,
    type: 'post',
    text: 'That\'s a start RT <a class="mention" href="https://x.com/grothjan" rel="noopener noreferrer" target="_blank">@grothjan</a> amazing [org.jboss.bootstrap.impl.base.server.AbstractServer] JBossAS [6.0.0.20100429-M3 "Neo"] Started in 17s',
    likes: 0,
    retweets: 1
  },
  {
    id: '13734288365',
    created: 1273507905000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kenglxn" rel="noopener noreferrer" target="_blank">@kenglxn</a> I know the feeling. I get to the point where I just give up getting online abroad. My followers get left in the dark too :)<br><br>In reply to: <a href="https://x.com/kenglxn/status/13733325655" rel="noopener noreferrer" target="_blank">@kenglxn</a> <span class="status">13733325655</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13732595549',
    created: 1273505685000,
    type: 'reply',
    text: '@mwessendorf Excellent news! I\'m looking forward to the upgrade myself. I didn\'t have time to upgrade before leaving for #jax2010.<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['jax2010']
  },
  {
    id: '13731795809',
    created: 1273504634000,
    type: 'post',
    text: 'My #jsf composite component demo from #jax2010 (broken into 6 steps) <a href="http://seaminaction.googlecode.com/svn/demos/presentations/jax2010/" rel="noopener noreferrer" target="_blank">seaminaction.googlecode.com/svn/demos/presentations/jax2010/</a> Will likely use in a Seam example.',
    likes: 1,
    retweets: 1,
    tags: ['jsf', 'jax2010']
  },
  {
    id: '13730843224',
    created: 1273503402000,
    type: 'post',
    text: 'I\'m glad to see the disco theme gone and #Devoxx is back to the slick black &amp; orange color scheme...now with gradients!',
    likes: 0,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '13730527412',
    created: 1273502987000,
    type: 'post',
    text: 'Greenland is mostly snow &amp; ice',
    photos: ['<div class="entry"><img class="photo" src="media/13730527412.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '13730506408',
    created: 1273502960000,
    type: 'post',
    text: 'Iceland\'s terrain is so dramatic:',
    photos: ['<div class="entry"><img class="photo" src="media/13730506408.jpg"></div>', '<div class="entry"><img class="photo" src="media/13730506408-2.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '13730054637',
    created: 1273502375000,
    type: 'post',
    text: 'As we flew over northern Iceland, we could see the ash cloud on the horizon. Amazing.',
    photos: ['<div class="entry"><img class="photo" src="media/13730054637.jpg"></div>'],
    likes: 0,
    retweets: 1
  },
  {
    id: '13729424738',
    created: 1273501567000,
    type: 'post',
    text: 'According to Lewis Black, I should have killed myself, though. Flight was extended to 10 hours to fly above Iceland and over Greenland.',
    likes: 0,
    retweets: 0
  },
  {
    id: '13729389723',
    created: 1273501523000,
    type: 'post',
    text: 'I barely made it out of Germany before the airport was shut down again because of the looming ash cloud.',
    likes: 0,
    retweets: 0
  },
  {
    id: '13628619247',
    created: 1273352370000,
    type: 'post',
    text: 'Spent the day sauntering around Mainz. Beautiful town. I love town centers like this one.',
    photos: ['<div class="entry"><img class="photo" src="media/13628619247.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '13628537630',
    created: 1273352236000,
    type: 'post',
    text: 'The serene Rhine, a view from the site of #jax2010.',
    photos: ['<div class="entry"><img class="photo" src="media/13628537630.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['jax2010']
  },
  {
    id: '13628447881',
    created: 1273352091000,
    type: 'post',
    text: 'A multimedia virtuoso. I\'ve got to find a way to squeeze this into one of my slidedecks.',
    photos: ['<div class="entry"><img class="photo" src="media/13628447881.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '13628316666',
    created: 1273351879000,
    type: 'post',
    text: 'Guess we don\'t need to do this anymore now that we have the internet and ebooks ;)',
    photos: ['<div class="entry"><img class="photo" src="media/13628316666.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '13628219571',
    created: 1273351729000,
    type: 'post',
    text: '#jax2010 eingang',
    photos: ['<div class="entry"><img class="photo" src="media/13628219571.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['jax2010']
  },
  {
    id: '13628162438',
    created: 1273351637000,
    type: 'post',
    text: 'Do your pillows follow the sun?',
    photos: ['<div class="entry"><img class="photo" src="media/13628162438.jpg"></div>'],
    likes: 0,
    retweets: 1
  },
  {
    id: '13628087864',
    created: 1273351517000,
    type: 'post',
    text: 'Not sure if Oracle is appealing to the masses or geeks in this airport ad. Either way, it makes IronMan a sellout',
    photos: ['<div class="entry"><img class="photo" src="media/13628087864.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '13627959414',
    created: 1273351312000,
    type: 'post',
    text: 'You guessed it, I asked for the upgrade.',
    photos: ['<div class="entry"><img class="photo" src="media/13627959414.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '13627518746',
    created: 1273350622000,
    type: 'post',
    text: 'Google seems to always modify their site when I\'m out of the country. It messes with my mind.',
    likes: 0,
    retweets: 0
  },
  {
    id: '13598330235',
    created: 1273307597000,
    type: 'post',
    text: 'Got lots of complements at #jax2010 about Seam in Action. Clearly, I have a lot of German readers ;) Yeah Germany!',
    likes: 0,
    retweets: 1,
    tags: ['jax2010']
  },
  {
    id: '13594179598',
    created: 1273299480000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> I\'m using the one from your book example and the one from the Arquillian examples.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/13593671155" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">13593671155</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13594155495',
    created: 1273299437000,
    type: 'post',
    text: '#Arquillian is now listed on the jboss.org project matrix: <a href="http://www.jboss.org/projects/matrix" rel="noopener noreferrer" target="_blank">www.jboss.org/projects/matrix</a> We\'re officially official!',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '13593548828',
    created: 1273298404000,
    type: 'post',
    text: 'Definitely a new experience for me being at #jax2010 w/ talks in another language. I even got introduced in German. Fish out of water ;)',
    likes: 0,
    retweets: 0,
    tags: ['jax2010']
  },
  {
    id: '13593197041',
    created: 1273297800000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Tag team!<br><br>In reply to: <a href="https://x.com/lincolnthree/status/13487080498" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">13487080498</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13593162524',
    created: 1273297742000,
    type: 'post',
    text: 'Ran into Chris Aniszczyk in hotel lobby. Learned that Eclipse RT != IDE,<br>got him interested in writing the OSGi container for #Arquillian.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '13593115453',
    created: 1273297662000,
    type: 'post',
    text: 'I enjoyed meeting <a class="mention" href="https://x.com/michaelschuetz" rel="noopener noreferrer" target="_blank">@michaelschuetz</a> at #jax2010. Introduced him to #Arquillian, showed him new jboss.org features and talked IDEs. Fun times!',
    likes: 1,
    retweets: 1,
    tags: ['jax2010', 'arquillian']
  },
  {
    id: '13593030427',
    created: 1273297519000,
    type: 'post',
    text: 'I\'ve missed working with the #seam3 devs this week. Exciting things are happening. I need to get back into the loop.',
    likes: 0,
    retweets: 0,
    tags: ['seam3']
  },
  {
    id: '13593007372',
    created: 1273297481000,
    type: 'post',
    text: 'I love seeing the wide-eyed look when I show the #Arquillian JMS test. Not talk on #Arquillian at #jax2010, but talked about it a ton.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'arquillian', 'jax2010']
  },
  {
    id: '13592932826',
    created: 1273297357000,
    type: 'post',
    text: 'I can honestly say I learned key things about #jsf composite components preparing demo for workshop at #jax2010. Tooling is ready too.',
    likes: 0,
    retweets: 0,
    tags: ['jsf', 'jax2010']
  },
  {
    id: '13592787384',
    created: 1273297116000,
    type: 'post',
    text: 'If #Groovy is the syntactic sugar, then #Java is the black coffee.',
    likes: 0,
    retweets: 1,
    tags: ['groovy', 'java']
  },
  {
    id: '13592723446',
    created: 1273297011000,
    type: 'post',
    text: 'If you\'re feeling sick, down a fruit2day (berry). That stuff gives your immune system an incredible boost. Knocks a cool right out.',
    likes: 0,
    retweets: 0
  },
  {
    id: '13592669350',
    created: 1273296923000,
    type: 'post',
    text: 'I felt extremely dehydrated at #jax2010. Barely holding on by end of yesterday. Got tons of water, slept for 13 hrs, feel strong again.',
    likes: 0,
    retweets: 0,
    tags: ['jax2010']
  },
  {
    id: '13473729838',
    created: 1273129420000,
    type: 'post',
    text: 'You know you\'re really busy when you don\'t even have time to tweet.',
    likes: 0,
    retweets: 0
  },
  {
    id: '13418299560',
    created: 1273054787000,
    type: 'post',
    text: 'Just finished typing up responses to an interview for javamagazin.de about Seam, CDI and Java EE 6. Amazing journey, best is yet to come.',
    likes: 0,
    retweets: 0
  },
  {
    id: '13418053994',
    created: 1273054307000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> We should cover all bases, Ant+Ivy, Gradle (Ivy implied), what else? Drives home point that build tool choice doesn\'t matter.<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/13389085051" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">13389085051</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13417993496',
    created: 1273054193000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pilhuhn" rel="noopener noreferrer" target="_blank">@pilhuhn</a> I tried to work out the Berlin JUG, but the schedule just wasn\'t cooperating. I\'ll be in town until Sunday.<br><br>In reply to: <a href="https://x.com/pilhuhn/status/13377839226" rel="noopener noreferrer" target="_blank">@pilhuhn</a> <span class="status">13377839226</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13417915278',
    created: 1273054039000,
    type: 'post',
    text: 'I watched the Conan O\'Brien interview the other night; discovered he has a style of twitter humor that I appreciate and attempt to achieve.',
    likes: 0,
    retweets: 0
  },
  {
    id: '13417879142',
    created: 1273053970000,
    type: 'post',
    text: 'Made it to Mainz for #jax2010. Actually arrived early, but given that sleep on the plane was just not happening, gotta sneak in some zzzzs.',
    likes: 0,
    retweets: 0,
    tags: ['jax2010']
  },
  {
    id: '13386395594',
    created: 1273009946000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> Yeah, I was excited about the water inside the hotel. Now they are just showing off ;)<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/13385841973" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">13385841973</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13386326703',
    created: 1273009853000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Shows how much I know. Whatever the air laptop is called. He needs an iPowerstrip because he had to choose just one to charge.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/13382722082" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">13382722082</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13385376169',
    created: 1273008547000,
    type: 'post',
    text: 'The beautiful lobby of the hotel where my wife\'s summer conference is scheduled.',
    photos: ['<div class="entry"><img class="photo" src="media/13385376169.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '13382681779',
    created: 1273004846000,
    type: 'post',
    text: 'Mr. Mac is sitting next to me at the airport: iMac, iPod, iPhone and iPad. Apple, got anything else to sell this guy?',
    likes: 0,
    retweets: 0
  },
  {
    id: '13376376321',
    created: 1272995928000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/datafront" rel="noopener noreferrer" target="_blank">@datafront</a> Join my club. Just getting Eclipse to work from my preso caused me to pull out a few hairs. I still use it, but NetBeans too.<br><br>In reply to: <a href="https://x.com/datafront/status/13336621715" rel="noopener noreferrer" target="_blank">@datafront</a> <span class="status">13336621715</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13376338627',
    created: 1272995877000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kenglxn" rel="noopener noreferrer" target="_blank">@kenglxn</a> I\'ll definitely keep doing it. It squeezes a lot more value out of the effort of doing the talk. And it embodies openness.<br><br>In reply to: <a href="https://x.com/kenglxn/status/13346963750" rel="noopener noreferrer" target="_blank">@kenglxn</a> <span class="status">13346963750</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13376229513',
    created: 1272995723000,
    type: 'post',
    text: 'Sayonara my peeps. I\'m shipping out to Germany in T-1hr to speak at #jax.',
    likes: 0,
    retweets: 0,
    tags: ['jax']
  },
  {
    id: '13330792595',
    created: 1272927264000,
    type: 'post',
    text: 'You know you are writing too many e-mails when you start replying to your own message before anyone else does ;)',
    likes: 0,
    retweets: 1
  },
  {
    id: '13330658121',
    created: 1272927089000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> Sorry for flooding you guys with so many e-mails. What can I say, ideas are just spilling from my head :)',
    likes: 0,
    retweets: 0
  },
  {
    id: '13328690765',
    created: 1272924506000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/datafront" rel="noopener noreferrer" target="_blank">@datafront</a> Sounds like a good question for the #Arquillian community space ;)<br><br>In reply to: <a href="https://x.com/datafront/status/13327837469" rel="noopener noreferrer" target="_blank">@datafront</a> <span class="status">13327837469</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '13328621604',
    created: 1272924413000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> I will agree that the #Maven support in #NetBeans is superb.<br><br>In reply to: <a href="https://x.com/jasondlee/status/13328317950" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">13328317950</span>',
    likes: 0,
    retweets: 0,
    tags: ['maven', 'netbeans']
  },
  {
    id: '13328202356',
    created: 1272923869000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexismp" rel="noopener noreferrer" target="_blank">@alexismp</a> Not really. First, mvn has no concept of classpath isolation, which is 1/2 the problem. Our concern is isolated container booting.<br><br>In reply to: <a href="https://x.com/alexismp/status/13319075783" rel="noopener noreferrer" target="_blank">@alexismp</a> <span class="status">13319075783</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13322525123',
    created: 1272915917000,
    type: 'reply',
    text: '@brianleathem Hmm, they let you download the slides but not the audio. I\'ll make sure the audio is available as well.<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13322506358',
    created: 1272915891000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/waynebeaton" rel="noopener noreferrer" target="_blank">@waynebeaton</a> Amazing. I was just talking to someone about a similar idea the other day. Glad to see it will be a reality.<br><br>In reply to: <a href="https://x.com/waynebeaton/status/13320122498" rel="noopener noreferrer" target="_blank">@waynebeaton</a> <span class="status">13320122498</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13322452279',
    created: 1272915813000,
    type: 'post',
    text: 'Creating slidecasts for my talks has been a great exercise in analyzing my speaking technique. I can see where I get stuck / how to improve.',
    likes: 0,
    retweets: 0
  },
  {
    id: '13319654710',
    created: 1272911812000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> What\'s nice about having the slides is that it makes it easy to jump around in the presentation using slides as visual cue.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/13318864260" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">13318864260</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13319317146',
    created: 1272911335000,
    type: 'post',
    text: 'If a utility class has no state, use an enum to make it a singleton so there\'s only one created in the VM.',
    likes: 0,
    retweets: 0
  },
  {
    id: '13318583589',
    created: 1272910293000,
    type: 'post',
    text: 'Real Java EE Testing with #Arquillian and #ShrinkWrap made the slideshare homepage (hot on twitter) <a href="http://slidesha.re/ajiO28" rel="noopener noreferrer" target="_blank">slidesha.re/ajiO28</a>',
    likes: 2,
    retweets: 3,
    tags: ['arquillian', 'shrinkwrap']
  },
  {
    id: '13318228052',
    created: 1272909802000,
    type: 'post',
    text: 'Welcome <a class="mention" href="https://x.com/kenglxn" rel="noopener noreferrer" target="_blank">@kenglxn</a> as a committer to #ShrinkWrap and <a class="mention" href="https://x.com/sten_aksel" rel="noopener noreferrer" target="_blank">@sten_aksel</a> as a committer to #Arquillian.',
    likes: 0,
    retweets: 1,
    tags: ['shrinkwrap', 'arquillian']
  },
  {
    id: '13318146855',
    created: 1272909691000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/paulojeronimo" rel="noopener noreferrer" target="_blank">@paulojeronimo</a> When you write your article, we\'ll highlight it on the project site via <a href="http://diigo.com/user/jbosstesting/arquillian" rel="noopener noreferrer" target="_blank">diigo.com/user/jbosstesting/arquillian</a><br><br>In reply to: <a href="https://x.com/paulojeronimo/status/13298201060" rel="noopener noreferrer" target="_blank">@paulojeronimo</a> <span class="status">13298201060</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13317470893',
    created: 1272908767000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> :) I was referring to the origins of the #Arquillian project. Pete\'s CDI TCK infrastructure and ShrinkWrap joined forces.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/13317051621" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">13317051621</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '13317344972',
    created: 1272908593000,
    type: 'post',
    text: 'There\'s a bandgap in effort between unit testing and integration testing. That\'s something that our #testrevolution seeks to eliminate.',
    likes: 0,
    retweets: 0,
    tags: ['testrevolution']
  },
  {
    id: '13316929278',
    created: 1272908029000,
    type: 'post',
    text: '"#Arquillian is the love child of the #CDI (JSR-299) TCK and #ShrinkWrap"',
    likes: 0,
    retweets: 2,
    tags: ['arquillian', 'cdi', 'shrinkwrap']
  },
  {
    id: '13316740735',
    created: 1272907779000,
    type: 'post',
    text: 'Family Feud had a comeback in my #Arquillian talk. Nothin\' like a game show to warm up the room. <a href="http://slidesha.re/ajiO28" rel="noopener noreferrer" target="_blank">slidesha.re/ajiO28</a>',
    likes: 1,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '13316623414',
    created: 1272907619000,
    type: 'reply',
    text: '@brianleathem <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> The key with slideshare is that the audio is synced w/ the slide transitions. That\'s your visual.<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13316555852',
    created: 1272907527000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/waynebeaton" rel="noopener noreferrer" target="_blank">@waynebeaton</a> Cool! Only one problem I see. It doesn\'t show me how to install them. I\'d be nice if I had that view inside #Eclipse.<br><br>In reply to: <a href="https://x.com/waynebeaton/status/13284109313" rel="noopener noreferrer" target="_blank">@waynebeaton</a> <span class="status">13284109313</span>',
    likes: 0,
    retweets: 0,
    tags: ['eclipse']
  },
  {
    id: '13316428500',
    created: 1272907352000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexismp" rel="noopener noreferrer" target="_blank">@alexismp</a> In fact, we are working on a proposal to get multiple archive support in #Arquillian right now. So it\'s on the roadmap.<br><br>In reply to: <a href="https://x.com/alexismp/status/13301641372" rel="noopener noreferrer" target="_blank">@alexismp</a> <span class="status">13301641372</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '13290277166',
    created: 1272866361000,
    type: 'post',
    text: 'Check out my SlideShare presentation w/ audio on #CDI, #Weld and the future of #Seam: <a href="http://slidesha.re/adhgQo" rel="noopener noreferrer" target="_blank">slidesha.re/adhgQo</a>',
    likes: 8,
    retweets: 1,
    tags: ['cdi', 'weld', 'seam']
  },
  {
    id: '13290246932',
    created: 1272866305000,
    type: 'post',
    text: 'Check out my SlideShare presentation w/ audio on Real Java EE Testing with #Arquillian &amp; #ShrinkWrap: <a href="http://slidesha.re/ajiO28" rel="noopener noreferrer" target="_blank">slidesha.re/ajiO28</a>',
    likes: 4,
    retweets: 2,
    tags: ['arquillian', 'shrinkwrap']
  },
  {
    id: '13290211678',
    created: 1272866241000,
    type: 'post',
    text: 'I recorded my first 2 #nfjs talks using my phone. In deciding what to do with the audio, I discovered the slidecast feature of slideshare.',
    likes: 0,
    retweets: 0,
    tags: ['nfjs']
  },
  {
    id: '13290176297',
    created: 1272866179000,
    type: 'post',
    text: '#Arquillian really caused some jaws to hit the floor at #nfjs. I could sense the awe and excitement. No doubt, it\'s filling a gap.',
    likes: 0,
    retweets: 1,
    tags: ['arquillian', 'nfjs']
  },
  {
    id: '13277413484',
    created: 1272849237000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> I now recall the use case when mocks are useful. Easy to customize a delegate method return value to test how consumer reacts.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/12900096916" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">12900096916</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13277185425',
    created: 1272848964000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> It\'s supposed to be intfAnnotation, but I can\'t get it to work. Here\'s a full list: <a href="http://stackoverflow.com/questions/1205995/what-is-the-list-of-valid-suppresswarnings-warning-names-in-java" rel="noopener noreferrer" target="_blank">stackoverflow.com/questions/1205995/what-is-the-list-of-valid-suppresswarnings-warning-names-in-java</a><br><br>In reply to: <a href="https://x.com/ALRubinger/status/13148183136" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">13148183136</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13275979918',
    created: 1272847496000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/crichardson" rel="noopener noreferrer" target="_blank">@crichardson</a> Actually, it\'s quite busy wasting your CPU and time ;)<br><br>In reply to: <a href="https://x.com/crichardson/status/13265568406" rel="noopener noreferrer" target="_blank">@crichardson</a> <span class="status">13265568406</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13275927465',
    created: 1272847429000,
    type: 'post',
    text: 'There\'s only 1 word for Rory McIlroy\'s performance at Quail Hollow: crescendo. His grande finale putt on 18 was sick. Hats off! #pgatour',
    likes: 0,
    retweets: 0,
    tags: ['pgatour']
  },
  {
    id: '13251937133',
    created: 1272815093000,
    type: 'post',
    text: 'Last week it felt like fall. Today, I felt like I was walking back from the beach on a summer vacation. #erraticweather',
    likes: 0,
    retweets: 0,
    tags: ['erraticweather']
  },
  {
    id: '13210678094',
    created: 1272751505000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> Same experience here. I thoroughly enjoy speaking at #nfjs events. It always produces great conversation and suggestions.<br><br>In reply to: <a href="https://x.com/matthewmccull/status/13181595599" rel="noopener noreferrer" target="_blank">@matthewmccull</a> <span class="status">13181595599</span>',
    likes: 0,
    retweets: 0,
    tags: ['nfjs']
  },
  {
    id: '13158567674',
    created: 1272672956000,
    type: 'post',
    text: 'Three talks down. So is all my energy ;) Lots of love for #javaee',
    likes: 0,
    retweets: 0,
    tags: ['javaee']
  },
  {
    id: '13139882323',
    created: 1272647115000,
    type: 'post',
    text: 'About to begin my 3 talk marathon at #nfjs. First up, #cdi #weld #seam3',
    likes: 0,
    retweets: 0,
    tags: ['nfjs', 'cdi', 'weld', 'seam3']
  },
  {
    id: '13138394978',
    created: 1272645374000,
    type: 'post',
    text: 'Hey bro! Welcome to my faithful following :)',
    likes: 0,
    retweets: 1
  },
  {
    id: '13110826084',
    created: 1272602107000,
    type: 'post',
    text: 'Cool! If you copy the source of a Java class and paste it on package node in the explorer, #eclipse creates the class w/ correct package.',
    likes: 0,
    retweets: 0,
    tags: ['eclipse']
  },
  {
    id: '13102553470',
    created: 1272591538000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/citytech" rel="noopener noreferrer" target="_blank">@citytech</a> Yep, I will be at JUDCon for sure. See you there!<br><br>In reply to: <a href="https://x.com/OlsonDigital/status/13100202533" rel="noopener noreferrer" target="_blank">@OlsonDigital</a> <span class="status">13100202533</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13093305562',
    created: 1272580025000,
    type: 'post',
    text: 'Just uploaded the slides for my #NFJS presentations tomorrow: CDI/Weld/Seam, Arquillian, Seam+RESTEasy.',
    likes: 0,
    retweets: 0,
    tags: ['nfjs']
  },
  {
    id: '13082396703',
    created: 1272565063000,
    type: 'post',
    text: '#ShrinkWrap is a great project to start contributing too, if you are looking to get your feet wet. Very clean and focused.',
    likes: 0,
    retweets: 1,
    tags: ['shrinkwrap']
  },
  {
    id: '13080432275',
    created: 1272562390000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexis039" rel="noopener noreferrer" target="_blank">@alexis039</a> Give it up. Your missing the Y chromosome. That\'s the only thing that works.<br><br>In reply to: <a href="https://x.com/alexis039/status/13035018248" rel="noopener noreferrer" target="_blank">@alexis039</a> <span class="status">13035018248</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13080310052',
    created: 1272562225000,
    type: 'post',
    text: '+1 for creative commons &amp; OSS <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> I don\'t know why they discouraged copying from friends in HS. Seems it yields the best results.',
    likes: 0,
    retweets: 0
  },
  {
    id: '13078787647',
    created: 1272560176000,
    type: 'post',
    text: 'You know the feeling when you think your brain just dumped all short-term memory? Yeah, I think I just got that.',
    likes: 0,
    retweets: 0
  },
  {
    id: '13054465349',
    created: 1272522466000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kenglxn" rel="noopener noreferrer" target="_blank">@kenglxn</a> Data center power outage. Follow <a class="mention" href="https://x.com/jbossorg" rel="noopener noreferrer" target="_blank">@jbossorg</a>. They are really good about IT status updates.<br><br>In reply to: <a href="https://x.com/kenglxn/status/13053797115" rel="noopener noreferrer" target="_blank">@kenglxn</a> <span class="status">13053797115</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13034425796',
    created: 1272496889000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> Of course. I\'m just making a point. I know what he does. Newcomers only know him as the guy who trolls blogs that use the term JEE.<br><br>In reply to: @pelegri <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13030005898',
    created: 1272491077000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Yeah! Our design team has an official website. <a href="http://design.jboss.org" rel="noopener noreferrer" target="_blank">design.jboss.org</a>. Like a builder finally working on his own house :)<br><br>In reply to: <a href="https://x.com/ALRubinger/status/13029558909" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">13029558909</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13028728778',
    created: 1272489326000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/mojavelinux" rel="noopener noreferrer" target="_blank">@mojavelinux</a> I think we should focus on the important stuff, that people are actually excited about "JEE". We could stand to trend on dzone.',
    likes: 0,
    retweets: 0
  },
  {
    id: '13028634205',
    created: 1272489199000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> It wouldn\'t even phase me. And I certainly wouldn\'t go bossing people around publically on their own blogs.<br><br>In reply to: <a href="https://x.com/arungupta/status/13025813464" rel="noopener noreferrer" target="_blank">@arungupta</a> <span class="status">13025813464</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13025669678',
    created: 1272485386000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/mojavelinux" rel="noopener noreferrer" target="_blank">@mojavelinux</a> I\'m just joking of course, but these comments he adds are not helping to promote the platform. It just makes us look trite.',
    likes: 0,
    retweets: 0
  },
  {
    id: '13025480622',
    created: 1272485132000,
    type: 'post',
    text: 'Does Bill Shannon do anything besides troll blogs correcting people for using JEE instead of Java EE? I\'m calling it JEE from now on.',
    likes: 0,
    retweets: 0
  },
  {
    id: '13025181340',
    created: 1272484738000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> The extension described in that article is exactly what the Seam JMS module by <a class="mention" href="https://x.com/jganoff" rel="noopener noreferrer" target="_blank">@jganoff</a> is going to provide.<br><br>In reply to: <a href="https://x.com/jasondlee/status/13024515566" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">13024515566</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13019610681',
    created: 1272477298000,
    type: 'post',
    text: 'I\'m working on slides for my #Arquillian presentation this Friday @ #NFJS Reston, VA. It\'s going to be fun!',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'nfjs']
  },
  {
    id: '13019547487',
    created: 1272477215000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Yes. We didn\'t discuss specifics, but we felt it should be component library independent. Sounds like we need an e-mail.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/13013280146" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">13013280146</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13012680661',
    created: 1272468713000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kenglxn" rel="noopener noreferrer" target="_blank">@kenglxn</a> I\'m right there with you. I just can\'t go back. Until clients figure out the whole conversation thing.<br><br>In reply to: <a href="https://x.com/kenglxn/status/13002637220" rel="noopener noreferrer" target="_blank">@kenglxn</a> <span class="status">13002637220</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13012585297',
    created: 1272468606000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Bean Validation integration is going into Seam Faces instead, afaik.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/13007968991" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">13007968991</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '13011550669',
    created: 1272467399000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <a class="mention" href="https://x.com/jonobacon" rel="noopener noreferrer" target="_blank">@jonobacon</a> I can now say that I\'ve read the whole book. It\'s simply brilliant. I\'m going to read it again and do a review.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/13011279244" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">13011279244</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12961539207',
    created: 1272397536000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> I\'ve heard of it, haven\'t looked into it. But feel free to add it to <a href="http://community.jboss.org/wiki/Arquillianintegrationideas" rel="noopener noreferrer" target="_blank">community.jboss.org/wiki/Arquillianintegrationideas</a><br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12961509220',
    created: 1272397493000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> What\'s funny is that the idea of using it for porn never even crossed my mind. Just proves my innocence ;)<br><br>In reply to: <a href="https://x.com/ALRubinger/status/12959684644" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">12959684644</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12959630589',
    created: 1272394750000,
    type: 'post',
    text: 'I love the autocomplete in the Chrome location bar. I type a few characters and up pops the URL I need.',
    likes: 0,
    retweets: 0
  },
  {
    id: '12959580567',
    created: 1272394671000,
    type: 'post',
    text: 'Chrome\'s incognito feature is super useful for testing how an anonymous user sees your site without having to kill your own session.',
    likes: 0,
    retweets: 0
  },
  {
    id: '12950702762',
    created: 1272382234000,
    type: 'post',
    text: 'It really is simple! RT <a class="mention" href="https://x.com/jvanzyl" rel="noopener noreferrer" target="_blank">@jvanzyl</a> Uploading Artifacts to the Central Maven Repository: DIY --&gt; <a href="http://www.sonatype.com/people/2010/04/uploading-artifacts-to-the-central-maven-repository-diy/" rel="noopener noreferrer" target="_blank">www.sonatype.com/people/2010/04/uploading-artifacts-to-the-central-maven-repository-diy/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12930935800',
    created: 1272350085000,
    type: 'post',
    text: 'After many days of authoring and editing, the #Arquillian project site is staged and ready to go live. Stay tuned!',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '12927371364',
    created: 1272343859000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kenglxn" rel="noopener noreferrer" target="_blank">@kenglxn</a> Exactly! That\'s why I got so excited about Arquillian right away. Because I realized we can finally stop faking it!<br><br>In reply to: <a href="https://x.com/kenglxn/status/12926533495" rel="noopener noreferrer" target="_blank">@kenglxn</a> <span class="status">12926533495</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12866567016',
    created: 1272260003000,
    type: 'post',
    text: 'I just setup my new 1080p monitor. My screen now covers two zip codes ;) Samsung 2333HD. It\'s a beauty. Woot!',
    likes: 1,
    retweets: 0
  },
  {
    id: '12865841339',
    created: 1272258781000,
    type: 'post',
    text: 'There is nothing worse than typing a whole wiki page, then having to type it all again from scratch. Sucks for me :(',
    likes: 0,
    retweets: 0
  },
  {
    id: '12839644411',
    created: 1272225377000,
    type: 'post',
    text: 'Delta, Delta, when are you going to bring my wife home? 1.5 hr delay and the Sun is out. Well, it won\'t be by the time you arrive.',
    likes: 0,
    retweets: 0
  },
  {
    id: '12801059633',
    created: 1272167015000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/codylerum" rel="noopener noreferrer" target="_blank">@codylerum</a> Well I\'ll be damned. Hey, that\'s progress ;) It\'s Subversive though, which I\'ve heard doesn\'t stack up to Subclipse. wdyt?<br><br>In reply to: <a href="https://x.com/codylerum/status/12800568243" rel="noopener noreferrer" target="_blank">@codylerum</a> <span class="status">12800568243</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12800396537',
    created: 1272166202000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/codylerum" rel="noopener noreferrer" target="_blank">@codylerum</a> Eclipse 3.5 SR2 has SVN support baked in?<br><br>In reply to: <a href="https://x.com/codylerum/status/12800208050" rel="noopener noreferrer" target="_blank">@codylerum</a> <span class="status">12800208050</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12799915578',
    created: 1272165606000,
    type: 'post',
    text: 'I mean, shit, at least preconfigure the Eclipse update sites for what we\'re going to need anyway.',
    likes: 0,
    retweets: 0
  },
  {
    id: '12799629419',
    created: 1272165251000,
    type: 'post',
    text: 'It\'s really sad that the latest Eclipse IDE for Java EE only supports Ant for builds and CVS for source control. What year is it?',
    likes: 1,
    retweets: 1
  },
  {
    id: '12726259942',
    created: 1272059332000,
    type: 'post',
    text: 'Help us determine what sessions should make up the third "Community" Track at JUDCon 2010! <a href="https://community.jboss.org/poll.jspa?poll=1042" rel="noopener noreferrer" target="_blank">community.jboss.org/poll.jspa?poll=1042</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12677203294',
    created: 1271989481000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> You are on fire today! Must be the new picture :)',
    likes: 0,
    retweets: 0
  },
  {
    id: '12663031864',
    created: 1271972988000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> But now we are mixing our movie mythology.<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/12662867227" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">12662867227</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12662968970',
    created: 1271972901000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> Indeed. If we ever decide to make a movie about Arquillian, he\'d be our hero ;)<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/12662867227" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">12662867227</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12662748960',
    created: 1271972603000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> I like the new picture ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '12648139031',
    created: 1271953510000,
    type: 'post',
    text: 'What\'s the fastest way to the Java EE 6 JavaDoc? <a href="http://javaserverfaces.org" rel="noopener noreferrer" target="_blank">javaserverfaces.org</a> and click on Java EE 6 API Documentation on the right.',
    likes: 2,
    retweets: 0
  },
  {
    id: '12576265568',
    created: 1271856624000,
    type: 'post',
    text: 'Why do hotels provide travel-size shampoo, conditioner, body wash, body lotion, &amp; mouth wash, but no toothpaste? Useful 3oz item to have.',
    likes: 0,
    retweets: 0
  },
  {
    id: '12555481759',
    created: 1271820730000,
    type: 'post',
    text: 'Curious about the future of #Java? Is it doomed? Check out this roundtable discussion at #developerworks <a href="http://www.ibm.com/developerworks/java/library/j-javaroundtable/index.html" rel="noopener noreferrer" target="_blank">www.ibm.com/developerworks/java/library/j-javaroundtable/index.html</a>',
    likes: 3,
    retweets: 5,
    tags: ['java', 'developerworks']
  },
  {
    id: '12555368060',
    created: 1271820593000,
    type: 'post',
    text: 'It was great to meet with Steven Boscarine again too. Seeing him reminded me we need to get back to those archetypes &amp; I owe him code. Doh!',
    likes: 0,
    retweets: 0
  },
  {
    id: '12554949172',
    created: 1271820076000,
    type: 'post',
    text: 'It took me a few of minutes to realize <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> was <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> when I met him. He\'s such a rock star in person :) (Hear that ladies?)',
    likes: 0,
    retweets: 0
  },
  {
    id: '12554612273',
    created: 1271819666000,
    type: 'post',
    text: 'Finally got to meet colleague and luminary <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> tonight at CTJUG. Thanks for making the trip, bringing Steven and skipping Lost! Wow!',
    likes: 0,
    retweets: 0
  },
  {
    id: '12554171584',
    created: 1271819133000,
    type: 'post',
    text: 'Thanks to <a class="mention" href="https://x.com/ctjava" rel="noopener noreferrer" target="_blank">@ctjava</a> for pulling together a very attentive crowd for my presentation at the CTJUG meeting on #cdi and #jsf2!',
    likes: 0,
    retweets: 0,
    tags: ['cdi', 'jsf2']
  },
  {
    id: '12486387610',
    created: 1271724624000,
    type: 'post',
    text: 'JBoss provides full disclosure about JIRA security incident, why SVN services got affected <a href="http://www.jboss.org/announcements/jira_security_incident_190410.html" rel="noopener noreferrer" target="_blank">www.jboss.org/announcements/jira_security_incident_190410.html</a>',
    likes: 0,
    retweets: 3
  },
  {
    id: '12486186365',
    created: 1271724394000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> To clarify, it\'s not a rule for central, it\'s to use the free OSS hosting that Sonatype provides. OSS == source available ;)<br><br>In reply to: @pelegri <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12462719908',
    created: 1271692626000,
    type: 'post',
    text: 'oss.sonatype.org now requiring -sources.jar and -javadoc.jar artifacts in order to promote to central. #maven #nexus #rules',
    likes: 0,
    retweets: 0,
    tags: ['maven', 'nexus', 'rules']
  },
  {
    id: '12461973511',
    created: 1271691673000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> The JBoss SVN outage is a side effect of security precautions after the JIRA security breach. #thatswhatiheard<br><br>In reply to: <a href="https://x.com/JohnAment/status/12457485605" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">12457485605</span>',
    likes: 0,
    retweets: 0,
    tags: ['thatswhatiheard']
  },
  {
    id: '12461423918',
    created: 1271690984000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> The market says Java wins, the panel says it\'s anything but. Java EE still has a lot of work to do to clear out the bad air.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/12439595214" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">12439595214</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12461278359',
    created: 1271690799000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kenglxn" rel="noopener noreferrer" target="_blank">@kenglxn</a> That\'s the goal, but we have some "roadblocks" to get past first. IMO, it\'s sorely needed because Weld project info is scattered.<br><br>In reply to: <a href="https://x.com/kenglxn/status/12445191720" rel="noopener noreferrer" target="_blank">@kenglxn</a> <span class="status">12445191720</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12439529237',
    created: 1271653750000,
    type: 'post',
    text: 'The #Arquillian artwork has been finalized! <a href="http://community.jboss.org/community/arquillian/blog/2010/04/19/the-arquillian-artwork-has-landed" rel="noopener noreferrer" target="_blank">community.jboss.org/community/arquillian/blog/2010/04/19/the-arquillian-artwork-has-landed</a> Thanks for helping making it great!',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '12437879008',
    created: 1271651053000,
    type: 'post',
    text: 'Andiamo is an effort to improve the out-of-the-box experience of JBoss AS <a href="https://jira.jboss.org/jira/browse/ANDIAMO" rel="noopener noreferrer" target="_blank">jira.jboss.org/jira/browse/ANDIAMO</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12437478547',
    created: 1271650449000,
    type: 'post',
    text: 'JBossBot - versatile IRC bot for JBoss related channels <a href="http://community.jboss.org/message/532642" rel="noopener noreferrer" target="_blank">community.jboss.org/message/532642</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12433611336',
    created: 1271645368000,
    type: 'post',
    text: 'Listen to the audio from the Battle of the Frameworks II @ #ete2010 <a href="http://chariottechcast.libsyn.com/index.php?post_id=604618" rel="noopener noreferrer" target="_blank">chariottechcast.libsyn.com/index.php?post_id=604618</a>',
    likes: 1,
    retweets: 0,
    tags: ['ete2010']
  },
  {
    id: '12433535489',
    created: 1271645278000,
    type: 'post',
    text: 'Pics of the framework advocates battling it out at #ete2010 <a href="http://picasaweb.google.com/dan.j.allen/BattleOfTheFrameworksIIEmergingTechConference2010" rel="noopener noreferrer" target="_blank">picasaweb.google.com/dan.j.allen/BattleOfTheFrameworksIIEmergingTechConference2010</a>',
    likes: 0,
    retweets: 0,
    tags: ['ete2010']
  },
  {
    id: '12430043741',
    created: 1271641182000,
    type: 'post',
    text: 'Great shot of <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> speaking at ETE 2010 about his project, PrettyFaces.',
    photos: ['<div class="entry"><img class="photo" src="media/12430043741.jpg"></div>'],
    likes: 1,
    retweets: 0
  },
  {
    id: '12424213233',
    created: 1271633729000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/jbossorg" rel="noopener noreferrer" target="_blank">@jbossorg</a> when is SVN going to be back up?',
    likes: 0,
    retweets: 0
  },
  {
    id: '12413534427',
    created: 1271618840000,
    type: 'post',
    text: 'Just setup a diigo account for #Arquillian to permanently track community articles and blogs: <a href="http://www.diigo.com/user/arquillian" rel="noopener noreferrer" target="_blank">www.diigo.com/user/arquillian</a>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '12326363993',
    created: 1271481684000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iamroberthanson" rel="noopener noreferrer" target="_blank">@iamroberthanson</a> I totally agree. I much prefer a response which cites historical reasons why a person as selected A, B or Z.<br><br>In reply to: <a href="https://x.com/iamroberthanson/status/12325683244" rel="noopener noreferrer" target="_blank">@iamroberthanson</a> <span class="status">12325683244</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12325601079',
    created: 1271480449000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/datafront" rel="noopener noreferrer" target="_blank">@datafront</a> Yep, I passed ICC sites about a dozen times on my way over to Rio tonight from Laurel. Story of our lives.<br><br>In reply to: <a href="https://x.com/datafront/status/12313832565" rel="noopener noreferrer" target="_blank">@datafront</a> <span class="status">12313832565</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12325525161',
    created: 1271480338000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/datafront" rel="noopener noreferrer" target="_blank">@datafront</a> We owe the community an update on the archetypes. I tried to get to it this week, but alas, it will be next week.<br><br>In reply to: <a href="https://x.com/datafront/status/11785503900" rel="noopener noreferrer" target="_blank">@datafront</a> <span class="status">11785503900</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12307800359',
    created: 1271456714000,
    type: 'post',
    text: 'The forming of the ICC, a connector road in MD decades in the making.',
    photos: ['<div class="entry"><img class="photo" src="media/12307800359.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '12303508560',
    created: 1271450578000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> No, it will be the same HTTP service you know and love ;) Before Nexus, we were managing with SVN...horrible.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/12299684532" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">12299684532</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12303489332',
    created: 1271450550000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/michaelschuetz" rel="noopener noreferrer" target="_blank">@michaelschuetz</a> Personally, it doesn\'t matter to me. JBoss choose Nexus so that they could consult w/ the Maven team at the same time.<br><br>In reply to: <a href="https://x.com/michaelschuetz/status/12303134805" rel="noopener noreferrer" target="_blank">@michaelschuetz</a> <span class="status">12303134805</span>',
    likes: 0,
    retweets: 1
  },
  {
    id: '12299956641',
    created: 1271445522000,
    type: 'post',
    text: '"Don\'t be afraid to look ignorant, people will figure that part out themselves<br>anyway." - colleague',
    likes: 0,
    retweets: 1
  },
  {
    id: '12296578931',
    created: 1271440932000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> The repository itself stays the same. Nexus is just how stuff gets into it. It enables much better control over permissions.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/12294074986" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">12294074986</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12261998410',
    created: 1271388444000,
    type: 'post',
    text: 'Congratulations to <a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> for being named the #arquillian project lead: <a href="http://community.jboss.org/message/537799#537799" rel="noopener noreferrer" target="_blank">community.jboss.org/message/537799#537799</a>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '12243526326',
    created: 1271364622000,
    type: 'post',
    text: 'NFJS magazine now in ePub! Sweet!',
    likes: 0,
    retweets: 0
  },
  {
    id: '12242726413',
    created: 1271363524000,
    type: 'post',
    text: 'I\'m meeting up with my tech buddies at DogFish in Gaithersburg, MD tonight.',
    likes: 0,
    retweets: 0
  },
  {
    id: '12240755646',
    created: 1271360863000,
    type: 'post',
    text: 'Damn it\'s been a busy day.',
    likes: 0,
    retweets: 0
  },
  {
    id: '12200654206',
    created: 1271300174000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> You didn\'t say you brought it up. Cool! Good job. Sure would be nice if the mailinglist was open for all to see.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/12198522015" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">12198522015</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12200478623',
    created: 1271299957000,
    type: 'post',
    text: 'My two cousins (brother and sister) just completed a marathon in Paris. Now they are going to crawl to Belgium and go have some chocolate.',
    likes: 0,
    retweets: 0
  },
  {
    id: '12198470886',
    created: 1271297540000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I can think of two great books on the subject. Have they read either of them?<br><br>In reply to: <a href="https://x.com/lightguardjp/status/12195587637" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">12195587637</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12198427129',
    created: 1271297487000,
    type: 'post',
    text: 'A little birdy told me that #Seam Faces Alpha1 is going to be released tomorrow. Now a little birdy told you ;)',
    likes: 0,
    retweets: 4,
    tags: ['seam']
  },
  {
    id: '12192584737',
    created: 1271290529000,
    type: 'post',
    text: 'The alien is out of the ship! The #Arquillian logo is final! Blog entry to come: <a href="http://twurl.nl/cvufbc" rel="noopener noreferrer" target="_blank">twurl.nl/cvufbc</a> <a href="http://twurl.nl/abwkw0" rel="noopener noreferrer" target="_blank">twurl.nl/abwkw0</a>',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '12188093280',
    created: 1271284777000,
    type: 'post',
    text: 'What\'s your project\'s Vietnam? For #Eclipse, it\'s the split file editor. For #android, it\'s editing text in gmail. For #Java, it\'s logging.',
    likes: 0,
    retweets: 0,
    tags: ['eclipse', 'android', 'java']
  },
  {
    id: '12187040860',
    created: 1271283341000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> I asked <a class="mention" href="https://x.com/jvanzyl" rel="noopener noreferrer" target="_blank">@jvanzyl</a> about adding a test-provided scope for deps needed to compile tests, but provided by test infrastructure.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/12182568799" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">12182568799</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12174941205',
    created: 1271266361000,
    type: 'post',
    text: 'Snapshots for #Weld docs are now available <a href="https://docs.jboss.org/weld/reference/snapshot/" rel="noopener noreferrer" target="_blank">docs.jboss.org/weld/reference/snapshot/</a>',
    likes: 0,
    retweets: 2,
    tags: ['weld']
  },
  {
    id: '12146355125',
    created: 1271221065000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> I freakin\' love that song (by Shivaree). So rich.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/12145934935" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">12145934935</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12145520195',
    created: 1271219722000,
    type: 'post',
    text: '"Nobody beat anyone up. Everybody was friendly." in #seam-dev. That\'s the way we roll. ;)',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '12145459323',
    created: 1271219629000,
    type: 'post',
    text: '"I think this will motivate some OSS ;)" - spoken by <a class="mention" href="https://x.com/mojavelinux" rel="noopener noreferrer" target="_blank">@mojavelinux</a> in #seam-dev. Woot!',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '12145416675',
    created: 1271219564000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/mojavelinux" rel="noopener noreferrer" target="_blank">@mojavelinux</a> brings happiness to the world &amp; writes longest lines :) Nobody is foul-mouthed in #seam-dev <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> had some AMAZING tea',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '12145310090',
    created: 1271219408000,
    type: 'post',
    text: 'The IRC summary on <a href="http://echelog.matzon.dk" rel="noopener noreferrer" target="_blank">echelog.matzon.dk</a> is hilarious! Need all 140 characters to summarize seam-dev today...',
    likes: 0,
    retweets: 0
  },
  {
    id: '12105768878',
    created: 1271168068000,
    type: 'post',
    text: 'It\'s bad when your note to self ends up being a note to someone else. Thankfully, it was my work e-mail :)',
    likes: 0,
    retweets: 0
  },
  {
    id: '12025772980',
    created: 1271041700000,
    type: 'post',
    text: 'A wiki page is worth a 1,000 explanations.',
    likes: 0,
    retweets: 0
  },
  {
    id: '12025663231',
    created: 1271041557000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> I\'m all about being passionate. But being passionate doesn\'t mean acting like a child. Everyone else is capable of behaving.<br><br>In reply to: <a href="https://x.com/jeremynorris/status/12019613436" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">12019613436</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12025599016',
    created: 1271041477000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> Of course we want to win. But saying after 5 months off that not winning is unacceptable is just being arrogant, IMO.<br><br>In reply to: <a href="https://x.com/jeremynorris/status/12019613436" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">12019613436</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12025520566',
    created: 1271041380000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I\'m sure Google will crawl the site. Ask here about stats: <a href="http://community.jboss.org/en/website" rel="noopener noreferrer" target="_blank">community.jboss.org/en/website</a><br><br>In reply to: <a href="https://x.com/lightguardjp/status/12022472911" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">12022472911</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12019164542',
    created: 1271033313000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> I\'m not really concerned w/ his personal life. Swearing on the course &amp; statement that "not winning == sucks" is my objection.<br><br>In reply to: <a href="https://x.com/jeremynorris/status/12018610336" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">12018610336</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12016511869',
    created: 1271029751000,
    type: 'post',
    text: 'And, a huge congratulations to Phil Mickelson for his win at the Masters. I\'m finally able to see that it\'s Phil that is the class act.',
    likes: 0,
    retweets: 0
  },
  {
    id: '12016453199',
    created: 1271029669000,
    type: 'post',
    text: 'I\'m disappointed w/ Tiger\'s behavior @ the Masters, esp his answers to questions after final round. It is a big deal, Tiger.',
    likes: 0,
    retweets: 0
  },
  {
    id: '12016305987',
    created: 1271029465000,
    type: 'post',
    text: 'We learn from the recent story of Tiger Woods to never assume that we are immune to people\'s judgement.',
    likes: 0,
    retweets: 0
  },
  {
    id: '12006309364',
    created: 1271014666000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jonobacon" rel="noopener noreferrer" target="_blank">@jonobacon</a> I pretty much feel exactly the same way. Throw me a rope!<br><br>In reply to: <a href="https://x.com/jonobacon/status/12006109982" rel="noopener noreferrer" target="_blank">@jonobacon</a> <span class="status">12006109982</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '12006294987',
    created: 1271014643000,
    type: 'post',
    text: 'If you have a profile on JBoss Community, you can now have a personal blog! <a href="http://community.jboss.org" rel="noopener noreferrer" target="_blank">community.jboss.org</a>. Post those tips!',
    likes: 0,
    retweets: 0
  },
  {
    id: '12004922019',
    created: 1271012580000,
    type: 'post',
    text: 'In case you were wondering what beans.xml is and why you need it: <a href="http://sfwk.org/Documentation/WhatIsBeansxmlAndWhyDoINeedIt" rel="noopener noreferrer" target="_blank">sfwk.org/Documentation/WhatIsBeansxmlAndWhyDoINeedIt</a> #cdi',
    likes: 3,
    retweets: 0,
    tags: ['cdi']
  },
  {
    id: '12002421160',
    created: 1271008910000,
    type: 'post',
    text: 'Facelets has a similar story to annotations. While they have been around for a while, they continue to evolve the Java EE programming model.',
    likes: 0,
    retweets: 1
  },
  {
    id: '11977640072',
    created: 1270965693000,
    type: 'post',
    text: 'Check out Jaime (<a class="mention" href="https://x.com/messmaster" rel="noopener noreferrer" target="_blank">@messmaster</a>) of <a href="http://www.garageenvy.com" rel="noopener noreferrer" target="_blank">www.garageenvy.com</a> w/ Leno and Bill Goldberg',
    photos: ['<div class="entry"><img class="photo" src="media/11977640072.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '11977459901',
    created: 1270965388000,
    type: 'post',
    text: 'It\'s cool, but surreal, to see friends on TV. Just watched Jaime Dietenhofer, my wife\'s former classmate, on Garage Mahal: Jay Leno episode',
    likes: 0,
    retweets: 0
  },
  {
    id: '11952851797',
    created: 1270928610000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/prabhatjha" rel="noopener noreferrer" target="_blank">@prabhatjha</a> Oops. Yep, I fat thumbed that tweet ;)<br><br>In reply to: <a href="https://x.com/prabhatjha/status/11951968642" rel="noopener noreferrer" target="_blank">@prabhatjha</a> <span class="status">11951968642</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11940387803',
    created: 1270911362000,
    type: 'post',
    text: 'So that\'s what those things on my desk are called. I always wondered what they were.',
    photos: ['<div class="entry"><img class="photo" src="media/11940387803.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '11919806918',
    created: 1270872551000,
    type: 'post',
    text: 'Dined w/ wife &amp; friends under watch of Budda at Buddakan, a hip Asian fusion restaurant in Society Hill.',
    photos: ['<div class="entry"><img class="photo" src="media/11919806918.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '11919250890',
    created: 1270871779000,
    type: 'reply',
    text: 'Great friendship -&gt; RT <a class="mention" href="https://x.com/jeremyg484" rel="noopener noreferrer" target="_blank">@jeremyg484</a>: #phillyete was excellent. Great sessions, great crowd, great beer...what more could you want?<br><br>In reply to: <a href="https://x.com/jeremyg484/status/11915017394" rel="noopener noreferrer" target="_blank">@jeremyg484</a> <span class="status">11915017394</span>',
    likes: 0,
    retweets: 0,
    tags: ['phillyete']
  },
  {
    id: '11919138771',
    created: 1270871620000,
    type: 'post',
    text: 'Dang it\'s cold. A 45 degree swing since yesterday afternoon. I so didn\'t pack for this!',
    likes: 0,
    retweets: 0
  },
  {
    id: '11908366957',
    created: 1270857281000,
    type: 'post',
    text: 'is worn out after a long week, capped of by two talks and plenty of debate. I gave it all I had. A+ to #phillyete organizers &amp; sponsors.',
    likes: 0,
    retweets: 0,
    tags: ['phillyete']
  },
  {
    id: '11908248786',
    created: 1270857112000,
    type: 'post',
    text: 'One thing is very clear to me after getting a chance to mingle w/ #phillyete attendees. Java EE 6 needs clearer "get started" docs.',
    likes: 0,
    retweets: 0,
    tags: ['phillyete']
  },
  {
    id: '11907794437',
    created: 1270856466000,
    type: 'post',
    text: 'The whole time I was in <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a>\'s #prettyfaces talk I was thinking "Man, you just gotta have this stuff!" It\'s like candy.',
    likes: 0,
    retweets: 0,
    tags: ['prettyfaces']
  },
  {
    id: '11848382225',
    created: 1270767798000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a>, the champion of pretty Java EE, featured here in the #phillyete #android app.',
    photos: ['<div class="entry"><img class="photo" src="media/11848382225.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['phillyete', 'android']
  },
  {
    id: '11842543383',
    created: 1270759557000,
    type: 'post',
    text: 'Kicked off the day with two long, back-to-back meetings. Finally got to tune into the conference after that.',
    likes: 0,
    retweets: 0
  },
  {
    id: '11842033146',
    created: 1270758801000,
    type: 'post',
    text: 'Jason Van Zyl pronounces Java with a strong Boston accent. Strange is that I don\'t detect the accent in any other word. Funny :)',
    likes: 0,
    retweets: 0
  },
  {
    id: '11805211444',
    created: 1270699195000,
    type: 'post',
    text: 'put on a few lbs of dairy at Franklin\'s Fountain, an old time ice cream parlor on Market St.',
    photos: ['<div class="entry"><img class="photo" src="media/11805211444.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '11805016676',
    created: 1270698915000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/ChariotSolution" rel="noopener noreferrer" target="_blank">@ChariotSolution</a> put on a great speaker event 2night. Glad to see #phillyete grow in a few short years. Looking forward to main event.',
    likes: 0,
    retweets: 0,
    tags: ['phillyete']
  },
  {
    id: '11804772189',
    created: 1270698574000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> Doh! How did I miss that? Very well, now I can go show it off ;)<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/11782610760" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">11782610760</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11804724450',
    created: 1270698507000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> I finally got on the IRC bandwagon recently, but it just so happened that I\'m on the road now at the #phillyete conference.<br><br>In reply to: <a href="https://x.com/jasondlee/status/11765742266" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">11765742266</span>',
    likes: 0,
    retweets: 0,
    tags: ['phillyete']
  },
  {
    id: '11781544945',
    created: 1270667354000,
    type: 'post',
    text: 'Passing thought while hitting the road. JavaArchive#addServiceProviderImplClass(SPI class, Impl class...) for #shrinkwrap?',
    likes: 0,
    retweets: 0,
    tags: ['shrinkwrap']
  },
  {
    id: '11781303105',
    created: 1270667023000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RockBandParts" rel="noopener noreferrer" target="_blank">@RockBandParts</a> Great! Thanks. Definitely worth a shot, esp for $20.<br><br>In reply to: <a href="https://x.com/RockBandParts/status/11742959101" rel="noopener noreferrer" target="_blank">@RockBandParts</a> <span class="status">11742959101</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11781281778',
    created: 1270666994000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> It\'s more like I lost steam. I\'ll be returning to that thread soon, with some refreshing news! Stay tuned!<br><br>In reply to: <a href="https://x.com/lightguardjp/status/11757910133" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">11757910133</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11781260431',
    created: 1270666965000,
    type: 'post',
    text: 'is speaking about JSR-299, Weld, Seam &amp; RESTEasy @ #phillyete. <a href="http://phillyemergingtech.com/speakers/dan-allen" rel="noopener noreferrer" target="_blank">phillyemergingtech.com/speakers/dan-allen</a> <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> will be speaking too.',
    likes: 0,
    retweets: 0,
    tags: ['phillyete']
  },
  {
    id: '11781178184',
    created: 1270666853000,
    type: 'post',
    text: 'Finally heading out the door to #phillyete. Hopefully, I will make it to the speaker dinner in time.',
    likes: 0,
    retweets: 0,
    tags: ['phillyete']
  },
  {
    id: '11765315329',
    created: 1270658718000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Actually, in the publishing step to java.net, Ant calls out to Maven. So you need to tweak those lines for the release plugin.<br><br>In reply to: <a href="https://x.com/jasondlee/status/11758265182" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">11758265182</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11765256367',
    created: 1270658636000,
    type: 'post',
    text: 'The boss is back in town. Thank goodness. I was just about losing my mind trying to juggle everything.',
    likes: 0,
    retweets: 0
  },
  {
    id: '11742169882',
    created: 1270619780000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/RockBandParts" rel="noopener noreferrer" target="_blank">@RockBandParts</a> Is XCell pad installation easy? I\'ve replaced on piezoelectric sensor in a head, so I am no noob to hacking the drum unit.',
    likes: 0,
    retweets: 0
  },
  {
    id: '11742109191',
    created: 1270619662000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/RockBandParts" rel="noopener noreferrer" target="_blank">@RockBandParts</a> The pads on my RB2 drums are bubbling up and slipping out on every song. Will the XCell pads tighten it all up again?',
    likes: 0,
    retweets: 0
  },
  {
    id: '11737867429',
    created: 1270612760000,
    type: 'post',
    text: 'A steadily expanding handbook for #Seam3 module leads <a href="http://sfwk.org/Seam3/ModuleHandbook" rel="noopener noreferrer" target="_blank">sfwk.org/Seam3/ModuleHandbook</a>',
    likes: 0,
    retweets: 1,
    tags: ['seam3']
  },
  {
    id: '11737823958',
    created: 1270612700000,
    type: 'post',
    text: 'Gory details on how to release a #Seam3 module. <a href="http://sfwk.org/Seam3/ModuleReleaseProcedure" rel="noopener noreferrer" target="_blank">sfwk.org/Seam3/ModuleReleaseProcedure</a> Only lab tested at this point.',
    likes: 0,
    retweets: 1,
    tags: ['seam3']
  },
  {
    id: '11737763863',
    created: 1270612619000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Mojarra already has a project on Nexus OSS. I\'ll forward you the info.<br><br>In reply to: <a href="https://x.com/jasondlee/status/11735482436" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">11735482436</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11734303684',
    created: 1270608211000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> It\'s really quite easy. In about 30 minutes I will have a really detailed guide to how we release Seam 3 modules (for reference).<br><br>In reply to: <a href="https://x.com/jasondlee/status/11733091737" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">11733091737</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11732370944',
    created: 1270605822000,
    type: 'post',
    text: 'Mojarra team, we\'re going to have to start linking Seam 3 faces and examples against MyFaces because you aren\'t in central. Sorry, policy.',
    likes: 1,
    retweets: 0
  },
  {
    id: '11727202783',
    created: 1270599451000,
    type: 'post',
    text: 'Just thought of something. When citing an example list, rather than saying ABC you could say ABZ. That\'s like sort of like alpha of 1 ... n',
    likes: 0,
    retweets: 0
  },
  {
    id: '11717868490',
    created: 1270586895000,
    type: 'post',
    text: 'Is the Drools team on JSR-331? Constraint Programming API?',
    likes: 0,
    retweets: 0
  },
  {
    id: '11714015215',
    created: 1270581566000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> I had a former coworker who never talked about anything but that tool over the water cooler. It was his fav.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11707032698',
    created: 1270571999000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Amen! +1 for transparency.<br><br>In reply to: <a href="https://x.com/maxandersen/status/11706258607" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">11706258607</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11706219885',
    created: 1270570956000,
    type: 'post',
    text: 'Glad it helped! RT <a class="mention" href="https://x.com/cbredesen" rel="noopener noreferrer" target="_blank">@cbredesen</a> <a class="mention" href="https://x.com/mojavelinux" rel="noopener noreferrer" target="_blank">@mojavelinux</a> saves the day yet again with his excellent JSF article series. <a href="http://java.dzone.com/articles/fluent-navigation-jsf-2?mz=3006-jboss" rel="noopener noreferrer" target="_blank">java.dzone.com/articles/fluent-navigation-jsf-2?mz=3006-jboss</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '11706162649',
    created: 1270570882000,
    type: 'post',
    text: 'We are so close to choosing #arquillian logo. Been going back &amp; forth with tweaks until all project devs were happy. Unveiling soon.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '11705910558',
    created: 1270570559000,
    type: 'post',
    text: 'I\'ll be speaking about #cdi, #jsf2 and #seam3 at Connecticut JUG on Apr. 20 at 5:15. Announcement should be here soon <a href="http://ctjava.org" rel="noopener noreferrer" target="_blank">ctjava.org</a>',
    likes: 0,
    retweets: 1,
    tags: ['cdi', 'jsf2', 'seam3']
  },
  {
    id: '11700106771',
    created: 1270563098000,
    type: 'post',
    text: '"We accept component models in Java EE; why aren\'t we following the same practices when it comes to testing?" - <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> #arquillian',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '11683786384',
    created: 1270532503000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Here\'s to admitting I\'m a fool. Actually, I think I just like to say things in my own creative way :) Call it improv.<br><br>In reply to: <a href="https://x.com/jasondlee/status/11676814435" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">11676814435</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11683733625',
    created: 1270532405000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Boom! Amazing.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/11682683482" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">11682683482</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11675453452',
    created: 1270520929000,
    type: 'post',
    text: 'Hudson lead leaves Oracle <a href="https://www.jenkins.io/blog/2010/04/07/kohsuke-leaves-sun/" rel="noopener noreferrer" target="_blank">www.jenkins.io/blog/2010/04/07/kohsuke-leaves-sun/</a>, but will start a company based around Hudson. So good news after all!',
    likes: 0,
    retweets: 0
  },
  {
    id: '11675373574',
    created: 1270520830000,
    type: 'post',
    text: 'ShrinkWrap is nice. Want to print out the contents of an archive? Archive#toString(Formatters.VERBOSE)',
    likes: 0,
    retweets: 0
  },
  {
    id: '11675271611',
    created: 1270520704000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Yes, I downloaded the whole movie to listen to that 5 second clip to find out ;) Ar-kil-e-an (but say it quickly)<br><br>In reply to: <a href="https://x.com/ALRubinger/status/11659983863" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">11659983863</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11675211054',
    created: 1270520630000,
    type: 'post',
    text: 'We also learned that <a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a>\'s first name, Aslak, is pronounced Osh (as in Oshgosh the clothing brand) + lock.',
    likes: 0,
    retweets: 0
  },
  {
    id: '11675052137',
    created: 1270520434000,
    type: 'post',
    text: 'We worked out today the correct pronunciation of #Arquillian. It\'s Ar-KILL-e-an, just how <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> has been pronouncing it all along.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '11604820693',
    created: 1270413521000,
    type: 'post',
    text: 'Just committed an #OpenWebBeans embedded container for #Arquillian: <a href="https://jira.jboss.org/jira/browse/ARQ-96" rel="noopener noreferrer" target="_blank">jira.jboss.org/jira/browse/ARQ-96</a>',
    likes: 0,
    retweets: 2,
    tags: ['openwebbeans', 'arquillian']
  },
  {
    id: '11516540425',
    created: 1270266420000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a>: a brilliant software engineer w/ incredible desire to build things the right way, everytime &amp; w/o compromise (via #linkedin)',
    likes: 0,
    retweets: 1,
    tags: ['linkedin']
  },
  {
    id: '11466406794',
    created: 1270187167000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Now you need to buy the poster a drink ;) And the open source heart comes full circle.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/11463933841" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">11463933841</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11408077464',
    created: 1270098007000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/michaelg" rel="noopener noreferrer" target="_blank">@michaelg</a> Yeah, but it at least needs to format the code samples because everything is just crunched into one long line right now.<br><br>In reply to: <a href="https://x.com/michaelg/status/11407969983" rel="noopener noreferrer" target="_blank">@michaelg</a> <span class="status">11407969983</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11407828443',
    created: 1270097624000,
    type: 'post',
    text: 'Woh! What did <a href="http://theserverside.com" rel="noopener noreferrer" target="_blank">theserverside.com</a> do with their site?? I can\'t read it like this!',
    likes: 0,
    retweets: 0
  },
  {
    id: '11391540276',
    created: 1270076287000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/SparrowSmith" rel="noopener noreferrer" target="_blank">@SparrowSmith</a> Finally!!!',
    likes: 0,
    retweets: 0
  },
  {
    id: '11390631612',
    created: 1270075008000,
    type: 'post',
    text: 'More like our Vietnam "Juergen Hoeller said recently Build and Logging are Java\'s Iraq. You think we\'d have these areas sorted out by 2010."',
    likes: 0,
    retweets: 0
  },
  {
    id: '11387372739',
    created: 1270070206000,
    type: 'post',
    text: 'In college, you\'d pre-party to get buzzed for the main event. Get your buzz for JBossWorld @ the #JUDCon pre-conference <a href="http://www.jboss.org/events/JUDCon.html" rel="noopener noreferrer" target="_blank">www.jboss.org/events/JUDCon.html</a>',
    likes: 0,
    retweets: 0,
    tags: ['judcon']
  },
  {
    id: '11378297644',
    created: 1270057418000,
    type: 'post',
    text: 'I drive my wife nuts when we need to eat because I\'m like "Wait! Let\'s check the reviews...nope, 2 stars, pick another place."',
    likes: 0,
    retweets: 0
  },
  {
    id: '11373003347',
    created: 1270050595000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> What\'s funny is that the 1st person I heard this from was my wife. She says at dinner "You know, Solaris isn\'t free anymore."<br><br>In reply to: <a href="https://x.com/lincolnthree/status/11371839396" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">11371839396</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11370819830',
    created: 1270047965000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cwash" rel="noopener noreferrer" target="_blank">@cwash</a> Hehehe. The ironic part is that I\'m not using it yet. I\'m gettin\' there ;)<br><br>In reply to: <a href="https://x.com/cwash/status/11370672453" rel="noopener noreferrer" target="_blank">@cwash</a> <span class="status">11370672453</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11370785694',
    created: 1270047926000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/datafront" rel="noopener noreferrer" target="_blank">@datafront</a> That\'s really a concern of EJB, and a value add of EJB. You could also opt to make a portable CDI extension to handle it.<br><br>In reply to: <a href="https://x.com/datafront/status/11364378485" rel="noopener noreferrer" target="_blank">@datafront</a> <span class="status">11364378485</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11370430936',
    created: 1270047520000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cwash" rel="noopener noreferrer" target="_blank">@cwash</a> Sure. But sometimes you need to beat the reader over the head with it ;)<br><br>In reply to: <a href="https://x.com/cwash/status/11351668410" rel="noopener noreferrer" target="_blank">@cwash</a> <span class="status">11351668410</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11370327557',
    created: 1270047396000,
    type: 'post',
    text: 'Here\'s the script I created to archive my twitter timeline incrementally. It gets the job done. <a href="http://gist.github.com/350421" rel="noopener noreferrer" target="_blank">gist.github.com/350421</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '11350779741',
    created: 1270012111000,
    type: 'post',
    text: 'If you are using #maven, the Maven shell should save your life just as maven-cli-plugin once did: <a href="http://shell.sonatype.org/" rel="noopener noreferrer" target="_blank">shell.sonatype.org/</a>',
    likes: 2,
    retweets: 1,
    tags: ['maven']
  },
  {
    id: '11350673640',
    created: 1270011944000,
    type: 'reply',
    text: '@brianleathem Found out it isn\'t support just yet because of a bug in the module, but now that there is a JIRA, it will be :)<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11350548344',
    created: 1270011755000,
    type: 'post',
    text: '"Session-scoped bean are like a stalker with threading and serialization issues; loafs on couch consuming memory" <a href="http://leadingyouastray.blogspot.com/2010/02/references-to-uicomponents-in-session.html" rel="noopener noreferrer" target="_blank">leadingyouastray.blogspot.com/2010/02/references-to-uicomponents-in-session.html</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11347664469',
    created: 1270007694000,
    type: 'post',
    text: 'Today is a good day to update your profile on jboss.org if you currently have no picture and it has you living in the Marshall Islands.',
    likes: 0,
    retweets: 0
  },
  {
    id: '11346007668',
    created: 1270005667000,
    type: 'post',
    text: '#javassist or #scala experts, why can\'t this Scala class be proxied? <a href="http://www.seamframework.org/Community/ScalaJSF20OnJboss6DeploymentError#comment122979" rel="noopener noreferrer" target="_blank">www.seamframework.org/Community/ScalaJSF20OnJboss6DeploymentError#comment122979</a>',
    likes: 0,
    retweets: 0,
    tags: ['javassist', 'scala']
  },
  {
    id: '11344153648',
    created: 1270003778000,
    type: 'post',
    text: 'I feel like I\'ve been moving backwards all day. Ever have days like that? I got a lot done, just not what I initially intended to do.',
    likes: 0,
    retweets: 0
  },
  {
    id: '11333586043',
    created: 1269990274000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> In CDI conversations are either transient or persistent. Gives better perspective I think. transient ~= request.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/11330005978" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">11330005978</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11333062472',
    created: 1269989579000,
    type: 'reply',
    text: '@brianleathem That\'s actually supported already, if you add in the Seam XML Bean Config module. You can define an initial value. We\'ll demo.<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11326859090',
    created: 1269980761000,
    type: 'post',
    text: '#Jazoon 2010, speaker list is revealed <a href="http://jazoon.com/Conference/Speakers" rel="noopener noreferrer" target="_blank">jazoon.com/Conference/Speakers</a>',
    likes: 0,
    retweets: 0,
    tags: ['jazoon']
  },
  {
    id: '11308981103',
    created: 1269957411000,
    type: 'post',
    text: 'What\'s up cuz! Glad to have you over here on twitter ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '11294715482',
    created: 1269929771000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/citytech" rel="noopener noreferrer" target="_blank">@citytech</a> I encourage you all to update your profiles on JBoss Community (jboss.org) so we can link up and share ideas.',
    likes: 0,
    retweets: 0
  },
  {
    id: '11276611281',
    created: 1269905527000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> How about an open mailinglist :) #jpanext',
    likes: 0,
    retweets: 0,
    tags: ['jpanext']
  },
  {
    id: '11274439771',
    created: 1269902762000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> Jozef just asked about that: <a href="http://community.jboss.org/message/534423" rel="noopener noreferrer" target="_blank">community.jboss.org/message/534423</a> JBoss AS-specific solution proposed.<br><br>In reply to: <a href="https://x.com/JohnAment/status/11271631717" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">11271631717</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11274385012',
    created: 1269902690000,
    type: 'reply',
    text: '@brianleathem Likely in the "yet to be fleshed out" document module. Thanks for reminding me about that.<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11272248332',
    created: 1269899758000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Ah, yes. Good point. <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a>?<br><br>In reply to: <a href="https://x.com/lightguardjp/status/11270685798" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">11270685798</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11270608358',
    created: 1269897443000,
    type: 'post',
    text: '#cdi conversation supports setting the timeout: <a href="http://docs.jboss.org/cdi/api/1.0-SP1/javax/enterprise/context/Conversation.html#setTimeout(long)" rel="noopener noreferrer" target="_blank">docs.jboss.org/cdi/api/1.0-SP1/javax/enterprise/context/Conversation.html#setTimeout(long)</a> Don\'t need Seam for that.',
    likes: 0,
    retweets: 0,
    tags: ['cdi']
  },
  {
    id: '11268876383',
    created: 1269895053000,
    type: 'reply',
    text: '@brianleathem Great question for the forums (unless you already posted).<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11268748228',
    created: 1269894876000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Um, the real jboss.org<br><br>In reply to: <a href="https://x.com/maxandersen/status/11266913452" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">11266913452</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11256899300',
    created: 1269879308000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AntholoJ" rel="noopener noreferrer" target="_blank">@AntholoJ</a> We\'re working on est. roadmaps for modules this week. We\'ll have a bundled release ~summer. So just grab the modules a la carte.<br><br>In reply to: <a href="https://x.com/AntholoJ/status/11240450332" rel="noopener noreferrer" target="_blank">@AntholoJ</a> <span class="status">11240450332</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11256691485',
    created: 1269879056000,
    type: 'post',
    text: 'Follow <a class="mention" href="https://x.com/jbossorg" rel="noopener noreferrer" target="_blank">@jbossorg</a> for status updates on production work for the jboss.org infrastructure and (hopefully rare) outages.',
    likes: 0,
    retweets: 2
  },
  {
    id: '11222145881',
    created: 1269824085000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> <a class="mention" href="https://x.com/germanescobar" rel="noopener noreferrer" target="_blank">@germanescobar</a> You are correct that this was all screwed up in the past. But Java EE 6 is different. It\'s not just an attempt.<br><br>In reply to: <a href="https://x.com/JohnAment/status/11221589159" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">11221589159</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11222107941',
    created: 1269824033000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> <a class="mention" href="https://x.com/germanescobar" rel="noopener noreferrer" target="_blank">@germanescobar</a> Stateless and singletons are different beasts. So what you really want are SFSB w/ CDI scope, which you have.<br><br>In reply to: <a href="https://x.com/JohnAment/status/11221589159" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">11221589159</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11222069061',
    created: 1269823980000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> <a class="mention" href="https://x.com/germanescobar" rel="noopener noreferrer" target="_blank">@germanescobar</a> There\'s no lifecycle mismatch between CDI and EJB any longer. CDI provides the lifecycle scoping for SFSB.<br><br>In reply to: <a href="https://x.com/JohnAment/status/11221589159" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">11221589159</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11212420176',
    created: 1269809971000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/germanescobar" rel="noopener noreferrer" target="_blank">@germanescobar</a> <a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> But core services would involve transactions/persistence, security and async (maybe a few others)<br><br>In reply to: <a href="https://x.com/germanescobar/status/11210482349" rel="noopener noreferrer" target="_blank">@germanescobar</a> <span class="status">11210482349</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11212392461',
    created: 1269809931000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/germanescobar" rel="noopener noreferrer" target="_blank">@germanescobar</a> <a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> HA is when it makes sense to involve EJB container: clustering/replication, pooling, timers, concurrency, etc.<br><br>In reply to: <a href="https://x.com/germanescobar/status/11210482349" rel="noopener noreferrer" target="_blank">@germanescobar</a> <span class="status">11210482349</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11212247480',
    created: 1269809727000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/germanescobar" rel="noopener noreferrer" target="_blank">@germanescobar</a> <a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> Ultimately what needs to happen, IMO is that POJOs get all the same core services of EJB, just minus the HA stuff.<br><br>In reply to: <a href="https://x.com/germanescobar/status/11210482349" rel="noopener noreferrer" target="_blank">@germanescobar</a> <span class="status">11210482349</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11209097795',
    created: 1269804967000,
    type: 'post',
    text: 'I\'m finally getting my tweets backed up on a regular basis. I\'ll post the code into a gist when I\'m done. It\'s kind of a hack though.',
    likes: 0,
    retweets: 0
  },
  {
    id: '11208964616',
    created: 1269804758000,
    type: 'post',
    text: 'I\'ve totally worn out my #rockband 2 drums. The heads are bubbling and popping off. I found out they are discontinued :( Time to hit eBay.',
    likes: 0,
    retweets: 0,
    tags: ['rockband']
  },
  {
    id: '11180712590',
    created: 1269753174000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> @brianleathem Seam in Action provides critical foundation of problem to be solved. Core has changed, extensions still apply.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/10845511230" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">10845511230</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11164748222',
    created: 1269728931000,
    type: 'post',
    text: 'Just added myself to the <a href="http://wefollow.com" rel="noopener noreferrer" target="_blank">wefollow.com</a> twitter directory under: #laurel_md #programming #jsf #java #seam #javaee',
    likes: 0,
    retweets: 0,
    tags: ['laurel_md', 'programming', 'jsf', 'java', 'seam', 'javaee']
  },
  {
    id: '11160401222',
    created: 1269721192000,
    type: 'post',
    text: 'In #rockband, I\'ve successfully completed two warm-up songs on the drums in expert mode. My best so far is 93%. Rock on!',
    likes: 0,
    retweets: 0,
    tags: ['rockband']
  },
  {
    id: '11108686000',
    created: 1269635865000,
    type: 'post',
    text: 'We are going to hold Seam meetings on IRC from now on: <a href="http://seamframework.org/Seam3/ProjectMeetings" rel="noopener noreferrer" target="_blank">seamframework.org/Seam3/ProjectMeetings</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11103761145',
    created: 1269628603000,
    type: 'post',
    text: '#arquillian is in at Jazoon! Just got paper accepted e-mail. Woot!',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '11102608481',
    created: 1269626979000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Ah :) Alright you two, take it outside. hehehe ;)<br><br>In reply to: <a href="https://x.com/lincolnthree/status/11102213531" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">11102213531</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11102519047',
    created: 1269626855000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> *glow* That\'s because I\'ve finally been empowered with an army that does what can\'t be done.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/11065663615" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">11065663615</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11101431264',
    created: 1269625403000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> Uh oh, perhaps <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> got confused about what ADF is, or who represents it :(<br><br>In reply to: <a href="https://x.com/aschwart/status/11100680992" rel="noopener noreferrer" target="_blank">@aschwart</a> <span class="status">11100680992</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11099531826',
    created: 1269622984000,
    type: 'reply',
    text: '@netdance Alright! Great news. We\'ve really been missing your ever-practical insight on the JSF EG list. ...and, he\'s back!<br><br>In reply to: @netdance <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11059274408',
    created: 1269559705000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cwash" rel="noopener noreferrer" target="_blank">@cwash</a> Indeed.<br><br>In reply to: <a href="https://x.com/cwash/status/11047616396" rel="noopener noreferrer" target="_blank">@cwash</a> <span class="status">11047616396</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11059179795',
    created: 1269559575000,
    type: 'post',
    text: 'Support the Big Red at one of these national party hotspots: <a href="http://ncaa.cit.cornell.edu/parties.html" rel="noopener noreferrer" target="_blank">ncaa.cit.cornell.edu/parties.html</a> #cornell',
    likes: 0,
    retweets: 0,
    tags: ['cornell']
  },
  {
    id: '11046673901',
    created: 1269541034000,
    type: 'post',
    text: 'Hope to see you in Reston, VA Apr 30 - May 2 for No Fluff Just Stuff #nfjs. Be early bird, get discount. <a href="http://www.nofluffjuststuff.com/conference/reston/2010/04/home" rel="noopener noreferrer" target="_blank">www.nofluffjuststuff.com/conference/reston/2010/04/home</a>',
    likes: 0,
    retweets: 1,
    tags: ['nfjs']
  },
  {
    id: '11042411037',
    created: 1269535066000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> Can you ask him to get categories right?<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/11029205790" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">11029205790</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11042379925',
    created: 1269535024000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> It was about 6 slices of cake ago :)<br><br>In reply to: <a href="https://x.com/jasondlee/status/11033309083" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">11033309083</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11022268103',
    created: 1269499654000,
    type: 'post',
    text: 'Did I mention my wife made me a red velvet cake for my birthday?',
    photos: ['<div class="entry"><img class="photo" src="media/11022268103.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '11022075803',
    created: 1269499204000,
    type: 'post',
    text: 'Just in time for my red alma mater (#cornell) to play in their first ever sweet sixteen. Go Red! I bought a red car just for the event! j/k',
    likes: 0,
    retweets: 0,
    tags: ['cornell']
  },
  {
    id: '11022043385',
    created: 1269499128000,
    type: 'post',
    text: 'Added to my red collection today. red wall (old bedroom), red school (Cornell), red company (Red Hat), red chair, red book, now red car :)',
    likes: 0,
    retweets: 0
  },
  {
    id: '11021959194',
    created: 1269498931000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aaronwalker" rel="noopener noreferrer" target="_blank">@aaronwalker</a> Yep, decided to trade a 2007 Prius for a 2010. As Beyonce would put it, it was time to upgrade. My circle of red is complete :)<br><br>In reply to: <a href="https://x.com/aaronwalker/status/11020882434" rel="noopener noreferrer" target="_blank">@aaronwalker</a> <span class="status">11020882434</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11020668951',
    created: 1269496188000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> We really got the best of both. We finally have embedded XM radio a floor mats (finally), and in red! We also got a great APR.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/11020584019" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">11020584019</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11020404021',
    created: 1269495658000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> That\'s what Eco mode is for :)<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '11018652557',
    created: 1269492430000,
    type: 'post',
    text: 'New car (and lovely wife)',
    photos: ['<div class="entry"><img class="photo" src="media/11018652557.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '11018625459',
    created: 1269492381000,
    type: 'post',
    text: 'Old car',
    photos: ['<div class="entry"><img class="photo" src="media/11018625459.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '10988740614',
    created: 1269450561000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/datafront" rel="noopener noreferrer" target="_blank">@datafront</a> That\'s why I selected Verizon during my years as a beltway bandit contractor ;) Back then, I didn\'t have a cool phone, though.<br><br>In reply to: <a href="https://x.com/datafront/status/10978134953" rel="noopener noreferrer" target="_blank">@datafront</a> <span class="status">10978134953</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10988672402',
    created: 1269450458000,
    type: 'post',
    text: 'Private messaging now works on JBoss Community again. Though used sparingly, it\'s important for first contact w/ a project member/lead.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10967545345',
    created: 1269413305000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> That feedback is valuable and will hopefully help prevent such stress from being allowed in the future.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '10965866875',
    created: 1269409564000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> You not only helped with those pages, you were my moral support throughout the whole authoring process ;) Thanks!<br><br>In reply to: <a href="https://x.com/lincolnthree/status/10962985193" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">10962985193</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10965779166',
    created: 1269409383000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Oh no! We really don\'t want to lose you ;) Hopefully you feel good that the community is standing up for what it believes in.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10962408657',
    created: 1269403464000,
    type: 'post',
    text: 'I also sparked one of the most length #seam debates in history about setting up shop at jboss.org <a href="http://old.nabble.com/Seam-3-space-on-JBoss-Community-td27950629.html" rel="noopener noreferrer" target="_blank">old.nabble.com/Seam-3-space-on-JBoss-Community-td27950629.html</a>',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '10962202196',
    created: 1269403170000,
    type: 'post',
    text: 'I haven\'t been hiding, I\'ve just been typing this for the last two days: <a href="http://seamframework.org/Seam3" rel="noopener noreferrer" target="_blank">seamframework.org/Seam3</a> #seam',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '10955139927',
    created: 1269394075000,
    type: 'post',
    text: 'On Feb 17, "Subversion becomes Apache Subversion" Um, just in time for it to be extinct? Aren\'t we all trying to move to DVCS?',
    likes: 0,
    retweets: 0
  },
  {
    id: '10859150230',
    created: 1269237100000,
    type: 'post',
    text: 'Way to go Big Red, taking down #5 and #4 seeds! If we take down #1, can we just call it history? #cornell #basketball #ncaa',
    likes: 0,
    retweets: 0,
    tags: ['cornell', 'basketball', 'ncaa']
  },
  {
    id: '10857631900',
    created: 1269234077000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Dude, I\'m so on the same wavelength with you! In fact, I\'ve been pitching it as an idea for a programming contractor show.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10857605605',
    created: 1269234034000,
    type: 'post',
    text: 'Give us your feedback on round 3 of the #arquillian logo designs. <a href="http://spreadsheets.google.com/viewform?hl=en&amp;formkey=dExac3NnX2VpRlBvQmdGNFkwYTdveEE6MA" rel="noopener noreferrer" target="_blank">spreadsheets.google.com/viewform?hl=en&amp;formkey=dExac3NnX2VpRlBvQmdGNFkwYTdveEE6MA</a> We are getting close!',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '10826244579',
    created: 1269187054000,
    type: 'post',
    text: 'Marriage Ref is a very strange show. But you should definitely catch the one with Ricky Gervais, Madonna and Larry David. Funny as hell.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10825504620',
    created: 1269185969000,
    type: 'post',
    text: 'Last night I discovered seach keywords in Chromium. Right click address bar and select Edit Search Engines...',
    likes: 0,
    retweets: 0
  },
  {
    id: '10825384407',
    created: 1269185794000,
    type: 'post',
    text: 'Birthday dessert. Mmmm.',
    photos: ['<div class="entry"><img class="photo" src="media/10825384407.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '10805868441',
    created: 1269145147000,
    type: 'post',
    text: 'Clearspace is dead. Long live Jive SBS. Part of keeping up with the times means keeping up with name changes ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '10796144874',
    created: 1269129832000,
    type: 'post',
    text: 'Thanks everyone for the birthday wishes! I\'m happy enjoying my free drink from Starbucks...and got the band back together ;) #rockband',
    likes: 0,
    retweets: 0,
    tags: ['rockband']
  },
  {
    id: '10794497214',
    created: 1269127263000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> Certain days are just birthday days, I suppose :)<br><br>In reply to: <a href="https://x.com/tech4j/status/10793874267" rel="noopener noreferrer" target="_blank">@tech4j</a> <span class="status">10793874267</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10785123941',
    created: 1269110939000,
    type: 'post',
    text: 'Get these odds. My sister and sister-in-law have the same birthday and my future brother-in-law and I have the same birthday. Crazy.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10759300130',
    created: 1269061297000,
    type: 'reply',
    text: '@mwessendorf I\'m not quite there yet. Decrease each digit by 1 ;)<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10757171943',
    created: 1269057741000,
    type: 'post',
    text: 'Celebrating my birth day and my birth minute, 00:01! I was a night owl even in my first breath!',
    likes: 0,
    retweets: 0
  },
  {
    id: '10748015413',
    created: 1269043856000,
    type: 'post',
    text: 'The search on jboss.org has so vastly improved. Go to a space and search...you get Ajax results with filtering. Freakin\' cool.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10728764262',
    created: 1269014201000,
    type: 'post',
    text: 'jboss.org upgrade is complete! Check out the bookmarks feature and "follow this space" for quick and easy linking/navigation. Woot!',
    likes: 0,
    retweets: 1
  },
  {
    id: '10696738997',
    created: 1268957589000,
    type: 'post',
    text: 'Watching my wife and her friend give a talk at the local library. Fun to be on the other side of our typical roles.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10683937443',
    created: 1268937685000,
    type: 'post',
    text: 'I\'m getting involved with the call to improve the JCP and set it free, as Mark Little talks about: <a href="http://www.computerworld.com/s/article/9171838/What_users_want_from_Oracle_s_Java_Community_Process?source=rss_news" rel="noopener noreferrer" target="_blank">www.computerworld.com/s/article/9171838/What_users_want_from_Oracle_s_Java_Community_Process?source=rss_news</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10680570758',
    created: 1268932452000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> Haha. Nope, I just call it like I see it ;)<br><br>In reply to: <a href="https://x.com/rruss/status/10680373813" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">10680373813</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10677829881',
    created: 1268928528000,
    type: 'post',
    text: 'At 5PM EST the JBoss Community team will be performing maintenance and upgrades in order to release the new SBS 4.0.5 Collaboration tool.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10647824313',
    created: 1268873783000,
    type: 'post',
    text: 'Got to listen the Rick present three new Clojure features, prototype, reify and datatype. Gave nice insight into the problems being solved.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10647000537',
    created: 1268872601000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> Last year the turnout really was bad. Enjoyed the folks that were there, but still weak. Glad to see the numbers are up.<br><br>In reply to: <a href="https://x.com/starbuxman/status/10637002252" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">10637002252</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10646829893',
    created: 1268872351000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> Yeah, it\'s nice to hear the instruction clearly. I should also mention that the music fades in and out. Very pleasant.<br><br>In reply to: <a href="https://x.com/arungupta/status/10645204271" rel="noopener noreferrer" target="_blank">@arungupta</a> <span class="status">10645204271</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10646671109',
    created: 1268872126000,
    type: 'post',
    text: 'Rich Hickey on Clojure vs Scala: "I believe Clojure is very simple. It has a lot of newness, yes, but it\'s very simple...and opinionated."',
    likes: 0,
    retweets: 1
  },
  {
    id: '10643241658',
    created: 1268867113000,
    type: 'post',
    text: '#android gets sound mgmt right. Pauses music when voice navigation announces a direction.',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '10643064047',
    created: 1268866859000,
    type: 'post',
    text: 'Penguin crossing at Oracle in Reston, VA facility.',
    photos: ['<div class="entry"><img class="photo" src="media/10643064047.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '10635770673',
    created: 1268855240000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kenglxn" rel="noopener noreferrer" target="_blank">@kenglxn</a> Yeah. You right click on a handle in the room list and select "Add". Then you get all the normal Pidgin buddy features.<br><br>In reply to: <a href="https://x.com/kenglxn/status/10635722207" rel="noopener noreferrer" target="_blank">@kenglxn</a> <span class="status">10635722207</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10635428522',
    created: 1268854683000,
    type: 'post',
    text: 'Sweet. Pidgin is exactly what I was looking for. Can assign custom names, icons and info to any handle. Now I can "see" who I\'m talking to.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10631990410',
    created: 1268848797000,
    type: 'post',
    text: 'I\'m going to try Pidgin again, give it a chance to impress me with IRC support...then I\'ll go from there.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10631464514',
    created: 1268847849000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> But we can expect improvements, even when a technology is decades old.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/10626347877" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">10626347877</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10631433997',
    created: 1268847795000,
    type: 'post',
    text: 'Great! RT<a class="mention" href="https://x.com/JAZOON" rel="noopener noreferrer" target="_blank">@JAZOON</a> We plan to inform speakers, who made a submission for #Jazoon by the end of next week.',
    likes: 0,
    retweets: 0,
    tags: ['jazoon']
  },
  {
    id: '10631423710',
    created: 1268847777000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/datafront" rel="noopener noreferrer" target="_blank">@datafront</a> I just feel more connected to people on twitter than IRC. Would be nice to have that in IRC. Maybe I just need a better client.<br><br>In reply to: <a href="https://x.com/datafront/status/10620389513" rel="noopener noreferrer" target="_blank">@datafront</a> <span class="status">10620389513</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10631375326',
    created: 1268847694000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Yep, new version of Clearspace, only now it\'s called Jive SBS - Social Business Software. Using internally and it\'s much improved.<br><br>In reply to: <a href="https://x.com/kito99/status/10613867081" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">10613867081</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10631318189',
    created: 1268847593000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> Nope, TSSJS is the one conference I\'m skipping this year. I just needed to catch my breath ;)<br><br>In reply to: <a href="https://x.com/starbuxman/status/10600416492" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">10600416492</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10609440499',
    created: 1268807188000,
    type: 'post',
    text: 'I remember what I don\'t like about IRC. People are reduced to (cryptic) handles. I like faces (or avatars) and a brief bio always available.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10599832454',
    created: 1268791570000,
    type: 'post',
    text: 'Anyone know when Jazoon is announcing approved presentations?',
    likes: 0,
    retweets: 0
  },
  {
    id: '10592564202',
    created: 1268781195000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/edburns" rel="noopener noreferrer" target="_blank">@edburns</a> Hear, hear!<br><br>In reply to: <a href="https://x.com/edburns/status/10590101494" rel="noopener noreferrer" target="_blank">@edburns</a> <span class="status">10590101494</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10589149491',
    created: 1268776008000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Yep, it doesn\'t match with reality.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/10588978059" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">10588978059</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10589013013',
    created: 1268775801000,
    type: 'post',
    text: 'I do not consider the JSR-314 EG disbanded, as this section states: <a href="http://jcp.org/en/procedures/jcp2#3.4" rel="noopener noreferrer" target="_blank">jcp.org/en/procedures/jcp2#3.4</a> #jsf2',
    likes: 0,
    retweets: 0,
    tags: ['jsf2']
  },
  {
    id: '10588941998',
    created: 1268775689000,
    type: 'post',
    text: 'JCP process con\'t: "The Spec Lead will typically be the Maintenance Lead and may call upon EG members and others for aid in that role."',
    likes: 0,
    retweets: 0
  },
  {
    id: '10588906617',
    created: 1268775635000,
    type: 'post',
    text: 'Quote from JCP process: "Upon Final Release, the Expert Group will have completed its work and disbands." I completely disagree!!!',
    likes: 0,
    retweets: 1
  },
  {
    id: '10588278430',
    created: 1268774677000,
    type: 'post',
    text: 'I\'m really looking forward to the upgrade of jboss.org portal, which will include a bookmark feature. I like to mark interesting pages.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10587546895',
    created: 1268773552000,
    type: 'post',
    text: 'jboss.org has made a commitment to communicate outages transparently: <a href="http://community.jboss.org/wiki/InfrastructureIssues" rel="noopener noreferrer" target="_blank">community.jboss.org/wiki/InfrastructureIssues</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10584212084',
    created: 1268768265000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <a href="http://community.jboss.org/thread/148191" rel="noopener noreferrer" target="_blank">community.jboss.org/thread/148191</a> Still under discussion.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/10583668972" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">10583668972</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10584179141',
    created: 1268768208000,
    type: 'post',
    text: 'Heard on the radio: "It\'s not who you know, it\'s who knows you." Good point!',
    likes: 0,
    retweets: 0
  },
  {
    id: '10583518957',
    created: 1268767129000,
    type: 'post',
    text: 'Zimbra\'s rich text editor is shit. Period. Too bad, because I\'m mostly comfortable with the rest of the interface.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10583272388',
    created: 1268766726000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I\'m referring to <a class="mention" href="https://x.com/jbossseam" rel="noopener noreferrer" target="_blank">@jbossseam</a><br><br>In reply to: <a href="https://x.com/lightguardjp/status/10582781172" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">10582781172</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10582268733',
    created: 1268765120000,
    type: 'post',
    text: 'Yeah, I have 500 followers! Not sure why that\'s significant, but I do think it\'s funny that one of my other accounts is my 500th follower ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '10529197678',
    created: 1268677706000,
    type: 'post',
    text: 'I\'m very surprised that the Bose Wave Radio does not adjust for DST automatically.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10524289547',
    created: 1268670099000,
    type: 'post',
    text: 'I\'m feeling pretty happy that I got my #javaone talks in on Friday, thereby avoiding the stress of site hanging over the weekend.',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '10524111417',
    created: 1268669837000,
    type: 'reply',
    text: 'Great news for #android! RT @mwessendorf: Web guru <a class="mention" href="https://x.com/timbray" rel="noopener noreferrer" target="_blank">@timbray</a> takes Google Android job <a href="http://news.cnet.com/8301-30685_3-20000423-264.html?part=rss&amp;subj=news&amp;tag=2547-1_3-0-20" rel="noopener noreferrer" target="_blank">news.cnet.com/8301-30685_3-20000423-264.html?part=rss&amp;subj=news&amp;tag=2547-1_3-0-20</a> (via <a class="mention" href="https://x.com/KohlerM" rel="noopener noreferrer" target="_blank">@KohlerM</a>) (via <a class="mention" href="https://x.com/jfarcand" rel="noopener noreferrer" target="_blank">@jfarcand</a>)',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '10524060466',
    created: 1268669761000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jvanzyl" rel="noopener noreferrer" target="_blank">@jvanzyl</a> Why not CDI? Weld has a Java SE bootstrap. Check it out.<br><br>In reply to: <a href="https://x.com/jvanzyl/status/10521553176" rel="noopener noreferrer" target="_blank">@jvanzyl</a> <span class="status">10521553176</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10502726167',
    created: 1268628007000,
    type: 'post',
    text: 'Do you know of any open source communities that have an SLA (not for the software itself, but for the project hosting)?',
    likes: 0,
    retweets: 0
  },
  {
    id: '10490217839',
    created: 1268608927000,
    type: 'post',
    text: 'Implementing CDI (BOF); Secrets of Seam, CDI, RichFaces, JSF 2; and another talk by <a class="mention" href="https://x.com/arubinger" rel="noopener noreferrer" target="_blank">@arubinger</a> on rapid Java EE development',
    likes: 0,
    retweets: 0
  },
  {
    id: '10490167080',
    created: 1268608848000,
    type: 'post',
    text: 'My talks: CDI, Weld &amp; future of Seam; Metawidget: UI generation done right; Real Java EE integration testing w/ Arquillian; Beyond JSF 2 ...',
    likes: 0,
    retweets: 0
  },
  {
    id: '10490122500',
    created: 1268608783000,
    type: 'post',
    text: 'I\'ve submitted or been included on a total of 7 #javaone talks. Hopefully, just the right amount stick (~2).',
    likes: 0,
    retweets: 1,
    tags: ['javaone']
  },
  {
    id: '10490070646',
    created: 1268608702000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> I\'m very glad to see that too! It has been a management problem and finally they are bringing down the hammer.<br><br>In reply to: <a href="https://x.com/kito99/status/10489165036" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">10489165036</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10455485372',
    created: 1268541667000,
    type: 'post',
    text: 'The US Capital by night.',
    photos: ['<div class="entry"><img class="photo" src="media/10455485372.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '10440266405',
    created: 1268517794000,
    type: 'post',
    text: 'Union Street Alexandria swimming in water...to match us swimming in pints ;)',
    photos: ['<div class="entry"><img class="photo" src="media/10440266405.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '10434706952',
    created: 1268507934000,
    type: 'post',
    text: 'Bar # 2. 60 minute IPA. Never gets old.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10430748779',
    created: 1268501418000,
    type: 'post',
    text: 'Sharing beers with future brother-in-law and friend scoping out rehearsal dinner sites.',
    photos: ['<div class="entry"><img class="photo" src="media/10430748779.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '10395867483',
    created: 1268437220000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> There you go, our group name: <a href="http://in.relation.to/Bloggers/OurGroupName" rel="noopener noreferrer" target="_blank">in.relation.to/Bloggers/OurGroupName</a> #cdi #weld #seam #richfaces #jbtools<br><br>In reply to: <a href="https://x.com/rruss/status/10392117602" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">10392117602</span>',
    likes: 0,
    retweets: 0,
    tags: ['cdi', 'weld', 'seam', 'richfaces', 'jbtools']
  },
  {
    id: '10386527971',
    created: 1268422456000,
    type: 'post',
    text: 'I\'ve been revising talk proposals for #javaone all day getting them to fit in character limits and make sure they sound inticing.',
    likes: 0,
    retweets: 1,
    tags: ['javaone']
  },
  {
    id: '10354316372',
    created: 1268364556000,
    type: 'post',
    text: 'I\'m following in the footsteps of <a class="mention" href="https://x.com/jonobacon" rel="noopener noreferrer" target="_blank">@jonobacon</a> :)',
    likes: 0,
    retweets: 0
  },
  {
    id: '10354195363',
    created: 1268364377000,
    type: 'post',
    text: 'Amidst all the other activity this week, I\'ve be made community manager for Seam &amp; Weld <a href="http://in.relation.to/Bloggers/TheCommunityManager" rel="noopener noreferrer" target="_blank">in.relation.to/Bloggers/TheCommunityManager</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10353677684',
    created: 1268363617000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cbredesen" rel="noopener noreferrer" target="_blank">@cbredesen</a> @brianleathem <a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> I\'m focused on the comments that say "I have to vote no because there is no way to vote yes on a revision"<br><br>In reply to: <a href="https://x.com/cbredesen/status/10349429012" rel="noopener noreferrer" target="_blank">@cbredesen</a> <span class="status">10349429012</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10352695896',
    created: 1268362190000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jonobacon" rel="noopener noreferrer" target="_blank">@jonobacon</a> I\'m very interested. Hopefully your coming to a town near me or we cross paths at a conference.<br><br>In reply to: <a href="https://x.com/jonobacon/status/10350871795" rel="noopener noreferrer" target="_blank">@jonobacon</a> <span class="status">10350871795</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10346925822',
    created: 1268353901000,
    type: 'post',
    text: 'Sad. JSR-275 got rejected because the JCP process is broken. Just look at the comments: <a href="http://jcp.org/en/jsr/results?id=5064" rel="noopener noreferrer" target="_blank">jcp.org/en/jsr/results?id=5064</a>',
    likes: 0,
    retweets: 1
  },
  {
    id: '10341093279',
    created: 1268343840000,
    type: 'reply',
    text: '@brianleathem I hear you. You just grab it when you can, like a Marine ;)<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10341034931',
    created: 1268343747000,
    type: 'post',
    text: '#Arquillian made Javalobby <a href="http://java.dzone.com/articles/arquillian-making-integration" rel="noopener noreferrer" target="_blank">java.dzone.com/articles/arquillian-making-integration</a> I was the lab rat in that Devoxx talk ;)',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '10340441525',
    created: 1268342809000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Sorry, I passed out for a while. It was either the turkey sandwich or that I haven\'t slept an 8 hr night in a week or more.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/10339664651" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">10339664651</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10328080148',
    created: 1268323213000,
    type: 'post',
    text: '@mwessendorf We are currently working out what to use for Seam 3. We\'d really like to pair up on JSF enhancements, so I\'m pushing for ASL.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10328022908',
    created: 1268323133000,
    type: 'post',
    text: 'Seam/RichFaces integration/coordination meeting starting now in #richfaces on irc.freenode.net',
    likes: 0,
    retweets: 1,
    tags: ['richfaces']
  },
  {
    id: '10326227694',
    created: 1268320599000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/wesleyhales" rel="noopener noreferrer" target="_blank">@wesleyhales</a> You are a champion.<br><br>In reply to: <a href="https://x.com/wesleyhales/status/10325249342" rel="noopener noreferrer" target="_blank">@wesleyhales</a> <span class="status">10325249342</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10308755850',
    created: 1268285016000,
    type: 'post',
    text: '5 posts on in.relation.to today! Can\'t say we aren\'t keeping you entertained :) I would have helped make it 6, but just ran out of gas.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10308299986',
    created: 1268284163000,
    type: 'post',
    text: 'Show your love for the #Arquillian release announcement: <a href="http://www.dzone.com/links/arquillian_100_alpha_1_released.html" rel="noopener noreferrer" target="_blank">www.dzone.com/links/arquillian_100_alpha_1_released.html</a>',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '10298577656',
    created: 1268269640000,
    type: 'post',
    text: 'A used DVD is like a used mp3. I mean, unless it is scratched, it doesn\'t really matter how many times it has been played.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10295788603',
    created: 1268265657000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremyg484" rel="noopener noreferrer" target="_blank">@jeremyg484</a> Yep, I\'ll admit it\'s an open invitation to experiment, see what\'s possible. Best suited for an interested community member atm.<br><br>In reply to: <a href="https://x.com/jeremyg484/status/10294124693" rel="noopener noreferrer" target="_blank">@jeremyg484</a> <span class="status">10294124693</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10293325573',
    created: 1268262053000,
    type: 'post',
    text: 'I\'ve started the "micro deployment" wave. Let\'s see how far it goes around ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '10292604076',
    created: 1268261001000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremyg484" rel="noopener noreferrer" target="_blank">@jeremyg484</a> Basically, I think you can get more mileage out of Arquillian\'s design. Piggyback on the support that is there.<br><br>In reply to: <a href="https://x.com/jeremyg484/status/10292403344" rel="noopener noreferrer" target="_blank">@jeremyg484</a> <span class="status">10292403344</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10292569673',
    created: 1268260949000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremyg484" rel="noopener noreferrer" target="_blank">@jeremyg484</a> The Spring tests are a popular solution :) Arquillian breaks problem into elemental parts w/ SPI: package, deploy, enrich, etc.<br><br>In reply to: <a href="https://x.com/jeremyg484/status/10292403344" rel="noopener noreferrer" target="_blank">@jeremyg484</a> <span class="status">10292403344</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10290120750',
    created: 1268257285000,
    type: 'post',
    text: 'Hey #spring community, want to write a Spring container impl for #arquillian? I\'m just throwing it out there.',
    likes: 0,
    retweets: 0,
    tags: ['spring', 'arquillian']
  },
  {
    id: '10290062991',
    created: 1268257200000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Hahaha! That\'s a great sign that we are wrestling over robustness. I never had any doubt #shrinkwrap would win ;)<br><br>In reply to: <a href="https://x.com/ALRubinger/status/10288750134" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">10288750134</span>',
    likes: 0,
    retweets: 0,
    tags: ['shrinkwrap']
  },
  {
    id: '10290002697',
    created: 1268257110000,
    type: 'post',
    text: 'Pete Muir is on fire today: <a href="http://in.relation.to" rel="noopener noreferrer" target="_blank">in.relation.to</a> This must be some vacation he is about to go on ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '10288390794',
    created: 1268254465000,
    type: 'post',
    text: 'But now that we have #arquillian underway, I predict that, across the board, jboss.org projects will be extremely well tested.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '10288352670',
    created: 1268254402000,
    type: 'post',
    text: 'I said in my #cdi presentation that "Weld is probably the most well tested piece of software at jboss.org." Actually, #shrinkwrap too.',
    likes: 0,
    retweets: 1,
    tags: ['cdi', 'shrinkwrap']
  },
  {
    id: '10287947237',
    created: 1268253758000,
    type: 'post',
    text: 'Huge day for #arquillian! A project intro <a href="http://in.relation.to/14771.lace" rel="noopener noreferrer" target="_blank">in.relation.to/14771.lace</a> and alpha1 release <a href="http://in.relation.to/14773.lace" rel="noopener noreferrer" target="_blank">in.relation.to/14773.lace</a>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '10282949183',
    created: 1268245730000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> No, #android is open source. The source code is available int git: <a href="http://android.git.kernel.org/" rel="noopener noreferrer" target="_blank">android.git.kernel.org/</a><br><br>In reply to: <a href="https://x.com/maxandersen/status/10282474403" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">10282474403</span>',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '10281518871',
    created: 1268243425000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Actually, we\'ve determined that twitter is a convenient way to keep constant flow of community between employee and manager.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/10281358854" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">10281358854</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10281054857',
    created: 1268242682000,
    type: 'post',
    text: 'Image quality in evince in Ubuntu Karmic sucks. Looks like it\'s fixed upstream, so we\'ll have to live with it for now.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10280545393',
    created: 1268241890000,
    type: 'post',
    text: 'Super good news. #android 2.1 is coming to t-mobile, including the G1. <a href="http://androidandme.com/2010/02/news/all-u-s-android-phones-to-receive-android-2-1-but-some-will-require-a-wipe/" rel="noopener noreferrer" target="_blank">androidandme.com/2010/02/news/all-u-s-android-phones-to-receive-android-2-1-but-some-will-require-a-wipe/</a> Wipe or no wipe, I\'m ready.',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '10278747769',
    created: 1268239170000,
    type: 'post',
    text: 'I just love waking up to a meeting I didn\'t know I had. #prep #fail',
    likes: 0,
    retweets: 0,
    tags: ['prep', 'fail']
  },
  {
    id: '10260763755',
    created: 1268204847000,
    type: 'post',
    text: 'The JavaDoc for #cdi is quite good. You should read it. <a href="http://docs.jboss.org/cdi/api/1.0-SP1/" rel="noopener noreferrer" target="_blank">docs.jboss.org/cdi/api/1.0-SP1/</a> <a class="mention" href="https://x.com/see" rel="noopener noreferrer" target="_blank">@see</a> javax.enterprise.inject.spi',
    likes: 0,
    retweets: 0,
    tags: ['cdi']
  },
  {
    id: '10260736275',
    created: 1268204779000,
    type: 'post',
    text: 'It was also great to see <a class="mention" href="https://x.com/barryhawkins" rel="noopener noreferrer" target="_blank">@barryhawkins</a> and <a class="mention" href="https://x.com/jlward4th" rel="noopener noreferrer" target="_blank">@jlward4th</a> again at #devnexus and meet <a class="mention" href="https://x.com/CharlieCollins" rel="noopener noreferrer" target="_blank">@CharlieCollins</a>. Conferences are catalysts.',
    likes: 0,
    retweets: 0,
    tags: ['devnexus']
  },
  {
    id: '10260671012',
    created: 1268204627000,
    type: 'post',
    text: 'To JBoss SAs, interested to receive your feedback about what we have planned for Seam 3: <a href="http://sfwk.org/Documentation/Seam3Modules" rel="noopener noreferrer" target="_blank">sfwk.org/Documentation/Seam3Modules</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10260619996',
    created: 1268204508000,
    type: 'reply',
    text: '@brianleathem It\'s coming soon, esp with <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> on board!<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10260583402',
    created: 1268204427000,
    type: 'post',
    text: 'I got a number of compliments on Seam in Action at #devnexus. I\'m so glad it turned out to be such a great resource for developers.',
    likes: 0,
    retweets: 1,
    tags: ['devnexus']
  },
  {
    id: '10259945432',
    created: 1268202937000,
    type: 'post',
    text: 'I was thrown off a bit in my talk because my laptop screen wasn\'t playing the presentation. The presenter console will boast my delivery.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10259908241',
    created: 1268202854000,
    type: 'post',
    text: 'I just got the presenter console working in OpenOffice Impress 3.1. Damn. Wish I had it for my talk at #devnexus!',
    photos: ['<div class="entry"><img class="photo" src="media/10259908241.png"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['devnexus']
  },
  {
    id: '10251721821',
    created: 1268188942000,
    type: 'post',
    text: 'Almost missed my damn flight. Gate change. Got my exercise for the night.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10250715871',
    created: 1268187548000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> I just sent them your way. (I\'ll have them up on sfwk.org soon too).<br><br>In reply to: <a href="https://x.com/arungupta/status/10248656769" rel="noopener noreferrer" target="_blank">@arungupta</a> <span class="status">10248656769</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10250628833',
    created: 1268187435000,
    type: 'post',
    text: '"Will your child be sitting in a car seat on the next flight?" Wouldn\'t it be a plane seat then?',
    likes: 0,
    retweets: 0
  },
  {
    id: '10247007164',
    created: 1268182481000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/burrsutter" rel="noopener noreferrer" target="_blank">@burrsutter</a> Your wife knows what she is talking about ;)<br><br>In reply to: <a href="https://x.com/burrsutter/status/10188245638" rel="noopener noreferrer" target="_blank">@burrsutter</a> <span class="status">10188245638</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10246755082',
    created: 1268182124000,
    type: 'post',
    text: 'At a Sam Adams Brewhouse drinking Sam Adams beer in a Bud Light glass. Hmmmm.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10246703120',
    created: 1268182049000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> Thanks for attending my talk. I appreciate the support and comradery!<br><br>In reply to: <a href="https://x.com/arungupta/status/10242707282" rel="noopener noreferrer" target="_blank">@arungupta</a> <span class="status">10242707282</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10242348637',
    created: 1268175759000,
    type: 'post',
    text: 'Said hi to Keith Donald at #devnexus. Chatted w/ him about Spring Roo as project generation foundation and hot deployment of Java classes.',
    likes: 0,
    retweets: 0,
    tags: ['devnexus']
  },
  {
    id: '10242225474',
    created: 1268175590000,
    type: 'post',
    text: 'Self observation from #devnexus. I am fired up about making the jboss.org community kick ass.',
    likes: 0,
    retweets: 0,
    tags: ['devnexus']
  },
  {
    id: '10241972620',
    created: 1268175208000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> Actually, it\'s qualifier marks a variant, alternative is an override.<br><br>In reply to: <a href="https://x.com/arungupta/status/10236598030" rel="noopener noreferrer" target="_blank">@arungupta</a> <span class="status">10236598030</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10241840198',
    created: 1268175008000,
    type: 'post',
    text: 'I had a nice chat w/ <a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> about progress in Java EE, making it simple for developers to adopt the platform and community docs.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10238821224',
    created: 1268170446000,
    type: 'post',
    text: 'Just finished my #cdi and #seam talk at #devnexus. Ran short on time again...there is just so much exciting stuff to talk about ;)',
    likes: 0,
    retweets: 0,
    tags: ['cdi', 'seam', 'devnexus']
  },
  {
    id: '10232758597',
    created: 1268160380000,
    type: 'post',
    text: '"Attractive things work better" User experience talk at #devnexus',
    likes: 0,
    retweets: 1,
    tags: ['devnexus']
  },
  {
    id: '10222506914',
    created: 1268144709000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GCAtech" rel="noopener noreferrer" target="_blank">@GCAtech</a> Unlocking Android, Manning. It\'s an excellent book. Good backstory &amp; starter guide. Only covers Android 1.5, but same foundation.<br><br>In reply to: <a href="https://x.com/GCAtech/status/10220383905" rel="noopener noreferrer" target="_blank">@GCAtech</a> <span class="status">10220383905</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10208083429',
    created: 1268113528000,
    type: 'post',
    text: '#arquillian team is compiling a special gift for you this week.',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '10208060612',
    created: 1268113483000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> Yep. I\'ll be around #devnexus tomorrow as well.<br><br>In reply to: <a href="https://x.com/arungupta/status/10187279200" rel="noopener noreferrer" target="_blank">@arungupta</a> <span class="status">10187279200</span>',
    likes: 0,
    retweets: 0,
    tags: ['devnexus']
  },
  {
    id: '10208049448',
    created: 1268113460000,
    type: 'post',
    text: 'I liked Neal Ford\'s talk, but he gives too much credence to iPhone and Apple. Android is storming mobile world &amp; amazon mp3 in music sales.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10207971102',
    created: 1268113298000,
    type: 'post',
    text: 'Learned in Neal Ford\'s entertaining keynote at #devnexus that the thumb is becoming the primary digit in humans (e.g., elevator button).',
    likes: 0,
    retweets: 0,
    tags: ['devnexus']
  },
  {
    id: '10207874596',
    created: 1268113104000,
    type: 'reply',
    text: '@mwessendorf Use TweetsRide. It pretty much does it all.<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10207849861',
    created: 1268113054000,
    type: 'post',
    text: 'Attended the Android talk at #devnexus. Didn\'t learn anything new since I\'ve already read speaker\'s book, but enjoyed supporting the cause.',
    likes: 0,
    retweets: 0,
    tags: ['devnexus']
  },
  {
    id: '10185914956',
    created: 1268079175000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> Awesome dude. We need to get together. You\'ve got the boot design my wife has w/ the wheel.<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/10182433986" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">10182433986</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10185583851',
    created: 1268078626000,
    type: 'post',
    text: 'Just had a great discussion with Wesley Hales about community. He and I share a very similar, optimistic vision for jboss.org. Yeah!',
    likes: 0,
    retweets: 0
  },
  {
    id: '10184530087',
    created: 1268076865000,
    type: 'post',
    text: 'Wesley Hales speaking about GateIn at #devnexus.',
    photos: ['<div class="entry"><img class="photo" src="media/10184530087.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['devnexus']
  },
  {
    id: '10170265537',
    created: 1268054699000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jakobkorherr" rel="noopener noreferrer" target="_blank">@jakobkorherr</a> Pictures from my tweet backlog ;) My net access was intermitent overseas.<br><br>In reply to: <a href="https://x.com/jakobkorherr/status/10169966473" rel="noopener noreferrer" target="_blank">@jakobkorherr</a> <span class="status">10169966473</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10170207164',
    created: 1268054599000,
    type: 'post',
    text: 'Hope you enjoyed that flashback to sights from my trip to Vienna for #jsfdays.',
    likes: 0,
    retweets: 0,
    tags: ['jsfdays']
  },
  {
    id: '10170169648',
    created: 1268054535000,
    type: 'post',
    text: 'An ice skating festival on ice highways and byways at Vienna\'s town center.',
    photos: ['<div class="entry"><img class="photo" src="media/10170169648.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '10170052050',
    created: 1268054337000,
    type: 'post',
    text: 'They like their sweets to be recognizable &amp; colorful in Vienna. Who let the chickens out?',
    photos: ['<div class="entry"><img class="photo" src="media/10170052050.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '10169975698',
    created: 1268054207000,
    type: 'post',
    text: 'Enjoying a beer in Vienna by candlelight ;)',
    photos: ['<div class="entry"><img class="photo" src="media/10169975698.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '10169927029',
    created: 1268054123000,
    type: 'post',
    text: 'Routine morning coffee and chocolate pastry fix at Aida in Vienna. Love those pastries.',
    photos: ['<div class="entry"><img class="photo" src="media/10169927029.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '10169836563',
    created: 1268053967000,
    type: 'post',
    text: 'Hofburg Imperial Palace in Vienna. Impressive.',
    photos: ['<div class="entry"><img class="photo" src="media/10169836563.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '10169594740',
    created: 1268053550000,
    type: 'post',
    text: 'About to depart for my trip to Atlanta to speak at #devnexus. The flight goes on to St. Thomas. On second thought...',
    likes: 0,
    retweets: 0,
    tags: ['devnexus']
  },
  {
    id: '10154275612',
    created: 1268021523000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> I sure wish I could be there to see it. I\'m going to speaking a couple hours earlier about #cdi and #seam @ DevNexus in Atlanta.',
    likes: 0,
    retweets: 0,
    tags: ['cdi', 'seam']
  },
  {
    id: '10134854403',
    created: 1267994039000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cbredesen" rel="noopener noreferrer" target="_blank">@cbredesen</a> Nope, I\'m using the chromium builds from Ubuntu PPA. I don\'t see chrome in Google\'s apt repository.<br><br>In reply to: <a href="https://x.com/cbredesen/status/10129305249" rel="noopener noreferrer" target="_blank">@cbredesen</a> <span class="status">10129305249</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10134503920',
    created: 1267993450000,
    type: 'post',
    text: 'Think #seam 2 security is powerful? Just wait until you see what is coming for Seam 3. Same API + federated/enterprise identity #picketbox',
    likes: 0,
    retweets: 0,
    tags: ['seam', 'picketbox']
  },
  {
    id: '10128992477',
    created: 1267984097000,
    type: 'post',
    text: 'A word of advice for international travel. Have your itinerary printed, not just you boarding pass. It was required to go Vienna -&gt; US.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10128557888',
    created: 1267983363000,
    type: 'post',
    text: 'I\'m getting really annoyed by the fact that file uploads are broken in the Chromium builds for Linux.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10088452805',
    created: 1267907376000,
    type: 'post',
    text: 'Why are there no constants for the javax.servlet.include.* request attributes in Servlet 3.0??? Hello. That would be helpful.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10086535924',
    created: 1267903940000,
    type: 'post',
    text: 'I found this picture of Felipe, his friend Claudio and me at my first international conference, Jazoon 08.',
    photos: ['<div class="entry"><img class="photo" src="media/10086535924.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '10086091501',
    created: 1267903162000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> What!? I\'m shocked. I know Felipe from talking to him at several conference, extremely nice guy. What happened?<br><br>In reply to: <a href="https://x.com/arungupta/status/10084745994" rel="noopener noreferrer" target="_blank">@arungupta</a> <span class="status">10084745994</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10080369038',
    created: 1267893741000,
    type: 'post',
    text: 'I heart the github gist. So useful.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10041732459',
    created: 1267822742000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/scottdavis99" rel="noopener noreferrer" target="_blank">@scottdavis99</a> I\'d take a Safari subscription. Much less materialistic.<br><br>In reply to: <a href="https://x.com/scottdavis99/status/10041048848" rel="noopener noreferrer" target="_blank">@scottdavis99</a> <span class="status">10041048848</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10041047507',
    created: 1267821626000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/jennrapp" rel="noopener noreferrer" target="_blank">@jennrapp</a> Don\'t forget <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10033573026',
    created: 1267809886000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I had to refresh my memory. Dealing w/ exceptions in web.xml is just wrong for JSF. Outside of lifecycle. A punt at best.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/10033030485" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">10033030485</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10033265584',
    created: 1267809438000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Notice it was done in Java code.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/10033030485" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">10033030485</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10032873736',
    created: 1267808875000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> There\'s confusion around the subject. What was fixed was reporting the exceptions via a central hub. Not declarative handling.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/10032821135" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">10032821135</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10031643894',
    created: 1267807102000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> We desperately need that in Seam 3 Faces. It didn\'t make it into JSF 2.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/10029624539" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">10029624539</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '10028984414',
    created: 1267803356000,
    type: 'post',
    text: 'I spend a lot of time tidying the e-mail message to which I\'m replying. I wish Gmail would understand the thread structure better.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10025736915',
    created: 1267798747000,
    type: 'post',
    text: 'Where the lack of compile-only scope stings is when using stripped APIs like javaee-6-api. Strange errors occur at test runtime.',
    likes: 0,
    retweets: 0
  },
  {
    id: '10025676353',
    created: 1267798656000,
    type: 'post',
    text: 'In Maven, there is no way to mark a dependency as compile only. There is provided, but that still ends up on the test classpath.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9996197709',
    created: 1267744999000,
    type: 'post',
    text: 'Correction, <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> will be covering #arquillian (and #shrinkwrap) at JBossWorld. Got my As mixed up :)',
    likes: 0,
    retweets: 0,
    tags: ['arquillian', 'shrinkwrap']
  },
  {
    id: '9989924081',
    created: 1267735624000,
    type: 'post',
    text: 'Are you a student interested in summer Seam projects (perhaps GSoC)? <a href="http://www.seamframework.org/Community/StudentProjects" rel="noopener noreferrer" target="_blank">www.seamframework.org/Community/StudentProjects</a>',
    likes: 0,
    retweets: 1
  },
  {
    id: '9989810326',
    created: 1267735444000,
    type: 'post',
    text: 'I do temperature in Celsius, time in 24hr, time zone in UTC, and I drink Belgian beer. Call me a renaissance American ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '9982654211',
    created: 1267723819000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Absolutely.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/9981020445" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">9981020445</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9980825107',
    created: 1267721015000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Similar to what we have done in Weld? Where you doing the explaining?<br><br>In reply to: <a href="https://x.com/ALRubinger/status/9980586697" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">9980586697</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9980116576',
    created: 1267719949000,
    type: 'post',
    text: 'At #jbossworld I\'ll be doing a Seam 3 talk w/ Pete Muir, a RichFaces + JSF 2 overview with <a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> and <a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> will cover #arquillian.',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld', 'arquillian']
  },
  {
    id: '9976350234',
    created: 1267714298000,
    type: 'post',
    text: 'Overview of the three pillars of Linux: <a href="http://www.linux-mag.com/id/7721/2/" rel="noopener noreferrer" target="_blank">www.linux-mag.com/id/7721/2/</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '9976028511',
    created: 1267713805000,
    type: 'post',
    text: 'The 3 pillars of Linux are Slackware, Debian and Red Hat, according to Linux Mag. I\'d consider Ubuntu a pillar too, even though it\'s Debian.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9975940640',
    created: 1267713669000,
    type: 'post',
    text: 'Another fun fact: Debian remains the largest non-commercial distributor of Linux.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9975903111',
    created: 1267713611000,
    type: 'post',
    text: 'Fun fact: Slackware remains the oldest surviving Linux distribution. In fact, I\'ve even used it (for a day).',
    likes: 0,
    retweets: 0
  },
  {
    id: '9975606409',
    created: 1267713157000,
    type: 'post',
    text: 'JSFUnit 1.2.0.Final is out: <a href="http://jsfunit.blogspot.com/" rel="noopener noreferrer" target="_blank">jsfunit.blogspot.com/</a>',
    likes: 0,
    retweets: 1
  },
  {
    id: '9948631907',
    created: 1267662554000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> We all have to have something to aspire to ;)<br><br>In reply to: <a href="https://x.com/lincolnthree/status/9946034615" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">9946034615</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9944119017',
    created: 1267655815000,
    type: 'post',
    text: 'Which development communities do you like best? Which ones do you find the most inviting? The most inspiring? The most successful?',
    likes: 0,
    retweets: 0
  },
  {
    id: '9941994547',
    created: 1267652498000,
    type: 'reply',
    text: '@mwessendorf Hehehe. Just a love tap ;)<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9941577264',
    created: 1267651830000,
    type: 'reply',
    text: '@mwessendorf Die Apple.<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9935976613',
    created: 1267642484000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> In this particular case, the root cause is that the JCP website is very far from user friendly.<br><br>In reply to: <a href="https://x.com/maxandersen/status/9935776825" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">9935776825</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9927690534',
    created: 1267629033000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> All add that to our list of changes we want to see in the JCP.<br><br>In reply to: <a href="https://x.com/maxandersen/status/9924460197" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">9924460197</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9924634512',
    created: 1267624285000,
    type: 'post',
    text: 'Up early this morning to do a #seam webinar as part of the JBoss EAP 5 series.',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '9917133575',
    created: 1267607888000,
    type: 'post',
    text: 'The in-depth story on coca-cola and its global brand covers influence, politics and customer loyalty. Fascinating.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9917103351',
    created: 1267607806000,
    type: 'post',
    text: 'Caught "Coca-cola: The real story behind the real thing" on CNBC. Now I get the "classic" qualifier; a patch for reinstating formula in \'83.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9899902674',
    created: 1267576991000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Richard explained that customization of the layout is certainly possible. I do believe it needs to be done in Java atm.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/9896026695" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">9896026695</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9893198762',
    created: 1267567041000,
    type: 'post',
    text: 'Frantically throwing together slides for a #seam webinar tomorrow: <a href="http://www.emergent360.com/news-events/details/jboss_productivity_frameworks/" rel="noopener noreferrer" target="_blank">www.emergent360.com/news-events/details/jboss_productivity_frameworks/</a>',
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '9891641635',
    created: 1267564581000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> Well, we are about to launch it...I guess you could say we are preparing for the real discussion ;)<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/9886367111" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">9886367111</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9886122739',
    created: 1267555456000,
    type: 'post',
    text: 'Having a great discussion internally about The Art of Community and how it applies to jboss.org.',
    likes: 0,
    retweets: 1
  },
  {
    id: '9885287769',
    created: 1267554070000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> Try the Maven shell website: <a href="http://shell.sonatype.org/" rel="noopener noreferrer" target="_blank">shell.sonatype.org/</a><br><br>In reply to: <a href="https://x.com/tech4j/status/9882747114" rel="noopener noreferrer" target="_blank">@tech4j</a> <span class="status">9882747114</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9873823296',
    created: 1267536163000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> Great! Btw, we are going to provide aid in making in happen. All we want is to be able to maintain our promise to the community.<br><br>In reply to: @pelegri <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9872550910',
    created: 1267533898000,
    type: 'post',
    text: 'We are happy to welcome Mark Struberg, Apache PMC, as an observer on the JSR-314-OPEN mailinglist.',
    likes: 0,
    retweets: 1
  },
  {
    id: '9845693526',
    created: 1267483954000,
    type: 'post',
    text: 'I created my first gist to show a #jsf2 postValidateEvent example: <a href="http://gist.github.com/318905" rel="noopener noreferrer" target="_blank">gist.github.com/318905</a>',
    likes: 0,
    retweets: 0,
    tags: ['jsf2']
  },
  {
    id: '9843219396',
    created: 1267480142000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> It\'s definitely not due to lack of trying. My e-mail is just winning.<br><br>In reply to: <a href="https://x.com/kito99/status/9842555279" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">9842555279</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9841519281',
    created: 1267477402000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Hahaha! Nice one ;)<br><br>In reply to: <a href="https://x.com/jasondlee/status/9841309613" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">9841309613</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9841110792',
    created: 1267476746000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> Plus, the web archives for JSR-314-OPEN broke long before the acquisition. This issue is way overdue to getting resolved.<br><br>In reply to: @pelegri <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9841010563',
    created: 1267476584000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> Oracle has said from the very start of this acquisition "Business as usual". I\'m holding them to that statement.<br><br>In reply to: @pelegri <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9836527675',
    created: 1267469099000,
    type: 'post',
    text: 'Our request to the #jcp PMO to have the JSR-314-OPEN web archives restored by Mar 1 was not met. It\'s time to turn up the heat.',
    likes: 0,
    retweets: 0,
    tags: ['jcp']
  },
  {
    id: '9835487317',
    created: 1267467349000,
    type: 'post',
    text: 'Early draft review of #jsr310 (Date &amp; Time) is underway! If you develop in Java, this should affect you! <a href="http://wiki.java.net/bin/view/Projects/DateTimeEDR1" rel="noopener noreferrer" target="_blank">wiki.java.net/bin/view/Projects/DateTimeEDR1</a>',
    likes: 0,
    retweets: 1,
    tags: ['jsr310']
  },
  {
    id: '9835222792',
    created: 1267466905000,
    type: 'post',
    text: 'Just booked my flight for #devnexus 2010 next week. Arrive Mon, leave Tues after my #cdi talk @ 2:50 PM. I\'ll be staying w/ my sis!',
    likes: 0,
    retweets: 0,
    tags: ['devnexus', 'cdi']
  },
  {
    id: '9832659292',
    created: 1267462739000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> I almost approaching a year *since* I had zero inbox :(<br><br>In reply to: <a href="https://x.com/kito99/status/9832188457" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">9832188457</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9831154634',
    created: 1267460371000,
    type: 'post',
    text: 'opensource.com is an online conversation, hosted by Red Hat, about applying open source principles/methodologies to software &amp; beyond.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9821584529',
    created: 1267444457000,
    type: 'post',
    text: 'Trying to play catch up today. Lots of ground to cover.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9810107396',
    created: 1267417771000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rayme" rel="noopener noreferrer" target="_blank">@rayme</a> Hahaha. That should have been "hats off" ;)<br><br>In reply to: <a href="https://x.com/rayme/status/9805716067" rel="noopener noreferrer" target="_blank">@rayme</a> <span class="status">9805716067</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9797169056',
    created: 1267398638000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/mojavelinux" rel="noopener noreferrer" target="_blank">@mojavelinux</a> But hats of to Germany for nudging out the USA in golds, and Canada for raking in the most.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9796830979',
    created: 1267398208000,
    type: 'post',
    text: 'We have to be really proud of our 37 medals in this #olympics, and for putting an end to Germany\'s reign ;) Well done USA!',
    likes: 0,
    retweets: 0,
    tags: ['olympics']
  },
  {
    id: '9795889266',
    created: 1267397114000,
    type: 'post',
    text: 'I was working on some patterns for doing JPA w/ CDI and JSF using the conversation scope on the plane. I\'ll do a blog soon.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9795729648',
    created: 1267396904000,
    type: 'post',
    text: 'The US dominated puck possession all of the 3rd period. Caught Canada sleeping. #olympics #hockey',
    likes: 0,
    retweets: 0,
    tags: ['olympics', 'hockey']
  },
  {
    id: '9795591539',
    created: 1267396718000,
    type: 'post',
    text: 'Woot! USA with the *clutch* of all clutch goals. Turning garbage into a paystub. Aaaaaah! #olympics #hockey',
    likes: 0,
    retweets: 0,
    tags: ['olympics', 'hockey']
  },
  {
    id: '9768921207',
    created: 1267344786000,
    type: 'post',
    text: 'Free we-fee at Vienna Airport FTW!',
    likes: 0,
    retweets: 0
  },
  {
    id: '9743664694',
    created: 1267301396000,
    type: 'post',
    text: 'The Austrian Palace gardens (e.g., Schonbrunn, Hofsburg) are missing their stylesheets this time of year. Nice structure, but ugly.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9735689420',
    created: 1267288629000,
    type: 'post',
    text: 'After getting used to Android\'s location awareness, it\'s *really* annoying to have to manual enter your location when at your computer.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9735538547',
    created: 1267288403000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Oh no, dude! That\'s terrible. What did we get you into? I\'ve got my fingers crossed it\'s something you can get over quickly.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/9728471908" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">9728471908</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9720957301',
    created: 1267258271000,
    type: 'post',
    text: 'Google Maps and Places for Android should translate reviews. Don\'t do much good if you can\'t read them.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9719780901',
    created: 1267255468000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kenglxn" rel="noopener noreferrer" target="_blank">@kenglxn</a> It\'s in the works.<br><br>In reply to: <a href="https://x.com/kenglxn/status/9626261452" rel="noopener noreferrer" target="_blank">@kenglxn</a> <span class="status">9626261452</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9719734601',
    created: 1267255362000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/annikaskywalkr" rel="noopener noreferrer" target="_blank">@annikaskywalkr</a> The story behind the story is that on another desktop the presentation was running in presentation mode ;)<br><br>In reply to: <a href="https://x.com/AnnikaSkywalker/status/9719509911" rel="noopener noreferrer" target="_blank">@AnnikaSkywalker</a> <span class="status">9719509911</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9719259474',
    created: 1267254287000,
    type: 'post',
    text: 'Here\'s a classic UI foul from OpenOffice. Exit is disabled. I guess that is one way to keep someone using the ap',
    photos: ['<div class="entry"><img class="photo" src="media/9719259474.png"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '9704347595',
    created: 1267230323000,
    type: 'post',
    text: 'Had a really excellent and fresh dinner tonight at VaPiano in Vienna. Especially enjoyable as it is a smoke-fee zone.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9704255617',
    created: 1267230182000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> Seam and Weld refdocs are released online to coincide with the software release. We use the wiki for more dynamic stuff.<br><br>In reply to: <a href="https://x.com/arungupta/status/9688278598" rel="noopener noreferrer" target="_blank">@arungupta</a> <span class="status">9688278598</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9704115628',
    created: 1267229971000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> I\'ll get back to you shortly on this. We are further along than it appears on the surface.<br><br>In reply to: <a href="https://x.com/jasondlee/status/9692336720" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">9692336720</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9685622391',
    created: 1267202476000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cbredesen" rel="noopener noreferrer" target="_blank">@cbredesen</a> <a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Cool, sounds like a nice enhancement to our process is in order. Anything we can do to make jboss.org compelling.<br><br>In reply to: <a href="https://x.com/cbredesen/status/9685355108" rel="noopener noreferrer" target="_blank">@cbredesen</a> <span class="status">9685355108</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9685466886',
    created: 1267202268000,
    type: 'post',
    text: 'Unfortunately, the 38% smoking populace in Austria is putting a damper on my visit. I\'ve learned the cafe == lots of smoking. Yak.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9685227855',
    created: 1267201950000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> You present a nice idea. Perhaps jboss.org should develop/provide an app for community feedback of artwork -&gt; JIRA<br><br>In reply to: <a href="https://x.com/lightguardjp/status/9681075772" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">9681075772</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9685153120',
    created: 1267201850000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> No dice. Looks like you have to host your own version of the form if you want to do something fancy like embedding images :(<br><br>In reply to: <a href="https://x.com/lightguardjp/status/9684981709" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">9684981709</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9684909845',
    created: 1267201523000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Sorry, I was just being sort of lazy in using the format Pete created. I\'ll see if the Google form lets me embed the images.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/9680803525" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">9680803525</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9670267790',
    created: 1267174565000,
    type: 'post',
    text: 'In the JSR-299: In theory and praxis talk at #jsfdays, Mark Struberg showed #cdi support for the existing #jsf2 <a class="mention" href="https://x.com/ViewScoped" rel="noopener noreferrer" target="_blank">@ViewScoped</a> annotation.',
    likes: 0,
    retweets: 0,
    tags: ['jsfdays', 'cdi', 'jsf2']
  },
  {
    id: '9670073315',
    created: 1267174072000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> @mwessendorf To view JSR-330 as anything more than common annotations is just loony. Who cares where the annotation resides.<br><br>In reply to: <a href="https://x.com/JohnAment/status/9627917698" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">9627917698</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9669975432',
    created: 1267173827000,
    type: 'post',
    text: 'Check out <a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a>\'s article on JSF 2 behaviors <a href="http://java.dzone.com/articles/jsf-2-client-behaviors" rel="noopener noreferrer" target="_blank">java.dzone.com/articles/jsf-2-client-behaviors</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9669151748',
    created: 1267171801000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> Thanks for the revisions! I\'ve committed both changes to SVN. I\'ll see if I can get a hot update of the online docs.<br><br>In reply to: <a href="https://x.com/arungupta/status/9652286337" rel="noopener noreferrer" target="_blank">@arungupta</a> <span class="status">9652286337</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9668827159',
    created: 1267171015000,
    type: 'post',
    text: 'The US is 2 medals away from most US medals ever in a Winter Olympics, and is on par to beat Germany as the medal leader for the 1st time.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9649374817',
    created: 1267141210000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Ah! Good suggestion! I didn\'t think of that. Good think I brought my key ;)<br><br>In reply to: <a href="https://x.com/jasondlee/status/9649107146" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">9649107146</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9648791589',
    created: 1267140344000,
    type: 'post',
    text: 'WTF <a class="mention" href="https://x.com/NBCOlympics" rel="noopener noreferrer" target="_blank">@NBCOlympics</a>. I can\'t watch the Olympic recap videos while I\'m out of the US. That\'s the one time I actually need the online content!',
    likes: 0,
    retweets: 0
  },
  {
    id: '9647516337',
    created: 1267138467000,
    type: 'post',
    text: 'I may not be able to speak the language, but I\'m looking around Austria and seeing the faces of my family members. Amazing. #ancestry',
    likes: 0,
    retweets: 0,
    tags: ['ancestry']
  },
  {
    id: '9647433343',
    created: 1267138342000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Seriously, ~95% of the time people write enterprise Java code, it\'s a regular top-level public class. i.e, not weird ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '9647377123',
    created: 1267138256000,
    type: 'post',
    text: 'Audio transcript from last #jsf EG meeting: <a href="https://javaserverfaces-spec-public.dev.java.net/files/documents/1936/148259/20100224-jsf-eg-audio.mp3" rel="noopener noreferrer" target="_blank">javaserverfaces-spec-public.dev.java.net/files/documents/1936/148259/20100224-jsf-eg-audio.mp3</a> It\'s a bit dry. Pressing to get past 2.0 Rev a.',
    likes: 0,
    retweets: 0,
    tags: ['jsf']
  },
  {
    id: '9646940102',
    created: 1267137596000,
    type: 'post',
    text: 'Had productive #jsf EG meeting. The list for JSF 2.0 Rev A is mostly complete, but caught some overlooked open issues filed against 1.2.',
    likes: 0,
    retweets: 0,
    tags: ['jsf']
  },
  {
    id: '9646845353',
    created: 1267137452000,
    type: 'post',
    text: 'Again enjoyed excellent conference w/ #jsf community @mwessendorf <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> <a class="mention" href="https://x.com/edburns" rel="noopener noreferrer" target="_blank">@edburns</a> <a class="mention" href="https://x.com/cagataycivici" rel="noopener noreferrer" target="_blank">@cagataycivici</a> <a class="mention" href="https://x.com/maxkatz" rel="noopener noreferrer" target="_blank">@maxkatz</a> M.Marinschek + more',
    likes: 0,
    retweets: 0,
    tags: ['jsf']
  },
  {
    id: '9646569518',
    created: 1267137037000,
    type: 'post',
    text: 'My body never recovered from the transition across the pond that divides the west (Atlantic ;)). First time w/ that problem. Dragging ass.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9646470465',
    created: 1267136889000,
    type: 'post',
    text: 'Managed to lay down a solid #cdi presentation at #jsfdays, despite setbacks of jetlag, laptop H20 carnage and bouts with OO.org Impress.',
    likes: 0,
    retweets: 0,
    tags: ['cdi', 'jsfdays']
  },
  {
    id: '9569864609',
    created: 1267005711000,
    type: 'post',
    text: 'JSF EG meeting tonight is at Hotel Novotel City Wien at 20:30.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9566254050',
    created: 1266996115000,
    type: 'post',
    text: 'I\'ve got to get cracking in preparation for my #cdi talks later today.',
    likes: 0,
    retweets: 0,
    tags: ['cdi']
  },
  {
    id: '9566228596',
    created: 1266996050000,
    type: 'post',
    text: 'Had to miss Arquillian design meeting yesterday. Can\'t wait to catch up on the log: <a href="http://community.jboss.org/wiki/ArquillianMeetingLog20100223" rel="noopener noreferrer" target="_blank">community.jboss.org/wiki/ArquillianMeetingLog20100223</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9566010728',
    created: 1266995502000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Jon_E" rel="noopener noreferrer" target="_blank">@Jon_E</a> Nope, because it was sung in the native language for the venue ;) Only we were clueless. But the story was in the faces &amp; gestures.<br><br>In reply to: <a href="https://x.com/Jon_E/status/9550729234" rel="noopener noreferrer" target="_blank">@Jon_E</a> <span class="status">9550729234</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9565924160',
    created: 1266995283000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> I used Serna for a while, quite good. Problem I have w/ all docbook tools is that *I* can\'t control XML formatting. No deal.<br><br>In reply to: <a href="https://x.com/jeremynorris/status/9556542112" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">9556542112</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9550509992',
    created: 1266970860000,
    type: 'post',
    text: 'Just got back from my first Opera, The Barber of Seville, with my wife and <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a>. Didn\'t understand a word, but loved every minute.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9550454544',
    created: 1266970781000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> The JBoss.org design team put together several nice docbook templates and docbook has worked well for me. I use vim.<br><br>In reply to: <a href="https://x.com/jeremynorris/status/9532237882" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">9532237882</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9526346326',
    created: 1266933332000,
    type: 'post',
    text: 'We really need to rethink about our position/support for HTML 5 in JSF 2.1 because it is picking up steam. New renderkit? #jsf2next',
    likes: 0,
    retweets: 1,
    tags: ['jsf2next']
  },
  {
    id: '9525981745',
    created: 1266932726000,
    type: 'post',
    text: 'I hate that Google tries to give me the non-english site when in Europe. Hello, look at my preferred language headers!',
    likes: 0,
    retweets: 0
  },
  {
    id: '9525534293',
    created: 1266931962000,
    type: 'post',
    text: 'In <a class="mention" href="https://x.com/Stephan007" rel="noopener noreferrer" target="_blank">@Stephan007</a>\'s talk on parleys.com. Cool app and content delivery platform. Learned that AIR 2 will run on #android. Hmmm.',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '9522995924',
    created: 1266927188000,
    type: 'post',
    text: 'JSF EG meeting tomorrow (2/24 19:30 UTC) to finalize JSF 2.0 Rev a list. #jsf2next',
    likes: 0,
    retweets: 0,
    tags: ['jsf2next']
  },
  {
    id: '9520718637',
    created: 1266921836000,
    type: 'post',
    text: 'Adam Bien: "In all of my projects with performance problems, it was the database, never the EJBs" #jsfdays',
    likes: 0,
    retweets: 0,
    tags: ['jsfdays']
  },
  {
    id: '9519839686',
    created: 1266919436000,
    type: 'post',
    text: 'Adam Bien: "CDI, the content is simpler than the title." #jsfdays',
    likes: 0,
    retweets: 0,
    tags: ['jsfdays']
  },
  {
    id: '9519533614',
    created: 1266918606000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Converting slides is still on my roadmap :(<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/9513341958" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">9513341958</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9519290341',
    created: 1266917921000,
    type: 'post',
    text: 'Watching Adam Bien speak about Java EE 6 while keeping us thoroughly entertained w/ embedded comedy routine.',
    photos: ['<div class="entry"><img class="photo" src="media/9519290341.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '9498653883',
    created: 1266880812000,
    type: 'post',
    text: 'There is a sufficient dirth of English in this county. Hmm, shapes and colors will have to do.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9498568934',
    created: 1266880689000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/mojavelinux" rel="noopener noreferrer" target="_blank">@mojavelinux</a> (That\'s the short circuit safety cut off I\'m referring to). The biggest problem is missed op for talk prep.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9498055500',
    created: 1266879949000,
    type: 'post',
    text: 'Water spilled and made its way under my laptop, triggered GFC and shutdown computer. Stability still unknown.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9477210041',
    created: 1266847680000,
    type: 'post',
    text: 'I received word I\'m a hero in another dimension. Who knew? Hope I\'m holding it together over there.',
    photos: ['<div class="entry"><img class="photo" src="media/9477210041.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '9448169416',
    created: 1266792342000,
    type: 'post',
    text: 'Austrian Airlines is very colorful. I feel like we are in romper room ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '9445705554',
    created: 1266788389000,
    type: 'post',
    text: 'On my way to Vienna for #JSFDays. Got my entourage in tow.',
    likes: 0,
    retweets: 0,
    tags: ['jsfdays']
  },
  {
    id: '9445587851',
    created: 1266788195000,
    type: 'post',
    text: 'What Austrian beers should I look for in Vienna? I\'m talking quality stuff that rivals the Belgians (in the current spirit of competition).',
    likes: 0,
    retweets: 0
  },
  {
    id: '9445514443',
    created: 1266788072000,
    type: 'post',
    text: 'Weld 1.0.1 final is currently being released!',
    likes: 1,
    retweets: 0
  },
  {
    id: '9434357437',
    created: 1266768847000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> Haha. Good one. Hey, you never know, music seems to work for everyone else ;)<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/9431566710" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">9431566710</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9422209328',
    created: 1266741704000,
    type: 'post',
    text: 'I\'d like to est. a mission statement &amp; code of conduct for jboss.org <a href="https://jira.jboss.org/jira/browse/CONTENT-15" rel="noopener noreferrer" target="_blank">jira.jboss.org/jira/browse/CONTENT-15</a> <a href="https://jira.jboss.org/jira/browse/CONTENT-16" rel="noopener noreferrer" target="_blank">jira.jboss.org/jira/browse/CONTENT-16</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9398187006',
    created: 1266698502000,
    type: 'post',
    text: 'Unified EL JSR in the pipeline: <a href="http://in.relation.to/Bloggers/UnifiedELJSRInThePipeline" rel="noopener noreferrer" target="_blank">in.relation.to/Bloggers/UnifiedELJSRInThePipeline</a> #elnext',
    likes: 0,
    retweets: 1,
    tags: ['elnext']
  },
  {
    id: '9397009431',
    created: 1266696381000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> Here is the open #elnext mailinglist: <a href="https://uel.dev.java.net/servlets/SummarizeList?listName=el-next" rel="noopener noreferrer" target="_blank">uel.dev.java.net/servlets/SummarizeList?listName=el-next</a> Kin-man is planning on submitting a JSR soon.<br><br>In reply to: <a href="https://x.com/aschwart/status/9380997411" rel="noopener noreferrer" target="_blank">@aschwart</a> <span class="status">9380997411</span>',
    likes: 0,
    retweets: 0,
    tags: ['elnext']
  },
  {
    id: '9394259637',
    created: 1266691530000,
    type: 'post',
    text: 'Got a question about running JSF on JBoss AS? As it in the new dedicated discussion forums: <a href="http://community.jboss.org/en/jbossas/jsf?view=discussions" rel="noopener noreferrer" target="_blank">community.jboss.org/en/jbossas/jsf?view=discussions</a> #jsf2',
    likes: 0,
    retweets: 0,
    tags: ['jsf2']
  },
  {
    id: '9361291259',
    created: 1266627290000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/NBCOlympics" rel="noopener noreferrer" target="_blank">@NBCOlympics</a> Plus, Plushenko arrived on the ice without taking is V8 because all of his jumps were sideways.<br><br>In reply to: <a href="https://x.com/NBCOlympics/status/9357220131" rel="noopener noreferrer" target="_blank">@NBCOlympics</a> <span class="status">9357220131</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9361265959',
    created: 1266627250000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/NBCOlympics" rel="noopener noreferrer" target="_blank">@NBCOlympics</a> I think Plushenko is a sore loser. Just like the S. Korean short track skater complaining about Apolo\'s medal.<br><br>In reply to: <a href="https://x.com/NBCOlympics/status/9357220131" rel="noopener noreferrer" target="_blank">@NBCOlympics</a> <span class="status">9357220131</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9324886220',
    created: 1266564257000,
    type: 'post',
    text: 'Evan Lysacek won figure skating gold over Plushenko. And he won because it just laid it down. I\'ve never seen such perfection. #olympics',
    likes: 0,
    retweets: 0,
    tags: ['olympics']
  },
  {
    id: '9317555404',
    created: 1266550903000,
    type: 'post',
    text: 'jcp.org is slow in changing references from Sun to Oracle. Just shows how difficult it is to get that site updated, even internally.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9301009762',
    created: 1266527061000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/richsharples" rel="noopener noreferrer" target="_blank">@richsharples</a> Welcome back to the hamster wheel.<br><br>In reply to: <a href="https://x.com/richsharples/status/9300084787" rel="noopener noreferrer" target="_blank">@richsharples</a> <span class="status">9300084787</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9300215242',
    created: 1266525827000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Indeed.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/9294191109" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">9294191109</span>',
    photos: ['<div class="entry"><img class="photo" src="media/9300215242.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '9299834619',
    created: 1266525222000,
    type: 'post',
    text: 'Just realized that Brisbane &amp; Sydney are in different timezones, off by 1H. Thought I had a timezone bug in my calendar. It was in my head.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9295612756',
    created: 1266518484000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> I have really liked Fidelity, but then again, I had nothing before that so maybe I\'m just naive ;)<br><br>In reply to: <a href="https://x.com/lincolnthree/status/9295520990" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">9295520990</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9294538167',
    created: 1266516808000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> Part of inbox 0 is admitting you can\'t do everything. You\'re going to throw out lots. Archive it all and you\'ll be in hot water ;)<br><br>In reply to: <a href="https://x.com/aschwart/status/9293388600" rel="noopener noreferrer" target="_blank">@aschwart</a> <span class="status">9293388600</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9291788379',
    created: 1266512619000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Actually, believe it or not, it\'s the days that I\'m hitting my e-mail hardest that I end up with more :( Negative progress.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/9291503793" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">9291503793</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9291403298',
    created: 1266512071000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Man, I haven\'t seen inbox 0 in almost a year. I\'m really starting to long for it :(<br><br>In reply to: <a href="https://x.com/lincolnthree/status/9290925802" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">9290925802</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9291383730',
    created: 1266512043000,
    type: 'post',
    text: 'Just to give you an idea of what #Arquillian can do for you: <a href="http://community.jboss.org/message/526886" rel="noopener noreferrer" target="_blank">community.jboss.org/message/526886</a>',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '9290023274',
    created: 1266510068000,
    type: 'post',
    text: 'The release of Weld 1.0.1 is being delayed until Monday to nail down the cause of some test failures in the GlassFish/Weld integration.',
    likes: 0,
    retweets: 1
  },
  {
    id: '9271876895',
    created: 1266473049000,
    type: 'post',
    text: 'My wife is in love with Shaun White\'s hair. Shuan, can I borrow it? It\'s too much to stuff into that helmet anyway ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '9271813022',
    created: 1266472927000,
    type: 'post',
    text: 'UI makeover, OpenOffice edition. [Before] Edit &gt; Changes... &gt; Show [After] View &gt; Changes. At least put the option in both places.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9270109311',
    created: 1266469785000,
    type: 'post',
    text: 'I realized that next week, I\'m going to be the black sheep in the crowd rooting for the Americans from Vienna, Austria ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '9270053622',
    created: 1266469686000,
    type: 'post',
    text: 'Theme of the day at the #olympics. Stayed on your feet, you were in the running for a medal.',
    likes: 0,
    retweets: 0,
    tags: ['olympics']
  },
  {
    id: '9270007754',
    created: 1266469606000,
    type: 'post',
    text: 'Shaun White just dominated the competition tonight in the #snowboarding halfpipe to take gold. You needed a telescope to see his tricks.',
    likes: 0,
    retweets: 0,
    tags: ['snowboarding']
  },
  {
    id: '9250720353',
    created: 1266440116000,
    type: 'post',
    text: 'I like the new organization of the JBoss AS downloads page: <a href="http://www.jboss.org/jbossas/downloads.html" rel="noopener noreferrer" target="_blank">www.jboss.org/jbossas/downloads.html</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9247988186',
    created: 1266435708000,
    type: 'post',
    text: 'I LOVE the linking feature on jboss.org. It truly embraces the essence of the web =&gt; hyperlinks.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9247838216',
    created: 1266435470000,
    type: 'post',
    text: 'Have you encountered a problem using jboss.org. Did you know there is an issue tracker for the site? <a href="https://jira.jboss.org/jira/browse/ORG" rel="noopener noreferrer" target="_blank">jira.jboss.org/jira/browse/ORG</a>',
    likes: 0,
    retweets: 4
  },
  {
    id: '9247675706',
    created: 1266435218000,
    type: 'post',
    text: 'We are working on laying down the mission for #Arquillian. <a href="http://community.jboss.org/thread/148245" rel="noopener noreferrer" target="_blank">community.jboss.org/thread/148245</a> Find out what it\'s all about.',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '9245150787',
    created: 1266431235000,
    type: 'post',
    text: 'I\'m going to be speaking about #cdi, #weld, #seam at DevNexus Atlanta in March: <a href="http://www.devnexus.com/site/speakers#Dan_Allen" rel="noopener noreferrer" target="_blank">www.devnexus.com/site/speakers#Dan_Allen</a>',
    likes: 0,
    retweets: 0,
    tags: ['cdi', 'weld', 'seam']
  },
  {
    id: '9245046695',
    created: 1266431069000,
    type: 'post',
    text: 'What is with this conference title? JavaOne + Develop I don\'t get it, nor does it flow. I\'m just calling it #JavaOne, or J1 for short.',
    likes: 0,
    retweets: 1,
    tags: ['javaone']
  },
  {
    id: '9244803121',
    created: 1266430684000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gmaggess" rel="noopener noreferrer" target="_blank">@gmaggess</a> JavaOne is definitely on the books for me, assuming I get that all important talk accepted ;)<br><br>In reply to: <a href="https://x.com/gmaggess/status/9205164727" rel="noopener noreferrer" target="_blank">@gmaggess</a> <span class="status">9205164727</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9244715992',
    created: 1266430547000,
    type: 'post',
    text: 'I can\'t wait to watch Shaun White and co. shred the pipe tonight in #olympic #snowboarding.',
    likes: 0,
    retweets: 1,
    tags: ['olympic', 'snowboarding']
  },
  {
    id: '9223325276',
    created: 1266388685000,
    type: 'post',
    text: 'In contrast, it\'s super simple to rebook a flight on SouthWest.com. #timesaver',
    likes: 0,
    retweets: 0,
    tags: ['timesaver']
  },
  {
    id: '9223031708',
    created: 1266388004000,
    type: 'post',
    text: 'I was playing around in the Oakley store, found out I really like the Antix lifestyle glasses =&gt; #wishlist',
    photos: ['<div class="entry"><img class="photo" src="media/9223031708.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['wishlist']
  },
  {
    id: '9220778199',
    created: 1266383379000,
    type: 'post',
    text: 'First round of feedback on #Arquillian logos has been sent to the jboss.org design team. We\'ll be back with more options next week.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '9209493676',
    created: 1266366080000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> We are talking the spec. Mojarra major/minor versions align with the spec version, but point release not directly related.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/9206876025" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">9206876025</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9206792128',
    created: 1266361864000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> JSF 2.0 Rev a is just bug fixes, clarifications and minor features if critical. JSF 2.1 is all about features &amp; enhancements.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/9206239696" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">9206239696</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9206638964',
    created: 1266361623000,
    type: 'reply',
    text: '@brianleathem The closest thing we have so far are the Weld Maven archetypes. Also look to ClickFrames to migrate to #javaee6 soon.<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['javaee6']
  },
  {
    id: '9206261095',
    created: 1266361047000,
    type: 'post',
    text: 'I wish you could exclude twitter handles from search results so you can weed out crap you know you don\'t want to see.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9206162148',
    created: 1266360889000,
    type: 'post',
    text: 'JSF 2.0 Rev a is going to have a ton of fixes and clarifications. I\'m really optimistic about it. Now, it just needs to happen ;) #jsf2',
    likes: 0,
    retweets: 0,
    tags: ['jsf2']
  },
  {
    id: '9205659439',
    created: 1266360090000,
    type: 'post',
    text: 'Finished editing <a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a>\'s article on #jsf2 component behaviors for DZone last night and wrote the forward today. Look for soon!',
    likes: 0,
    retweets: 0,
    tags: ['jsf2']
  },
  {
    id: '9204975669',
    created: 1266359019000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Haha. She was just joking. I was supposed to be in Hartford for the CTJUG but it got snowed out. So now I\'m home for dinner ;)<br><br>In reply to: <a href="https://x.com/lightguardjp/status/9201796593" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">9201796593</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9200445218',
    created: 1266352231000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> I guess there isn\'t room in the house for me and the roses. What was to be a reminder is now an opponent ;)<br><br>In reply to: <a href="https://x.com/aalmiray/status/9199126137" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">9199126137</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9199040011',
    created: 1266350086000,
    type: 'post',
    text: 'My wife just said to me "You messed up my dinner plans because you are here." No feeling the love :(',
    likes: 0,
    retweets: 0
  },
  {
    id: '9197885893',
    created: 1266348187000,
    type: 'post',
    text: 'Not being able to edit the original message on reply in the Gmail Android client is as sore a spot as lack of Eclipse split file editor.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9187722686',
    created: 1266332097000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Just an awesome community member ;)<br><br>In reply to: <a href="https://x.com/lightguardjp/status/9187402771" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">9187402771</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9187276387',
    created: 1266331399000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Woot! Good call! #Arquillian to the rescue.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/9184918370" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">9184918370</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '9187195548',
    created: 1266331270000,
    type: 'post',
    text: 'TripIt really gets tripped up when you need to reschedule a trip. You have to edit every single plan in the whole trip one-by-one. Yuck.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9186379714',
    created: 1266329959000,
    type: 'post',
    text: 'My trip to CTJUG today has been cancelled. I\'m already rescheduled and rebooked for the Apr 20th meeting. See you then!',
    likes: 0,
    retweets: 0
  },
  {
    id: '9176731153',
    created: 1266307122000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> With snowboarding, it goes from easy to painful REAL fast. As soon as that edge catches, good night.<br><br>In reply to: <a href="https://x.com/maxandersen/status/9135228937" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">9135228937</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9176571147',
    created: 1266306678000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> If you can provide a Maven project to test before/after, then file an issue and we\'ll look at it.<br><br>In reply to: <a href="https://x.com/jasondlee/status/9172252572" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">9172252572</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9176504452',
    created: 1266306497000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Awesome!<br><br>In reply to: <a href="https://x.com/lincolnthree/status/9167359002" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">9167359002</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9175965527',
    created: 1266305051000,
    type: 'post',
    text: 'OpenOffice 3 no longer ignores my request to "Export notes" when creating a PDF, finally.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9173230304',
    created: 1266298824000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> Truthfully, the best Java conferences are in Europe: Devoxx, Jazoon, JavaZone, JAX, jFocus, etc. Come on USA!<br><br>In reply to: <a href="https://x.com/jeremynorris/status/9172082927" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">9172082927</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9168957192',
    created: 1266291408000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gmaggess" rel="noopener noreferrer" target="_blank">@gmaggess</a> Nope, TSSJS is one conference I will not be attending this conference season.<br><br>In reply to: <a href="https://x.com/gmaggess/status/9159907570" rel="noopener noreferrer" target="_blank">@gmaggess</a> <span class="status">9159907570</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9168942289',
    created: 1266291384000,
    type: 'post',
    text: 'It\'s amazing the effect twitter, etc is having on #olympic announcers; finally shutting up and letting us watch the performances in peace.',
    likes: 0,
    retweets: 0,
    tags: ['olympic']
  },
  {
    id: '9165575082',
    created: 1266286066000,
    type: 'post',
    text: 'After weighing a lot of permutations, I finally settled on this Sessions #snowboarding jacket and pants pairing.',
    photos: ['<div class="entry"><img class="photo" src="media/9165575082.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['snowboarding']
  },
  {
    id: '9157391892',
    created: 1266270405000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Clearly the writer does not understand the distinction between implementation and reference implementation.<br><br>In reply to: <a href="https://x.com/jasondlee/status/9157310478" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">9157310478</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9157354696',
    created: 1266270340000,
    type: 'post',
    text: 'Just wrapped up the #Arquillian Kickoff Design meeting. Chat log in techicolor <a href="http://community.jboss.org/wiki/ArquillianMeetingLog20100215" rel="noopener noreferrer" target="_blank">community.jboss.org/wiki/ArquillianMeetingLog20100215</a>',
    likes: 0,
    retweets: 2,
    tags: ['arquillian']
  },
  {
    id: '9150841138',
    created: 1266258826000,
    type: 'post',
    text: '#Arquillian Design Kickoff about to start: <a href="http://community.jboss.org/wiki/ArquillianMeetingInformation" rel="noopener noreferrer" target="_blank">community.jboss.org/wiki/ArquillianMeetingInformation</a>',
    likes: 0,
    retweets: 2,
    tags: ['arquillian']
  },
  {
    id: '9150553894',
    created: 1266258340000,
    type: 'post',
    text: 'We really want to get your feedback on what direction to take w/ the #arquillian logo. <a href="http://spreadsheets.google.com/viewform?formkey=dHplX3FyVVNUVmtLTkd5dVB4Y0ZQM2c6MA" rel="noopener noreferrer" target="_blank">spreadsheets.google.com/viewform?formkey=dHplX3FyVVNUVmtLTkd5dVB4Y0ZQM2c6MA</a>',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '9150278566',
    created: 1266257876000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> Ah! Well, it\'s still Google\'s voice :)<br><br>In reply to: <a href="https://x.com/rruss/status/9148265180" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">9148265180</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9148168167',
    created: 1266254307000,
    type: 'post',
    text: 'The Google Calendar error pages uses an expression that my buddy and former colleague always used to say.',
    photos: ['<div class="entry"><img class="photo" src="media/9148168167.png"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '9127877632',
    created: 1266210195000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/tmobile_usa" rel="noopener noreferrer" target="_blank">@tmobile_usa</a> is doing a shitty job of communicating with customers about the upgrade to Android 2.1. I shouldn\'t have to rely on rumors.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9127670044',
    created: 1266209800000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dfab_con" rel="noopener noreferrer" target="_blank">@dfab_con</a> The t-mobile customer service rep did mention they were adopting Android 2.1, but didn\'t say when, which is what irritates me.<br><br>In reply to: <a href="https://x.com/dfabu/status/9126068565" rel="noopener noreferrer" target="_blank">@dfabu</a> <span class="status">9126068565</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9127484118',
    created: 1266209426000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dfab_con" rel="noopener noreferrer" target="_blank">@dfab_con</a> I heard it on Twitter. Therefore, it has to be true ;)<br><br>In reply to: <a href="https://x.com/dfabu/status/9126068565" rel="noopener noreferrer" target="_blank">@dfabu</a> <span class="status">9126068565</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9127355573',
    created: 1266209176000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I didn\'t mean to reply sharply to your Nexus One suggestion. Just irritated about early Android adopters getting the shaft.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/9118309784" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">9118309784</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9127090055',
    created: 1266208715000,
    type: 'post',
    text: 'I wonder how many times #olympics will trend in twitter in the next two weeks. Who knows, but it will be fun to follow along ;)',
    likes: 0,
    retweets: 0,
    tags: ['olympics']
  },
  {
    id: '9125438241',
    created: 1266205780000,
    type: 'post',
    text: 'I really dig the music skated to by Dube and Davison. Lots of talk about it on twitter, where I found out it\'s called Lux Aeterna.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9119359392',
    created: 1266195395000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I won\'t buy a NexusOne b/c I don\'t have tons of money to burn on ridiculous phone prices. My G1 came cheap with a plan.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/9118309784" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">9118309784</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9119323876',
    created: 1266195328000,
    type: 'post',
    text: 'Finally an answer! T-mobile customers must wait for #android 2.1 to accomodate the G1 limits. I can wait. And no, I won\'t buy a NexusOne.',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '9117835661',
    created: 1266192520000,
    type: 'post',
    text: 'I\'m going to leave t-mobile if they don\'t give a straight answer about when upgrade to #android 2.0 will happen. I\'m sick of the runaround.',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '9113937815',
    created: 1266185115000,
    type: 'post',
    text: 'People of Maryland. Please take a humongous chill pill (you know who you are). This is not the apocylapse.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9111985569',
    created: 1266181341000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Kin-man will likely lead. Info to come.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/9104028081" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">9104028081</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9089678071',
    created: 1266131902000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> At ease. My statements are licensed under the Creative Commons (Attribution), unless otherwise stated. Free the knowledge ;)<br><br>In reply to: <a href="https://x.com/ALRubinger/status/9018398035" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">9018398035</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '9089543894',
    created: 1266131565000,
    type: 'post',
    text: 'For those that don\'t know, the Unified EL is currently stuck inside of the JSP 2.x specification (JSR-245).',
    likes: 0,
    retweets: 0
  },
  {
    id: '9089532267',
    created: 1266131535000,
    type: 'post',
    text: 'Just in! Unified EL is finally going to be spun off as its own JSR! The goal is to make EL a 1st class citizen and improve ease of use.',
    likes: 0,
    retweets: 2
  },
  {
    id: '9089359042',
    created: 1266131104000,
    type: 'post',
    text: 'TwitterRide, my twitter client of choice on Android, just got a big upgrade. It\'s now called TweetsRide, though I still don\'t get the name.',
    likes: 0,
    retweets: 0
  },
  {
    id: '9089328459',
    created: 1266131029000,
    type: 'post',
    text: 'I\'m so stoked. On a second attempt on Friday, I landed my first backside 360 off of a kicker (jump). I went for it big and got big air.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8996447450',
    created: 1265952970000,
    type: 'post',
    text: 'Wow. Long day. I\'m worn out. Tomorrow I get to have fun. I\'m swapping Friday and Saturday so that I can go take advantage of this snow ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '8980302835',
    created: 1265926662000,
    type: 'post',
    text: 'I just created my first form using Google Docs. Ridiculously easy. Wow.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8980066009',
    created: 1265926278000,
    type: 'post',
    text: 'Be part of the design process for the #Arquillian logo: <a href="http://community.jboss.org/message/525921" rel="noopener noreferrer" target="_blank">community.jboss.org/message/525921</a>',
    likes: 0,
    retweets: 2,
    tags: ['arquillian']
  },
  {
    id: '8975671673',
    created: 1265919025000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> I use HTML. I prefer HTML in general because it doesn\'t do strange things with wrapping.<br><br>In reply to: <a href="https://x.com/rruss/status/8974814281" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">8974814281</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8974397458',
    created: 1265916852000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/unitygirl" rel="noopener noreferrer" target="_blank">@unitygirl</a> Congrats!<br><br>In reply to: <a href="https://x.com/wow3community/status/8971508922" rel="noopener noreferrer" target="_blank">@wow3community</a> <span class="status">8971508922</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8974232130',
    created: 1265916579000,
    type: 'post',
    text: 'The saga with EL continues: <a href="http://community.jboss.org/thread/148045" rel="noopener noreferrer" target="_blank">community.jboss.org/thread/148045</a> #arquillian',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '8949722258',
    created: 1265868400000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jonobacon" rel="noopener noreferrer" target="_blank">@jonobacon</a> Yeah. Even Gregory House shows enough love to be redeemable. Heroes must have flaws, but they also must be human(ish).<br><br>In reply to: <a href="https://x.com/jonobacon/status/8948913907" rel="noopener noreferrer" target="_blank">@jonobacon</a> <span class="status">8948913907</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8925649811',
    created: 1265846384000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> You could car pool w/ <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a>.<br><br>In reply to: <a href="https://x.com/aschwart/status/8924036778" rel="noopener noreferrer" target="_blank">@aschwart</a> <span class="status">8924036778</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8922826940',
    created: 1265842066000,
    type: 'post',
    text: 'I\'ll be speaking at the Connecticut JUG on Tuesday, Feb 16. <a href="http://www.ctjava.org/" rel="noopener noreferrer" target="_blank">www.ctjava.org/</a>',
    likes: 0,
    retweets: 1
  },
  {
    id: '8921633788',
    created: 1265840228000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/javier_castanon" rel="noopener noreferrer" target="_blank">@javier_castanon</a> I retweeted with Metawidget in mind: <a href="http://metawidget.org" rel="noopener noreferrer" target="_blank">metawidget.org</a>. Generated code should be transient (less to maintain).<br><br>In reply to: <a href="https://x.com/javier_castanon/status/8920219637" rel="noopener noreferrer" target="_blank">@javier_castanon</a> <span class="status">8920219637</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8917126588',
    created: 1265833200000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> I think you mean by David Allen: <a href="https://www.audible.com/pd/Getting-Things-Done-Audiobook/B002VA8IQK?productID=BK_SANS_000347" rel="noopener noreferrer" target="_blank">www.audible.com/pd/Getting-Things-Done-Audiobook/B002VA8IQK?productID=BK_SANS_000347</a> If you followed my approach, you would be in hot water.<br><br>In reply to: <a href="https://x.com/maxandersen/status/8916885669" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">8916885669</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8916683617',
    created: 1265832504000,
    type: 'post',
    text: 'Zimbra also top-posts by default, which is grounds for getting me killed. <a href="http://www.catb.org/jargon/html/T/top-post.html" rel="noopener noreferrer" target="_blank">www.catb.org/jargon/html/T/top-post.html</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8916578238',
    created: 1265832339000,
    type: 'post',
    text: 'Zimbra has the absolute worst rich text editor. It destroys the formatting of the original message and quotes in the most bonehead way. Grr!',
    likes: 0,
    retweets: 0
  },
  {
    id: '8914532126',
    created: 1265829162000,
    type: 'post',
    text: '@stmaryscountymd "School\'s out for Winter"',
    likes: 0,
    retweets: 0
  },
  {
    id: '8914444444',
    created: 1265829024000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/apemberton" rel="noopener noreferrer" target="_blank">@apemberton</a> Yes, tiny_mce3 it appears. If there is any info or code you could provide, we would really appreciate it. Add comment to JIRA?<br><br>In reply to: <a href="https://x.com/apemberton/status/8903041588" rel="noopener noreferrer" target="_blank">@apemberton</a> <span class="status">8903041588</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8901228759',
    created: 1265808676000,
    type: 'post',
    text: '#weld 1.0.1-CR2 is now available for final inspection! <a href="https://in.relation.to/2010/02/10/weld-101-cr-2-is-available-for-final-inspection/" rel="noopener noreferrer" target="_blank">in.relation.to/2010/02/10/weld-101-cr-2-is-available-for-final-inspection/</a>',
    likes: 0,
    retweets: 2,
    tags: ['weld']
  },
  {
    id: '8894583459',
    created: 1265792419000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> Amen. We are on the same page there. I want a plain HTML textarea if I\'m doing wiki markup.<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/8894006268" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">8894006268</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8893966902',
    created: 1265790678000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> Dude, that wiki edit mode is completely unusable IMO. Having to type inside of a quote block is totally awkward.<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/8893861140" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">8893861140</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8892717482',
    created: 1265787233000,
    type: 'post',
    text: 'Please vote on this jboss.org issue to allow quoted text to be broken up in Clearspace. <a href="https://jira.jboss.org/jira/browse/ORG-466" rel="noopener noreferrer" target="_blank">jira.jboss.org/jira/browse/ORG-466</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8891661192',
    created: 1265784446000,
    type: 'post',
    text: 'I\'ve wasted more than an hour just trying to get Eclipse to open the #arquillian projects because it\'s unhappy w/ generics. Hi NetBeans.',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '8889002837',
    created: 1265778526000,
    type: 'post',
    text: 'The governance of the Maven Central repository is getting more strict. If you have legacy publishing scripts, take note!',
    likes: 0,
    retweets: 0
  },
  {
    id: '8887419619',
    created: 1265775648000,
    type: 'post',
    text: 'Open question. Which is the correct term for the embedded version of GlassFish? Embedded GlassFish V3 or GlassFish Embedded V3?',
    likes: 0,
    retweets: 0
  },
  {
    id: '8885453284',
    created: 1265772582000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> It\'s simple because it\'s consistent w/ what is being put in the Maven repository. Btw, that should have been 1.0.1-CR2.<br><br>In reply to: <a href="https://x.com/maxandersen/status/8884294350" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">8884294350</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8885404899',
    created: 1265772507000,
    type: 'post',
    text: 'I\'m taking a more proactive approach with the snow on this go around.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8881536592',
    created: 1265766574000,
    type: 'post',
    text: 'I\'m deciding to do away with this ridiculous duality in version schemes for Weld =&gt; going w/ the Maven standard 1.0.1-SP2. Simple is good.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8877466620',
    created: 1265760239000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> You should let him know that he now has a RESTEasy space on jboss.org for user forums and (categorized) wiki pages.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/8877377799" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">8877377799</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8877336506',
    created: 1265760040000,
    type: 'post',
    text: 'I just confirmed I\'ll be speaking at the Northern Virginia Software Symposium, Apr 30 - May 2, 2010. #nfjs #arquillian #cdi',
    likes: 0,
    retweets: 0,
    tags: ['nfjs', 'arquillian', 'cdi']
  },
  {
    id: '8874615166',
    created: 1265755720000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> How about ZeroWasteFactory, GreenFactory, EcoFactory, ZeroSumFactory? Also Coop, Carousal, Interplay, Reciprocate, Wave, Cycle<br><br>In reply to: <a href="https://x.com/ALRubinger/status/8869456360" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">8869456360</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8867674117',
    created: 1265744193000,
    type: 'post',
    text: '#weld 1.0.1-CR2 is now in the Maven central repository. I\'m working on pushing out the distribution and doing a release announcement today.',
    likes: 0,
    retweets: 1,
    tags: ['weld']
  },
  {
    id: '8863718334',
    created: 1265737504000,
    type: 'reply',
    text: '@mwessendorf Same outlook here.<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8862996873',
    created: 1265736290000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> I should have prefixed it by saying I started with low expectations. It tackles what my Integra could only laugh at.<br><br>In reply to: @pelegri <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8844736905',
    created: 1265697794000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> I\'m goofy on many levels, not just in #snowboarding ;)<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['snowboarding']
  },
  {
    id: '8842221942',
    created: 1265692169000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Appreciate the concern. But my Prius has worked fine for close to three years. My configuration is stable ;)<br><br>In reply to: <a href="https://x.com/lightguardjp/status/8842047696" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">8842047696</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8842099461',
    created: 1265691936000,
    type: 'post',
    text: 'Perhaps I should have headed down to DC for free urban skiing/snowboarding instead of fighting the crowds at the resort.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8842008063',
    created: 1265691766000,
    type: 'post',
    text: 'I almost forgot. I landed my first 360 from the flats today. Finally! #snowboarding',
    likes: 0,
    retweets: 0,
    tags: ['snowboarding']
  },
  {
    id: '8841042528',
    created: 1265690009000,
    type: 'post',
    text: 'The JavaRanch book promo this week features Java AS 5 development <a href="http://www.coderanch.com/forums/f-63/JBoss" rel="noopener noreferrer" target="_blank">www.coderanch.com/forums/f-63/JBoss</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8840963569',
    created: 1265689869000,
    type: 'post',
    text: 'Oops, got my grabs mixed up. I think that is a method grab. Gotta get it sorted on my next outing ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '8840650197',
    created: 1265689318000,
    type: 'post',
    text: 'Those photos are courtesy of my personal paparazzi, Kari Greer. Props Kari! <a href="http://photos.kariphotos.com/Kellogg_Lookout_Silver-Mtn_2010/index.html" rel="noopener noreferrer" target="_blank">photos.kariphotos.com/Kellogg_Lookout_Silver-Mtn_2010/index.html</a> <a href="http://www.kariphotos.com" rel="noopener noreferrer" target="_blank">www.kariphotos.com</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8840544034',
    created: 1265689138000,
    type: 'post',
    text: 'Let\'s see that grab again! Throw it down.',
    photos: ['<div class="entry"><img class="photo" src="media/8840544034.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '8840495001',
    created: 1265689053000,
    type: 'post',
    text: 'My grand finale. Big air stalefish grab with a hand drag on the back end.',
    photos: ['<div class="entry"><img class="photo" src="media/8840495001.gif"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '8840445499',
    created: 1265688967000,
    type: 'post',
    text: 'Who needs slopes when you\'ve got so much air to ride?',
    photos: ['<div class="entry"><img class="photo" src="media/8840445499.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '8840414300',
    created: 1265688913000,
    type: 'post',
    text: 'Getting the hurricane started with a 180 off the jump.',
    photos: ['<div class="entry"><img class="photo" src="media/8840414300.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '8840354443',
    created: 1265688811000,
    type: 'post',
    text: 'Launching off to go ride some air.',
    photos: ['<div class="entry"><img class="photo" src="media/8840354443.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '8840305939',
    created: 1265688727000,
    type: 'post',
    text: 'Workin\' the wall.',
    photos: ['<div class="entry"><img class="photo" src="media/8840305939.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '8840189942',
    created: 1265688531000,
    type: 'post',
    text: 'I name this one the floor grab.',
    photos: ['<div class="entry"><img class="photo" src="media/8840189942.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '8840168815',
    created: 1265688496000,
    type: 'post',
    text: 'Silver Mountain looking very silvery on an early January morning.',
    photos: ['<div class="entry"><img class="photo" src="media/8840168815.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '8840139884',
    created: 1265688447000,
    type: 'post',
    text: 'I made a quick recovery and carried my wife "over the threshold" for the first time.',
    photos: ['<div class="entry"><img class="photo" src="media/8840139884.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '8840111969',
    created: 1265688399000,
    type: 'post',
    text: 'I confused my wife carrying contest with my wife dropping contest.',
    photos: ['<div class="entry"><img class="photo" src="media/8840111969.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '8838269250',
    created: 1265685397000,
    type: 'post',
    text: 'I love a good retweeting session after a long day out of the office. It\'s like doing a shot of 100 proof instead of 12 beers. Enjoy.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8837861134',
    created: 1265684744000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> Yeah, I was spinning an exaggerated statement for effect, but isn\'t quite playing right. I\'ll tweak to be fair to unit tests<br><br>In reply to: <a href="https://x.com/jeremynorris/status/8817363150" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">8817363150</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8837643926',
    created: 1265684410000,
    type: 'post',
    text: 'The snow at #whitetail today is buttery soft and super fun to ride. Crowds, however, not so soft or fun :( Hopefully less of crowd on Thurs',
    likes: 0,
    retweets: 0,
    tags: ['whitetail']
  },
  {
    id: '8837550236',
    created: 1265684261000,
    type: 'post',
    text: 'Despite horrendous road conditions, I continue to be impressed by how the Prius handles snow &amp; ice. I think it\'s sore now, though.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8837491878',
    created: 1265684165000,
    type: 'post',
    text: 'Just getting out of my community (aka demilitarized plowing zone) was a huge challenge this morning. Highways were shaky w/ missing lanes.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8797253418',
    created: 1265605592000,
    type: 'post',
    text: 'The first rough draft of the #arquillian reference guide is complete. <a href="http://community.jboss.org/message/524670#524670" rel="noopener noreferrer" target="_blank">community.jboss.org/message/524670#524670</a> Review at will ;)',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '8795982532',
    created: 1265602944000,
    type: 'post',
    text: 'Fellow coworker followers. I\'ll be out tomorrow celebrating #snowmaggedon slope style. I\'ll hit you up when I\'m back in the saddle.',
    likes: 0,
    retweets: 0,
    tags: ['snowmaggedon']
  },
  {
    id: '8795871110',
    created: 1265602725000,
    type: 'post',
    text: 'My vote for #superbowlad definitely goes to Doritos dog collar and screaming chickens. Also have a standing vote for the e-trade babies.',
    likes: 0,
    retweets: 0,
    tags: ['superbowlad']
  },
  {
    id: '8793706955',
    created: 1265598860000,
    type: 'post',
    text: 'No guts, no glory. Way to go #saints! An onside kick and an interception return for a TD is a win with style!',
    likes: 0,
    retweets: 0,
    tags: ['saints']
  },
  {
    id: '8785037508',
    created: 1265586763000,
    type: 'post',
    text: 'My neighbor shoveling his car. Not shoveling *out* his car, but shoveling the car itself.',
    photos: ['<div class="entry"><img class="photo" src="media/8785037508.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '8784970912',
    created: 1265586669000,
    type: 'post',
    text: 'Our street at sunset last night. The only thing that had been through was the police tank.',
    photos: ['<div class="entry"><img class="photo" src="media/8784970912.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '8784435294',
    created: 1265585928000,
    type: 'post',
    text: 'I finished digging out my car, just in time for kickoff. But now I\'m typing goofy. Good thing I worked on the documentation first.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8775535476',
    created: 1265570946000,
    type: 'post',
    text: 'Submitted a talk on #arquillian titled "Real Java EE testing with Arquillian" to <a class="mention" href="https://x.com/JAZOON" rel="noopener noreferrer" target="_blank">@JAZOON</a>. Aslak will lead the talk, I\'ll be his lab rat ;)',
    likes: 0,
    retweets: 1,
    tags: ['arquillian']
  },
  {
    id: '8773363548',
    created: 1265566975000,
    type: 'post',
    text: 'My neighbors are shoveling the street. Cabin fever?',
    likes: 0,
    retweets: 0
  },
  {
    id: '8766624183',
    created: 1265554428000,
    type: 'post',
    text: 'Ubuntu\'s issue tracking system has the feature "Does this bug affect you?" A good way to get an indication of how widespread it is.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8766568041',
    created: 1265554317000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Hahaha! Actually, I\'m just trying to pass the time until the game starts ;) Plus, I\'m playing hooky and going boarding tomorrow!<br><br>In reply to: <a href="https://x.com/ALRubinger/status/8766018700" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">8766018700</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8765964076',
    created: 1265553119000,
    type: 'post',
    text: 'Just saw a commercial that showcased a website better than I\'ve ever seen done. No cheesy mouse cursor! Used pinch zoom and flick scroll.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8765915657',
    created: 1265553025000,
    type: 'post',
    text: 'Up early, back to working on the reference guide for #arquillian.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '8765892391',
    created: 1265552982000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Cmoose" rel="noopener noreferrer" target="_blank">@Cmoose</a> Indeed, that is a great idea. Kind of along the same lines of recovering energy from braking in a car. #cleanenergy<br><br>In reply to: <a href="https://x.com/Cmoose/status/8765475719" rel="noopener noreferrer" target="_blank">@Cmoose</a> <span class="status">8765475719</span>',
    likes: 0,
    retweets: 0,
    tags: ['cleanenergy']
  },
  {
    id: '8740294707',
    created: 1265497942000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> I just finished trying to give myself a heart attack. I started cutting into my documentation-writing energy :(<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8740222129',
    created: 1265497805000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> Was just talking about that this morning. In engineering, we learn make it 2x as robust as necessary, software =&gt; just enough<br><br>In reply to: <a href="https://x.com/matthewmccull/status/8737655551" rel="noopener noreferrer" target="_blank">@matthewmccull</a> <span class="status">8737655551</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8735684589',
    created: 1265489000000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Buried. I\'ll post a pic soon when I try to tackle the first round of shoveling ;)<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8735626331',
    created: 1265488884000,
    type: 'post',
    text: 'Posted outline for the Arquillian User Guide, which I\'m working on this weekend: <a href="http://community.jboss.org/wiki/ArquillianUserGuideOutline" rel="noopener noreferrer" target="_blank">community.jboss.org/wiki/ArquillianUserGuideOutline</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8711155916',
    created: 1265434222000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Yep, we just need to get some things cleaned up.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/8707990920" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">8707990920</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8707981120',
    created: 1265428274000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> This weekend I\'ll get together a blog post on how to add an Arquillian test to a project created w/ a Weld archetype.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8707943382',
    created: 1265428209000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Arquillian does build. It\'s just that the demo tests aren\'t running successfully.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/8702938942" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">8702938942</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8702933314',
    created: 1265420057000,
    type: 'post',
    text: 'Kenai is not going away after all. It is simply becoming the infrastructure behind the new java.net. FTW! <a href="http://blogs.sun.com/projectkenai/entry/the_future_of_kenai_com" rel="noopener noreferrer" target="_blank">blogs.sun.com/projectkenai/entry/the_future_of_kenai_com</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8700096333',
    created: 1265415348000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> +1 It\'s been a great week. Nothing is better than ending a week hoping for the next week to start so you can get more shit done.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/8699643328" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">8699643328</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8697279281',
    created: 1265410588000,
    type: 'post',
    text: 'Before you take off for the weekend, remember Sunday, Feb 7 is the deadline for Jazoon\'s call for papers. <a href="http://jazoon.com" rel="noopener noreferrer" target="_blank">jazoon.com</a>',
    likes: 0,
    retweets: 1
  },
  {
    id: '8697155011',
    created: 1265410374000,
    type: 'post',
    text: 'RESTEasy has a space on jboss.org <a href="http://community.jboss.org/en/resteasy" rel="noopener noreferrer" target="_blank">community.jboss.org/en/resteasy</a> Wiki articles have been moved there. Also serves as user forums.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8696416706',
    created: 1265409125000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> And that\'s just the beginning ;) Arquillian is going to be the star of the next release.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/8695785768" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">8695785768</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8695110082',
    created: 1265406952000,
    type: 'post',
    text: '#weld archetypes cited as "a perfect case study of how using Archetypes benefits the community" <a href="http://www.sonatype.com/people/2010/01/maven-archetypes-and-nexus-there-is-no-faster-way/" rel="noopener noreferrer" target="_blank">www.sonatype.com/people/2010/01/maven-archetypes-and-nexus-there-is-no-faster-way/</a>',
    likes: 0,
    retweets: 0,
    tags: ['weld']
  },
  {
    id: '8694243792',
    created: 1265405455000,
    type: 'post',
    text: 'The snow blanket has officially landed. See you in a couple of days, sun.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8689274004',
    created: 1265396663000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/mojavelinux" rel="noopener noreferrer" target="_blank">@mojavelinux</a> Who needs groundhogs?',
    likes: 0,
    retweets: 0
  },
  {
    id: '8689075927',
    created: 1265396329000,
    type: 'post',
    text: 'A sure sign that snow is coming.',
    photos: ['<div class="entry"><img class="photo" src="media/8689075927.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '8683400919',
    created: 1265387013000,
    type: 'post',
    text: 'Weld 1.0.1-CR2 is in the process of being released. This will be a full distro. #weld',
    likes: 0,
    retweets: 0,
    tags: ['weld']
  },
  {
    id: '8653833465',
    created: 1265328006000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> Is that Eldora?<br><br>In reply to: <a href="https://x.com/matthewmccull/status/8653405002" rel="noopener noreferrer" target="_blank">@matthewmccull</a> <span class="status">8653405002</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8653120252',
    created: 1265326808000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> Nice, a lot cleaner :)<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/8650203648" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">8650203648</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8652983573',
    created: 1265326576000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DavidGeary" rel="noopener noreferrer" target="_blank">@DavidGeary</a> Want to write apps? No better choice than the Droid. I know you like GWT. Remember Google made Eclipse plugin for Android too ;)<br><br>In reply to: @davidgeary <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8650946827',
    created: 1265323210000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> Equal opportunity ;)<br><br>In reply to: <a href="https://x.com/aschwart/status/8649863327" rel="noopener noreferrer" target="_blank">@aschwart</a> <span class="status">8649863327</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8649633938',
    created: 1265320989000,
    type: 'post',
    text: 'Just sent a long letter to the JUG program manager. Requested that list of JUGs be kept up to date and to use a site like meetup.com.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8645490166',
    created: 1265313596000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Send me an e-mail about that. I\'ve got a story to share and some planning to do myself.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/8645052737" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">8645052737</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8641583005',
    created: 1265306501000,
    type: 'post',
    text: 'Yeah! I successfully deployed my first Google App Engine application based on JSF 2.0 (Mojarra) and CDI 1.0 (Weld). Thanks <a class="mention" href="https://x.com/SBryzak" rel="noopener noreferrer" target="_blank">@SBryzak</a>! #gae',
    likes: 0,
    retweets: 0,
    tags: ['gae']
  },
  {
    id: '8639701807',
    created: 1265303206000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rdifrango" rel="noopener noreferrer" target="_blank">@rdifrango</a> Or break something else, like your computer screen or wall ;)<br><br>In reply to: <a href="https://x.com/rdifrango/status/8639607684" rel="noopener noreferrer" target="_blank">@rdifrango</a> <span class="status">8639607684</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8639484891',
    created: 1265302844000,
    type: 'post',
    text: 'Great. I can\'t install Eclipse plugins anymore because of some bizarre error. When Eclipse fails, it fails royally. Time for reinstall :(',
    likes: 0,
    retweets: 0
  },
  {
    id: '8638424793',
    created: 1265301075000,
    type: 'post',
    text: 'If you are using Eclipse 3.5.1 with Ubuntu 9.10 and having trouble w/ buttons not working, see <a href="http://mou.me.uk/2009/10/31/fixing-eclipse-in-ubuntu-9-10-karmic-koala/" rel="noopener noreferrer" target="_blank">mou.me.uk/2009/10/31/fixing-eclipse-in-ubuntu-9-10-karmic-koala/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8638086690',
    created: 1265300524000,
    type: 'post',
    text: 'You are a superstar! RT <a class="mention" href="https://x.com/SBryzak" rel="noopener noreferrer" target="_blank">@SBryzak</a> Read part 1 of my article describing how to run CDI on Google App Engine - <a href="http://in.relation.to/14203.lace" rel="noopener noreferrer" target="_blank">in.relation.to/14203.lace</a>',
    likes: 2,
    retweets: 1
  },
  {
    id: '8636935162',
    created: 1265298653000,
    type: 'post',
    text: 'The #jbosstesting IRC channel is now mentioned on the jboss.org project space for Arquillian: <a href="http://community.jboss.org/en/arquillian" rel="noopener noreferrer" target="_blank">community.jboss.org/en/arquillian</a>',
    likes: 0,
    retweets: 0,
    tags: ['jbosstesting']
  },
  {
    id: '8636726291',
    created: 1265298311000,
    type: 'post',
    text: 'Responding quickly to my request, the jboss.org dev team has added a twitter URL field to the jboss.org profile form! Go fill it in!',
    likes: 0,
    retweets: 0
  },
  {
    id: '8610650854',
    created: 1265243491000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Nope, just #jbosstesting. You just gave me an idea, we should put this on the relevant jboss.org Spaces pages.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/8609659994" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">8609659994</span>',
    likes: 0,
    retweets: 0,
    tags: ['jbosstesting']
  },
  {
    id: '8608185139',
    created: 1265239660000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Brian_Fox" rel="noopener noreferrer" target="_blank">@Brian_Fox</a> Yes and no. Can\'t it just be put in the plugin management section of the Maven super pom? Defaulting the plugin config?<br><br>In reply to: <a href="https://x.com/Brian_Fox/status/8606799604" rel="noopener noreferrer" target="_blank">@Brian_Fox</a> <span class="status">8606799604</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8606946873',
    created: 1265237614000,
    type: 'post',
    text: 'EveryDNS was sold to Dyn Inc. (DynDNS.com). I\'ve used EveryDNS ever since the day I registered my first domain in 2001.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8606736876',
    created: 1265237260000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Brian_Fox" rel="noopener noreferrer" target="_blank">@Brian_Fox</a> I think the default JDK target should be based on the Maven binary being used. Use Maven 3, default is JDK 6. Simple to me.<br><br>In reply to: <a href="https://x.com/Brian_Fox/status/8606429755" rel="noopener noreferrer" target="_blank">@Brian_Fox</a> <span class="status">8606429755</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8606593965',
    created: 1265237019000,
    type: 'post',
    text: 'Btw, my rants are nothing against the JUGs themselves. JUGs are so awesome. Great crowds. Just hard to coordinate for speakers.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8606460341',
    created: 1265236795000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> Sept 7 might work. I\'ll let you know.<br><br>In reply to: <a href="https://x.com/matthewmccull/status/8606135176" rel="noopener noreferrer" target="_blank">@matthewmccull</a> <span class="status">8606135176</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8606374198',
    created: 1265236647000,
    type: 'post',
    text: 'Amen! RT <a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> Why the default maven target is still not JDK 1.5 ? It\'s been over 5 years now.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8606253554',
    created: 1265236442000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> Absolutely. In fact, right now I\'m working on getting some semblance of tour going ;) I\'m looking Apr - Jun time frame.<br><br>In reply to: <a href="https://x.com/matthewmccull/status/8606135176" rel="noopener noreferrer" target="_blank">@matthewmccull</a> <span class="status">8606135176</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8606084927',
    created: 1265236160000,
    type: 'post',
    text: 'There needs to be a single, updated directory of active JUGs. Not half a dozen pages filled with obsolete JUGs. It shouldn\'t be that hard.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8606046035',
    created: 1265236093000,
    type: 'post',
    text: 'The JUG program is so poorly managed it\'s making me crazy. Is Nichole Scott still the program manager under Oracle? I need to get in touch.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8603332519',
    created: 1265231463000,
    type: 'post',
    text: 'Interested in how JSF 2.0 works in a portal? Check out this webinar: <a href="http://www.icefaces.org/main/resources/upcoming-webinars.iface?webinar=478643680" rel="noopener noreferrer" target="_blank">www.icefaces.org/main/resources/upcoming-webinars.iface?webinar=478643680</a> I just might be attending.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8602995961',
    created: 1265230877000,
    type: 'post',
    text: 'Shaweeet! TripIt is now available for Android. About time they got on the bandwagon!',
    likes: 0,
    retweets: 0
  },
  {
    id: '8599945130',
    created: 1265225393000,
    type: 'post',
    text: 'The worst part about reconnecting with an old friend: Getting on their list of e-mail forwards.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8576108737',
    created: 1265174790000,
    type: 'post',
    text: 'Weld 1.0.1-CR1 is now in the central Maven repository (aka released). A full distro won\'t be created until CR2. #weld #cdi',
    likes: 0,
    retweets: 0,
    tags: ['weld', 'cdi']
  },
  {
    id: '8575687711',
    created: 1265173884000,
    type: 'post',
    text: 'My wireless keyboard is going insane. Typing this tweet took 20 tries.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8575625195',
    created: 1265173755000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I\'ve been swamped, finally getting around to it. I\'ve invited the JSF EG to swing by. So perhaps more traffic tomorrow.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/8574789935" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">8574789935</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8575542139',
    created: 1265173584000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Strange. At some point they must of disabled HTML in signatures. I cleaned it up. Thx.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/8575278906" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">8575278906</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8574313539',
    created: 1265171187000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/edburns" rel="noopener noreferrer" target="_blank">@edburns</a> is promoting his #jsf2 book on JavaRanch this week. Get your JSF 2 question answered: <a href="http://www.coderanch.com/forums/f-82/JSF" rel="noopener noreferrer" target="_blank">www.coderanch.com/forums/f-82/JSF</a>',
    likes: 0,
    retweets: 0,
    tags: ['jsf2']
  },
  {
    id: '8569913523',
    created: 1265163993000,
    type: 'post',
    text: 'I\'m really enjoying using jboss.org since the upgrade. I\'m looking forward to the day that we can reunite the Seam project. Stay tuned.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8569003924',
    created: 1265162604000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Actually, the other thing I was going to say is that it is nice that you bring this up as we get ready to impl in Seam 3.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/8565595305" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">8565595305</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8564177544',
    created: 1265155162000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Good job. I look forward to seeing examples of the custom exception handler. Also, replace the XSD snippet w/ bullets.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/8552464462" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">8552464462</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8563485879',
    created: 1265154022000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cbredesen" rel="noopener noreferrer" target="_blank">@cbredesen</a> I\'ll talk to Rayme. I bet this is an easy one to setup.<br><br>In reply to: <a href="https://x.com/cbredesen/status/8557475584" rel="noopener noreferrer" target="_blank">@cbredesen</a> <span class="status">8557475584</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8563379724',
    created: 1265153852000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Awesome! I\'ll ping you about it when the date gets closer.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/8558586828" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">8558586828</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8557348250',
    created: 1265143742000,
    type: 'post',
    text: 'Just booked my trip to speak about #cdi, #weld and #jsf2 at Connecticut JUG on Feb 16. Details to be posted on <a href="http://ctjava.org" rel="noopener noreferrer" target="_blank">ctjava.org</a> soon.',
    likes: 0,
    retweets: 0,
    tags: ['cdi', 'weld', 'jsf2']
  },
  {
    id: '8553762481',
    created: 1265137343000,
    type: 'post',
    text: 'Some responses about where we are headed with Arquillian: <a href="http://community.jboss.org/message/523756#523756" rel="noopener noreferrer" target="_blank">community.jboss.org/message/523756#523756</a> Formal user guide in the works.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8552000415',
    created: 1265134214000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> To github! Hear, hear!<br><br>In reply to: <a href="https://x.com/jasondlee/status/8547565398" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">8547565398</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8529786363',
    created: 1265085817000,
    type: 'post',
    text: 'About once a week, Firefox slows to a crawl and I have to restart it; fairly certain Flash is to blame, so I\'m glad to see Flash on the out.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8521477729',
    created: 1265071692000,
    type: 'post',
    text: 'I was so thrilled to be able to hear Obama answer direct questions today without the senseless babbling of a newscaster. Internet FTW!',
    likes: 0,
    retweets: 0
  },
  {
    id: '8513171897',
    created: 1265057369000,
    type: 'post',
    text: 'If you want to present, but aren\'t sure how to earn that ever important experience, become a Jazoon rookie <a href="http://jazoon.com/Conference/Jazoon-Rookie" rel="noopener noreferrer" target="_blank">jazoon.com/Conference/Jazoon-Rookie</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8513070076',
    created: 1265057182000,
    type: 'post',
    text: 'Working on presentation proposals for Jazoon.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8487587946',
    created: 1265003554000,
    type: 'post',
    text: 'Liven your writing environment. Vote for OpenOffice to support a bitmap as the application background: <a href="http://www.johannes-eva.net/index.php?page=ooo_background" rel="noopener noreferrer" target="_blank">www.johannes-eva.net/index.php?page=ooo_background</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8484585031',
    created: 1264998209000,
    type: 'post',
    text: 'Watching the open discussion that Obama had w/ the GOP was extremely useful to me as a citizen: <a href="http://www.whitehouse.gov/blog/2010/01/29/president-holds-open-discussion-across-aisle" rel="noopener noreferrer" target="_blank">www.whitehouse.gov/blog/2010/01/29/president-holds-open-discussion-across-aisle</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8461516959',
    created: 1264963647000,
    type: 'post',
    text: 'Finally bought and started reading The Art of Community by Jono Bacon. Reading epub format on #android. Impressed by first chapter.',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '8429007860',
    created: 1264893564000,
    type: 'post',
    text: 'Winter Wonderland at my local ski resort (Whitetail). Might need to be takin\' a trip.',
    photos: ['<div class="entry"><img class="photo" src="media/8429007860.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '8420826480',
    created: 1264877081000,
    type: 'post',
    text: 'Damn, the asadmin lead at Sun got cut. The tool really pioneered scripting of the application server. I hope it has a future.',
    likes: 0,
    retweets: 1
  },
  {
    id: '8419628157',
    created: 1264874809000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Thanks for the tip. I\'ve got some ideas and I\'ll pass them on. I\'d like to do what I can to strengthen the network.<br><br>In reply to: <a href="https://x.com/jasondlee/status/8415507189" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">8415507189</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8419570415',
    created: 1264874703000,
    type: 'reply',
    text: '@mwessendorf That looks really nice, clean and professional.<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8419540901',
    created: 1264874649000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/richsharples" rel="noopener noreferrer" target="_blank">@richsharples</a> Same here. I traveled to Idaho and Colorado a week ago, and where does it snow? At home in Maryland. Crazy El Nino.<br><br>In reply to: <a href="https://x.com/richsharples/status/8408586138" rel="noopener noreferrer" target="_blank">@richsharples</a> <span class="status">8408586138</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8419477583',
    created: 1264874533000,
    type: 'post',
    text: 'This will make you do a double take: <a href="http://blogs.sun.com/" rel="noopener noreferrer" target="_blank">blogs.sun.com/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8403875862',
    created: 1264836336000,
    type: 'post',
    text: 'Here\'s the vid of Shaun White eating it on a practice run, only to show he is doing hard stuff and is a total animal. <a href="http://scp.ly/4957" rel="noopener noreferrer" target="_blank">scp.ly/4957</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8403781941',
    created: 1264836072000,
    type: 'post',
    text: 'Shaun White boosted a face plant on the snowboard superpipe lip, shook it off, then stomped one down on the next run to take gold. #sick',
    likes: 0,
    retweets: 0,
    tags: ['sick']
  },
  {
    id: '8403419759',
    created: 1264835104000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> So the story is, a list exists <em>&lt;expired link&gt;</em> but its missing key JUGs and we also miss a centralized event calendar.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/8402120070" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">8402120070</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8400653981',
    created: 1264828695000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kevin_farnham" rel="noopener noreferrer" target="_blank">@kevin_farnham</a> That\'s a big improvement since I checked it last year. Missing some key entries though like the Colorado JUGs.<br><br>In reply to: <a href="https://x.com/kevin_farnham/status/8400349908" rel="noopener noreferrer" target="_blank">@kevin_farnham</a> <span class="status">8400349908</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8396839226',
    created: 1264821702000,
    type: 'post',
    text: 'Message from Red Hat to JUGs everywhere. It would be a lot easier to send speakers if there was a decent centralized index of active JUGs.',
    likes: 0,
    retweets: 1
  },
  {
    id: '8395478568',
    created: 1264819344000,
    type: 'post',
    text: 'People that do snowmobile aerials are deranged. Period.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8389199213',
    created: 1264808426000,
    type: 'post',
    text: 'Red Hat state of the union 2010 <a href="http://press.redhat.com/2010/01/26/state-of-the-union-at-red-hat-2/" rel="noopener noreferrer" target="_blank">press.redhat.com/2010/01/26/state-of-the-union-at-red-hat-2/</a> Heavy emphasis on open source in government and open standards. Moving.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8388615504',
    created: 1264807377000,
    type: 'post',
    text: 'Transparency, participation and collaboration in the U.S. government: <a href="http://www.whitehouse.gov/open" rel="noopener noreferrer" target="_blank">www.whitehouse.gov/open</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8382048099',
    created: 1264795840000,
    type: 'post',
    text: 'A Cornell student will compete in the Vancouver Olympics: Rebecca Johnston \'12. Also 2 alums: Jamie Moriarty \'03, Douglas Murray \'03 #hockey',
    likes: 0,
    retweets: 0,
    tags: ['hockey']
  },
  {
    id: '8377497121',
    created: 1264788039000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Congrats to your family. For him, the best is yet to come ;)<br><br>In reply to: <a href="https://x.com/lightguardjp/status/8359013105" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">8359013105</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8377354107',
    created: 1264787810000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Think of it like TDD. You need the tests in place before you get too close to the 40 deployment.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8358643481',
    created: 1264748591000,
    type: 'post',
    text: 'I\'ll seize the moment while word is spreading. Lincoln Baxter is joining Red Hat to work full time on open source and standards. Congrats!',
    likes: 0,
    retweets: 1
  },
  {
    id: '8357029843',
    created: 1264744742000,
    type: 'post',
    text: 'I\'m thrilled to hear the US is putting resources towards a decent train system. I dream of when I can move around the US like I can the EU.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8356471489',
    created: 1264743535000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/edburns" rel="noopener noreferrer" target="_blank">@edburns</a> Two thumbs up!<br><br>In reply to: <a href="https://x.com/edburns/status/8355728267" rel="noopener noreferrer" target="_blank">@edburns</a> <span class="status">8355728267</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8353966412',
    created: 1264738887000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> Good to hear. We don\'t want to jeopardize the great momentum we have established w/ JSF.<br><br>In reply to: <a href="https://x.com/arungupta/status/8348395198" rel="noopener noreferrer" target="_blank">@arungupta</a> <span class="status">8348395198</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8353850896',
    created: 1264738691000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> Have you tried out Arquillian?<br><br>In reply to: <a href="https://x.com/jeremynorris/status/8084320618" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">8084320618</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8348327181',
    created: 1264729907000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/cagataycivici" rel="noopener noreferrer" target="_blank">@cagataycivici</a> Ping me if you need help or advice getting setup with Nexus OSS hosting to sync to Maven central. Pretty easy though.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8348297305',
    created: 1264729857000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/cagataycivici" rel="noopener noreferrer" target="_blank">@cagataycivici</a> You can get PrimeFaces published to the Maven central to help adoption: <a href="http://nexus.sonatype.org/oss-repository-hosting.html" rel="noopener noreferrer" target="_blank">nexus.sonatype.org/oss-repository-hosting.html</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8348024955',
    created: 1264729408000,
    type: 'post',
    text: 'Anticipating news about continued employment from other members of the Mojarra team: <a class="mention" href="https://x.com/edburns" rel="noopener noreferrer" target="_blank">@edburns</a> <a class="mention" href="https://x.com/rogerk09" rel="noopener noreferrer" target="_blank">@rogerk09</a> and <a class="mention" href="https://x.com/RLubke" rel="noopener noreferrer" target="_blank">@RLubke</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8347667881',
    created: 1264728819000,
    type: 'reply',
    text: '@netdance Bullocks. My condolences. They don\'t appreciate what they are losing. Keep in touch if you need a gig. You won\'t go hungry ;)<br><br>In reply to: @netdance <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8347595937',
    created: 1264728701000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Congrats!<br><br>In reply to: <a href="https://x.com/jasondlee/status/8347156330" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">8347156330</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8346898231',
    created: 1264727558000,
    type: 'post',
    text: 'You can now follow the weld-dev mailinglist on nabble <a href="http://n3.nabble.com/Weld-development-discussions-f46994.html" rel="noopener noreferrer" target="_blank">n3.nabble.com/Weld-development-discussions-f46994.html</a> (dating back to 11/2009). #cdi #weld',
    likes: 0,
    retweets: 0,
    tags: ['cdi', 'weld']
  },
  {
    id: '8346739921',
    created: 1264727297000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cagataycivici" rel="noopener noreferrer" target="_blank">@cagataycivici</a> Great book. You\'ll really enjoy it. Perhaps it will motivate you to create UI components that consume RESTful web services ;)<br><br>In reply to: @cagataycivici <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8344551678',
    created: 1264723589000,
    type: 'post',
    text: 'I lot can happen in a year when you work out in the open: <a href="http://ocpsoft.com/opensource/one-years-time/" rel="noopener noreferrer" target="_blank">ocpsoft.com/opensource/one-years-time/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8344248265',
    created: 1264723085000,
    type: 'reply',
    text: '@netdance Are you serious? I\'m hoping you aren\'t getting at what I think your getting at.<br><br>In reply to: @netdance <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8335800982',
    created: 1264708270000,
    type: 'post',
    text: 'Remember to pimp your CDI and/or Weld articles and tutorials here: <a href="http://seamframework.org/Documentation/LinksToExternalCDIAndWeldDocumentation" rel="noopener noreferrer" target="_blank">seamframework.org/Documentation/LinksToExternalCDIAndWeldDocumentation</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8335149963',
    created: 1264707114000,
    type: 'post',
    text: 'Interested in defining/configuring #cdi beans in XML? Looking for feedback: <a href="http://old.nabble.com/XML-Extensions-td27195131.html" rel="noopener noreferrer" target="_blank">old.nabble.com/XML-Extensions-td27195131.html</a> #weld',
    likes: 0,
    retweets: 0,
    tags: ['cdi', 'weld']
  },
  {
    id: '8335003277',
    created: 1264706860000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Let me know when you get the integration up and running and I\'ll spread the word with a post in in.relation.to.<br><br>In reply to: <a href="https://x.com/jasondlee/status/8331731032" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">8331731032</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8334941729',
    created: 1264706751000,
    type: 'post',
    text: 'I\'m trying to triage my inbox. It looks like a field laced with land mines. New messages separated by read messages in limbo. Eek.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8331183194',
    created: 1264700247000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> When I saw the news about kenai, I immediately thought of you (and FacesTester). GitHub is a good experience. Go for it.<br><br>In reply to: <a href="https://x.com/jasondlee/status/8323882328" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">8323882328</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8331022488',
    created: 1264699977000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> Yeah, Qdoba is huge in the Denver suburbs. Wow. There are a couple around here. I\'ll have to give it a try.<br><br>In reply to: <a href="https://x.com/rruss/status/8326681253" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">8326681253</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8311165835',
    created: 1264656235000,
    type: 'post',
    text: 'I have to admit I caved to endorsement and had Chipotle tonight after seeing Steve Ells (CEO) on Oprah. In truth, supporting ethics ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '8310919345',
    created: 1264655732000,
    type: 'post',
    text: 'Obama: "[We have] to do our work openly and to give our people the government they deserve." +1 for openness. <a href="http://opensource.com" rel="noopener noreferrer" target="_blank">opensource.com</a>',
    likes: 0,
    retweets: 1
  },
  {
    id: '8310784223',
    created: 1264655456000,
    type: 'post',
    text: 'Obama had a "go hard or go home" CEO-like demeanor in the #sotu, my kind of attitude indeed. We need to stop stalling and get going.',
    likes: 0,
    retweets: 0,
    tags: ['sotu']
  },
  {
    id: '8310670285',
    created: 1264655228000,
    type: 'post',
    text: '"To close the credibility gap...we have to do our work openly" A call to action the JCP as a whole should embrace inside out #sotu',
    likes: 0,
    retweets: 1,
    tags: ['sotu']
  },
  {
    id: '8309680340',
    created: 1264653300000,
    type: 'post',
    text: 'I really like Obama\'s call out on token measures. He challenges lawmakers: you have to keep going until you please the people. Amen. #sotu',
    likes: 0,
    retweets: 0,
    tags: ['sotu']
  },
  {
    id: '8309432043',
    created: 1264652822000,
    type: 'post',
    text: 'Java EE faces a similar position as the US gov\'t. A lack of trust from the community. Time to open up and break away from the legacy.',
    likes: 0,
    retweets: 1
  },
  {
    id: '8309071354',
    created: 1264652141000,
    type: 'post',
    text: 'I was once again inspired by Obama\'s #sotu address. But for different reasons. He reminded the reps of their purpose and why US is unique.',
    likes: 0,
    retweets: 0,
    tags: ['sotu']
  },
  {
    id: '8302368767',
    created: 1264640231000,
    type: 'post',
    text: 'Caught episode about "real food" on Oprah during lunch (just happened to be on when I sat down). Good news. Chipotle is real food ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '8291165928',
    created: 1264621047000,
    type: 'post',
    text: 'Eclipse should change "Delete" on a project to "Remove". Delete is what happens when you check "delete project contents on disk".',
    likes: 0,
    retweets: 1
  },
  {
    id: '8291088479',
    created: 1264620924000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> OpenOffice needs all the help it can get. I really hope Oracle can find a way to make it tolerable software.<br><br>In reply to: <a href="https://x.com/jasondlee/status/8290587045" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">8290587045</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8290506477',
    created: 1264620038000,
    type: 'post',
    text: 'The conflict between m2eclipse and the Maven enforcer plugin continues to bit us: <a href="https://issues.sonatype.org/browse/MNGECLIPSE-768" rel="noopener noreferrer" target="_blank">issues.sonatype.org/browse/MNGECLIPSE-768</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8287709937',
    created: 1264615068000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> She has a recurring shoulder slip that is putting a dent in her snowboarding career. Need to get her fixed on off the DL.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/8287275814" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">8287275814</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8287623775',
    created: 1264614901000,
    type: 'post',
    text: 'Remember when a Nintendo game wouldn\'t work? Blowing inside the cartridge would magically fix the problem. No internet. We just knew.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8286939111',
    created: 1264613550000,
    type: 'post',
    text: 'My wife to her most peaceful place on earth: inside an MRI machine.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8257857078',
    created: 1264552617000,
    type: 'post',
    text: 'I\'m so glad I got my US passport last year. Getting one now seems to be a nightmare. The process took less than two weeks when I did it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8257787669',
    created: 1264552500000,
    type: 'post',
    text: 'Audio from the Jan 20, 2010 JSF EG meeting: <a href="https://javaserverfaces-spec-public.dev.java.net/files/documents/1936/147116/20100120-eg-audio.wav" rel="noopener noreferrer" target="_blank">javaserverfaces-spec-public.dev.java.net/files/documents/1936/147116/20100120-eg-audio.wav</a> discussing plans for JSF 2.0 Rev A.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8257167090',
    created: 1264551452000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/StevenBoscarine" rel="noopener noreferrer" target="_blank">@StevenBoscarine</a> made the first forum post in the new Arquillian space. <a href="http://community.jboss.org/en/arquillian?view=discussions" rel="noopener noreferrer" target="_blank">community.jboss.org/en/arquillian?view=discussions</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8256255014',
    created: 1264549866000,
    type: 'post',
    text: 'Richard put together some videos demonstrating the purpose of #metawidget <a href="http://metawidget.org/videos.html" rel="noopener noreferrer" target="_blank">metawidget.org/videos.html</a> Great sales pitch vid.',
    likes: 0,
    retweets: 0,
    tags: ['metawidget']
  },
  {
    id: '8255002984',
    created: 1264547704000,
    type: 'post',
    text: 'My sis has put together a website for her wedding. My wife and I are both in the wedding party. <a href="http://macandangela.com/" rel="noopener noreferrer" target="_blank">macandangela.com/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8254354782',
    created: 1264546639000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MertCaliskan" rel="noopener noreferrer" target="_blank">@MertCaliskan</a> You know it! We come pre-styled baby!<br><br>In reply to: @mertcaliskan <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8254241657',
    created: 1264546453000,
    type: 'post',
    text: 'I know too many people in this situation: RT <a class="mention" href="https://x.com/MertCaliskan" rel="noopener noreferrer" target="_blank">@MertCaliskan</a> definitely my case :) <em>&lt;expired link&gt;</em>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8253943202',
    created: 1264545963000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MertCaliskan" rel="noopener noreferrer" target="_blank">@MertCaliskan</a> Thanks for the feedback! Normally I won\'t bother you all with my own personal decisions, but I\'ve been agonizing over this.<br><br>In reply to: @mertcaliskan <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8253050141',
    created: 1264544453000,
    type: 'post',
    text: 'As my snowboard pants are more than a decade old, it\'s time to upgrade. Which combo do you think works best?',
    photos: ['<div class="entry"><img class="photo" src="media/8253050141.png"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '8252102117',
    created: 1264542832000,
    type: 'post',
    text: 'thinks the state of the union address (Jan 27, 9 PM EST) should be treated w/ as much importance as a national holiday.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8251836387',
    created: 1264542372000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> Nah, I don\'t want my timeline to be filled up with endless rants. Besides, I doubt he would embrace the 140 char limit.<br><br>In reply to: <a href="https://x.com/jeremynorris/status/8247450269" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">8247450269</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8251616743',
    created: 1264541990000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/edburns" rel="noopener noreferrer" target="_blank">@edburns</a> refutes the slander against JSF <a href="http://weblogs.java.net/blog/edburns/archive/2010/01/22/analysis-peter-thomass-jsf-critical-rant" rel="noopener noreferrer" target="_blank">weblogs.java.net/blog/edburns/archive/2010/01/22/analysis-peter-thomass-jsf-critical-rant</a> Puts several well-known anti-JSF articles into context.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8249912228',
    created: 1264538976000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> Damn people don\'t read the signs. Don\'t get on the plane if you are sick. Of course, it could have been the friend I sat with. Doh!<br><br>In reply to: <a href="https://x.com/rruss/status/8248772791" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">8248772791</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8244804598',
    created: 1264529640000,
    type: 'post',
    text: 'I\'m getting back up to speed after being horizontal yesterday with a head cold. I must have picked up something on the return flight ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '8180241757',
    created: 1264395811000,
    type: 'post',
    text: 'That Saints-Vikings took a lot of energy just to watch. Clearly there are 3 Super Bowl-quality teams this year. Congrats to the #saints!',
    likes: 0,
    retweets: 0,
    tags: ['saints']
  },
  {
    id: '8128792462',
    created: 1264291137000,
    type: 'post',
    text: 'I\'m suprised how many times I cited Outliers in the last week. Clearly had some solid points that relate well in the real world.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8124677574',
    created: 1264282603000,
    type: 'post',
    text: 'My battle scars from a week of snowboarding.',
    photos: ['<div class="entry"><img class="photo" src="media/8124677574.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '8103743331',
    created: 1264231584000,
    type: 'post',
    text: 'It\'s now possible to submit reviews directly from the Places Directory for Android. So cool.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8103389668',
    created: 1264230481000,
    type: 'post',
    text: 'We said ! I-70 and checked out Eldora Mountain. Locals snub noses at it, but it\'s a step up from east coast skiing. Great for learning.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8103332844',
    created: 1264230320000,
    type: 'post',
    text: 'I got so close to landing a 360 at Eldora, but I splashed out as I touched down. I can visualize the landing, just need fresh legs.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8103267440',
    created: 1264230138000,
    type: 'post',
    text: 'A shot of the flatirons in Boulder from Arapahoe. Camera phone doesn\'t do it justice though.',
    photos: ['<div class="entry"><img class="photo" src="media/8103267440.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '8103223143',
    created: 1264230020000,
    type: 'post',
    text: 'I would love to live up in the front range and or canyons above Denver, but mobile networks are non-existent.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8103164367',
    created: 1264229861000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> I actually came in on the Nevada SouthWest plane but could get a good shot of it.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/8102613509" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">8102613509</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8102832932',
    created: 1264228940000,
    type: 'post',
    text: 'I took a pitstop in Starbucks today to sumbit my #JBossWorld2010 presentations. JSF, RESTEasy and Seam stuff. Details later.',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld2010']
  },
  {
    id: '8102761258',
    created: 1264228747000,
    type: 'post',
    text: 'Excited to hear Verizon advocating the openness of Android. Annoyed that app providers are referring to platform as "droid"',
    likes: 0,
    retweets: 0
  },
  {
    id: '8102686420',
    created: 1264228549000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> It\'s good and bad. Good that they are there to compile against, but bad that they are misleading not being a full impl.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/8087646393" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">8087646393</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8102655411',
    created: 1264228466000,
    type: 'post',
    text: 'Nice to return to a clean home. Cleaning service FTW!',
    likes: 0,
    retweets: 0
  },
  {
    id: '8102628301',
    created: 1264228395000,
    type: 'post',
    text: 'Exchanged stories about snow-capped mountain adventures w/ <a class="mention" href="https://x.com/gscottshaw" rel="noopener noreferrer" target="_blank">@gscottshaw</a> on the plane. Conclusion: he was covered in powder, I got none ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '8098575220',
    created: 1264219657000,
    type: 'post',
    text: 'A better shot of the SouthWest Maryland plane.',
    photos: ['<div class="entry"><img class="photo" src="media/8098575220.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '8098420212',
    created: 1264219360000,
    type: 'post',
    text: 'A shot of the Maryland SouthWest plane.',
    photos: ['<div class="entry"><img class="photo" src="media/8098420212.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '8091696801',
    created: 1264207937000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Cardboard straw. Forgot to say why it was earth friendly ;)<br><br>In reply to: <a href="https://x.com/lightguardjp/status/8088194064" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">8088194064</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8091394001',
    created: 1264207382000,
    type: 'post',
    text: 'Walked up to the gate in Denver and was shocked to see my friend <a class="mention" href="https://x.com/gscottshaw" rel="noopener noreferrer" target="_blank">@gscottshaw</a> sitting there waiting for the same flight.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8087646599',
    created: 1264200451000,
    type: 'post',
    text: 'Earth friendly straw at Ted\'s Montana Grill in Denver. They sure are serious about the environment in these parts.',
    photos: ['<div class="entry"><img class="photo" src="media/8087646599.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '8083734973',
    created: 1264193199000,
    type: 'post',
    text: 'Awesome! <a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> Submitted a proposal for #JbossWorld: The Future of Java EE Testing, #Arquillian and #ShrinkWrap',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld', 'arquillian', 'shrinkwrap']
  },
  {
    id: '8017782186',
    created: 1264054529000,
    type: 'post',
    text: 'Just finished a whirlwind tour of region north of Denver: Boulder -&gt; Longmont -&gt; Estes Park -&gt; Loveland -&gt; Ft Collins -&gt; Boulder.',
    likes: 0,
    retweets: 0
  },
  {
    id: '8017744529',
    created: 1264054433000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/edburns" rel="noopener noreferrer" target="_blank">@edburns</a> Sorry I had to miss the JSF EG meeting. I really wanted to join, but it feel smack in the middle of my vacation :(<br><br>In reply to: <a href="https://x.com/edburns/status/7994102734" rel="noopener noreferrer" target="_blank">@edburns</a> <span class="status">7994102734</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '8017712381',
    created: 1264054350000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Congrats!<br><br>In reply to: <a href="https://x.com/lightguardjp/status/7995129300" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">7995129300</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7993339507',
    created: 1264009172000,
    type: 'post',
    text: 'I carried my wife in the Wife Carrying Contest at Lookout Pass, a first for us. But, I dropped her :( We\'ll just have to do more practice.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7974043958',
    created: 1263960505000,
    type: 'post',
    text: 'A broomball match on Pearl St in Boulder.',
    photos: ['<div class="entry"><img class="photo" src="media/7974043958.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '7962671376',
    created: 1263940944000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Yeah, but that\'s not a solution, it is a workaround.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/7958040040" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">7958040040</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7962588727',
    created: 1263940794000,
    type: 'post',
    text: 'The view descending into SLC.',
    photos: ['<div class="entry"><img class="photo" src="media/7962588727.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '7959455001',
    created: 1263935040000,
    type: 'post',
    text: 'What the #$@% does the Agenda view in Google Calendar on Android always pop to some random date rather than the current date?',
    likes: 0,
    retweets: 0
  },
  {
    id: '7958006864',
    created: 1263932253000,
    type: 'post',
    text: 'Learned to identify the conifer varieties on Silver Mt skiing w/ forester: W. Hemlock, Tamarack, Douglas Fur, White Pine, and others.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7957987696',
    created: 1263932215000,
    type: 'post',
    text: 'Thanks Ruff family for a wonderful stay and home-cooked feasts in Northern Idaho!',
    likes: 0,
    retweets: 0
  },
  {
    id: '7957967257',
    created: 1263932175000,
    type: 'post',
    text: 'My Bose QC 15 headphones got their first "air test". Like stepping into an acoustic, sound-proofed room. Wow!',
    likes: 0,
    retweets: 0
  },
  {
    id: '7957936273',
    created: 1263932114000,
    type: 'post',
    text: 'Wish I could disable shutter on android camera. Can the law really require a camera phone to make a shutter sound? If so, that\'s bullshit.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7954505010',
    created: 1263925322000,
    type: 'reply',
    text: '@mwessendorf +1<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7954099509',
    created: 1263924524000,
    type: 'post',
    text: 'Time zone handling in Google calendar is so confusing to use (if it even works) that the calendar actually becomes a hindrance.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7937651016',
    created: 1263886307000,
    type: 'post',
    text: 'Fun fact about Kellogg, ID: Home of the Bunker Hill Superfund Site, a former mining and smelting complex. Now we vacation there ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '7937599346',
    created: 1263886154000,
    type: 'post',
    text: 'Note to self: Eat before getting on a 5 hour SouthWest flight. The peanuts and pretzels just aren\'t going to cut it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7937406208',
    created: 1263885558000,
    type: 'post',
    text: 'I\'ve got frontside and backside 180 down, regular and goofy. Learning backside 180 cost me a black eye. The 360 still remains in my quest.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7936819968',
    created: 1263883849000,
    type: 'post',
    text: 'The pavaraizzi were following me around the mountain (pics soon). Photographer showed uncanny ability to ski backwards while shooting.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7936624255',
    created: 1263883299000,
    type: 'post',
    text: 'Scenery at Kellog Peek was stunning today. 360 deg of Rocky Mountains covered in conifers that look like arrowheads stuck in the ground.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7936567699',
    created: 1263883146000,
    type: 'post',
    text: 'Went to Silver Mt today. Strange that there is no snow in the town, but you cross the snow line on ride up gondola to base of the resort.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7855432022',
    created: 1263709242000,
    type: 'post',
    text: 'I was really hoping the Ravens were going to hand one to the Colts, because my wife looks really cute in her new Ravens jersey. Oh well.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7855405963',
    created: 1263709183000,
    type: 'post',
    text: 'I alternated skiing between Montana and Idaho today @ Lookout Pass Ski Area. Rain was in the forecast, but it snowed all day long ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '7816845196',
    created: 1263619395000,
    type: 'post',
    text: 'The real JavaOne or just an imposter?',
    photos: ['<div class="entry"><img class="photo" src="media/7816845196.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '7809098154',
    created: 1263603889000,
    type: 'post',
    text: 'Staying at the base of <a class="mention" href="https://x.com/SilverMtnResort" rel="noopener noreferrer" target="_blank">@SilverMtnResort</a> in Kellogg, ID. Within walking distance of the childhood house of a good friend from back east.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7808845707',
    created: 1263603406000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> It wasn\'t a long layover. In fact, because of the missed approach, we ended up walking off one plane and onto another. #hungry<br><br>In reply to: <a href="https://x.com/lightguardjp/status/7797852586" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">7797852586</span>',
    likes: 0,
    retweets: 0,
    tags: ['hungry']
  },
  {
    id: '7797283119',
    created: 1263580741000,
    type: 'post',
    text: 'After reading a whole chapter in Outliers about missed approaches and plane crashes, it was freaky when we missed the 1st approach in SLC.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7779732169',
    created: 1263539671000,
    type: 'post',
    text: 'Aggregate feed of most official JBoss development blogs: <a href="http://www.jboss.org/feeds/view/all" rel="noopener noreferrer" target="_blank">www.jboss.org/feeds/view/all</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7779691909',
    created: 1263539546000,
    type: 'post',
    text: 'I think both Gavin King (JSR-299) and Emmanuel Bernard (JSR-303) should be nominated for Star Spec Leads. Hell, I think they should win.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7779676645',
    created: 1263539498000,
    type: 'post',
    text: 'Do you have candidates for 2010 Star Spec Leads? Please email heather@jcp.org if you would like to nominate a Spec Lead for this honor.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7778973762',
    created: 1263537506000,
    type: 'post',
    text: 'I like the look of the ShrinkWrap space start page: <a href="http://community.jboss.org/en/shrinkwrap?view=overview" rel="noopener noreferrer" target="_blank">community.jboss.org/en/shrinkwrap?view=overview</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7777171088',
    created: 1263532983000,
    type: 'post',
    text: 'The sky must be falling, my wife is using FaceBook. She\'s asking me what Mafia Wars is. I told her "ignore everything but the pictures".',
    likes: 0,
    retweets: 0
  },
  {
    id: '7776770907',
    created: 1263532069000,
    type: 'post',
    text: 'All packed for an early AM departure BWI -&gt; GEG (Spokane, WA). I\'ll be out for a week (though likely tweeting about snow things).',
    likes: 0,
    retweets: 0
  },
  {
    id: '7776696123',
    created: 1263531905000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> Awesome! Just like old buddies. It was a reunion day in two respects. I was on a call this morning w/ Steven ;)<br><br>In reply to: <a href="https://x.com/aschwart/status/7768416554" rel="noopener noreferrer" target="_blank">@aschwart</a> <span class="status">7768416554</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7762495672',
    created: 1263505282000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Yep, already filed a JIRA this morning.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/7761044041" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">7761044041</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7759029954',
    created: 1263498373000,
    type: 'post',
    text: 'Basically, we are rice farmers ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '7759021883',
    created: 1263498357000,
    type: 'post',
    text: 'Gladwell perfectly explains the motivation behind open source work: it\'s autonomous, complex and meaningful (effort leads to reward)',
    likes: 0,
    retweets: 0
  },
  {
    id: '7758922533',
    created: 1263498154000,
    type: 'post',
    text: 'Outliers are those who\'ve been given opportunities and have had the strength and presence of mind to seize them - Malcolm Gladwell',
    likes: 0,
    retweets: 0
  },
  {
    id: '7758224474',
    created: 1263496732000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Not quite. This is more like a way to filter down the conversations happening on jboss.org to what likely interests you.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/7758026824" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">7758026824</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7756999131',
    created: 1263494240000,
    type: 'post',
    text: 'Today is a good day to update your profile on jboss.org if you currently have no picture and it has you living in the Marshall Islands.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7756772770',
    created: 1263493783000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Ah, shucks. Must have been the beer I brought to the channel that won you over.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/7756742310" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">7756742310</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7755198874',
    created: 1263490629000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> No, not for Seam 2. Support is sold as a feature pack for EAP 5. I expect the future to be different, but no official word.<br><br>In reply to: @pelegri <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7755128635',
    created: 1263490489000,
    type: 'post',
    text: 'Cake writes the strangest songs that are somehow catchy. Recently ripped Prolonging the Magic in the process of organizing music.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7754953927',
    created: 1263490146000,
    type: 'post',
    text: 'My first friend on jboss.org is <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a>. I\'m with the cool crowd now ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '7754680031',
    created: 1263489608000,
    type: 'post',
    text: 'Here\'s the link to the #shrinkwrap dev space <a href="http://community.jboss.org/community/shrinkwrap/dev?view=overview" rel="noopener noreferrer" target="_blank">community.jboss.org/community/shrinkwrap/dev?view=overview</a>',
    likes: 0,
    retweets: 0,
    tags: ['shrinkwrap']
  },
  {
    id: '7754671358',
    created: 1263489592000,
    type: 'post',
    text: 'ShrinkWrap now has a dedicated space on jboss.org <a href="http://community.jboss.org/community/shrinkwrap?view=overview" rel="noopener noreferrer" target="_blank">community.jboss.org/community/shrinkwrap?view=overview</a> #shrinkwrap Dev discusses go in nested space.',
    likes: 0,
    retweets: 0,
    tags: ['shrinkwrap']
  },
  {
    id: '7754264629',
    created: 1263488802000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> Weld is two things. JSR-299 RI and bring CDI to other environments, such as SE and Servlet. Seam is the portable CDI extensions.<br><br>In reply to: @pelegri <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7753395686',
    created: 1263487168000,
    type: 'post',
    text: 'New IRC room focused on testing in the context of #JBoss projects: #jbosstesting on Freenode #arquillian #shrinkwrap #embeddedas',
    likes: 1,
    retweets: 2,
    tags: ['jboss', 'jbosstesting', 'arquillian', 'shrinkwrap', 'embeddedas']
  },
  {
    id: '7753350604',
    created: 1263487081000,
    type: 'post',
    text: 'I fixed the permissions on <a href="http://seamframework.org/Documentation/LinksToExternalCDIAndWeldDocumentation" rel="noopener noreferrer" target="_blank">seamframework.org/Documentation/LinksToExternalCDIAndWeldDocumentation</a> so you can actually edit. Got a #cdi (JSR-299) or #weld tutorial?',
    likes: 0,
    retweets: 0,
    tags: ['cdi', 'weld']
  },
  {
    id: '7750736724',
    created: 1263482142000,
    type: 'post',
    text: 'Just had a demo presentation of <a href="http://www.clickframes.org" rel="noopener noreferrer" target="_blank">www.clickframes.org</a> by the devs of #clickframes. Generates Seam 2 apps from a spec. Very cool!',
    likes: 0,
    retweets: 2,
    tags: ['clickframes']
  },
  {
    id: '7736764511',
    created: 1263444534000,
    type: 'post',
    text: 'Arquillian has an official "space" on jboss.org. Space = wiki+forum. <a href="http://community.jboss.org/en/arquillian" rel="noopener noreferrer" target="_blank">community.jboss.org/en/arquillian</a> Use subspace for dev discussions.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7735098397',
    created: 1263441166000,
    type: 'post',
    text: 'I gave my Ride Machete a thorough lashing today. Doesn\'t even look used. Wow. My wrist, on the other hand, feels very used =&gt; wrist guards.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7734871357',
    created: 1263440735000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> Thanks! They\'re off to a great start. And embedded-glassfish:run is like jetty crossed w/ JEE. A killer dev environment.<br><br>In reply to: <a href="https://x.com/jeremynorris/status/7717261691" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">7717261691</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7734819143',
    created: 1263440637000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Utah isn\'t in the cards for this trip, but certainly a possibility for future. I\'ve seen a SnowBird blizzard first hand. +1<br><br>In reply to: <a href="https://x.com/lightguardjp/status/7729459598" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">7729459598</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7729398640',
    created: 1263430993000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> True. But it\'s still bullshit that JAX-RS did not get the respect of being included in the web profile. #peoplelivinginpast<br><br>In reply to: <a href="https://x.com/jeremynorris/status/7723743001" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">7723743001</span>',
    likes: 0,
    retweets: 0,
    tags: ['peoplelivinginpast']
  },
  {
    id: '7729047584',
    created: 1263430352000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> My previous board was shit. I couldn\'t even tell you who made it. Blue-light special.<br><br>In reply to: <a href="https://x.com/metacosm/status/7727113123" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">7727113123</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7728968387',
    created: 1263430205000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> Have snowboard, will travel and ride<br><br>In reply to: <a href="https://x.com/metacosm/status/7727160272" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">7727160272</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7728950675',
    created: 1263430172000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> Doing 3 ski areas in 1 week. This was warm-up/road test @ WhiteTail. Idaho over the weekend (friends) then Denver (move research)<br><br>In reply to: <a href="https://x.com/metacosm/status/7727160272" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">7727160272</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7726401679',
    created: 1263425323000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MertCaliskan" rel="noopener noreferrer" target="_blank">@MertCaliskan</a> Excellent. Be sure to tweet it, definitely interested.<br><br>In reply to: @mertcaliskan <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7726339990',
    created: 1263425205000,
    type: 'post',
    text: 'The Ride Machete board is just sick. I hit the first jump and it launched me into the stratosphere! Also has great ground control.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7709239496',
    created: 1263392172000,
    type: 'post',
    text: 'Variation on an old proverb: If you are a hard working programmer, your application will not be lazy.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7687430985',
    created: 1263339050000,
    type: 'post',
    text: 'I just learned that the prototype for CDI beans defined using an XML syntax lives here: <a href="https://launchpad.net/jbraze/trunk" rel="noopener noreferrer" target="_blank">launchpad.net/jbraze/trunk</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7687204047',
    created: 1263338659000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> I got a Ride Machete 2010 <a href="http://ridesnowboards.com/boards/machete" rel="noopener noreferrer" target="_blank">ridesnowboards.com/boards/machete</a> Also have the K2 Auto bindings.<br><br>In reply to: <a href="https://x.com/metacosm/status/7687032251" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">7687032251</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7687039959',
    created: 1263338372000,
    type: 'post',
    text: 'I\'m workin\' on getting a set of forums setup on jboss.org focused on testing. Out of that would be the creation of a forum for Arquillian.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7686729019',
    created: 1263337818000,
    type: 'post',
    text: 'I\'ve got to get together my snowboarding soundtrack tonight for the 2010 season.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7686715112',
    created: 1263337794000,
    type: 'post',
    text: 'Speaking of skiing, tomorrow I\'m playing hookey to test drive the new snowboard I got for xmas!',
    likes: 0,
    retweets: 0
  },
  {
    id: '7686674288',
    created: 1263337722000,
    type: 'post',
    text: 'I proudly aired my absences when my parents would take me skiing for the day...or when I got to compete in an invitational swim meet.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7686621087',
    created: 1263337628000,
    type: 'post',
    text: 'I don\'t believe in perfect attendance records. They are a bullshit metric. It\'s evidence of a person with a boring life.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7686485901',
    created: 1263337392000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Don\'t worry, I\'ve been pretty backlogged myself.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/7686091263" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">7686091263</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7686466941',
    created: 1263337361000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> I used m2eclipse and I have JBT 3.1 I believe. I need to upgrade to test the CDI completion.<br><br>In reply to: <a href="https://x.com/maxandersen/status/7685925849" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">7685925849</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7685890616',
    created: 1263336347000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> <a href="http://www.lasermyeye.org/encyclopedia/blinkrate.html" rel="noopener noreferrer" target="_blank">www.lasermyeye.org/encyclopedia/blinkrate.html</a><br><br>In reply to: <a href="https://x.com/metacosm/status/7685773664" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">7685773664</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7685854390',
    created: 1263336284000,
    type: 'post',
    text: 'The Weld archetypes now have a consolidated project information page. <a href="http://www.seamframework.org/Documentation/WeldArchetypeProjectInformation" rel="noopener noreferrer" target="_blank">www.seamframework.org/Documentation/WeldArchetypeProjectInformation</a> Looking for contributors!',
    likes: 0,
    retweets: 0
  },
  {
    id: '7685627092',
    created: 1263335877000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Embrace the cheesiness ;)<br><br>In reply to: <a href="https://x.com/jasondlee/status/7684984587" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">7684984587</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7685610744',
    created: 1263335847000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/MertCaliskan" rel="noopener noreferrer" target="_blank">@MertCaliskan</a> Where is the demo?',
    likes: 0,
    retweets: 0
  },
  {
    id: '7685582772',
    created: 1263335796000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> No, staring at a computer screen reduces your blink rate. And you tend to get dry eye after Lasik for a while (you may not).<br><br>In reply to: <a href="https://x.com/metacosm/status/7684174560" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">7684174560</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7684117070',
    created: 1263333159000,
    type: 'post',
    text: 'RichFaces has gone CafePress <a href="http://www.cafepress.com/jbossorg/7028316" rel="noopener noreferrer" target="_blank">www.cafepress.com/jbossorg/7028316</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7684102592',
    created: 1263333133000,
    type: 'post',
    text: 'Conan is a hero. RT <a class="mention" href="https://x.com/cnnbrk" rel="noopener noreferrer" target="_blank">@cnnbrk</a>: Conan releases statement on late-night situation <a href="https://archive.nytimes.com/mediadecoder.blogs.nytimes.com/2010/01/12/conan-obrien-says-he-wont-do-tonight-show-following-leno/" rel="noopener noreferrer" target="_blank">archive.nytimes.com/mediadecoder.blogs.nytimes.com/2010/01/12/conan-obrien-says-he-wont-do-tonight-show-following-leno/</a>',
    likes: 0,
    retweets: 1
  },
  {
    id: '7680138690',
    created: 1263325539000,
    type: 'post',
    text: 'The only bitch about lasik w/ tech people is that you\'re going to wrestle with dry eyes for a year because you\'re blink rate is lower.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7680105111',
    created: 1263325470000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Seriously, you\'re more likely to be in a car crash and loose your vision (or life). Or even infection from contacts.<br><br>In reply to: <a href="https://x.com/maxandersen/status/7677904721" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">7677904721</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7680059522',
    created: 1263325374000,
    type: 'post',
    text: 'Interesting. myfamily.com just introduced a feature to link your account to Facebook. Somewhat of a white flag against the momentum of FB.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7677458259',
    created: 1263320128000,
    type: 'post',
    text: 'I just got the vision restriction removed from my license. #lasik FTW! Feels good. Got a better picture too.',
    likes: 0,
    retweets: 0,
    tags: ['lasik']
  },
  {
    id: '7677381416',
    created: 1263319972000,
    type: 'post',
    text: 'I\'ve been in a quiet sort of mood the last couple of days. Just trying to get caught up.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7583806673',
    created: 1263101847000,
    type: 'post',
    text: 'Long day sorting through ~ 1000 photos. Fond memories, but neck hurts.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7573973985',
    created: 1263081753000,
    type: 'post',
    text: 'Looking through family photos, came across picture of Bernie Madoff. Freaky. Served on NASD board where dad works',
    photos: ['<div class="entry"><img class="photo" src="media/7573973985.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '7567164768',
    created: 1263066968000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jennifercord" rel="noopener noreferrer" target="_blank">@jennifercord</a> Oh yeah and Google Places. Maps offers search for places but I like the layout of places better.<br><br>In reply to: <a href="https://x.com/jennifercord/status/7563916790" rel="noopener noreferrer" target="_blank">@jennifercord</a> <span class="status">7563916790</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7565257276',
    created: 1263062822000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jennifercord" rel="noopener noreferrer" target="_blank">@jennifercord</a> Awesome! Enjoy Google Nav (built in), Google Goggles, Shazam, TwitterRide, WeatherBug, Pandora, TipIt, Sky Map, AK Notepad<br><br>In reply to: <a href="https://x.com/jennifercord/status/7563916790" rel="noopener noreferrer" target="_blank">@jennifercord</a> <span class="status">7563916790</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7562832816',
    created: 1263057631000,
    type: 'post',
    text: 'I\'m very happy that the multimedia keys on my Logitech keyboard finally work. The only work they did in RHEL 5 was to crash the media app.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7562806310',
    created: 1263057576000,
    type: 'post',
    text: 'I\'m enjoying Outliers. I have always believe that success is about having the good fortune of having, seeing and seizing opportunities.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7562771171',
    created: 1263057500000,
    type: 'post',
    text: 'From Outliers: "Sun Microsystems, one of the oldest and most important of Silicon Valley\'s software companies" An era is coming to an end.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7562714580',
    created: 1263057381000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> The buzz in the air at conferences just gets my brain waves flowing and I bang out code and articles as a result ;)<br><br>In reply to: <a href="https://x.com/lightguardjp/status/7548263151" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">7548263151</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7562630357',
    created: 1263057201000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Man, I\'m coming over to play ;) I say that like I haven\'t been invited 10 times by a good friend to play at his house. Work overload<br><br>In reply to: <a href="https://x.com/kito99/status/7538281117" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">7538281117</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7534739057',
    created: 1262989600000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Sweet!!! What a nice way to end the week! Now you have to go find someone for a nice dinner ;) I\'ll have a beer for you.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/7534633169" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">7534633169</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7534032243',
    created: 1262988257000,
    type: 'post',
    text: 'I\'m all booked for JSFdays2010. As it turns out, it\'s cheaper for me to stay in Vienna longer (Sun - Sun). Darn, I guess I get to have fun!',
    likes: 0,
    retweets: 0
  },
  {
    id: '7532684023',
    created: 1262985624000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Where do I need to be?<br><br>In reply to: <a href="https://x.com/maxandersen/status/7532286738" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">7532286738</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7510544548',
    created: 1262934364000,
    type: 'post',
    text: '"...because that\'s the to give up the think that makes civilization" <a href="http://www.ted.com/talks/bill_joy_muses_on_what_s_next.html" rel="noopener noreferrer" target="_blank">www.ted.com/talks/bill_joy_muses_on_what_s_next.html</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7510512635',
    created: 1262934273000,
    type: 'post',
    text: 'Bill Joy: "We can\'t give up the rule of law to fight an asymmetric threat." +1',
    likes: 0,
    retweets: 0
  },
  {
    id: '7508828327',
    created: 1262929883000,
    type: 'post',
    text: 'Take what Gavin says w/ a grain of salt, but he does give you points to mull over <a href="http://in.relation.to/Bloggers/YouShouldUpgradeToJavaEE6" rel="noopener noreferrer" target="_blank">in.relation.to/Bloggers/YouShouldUpgradeToJavaEE6</a>',
    likes: 1,
    retweets: 2
  },
  {
    id: '7507868902',
    created: 1262927734000,
    type: 'post',
    text: 'Are you an Ubuntu user that wants to hack on Android. Check out Tomdroid <a href="https://launchpad.net/tomdroid" rel="noopener noreferrer" target="_blank">launchpad.net/tomdroid</a> #tomboy #android Ubuntu One sync',
    likes: 0,
    retweets: 0,
    tags: ['tomboy', 'android']
  },
  {
    id: '7507305224',
    created: 1262926663000,
    type: 'post',
    text: 'Just discovered Google Goggles. Definitely looks like something that will come in hand. It\'s barcode scanner expanded to anything visual.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7495414312',
    created: 1262904856000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jclingan" rel="noopener noreferrer" target="_blank">@jclingan</a> eApps is awesome. Everyone should consider them if you are looking for a hosting provider that takes the job seriously.<br><br>In reply to: <a href="https://x.com/jclingan/status/7493929789" rel="noopener noreferrer" target="_blank">@jclingan</a> <span class="status">7493929789</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7493913618',
    created: 1262901912000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cagataycivici" rel="noopener noreferrer" target="_blank">@cagataycivici</a> If there\'s one piece of advise I can give you from my own journey, it would be "trust me, it will pay off"<br><br>In reply to: @cagataycivici <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7491982220',
    created: 1262898067000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> @brianleathem You will be able to teach it after reading chap 8 and 9 in Seam in Action ;)<br><br>In reply to: <a href="https://x.com/lightguardjp/status/7491300242" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">7491300242</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7470549556',
    created: 1262846537000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> If you tip toe around tx methods, it can be done, but it\'s ugly w/o manual fm. JPA spec leads continue to ignore problem.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/7470139243" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">7470139243</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7470199846',
    created: 1262845592000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> What\'s going to be done. Actually, it is pretty easy. <a class="mention" href="https://x.com/observes" rel="noopener noreferrer" target="_blank">@observes</a> AfterBeanDiscovery. Boot spring. Register beans as beans.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/7464871477" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">7464871477</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7470156138',
    created: 1262845474000,
    type: 'post',
    text: 'Just about got the hang of hard level for drums. I made a breakthrough when I discovered practice mode. Random banging -&gt; beats.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7470134098',
    created: 1262845417000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> I picked up the drum throne from Guitar Center. I didn\'t tell the guy I was going to use it for Rock Band ;)<br><br>In reply to: <a href="https://x.com/lincolnthree/status/7466170900" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">7466170900</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7470112405',
    created: 1262845359000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Good luck! And tell Pete to get on Twitter ;)<br><br>In reply to: <a href="https://x.com/lincolnthree/status/7467673796" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">7467673796</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7470077784',
    created: 1262845269000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I was just looking at that the other day. Didn\'t find away, but I argued it\'s better to use a <a class="mention" href="https://x.com/startup" rel="noopener noreferrer" target="_blank">@startup</a> EJB anyway.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/7469408379" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">7469408379</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7465254940',
    created: 1262834934000,
    type: 'post',
    text: 'My new drum throne. Time to rock!',
    photos: ['<div class="entry"><img class="photo" src="media/7465254940.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '7460075277',
    created: 1262824547000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> This falls under my initiative to fix the JCP, which is to have open mailinglists for all specs. Using weld-dev is wrong.<br><br>In reply to: <a href="https://x.com/JohnAment/status/7459288224" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">7459288224</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7460040698',
    created: 1262824480000,
    type: 'post',
    text: 'In Gmail autocomplete, the label for the e-mail address (Work, Home, Other, etc) isn\'t shown, so you end up sending to the wrong address.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7458818025',
    created: 1262822095000,
    type: 'post',
    text: '2nite I\'m going out to get a drum throne for playing Rock Band and some bottles of DuClaw\'s Devil\'s Milk. I love a good supply run mission.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7458755065',
    created: 1262821969000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Not even with this quicktime plugin? <a href="http://www.vorbis.com/setup_osx/" rel="noopener noreferrer" target="_blank">www.vorbis.com/setup_osx/</a><br><br>In reply to: <a href="https://x.com/lightguardjp/status/7458687709" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">7458687709</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7458732302',
    created: 1262821924000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> You should have Aslak on the call too. I\'ll show up if you invite me ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '7458717846',
    created: 1262821895000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> I would definitely ask him how #arquillian compares to Spring\'s AbstractSpringContextTests. For developers wanting to switch.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '7457867491',
    created: 1262820233000,
    type: 'post',
    text: 'Believe it or not, I\'ve hardly used bookmarks in the last two years. I mostly just keep 10,000 tabs open. Time to get organized.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7457729144',
    created: 1262819965000,
    type: 'post',
    text: 'Maven archetype was migrated to github: <a href="http://github.com/sonatype/sonatype-archetype" rel="noopener noreferrer" target="_blank">github.com/sonatype/sonatype-archetype</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7457703807',
    created: 1262819915000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> You should write in using Relax NG compact and then use trang to convert it. <a href="http://en.wikipedia.org/wiki/RELAX_NG" rel="noopener noreferrer" target="_blank">en.wikipedia.org/wiki/RELAX_NG</a><br><br>In reply to: <a href="https://x.com/aalmiray/status/7457473876" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">7457473876</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7457662490',
    created: 1262819835000,
    type: 'post',
    text: 'Are you cool with the changes to the CDI SPI listed here for 1st maintenance release? <a href="http://sfwk.org/Weld/PortableExtensionWishlist" rel="noopener noreferrer" target="_blank">sfwk.org/Weld/PortableExtensionWishlist</a> #cdi',
    likes: 0,
    retweets: 0,
    tags: ['cdi']
  },
  {
    id: '7457469174',
    created: 1262819466000,
    type: 'post',
    text: 'Do you think the JSF 2 project stage should be modifiable at runtime? Or do you see it as a deployment setting? We are debating #jsf',
    likes: 0,
    retweets: 0,
    tags: ['jsf']
  },
  {
    id: '7457380542',
    created: 1262819298000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jvanzyl" rel="noopener noreferrer" target="_blank">@jvanzyl</a> Actually, you could reduce it to just one step. Open an existing Maven 2 project. Done!<br><br>In reply to: <a href="https://x.com/jvanzyl/status/7457049798" rel="noopener noreferrer" target="_blank">@jvanzyl</a> <span class="status">7457049798</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7456990936',
    created: 1262818565000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jonobacon" rel="noopener noreferrer" target="_blank">@jonobacon</a> #rhythmbox has improved quite a lot. However, the fact that I can\'t edit mp3 tags still irks me. That\'s why I also use #exaile.<br><br>In reply to: <a href="https://x.com/jonobacon/status/7456431810" rel="noopener noreferrer" target="_blank">@jonobacon</a> <span class="status">7456431810</span>',
    likes: 0,
    retweets: 0,
    tags: ['rhythmbox', 'exaile']
  },
  {
    id: '7456952583',
    created: 1262818492000,
    type: 'post',
    text: 'Google considers Arquillian the test framework more important than Arquillian the alien species. I\'m just saying.',
    likes: 0,
    retweets: 2
  },
  {
    id: '7456931359',
    created: 1262818451000,
    type: 'post',
    text: 'Arquillian just got support for running tests in a standalone CDI environment. <a href="https://jira.jboss.org/jira/browse/ARQ-54" rel="noopener noreferrer" target="_blank">jira.jboss.org/jira/browse/ARQ-54</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7456530653',
    created: 1262817681000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I make it a point not to rip to mp3 (even though I have to buy them). I just want to make sure I rip songs at good quality.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/7455262077" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">7455262077</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7454964861',
    created: 1262814611000,
    type: 'post',
    text: 'I ripped a CD last night then listened to both the CD and the ogg files (quality 5) w/ my noise-canceling Bose headphones; no difference',
    likes: 0,
    retweets: 0
  },
  {
    id: '7453758174',
    created: 1262812110000,
    type: 'reply',
    text: '@brianleathem Empty config files FTW!<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7453666096',
    created: 1262811915000,
    type: 'post',
    text: 'Is anyone working on a CDI (JSR-299) or Java EE 6 book? We will promote it in the upcoming CDI dzone refcard. #cdi #javaee6 #refcard',
    likes: 0,
    retweets: 0,
    tags: ['cdi', 'javaee6', 'refcard']
  },
  {
    id: '7450958095',
    created: 1262806235000,
    type: 'post',
    text: 'A week-long online codecamp (forum-based) for Java EE 6 (Jan. 12th - 20th, 2010) <a href="http://www.javapassion.com/courses/javaee6codecamp.html" rel="noopener noreferrer" target="_blank">www.javapassion.com/courses/javaee6codecamp.html</a> #javaee6 #codecamp',
    likes: 0,
    retweets: 1,
    tags: ['javaee6', 'codecamp']
  },
  {
    id: '7450566926',
    created: 1262805406000,
    type: 'reply',
    text: '@brianleathem You can make faces-config.xml empty, but I recommend having one as it eliminates the need for the servlet mapping in web.xml.<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7446110861',
    created: 1262796237000,
    type: 'reply',
    text: '@brianleathem Events are more fine-grained than phase listeners; you can be sure you are intercepting the life cycle in the right context.<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7446094217',
    created: 1262796204000,
    type: 'reply',
    text: '@brianleathem Yeah, the most obvious candidate is the PreRenderViewEvent, which fires before the view is rendered.<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7419625061',
    created: 1262734288000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> You can use events for just about everything you used phase listeners for. I\'d like to see some performance comparisons.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/7418763461" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">7418763461</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7419597291',
    created: 1262734232000,
    type: 'post',
    text: 'If I were to play Uprising by Muse on the drums in Rock Band, I would definitely break the drums. My drums are happy it isn\'t available.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7418273090',
    created: 1262731600000,
    type: 'post',
    text: 'Wanna see some real talent. Slide to 2:00 in the video on this page: <a href="http://bowfire.com" rel="noopener noreferrer" target="_blank">bowfire.com</a> #sick #talent I saw their show 2 years ago.',
    likes: 0,
    retweets: 0,
    tags: ['sick', 'talent']
  },
  {
    id: '7417935089',
    created: 1262730924000,
    type: 'post',
    text: 'I just realized for the first time today that there is a link to "Expand All" messages in a thread/conversation in Gmail. #tunnelvision',
    likes: 0,
    retweets: 0,
    tags: ['tunnelvision']
  },
  {
    id: '7413225971',
    created: 1262720986000,
    type: 'post',
    text: 'If I deploy and archetype to #maven central, what does it take to get it listed in the master archetype catalog?',
    likes: 0,
    retweets: 0,
    tags: ['maven']
  },
  {
    id: '7413127930',
    created: 1262720771000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/crazybob" rel="noopener noreferrer" target="_blank">@crazybob</a> Hmm, I would consider getting a NexusOne, but only if I can do a family plan since my wife has the myTouch.<br><br>In reply to: <a href="https://x.com/crazybob/status/7412683660" rel="noopener noreferrer" target="_blank">@crazybob</a> <span class="status">7412683660</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7395379474',
    created: 1262672677000,
    type: 'post',
    text: 'It\'s irritating that I can\'t preview the whole mp3 on amazon. I know it may be abused, but I need full preview to disambiguate remixes.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7391589871',
    created: 1262664051000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mosabua" rel="noopener noreferrer" target="_blank">@mosabua</a> I gotcha. I just want people to recognize that this isn\'t giving up. It\'s just a refreshed approach to building a solid OS.<br><br>In reply to: <a href="https://x.com/mosabua/status/7381415484" rel="noopener noreferrer" target="_blank">@mosabua</a> <span class="status">7381415484</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7391512184',
    created: 1262663910000,
    type: 'post',
    text: '#avatar was spectacular! Definitely worth the wait for the IMAX 3D. I felt like I had a Na\'vi avatar of my own by the movie\'s end.',
    likes: 0,
    retweets: 0,
    tags: ['avatar']
  },
  {
    id: '7391347193',
    created: 1262663590000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jganoff" rel="noopener noreferrer" target="_blank">@jganoff</a> We def got hooked up. It\'s the AMC theater in Columbia, MD. Had to dry them off because they were fresh out of the glasses washer<br><br>In reply to: <a href="https://x.com/jganoff/status/7388252178" rel="noopener noreferrer" target="_blank">@jganoff</a> <span class="status">7388252178</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7391247563',
    created: 1262663400000,
    type: 'post',
    text: 'Does anyone have a revision &gt; 50 of a page titled "Change Log for JSR-000314" in your browser cache? (revision number is in footer). #jsf',
    likes: 0,
    retweets: 0,
    tags: ['jsf']
  },
  {
    id: '7383910192',
    created: 1262649543000,
    type: 'post',
    text: 'Entering Avatar 3D IMAX with futuristic glasses/face shield.',
    photos: ['<div class="entry"><img class="photo" src="media/7383910192.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '7378904123',
    created: 1262639090000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> And here is the youtube video: <a href="https://youtu.be/78FQDgcWzLI" rel="noopener noreferrer" target="_blank">youtu.be/78FQDgcWzLI</a><br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7378781800',
    created: 1262638826000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> The tweet answer is that debian has a central repository policy that works. Hunting down/reconciling RPMs is torture.<br><br>In reply to: <a href="https://x.com/maxandersen/status/7378147547" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">7378147547</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7376100920',
    created: 1262632756000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> Right, and my point has always been that the cenral repository policy in debian is already there and easy to model.<br><br>In reply to: <a href="https://x.com/jeremynorris/status/7375176978" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">7375176978</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7374872546',
    created: 1262629962000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mosabua" rel="noopener noreferrer" target="_blank">@mosabua</a> No, a Linux distro can be distinct even if it uses a common foundation. Red Hat focuses on SELinux, etc. It has value to add.<br><br>In reply to: <a href="https://x.com/mosabua/status/7374084221" rel="noopener noreferrer" target="_blank">@mosabua</a> <span class="status">7374084221</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7373669814',
    created: 1262627263000,
    type: 'post',
    text: 'I\'m going to propose something radical for the new decade. Red Hat should switch to a Debian foundation. Scrap RPMs. They suck.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7373433879',
    created: 1262626731000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jclingan" rel="noopener noreferrer" target="_blank">@jclingan</a> I don\'t know how useful running app servers on phones will be. Better is to create kick ass REST clients to hit a remote server.<br><br>In reply to: <a href="https://x.com/jclingan/status/7373194772" rel="noopener noreferrer" target="_blank">@jclingan</a> <span class="status">7373194772</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7373037224',
    created: 1262625846000,
    type: 'post',
    text: 'Ubuntu 9.10 makes adding an encrypted home directory (w/ ecrypt) so simple a novice can do it. <a href="http://www.linux-mag.com/cache/7568/1.html" rel="noopener noreferrer" target="_blank">www.linux-mag.com/cache/7568/1.html</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7373013594',
    created: 1262625791000,
    type: 'post',
    text: 'From what I can tell, the ext4 file system seems to be quite speedy. I\'m even working on top of a ecrypt home directory.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7372994338',
    created: 1262625746000,
    type: 'post',
    text: 'When I installed Ubuntu on my amd64 desktop, I inadvertently used the i386 CD. Frankly, I doubt I\'ll notice a difference; less headache.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7372935412',
    created: 1262625612000,
    type: 'post',
    text: 'I took a break from work over the holidays, but not from the computer. Spent most of my time "spring cleaning" my computers.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7372923898',
    created: 1262625585000,
    type: 'post',
    text: 'PrimeFaces 2.0 requires zero configuration in a Servlet 3.0 environment like Glassfish 3. (via <a class="mention" href="https://x.com/primefaces" rel="noopener noreferrer" target="_blank">@primefaces</a>)',
    likes: 0,
    retweets: 0
  },
  {
    id: '7372916597',
    created: 1262625568000,
    type: 'post',
    text: 'The import goal for Maven 2 archetypes is progressing nicely: <a href="http://jira.codehaus.org/browse/ARCHETYPE-273" rel="noopener noreferrer" target="_blank">jira.codehaus.org/browse/ARCHETYPE-273</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7372349833',
    created: 1262624308000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Stephan007" rel="noopener noreferrer" target="_blank">@Stephan007</a> Valerie just granted me access. I\'ll be in touch about leveraging the platform at JBoss!<br><br>In reply to: <a href="https://x.com/Stephan007/status/7371369523" rel="noopener noreferrer" target="_blank">@Stephan007</a> <span class="status">7371369523</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7372285026',
    created: 1262624165000,
    type: 'post',
    text: 'Drummers have their own emacs vs vi debate: traditional vs matched grip. It\'s not just us geeks that get into flame wars ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '7372029891',
    created: 1262623601000,
    type: 'post',
    text: 'Btw, the JavaScript at the beginning of <a href="https://java.dzone.com/articles/ajax-jsf-joined" rel="noopener noreferrer" target="_blank">java.dzone.com/articles/ajax-jsf-joined</a> is code I found in projects I had archived from previous gigs ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '7371982288',
    created: 1262623498000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> Great! Cross that off my todo list. Looks like I\'m not quite caught up yet.<br><br>In reply to: <a href="https://x.com/aschwart/status/7371931973" rel="noopener noreferrer" target="_blank">@aschwart</a> <span class="status">7371931973</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7371958215',
    created: 1262623446000,
    type: 'post',
    text: 'One tip that I find extremely helpful for future Linux upgrades/reinstalls is to maintain a list of any packages you added.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7371697542',
    created: 1262622851000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> Awesome. I look forward to moving the series along. I still have to get Nitin to publish Jay\'s Ajax article.<br><br>In reply to: <a href="https://x.com/aschwart/status/7370901935" rel="noopener noreferrer" target="_blank">@aschwart</a> <span class="status">7370901935</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7371675011',
    created: 1262622799000,
    type: 'post',
    text: 'I requested an enhancement to be able to save a search in Gnome. Issue report includes use case: <a href="https://bugzilla.gnome.org/show_bug.cgi?id=606029" rel="noopener noreferrer" target="_blank">bugzilla.gnome.org/show_bug.cgi?id=606029</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7370847705',
    created: 1262620943000,
    type: 'post',
    text: 'As a speaker, I\'m going to get access to the Parleys presos for Devoxx 2009. That should help me lobby to use them for our conferences ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '7370544276',
    created: 1262620289000,
    type: 'post',
    text: 'Here we go. New Year. New OS. New backup system. New headphones. New songs in library. Let\'s roll out.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7370453494',
    created: 1262620079000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> I held off until this morning. I\'m not totally ready to come back though. I was having too much fun drumming, I think ;)<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7370409823',
    created: 1262619983000,
    type: 'post',
    text: 'Ubuntu now tells you which programs are accessing a removable drive when you select "Safely Remove Drive" +1 for usability!',
    likes: 0,
    retweets: 0
  },
  {
    id: '7370372972',
    created: 1262619902000,
    type: 'post',
    text: 'I (re)discovered atunes last night. Killer x-platform music library app. A near drop-in replacement for passed away amaroK 1.4.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7356781787',
    created: 1262581325000,
    type: 'post',
    text: 'My wife is Lightening. I\'m Thunder. Our band: Shaggy Bunnies ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '7356765129',
    created: 1262581288000,
    type: 'post',
    text: 'Just finished playing Rock Band with my wife for ~ 3 hours. Good times. We traded off playing guitar and drums, though drums are my fav.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7356722450',
    created: 1262581200000,
    type: 'post',
    text: 'I finally have tickets to go see Avatar in IMAX 3D tomorrow night. There was quite a rush last week and tickets were always sold out.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7325079658',
    created: 1262501243000,
    type: 'post',
    text: 'I have a blister on every finger after dialing it up to hard on drums in Rock Band 2. I\'m getting quite a workout at that level ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '7310598216',
    created: 1262468284000,
    type: 'post',
    text: 'Finally, it\'s simple to theme your KDE apps using GTK+ theme in Ubuntu Karmic. Had to add systemsettings package, though. Why not default?',
    likes: 0,
    retweets: 0
  },
  {
    id: '7310520274',
    created: 1262468086000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jennifercord" rel="noopener noreferrer" target="_blank">@jennifercord</a> Absolutely. Gmail supports the two major e-mail types: IMAP and POP3. Outlook can connect to both. Lots of options there.<br><br>In reply to: <a href="https://x.com/jennifercord/status/7299622612" rel="noopener noreferrer" target="_blank">@jennifercord</a> <span class="status">7299622612</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7290691803',
    created: 1262408829000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jennifercord" rel="noopener noreferrer" target="_blank">@jennifercord</a> These people are thinking about this way too much. Twenty-ten it is. Let\'s move into it now ;)<br><br>In reply to: <a href="https://x.com/jennifercord/status/7288964552" rel="noopener noreferrer" target="_blank">@jennifercord</a> <span class="status">7288964552</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7280699230',
    created: 1262384496000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> 10 years ago (and some change) I first starting using Linux. I loved the adventure. Today Linux is just fun ;)<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7280656783',
    created: 1262384390000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cagataycivici" rel="noopener noreferrer" target="_blank">@cagataycivici</a> According to EL spec, you put a backslash before the #.<br><br>In reply to: @cagataycivici <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  }
])
