'use strict'

inflateTweets([
  {
    id: '1068506890',
    created: 1229747735000,
    type: 'post',
    text: 'I racked up 1000+ pictures from my dash around the French-speaking parts of Europe. My wife is currently uploading them to Flickr to share.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1068502788',
    created: 1229747534000,
    type: 'post',
    text: 'Just put the finishing touches on 4 #javaone abstracts and 1 #communityone abstract. I\'m hoping at least one is going to stick this year.',
    likes: 0,
    retweets: 0,
    tags: ['javaone', 'communityone']
  },
  {
    id: '1067465422',
    created: 1229707833000,
    type: 'post',
    text: 'Nightly snapshots of Seam documentation now available: <a href="http://docs.jboss.org/seam/snapshot/" rel="noopener noreferrer" target="_blank">docs.jboss.org/seam/snapshot/</a> Should help when using code from the HEAD.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1067448180',
    created: 1229707312000,
    type: 'post',
    text: 'New review for Seam in Action: <a href="http://www.thetechstatic.com/?p=224" rel="noopener noreferrer" target="_blank">www.thetechstatic.com/?p=224</a> "This book is highly recommended for academic and corporate libraries."',
    likes: 0,
    retweets: 0
  },
  {
    id: '1067409154',
    created: 1229706187000,
    type: 'post',
    text: 'US immigration official asked me where I worked. He knew Red Hat. He asked how it was going and said he wished their software was faster.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1067287622',
    created: 1229702650000,
    type: 'post',
    text: 'France has 4 train systems. Confusing at times, but diversity == great coverage.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1067285124',
    created: 1229702577000,
    type: 'post',
    text: 'Trip home: long walk &gt; metro (8) &gt; metro (4) &gt; RER (B) &gt; long walk in CDG &gt; security &gt; bus &gt; plane &gt; security &gt; plane &gt; customs &gt; security.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1067281484',
    created: 1229702473000,
    type: 'post',
    text: 'Vacuum sealing the clothes before putting in suitcase worked great during our trip. We had to pack 4 times moving from city to city.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1067266203',
    created: 1229702025000,
    type: 'post',
    text: 'Web frameworks are the modern Venus. We are obsessed with attaining the perfect form, thus producing a plethora of art in the wake.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1067263418',
    created: 1229701938000,
    type: 'post',
    text: 'Day 3: Le Louvre. About half way through I think I ascended into Heaven through one of the millions of Renaissance paintings.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1067261514',
    created: 1229701879000,
    type: 'post',
    text: 'My wife and I are attempting to speak en peu French badly.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1067260769',
    created: 1229701858000,
    type: 'post',
    text: 'Paris in winter == cold, cold, cold. It\'s charm is maintaining the balance.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1067259893',
    created: 1229701832000,
    type: 'post',
    text: 'Day 2: L\'Orsay. I have been influenced by the lowly impressionists. I admit to enjoying many encounters with Venus as well.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1067257534',
    created: 1229701761000,
    type: 'post',
    text: 'Day 1 (night) in France: We went to Champs-Élysées to see and be seen. Très français.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1067254595',
    created: 1229701675000,
    type: 'post',
    text: 'Paris is like Unix. Each store does one thing well. Boulangerie, boucherie, fromagier, pharmacie, fruits &amp; legemes. Then you chain them.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1067247952',
    created: 1229701482000,
    type: 'post',
    text: 'Home Sweet Paris',
    likes: 0,
    retweets: 0
  },
  {
    id: '1067247672',
    created: 1229701472000,
    type: 'post',
    text: 'At brewer tour at De Halve Maan. Incredibly entertaining. The reward at the end is the tasty Bruge Zot, the only beer brewed in Belgium.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1067244652',
    created: 1229701383000,
    type: 'post',
    text: 'Ironic, the only place I have successfully "borrowed" a WiFi connection is in Bruge, the most "historic" city I have visited thus far.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1067243672',
    created: 1229701357000,
    type: 'post',
    text: 'Connecting to a WiFi access point is a game and I hate playing it. Waste of time. Perhaps I\'m dumb, but my connection percentage is &lt; 10%.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1067242641',
    created: 1229701327000,
    type: 'post',
    text: 'Europeans don\'t play the stupid 99 cent game. 1 euro, 2 euro, 5 euro, 10 euro. Whole numbers. No tax. No gimmicks. No petty change ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '1067239927',
    created: 1229701245000,
    type: 'post',
    text: 'Europeans are intelligent. Why do they insist on smoking? Grrrr. I detest cigarette smoke. Not to mention that it screws with my breathing.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1055848801',
    created: 1229204565000,
    type: 'post',
    text: 'I\'ve had nothing but positive feedback about both my talk and book at Devoxx. Sure makes those late nights worth it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1055847607',
    created: 1229204505000,
    type: 'post',
    text: 'I honestly don\'t understand how the tram ticketing system in Belgium is enforced. Seems to be 100% honor-based. I\'m honest, trust me ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '1055846151',
    created: 1229204430000,
    type: 'post',
    text: 'Paring down my "must try" beer list. Westmalle triple, Affligem, Orval, Gueuze (lambic), Kriek (lambic, sweet). More to go.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1055833178',
    created: 1229203803000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danny_l" rel="noopener noreferrer" target="_blank">@danny_l</a> Heck if I know why Rotterdam. All I know is that you should have seen the heart in the foam. It was true love (of the coffee sort).<br><br>In reply to: @danny_l <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1055829301',
    created: 1229203601000,
    type: 'post',
    text: 'Enjoyed the first day of my long awaited vacation. Chocolate, beer, chocolate and more beer. Much captured with my Canon EOS and stomach.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1055828180',
    created: 1229203546000,
    type: 'post',
    text: 'Great talk by Adem Bien on Fri. I identify (and laugh) strongly with his sarcastic style. Realism like his is why Java EE is improving.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1055824519',
    created: 1229203374000,
    type: 'post',
    text: 'Interviewed by Ted Neward at Devoxx on Fri. My mind is quite unpredictable. I speak what passes through it. Expect the unexpected.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1049407485',
    created: 1228926030000,
    type: 'post',
    text: 'easyb is now high on my already dense list of things to get into. easyb + a groovier SeamTest would be a real win.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1049123797',
    created: 1228916663000,
    type: 'post',
    text: 'No coverage of XSRF in OSWAP talk. Damn, so much more critical than XSS and SQLi.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1049118059',
    created: 1228916432000,
    type: 'post',
    text: 'I dislike when XSS vulnerabilities are demoed using JavaScript\'s alert method. Not a good proof of concept. Show cookie stealing.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1048955923',
    created: 1228908058000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> announcing the completion of his masterpiece at #devoxx. <a href="https://www.flickr.com/photos/dhardiker/3095207690/in/pool-devoxx08" rel="noopener noreferrer" target="_blank">www.flickr.com/photos/dhardiker/3095207690/in/pool-devoxx08</a><br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/1048952526" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">1048952526</span>',
    likes: 0,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '1048952378',
    created: 1228907839000,
    type: 'post',
    text: 'The Seam wolf in the flesh: <a href="https://www.flickr.com/photos/dhardiker/3096073381/in/pool-devoxx08" rel="noopener noreferrer" target="_blank">www.flickr.com/photos/dhardiker/3096073381/in/pool-devoxx08</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1048951757',
    created: 1228907798000,
    type: 'post',
    text: 'Even superman (<a class="mention" href="https://x.com/maxanderson" rel="noopener noreferrer" target="_blank">@maxanderson</a>) came to speak at #devoxx. 1 of 4 talks. <a href="https://www.flickr.com/photos/dhardiker/3095182890/in/pool-devoxx08" rel="noopener noreferrer" target="_blank">www.flickr.com/photos/dhardiker/3095182890/in/pool-devoxx08</a>',
    likes: 0,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '1048949101',
    created: 1228907632000,
    type: 'post',
    text: 'Check out this hip dude (<a class="mention" href="https://x.com/aglover" rel="noopener noreferrer" target="_blank">@aglover</a>) speaking at #devoxx <a href="https://www.flickr.com/photos/dhardiker/3097156298/in/pool-devoxx08" rel="noopener noreferrer" target="_blank">www.flickr.com/photos/dhardiker/3097156298/in/pool-devoxx08</a>',
    likes: 0,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '1048942092',
    created: 1228907162000,
    type: 'post',
    text: 'Logica coffee stand at #devoxx is a must visit. They flew in barista from Rotterdam and you get a heart in your foam.',
    likes: 0,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '1048938583',
    created: 1228906938000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Hmm, I knew I should have doused my book in Axe.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1048936673',
    created: 1228906822000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jclingan" rel="noopener noreferrer" target="_blank">@jclingan</a> Mark Proctor lost his whole presentation for Devoxx because of a crash in Windows/Word. You are not alone, not just StarOffice.<br><br>In reply to: <a href="https://x.com/jclingan/status/1048165168" rel="noopener noreferrer" target="_blank">@jclingan</a> <span class="status">1048165168</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1048935618',
    created: 1228906769000,
    type: 'post',
    text: 'Lost track of time yesterday and didn\'t get back to the hotel to meet my wife when she arrived. Found her sleeping in the lobby for 2 hrs!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1048934339',
    created: 1228906675000,
    type: 'post',
    text: 'Did my first book signing yesterday. Got about 20 visitors. I love meeting my readers and knowing that I was able to make a positive impact.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1048932738',
    created: 1228906550000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jclingan" rel="noopener noreferrer" target="_blank">@jclingan</a> OpenOffice.org 3 has been much more stable for me. I can\'t tell you the pain I went through writing my book w/ OO.org 2. Lock up!<br><br>In reply to: <a href="https://x.com/jclingan/status/1048202797" rel="noopener noreferrer" target="_blank">@jclingan</a> <span class="status">1048202797</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1048931995',
    created: 1228906503000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aglover" rel="noopener noreferrer" target="_blank">@aglover</a> and I checked out a cybercafe. Stupid Belgium keyboards. Took 15 to type our passwords. Just different enough to make you retarded.<br><br>In reply to: <a href="https://x.com/aglover/status/1045108618" rel="noopener noreferrer" target="_blank">@aglover</a> <span class="status">1045108618</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1048928855',
    created: 1228906304000,
    type: 'post',
    text: 'Seam in Action talk went great. Amazing, 3 hours still wasn\'t enough for me. I need to learn to be more terse I think.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1044864135',
    created: 1228734671000,
    type: 'post',
    text: 'Electronics stores are so much better than what we have in the US. It\'s like what would happen if Apple bought Best Buy.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1044849915',
    created: 1228733644000,
    type: 'post',
    text: 'I\'ve now flown both British Airways and Air France. The winner: Air France. But Virgin Atlantic still tops both. Airbus over Boeing too.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1044817607',
    created: 1228731255000,
    type: 'post',
    text: 'I forgot my point and shoot camera. My wife will be bringing our new DSLR but I could have captured some things for the memories at least.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1044803058',
    created: 1228730150000,
    type: 'post',
    text: 'I was getting tons of spam in my gmail. I added one simple search and it killed 99% of it. filter: "click here to view as a webpage"',
    likes: 0,
    retweets: 0
  },
  {
    id: '1044800956',
    created: 1228729988000,
    type: 'post',
    text: 'It\'s so bizarre to be where you can\'t read and you don\'t understand what anyone is saying. I guess this is what it\'s like to be illiterate.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1044797799',
    created: 1228729772000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Good tip about the ports. However, it is easier to just add another IP address and run one instance per IP.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/1042250825" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">1042250825</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1044796674',
    created: 1228729686000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jclingan" rel="noopener noreferrer" target="_blank">@jclingan</a> Awesome, I\'m definitely going to be bookmarking that one. I got Seam+JBoss AS cluster going, next up is GlassFish.<br><br>In reply to: <a href="https://x.com/jclingan/status/1042523357" rel="noopener noreferrer" target="_blank">@jclingan</a> <span class="status">1042523357</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1044794702',
    created: 1228729552000,
    type: 'post',
    text: 'Terminal 5 at Heathrow is a mall with an airport connected to it. Just try not to buy anything, I dare you. They dare you.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1044793711',
    created: 1228729477000,
    type: 'post',
    text: 'I pronounced the Redskin\'s season over after last week. This week they sealed the deal. We\'ll, now I can focus on my adventures in Europe.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1044789225',
    created: 1228729135000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Got a System 76 for my wife 2 years ago. It\'s been pretty solid, though I wish the Asus hardware was a bit more robust. Then ...<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1044787423',
    created: 1228728998000,
    type: 'post',
    text: 'Read the book Tribes by Seth Godin on the plane. Helps us remember why we got into open source. Here\'s to my tribe (where ever they may be)!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1044784702',
    created: 1228728816000,
    type: 'post',
    text: 'I\'m finally back to a network connection after my long trip to Antwerp, Belgium. Blood is flowing through my computer again!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1042876643',
    created: 1228617782000,
    type: 'post',
    text: 'Still sitting on runway. Had to de-ice. Make sure to relax tommorrow! You have a big week ahead!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1042756047',
    created: 1228611575000,
    type: 'post',
    text: 'Heading off to Belgium for the Devoxx conference. Snow at departure, likely snow expected at destination.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1032359153',
    created: 1228143869000,
    type: 'post',
    text: 'Already starting to get New Year\'s wrap ups. Heck, Christmas is old news, we need to get start w/ the next holiday.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1032356092',
    created: 1228143760000,
    type: 'post',
    text: 'Spent time hanging out with <a class="mention" href="https://x.com/gscottshaw" rel="noopener noreferrer" target="_blank">@gscottshaw</a> and family at his son\'s b-day party. Both father and son are enjoying lots of shiny new toys.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1032352724',
    created: 1228143640000,
    type: 'post',
    text: 'Back to the grind today. I was very happy to see that RedHat/JBoss employees honor the break. I hate coming back to a pile of e-mail.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1032350000',
    created: 1228143549000,
    type: 'post',
    text: 'Optimizing imports on project in Eclipse 3.3 removes all imports on package-info.java files. Hope that is fixed in 3.4.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1032349106',
    created: 1228143512000,
    type: 'post',
    text: 'I really need to get my talk submissions done. The rush is on. I gotta get used to this advanced planning. It\'s new for me.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1032339390',
    created: 1228143132000,
    type: 'post',
    text: 'Thanksgiving was wacky and fun. We had a free-range turkey from the family farm and played an intense card game for 3+ hours. Good beer too.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1032337077',
    created: 1228143046000,
    type: 'post',
    text: 'Going out to shop is sad though, because no matter how many deals and coupons you have, Amazon beats the price. So what\'s the point?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1032335951',
    created: 1228143007000,
    type: 'post',
    text: 'Black Friday is dangerous for me because I get fixated on things like this and I am very competitive, so I don\'t like to miss a deal.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1032333819',
    created: 1228142931000,
    type: 'post',
    text: 'I decided to join the Black Friday madness this year. It was interesting, and a bit dangerous. The good news is my shopping is nearly done.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1025270837',
    created: 1227734664000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jclingan" rel="noopener noreferrer" target="_blank">@jclingan</a> Oh, I am very interesting in the gf cluster. Will help our cluster efforts w/ Seam.<br><br>In reply to: <a href="https://x.com/jclingan/status/1024938523" rel="noopener noreferrer" target="_blank">@jclingan</a> <span class="status">1024938523</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1024733534',
    created: 1227715419000,
    type: 'post',
    text: 'Chapter 14 and 15 of Seam in Action released in final PDF <a href="http://manning.com/dallen" rel="noopener noreferrer" target="_blank">manning.com/dallen</a>. Book is officially done.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1024729943',
    created: 1227715300000,
    type: 'post',
    text: 'I got a set of M-Audio speakers for my workstation yesterday. My first desktop setup I have ever had (w/ docking station). Now need a chair.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1024726402',
    created: 1227715188000,
    type: 'post',
    text: 'I have a Nespresso latte each morning and read news on my PepperPad. Love the pad, but company went under so no more updates. Sad.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1024722949',
    created: 1227715064000,
    type: 'post',
    text: 'I documented how to use <a class="mention" href="https://x.com/in" rel="noopener noreferrer" target="_blank">@in</a> to inject one Seam EJB component into another in standard Java EE 5 environments. Requires XML, blah!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1024720203',
    created: 1227714961000,
    type: 'post',
    text: 'Back to tech, I have been using JBoss AS 5 this week. Seems very stable and adheres to the spec. I\'m excited! I still like GlassFish too.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1024717206',
    created: 1227714857000,
    type: 'post',
    text: 'We visited friends on Sat night; a dinner party. We felt like normal people for a day. That\'s what normal people do, right?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1024714516',
    created: 1227714762000,
    type: 'post',
    text: 'Why are there so many conference submissions due during the Holiday season? Maybe an excuse to avoid "parties".',
    likes: 0,
    retweets: 0
  },
  {
    id: '1024710628',
    created: 1227714632000,
    type: 'post',
    text: 'My dishwasher sounds like a tank. I pray it dies every time it runs so I have a valid reason to replace it. Noise is bad for my environment.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1024704015',
    created: 1227714416000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jclingan" rel="noopener noreferrer" target="_blank">@jclingan</a> Do the have to put Raiders in 3D in order to get fans to watch the game? Here in Washington we watch because we might win.<br><br>In reply to: <a href="https://x.com/jclingan/status/1021894734" rel="noopener noreferrer" target="_blank">@jclingan</a> <span class="status">1021894734</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1024696692',
    created: 1227714158000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jbossnews" rel="noopener noreferrer" target="_blank">@jbossnews</a> ...and in the bar scene. Geekspeak gets passionate there. The question is, who will be coolest cat. Emmanuel? Max?<br><br>In reply to: <a href="https://x.com/RHMiddleware/status/1023687635" rel="noopener noreferrer" target="_blank">@RHMiddleware</a> <span class="status">1023687635</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1024687671',
    created: 1227713844000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Haha, just when you thought you didn\'t have to write anything new ;) Bring it home!<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/1024051737" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">1024051737</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1020913439',
    created: 1227538083000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Hoarder" rel="noopener noreferrer" target="_blank">@Hoarder</a> No, you shouldn\'t blame yourself. You want good apps that make life better. Apple just poisons you\'re water w/ control.<br><br>In reply to: <a href="https://x.com/VaskinK/status/1018032889" rel="noopener noreferrer" target="_blank">@VaskinK</a> <span class="status">1018032889</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1020907648',
    created: 1227537855000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> I feel you\'re pain man. Trust me though, the euphoria of finishing will last for a loooong time.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/1019607969" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">1019607969</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1017185961',
    created: 1227301879000,
    type: 'post',
    text: 'The iPhone store is evil. Don\'t you realize that Apple is tricking you into using closed-source proprietary software? History is repeating!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1017182596',
    created: 1227301738000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> It\'s a good read. My only complaint is that it falls off a cliff in the last two sections. A miracle occurs I guess.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/1016863200" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">1016863200</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1017180228',
    created: 1227301645000,
    type: 'post',
    text: 'Dan\'s rules for project names #1: Make it phonetic. Have you ever had a conversation about Acegi? Hoped the other person would say it first?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1015942201',
    created: 1227239495000,
    type: 'post',
    text: 'I\'m loving the heck out of Skype. The fact it ain\'t OSS burns inside, but I\'ve wanted effortless video communication for so long. It\'s here!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1015927091',
    created: 1227238779000,
    type: 'post',
    text: 'Believe it or not, the smoke alarm in my house went off for two cycles mid-way through the talk. Those demo gods sure don\'t like me much.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1015926226',
    created: 1227238735000,
    type: 'post',
    text: 'Was on GlassFish TV today talking about Seam and GlassFish (deploying A -&gt; B). <a href="http://wikis.sun.com/display/TheAquarium/SeamAndGlassFish" rel="noopener noreferrer" target="_blank">wikis.sun.com/display/TheAquarium/SeamAndGlassFish</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1015923912',
    created: 1227238623000,
    type: 'post',
    text: 'My Gmail is looking very shiny (and sleek) today. I went with the Shiny theme.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1009128557',
    created: 1226897703000,
    type: 'post',
    text: 'Decide it\'s time to get serious about photos. Studied cameralabs.com and played with models at Circuit City. Chose the Canon XSi.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1009124346',
    created: 1226897418000,
    type: 'post',
    text: 'Seam talk went well on Wednesday. Passed the 2 hour mark. A nice warmup for my 3 hour stand at Devoxx.',
    likes: 0,
    retweets: 0
  },
  {
    id: '999455397',
    created: 1226354724000,
    type: 'post',
    text: 'I\'m giving a Seam talk Wed night at the NovaJUG meeting.',
    likes: 0,
    retweets: 0
  },
  {
    id: '999450840',
    created: 1226354533000,
    type: 'post',
    text: 'Wow, just notice that gas dipped below $2 in Laurel, MD!',
    likes: 0,
    retweets: 0
  },
  {
    id: '991230980',
    created: 1225865807000,
    type: 'post',
    text: 'Yeah America! And I say to the rest of the world, "we\'re back!"',
    likes: 0,
    retweets: 0
  },
  {
    id: '991230178',
    created: 1225865773000,
    type: 'post',
    text: 'At moments like this, people say "I hope I never wake from this dream." For me, I\'m too excited to go to sleep. But tomorrow IS a new day!',
    likes: 0,
    retweets: 0
  },
  {
    id: '990446837',
    created: 1225846516000,
    type: 'post',
    text: 'A car ran a red light and came so close to slamming in to me I am still shaking. I am so thankful for the breaks on my Prius.',
    likes: 0,
    retweets: 0
  },
  {
    id: '989545629',
    created: 1225814561000,
    type: 'post',
    text: 'In line waiting to vote. When can I vote on enabling online voting? Seriously.',
    likes: 0,
    retweets: 0
  },
  {
    id: '988447205',
    created: 1225756316000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> Gregor is better than email. At least it gets non-tech people used to going to a central repository to get something.<br><br>In reply to: <a href="https://x.com/dhanji/status/986969141" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">986969141</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '988442586',
    created: 1225756101000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SUBZERO66" rel="noopener noreferrer" target="_blank">@SUBZERO66</a> Amen! Not to mention you don\'t get those deep cuts on your hands. Some packages now at least use perforation in the plastic.<br><br>In reply to: <a href="https://x.com/bmuschko/status/987542417" rel="noopener noreferrer" target="_blank">@bmuschko</a> <span class="status">987542417</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '988436911',
    created: 1225755824000,
    type: 'post',
    text: 'Get JBoss Developer Studio 2 Beta 2 for free! <a href="https://inquiries.redhat.com/go/redhat/JBDS2?sc_cid=70160000000H51dAAC" rel="noopener noreferrer" target="_blank">inquiries.redhat.com/go/redhat/JBDS2?sc_cid=70160000000H51dAAC</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '985879705',
    created: 1225592891000,
    type: 'post',
    text: 'I\'m loving my 22" monitor. I didn\'t think I would appreciate it much, but I do! I have sooooo much real estate! Need me to lend you some?',
    likes: 0,
    retweets: 0
  },
  {
    id: '980658197',
    created: 1225293979000,
    type: 'post',
    text: 'At a EclipseWorld in Reston. Listening to talk on Mylyn. I really like how Brian Sam-Bodden does his slides, doesn\'t use a fixed template.',
    likes: 0,
    retweets: 0
  },
  {
    id: '978322349',
    created: 1225158603000,
    type: 'post',
    text: 'After skipping a night of sleep, I finally have RHEL 5 setup perfectly. Just...don\'t...screw...it...up...now.',
    likes: 0,
    retweets: 0
  },
  {
    id: '978320879',
    created: 1225158530000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Hahaha. That\'s the welcome package for Red Hat. Inside are my favorite things in the world: forms. Not!<br><br>In reply to: <a href="https://x.com/maxandersen/status/971067921" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">971067921</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '978312413',
    created: 1225158118000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Couldn\'t agree with you more. It\'s amazing how bad Word is allowed to be in today\'s competitive software market.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/974056842" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">974056842</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '971048241',
    created: 1224708670000,
    type: 'post',
    text: 'Quick Link plugin from Gmail Labs forgets links after you logout. I was hoping I could use it to save my "ham search". No luck yet.',
    likes: 0,
    retweets: 0
  },
  {
    id: '971039608',
    created: 1224708343000,
    type: 'post',
    text: 'I thought the pilot was going to kill us all on the decent into Wash-Reagan on a prop plane. The woman next to me was bracing for her life.',
    likes: 0,
    retweets: 0
  },
  {
    id: '971031024',
    created: 1224708018000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/crazybob" rel="noopener noreferrer" target="_blank">@crazybob</a> I\'m definitely getting one.<br><br>In reply to: <a href="https://x.com/crazybob/status/970725059" rel="noopener noreferrer" target="_blank">@crazybob</a> <span class="status">970725059</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '971029520',
    created: 1224707958000,
    type: 'post',
    text: 'A new chapter begins: <a href="https://mojavelinux.com/blog/archives/2008/10/a_new_chapter_begins/" rel="noopener noreferrer" target="_blank">mojavelinux.com/blog/archives/2008/10/a_new_chapter_begins/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '963467511',
    created: 1224223392000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Keep buying those books! My wife once told me that writers are broke because they spend all their money supporting other writers.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '963465983',
    created: 1224223268000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gscottshaw" rel="noopener noreferrer" target="_blank">@gscottshaw</a> That\'s like incorporating design patterns into your software application with little regard for the application\'s goals.<br><br>In reply to: <a href="https://x.com/gscottshaw/status/962576656" rel="noopener noreferrer" target="_blank">@gscottshaw</a> <span class="status">962576656</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '963463514',
    created: 1224223057000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> You really have to put on your thinking cap to read through the WebBeans spec. But expect to get smarter for the effort.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '963460728',
    created: 1224222792000,
    type: 'post',
    text: 'Went sailing today with friends who live on a sailboat, if you can believe it. Got to see 50 schooners start a race! <a href="http://schoonerrace.org" rel="noopener noreferrer" target="_blank">schoonerrace.org</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '961906840',
    created: 1224132146000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gscottshaw" rel="noopener noreferrer" target="_blank">@gscottshaw</a> There\'s only 5 top reasons why Vista failed? Actually, I can narrow it down to just one. It\'s Windows.<br><br>In reply to: <a href="https://x.com/gscottshaw/status/954505578" rel="noopener noreferrer" target="_blank">@gscottshaw</a> <span class="status">954505578</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '960067945',
    created: 1224040263000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> A book store is where you go to drink coffee.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '960066028',
    created: 1224040157000,
    type: 'post',
    text: 'I was filling out an address for at sys-con and one of the options for country was USSR. Do they know something we don\'t?',
    likes: 0,
    retweets: 0
  },
  {
    id: '957291548',
    created: 1223878077000,
    type: 'post',
    text: 'Oh my bumbling \'skins. What happen today? Can\'t you just keep your hands on the ball when within scoring range?',
    likes: 0,
    retweets: 0
  },
  {
    id: '955870844',
    created: 1223761934000,
    type: 'post',
    text: 'Using conditionals in Ant is like using a reverse polish notation calculator. You have to decide the operator before the literal.',
    likes: 0,
    retweets: 0
  },
  {
    id: '955192517',
    created: 1223704420000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Enjoy yourself! And congratulations! I\'m sure the day will be perfect no matter what the weather!<br><br>In reply to: <a href="https://x.com/maxandersen/status/955176833" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">955176833</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '953417288',
    created: 1223593314000,
    type: 'post',
    text: 'I wrote in a seam-dev thread that Windows batch syntax is like those big Legos you give to 5 year olds.',
    likes: 0,
    retweets: 0
  },
  {
    id: '953411628',
    created: 1223592992000,
    type: 'post',
    text: 'A developer at my former employer told me I write the most perfect code he has ever seen. Only problem is, it takes me forever to write it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '953410647',
    created: 1223592932000,
    type: 'post',
    text: 'I haven\'t broadcasted to the world that I twitter yet. I\'m still debating with myself whether it is the right move.',
    likes: 0,
    retweets: 0
  },
  {
    id: '953401443',
    created: 1223592412000,
    type: 'post',
    text: 'Visited my former employer (CodeRyte) today to deliver a signed copy of Seam in Action. It was fun to enjoy a couple minutes of fame.',
    likes: 0,
    retweets: 0
  },
  {
    id: '951785895',
    created: 1223497098000,
    type: 'post',
    text: 'A former colleague is looking to hire a log collection engineer. Really? Is that a job?',
    likes: 0,
    retweets: 0
  },
  {
    id: '947931490',
    created: 1223274215000,
    type: 'post',
    text: 'Since the Ivy developers are good at making this suck less (i.e., Maven), do you think they could make the Ant manual suck less?',
    likes: 0,
    retweets: 0
  },
  {
    id: '947927430',
    created: 1223273834000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> You didn\'t like the spoof of the Couric interviews with Palin? "I would like to use one of my lifelines."<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '947926952',
    created: 1223273784000,
    type: 'post',
    text: 'I couldn\'t have done it alone: <a href="http://www.amazon.com/gp/blog/A194EA6FA6IEU1/ref=cm_pdp_blog_post_title_1#postPMCA194EA6FA6IEU1at1223009845" rel="noopener noreferrer" target="_blank">www.amazon.com/gp/blog/A194EA6FA6IEU1/ref=cm_pdp_blog_post_title_1#postPMCA194EA6FA6IEU1at1223009845</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '947726824',
    created: 1223257974000,
    type: 'post',
    text: 'I swear, VPN is nothing but a humongous waste of time.',
    likes: 0,
    retweets: 0
  },
  {
    id: '946662467',
    created: 1223167946000,
    type: 'post',
    text: 'In Ant, the if condition cannot prevent dependent targets from running. What kind of sense does that make? <a href="http://ant.apache.org/manual/using.html#targets" rel="noopener noreferrer" target="_blank">ant.apache.org/manual/using.html#targets</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '945845913',
    created: 1223096127000,
    type: 'post',
    text: 'With the way things are going, there\'s only one way to express your feelings: <a href="http://2.bp.blogspot.com/_pSHP7VYSIjE/SK-0hK7cMKI/AAAAAAAAAKE/EA0n17hZHro/s1600-h/you-suck.jpg" rel="noopener noreferrer" target="_blank">2.bp.blogspot.com/_pSHP7VYSIjE/SK-0hK7cMKI/AAAAAAAAAKE/EA0n17hZHro/s1600-h/you-suck.jpg</a> I agree with that kid. We all do.',
    likes: 0,
    retweets: 0
  },
  {
    id: '945636975',
    created: 1223080430000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> The seam-gen settings are copied into the project, but it\'s strictly to preserve them. We need to be able to run seam from there.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '945513354',
    created: 1223072014000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jbandi" rel="noopener noreferrer" target="_blank">@jbandi</a> My 3-IDEs are NetBeans, Eclipse, and IntelliJ. I don\'t use a Mac, so no TextMate. You could count VIM as a 3.5.<br><br>In reply to: <a href="https://x.com/jbandi/status/941602242" rel="noopener noreferrer" target="_blank">@jbandi</a> <span class="status">941602242</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '945512187',
    created: 1223071940000,
    type: 'post',
    text: 'Try out deploying your seam-gen project to GlassFish: <a href="https://mojavelinux.com/blog/archives/2008/10/deploying_a_seamgen_project_to_glassfish/" rel="noopener noreferrer" target="_blank">mojavelinux.com/blog/archives/2008/10/deploying_a_seamgen_project_to_glassfish/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '945511487',
    created: 1223071892000,
    type: 'post',
    text: 'Give your seam-gen projects longevity by using Ivy to manage dependencies: <a href="https://in.relation.to/2008/10/03/managing-the-dependencies-of-a-seamgen-project-with-ivy/" rel="noopener noreferrer" target="_blank">in.relation.to/2008/10/03/managing-the-dependencies-of-a-seamgen-project-with-ivy/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '945508173',
    created: 1223071665000,
    type: 'post',
    text: 'I don\'t want to burn posts talking about Palin, but here is what I was getting at: <a href="http://weblog.sinteur.com/wp-content/uploads/2008/10/2909496470_d751e8a3dc.jpg" rel="noopener noreferrer" target="_blank">weblog.sinteur.com/wp-content/uploads/2008/10/2909496470_d751e8a3dc.jpg</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '944566705',
    created: 1223012067000,
    type: 'post',
    text: 'I want to modify that last statement. What gets me is why people aren\'t criticizing Palin for not answering the questions and rebutting.',
    likes: 0,
    retweets: 0
  },
  {
    id: '944538665',
    created: 1223009750000,
    type: 'post',
    text: 'I can\'t fathom why anyone thinks Palin did good. She\'s so freakin\' annoying. Perhaps I\'m showing my Ivy League, but Biden was on the money.',
    likes: 0,
    retweets: 0
  },
  {
    id: '944536848',
    created: 1223009601000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> I\'m pscyhed about EL completion. That\'s the main driver that will win me over. My only bone with IDEs is that they are slow.<br><br>In reply to: <a href="https://x.com/maxandersen/status/943710013" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">943710013</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '943381128',
    created: 1222960567000,
    type: 'post',
    text: 'They should put a picture of water cooler on the Twitter website.',
    likes: 0,
    retweets: 0
  },
  {
    id: '943380428',
    created: 1222960538000,
    type: 'post',
    text: 'Both Borders and B&amp;N stopped putting out JDJ. What\'s a Java guy to read? I guess I\'m stuck with Nature magazines.',
    likes: 0,
    retweets: 0
  },
  {
    id: '943375095',
    created: 1222960283000,
    type: 'post',
    text: 'There is one good reason to by Seam in Action. It\'s ILLUSTRATED, according to Amazon. Not the word I would use, but I\'m not going to argue.',
    likes: 0,
    retweets: 0
  },
  {
    id: '943372318',
    created: 1222960147000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Alright! I hope you agree that I am more of a writer than a diver. I just about threw out my back in Tuscany. I deserved it ;)<br><br>In reply to: <a href="https://x.com/maxandersen/status/942114636" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">942114636</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '943365907',
    created: 1222959861000,
    type: 'post',
    text: 'My wife is out of town this week and I am having to fend for myself. Distance makes a man\'s instinct grow more domestic.',
    likes: 0,
    retweets: 0
  },
  {
    id: '942967568',
    created: 1222930550000,
    type: 'post',
    text: 'Watching Poker After Dark is like watching programmers write code. Could you imagine a show where you watch the Seam developers work?',
    likes: 0,
    retweets: 0
  },
  {
    id: '941638504',
    created: 1222848383000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> His book is great. Makes you laugh because you have seen his stories play out many times.<br><br>In reply to: <a href="https://x.com/maxandersen/status/941522984" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">941522984</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '941637369',
    created: 1222848284000,
    type: 'post',
    text: 'Linux in 2001: A means to world liberation! <a href="https://www.google.com/search2001/search?q=linux&amp;hl=en&amp;btnG=Search" rel="noopener noreferrer" target="_blank">www.google.com/search2001/search?q=linux&amp;hl=en&amp;btnG=Search</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '941227650',
    created: 1222817927000,
    type: 'post',
    text: 'I\'ve been playing with Ivy lately and I will be writing about it soon. It definitely has utility.',
    likes: 0,
    retweets: 0
  },
  {
    id: '941225959',
    created: 1222817820000,
    type: 'post',
    text: 'I have a resident spider that insists on building its web right outside my front door. Ahhh!',
    likes: 0,
    retweets: 0
  },
  {
    id: '939617140',
    created: 1222716462000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aglover" rel="noopener noreferrer" target="_blank">@aglover</a> I gave buildr a try once and immediately discarded it for being poorly documented an unstable. Hopefully they have progressed.<br><br>In reply to: <a href="https://x.com/aglover/status/938165425" rel="noopener noreferrer" target="_blank">@aglover</a> <span class="status">938165425</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '939615556',
    created: 1222716377000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> btw, I am keeping a collection of all links related to Seam here: <a href="http://delicious.com/seaminaction" rel="noopener noreferrer" target="_blank">delicious.com/seaminaction</a> (also <a href="http://manning.com/dallen" rel="noopener noreferrer" target="_blank">manning.com/dallen</a>)<br><br>In reply to: <a href="https://x.com/dhanji/status/938989745" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">938989745</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '939614385',
    created: 1222716320000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> I think your best option is to forward them to the book, just as I directed my readers to Java Persistence w/Hibernate.<br><br>In reply to: <a href="https://x.com/dhanji/status/938989745" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">938989745</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '939612252',
    created: 1222716218000,
    type: 'post',
    text: 'I find myself sliding naturally into NetBeans. I just seems to do what I want when I want it to happen. That said, I am still a 3-IDE man.',
    likes: 0,
    retweets: 0
  },
  {
    id: '937858778',
    created: 1222581728000,
    type: 'post',
    text: '5 stars on Amazon and life is good ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '937858273',
    created: 1222581674000,
    type: 'post',
    text: 'Drinking beer out in the country today. Maryland Microbrew Festival. Founds some new favorites, but they ain\'t bottled :(',
    likes: 0,
    retweets: 0
  },
  {
    id: '934622461',
    created: 1222362996000,
    type: 'post',
    text: 'Discreet 2GB Pen? <a href="http://www.skymall.com/shopping/detail.htm?pid=102632616&amp;c=11050&amp;pnr=29H&amp;cm_mmc=Email-_-SkyMall-_-0922Computers29H-_-Pen" rel="noopener noreferrer" target="_blank">www.skymall.com/shopping/detail.htm?pid=102632616&amp;c=11050&amp;pnr=29H&amp;cm_mmc=Email-_-SkyMall-_-0922Computers29H-_-Pen</a> What do they expect you to be discreet about. The fact that it has porn on it?',
    likes: 0,
    retweets: 0
  },
  {
    id: '933151633',
    created: 1222271848000,
    type: 'post',
    text: 'Had a Raison d\'etre at Dogfish Head. It was definitely an evening to celebrate life.',
    likes: 0,
    retweets: 0
  },
  {
    id: '932274622',
    created: 1222209570000,
    type: 'post',
    text: 'Hangin out with steve saksa, paul duval and andy glover at dogfish haed goooooood beer',
    likes: 0,
    retweets: 0
  },
  {
    id: '923487363',
    created: 1221580317000,
    type: 'post',
    text: 'I love coming into a client\'s office and finding that I have no chair.',
    likes: 0,
    retweets: 0
  },
  {
    id: '919361587',
    created: 1221250315000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> As <a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> said, "I feel mad now because of how much time I wasted before."<br><br>In reply to: <a href="https://x.com/dhanji/status/918707328" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">918707328</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '919360551',
    created: 1221250257000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> To clarify, what I mean is that with Seam, I finally started to get shit done writing apps. And I see others doing the same.<br><br>In reply to: <a href="https://x.com/dhanji/status/918707328" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">918707328</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '919355409',
    created: 1221249959000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> Recall I wrote about Seam because I found it useful. I get shit done. The developer community is too academic. Life isn\'t perfect.<br><br>In reply to: <a href="https://x.com/dhanji/status/918707328" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">918707328</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '919351381',
    created: 1221249754000,
    type: 'post',
    text: 'Why is there no free hosting for Java EE open source projects. Is it that hard for sourceforge to offer JBoss AS, Tomcat or GlassFish?',
    likes: 0,
    retweets: 0
  },
  {
    id: '917546958',
    created: 1221134437000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> The righteous bijection debate. People just need to read chapter 6 of Seam in Action to "get it". Personally, I like it.<br><br>In reply to: <a href="https://x.com/dhanji/status/917403394" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">917403394</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '917544434',
    created: 1221134238000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> You might end up with 10. Either way, your car is never big enough to shuttle home what you buy there. At least they feed you.<br><br>In reply to: <a href="https://x.com/maxandersen/status/917508954" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">917508954</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '917543639',
    created: 1221134171000,
    type: 'post',
    text: 'A sign you have been doing a lot of coding. You can\'t find the "refactor" option in the context menu of your file explorer.',
    likes: 0,
    retweets: 0
  },
  {
    id: '915231728',
    created: 1220976284000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> I was thinking the same thing, but then realized that we shouldn\'t need an occasion to fly a flag. I guess we forgot that.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '915148529',
    created: 1220972239000,
    type: 'post',
    text: 'American flags on every bridge down 66 in Virginia. Nice show of pride in country. Go USA!',
    likes: 0,
    retweets: 0
  },
  {
    id: '911358458',
    created: 1220660438000,
    type: 'post',
    text: 'Evolution of a term: copy-paste is now wiki-driven development',
    likes: 0,
    retweets: 0
  },
  {
    id: '911358106',
    created: 1220660412000,
    type: 'post',
    text: 'I survived my marathon day at JSF One!!!! 3 talks, 1 expert panel, 1 BOF and lots of chatting in between.',
    likes: 0,
    retweets: 0
  },
  {
    id: '906958957',
    created: 1220372014000,
    type: 'post',
    text: 'The clash of the titans is about to begin: Seam and Spring Web Flow team members decend on the same turf. It should be a good match.',
    likes: 0,
    retweets: 0
  },
  {
    id: '906956456',
    created: 1220371870000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Amen to that! Our profession seems to be steady and slow, with lots of stacktraces thrown in for a little extra workout.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '903498621',
    created: 1220053642000,
    type: 'post',
    text: 'I have books!!!!!!!!!!!!!! The UPS dude sure knows how to brighten one\'s day!',
    likes: 0,
    retweets: 0
  },
  {
    id: '902231486',
    created: 1219965017000,
    type: 'post',
    text: 'It\'s out! <a href="https://mojavelinux.com/blog/archives/2008/08/seam_in_action_is_final_finally/" rel="noopener noreferrer" target="_blank">mojavelinux.com/blog/archives/2008/08/seam_in_action_is_final_finally/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '900707416',
    created: 1219857099000,
    type: 'post',
    text: 'Tabs in epiphany are retarded. You have to click arrows to page to overflow. My wife looks at me and goes: What is wrong with your browser?',
    likes: 0,
    retweets: 0
  },
  {
    id: '900677171',
    created: 1219855378000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> I gave up giving up caffine after I crossed the 6 months mark with Seam in Action.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '897512103',
    created: 1219604552000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> And MySQL is not a real database because the developers refuse to fix what is holding it back. H2 is more capable and it is 1MB.<br><br>In reply to: <a href="https://x.com/dhanji/status/897197320" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">897197320</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '897511209',
    created: 1219604472000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> Of course you are not misinformed. You\'re one of the elite in intelligence. But many folks truly don\'t understand the issue.<br><br>In reply to: <a href="https://x.com/dhanji/status/897197320" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">897197320</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '897508834',
    created: 1219604257000,
    type: 'post',
    text: 'I love the smell of stacktraces in the morning.',
    likes: 0,
    retweets: 0
  },
  {
    id: '897075866',
    created: 1219553671000,
    type: 'post',
    text: 'The men\'s 10m platform was a thrill! Sorry China, but I was glad to see another country nail a set of dives. Way to go Aussies!',
    likes: 0,
    retweets: 0
  },
  {
    id: '897074213',
    created: 1219553477000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> I sure wish I had the ability to create cool graphics. Me thinks I need to hire a sidekick. Anyway, that logo rox!<br><br>In reply to: <a href="https://x.com/maxandersen/status/896729809" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">896729809</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '897073046',
    created: 1219553356000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> Oh dear. Here\'s where the misinformation is shining through. Read JPwH or Seam IA chapter 8/9. We may never get the point across.<br><br>In reply to: <a href="https://x.com/dhanji/status/896160698" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">896160698</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '895887941',
    created: 1219436750000,
    type: 'post',
    text: 'I\'m feeling very alpha. I changed the brake pads on my Acura with some guidance from my father-in-law. So far so good.',
    likes: 0,
    retweets: 0
  },
  {
    id: '894856204',
    created: 1219355514000,
    type: 'post',
    text: 'I still edit XML using VIM because I don\'t agree with how XML code formatters work. Unfortunately, it burns a lot of time.',
    likes: 0,
    retweets: 0
  },
  {
    id: '894853485',
    created: 1219355325000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> Either way, don\'t ever call yourself a former open source contributor. Like an Olympian, once a contributor, always a contributor.<br><br>In reply to: <a href="https://x.com/mraible/status/894583236" rel="noopener noreferrer" target="_blank">@mraible</a> <span class="status">894583236</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '894850199',
    created: 1219355098000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> I like the EL expression substitution. One idea I\'ve had forever is to allow collection substitution so you can mock a table.<br><br>In reply to: <a href="https://x.com/maxandersen/status/894828990" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">894828990</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '894847254',
    created: 1219354887000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Who did the graphic in the blog entry?<br><br>In reply to: <a href="https://x.com/maxandersen/status/894828990" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">894828990</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '894846716',
    created: 1219354845000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Congrats man! Can\'t wait to try it out.<br><br>In reply to: <a href="https://x.com/maxandersen/status/894828990" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">894828990</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '892377985',
    created: 1219167715000,
    type: 'post',
    text: 'I love my country, don\'t mistake that. But I don\'t like the comment "America is the greatest country in the world." It hurts progress.',
    likes: 0,
    retweets: 0
  },
  {
    id: '892376094',
    created: 1219167581000,
    type: 'post',
    text: 'Desks that are too high give me an instant headache. I like keyboard trays that I can sit on (as a test of sturdiness).',
    likes: 0,
    retweets: 0
  },
  {
    id: '892373766',
    created: 1219167430000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> I really think Grails is cryptic, yes. I\'m going to study it more, I\'m not giving up. But Perl does magic stuff and it\'s horrible.<br><br>In reply to: <a href="https://x.com/aalmiray/status/891805938" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">891805938</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '892371396',
    created: 1219167275000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> Yeah, you avoid those drivers and one irons ;)<br><br>In reply to: <a href="https://x.com/mraible/status/892195619" rel="noopener noreferrer" target="_blank">@mraible</a> <span class="status">892195619</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '892369824',
    created: 1219167171000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> I like "how to save your DBA\'s butt" hahahaha!<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/892337100" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">892337100</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '890000536',
    created: 1218952831000,
    type: 'post',
    text: 'Phelps is speechless. I\'m speechless. Hooray for Maryland!',
    likes: 0,
    retweets: 0
  },
  {
    id: '889999539',
    created: 1218952708000,
    type: 'post',
    text: 'I finally sat down and read several chapters on Grails. I don\'t buy it. In fact, it\'s downright scary. It\'s like Perl: cryptic.',
    likes: 0,
    retweets: 0
  },
  {
    id: '889185208',
    created: 1218863492000,
    type: 'post',
    text: 'Michael Phelps == Tiger Woods is extremely accurate. Both leave you saying "Unbelievable" over and over. They only know one word: Win.',
    likes: 0,
    retweets: 0
  },
  {
    id: '888866809',
    created: 1218835209000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> So what\'s the east coast, China?<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/888781425" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">888781425</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '888619414',
    created: 1218818010000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/scottdavis99" rel="noopener noreferrer" target="_blank">@scottdavis99</a> Sarah is a beautiful name, but then again, I\'m biased.<br><br>In reply to: <a href="https://x.com/scottdavis99/status/888379213" rel="noopener noreferrer" target="_blank">@scottdavis99</a> <span class="status">888379213</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '888616857',
    created: 1218817841000,
    type: 'post',
    text: 'Why would Gmail say "We don\'t usually post about problems like this on our blog." To me, service is about transparency. Report all outages!',
    likes: 0,
    retweets: 0
  },
  {
    id: '887058841',
    created: 1218687977000,
    type: 'post',
    text: 'Whooooooooooooooooooooooooooooooohooooooooooooooooooooo!!!!!! I thought that project would never end! I\'m freeeeeeeeeeeeeeeeeeee!',
    likes: 0,
    retweets: 0
  },
  {
    id: '887058467',
    created: 1218687940000,
    type: 'post',
    text: 'I\'m freakin\' done! Hell Yeah! I\'m happier than the Chinese right now!And I stuck the landing right at the end. Seam in Action forever!',
    likes: 0,
    retweets: 0
  },
  {
    id: '884685721',
    created: 1218500245000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Btw, the IMAP access worked during that time. It was just the web interface that was down.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/884607008" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">884607008</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '884685391',
    created: 1218500222000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> So the gmail error wasn\'t just me. Came at a bad time since I am trying to publish tomorrow! Oh, the stress!<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/884607008" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">884607008</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '883789853',
    created: 1218426341000,
    type: 'post',
    text: 'Smash that France! Lezak had red, white, and blue flowing through his veins on that last 50! 8 golds for Phelps, here it comes!',
    likes: 0,
    retweets: 0
  },
  {
    id: '883396928',
    created: 1218389090000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> I\'m saying that the e-mails I typically get from travel agencies are unreadable because they send raw reports ;) Experts!<br><br>In reply to: <a href="https://x.com/maxandersen/status/883392885" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">883392885</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '882298741',
    created: 1218262558000,
    type: 'post',
    text: 'del.icio.us got a face lift, and a URL that doesn\'t make me type like a I have studder.',
    likes: 0,
    retweets: 0
  },
  {
    id: '881931895',
    created: 1218229741000,
    type: 'post',
    text: 'TiVo and the internet are a paradox. The 1st can pause time, but not the 2nd. Makes the internet a spoiler. Messes up Olympics viewing.',
    likes: 0,
    retweets: 0
  },
  {
    id: '881928940',
    created: 1218229505000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> Best of luck! Be sure to correct *anything* you can during copy editing because they will not be happy if you try to change the PDF!<br><br>In reply to: <a href="https://x.com/dhanji/status/880195996" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">880195996</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '881927046',
    created: 1218229366000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> You mean no more illegible emails to send to your friends containing SQLPlus output? Thanks for the tip, btw.<br><br>In reply to: <a href="https://x.com/maxandersen/status/881474862" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">881474862</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '880040561',
    created: 1218081929000,
    type: 'post',
    text: 'I feel like I have been timewarped back to the 70s when I am on JavaRanch. It really is frightening. Take me back to seamframework.org!',
    likes: 0,
    retweets: 0
  },
  {
    id: '880039114',
    created: 1218081811000,
    type: 'post',
    text: 'I just had authentic Italic pasta and olive oil that I brought back from Rome. Mmmmmmmm.',
    likes: 0,
    retweets: 0
  },
  {
    id: '880034915',
    created: 1218081470000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> I can\'t count the number of times I said "I hope I can finish the book before..." I\'m just happy to say the end is eminent now.<br><br>In reply to: <a href="https://x.com/dhanji/status/879993972" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">879993972</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '879437829',
    created: 1218037529000,
    type: 'post',
    text: 'I\'m doing a promo for Seam in Action at JavaRanch this week: <a href="http://saloon.javaranch.com/cgi-bin/ubb/ultimatebb.cgi?ubb=forum&amp;f=83" rel="noopener noreferrer" target="_blank">saloon.javaranch.com/cgi-bin/ubb/ultimatebb.cgi?ubb=forum&amp;f=83</a> Ask a question, enter to win a free book!',
    likes: 0,
    retweets: 0
  },
  {
    id: '878987361',
    created: 1217996304000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_S_A_R_A_H_" rel="noopener noreferrer" target="_blank">@_S_A_R_A_H_</a>, beer me<br><br>In reply to: <a href="https://x.com/_S_A_R_A_H_/status/878894487" rel="noopener noreferrer" target="_blank">@_S_A_R_A_H_</a> <span class="status">878894487</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '877454951',
    created: 1217873497000,
    type: 'post',
    text: 'Gmail has been having incredibly poor performance lately.',
    likes: 0,
    retweets: 0
  },
  {
    id: '876815065',
    created: 1217815567000,
    type: 'post',
    text: 'I am in hell right now.',
    likes: 0,
    retweets: 0
  },
  {
    id: '876035139',
    created: 1217726740000,
    type: 'post',
    text: 'My wife is out of town for a conference so I have to fend for myself. That calls for a manwich. Sloppy Joes!',
    likes: 0,
    retweets: 0
  },
  {
    id: '875212349',
    created: 1217634994000,
    type: 'post',
    text: 'No updates. I have until 8/12 to be done with the book for it to be printed by Sept 1. I am "going dark" until then.',
    likes: 0,
    retweets: 0
  },
  {
    id: '875212018',
    created: 1217634967000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> At this point, I think that one hundred pushups would kill me. My first task after finishing the book is to get back in shape.<br><br>In reply to: <a href="https://x.com/maxandersen/status/875120398" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">875120398</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '870879323',
    created: 1217275198000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iamroberthanson" rel="noopener noreferrer" target="_blank">@iamroberthanson</a> First, true. Second, you would adore Web Beans. It is true type-safe development and a very creative use of annotations.<br><br>In reply to: <a href="https://x.com/iamroberthanson/status/870869184" rel="noopener noreferrer" target="_blank">@iamroberthanson</a> <span class="status">870869184</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '870873728',
    created: 1217274808000,
    type: 'post',
    text: 'Landmark: All chapters through copy editing. Soon I will be saying that about typesetting as currently 9 out of 13 through layout.',
    likes: 0,
    retweets: 0
  },
  {
    id: '870861139',
    created: 1217273923000,
    type: 'post',
    text: 'I was so dedicated to reading the Web Beans spec while I was on the beach in Sandbridge that I got sunburn on my arm from lack of movement.',
    likes: 0,
    retweets: 0
  },
  {
    id: '870787369',
    created: 1217268475000,
    type: 'post',
    text: 'Traveling a bit gives you a pretty good idea of what you need. I need: a new camera, computer, phone, roaming internet, music player.',
    likes: 0,
    retweets: 0
  },
  {
    id: '869841989',
    created: 1217177964000,
    type: 'post',
    text: 'You know you\'re an American in Europe when your cell phone is a brick, you don\'t understand anything said, and your money is worthless.',
    likes: 0,
    retweets: 0
  },
  {
    id: '869840393',
    created: 1217177796000,
    type: 'post',
    text: 'I went to take a picture of the Colosseum only to realize that the camera battery was in hotel room. Story of my life. Jay got pics though.',
    likes: 0,
    retweets: 0
  },
  {
    id: '869839823',
    created: 1217177744000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Another screen is shot? We put a curse on you when you showed it off I guess. It got too much Tuscan sun ;)<br><br>In reply to: <a href="https://x.com/maxandersen/status/869831560" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">869831560</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '869838171',
    created: 1217177581000,
    type: 'post',
    text: 'Discussing global climate change, someone said "It just means they can grow grapes for wine in the UK, not France." Emmanuel screamed "No!"',
    likes: 0,
    retweets: 0
  },
  {
    id: '869834536',
    created: 1217177235000,
    type: 'post',
    text: 'Holy crap, it is hailing here in Maryland. I haven\'t seen hail in 10+ years.',
    likes: 0,
    retweets: 0
  },
  {
    id: '869833848',
    created: 1217177168000,
    type: 'post',
    text: 'Shane Bryzak is a culinary master. Thanks for treating us Shane!',
    likes: 0,
    retweets: 0
  },
  {
    id: '869831669',
    created: 1217176960000,
    type: 'post',
    text: 'Briefly checked out emmanuelbernard\'s Sony eBook reader. It\'s definitely on my "once I have money again I will buy" list.',
    likes: 0,
    retweets: 0
  },
  {
    id: '869827824',
    created: 1217176597000,
    type: 'post',
    text: 'Guy on airplane was video taping the flight preparation routine. Yeah, that\'s going to be a thrill to watch. To bad I\'ll miss it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '869826552',
    created: 1217176484000,
    type: 'post',
    text: 'Christian Bauer cracks me up. He has that cynical comedian thing going.',
    likes: 0,
    retweets: 0
  },
  {
    id: '869825163',
    created: 1217176350000,
    type: 'post',
    text: 'I had no idea JBoss was such an international and diverse group of developers. 10+ countries were represented at Seam/Hibernate meeting.',
    likes: 0,
    retweets: 0
  },
  {
    id: '869823703',
    created: 1217176212000,
    type: 'post',
    text: 'Two days ago I stood in Ancient Rome. Incredible.',
    likes: 0,
    retweets: 0
  },
  {
    id: '869822742',
    created: 1217176123000,
    type: 'post',
    text: 'No tweets from Tuscan Villa. Internet connection was practically non-existent. 3G dongle to wireless router through thick walls. No chance.',
    likes: 0,
    retweets: 0
  },
  {
    id: '863214143',
    created: 1216529745000,
    type: 'post',
    text: 'Rolled the dice on a local seafood joint Margies and Rays and it was stellar. Great pick!',
    likes: 0,
    retweets: 0
  },
  {
    id: '863126697',
    created: 1216519991000,
    type: 'post',
    text: '7hr drive from MD to VA Beach. Brutal, just brutal.',
    likes: 0,
    retweets: 0
  },
  {
    id: '862478873',
    created: 1216440985000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> btw, do you have Seam in Action? If not, contact me. I definitely want to get it into developers\' hands.<br><br>In reply to: <a href="https://x.com/mraible/status/862476619" rel="noopener noreferrer" target="_blank">@mraible</a> <span class="status">862476619</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '862478477',
    created: 1216440938000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> You say that because you are not writing for them ;) Just kidding of course. Writing is just hard, so you are bitter by the end.<br><br>In reply to: <a href="https://x.com/mraible/status/862476619" rel="noopener noreferrer" target="_blank">@mraible</a> <span class="status">862476619</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '862477673',
    created: 1216440846000,
    type: 'post',
    text: 'Bumber sticker: We are making enemies faster than we can kill them. Also: Killing one person is murder, killing 1000 is foreign policy.',
    likes: 0,
    retweets: 0
  },
  {
    id: '862233163',
    created: 1216415904000,
    type: 'post',
    text: 'Tomorrow I\'m off for a brief stint at the beach where I will catch rays, drink brew, and consume the Web Beans spec. Then, to Tuscany!',
    likes: 0,
    retweets: 0
  },
  {
    id: '861149583',
    created: 1216318574000,
    type: 'post',
    text: 'A new feature of Seam: transparent integration of Java EE discussions and a Tuscan farmhouse. I can\'t wait for the release!',
    likes: 0,
    retweets: 0
  },
  {
    id: '861054274',
    created: 1216312055000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Congrats! Can\'t wait to try it. But it will have to be after I finish reviewing my final proofs.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/861047743" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">861047743</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '860065108',
    created: 1216222798000,
    type: 'post',
    text: '1-20-09 the end of an error.',
    likes: 0,
    retweets: 0
  },
  {
    id: '858482484',
    created: 1216078028000,
    type: 'post',
    text: 'It\'s absolutely incredible how many technologies I have had to learn or draw on past knowledge of to write this book.',
    likes: 0,
    retweets: 0
  },
  {
    id: '857673623',
    created: 1216004962000,
    type: 'post',
    text: 'One flaw will ruin a software program. I truly believe that. Don\'t listen to people that tell you that it\'s not a big deal. Fix the flaw.',
    likes: 0,
    retweets: 0
  },
  {
    id: '857644645',
    created: 1216002157000,
    type: 'post',
    text: 'Visio 2007 has the most bonehead save dialog I have ever used. I put more thought into saving than I do creating the damn drawing.',
    likes: 0,
    retweets: 0
  },
  {
    id: '857571080',
    created: 1215994773000,
    type: 'post',
    text: 'I never knew about java.awt.Robot. That has some serious potential when combined with Groovy. I definitely cannot do that easily in bash.',
    likes: 0,
    retweets: 0
  },
  {
    id: '857566543',
    created: 1215994287000,
    type: 'post',
    text: 'My computer is slowly deteriorating. Suspend is hit or miss. My mouse has gone wacky. Now artifacts appear on my screen. I hate it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '857490430',
    created: 1215985803000,
    type: 'post',
    text: 'So I added an example in Groovy in Seam in Action. I made it as ridiculous as possible in the number of lines reduced just to show off.',
    likes: 0,
    retweets: 0
  },
  {
    id: '857485517',
    created: 1215985259000,
    type: 'post',
    text: 'Interesting, since Seam injects based on name, you can eliminate half your imports if you write your components in Groovy.',
    likes: 0,
    retweets: 0
  },
  {
    id: '857356133',
    created: 1215970869000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Just added this to Seam in Action, but for plain Hibernate (chapter 10). We should make this part of the Seam debug page.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/857355797" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">857355797</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '857349489',
    created: 1215970191000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> We you are writing an application, you don\'t care about HTTP. You care about what action the user took. A "website" is different.<br><br>In reply to: <a href="https://x.com/dhanji/status/857113552" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">857113552</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '857348678',
    created: 1215970100000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> HTTP is a screwed up protocol. Why map directly to it? We abtract to think about what the user is doing, not how it is communicated.<br><br>In reply to: <a href="https://x.com/dhanji/status/857113552" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">857113552</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '857347419',
    created: 1215969964000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> Broken, but an easy fix (in terms of impact). We don\'t have to throwout the baby with the bathwater. Seam could definitely use this.<br><br>In reply to: <a href="https://x.com/dhanji/status/857113552" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">857113552</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '856781285',
    created: 1215897593000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> I don\'t think JSF is that abstract. Perfect. No. But not too abstract. JSF just failed from not being used before it was finialized.<br><br>In reply to: <a href="https://x.com/dhanji/status/856439246" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">856439246</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '856780129',
    created: 1215897456000,
    type: 'post',
    text: 'I failed to understand why the hell developers use Windows. VMWare with Windows I can understand to get by. Windows alone, no #$% sense.',
    likes: 0,
    retweets: 0
  },
  {
    id: '854338354',
    created: 1215663957000,
    type: 'post',
    text: 'Holy crap! There is a new version of Ant! Have all the glaciers melted yet?',
    likes: 0,
    retweets: 0
  },
  {
    id: '853757852',
    created: 1215614449000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> I\'m probably going to use Acrobat during the editing of the final proof. For now, I\'m taking the path of least resistence.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/853710431" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">853710431</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '853707739',
    created: 1215610852000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Yep, that\'s what I figured. I have to use it because my editors use track changes/comments heavily. It\'s so annoying.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/853677493" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">853677493</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '853431313',
    created: 1215577991000,
    type: 'post',
    text: 'My cousin\'s email got hacked and the hacker sent out a message that he was stuck in London and needed money. I almost bit on it. Damn email.',
    likes: 0,
    retweets: 0
  },
  {
    id: '853430955',
    created: 1215577952000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/scottdavis99" rel="noopener noreferrer" target="_blank">@scottdavis99</a> Dang, I wish I could have been there.<br><br>In reply to: <a href="https://x.com/scottdavis99/status/853430928" rel="noopener noreferrer" target="_blank">@scottdavis99</a> <span class="status">853430928</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '853430829',
    created: 1215577940000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Did you have to install Office for the book writing?<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/853124552" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">853124552</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '853028632',
    created: 1215540466000,
    type: 'post',
    text: 'I know many people love Gmail, but it really is a mediocre e-mail program. I\'m not saying it sucks, just that it\'s not God\'s gift to email.',
    likes: 0,
    retweets: 0
  },
  {
    id: '853011346',
    created: 1215539014000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/scottdavis99" rel="noopener noreferrer" target="_blank">@scottdavis99</a> People must think you live in Virginia you are hear so much. I\'d attend, but I\'m frantically trying to finish my book.<br><br>In reply to: <a href="https://x.com/scottdavis99/status/852922642" rel="noopener noreferrer" target="_blank">@scottdavis99</a> <span class="status">852922642</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '850896777',
    created: 1215293271000,
    type: 'post',
    text: 'My neighbors are trying to coax me out of my cave for a BBQ. Must...work...more...',
    likes: 0,
    retweets: 0
  },
  {
    id: '850345638',
    created: 1215211071000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> It\'s amazing how quickly software projects grow up, isn\'t it? Hard to imagine that my dom JavaScript libraries are 8 years young.<br><br>In reply to: <a href="https://x.com/aalmiray/status/850155899" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">850155899</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '850326222',
    created: 1215208484000,
    type: 'post',
    text: 'It\'s going to be a wet 4th in the nations captial. I\'m not sure whether to brave the rain or stay dry.',
    likes: 0,
    retweets: 0
  },
  {
    id: '850253102',
    created: 1215199375000,
    type: 'post',
    text: 'I\'m sure there are many envious women who would gladly take my spot on the plane to Tuscany. One of them is my wife ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '850252077',
    created: 1215199244000,
    type: 'post',
    text: 'I\'m all booked for Tuscany. I\'ve never been to Europe before and now I will have been there twice in less than 30 days.',
    likes: 0,
    retweets: 0
  },
  {
    id: '848728528',
    created: 1215032595000,
    type: 'post',
    text: 'Damn, Xandros bought Linspire. I have always liked Linspire when looking at the consumer side of Linux. I don\'t like Xandros nearly as much.',
    likes: 0,
    retweets: 0
  },
  {
    id: '848613645',
    created: 1215021951000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> GraniteDS sure does look sexy. There is just too much good stuff to play with these days.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/848474618" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">848474618</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '848611160',
    created: 1215021717000,
    type: 'post',
    text: 'Goodbye Gates. Tux won\'t miss you.',
    likes: 0,
    retweets: 0
  },
  {
    id: '848237961',
    created: 1214982056000,
    type: 'post',
    text: 'Just spoke with Manning today to iron out my home stretch. Looks like eBook by Aug 15 and print by Aug 31. Amen. What a year.',
    likes: 0,
    retweets: 0
  },
  {
    id: '847305917',
    created: 1214876964000,
    type: 'post',
    text: 'Just had a "geek" dinner with Andy Glover, Steve Saksa and Todd Safford. On the menu: easyB, Maven 2, GWT, book writing, Hudson, Groovy, etc',
    likes: 0,
    retweets: 0
  },
  {
    id: '847020521',
    created: 1214845109000,
    type: 'post',
    text: 'It\'s so irritating that Gmail eats good mail. If I forget to check my spam for a while (using a carefully crafted search) I miss stuff.',
    likes: 0,
    retweets: 0
  },
  {
    id: '846653040',
    created: 1214800730000,
    type: 'post',
    text: 'I think the Java language is going to be cleaned up by another type-safe language built on the JVM. Scala has a slightly different focus.',
    likes: 0,
    retweets: 0
  },
  {
    id: '846652418',
    created: 1214800652000,
    type: 'post',
    text: 'I forgot to mention that I was thoroughly impressed with Scala during Martin\'s talk at #jazoon, despite being skeptical going into it.',
    likes: 0,
    retweets: 0,
    tags: ['jazoon']
  },
  {
    id: '845912065',
    created: 1214697452000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Not even time for TiVo? I always catch some recordings after a late night of writing or hacking.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/845738983" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">845738983</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '845723683',
    created: 1214671579000,
    type: 'post',
    text: 'Was thrilled to see my wife again. One of these times she\'s going to get to come with me to Europe.',
    likes: 0,
    retweets: 0
  },
  {
    id: '845722573',
    created: 1214671483000,
    type: 'post',
    text: 'First trip to Europe, first time victim of lost baggage. Despite panic, has happy ending. Bag just never left Paris. Found out later.',
    likes: 0,
    retweets: 0
  },
  {
    id: '845721913',
    created: 1214671412000,
    type: 'post',
    text: 'Talked to sysadmin who claim that developers don\'t take credit for production problems. Definitely a concern that needs to be addressed.',
    likes: 0,
    retweets: 0
  },
  {
    id: '845720869',
    created: 1214671299000,
    type: 'post',
    text: 'Conclusion. mvn site:site is shit apart from the reports. Even reports might be better handled externally.',
    likes: 0,
    retweets: 0
  },
  {
    id: '845720767',
    created: 1214671288000,
    type: 'post',
    text: 'I like Mark\'s advice. Figure out what you want to accomplish, then think of best way to solve it. Going the other way you lose perspective.',
    likes: 0,
    retweets: 0
  },
  {
    id: '845720574',
    created: 1214671265000,
    type: 'post',
    text: 'Had dinner with Mark Newton, lead of JBoss.org and Paul Gier, Maven evangelist @ JBoss. Discussed Maven and solving software challenges.',
    likes: 0,
    retweets: 0
  },
  {
    id: '845719846',
    created: 1214671182000,
    type: 'post',
    text: 'Attended Java Puzzlers talk for first time. They\'re more about what is wrong with Java\'s API/behavior than learning what pitfalls to avoid.',
    likes: 0,
    retweets: 0
  },
  {
    id: '845719443',
    created: 1214671137000,
    type: 'post',
    text: 'Got a copy of Effective Java which Joshua Block signed. Sweet.',
    likes: 0,
    retweets: 0
  },
  {
    id: '845719368',
    created: 1214671128000,
    type: 'post',
    text: 'Bag too heavy. Why must technology weigh too much. Transporting is a bad thing today. Headache.',
    likes: 0,
    retweets: 0
  },
  {
    id: '845719049',
    created: 1214671093000,
    type: 'post',
    text: 'You end up spending your entire layover in Paris CDG transporting. I\'m not sure if that is a good or a bad thing.',
    likes: 0,
    retweets: 0
  },
  {
    id: '844411669',
    created: 1214511594000,
    type: 'post',
    text: 'I will miss Europe when I leave tomorrow. However, one thing I will *not* miss is the smoking. I hate smoking with a passion.',
    likes: 0,
    retweets: 0
  },
  {
    id: '844410826',
    created: 1214511535000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iamroberthanson" rel="noopener noreferrer" target="_blank">@iamroberthanson</a> It\'s always nice to know that your marketing department is working against you. As if writing wasn\'t hard enough.<br><br>In reply to: <a href="https://x.com/iamroberthanson/status/844331411" rel="noopener noreferrer" target="_blank">@iamroberthanson</a> <span class="status">844331411</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '844186073',
    created: 1214491722000,
    type: 'post',
    text: 'Americans may not be prompt like the Swiss, but the ratio of clock count from US to Suisse is about 100:1.',
    likes: 0,
    retweets: 0
  },
  {
    id: '844177023',
    created: 1214490949000,
    type: 'post',
    text: 'I am so freakin\' tired, and then I look at my e-mail. It\'s only uphill from here.',
    likes: 0,
    retweets: 0
  },
  {
    id: '844176540',
    created: 1214490910000,
    type: 'post',
    text: 'I had "the big stage" for my bijection talk today. From the feedback, it went quite well. It went so quick, though, I barely remember it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '844175016',
    created: 1214490794000,
    type: 'post',
    text: 'I put up my poster for Seam in Action and it mysteriously disappeared; either got confiscated (most likely) or stolen (not so likely). Hmm',
    likes: 0,
    retweets: 0
  },
  {
    id: '844173370',
    created: 1214490671000,
    type: 'post',
    text: 'Dammit why the hell is Manning sending out emails entitled "No Such Thing as a Java developer"',
    likes: 0,
    retweets: 0
  },
  {
    id: '843683810',
    created: 1214434799000,
    type: 'post',
    text: 'Looks like seamframework.org is back up again.',
    likes: 0,
    retweets: 0
  },
  {
    id: '843683215',
    created: 1214434734000,
    type: 'post',
    text: 'seamframework.org box or network died today. Hopefully Christian will be able to get everything recovered.',
    likes: 0,
    retweets: 0
  },
  {
    id: '843682674',
    created: 1214434680000,
    type: 'post',
    text: 'Close your eyes. If you hear the dopler effect, you are in Europe. If the sirens are after you, you\'re in an action movie.',
    likes: 0,
    retweets: 0
  },
  {
    id: '843676716',
    created: 1214434052000,
    type: 'post',
    text: 'Those bells are going to come early. I better get what shuteye I can muster.',
    likes: 0,
    retweets: 0
  },
  {
    id: '843676162',
    created: 1214433999000,
    type: 'post',
    text: 'Didn\'t mention it, but yesterday I met Christian Bauer. I figured he would give me hell for plenty of things. I found the message within.',
    likes: 0,
    retweets: 0
  },
  {
    id: '843672453',
    created: 1214433621000,
    type: 'post',
    text: 'Ted Neward called EJB "Enterprise Jesus Beans". He said not to blog that, but he didn\'t say not to twitter it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '843671777',
    created: 1214433565000,
    type: 'post',
    text: 'What a day. Survived my talk. Experienced Euro08. Got invited to Javapolis 08. Like I said, what a day.',
    likes: 0,
    retweets: 0
  },
  {
    id: '843024727',
    created: 1214371878000,
    type: 'post',
    text: 'Prepping for Spring/Seam presentation. Lot\'s of Spring developers here, so this should be a good audience.',
    likes: 0,
    retweets: 0
  },
  {
    id: '843024442',
    created: 1214371845000,
    type: 'post',
    text: 'Well, its 7AM and Zurich is once again up, courtesy of the bells. I really don\'t need an alarm clock.',
    likes: 0,
    retweets: 0
  },
  {
    id: '843024094',
    created: 1214371810000,
    type: 'post',
    text: 'Nighttime in Zurich: biere, cabaret, and Take Away. Lots of each. 	<br>Je suis juste ici pour la bière.',
    likes: 0,
    retweets: 0
  },
  {
    id: '843023376',
    created: 1214371720000,
    type: 'post',
    text: 'I built a sample application in 2 hours last night for the demo. I didn\'t have time before I left from US.',
    likes: 0,
    retweets: 0
  },
  {
    id: '843023110',
    created: 1214371685000,
    type: 'post',
    text: 'I was quite for a while. A fellow from Poland/Suisse just about talked my ear off. Very interesting, no doubt. Just long. My poor feet.',
    likes: 0,
    retweets: 0
  },
  {
    id: '842404199',
    created: 1214309764000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> If I can offer you one other piece of advice, try to get Manning to involve the technical editor during the final review.<br><br>In reply to: <a href="https://x.com/dhanji/status/842393227" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">842393227</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '842355272',
    created: 1214303995000,
    type: 'post',
    text: 'Rod Johnson on stage. He still beats up on EJB and the JCP, despite any claims he doesn\'t.',
    likes: 0,
    retweets: 0
  },
  {
    id: '842354991',
    created: 1214303955000,
    type: 'post',
    text: 'I despise the pimping of Tomcat. I\'m all for lightweight, but it is so mediocre and half-assed. I\'d like to see GlassFish succeed.',
    likes: 0,
    retweets: 0
  },
  {
    id: '842354631',
    created: 1214303908000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> Haha, welcome to the beginning of those who want to tell you how to write your book. Appreciate it, but remember who the author is.<br><br>In reply to: <a href="https://x.com/dhanji/status/842346243" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">842346243</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '842343921',
    created: 1214302418000,
    type: 'post',
    text: 'I thoroughly enjoyed listening to Simon Phipps. It is an important step in my restatement of my position of Open Source which I plan to do.',
    likes: 0,
    retweets: 0
  },
  {
    id: '842320826',
    created: 1214299292000,
    type: 'post',
    text: 'Mr Open Source from Sun is on the stage, Simon Phipps. I never take what happened thanks to him for granted.',
    likes: 0,
    retweets: 0
  },
  {
    id: '842306409',
    created: 1214297300000,
    type: 'post',
    text: 'I like the point that you start w/ a simple language because program is simple but then it grows and you have to rewrite during success. Ahh',
    likes: 0,
    retweets: 0
  },
  {
    id: '842304220',
    created: 1214296990000,
    type: 'post',
    text: 'I am using my PepperPad at this conference. Infinitely more convenient and stable than my crappy laptop.',
    likes: 0,
    retweets: 0
  },
  {
    id: '842303094',
    created: 1214296830000,
    type: 'post',
    text: 'Saw a person on TVV w/ Jazoon shirt. Connected up w/ him to ensure I didn\'t get lost. Good short discussion as well. <a class="mention" href="https://x.com/JAZOON" rel="noopener noreferrer" target="_blank">@JAZOON</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '842301948',
    created: 1214296675000,
    type: 'post',
    text: 'Creator of Scala is doing keynote <a class="mention" href="https://x.com/JAZOON" rel="noopener noreferrer" target="_blank">@JAZOON</a> talking about scalable language. I agree that typing is important but doesn\'t mean laborous.',
    likes: 0,
    retweets: 0
  },
  {
    id: '842244705',
    created: 1214288645000,
    type: 'post',
    text: 'I cannot believe I am in Swiss and I don\'t know what time it is. Geez. Definitely need to find a watch.',
    likes: 0,
    retweets: 0
  },
  {
    id: '842244515',
    created: 1214288620000,
    type: 'post',
    text: 'The most difficult part so far is that my mind doesn\'t have the capacity of digesting Swiss names. The characters just don\'t make any sense.',
    likes: 0,
    retweets: 0
  },
  {
    id: '842229275',
    created: 1214286428000,
    type: 'post',
    text: 'The church bells in Zurich ring every hour and 15 minutes before the hour, just so that you are ready for the hour.',
    likes: 0,
    retweets: 0
  },
  {
    id: '842228100',
    created: 1214286282000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Especially on the TVV. I thought they were trying to bake me for an evening snack.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/841995488" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">841995488</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '842227541',
    created: 1214286203000,
    type: 'post',
    text: 'Apparantely, you are supposed to wake up in Zurich at 7:00am. No need for alarm clocks. Those bells will wake the dead.',
    likes: 0,
    retweets: 0
  },
  {
    id: '841818514',
    created: 1214243554000,
    type: 'post',
    text: 'So are we going to go with <a class="mention" href="https://x.com/JAZOON" rel="noopener noreferrer" target="_blank">@JAZOON</a> for all conference activity?',
    likes: 0,
    retweets: 0
  },
  {
    id: '841817934',
    created: 1214243496000,
    type: 'post',
    text: 'Watched Fool\'s Gold on my sleepless flight. The girl in the movie is using twitter (or something comparable that doesn\'t crash).',
    likes: 0,
    retweets: 0
  },
  {
    id: '841817141',
    created: 1214243415000,
    type: 'post',
    text: 'I already set off the alarm in the hotel. How was I supposed to know that wasn\'t the lift button. The lift looks like a door!',
    likes: 0,
    retweets: 0
  },
  {
    id: '841816436',
    created: 1214243344000,
    type: 'post',
    text: 'The sterotype really is true about the Swiss. The are efficient in so many ways. Too many to mention in 140 chars.',
    likes: 0,
    retweets: 0
  },
  {
    id: '841815510',
    created: 1214243272000,
    type: 'post',
    text: 'I got my first smell of fresh French air. The results? France smells like cheese. Stinky (in a good way).',
    likes: 0,
    retweets: 0
  },
  {
    id: '841813977',
    created: 1214243137000,
    type: 'post',
    text: 'To be blunt like the French, what they are really speaking is Frenglish. I\'m not even sure if they are conscious of the mix.',
    likes: 0,
    retweets: 0
  },
  {
    id: '841813068',
    created: 1214243056000,
    type: 'post',
    text: 'It\'s true what they say. The French definitely push their language. Good thing I sort of understand it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '841812167',
    created: 1214242972000,
    type: 'post',
    text: 'Just let me say "I\'m in Europe, holy shit!" Now back to the regularly scheduled ranting.',
    likes: 0,
    retweets: 0
  },
  {
    id: '841811252',
    created: 1214242888000,
    type: 'post',
    text: 'I have not yet learned the art of sleeping on a plane. I lot of shifting, not a lot of sleeping. At least I was fed well.',
    likes: 0,
    retweets: 0
  },
  {
    id: '841190761',
    created: 1214173151000,
    type: 'post',
    text: 'Got my power converter in one hand and my tourist guide in the other.',
    likes: 0,
    retweets: 0
  },
  {
    id: '841190541',
    created: 1214173121000,
    type: 'post',
    text: 'Here I come Europe. First time in 30 years. Does that make me an "international business traveler"? Not by a long shot.',
    likes: 0,
    retweets: 0
  },
  {
    id: '841190289',
    created: 1214173085000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> If she was in Paris, I don\'t think she would care about sleep. She is jealous enough over Zurich.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/841153211" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">841153211</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '841104682',
    created: 1214163210000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> You are making my wife very jealous.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/840729087" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">840729087</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '840653740',
    created: 1214102664000,
    type: 'post',
    text: 'I am soooooooooooooooo freakin\' tired.',
    likes: 0,
    retweets: 0
  },
  {
    id: '840051412',
    created: 1214022817000,
    type: 'post',
    text: 'I\'ve been pretty impressed with ICEFaces while researching it for chapter 12. It\'s approach to Ajax is to detect changes and send them.',
    likes: 0,
    retweets: 0
  },
  {
    id: '839103244',
    created: 1213922582000,
    type: 'post',
    text: 'Got my first negative post on Seam in Action. Dude says I could have done better. With confidence I can say, "I left it all on the field."',
    likes: 0,
    retweets: 0
  },
  {
    id: '838804514',
    created: 1213894280000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Are you in Zurich already? I need to get myself one of the Euro pluggy thingies<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/838647243" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">838647243</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '838414955',
    created: 1213854242000,
    type: 'post',
    text: 'Tiger Woods is out for the rest of the season. I guess there was a reason he fought to the end to win that US Open title. Knee was shot.',
    likes: 0,
    retweets: 0
  },
  {
    id: '836458142',
    created: 1213665806000,
    type: 'post',
    text: 'My bro got a 998 Ducati, 350 lbs, 123 hp. Like a 2000 lb car with 200 hp. 6 years old, but only 3 miles on it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '836440074',
    created: 1213663998000,
    type: 'post',
    text: 'No matter how much you try to force yourself to work, when you are burnt-out, you just can\'t.',
    likes: 0,
    retweets: 0
  },
  {
    id: '836439679',
    created: 1213663959000,
    type: 'post',
    text: 'Another quote from my wife: "My plants are deer resistant. They just aren\'t landscaper resistant."',
    likes: 0,
    retweets: 0
  },
  {
    id: '836439306',
    created: 1213663916000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Yeah, I think that the observer/event is the most underused feature of Seam. Stereotypes are almost just a pattern.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/836420833" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">836420833</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '836297024',
    created: 1213649849000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> Wasn\'t that incredible. Tiger is just a rock. As Mediate said yesterday, "Unbelievable. I knew he would make it."<br><br>In reply to: <a href="https://x.com/mraible/status/836288667" rel="noopener noreferrer" target="_blank">@mraible</a> <span class="status">836288667</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '835703815',
    created: 1213589878000,
    type: 'post',
    text: 'I swear I know when I forget things. It\'s a feeling. Like when you realize something. Only, I realize that I forgot something. Brain dump.',
    likes: 0,
    retweets: 0
  },
  {
    id: '835633390',
    created: 1213581649000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> Yeah, Manning does whatever Manning wants. I told them to pair with w/ Java Persistence with Hibernate or JBoss in Action.<br><br>In reply to: <a href="https://x.com/dhanji/status/835534790" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">835534790</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '835632834',
    created: 1213581598000,
    type: 'post',
    text: 'Tiger Woods is unbelievable. Just when you think it can\'t be done, he freakin\' does it. <a href="http://ukpress.google.com/article/ALeqM5gHgdBRtmJSkkoEO96eDmyrF1LFnw" rel="noopener noreferrer" target="_blank">ukpress.google.com/article/ALeqM5gHgdBRtmJSkkoEO96eDmyrF1LFnw</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '835385238',
    created: 1213551334000,
    type: 'post',
    text: 'Editing is like driving a truck through mud.',
    likes: 0,
    retweets: 0
  },
  {
    id: '835385211',
    created: 1213551332000,
    type: 'post',
    text: 'Going through feedback is time consuming. Just now getting to feedback I got a couple months ago.',
    likes: 0,
    retweets: 0
  },
  {
    id: '835324261',
    created: 1213544219000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> Look for the book "First Draft in 30 Days" which explains the process. It isn\'t geared towards tech books, but the idea is the same.<br><br>In reply to: <a href="https://x.com/dhanji/status/835129031" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">835129031</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '835322197',
    created: 1213544003000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> The book is Seam in Action <a href="http://manning.com/dallen" rel="noopener noreferrer" target="_blank">manning.com/dallen</a>. An outline is not a ToC. An outline is like a very terse rough draft.<br><br>In reply to: <a href="https://x.com/dhanji/status/835129031" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">835129031</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '835132460',
    created: 1213515238000,
    type: 'post',
    text: 'Tiger Woods was a god today. Not that he isn\'t always, but he never ceases to amaze.',
    likes: 0,
    retweets: 0
  },
  {
    id: '835128631',
    created: 1213514563000,
    type: 'post',
    text: 'Btw, just for the record, I proposed Web Beans to be renamed to Contextual JavaBeans (CJBs). It\'s more about context than web.',
    likes: 0,
    retweets: 0
  },
  {
    id: '835127031',
    created: 1213514308000,
    type: 'post',
    text: 'Digital loss is worse than a fire. At least with a fire there is evidence you lost something. Once data is gone, its just a memory.',
    likes: 0,
    retweets: 0
  },
  {
    id: '835126641',
    created: 1213514244000,
    type: 'post',
    text: 'I am so sick of living out of someone else\'s computer. I need to buy my own, get a big backup drive, and turn my backups on again.',
    likes: 0,
    retweets: 0
  },
  {
    id: '835126443',
    created: 1213514210000,
    type: 'post',
    text: 'I\'ve been using Zim, a desktop wiki, while writing the book. Total lifesaver. On Thurs, 10 of my pages got corrupted. Ouch! Minor setback.',
    likes: 0,
    retweets: 0
  },
  {
    id: '835125592',
    created: 1213514067000,
    type: 'post',
    text: 'I have been working nearly 24x7 to get the book done. This hurts, but I want this book out. I guess I know how a pregnant woman feels.',
    likes: 0,
    retweets: 0
  },
  {
    id: '835123301',
    created: 1213513655000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> In my first book writing experience, I have learned that a full outline is critical. Next book I write, outline first, then write.<br><br>In reply to: <a href="https://x.com/dhanji/status/835117397" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">835117397</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '831705340',
    created: 1213137774000,
    type: 'post',
    text: 'Quote from my wife: "Your eyes were a little bit bigger than your wrap" (fajitas)',
    likes: 0,
    retweets: 0
  },
  {
    id: '831688371',
    created: 1213135988000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dwaite" rel="noopener noreferrer" target="_blank">@dwaite</a> You say that looking at Seam code makes you long for Rails. Seam is annotations and integrations. The verbosity is Java. Use Groovy.<br><br>In reply to: <a href="https://x.com/dwaite/status/831386841" rel="noopener noreferrer" target="_blank">@dwaite</a> <span class="status">831386841</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '831637535',
    created: 1213131141000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> Ah, DI in Action, that\'s right.<br><br>In reply to: <a href="https://x.com/dhanji/status/831359993" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">831359993</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '831636799',
    created: 1213131071000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> I could lend you some of my 49 MPG. My neighbors have moved on to bikes. They don\'t write software for a living.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/831619136" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">831619136</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '831528025',
    created: 1213120874000,
    type: 'post',
    text: 'This tomato restriction is killing me (even though the point is to keep me alive). Went to Baja Fresh...yeah, no salsa. Buy Local!',
    likes: 0,
    retweets: 0
  },
  {
    id: '831512268',
    created: 1213119546000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> What are you driving these days?<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/831025051" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">831025051</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '831511710',
    created: 1213119495000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> Um, writing a book is hard. What book are you writing?<br><br>In reply to: <a href="https://x.com/dhanji/status/831359993" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">831359993</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '831162774',
    created: 1213084495000,
    type: 'post',
    text: 'I meant to mention that JBoss Envers <a href="http://www.jboss.org/envers" rel="noopener noreferrer" target="_blank">www.jboss.org/envers</a> looks promising. Not everyone\'s going to need it, but when you do, yeah!',
    likes: 0,
    retweets: 0
  },
  {
    id: '831119808',
    created: 1213078257000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> Precisely. Trust that there is plenty of writing in my future. If I can say that I finished this one book, there is no limit.<br><br>In reply to: <a href="https://x.com/aalmiray/status/831113855" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">831113855</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '831103851',
    created: 1213076101000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> JBoss Seam 2E is promising lots of Groovy coverage and frankly that belongs in Groovy in Action. Perhaps a Seam+Groovy book too.<br><br>In reply to: <a href="https://x.com/aalmiray/status/831103495" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">831103495</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '831102995',
    created: 1213075993000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> I definitely think that Groovy+Seam is an ideal marriage. It\'s absolutely useful. Just for the book, the focus is Seam concepts.<br><br>In reply to: <a href="https://x.com/aalmiray/status/830975442" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">830975442</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '831016302',
    created: 1213066059000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/scottdavis99" rel="noopener noreferrer" target="_blank">@scottdavis99</a> For a while, Applebee\'s rediscovered their honeymoon period, only to fade back into mediocrity shortly thereafter.<br><br>In reply to: <a href="https://x.com/scottdavis99/status/830974312" rel="noopener noreferrer" target="_blank">@scottdavis99</a> <span class="status">830974312</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '831015589',
    created: 1213065993000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SUBZERO66" rel="noopener noreferrer" target="_blank">@SUBZERO66</a> Story of my life (in reference to the ghost of Perl)<br><br>In reply to: <a href="https://x.com/bmuschko/status/830956558" rel="noopener noreferrer" target="_blank">@bmuschko</a> <span class="status">830956558</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '830800882',
    created: 1213044440000,
    type: 'post',
    text: 'People use the phrase "this came across my desk". The only thing that moves across my desk is dust.',
    likes: 0,
    retweets: 0
  },
  {
    id: '830753585',
    created: 1213040675000,
    type: 'post',
    text: 'Don\'t get me wrong, Groovy is awesome. It\'s just a different (powerful) approach to developing your app logic. Seam still works the same.',
    likes: 0,
    retweets: 0
  },
  {
    id: '830750987',
    created: 1213040467000,
    type: 'post',
    text: 'I have tried to find places to add Groovy examples in Seam in Action, but the reality is, Groovy is an orthogonal topic. Just wouldn\'t fit.',
    likes: 0,
    retweets: 0
  },
  {
    id: '830163224',
    created: 1212981786000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Boblee" rel="noopener noreferrer" target="_blank">@Boblee</a> I picked up the Nespresso C100 <a href="https://www.williams-sonoma.com/products/sku8013930/index.cfm?bnrid=3100117" rel="noopener noreferrer" target="_blank">www.williams-sonoma.com/products/sku8013930/index.cfm?bnrid=3100117</a> coupled with the Aeroccino about 1 year ago and its better than Starbucks.<br><br>In reply to: <a href="https://x.com/BobPopVeTV/status/823942985" rel="noopener noreferrer" target="_blank">@BobPopVeTV</a> <span class="status">823942985</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '829990678',
    created: 1212960713000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> Yeah, except I have never seen a project that actually used JSPX. Too little, too late in that department. JSP is dead to me.<br><br>In reply to: <a href="https://x.com/dhanji/status/829981709" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">829981709</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '829963354',
    created: 1212957261000,
    type: 'post',
    text: 'There are friends of people in my neighborhood that seem to think that my street is a nightclub. Interrupts my late night writing sessions.',
    likes: 0,
    retweets: 0
  },
  {
    id: '829962464',
    created: 1212957154000,
    type: 'post',
    text: 'I can\'t believe Thunderbird cannot use local mbox files (without hacking it). It would be extremely helpful for testing e-mails locally.',
    likes: 0,
    retweets: 0
  },
  {
    id: '829771958',
    created: 1212934851000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> No, one of JSPs biggest mistakes was not supporting valid XML. Granted embedded Java is ugly. But still, no use of PI &lt;?java ...<br><br>In reply to: <a href="https://x.com/dhanji/status/829754533" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">829754533</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '829770613',
    created: 1212934694000,
    type: 'post',
    text: 'I also noticed that Google changed their favicon. I thought it might have been part of the theme the other day, but it stuck.',
    likes: 0,
    retweets: 0
  },
  {
    id: '829747246',
    created: 1212931711000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> That\'s funny that TestNG scans JAR files. Do people really package their tests? I just never thought of doing it that way.<br><br>In reply to: <a href="https://x.com/dhanji/status/829630414" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">829630414</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '829746107',
    created: 1212931545000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> Did you see this post of interview w/ IntelliJ developer that talks about Scala? <a href="http://www.artima.com/lejava/articles/javaone_2008_dmitry_jemerov.html" rel="noopener noreferrer" target="_blank">www.artima.com/lejava/articles/javaone_2008_dmitry_jemerov.html</a><br><br>In reply to: <a href="https://x.com/aalmiray/status/829585520" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">829585520</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '829745089',
    created: 1212931410000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SUBZERO66" rel="noopener noreferrer" target="_blank">@SUBZERO66</a> Hahaha! Well, sometimes you have to celebrate your birthday just because it is a rainy day ;)<br><br>In reply to: <a href="https://x.com/bmuschko/status/829738993" rel="noopener noreferrer" target="_blank">@bmuschko</a> <span class="status">829738993</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '829493131',
    created: 1212891352000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> Check out the Seam source code. There are lots of examples of this. Also, check out the source code for Hibernate EntityManager.<br><br>In reply to: <a href="https://x.com/dhanji/status/829459474" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">829459474</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '829492775',
    created: 1212891304000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SUBZERO66" rel="noopener noreferrer" target="_blank">@SUBZERO66</a> Happy Birthday! Hope you kept dry in the storm!<br><br>In reply to: <a href="https://x.com/bmuschko/status/829458791" rel="noopener noreferrer" target="_blank">@bmuschko</a> <span class="status">829458791</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '829435474',
    created: 1212883165000,
    type: 'post',
    text: 'Isn\'t it great when thunder sets off a car alarm?',
    likes: 0,
    retweets: 0
  },
  {
    id: '829434608',
    created: 1212883025000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/crazybob" rel="noopener noreferrer" target="_blank">@crazybob</a> It would be really sad if your wife was IM-ing you to tell you that she was in bed next to you naked.<br><br>In reply to: <a href="https://x.com/crazybob/status/829425328" rel="noopener noreferrer" target="_blank">@crazybob</a> <span class="status">829425328</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '828798198',
    created: 1212803868000,
    type: 'post',
    text: 'A breakdown of modern web design by HLS (<a href="http://tapestryjava.blogspot.com/2008/06/time-breakdown-of-modern-web-design.html" rel="noopener noreferrer" target="_blank">tapestryjava.blogspot.com/2008/06/time-breakdown-of-modern-web-design.html</a>). I couldn\'t have graphed it better myself.',
    likes: 0,
    retweets: 0
  },
  {
    id: '828702552',
    created: 1212792175000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Congrats on the solid reviews. As for scoring, that\'s exactly what I\'ve been saying. Closing the deal has been evading me.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/828700042" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">828700042</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '828575905',
    created: 1212775644000,
    type: 'post',
    text: 'I\'ve never seen Google use a filled in background in their logo like today. Kind of strange. It\'s a painting.',
    likes: 0,
    retweets: 0
  },
  {
    id: '828228249',
    created: 1212735039000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> I don\'t think you can. I tried it once.<br><br>In reply to: <a href="https://x.com/dhanji/status/828215422" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">828215422</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '828227876',
    created: 1212734989000,
    type: 'post',
    text: 'Why do teams that use JDBC instead of ORM rant about it like they are recovering alcoholics? <a href="http://cookiesareforclosers.com/blog/2008/06/linkedin-architecture" rel="noopener noreferrer" target="_blank">cookiesareforclosers.com/blog/2008/06/linkedin-architecture</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '827723091',
    created: 1212678525000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> I know that TetraTech, for whom I do some consulting, uses ColdFusion. They used to love it, now they dig Seam ;)<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/827721561" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">827721561</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '826272439',
    created: 1212524962000,
    type: 'post',
    text: 'I have clicked on more Google Ads in Gmail than anywhere else on the internet. In fact, I never click on ads elsewhere.',
    likes: 0,
    retweets: 0
  },
  {
    id: '826199247',
    created: 1212517601000,
    type: 'post',
    text: 'Unfortunately, I don\'t have a link for that record because the pool was leveled before the digital age. Should have stolen the record board.',
    likes: 0,
    retweets: 0
  },
  {
    id: '826198512',
    created: 1212517539000,
    type: 'post',
    text: '20 years ago I set my first swimming record. 19:88 in 9-10 25 meter breaststroke. Same as the year. I later beat my own record.',
    likes: 0,
    retweets: 0
  },
  {
    id: '825664647',
    created: 1212461462000,
    type: 'post',
    text: 'I need to build myself a thesaurus shell.',
    likes: 0,
    retweets: 0
  },
  {
    id: '825663428',
    created: 1212461318000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> Unfortunately, my play time has been limited lately. But I live and breathe shells. I will add that to my list as well.<br><br>In reply to: <a href="https://x.com/aalmiray/status/825663143" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">825663143</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '825652751',
    created: 1212460312000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> I\'m massively out of shape, so I didn\'t catch the train my wife was on. Had to ride all the way to where to she was going. Sucked.<br><br>In reply to: <a href="https://x.com/aalmiray/status/825650256" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">825650256</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '825651849',
    created: 1212460243000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> Drove about 1/4 mile. Car was beeping like hell. Saw failure light. Stopped car. Wouldn\'t start again. Ran off like a crazy man.<br><br>In reply to: <a href="https://x.com/aalmiray/status/825650256" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">825650256</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '825650621',
    created: 1212460084000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iamroberthanson" rel="noopener noreferrer" target="_blank">@iamroberthanson</a> Straight from the horse\'s mouth: <a href="http://www.microsoft.com/presspass/press/2008/may08/05-21ExpandedFormatsPR.mspx" rel="noopener noreferrer" target="_blank">www.microsoft.com/presspass/press/2008/may08/05-21ExpandedFormatsPR.mspx</a> Office 2007 SP2 will have ODF support built in.<br><br>In reply to: <a href="https://x.com/iamroberthanson/status/823493123" rel="noopener noreferrer" target="_blank">@iamroberthanson</a> <span class="status">823493123</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '825648722',
    created: 1212459934000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nanikjava" rel="noopener noreferrer" target="_blank">@nanikjava</a> Sure, writing this book has been the best and worst thing of my life. It\'s only bad because you need more support than you know.<br><br>In reply to: <a href="https://x.com/nanikjava/status/825605124" rel="noopener noreferrer" target="_blank">@nanikjava</a> <span class="status">825605124</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '825646800',
    created: 1212459728000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> My advice for Groovy devs is to run with this compatibility angle. It\'s working. Perhaps because Ruby is already so hostile.<br><br>In reply to: <a href="https://x.com/aalmiray/status/825632489" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">825632489</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '825643670',
    created: 1212459442000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Google shell is a dream come true. You can sure I will be using that tool!<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/825641783" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">825641783</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '825642638',
    created: 1212459314000,
    type: 'post',
    text: 'An explosion at The Planet web host facility caused 9000 servers to go down. Took out several Manning sites. <a href="http://www.thewhir.com/marketwatch/060208_The_Planet_Explosion_Causes_Outages.cfm" rel="noopener noreferrer" target="_blank">www.thewhir.com/marketwatch/060208_The_Planet_Explosion_Causes_Outages.cfm</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '825397192',
    created: 1212434191000,
    type: 'post',
    text: 'A line from Deadliest Catch applies perfectly to writing a technical book: "You have to be a little bit twisted to do this job."',
    likes: 0,
    retweets: 0
  },
  {
    id: '825364932',
    created: 1212431239000,
    type: 'post',
    text: 'FOB == Car keys. Without the FOB, the car stops driving. I chased her all the way to Virginia to retrieve it. Boy was she shocked to see me.',
    likes: 0,
    retweets: 0
  },
  {
    id: '825363814',
    created: 1212431106000,
    type: 'post',
    text: 'Lack of sleep is causing me to do really stupid shit. Dropped wife off at Metro the other day and the FOB for Prius went with her.',
    likes: 0,
    retweets: 0
  },
  {
    id: '825361689',
    created: 1212430938000,
    type: 'post',
    text: 'Note to self: Don\'t do presentations at the last minute. It\'s painful.',
    likes: 0,
    retweets: 0
  },
  {
    id: '824775947',
    created: 1212351634000,
    type: 'post',
    text: 'A sign that an annotation should have been used in the design: <a href="http://static.springframework.org/spring/docs/2.5.x/api/org/springframework/beans/factory/InitializingBean.html" rel="noopener noreferrer" target="_blank">static.springframework.org/spring/docs/2.5.x/api/org/springframework/beans/factory/InitializingBean.html</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '824350213',
    created: 1212286722000,
    type: 'post',
    text: 'A great way to get good service at a restaurant is to bring a notepad. Was working on presentation slides and had waitress nervous as hell.',
    likes: 0,
    retweets: 0
  },
  {
    id: '821850855',
    created: 1211993655000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> The sacrifice he talks about nearly parallels what I have done for this book. The challenge is that few people pay visionaries.<br><br>In reply to: <a href="https://x.com/mraible/status/821390847" rel="noopener noreferrer" target="_blank">@mraible</a> <span class="status">821390847</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '821846696',
    created: 1211993297000,
    type: 'post',
    text: 'Microsoft is going to make support for ODF official, and take precedence over OOXML. Office document compatibility finally happening!',
    likes: 0,
    retweets: 0
  },
  {
    id: '821318915',
    created: 1211934603000,
    type: 'post',
    text: 'I played croquet on Sunday and kept hitting the Wicket. My wife said it was because it is my rival framework. Thought that was classic.',
    likes: 0,
    retweets: 0
  },
  {
    id: '818369246',
    created: 1211555698000,
    type: 'post',
    text: 'Doctoral thesis: A bare-machine web server. Dirty thoughts aside, I am thinking a it is web server operating system. Good idea.',
    likes: 0,
    retweets: 0
  },
  {
    id: '818367197',
    created: 1211555571000,
    type: 'post',
    text: 'Sis is cum laude. Way to go sis! I can\'t remember if I made it to magna. Shit, I lived through Cornell. I didn\'t wing myself off a bridge.',
    likes: 0,
    retweets: 0
  },
  {
    id: '818367066',
    created: 1211555566000,
    type: 'post',
    text: 'At my sister in law\'s graduation. Graduations always make me feel like I need to step up and work harder.',
    likes: 0,
    retweets: 0
  },
  {
    id: '816076664',
    created: 1211322107000,
    type: 'post',
    text: 'I can\'t believe that NetBeans doesn\'t support toggling comments in XML.',
    likes: 0,
    retweets: 0
  },
  {
    id: '812724329',
    created: 1210942044000,
    type: 'post',
    text: 'If you have too much time and money, writing a book is a great way to burn through both.',
    likes: 1,
    retweets: 0
  },
  {
    id: '812724047',
    created: 1210942016000,
    type: 'post',
    text: 'Just sent chapter 5 of Seam in Action off to copyediting and have the source code committed to match. My target date to finish is June 21.',
    likes: 0,
    retweets: 0
  },
  {
    id: '812723103',
    created: 1210941937000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Congrats on reaching the two third mark Emmanuel! I can appreciate it. Definitely put me down as a final reviewer!<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/812356485" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">812356485</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '812720720',
    created: 1210941716000,
    type: 'post',
    text: 'Seam 2.0.2.GA has been released: <a href="http://in.relation.to/Bloggers/Seam202GAIsOut" rel="noopener noreferrer" target="_blank">in.relation.to/Bloggers/Seam202GAIsOut</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '809013100',
    created: 1210560080000,
    type: 'post',
    text: 'I am going to need to build an ark soon it has rained so much since my return. Good thing I\'m a good swimmer.',
    likes: 0,
    retweets: 0
  },
  {
    id: '809012302',
    created: 1210559988000,
    type: 'post',
    text: 'Forgot to mention, had at least three opportunities to introduce myself to Rod Johnson, but subconsciously passed. Hmmm. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '807782154',
    created: 1210394092000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> I think that one IDE is a false goal. I run at least two ideas, sometimes three at a time. But do try NetBeans.<br><br>In reply to: <a href="https://x.com/aalmiray/status/807633303" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">807633303</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '807780568',
    created: 1210393884000,
    type: 'reply',
    text: '@sdnjavaone08 It was quite an experience, both JavaOne and the tweeting!<br><br>In reply to: <a href="https://x.com/Team_SDN/status/807571580" rel="noopener noreferrer" target="_blank">@Team_SDN</a> <span class="status">807571580</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '807780220',
    created: 1210393833000,
    type: 'post',
    text: 'I sure am glad to be back with my wife. She said her week without me was super productive. I just hope she doesn\'t kick me out ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '807779788',
    created: 1210393783000,
    type: 'post',
    text: 'I am not totally abandoning the concept and may even write an entry on my experience. I just need to put it down now.',
    likes: 0,
    retweets: 0
  },
  {
    id: '807779658',
    created: 1210393763000,
    type: 'post',
    text: 'My return home from JavaOne marks the end of my experiment with twitter. I must now concentrate on the completion of Seam in Action.',
    likes: 0,
    retweets: 0
  },
  {
    id: '807144207',
    created: 1210331597000,
    type: 'post',
    text: 'Forgot to say that the safety routine on Virgin was funny as hell. Animated cartoon w/ lots of sarcasm.',
    likes: 0,
    retweets: 0
  },
  {
    id: '807142939',
    created: 1210331434000,
    type: 'post',
    text: 'Airtran seats suck. Missing the Virgin cabin right now. Couldn\'t find coffee in during layover.',
    likes: 0,
    retweets: 0
  },
  {
    id: '806999488',
    created: 1210311853000,
    type: 'post',
    text: 'The ICEFaces developers said that they make heavy use of the event/observer in Seam which I think is an underused feature. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806957856',
    created: 1210306272000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iamroberthanson" rel="noopener noreferrer" target="_blank">@iamroberthanson</a> Congrats on the sellout!<br><br>In reply to: <a href="https://x.com/iamroberthanson/status/806903447" rel="noopener noreferrer" target="_blank">@iamroberthanson</a> <span class="status">806903447</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '806948571',
    created: 1210305196000,
    type: 'post',
    text: 'Sad to have to say goodbye to San Francisco. My programming heart will remain hear for sure. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806947822',
    created: 1210305111000,
    type: 'post',
    text: 'Just had dinner w/ <a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Very much enjoyed our conversation and look forward to hanging out again at Jazoon. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806946811',
    created: 1210305002000,
    type: 'post',
    text: 'Since ICEfaces team was so cool I am going to squeeze them into chp 12 somehow. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806946008',
    created: 1210304914000,
    type: 'post',
    text: 'JBoss party at Jillians was packed. I talked too much to get food. More chats with ICEFaces team. They seem to really like me. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806944814',
    created: 1210304791000,
    type: 'post',
    text: 'Podcast went pretty well with Kito. Tough to focus on little sleep though. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806944121',
    created: 1210304718000,
    type: 'post',
    text: 'I didn\'t know what to do with my bag, but airlines have a bag check service in Moscone South. Thank goodness. Got boarding pass too #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806796973',
    created: 1210289109000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/scottdavis99" rel="noopener noreferrer" target="_blank">@scottdavis99</a> Groovy support in NetBeans is crucial for NetBeans adoption and another jab at Eclipse for being slow on the uptake. #javaone<br><br>In reply to: <a href="https://x.com/scottdavis99/status/806750791" rel="noopener noreferrer" target="_blank">@scottdavis99</a> <span class="status">806750791</span>',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806796422',
    created: 1210289053000,
    type: 'post',
    text: 'About to step into a podcast with Kito. This is my first one! #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806796246',
    created: 1210289036000,
    type: 'post',
    text: 'I was quite because I was working on Seam in Action, which I have not done enough of in the last couple of days. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806697797',
    created: 1210279809000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/edyavno" rel="noopener noreferrer" target="_blank">@edyavno</a> You answered your own question. If project isn\'t tight for performance, then Groovy is clear winner. Go w/ Grails or Seam. #javaone<br><br>In reply to: <a href="https://x.com/edyavno/status/806687910" rel="noopener noreferrer" target="_blank">@edyavno</a> <span class="status">806687910</span>',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806693041',
    created: 1210279424000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/edyavno" rel="noopener noreferrer" target="_blank">@edyavno</a> Java and Groovy are just different. Perfect complements really. Whether you do web in Groovy over Java is another debate. #javaone<br><br>In reply to: <a href="https://x.com/edyavno/status/806687910" rel="noopener noreferrer" target="_blank">@edyavno</a> <span class="status">806687910</span>',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806692266',
    created: 1210279355000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/edyavno" rel="noopener noreferrer" target="_blank">@edyavno</a> You could have another static language on the JVM that does give you abbreviated syntax but would still be static and fast.<br><br>In reply to: <a href="https://x.com/edyavno/status/806687910" rel="noopener noreferrer" target="_blank">@edyavno</a> <span class="status">806687910</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '806691749',
    created: 1210279307000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/edyavno" rel="noopener noreferrer" target="_blank">@edyavno</a> Second, because it is dynamic, it is always going to have pseudo refactoring. Again, not a negative. But it ain\'t Java. #javaone<br><br>In reply to: <a href="https://x.com/edyavno/status/806687910" rel="noopener noreferrer" target="_blank">@edyavno</a> <span class="status">806687910</span>',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806690824',
    created: 1210279231000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/edyavno" rel="noopener noreferrer" target="_blank">@edyavno</a> 2 reasons Groovy is not the next Java. First, it is dynamic, so it will always be slower (not a negative). Java is for performance.<br><br>In reply to: <a href="https://x.com/edyavno/status/806687910" rel="noopener noreferrer" target="_blank">@edyavno</a> <span class="status">806687910</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '806659727',
    created: 1210276597000,
    type: 'post',
    text: 'If you think Groovy is the next Java, talk to <a class="mention" href="https://x.com/crichardson" rel="noopener noreferrer" target="_blank">@crichardson</a> and he will convince you otherwise. Personally, I dig Groovy. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806655049',
    created: 1210276192000,
    type: 'post',
    text: 'E-mail is fundamentally broken. When as the human race are we going to fix it?',
    likes: 0,
    retweets: 0
  },
  {
    id: '806653578',
    created: 1210276074000,
    type: 'post',
    text: 'Seriously, why do people still use HTML select menus in forms? This isn\'t even an RIA issue, it is an application not sucking issue.',
    likes: 0,
    retweets: 0
  },
  {
    id: '806652402',
    created: 1210275973000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iamroberthanson" rel="noopener noreferrer" target="_blank">@iamroberthanson</a> and coworker are undefeated in foosball in Moscone South. Challenge them before their egos get too big. #javaone<br><br>In reply to: <a href="https://x.com/iamroberthanson/status/806540419" rel="noopener noreferrer" target="_blank">@iamroberthanson</a> <span class="status">806540419</span>',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806630815',
    created: 1210274103000,
    type: 'post',
    text: 'I have to stop and try to plug the e-mail leak before the ship sinks. Talked to a guy last night about 0 inbox. A dream of mine. A dream.',
    likes: 0,
    retweets: 0
  },
  {
    id: '806630421',
    created: 1210274066000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> I tried Mylyn seriously for a couple of weeks. Generally, I like the idea, but it requires dedication to get into it.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/806605296" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">806605296</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '806599285',
    created: 1210271400000,
    type: 'post',
    text: 'I\'m going to the JBoss party tonight before catching the red-eye home to MD. Last chance to catch me. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806584379',
    created: 1210270201000,
    type: 'post',
    text: 'Going to breakfast at Mel\'s, a tradition my former manager started two years ago. Greasy food keeps you going. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806554844',
    created: 1210267854000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/crazybob" rel="noopener noreferrer" target="_blank">@crazybob</a> Same experience here with hotel maids. I just pretty much wait until I can embarrase them away.<br><br>In reply to: <a href="https://x.com/crazybob/status/806537478" rel="noopener noreferrer" target="_blank">@crazybob</a> <span class="status">806537478</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '806554094',
    created: 1210267798000,
    type: 'post',
    text: 'Didn\'t end up wearing any of the shortsleeved clothing that brought. Way too cold on the boundaries of the day.',
    likes: 0,
    retweets: 0
  },
  {
    id: '806553994',
    created: 1210267790000,
    type: 'post',
    text: 'Dragging ass and getting booted from my hotel (last day). Need to pack and head to Moscoe, find a comfy chair, and nap. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806553891',
    created: 1210267782000,
    type: 'post',
    text: 'Last night the dinner w/ Kito was at New Delhi. Highly recommended. I especially liked the fruit and nut Nan. Original. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806553768',
    created: 1210267772000,
    type: 'post',
    text: 'Forgot to mention that Reza\'s wife is an environmental educator. We shared ecostories.',
    likes: 0,
    retweets: 0
  },
  {
    id: '806247014',
    created: 1210241542000,
    type: 'post',
    text: '@iamroberhanson said that I am tweeting like crazy. My hope is to hit 300 updates by tomorrow night. Could cost me dearly.',
    likes: 0,
    retweets: 0
  },
  {
    id: '806246371',
    created: 1210241459000,
    type: 'post',
    text: 'I made the statement that "Seam is not JBoss". That was sort of nonsensical. I guess I just want people to appreciate it on merit. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806245846',
    created: 1210241398000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> I need to hear how 303 prevents global warming. I will ask <a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> at JBoss party tomorrow.<br><br>In reply to: <a href="https://x.com/dhanji/status/806244982" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">806244982</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '806245462',
    created: 1210241348000,
    type: 'post',
    text: 'Dinner with Kito was great, but I need to figure out what the hell I am doing next. I am just babbling. Same went with later discussions.',
    likes: 0,
    retweets: 0
  },
  {
    id: '806245004',
    created: 1210241302000,
    type: 'post',
    text: 'Forgot to mention that I talked to OpenOffice.org booth about using it for writing Seam in Action. Got a t-shirt for my effort. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806244549',
    created: 1210241242000,
    type: 'post',
    text: 'Is it possible to hate something so much that you wrap around and then love it? That is what I hope for Maven 2. Definitely *not* Windows.',
    likes: 0,
    retweets: 0
  },
  {
    id: '806244074',
    created: 1210241179000,
    type: 'post',
    text: 'Carlos didn\'t like my "compile-only" Maven scope idea. I am not doing well convincing people of things today. Must be tired. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806242932',
    created: 1210241032000,
    type: 'post',
    text: 'My shoulders are *killing* me. My backpack just hung on them all day almost pulling them out of sockets. Must invest in smaller computer.',
    likes: 0,
    retweets: 0
  },
  {
    id: '806242542',
    created: 1210240979000,
    type: 'post',
    text: 'There are *so* many different viewpoints in this community. The range is startling. For every advocate, there is a hater to match. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806241778',
    created: 1210240880000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/crazybob" rel="noopener noreferrer" target="_blank">@crazybob</a> casually said he might work on the next Java language, following mention that Groovy announced they might break syntax. #javaone<br><br>In reply to: <a href="https://x.com/crazybob/status/806232668" rel="noopener noreferrer" target="_blank">@crazybob</a> <span class="status">806232668</span>',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806240686',
    created: 1210240754000,
    type: 'post',
    text: 'This was a bad day not to have a session pass. Missed talk by <a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> on 303 and missed Java EE BOF. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806239292',
    created: 1210240537000,
    type: 'post',
    text: 'Rumor was that Groovy did well at the Scripting Bowl. However, the benchmark was skewed by the variation in developer talent. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806238484',
    created: 1210240431000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> I went through two last calls tonight. Stayed out way too late.<br><br>In reply to: <a href="https://x.com/mraible/status/806155623" rel="noopener noreferrer" target="_blank">@mraible</a> <span class="status">806155623</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '806238155',
    created: 1210240378000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/crazybob" rel="noopener noreferrer" target="_blank">@crazybob</a> I just noticed that too about the truncation of archives in the Recent list.<br><br>In reply to: <a href="https://x.com/crazybob/status/806232668" rel="noopener noreferrer" target="_blank">@crazybob</a> <span class="status">806232668</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '806237149',
    created: 1210240237000,
    type: 'post',
    text: 'Got into discussing GMail and how to manage mail. I gave up on my inbox long ago. My email owns me. Is GMail the best?',
    likes: 0,
    retweets: 0
  },
  {
    id: '806236820',
    created: 1210240199000,
    type: 'post',
    text: 'I got beat up pretty good for using Linux. I\'m just cannot stand Windows. Other people are going to have the reverse view. Agree to disagree',
    likes: 0,
    retweets: 0
  },
  {
    id: '806236623',
    created: 1210240165000,
    type: 'post',
    text: 'I guess I\'m just weird that I believe in Open Source for the better of human kind. No one else at the table sees it that way.',
    likes: 0,
    retweets: 0
  },
  {
    id: '806236466',
    created: 1210240147000,
    type: 'post',
    text: 'Hani and Cedric like proprietary software better. However, most agree that JIRA sucks. Personally, I dig JIRA. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806236195',
    created: 1210240107000,
    type: 'post',
    text: 'Most people hate Maven. Carlos says "pay me if you want it to be better." Then we fought about motivations behind open source. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806235981',
    created: 1210240077000,
    type: 'post',
    text: 'Got into religious debates over Linux vs Windows and Maven vs itself. That brought Carlos Sanchez into the discussion. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806235846',
    created: 1210240062000,
    type: 'post',
    text: 'Stuck with Bob, Cedric, and Hani, and others on to another bar. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806235559',
    created: 1210240024000,
    type: 'post',
    text: 'Met with Bob Lee, Cedric Beust, and Hani Suleiman when Eclipse party got broken up. Bob said not too interested in WebBeans atm. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806234275',
    created: 1210239861000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iamroberthanson" rel="noopener noreferrer" target="_blank">@iamroberthanson</a> is a Java developer. He makes that very clear. <a class="mention" href="https://x.com/crazybob" rel="noopener noreferrer" target="_blank">@crazybob</a> concurs. #javaone<br><br>In reply to: <a href="https://x.com/iamroberthanson/status/804865721" rel="noopener noreferrer" target="_blank">@iamroberthanson</a> <span class="status">804865721</span>',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806233856',
    created: 1210239804000,
    type: 'post',
    text: 'At JavaOne, all roads lead to the Thirsty Bear. They should just hold the BOFs in the damn bar. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806233761',
    created: 1210239791000,
    type: 'post',
    text: 'Met briefly with the Spring Web Flow developer. He probably doesn\'t give a shit who I am. Typical Spring these days. Ego. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806233542',
    created: 1210239757000,
    type: 'post',
    text: 'After a tour de SF we landed at the Eclipse party at the Bear. Spent most of the time at the bar w/ Rob and coworker. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806233424',
    created: 1210239739000,
    type: 'post',
    text: 'What a day! I am in dire need of rest after all of the socializing and debating. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806225530',
    created: 1210238653000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/snoopdave" rel="noopener noreferrer" target="_blank">@snoopdave</a> You mean you haven\'t been brainwashed by the iPhone crowd yet?<br><br>In reply to: <a href="https://x.com/snoopdave/status/806034448" rel="noopener noreferrer" target="_blank">@snoopdave</a> <span class="status">806034448</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '806225267',
    created: 1210238615000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/crichardson" rel="noopener noreferrer" target="_blank">@crichardson</a> Amen to that Chris. My feet are killing me! Standing in the pavilion with a backpack just isn\'t good for the body. #javaone<br><br>In reply to: <a href="https://x.com/crichardson/status/806038491" rel="noopener noreferrer" target="_blank">@crichardson</a> <span class="status">806038491</span>',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806062877',
    created: 1210217228000,
    type: 'post',
    text: 'Caught up w/ Robert Hanson of GWT In Action. I am really not just seeking out Manning authors I swear. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '806061796',
    created: 1210217111000,
    type: 'post',
    text: 'Just finished dinner w/ Kito. Thanks for the treat! Got to push for some improvements I want in JSF 2.0. XML Schema based views. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805951169',
    created: 1210205594000,
    type: 'post',
    text: 'About to have dinner with Kito. Need to talk to him about JSF creating strict XHTML. Even Struts does that. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805949980',
    created: 1210205465000,
    type: 'post',
    text: 'Just talked to the ICESoft team. Great bunch of folks. I need to spend more time with ICEFaces. Need more hours in the day. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805914730',
    created: 1210201848000,
    type: 'post',
    text: 'Need to remember all of these promises. I need a sidekick that writes this shit down. If I said something just email me. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805913425',
    created: 1210201725000,
    type: 'post',
    text: 'Meet a couple more folks by the JBoss booth, some developers some users. Talked some about future of Seam. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805878152',
    created: 1210198212000,
    type: 'post',
    text: 'Just met w/ Gavin King. He rides! Need to get my bro to send him some tires. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805868727',
    created: 1210197332000,
    type: 'post',
    text: 'I just spoke with the imfamous Mike Keith who kept manual flushing out of the JPA spec. Couldn\'t convince him in the short debate. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805850834',
    created: 1210195732000,
    type: 'post',
    text: 'Search for Linda\'s blog entry on JPA 2.0. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805846812',
    created: 1210195375000,
    type: 'post',
    text: 'Standing room only in the JPA 2.0 talk. No surprise since we all persist data. #javaone',
    likes: 1,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805844165',
    created: 1210195131000,
    type: 'post',
    text: 'I am outside the JPA 2.0 session going to challenge L. DeMichael about manual flushing. No brainer. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805843013',
    created: 1210195030000,
    type: 'post',
    text: 'Just met w/ Debu Panda EJB In Action author. 2E is in the works. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805823101',
    created: 1210193322000,
    type: 'post',
    text: 'Talked to Max Katz from Exadel. He is doing book on RichFaces due out in June. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805808414',
    created: 1210192072000,
    type: 'post',
    text: 'Just had a nice long talk w/ Jenni Aloi from IBM dW. Talk to her if you want to write. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805711892',
    created: 1210183878000,
    type: 'post',
    text: 'Dinner tonight and podcast tonight with w/ Kito mann.',
    likes: 0,
    retweets: 0
  },
  {
    id: '805708959',
    created: 1210183629000,
    type: 'reply',
    text: '@sdnjavaone08 I might need to check out that event. However, the competition for attention tonight is quite high. #javaone<br><br>In reply to: <a href="https://x.com/Team_SDN/status/805706741" rel="noopener noreferrer" target="_blank">@Team_SDN</a> <span class="status">805706741</span>',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805707184',
    created: 1210183495000,
    type: 'post',
    text: 'Finally got to sleep in this morning (more like my body decided). I am getting a real breakfast and then heading to the pavilion. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805706672',
    created: 1210183454000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> I haven\'t yet grasped how where Grails fits into JSF or even Seam. It isn\'t necessarily that the message is lacking, I\'m just busy<br><br>In reply to: <a href="https://x.com/aalmiray/status/805621950" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">805621950</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '805306780',
    created: 1210145802000,
    type: 'post',
    text: 'Would JSF suck less if it were Groovy based? I envision a Groovy builder that makes a JSF component tree. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805305567',
    created: 1210145636000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> OMG, you actually know about Studs? I feel like I\'ve lived two lives, one PHP, one Java. Studs is really what got me into Java.<br><br>In reply to: <a href="https://x.com/aalmiray/status/805292249" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">805292249</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '805304343',
    created: 1210145453000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/TheSteve0" rel="noopener noreferrer" target="_blank">@TheSteve0</a> Will be there tomorrow. Today was totally jammed up with my twitter experiment and lunch with Reza Rahman.<br><br>In reply to: <a href="https://x.com/TheSteve0/status/805224352" rel="noopener noreferrer" target="_blank">@TheSteve0</a> <span class="status">805224352</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '805303710',
    created: 1210145365000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> Pete Muir said that it would have to be remoting. There is no question that we need to make "bindable" remoting in Seam.<br><br>In reply to: <a href="https://x.com/aalmiray/status/805292249" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">805292249</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '805302136',
    created: 1210145115000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ikarzali" rel="noopener noreferrer" target="_blank">@ikarzali</a> The meaning of your question about Seam on Geronimo isn\'t clear.<br><br>In reply to: <a href="https://x.com/ikarzali/status/805301178" rel="noopener noreferrer" target="_blank">@ikarzali</a> <span class="status">805301178</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '805290842',
    created: 1210143536000,
    type: 'post',
    text: 'Cisco is using Seam on Geronimo with great success. No docs in Seam yet. They are about to deploy the application to production. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805284158',
    created: 1210142544000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/mojavelinux" rel="noopener noreferrer" target="_blank">@mojavelinux</a> Ah, looks like Exadel already as a JavaFX+Seam in the works. Binary remoting protocol. I\'ll look at it. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805279039',
    created: 1210141798000,
    type: 'post',
    text: 'Seam 2.0.2.CR2 is out: <a href="http://in.relation.to/Bloggers/Seam202CR2" rel="noopener noreferrer" target="_blank">in.relation.to/Bloggers/Seam202CR2</a> #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805277981',
    created: 1210141640000,
    type: 'post',
    text: 'If JavaFX has any merit at all, we need to get JavaFX+Seam going. Volunteers? #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805276305',
    created: 1210141400000,
    type: 'post',
    text: 'Sun needs to eat its own dog food more. Why are the replays of the general sessions not done in JavaFX? #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805275754',
    created: 1210141323000,
    type: 'post',
    text: 'I still have to try it (Glassfish) before I "buy" it, though. I am definitely getting support for it committed to seam-gen. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805275495',
    created: 1210141280000,
    type: 'post',
    text: 'Glassfish just continues to impress me. Dynamic modules in the mgmt console; dynamic feature loading; slick admin console. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805274837',
    created: 1210141190000,
    type: 'post',
    text: 'Watched the Sun General Session from the main hall. I am actually excited about Java EE 6 all by itself. Very impressive progress. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805273976',
    created: 1210141078000,
    type: 'post',
    text: 'I ran into several Seam in Action readers today. All very appreciative of my effort. Very encouraging as it has been a long haul. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805270598',
    created: 1210140647000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> Amen brother. Batteries and wifi are the suck. It\'s interesting how technology breaks when you lean on it. #javaone<br><br>In reply to: <a href="https://x.com/aalmiray/status/805268531" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">805268531</span>',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805268327',
    created: 1210140367000,
    type: 'post',
    text: 'Never made it to the pavilion today. I will immerse myself in vendor goodness debauchery tomorrow. My wife needs swag. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805264249',
    created: 1210139846000,
    type: 'post',
    text: 'Just tried to log into twitter and it was crashed. Good thing they are rewriting it in Java.',
    likes: 0,
    retweets: 0
  },
  {
    id: '805263968',
    created: 1210139810000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/scottdavis" rel="noopener noreferrer" target="_blank">@scottdavis</a> I forgot to mention to you when we talked that my current client is one of the biggest ESRI / GIS consulting shops in VA.<br><br>In reply to: <a href="https://x.com/davisl69/status/128420742" rel="noopener noreferrer" target="_blank">@davisl69</a> <span class="status">128420742</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '805236059',
    created: 1210133737000,
    type: 'post',
    text: 'There is NO reason that I should have been turned down to do this Seam talk. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805224722',
    created: 1210132094000,
    type: 'post',
    text: 'My mistake. About 20% of room uses JSF w/o Seam. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805222992',
    created: 1210131583000,
    type: 'post',
    text: 'Everyone in room has heard of Seam. Most haven\'t used it in day jobs. Probably using Struts. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805220458',
    created: 1210131300000,
    type: 'post',
    text: 'I didn\'t realize Yuan was involved in JBoss Seam 2E book. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805214648',
    created: 1210130691000,
    type: 'post',
    text: 'At Yuan\'s Seam BOF. Obviously not here to learn about Seam but to meet as many people as possible. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805208691',
    created: 1210130068000,
    type: 'post',
    text: 'So Reza put a challenge to Seam to make our stance on Java EE more clear. I think that generally we have some discussing to do.',
    likes: 0,
    retweets: 0
  },
  {
    id: '805205893',
    created: 1210129790000,
    type: 'post',
    text: 'Lack of network slows down my posting significantly (and is costing me $$) #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805204833',
    created: 1210129685000,
    type: 'post',
    text: 'Just ran into <a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> in pavilion. Hope to catch up with more Seam folk. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805203762',
    created: 1210129584000,
    type: 'post',
    text: 'Back from an early dinner and drink with Reza Rahman, EJB 3 In Action #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '805202390',
    created: 1210129455000,
    type: 'post',
    text: 'Back from a long silence. Was at early dinner and drink with Reza',
    likes: 0,
    retweets: 0
  },
  {
    id: '804972480',
    created: 1210105959000,
    type: 'post',
    text: 'Too bad COne attendees can\'t just buy a lunch. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804971417',
    created: 1210105863000,
    type: 'post',
    text: 'They are pulling in Lingo concept of async calls on side-by-side components into EJB 3.1. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804970246',
    created: 1210105765000,
    type: 'post',
    text: 'Cron-like functionality in EJB 3.1 Timer. Finally! Amen to making the syntax simple. I hate cron syntax.',
    likes: 0,
    retweets: 0
  },
  {
    id: '804968980',
    created: 1210105652000,
    type: 'post',
    text: 'Lots of Seam ideas getting roled into EJB 3.1 like startup components. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804967927',
    created: 1210105559000,
    type: 'post',
    text: 'Wow, this concurrency stuff in EJB 3.1 is going to be great for Seam to tap into. Good start. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804966503',
    created: 1210105430000,
    type: 'post',
    text: 'Seam just has so much more freedom to solve these EJB problems/needs faster. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804965241',
    created: 1210105315000,
    type: 'post',
    text: 'Oh my word, they are actually addressing the lack of standard JNDI naming. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804964049',
    created: 1210105211000,
    type: 'post',
    text: 'Finally no EJB JAR! I never found much use for EAR. Long live WARs. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804962722',
    created: 1210105101000,
    type: 'post',
    text: 'I wonder how they are doing interception on no interface components in EJB 3.1. Java doen\'t have standard bytecode enhancer #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804927724',
    created: 1210102023000,
    type: 'post',
    text: 'It\'s probably good the are spinning off JPA from EJB 3.1, though that is the current perception anyway. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804925956',
    created: 1210101861000,
    type: 'post',
    text: 'I\'m in on the EJB 3.1 talk. Yeah, i could read the spec. Call it tradition. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804904950',
    created: 1210099999000,
    type: 'post',
    text: 'Can\'t Linux figure out that I am in California? My clock should have updated automatically like my phone. Yes, I am demanding.',
    likes: 0,
    retweets: 0
  },
  {
    id: '804904130',
    created: 1210099917000,
    type: 'post',
    text: 'This session catalog for J1 is the suck. Seriously, it needs a Kayak.com type of user interface. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804901528',
    created: 1210099698000,
    type: 'post',
    text: 'I hate freakin\' logins. I spend 10% of my life logging in to stuff. Can we get beyond this crap?',
    likes: 0,
    retweets: 0
  },
  {
    id: '804898771',
    created: 1210099459000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SteveGio" rel="noopener noreferrer" target="_blank">@SteveGio</a> Oh yeah, the FB application has to deal with a shitty network. Wasn\'t impressed.<br><br>In reply to: @stevegio <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '804898234',
    created: 1210099408000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SteveGio" rel="noopener noreferrer" target="_blank">@SteveGio</a> True that. A session on a spec is probably best for those looking for motivation to read the spec.<br><br>In reply to: @stevegio <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '804897437',
    created: 1210099336000,
    type: 'post',
    text: 'For those wondering, I am not speaking at JavaOne. My talk didn\'t have NetBeans or GlassFish in the title. j/k #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804896143',
    created: 1210099217000,
    type: 'post',
    text: 'Just got twitter posts from phone setup. That will help when network sucks.',
    likes: 0,
    retweets: 0
  },
  {
    id: '804881509',
    created: 1210097918000,
    type: 'post',
    text: 'Seam in Action still holding in top two spots on manning.com. Note to self. Get it done! Damn I am slow. But thorough!',
    likes: 0,
    retweets: 0
  },
  {
    id: '804880801',
    created: 1210097857000,
    type: 'post',
    text: 'Sat in on Kito and Carol\'s Seam talk yesterday at COne. 99% raised hands for who has heard of Seam. Great showing! #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804880591',
    created: 1210097841000,
    type: 'post',
    text: 'Hit me up if you have Seam questions. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804878226',
    created: 1210097642000,
    type: 'post',
    text: 'Okay, browser tooltips to see a list of who is following someone is a waste of my time. Can I just get a list?',
    likes: 0,
    retweets: 0
  },
  {
    id: '804874466',
    created: 1210097317000,
    type: 'post',
    text: 'Paul Hammant claimed that PicoContainer is a much better core than Spring last night. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804872406',
    created: 1210097153000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marshallk" rel="noopener noreferrer" target="_blank">@marshallk</a> Yeah, the stupid session surveys are so low tech. They need to let us twitter session reviews. Bubbles? Is this 1980? #javaone<br><br>In reply to: <a href="https://x.com/marshallk/status/804843922" rel="noopener noreferrer" target="_blank">@marshallk</a> <span class="status">804843922</span>',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804871215',
    created: 1210097056000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SteveGio" rel="noopener noreferrer" target="_blank">@SteveGio</a> It\'s the network gods really.<br><br>In reply to: @stevegio <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '804868399',
    created: 1210096813000,
    type: 'post',
    text: 'Thanks for the free No Fluff Just Stuff vol 2 book Jay! (given away at G2One mini). #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804867993',
    created: 1210096784000,
    type: 'reply',
    text: '@newsmavens Guillaume LaForge answered this last night at G2One mini. He made no delusions that Java will always be faster. #javaone<br><br>In reply to: @newsmavens <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804866761',
    created: 1210096678000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/arnihermann" rel="noopener noreferrer" target="_blank">@arnihermann</a> The wireless in the general sessions is shit.<br><br>In reply to: <a href="https://x.com/arnihermann/status/804863498" rel="noopener noreferrer" target="_blank">@arnihermann</a> <span class="status">804863498</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '804866586',
    created: 1210096663000,
    type: 'post',
    text: 'summize.com definitely needs Ajax Push.',
    likes: 0,
    retweets: 0
  },
  {
    id: '804866130',
    created: 1210096622000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SteveGio" rel="noopener noreferrer" target="_blank">@SteveGio</a> Why are standards so bad? You only make them have less chance by opting out. Vote!<br><br>In reply to: @stevegio <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '804865122',
    created: 1210096530000,
    type: 'post',
    text: 'I told at least 5 people last night how awesome and supportive my wife is. What 6 year marriage beats that?',
    likes: 0,
    retweets: 0
  },
  {
    id: '804864171',
    created: 1210096448000,
    type: 'post',
    text: 'I have now made more twitter updates in 3 days than I have written blog entries in 6 years!',
    likes: 0,
    retweets: 0
  },
  {
    id: '804862907',
    created: 1210096340000,
    type: 'reply',
    text: '@JavaOne2008 Companies need to understand, though, that a developer != designer. Java doesn\'t just hand you designs. #javaone<br><br>In reply to: <a href="https://x.com/JavaOne/status/804844374" rel="noopener noreferrer" target="_blank">@JavaOne</a> <span class="status">804844374</span>',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804862105',
    created: 1210096269000,
    type: 'post',
    text: 'Too many diet drinks. The coolers are left half full because nobody wants them. I would say just do non-diet + waters. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804853628',
    created: 1210095540000,
    type: 'post',
    text: 'What we need is async posting with offline app. I freeze up waiting for the post to go through.',
    likes: 0,
    retweets: 0
  },
  {
    id: '804853390',
    created: 1210095520000,
    type: 'post',
    text: 'I\'m still not sure how well Twitter scales. Either that or this network is just shit.',
    likes: 0,
    retweets: 0
  },
  {
    id: '804853218',
    created: 1210095505000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/crazybob" rel="noopener noreferrer" target="_blank">@crazybob</a> No face either.<br><br>In reply to: <a href="https://x.com/crazybob/status/804834790" rel="noopener noreferrer" target="_blank">@crazybob</a> <span class="status">804834790</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '804850860',
    created: 1210095306000,
    type: 'post',
    text: 'I need to pick out my two sessions. I wasn\'t expecting that so I am not registered for anything. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804850528',
    created: 1210095276000,
    type: 'post',
    text: 'Technology is key to saving the environment; tells us where we stand. No numbers == handwaving. Why is Al Gore not on stage? #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804850317',
    created: 1210095258000,
    type: 'post',
    text: 'This sensor thing is a bit too big brother for me. We have to be careful what we ask for. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804850223',
    created: 1210095249000,
    type: 'post',
    text: 'Neil is working on the environment. OpenEco got facetime too. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804849965',
    created: 1210095226000,
    type: 'post',
    text: 'I scratched more records than I played. That was a decade before I drank my first cup of coffee. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804849851',
    created: 1210095217000,
    type: 'post',
    text: 'Where is Neil Young\'s face? #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804849772',
    created: 1210095208000,
    type: 'post',
    text: 'Does Neil Young know what Java is? I guess you can\'t make music these days without technology somewhere. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804849692',
    created: 1210095199000,
    type: 'post',
    text: 'Hostile operating system...hmmm. Windows? Hostile browser...hmmm. IE? Great jab Jonathan! #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804849620',
    created: 1210095194000,
    type: 'post',
    text: 'Keep making Java accessible and fast Sun because you aren\'t without dissent. Don\'t mistake that. Python, Ruby, even PHP. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804849478',
    created: 1210095180000,
    type: 'post',
    text: 'Hydrozine? This might be too pipeline for me. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804849373',
    created: 1210095171000,
    type: 'post',
    text: 'The network is as vital to technology as water is to biology. Screw paying for the internet! #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804824801',
    created: 1210093188000,
    type: 'post',
    text: 'I am cold on MySQL. I really don\'t believe it is a real database anymore. H2! #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804824294',
    created: 1210093142000,
    type: 'post',
    text: 'The network is as vital to technology is water biology. Screw paying for the internet! #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804824178',
    created: 1210093134000,
    type: 'post',
    text: 'Android just got demo love. Google thanks you Sun. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804824059',
    created: 1210093125000,
    type: 'post',
    text: 'Audio and video in JavaFX. Amen if we can get the VM handling this stuff cross platform. I am sick of crappy multimedia on Linux. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804823910',
    created: 1210093115000,
    type: 'post',
    text: 'Drag out of the browser using Java plugin (implemented using JavaFX). Do that in flash. I do see the vision. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804823818',
    created: 1210093106000,
    type: 'post',
    text: 'Name a major social networking tool running Java. I\'m just saying. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804823619',
    created: 1210093090000,
    type: 'post',
    text: 'Facebook is now on screen. A plugin in nice, but Facebook needs to be Java to really talk. I\'m just saying. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804823494',
    created: 1210093080000,
    type: 'post',
    text: 'Kayak.com and Twitter just showed up on screen. Kayak got me here so I can attest to it. Twitter, well, it needs ot be Java. #javaone',
    likes: 1,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804823310',
    created: 1210093062000,
    type: 'post',
    text: 'RIA is a great goal, but poor servers. Saw a talk on Ajax Push using NIO yesterday and the client server paradigm is back. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804823165',
    created: 1210093052000,
    type: 'post',
    text: 'Rich Green is up at 6:20 AM! That is just after my bedtime. Then again, he lives in the past in CA, so I have 3 hours to catch up. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804822834',
    created: 1210093022000,
    type: 'post',
    text: 'The pipe at JavaOne is not working for me on a laptop. This wifi song and dance sucks. It is messing the demos too. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804822754',
    created: 1210093013000,
    type: 'post',
    text: 'Ubuntu is quite literally failing me. Frustrating. I cannot believe how badly it is breaking down in the field.',
    likes: 0,
    retweets: 0
  },
  {
    id: '804822723',
    created: 1210093011000,
    type: 'post',
    text: 'Amazon CEO is buying a book on stage. Buy Seam in Action! Haha #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804822552',
    created: 1210092993000,
    type: 'post',
    text: 'Kindle is on center stage. They have it integrated with Amazon.com now. There is no question I need one of those. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804822410',
    created: 1210092982000,
    type: 'post',
    text: 'Breakdancers at the JavaOne opener? Interesting. Impressive. But interesting. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804822297',
    created: 1210092972000,
    type: 'post',
    text: 'I was on the receiving end of the t-shirt launch! What are the chances of that? Pinch me Google, I\'m feeling lucky! #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804789289',
    created: 1210090416000,
    type: 'post',
    text: 'I actually made it to breakfast (after some hustle), but no breakfast for CommunityOne converts. Worse! #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804743683',
    created: 1210087002000,
    type: 'reply',
    text: '@JavaOne2008 My alarm clock couldn\'t get me to breakfast in time. Only if were a dumbell that dropped from the ceiling.<br><br>In reply to: <a href="https://x.com/JavaOne/status/804739039" rel="noopener noreferrer" target="_blank">@JavaOne</a> <span class="status">804739039</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '804742929',
    created: 1210086946000,
    type: 'post',
    text: 'What would really impress me is if Neil Young showed up at the Thirsty Bear #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804742217',
    created: 1210086894000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/crichardson" rel="noopener noreferrer" target="_blank">@crichardson</a>, thanks for the dinner! It was very tasty (and just the right amount spicy). You saved me before I went unconscious.<br><br>In reply to: <a href="https://x.com/crichardson/status/804354476" rel="noopener noreferrer" target="_blank">@crichardson</a> <span class="status">804354476</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '804741341',
    created: 1210086827000,
    type: 'post',
    text: 'I am dragging ass in a major way.',
    likes: 0,
    retweets: 0
  },
  {
    id: '804507941',
    created: 1210061449000,
    type: 'post',
    text: 'Walked by the bookstore almost in tears the Seam in Action cannot be a part of it. That\'s okay, rushing it is just vein. All things in time.',
    likes: 0,
    retweets: 0
  },
  {
    id: '804505594',
    created: 1210061127000,
    type: 'post',
    text: 'Found out that I get two free sessions for attending CommunityOne. Bonus! #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804503982',
    created: 1210060907000,
    type: 'post',
    text: '6:30 AM is going to come early. This is becoming a theme. I like my 11 AM rises. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804503889',
    created: 1210060895000,
    type: 'post',
    text: 'I hate this computer. If I can figure out how to twitter without it, I will just ditch it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '804503823',
    created: 1210060887000,
    type: 'post',
    text: 'Props to Atlassian for a great party. Props to Contegix for hosting the Maven 2 repository. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804503457',
    created: 1210060838000,
    type: 'post',
    text: 'I need to drink more often so that I can avoid spilling beers on people I just meet. I still say it was the excitement talking. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804503311',
    created: 1210060820000,
    type: 'post',
    text: 'Met more people in person for the first time than I can count. Even some I didn\'t expect to see! #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804503234',
    created: 1210060812000,
    type: 'post',
    text: 'By tomorrow I\'ll have attended 3 conferences in 24 hours. The G2One mini-conference was great. Good to see the NFJS folk again. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804502920',
    created: 1210060773000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/crazybob" rel="noopener noreferrer" target="_blank">@crazybob</a> The party is going strong already crazybob!<br><br>In reply to: <a href="https://x.com/crazybob/status/804418366" rel="noopener noreferrer" target="_blank">@crazybob</a> <span class="status">804418366</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '804310960',
    created: 1210036519000,
    type: 'post',
    text: 'May stop by G2One after dinner and then to the Bear. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804310729',
    created: 1210036497000,
    type: 'post',
    text: 'However, I am currently running on 2 hours of sleep and 1 cookie, so I think food is in the works. Meet up with <a class="mention" href="https://x.com/crichardson" rel="noopener noreferrer" target="_blank">@crichardson</a>. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804309324',
    created: 1210036346000,
    type: 'post',
    text: 'Overall, I think CommunityOne was well worth it. Its always tough to pick the right sessions. Got in a couple good ones. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804308533',
    created: 1210036257000,
    type: 'reply',
    text: '@JavaOne2008 Yeah, I\'ve been watching that all day and its just tantilizing.<br><br>In reply to: <a href="https://x.com/JavaOne/status/804241070" rel="noopener noreferrer" target="_blank">@JavaOne</a> <span class="status">804241070</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '804221085',
    created: 1210026511000,
    type: 'post',
    text: 'Of course, Seam already supports all of this Groovy stuff ;) Its just nice to see that JSF is natively supporting it. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804219431',
    created: 1210026327000,
    type: 'post',
    text: 'Why must every appserver make 404 error pages look like Tomcat? Make it something fun! #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804217947',
    created: 1210026171000,
    type: 'post',
    text: 'Sun is not messing around with Glassfish V3. They have a Maven 2 plugin, a Ruby gem, and java -jar glassfish.jar app.war to launch. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804216958',
    created: 1210026065000,
    type: 'post',
    text: 'Groovy JSF, now we are talking! It got caught in my overload filter earlier this week. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804206744',
    created: 1210025026000,
    type: 'post',
    text: 'My computer officially hates me. I\'m tempted to get an iPhone just to spite it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '804200071',
    created: 1210024363000,
    type: 'post',
    text: 'Sun is claiming that Glassfish V2 is the fasted Java EE server. Are there metrics for that? Don\'t get me wrong, I like Glassfish. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804195334',
    created: 1210023900000,
    type: 'reply',
    text: '@JavaOne2008 Darn, I missed lunch. Now worries, technology over food!<br><br>In reply to: <a href="https://x.com/JavaOne/status/804138526" rel="noopener noreferrer" target="_blank">@JavaOne</a> <span class="status">804138526</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '804106529',
    created: 1210015787000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Glad I\'m not the only one to admit it. I drive my wife insane before a trip, running all over the house.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/804018211" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">804018211</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '804105880',
    created: 1210015729000,
    type: 'post',
    text: 'I\'m excited to see what is going on with NetBeans and IcedTea (OpenJDK) at CommunityOne. NetBeans has come around. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804105711',
    created: 1210015711000,
    type: 'post',
    text: 'Delays are killing me today. First the Virgin flight. Now the hotel. Front desk person is at lunch. Hey, its $93.',
    likes: 0,
    retweets: 0
  },
  {
    id: '804105579',
    created: 1210015700000,
    type: 'post',
    text: 'BTW, Happy Cinco de Mayo all you JavaOne followers! A good night to start drinking. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '804105414',
    created: 1210015682000,
    type: 'post',
    text: 'I\'ll tell you, one baby will kill your flight. Caught a brief power nap, but not much after that.',
    likes: 0,
    retweets: 0
  },
  {
    id: '804105318',
    created: 1210015672000,
    type: 'post',
    text: 'Virgin American planes look cool. Retro lights, leather seats, and an inflight music systems. Richard Branson knows how to roll.',
    likes: 0,
    retweets: 0
  },
  {
    id: '804104860',
    created: 1210015630000,
    type: 'post',
    text: 'No free wifi in Dullus International. Common people, get with the times.',
    likes: 0,
    retweets: 0
  },
  {
    id: '804104625',
    created: 1210015607000,
    type: 'post',
    text: '5 AM came early. Ouch!',
    likes: 0,
    retweets: 0
  },
  {
    id: '803637070',
    created: 1209964157000,
    type: 'post',
    text: 'I need to get to bed, that 5:00am wake up call to catch my flight is fast approaching.',
    likes: 0,
    retweets: 0
  },
  {
    id: '803631667',
    created: 1209963459000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> I am feeling that hurt in a major way right now. I often yell at myself for making no sense.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/803570641" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">803570641</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '803425609',
    created: 1209937093000,
    type: 'post',
    text: 'There\'s some sort of cruel irony in having to search for one\'s glasses.',
    likes: 0,
    retweets: 0
  },
  {
    id: '803416878',
    created: 1209935923000,
    type: 'post',
    text: 'I guess the next step is for the airline to remind you of your flight via twitter.',
    likes: 0,
    retweets: 0
  },
  {
    id: '803403845',
    created: 1209934314000,
    type: 'post',
    text: 'Virgin American reminds you of your flight via email, reminding you to check-in online. Way to be!',
    likes: 0,
    retweets: 0
  },
  {
    id: '803402388',
    created: 1209934142000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> There is still skepticism in the world that wikipedia is a credible source, but to others that is just FUD talking.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/803387987" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">803387987</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '803384327',
    created: 1209931884000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Is it frightening that us engineer types do research using wikipedia? I can admit to using it for Seam in Action.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/803374363" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">803374363</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '803270057',
    created: 1209918130000,
    type: 'post',
    text: 'I swear you could spend your life replying to e-mail.',
    likes: 0,
    retweets: 0
  },
  {
    id: '803229281',
    created: 1209913398000,
    type: 'post',
    text: 'Getting my stuff together for an early morning departure tomorrow. It\'s going to be a busy day.',
    likes: 0,
    retweets: 0
  },
  {
    id: '802921672',
    created: 1209864180000,
    type: 'post',
    text: 'Trying to learn how to find the right words without thesaurus.com. Some controversy forced them to switch sources, killing its usefulness.',
    likes: 0,
    retweets: 0
  },
  {
    id: '802833962',
    created: 1209852057000,
    type: 'post',
    text: 'My plan is to hit the JavaBloggers beerfest Mon, the JBoss bash Thurs. and maybe the Eclipse gala Wed. (I\'m cold on Eclipse atm) #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '802755102',
    created: 1209841445000,
    type: 'post',
    text: 'In truth, I\'m still feeling the pain of finishing Seam in Action, which I will be working on from my hotel room. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '802754831',
    created: 1209841416000,
    type: 'post',
    text: 'My motto will be "I\'m just here for the beer." #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '802753395',
    created: 1209841238000,
    type: 'post',
    text: 'I am going to twitter at JavaOne from the fringe. I will be there Monday 10:30 am - Thurs 11:00 pm. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '802750447',
    created: 1209840864000,
    type: 'post',
    text: 'I had to change the default colors on Twitter. That ugly colors are really what turned me off of twitter from the start. Eye candy!',
    likes: 0,
    retweets: 0
  },
  {
    id: '802750089',
    created: 1209840819000,
    type: 'post',
    text: 'Firefox is being a total PITA. A new tab takes 15 seconds to open! Definitely a conflict with my addons.',
    likes: 0,
    retweets: 0
  },
  {
    id: '802749578',
    created: 1209840751000,
    type: 'post',
    text: 'I finally caved to the peer pressure and joined Twitter. I going to use it to "plug in" to JavaOne. As if I ever unplug.',
    likes: 0,
    retweets: 0
  }
])
