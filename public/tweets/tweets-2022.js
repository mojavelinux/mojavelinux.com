'use strict'

inflateTweets([
  {
    id: '1609434378308046848',
    created: 1672554032000,
    type: 'post',
    text: 'Happy New Year, y\'all. Let\'s make it a good one!',
    likes: 13,
    retweets: 0
  },
  {
    id: '1607142687408541696',
    created: 1672007650000,
    type: 'post',
    text: 'Holiday toys! (It\'s been more than a minute since I\'ve played anything). <a class="mention" href="https://x.com/KLASKgame" rel="noopener noreferrer" target="_blank">@KLASKgame</a>',
    photos: ['<div class="item"><img class="photo" src="media/1607142687408541696.jpg"></div>'],
    likes: 7,
    retweets: 0
  },
  {
    id: '1606866381160730624',
    created: 1671941774000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/eddiejaoude" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> 💯<br><br>In reply to: <a href="https://x.com/eddiejaoude/status/1606855822109188101" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> <span class="status">1606855822109188101</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1606519502786920448',
    created: 1671859071000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> <a class="mention" href="https://x.com/Java_Champions" rel="noopener noreferrer" target="_blank">@Java_Champions</a> <a class="mention" href="https://x.com/jbangdev" rel="noopener noreferrer" target="_blank">@jbangdev</a> <a class="mention" href="https://x.com/javabake" rel="noopener noreferrer" target="_blank">@javabake</a> <a class="mention" href="https://x.com/sdkman_" rel="noopener noreferrer" target="_blank">@sdkman_</a> <a class="mention" href="https://x.com/github" rel="noopener noreferrer" target="_blank">@github</a> <a href="https://github.com/aalmiray/java-champions" rel="noopener noreferrer" target="_blank">github.com/aalmiray/java-champions</a><br><br>In reply to: <a href="https://x.com/maxandersen/status/1606506419645284353" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1606506419645284353</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1606142220221808640',
    created: 1671769120000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> Wow! Magical!<br><br>In reply to: <a href="https://x.com/starbuxman/status/1605503112189394944" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">1605503112189394944</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1605640282652831744',
    created: 1671649449000,
    type: 'post',
    text: 'As of today, winter is ending*<br><br>*in the North Hemisphere',
    likes: 4,
    retweets: 0
  },
  {
    id: '1605330791071772672',
    created: 1671575660000,
    type: 'post',
    text: 'In other news, I would never work at a company with a corporate firewall that does shit like this. I hope companies realize how ridiculous it is to block sites from adult professionals. Trust much?',
    likes: 6,
    retweets: 0
  },
  {
    id: '1605330317857865728',
    created: 1671575548000,
    type: 'post',
    text: '"I\'m using the issue tracker to ask questions, despite being asked not to, because my corporate firewall blocks the chat URL" is the latest in unacceptable behavior I\'ve observed in OSS and absolutely not my problem.',
    likes: 8,
    retweets: 0
  },
  {
    id: '1605313058754002944',
    created: 1671571433000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> <a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> FYI, Asciidoctor core has no dependencies (except for optional features).<br><br>In reply to: <a href="https://x.com/headius/status/1605230307552202753" rel="noopener noreferrer" target="_blank">@headius</a> <span class="status">1605230307552202753</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1604946858224541696',
    created: 1671484124000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> C<br><br>In reply to: <a href="https://x.com/ggrossetie/status/1604874859552690176" rel="noopener noreferrer" target="_blank">@ggrossetie</a> <span class="status">1604874859552690176</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1604696815189663745',
    created: 1671424509000,
    type: 'post',
    text: 'Oh, this is going to be a fun week. 🥶',
    photos: ['<div class="item"><img class="photo" src="media/1604696815189663745.jpg"></div>'],
    likes: 2,
    retweets: 0
  },
  {
    id: '1602699823748190211',
    created: 1670948389000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> 👍<br><br>In reply to: <a href="https://x.com/abelsromero/status/1602685486698569729" rel="noopener noreferrer" target="_blank">@abelsromero</a> <span class="status">1602685486698569729</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1602699736670244866',
    created: 1670948368000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> Ooooh. I would want that too!<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/1602644389867753473" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">1602644389867753473</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1602449226088255488',
    created: 1670888642000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> Got it. I was missing the term "Back" when looking through the keymap. I keep thinking it was "Last Location" or something. In hindsight, it makes perfect sense. I was overthinking it ;)<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/1602446521831948288" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">1602446521831948288</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1602446023850442752',
    created: 1670887878000,
    type: 'post',
    text: 'Aha! Alt+Left (Back). Got it.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1602445916862320641',
    created: 1670887853000,
    type: 'post',
    text: 'In IntelliJ, how do you jump to previous cursor location (not edit location) after navigating using Ctrl+Click?',
    likes: 1,
    retweets: 0
  },
  {
    id: '1602420294102454273',
    created: 1670881744000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> The cleanest solution I can think of is to wrap the table in a div...which I\'ll need to do with JavaScript. So, yep. Yep, indeed.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1602286322818990080" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1602286322818990080</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1602277560007565313',
    created: 1670847713000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> Yep. It\'s a mess. Unfortunately, when I\'m styling HTML I don\'t control (such as HTML converted from AsciiDoc), I\'m just lost trying to get the table to behave.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1602275779936751616" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1602275779936751616</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1602275308316422146',
    created: 1670847176000,
    type: 'post',
    text: 'overflow-x auto, that is (but it didn\'t fit in the tweet ;))',
    likes: 0,
    retweets: 0
  },
  {
    id: '1602270492894773254',
    created: 1670846028000,
    type: 'post',
    text: 'I really wish you could just set `max-width: 100%; scroll: auto` on a table and have it do what every other element in HTML does when the content overflows the parent container. It\'s maddening that it doesn\'t work that way. (And no, `table-layout: fixed` is just absurdly broken).',
    likes: 3,
    retweets: 0
  },
  {
    id: '1602134718295601153',
    created: 1670813657000,
    type: 'post',
    text: 'To be fair, I should say they had some chemistry. Lyle and Donville really worked great together. But the chemistry (and trust) on that team is few and far between.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1602126701646868481',
    created: 1670811746000,
    type: 'post',
    text: 'There\'s no question that <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> is having an impact on <a class="mention" href="https://x.com/NLL" rel="noopener noreferrer" target="_blank">@NLL</a> viewership, at least in my mind. I, for one, am a PLL fan who\'s watching NLL to see my favorite players play. I\'m quickly warming up to the box lacrosse format, despite initially being very skeptical I would.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1602086204576522240',
    created: 1670802091000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> I think the one thing the Cannons need is stability. I get the need for free agency activity. But after that, they need to stop blowing up the roster. Chemistry is critical for lacrosse, and they have none.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1602085359948636165',
    created: 1670801889000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> I really want to see Spallina as a PLL coach. I never watched the MLL, so it would be an honor to see him in his element before his career is in the record books. I think the PLL is the way to go out with a 💥.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1602081088284885000',
    created: 1670800871000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> My new hobby is sneaking out to the brewery and listening to a Sticks In episode.',
    photos: ['<div class="item"><img class="photo" src="media/1602081088284885000.jpg"></div>'],
    likes: 2,
    retweets: 1
  },
  {
    id: '1601324506613587968',
    created: 1670620488000,
    type: 'post',
    text: 'Documentation is such a huge backstop. I couldn\'t do support efficiently without it.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1600966484976541696',
    created: 1670535129000,
    type: 'quote',
    text: 'I\'m in shock and completely gutted. I interacted with Chris through his work on TruffleRuby. His work has been, and will continue to be, foundational to the advancement of Ruby. Whenever I interacted with Chris, he always brought his best game and great attitude. It was a honor.<br><br>Quoting: <a href="https://x.com/flavorjones/status/1600436490885947393" rel="noopener noreferrer" target="_blank">@flavorjones</a> <span class="status">1600436490885947393</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1600607615242842112',
    created: 1670449567000,
    type: 'quote',
    text: 'Wow. That\'s action.<br><br>Quoting: <a href="https://x.com/jetbrains/status/1600084596325986304" rel="noopener noreferrer" target="_blank">@jetbrains</a> <span class="status">1600084596325986304</span>',
    likes: 8,
    retweets: 2
  },
  {
    id: '1600468858976206849',
    created: 1670416485000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> <a class="mention" href="https://x.com/AndyCopelan_PLL" rel="noopener noreferrer" target="_blank">@AndyCopelan_PLL</a> I finally had a chance to listen to this episode. You\'re a great interviewer! Andy sounded very relaxed and provided great answers to some tough questions. It was really awesome to get to know more about who he is. He seems to be such a gracious dude. Made me even more of a fan.<br><br>In reply to: <a href="https://x.com/danarestia/status/1598734630747885570" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1598734630747885570</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1600452904908779520',
    created: 1670412682000,
    type: 'post',
    text: 'Does GitHub Actions have some way to show the test coverage result of a CI workflow in the web interface?',
    likes: 2,
    retweets: 0
  },
  {
    id: '1599889217865543680',
    created: 1670278288000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rob_winch" rel="noopener noreferrer" target="_blank">@rob_winch</a> <a class="mention" href="https://x.com/maciejwalkowiak" rel="noopener noreferrer" target="_blank">@maciejwalkowiak</a> <a class="mention" href="https://x.com/marcusdacoregio" rel="noopener noreferrer" target="_blank">@marcusdacoregio</a> <a class="mention" href="https://x.com/NiestrojRobert" rel="noopener noreferrer" target="_blank">@NiestrojRobert</a> <a class="mention" href="https://x.com/springboot" rel="noopener noreferrer" target="_blank">@springboot</a> "a few items to fix" is putting it mildly. There were some major advancements we needed in Antora first, and that\'s precisely why we took on this project. Spring is helping to advance Antora for everyone\'s benefit.<br><br>In reply to: <a href="https://x.com/rob_winch/status/1523701958988632064" rel="noopener noreferrer" target="_blank">@rob_winch</a> <span class="status">1523701958988632064</span>',
    likes: 6,
    retweets: 0
  },
  {
    id: '1599683055933091840',
    created: 1670229135000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/NLL" rel="noopener noreferrer" target="_blank">@NLL</a> <a class="mention" href="https://x.com/newyorkriptide" rel="noopener noreferrer" target="_blank">@newyorkriptide</a> Should have been 5 goals. Taking away that final goal was absolute robbery. Every person on the planet who watched the result of those lightening fast reflexes knows that ball crossed the goal line...multiple times. But it\'s cool because we still got to witness it. 🌊<br><br>In reply to: <a href="https://x.com/NLL/status/1599244287228755968" rel="noopener noreferrer" target="_blank">@NLL</a> <span class="status">1599244287228755968</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1599337427059236864',
    created: 1670146731000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/PlantUML" rel="noopener noreferrer" target="_blank">@PlantUML</a> That means a lot to me, both that it\'s helping you and the fact that you appreciate the effort that goes into providing it. Thanks!<br><br>In reply to: <a href="https://x.com/agoncal/status/1599331572032774146" rel="noopener noreferrer" target="_blank">@agoncal</a> <span class="status">1599331572032774146</span>',
    likes: 5,
    retweets: 1
  },
  {
    id: '1598937657706311680',
    created: 1670051419000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/NLLwings" rel="noopener noreferrer" target="_blank">@NLLwings</a> And the leading scorer, no less!<br><br>In reply to: <a href="#1597725220605165568">1597725220605165568</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1598818864955293696',
    created: 1670023096000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CoachKav44" rel="noopener noreferrer" target="_blank">@CoachKav44</a> I, for one, am doing my part to treat them like superheros, because that\'s what they are. Every game they put on is a championship game. It\'s an absolute privilege to be witness to such wizardry, whether it be a shot, pass, save, stick check, or ground ball.<br><br>In reply to: <a href="https://x.com/CoachKav44/status/1598765041301454850" rel="noopener noreferrer" target="_blank">@CoachKav44</a> <span class="status">1598765041301454850</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1598621935910662144',
    created: 1669976145000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rotnroll666" rel="noopener noreferrer" target="_blank">@rotnroll666</a> <a class="mention" href="https://x.com/maciejwalkowiak" rel="noopener noreferrer" target="_blank">@maciejwalkowiak</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> Just to name a few things we\'ve got working now with Antora:<br><br>- Gradle-based build<br>- docs version populated by Gradle<br>- Gradle property to AsciiDoc attribute mapping<br>- tabs (+ automated migration)<br>- code chomping &amp; folding<br>- release line aliasing<br>- partial builds<br>- &amp; more to come<br><br>In reply to: <a href="https://x.com/rotnroll666/status/1598617501558050816" rel="noopener noreferrer" target="_blank">@rotnroll666</a> <span class="status">1598617501558050816</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1598620525097459712',
    created: 1669975808000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rotnroll666" rel="noopener noreferrer" target="_blank">@rotnroll666</a> <a class="mention" href="https://x.com/maciejwalkowiak" rel="noopener noreferrer" target="_blank">@maciejwalkowiak</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> And the big updates are yet to land. I\'m building on what Rob started and trying to push Antora further in the process. For example, we now have partial builds working (build per reference). See <a href="https://github.com/spring-projects/spring-security/actions/runs/3600712975/jobs/6065671097" rel="noopener noreferrer" target="_blank">github.com/spring-projects/spring-security/actions/runs/3600712975/jobs/6065671097</a><br><br>In reply to: <a href="https://x.com/rotnroll666/status/1598617501558050816" rel="noopener noreferrer" target="_blank">@rotnroll666</a> <span class="status">1598617501558050816</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1598620089599029248',
    created: 1669975704000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maciejwalkowiak" rel="noopener noreferrer" target="_blank">@maciejwalkowiak</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> And an important experiment too. I just wanted you to be aware that this is the direction things are heading. Any input you can provide from your experience (what works, what doesn\'t, what has to be changed in the Spring Framework docs) will certainly be valuable.<br><br>In reply to: <a href="https://x.com/maciejwalkowiak/status/1598617552430800897" rel="noopener noreferrer" target="_blank">@maciejwalkowiak</a> <span class="status">1598617552430800897</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1598616907074052096',
    created: 1669974946000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maciejwalkowiak" rel="noopener noreferrer" target="_blank">@maciejwalkowiak</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> I\'ve spent the last ~6 months helping to get an Antora-based foundation set up for Spring projects. Right now, Spring Security serves as the archetype. See <a href="https://docs.spring.io/spring-security/reference/" rel="noopener noreferrer" target="_blank">docs.spring.io/spring-security/reference/</a> There\'s still a lot to do to prepare the content. What I\'m focused on is the build + extensions.<br><br>In reply to: <a href="https://x.com/maciejwalkowiak/status/1598607176649433088" rel="noopener noreferrer" target="_blank">@maciejwalkowiak</a> <span class="status">1598607176649433088</span>',
    likes: 12,
    retweets: 0
  },
  {
    id: '1598534521896734720',
    created: 1669955304000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/TCunnington_Lax" rel="noopener noreferrer" target="_blank">@TCunnington_Lax</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> Top 10.<br><br>In reply to: <a href="https://x.com/TCunnington_Lax/status/1598437597633826832" rel="noopener noreferrer" target="_blank">@TCunnington_Lax</a> <span class="status">1598437597633826832</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1598074213637951495',
    created: 1669845557000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ntorresdev" rel="noopener noreferrer" target="_blank">@ntorresdev</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> If you ask this question in <a href="https://chat.asciidoctor.org" rel="noopener noreferrer" target="_blank">chat.asciidoctor.org</a>, I\'d be happy to share the strategy I use to do exactly what you\'re doing.<br><br>In reply to: <a href="https://x.com/ntorresdev/status/1597972827881508865" rel="noopener noreferrer" target="_blank">@ntorresdev</a> <span class="status">1597972827881508865</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1597725220605165568',
    created: 1669762351000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/NLLwings" rel="noopener noreferrer" target="_blank">@NLLwings</a> Wait, Blaze is offense? You are blowing my mind. 🤯<br><br>In reply to: <a href="https://x.com/NLLwings/status/1597683588895768579" rel="noopener noreferrer" target="_blank">@NLLwings</a> <span class="status">1597683588895768579</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1597173877314289665',
    created: 1669630901000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/silopolis" rel="noopener noreferrer" target="_blank">@silopolis</a> @remoquete To be clear, the Eclipse Foundation isn\'t taking over. That\'s just the place where the AsciiDoc Language spec is being developed using the EFSP as the guide.<br><br>In reply to: <a href="https://x.com/silopolis/status/1596949174834139137" rel="noopener noreferrer" target="_blank">@silopolis</a> <span class="status">1596949174834139137</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1596034609958952960',
    created: 1669359278000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lukaseder" rel="noopener noreferrer" target="_blank">@lukaseder</a> They know exactly what they are suggesting. I\'m not going to keep letting people act like they were born yesterday.<br><br>In reply to: <a href="https://x.com/lukaseder/status/1596021373180481536" rel="noopener noreferrer" target="_blank">@lukaseder</a> <span class="status">1596021373180481536</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1595964798063501313',
    created: 1669342634000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> I was just dreaming of riding a pogostick. How freaky!<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/1594666942656397313" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">1594666942656397313</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1595950217160847362',
    created: 1669339157000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PaulRabil" rel="noopener noreferrer" target="_blank">@PaulRabil</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> I\'m thankful for having discovered <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> this year, learning about the Haudenosaunee, and for both reconnecting me with the Creator\'s game that I so enjoy. May the coming year bring our lacrosse family great fortune and healing.<br><br>In reply to: <a href="https://x.com/PaulRabil/status/1595799432271986689" rel="noopener noreferrer" target="_blank">@PaulRabil</a> <span class="status">1595799432271986689</span>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1595933880443236352',
    created: 1669335262000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lukaseder" rel="noopener noreferrer" target="_blank">@lukaseder</a> Yes, that\'s what I said. If it does, then I\'d do it...given I have the time...and on my own schedule.<br><br>In reply to: <a href="https://x.com/lukaseder/status/1595902430335873024" rel="noopener noreferrer" target="_blank">@lukaseder</a> <span class="status">1595902430335873024</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1595632743139794946',
    created: 1669263466000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ktosopl" rel="noopener noreferrer" target="_blank">@ktosopl</a> HTML5 still amazes me to this day. That tweet aged well.<br><br>In reply to: <a href="https://x.com/ktosopl/status/1595593017338957824" rel="noopener noreferrer" target="_blank">@ktosopl</a> <span class="status">1595593017338957824</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1595562359841587200',
    created: 1669246685000,
    type: 'post',
    text: 'I get this a lot. "We can\'t pay someone to do that. Can you do it for us (for free)?"<br><br>No.<br><br>(If I do it for free, it\'s because *I* want to do it or I believe it will benefit the greater community).',
    likes: 20,
    retweets: 3
  },
  {
    id: '1595174650840895488',
    created: 1669154248000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JoeCyberGuru" rel="noopener noreferrer" target="_blank">@JoeCyberGuru</a> <a class="mention" href="https://x.com/heckj" rel="noopener noreferrer" target="_blank">@heckj</a> I think issues are a different beast. Those are not often as well-formed and can be highly contextual. A PR likely took someone time to research, implement, and submit. If I\'m going to close a PR in one of my projects without merging, I\'m going to at least say something.<br><br>In reply to: <a href="https://x.com/JoeCyberGuru/status/1595102607533821952" rel="noopener noreferrer" target="_blank">@JoeCyberGuru</a> <span class="status">1595102607533821952</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1594964126060937216',
    created: 1669104055000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/heckj" rel="noopener noreferrer" target="_blank">@heckj</a> And to be clear, that\'s totally their right to do so. It\'s not implicitly right or wrong. But that tactic does have consequences. As the receiver, it\'s a clear signal that my time is better spent elsewhere.<br><br>In reply to: <a href="https://x.com/heckj/status/1594958262780280834" rel="noopener noreferrer" target="_blank">@heckj</a> <span class="status">1594958262780280834</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1594952874177757184',
    created: 1669101372000,
    type: 'post',
    text: 'If a human says "not interested", "not right now", "not correct", or just holds it open for when the time is right, I can understand that. Not every PR is going to fit and it means I need to come forward with something else or rethink the approach.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1594846052007686145',
    created: 1669075904000,
    type: 'post',
    text: 'When my PR is closed by a bot because it\'s "stale" only because the project maintainer did not merge it or request changes, I probably won\'t contribute to that project again. (It\'s not the delay that\'s the issue, it\'s the closing by a bot for reasons I can\'t control).',
    likes: 28,
    retweets: 4
  },
  {
    id: '1594486210482737152',
    created: 1668990111000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ysb33r" rel="noopener noreferrer" target="_blank">@ysb33r</a> I heard you may want to keep the account just to prevent anyone else from taking the username and impersonating you...if that\'s something you\'re concerned about.<br><br>That aside, see you in the fediverse! (I\'ll get set up there over the holidays).<br><br>In reply to: <a href="https://x.com/ysb33r/status/1594485011318599680" rel="noopener noreferrer" target="_blank">@ysb33r</a> <span class="status">1594485011318599680</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1593575986842378240',
    created: 1668773097000,
    type: 'post',
    text: 'This was my most popular tweet. I\'m very proud of that because, hopefully, it got someone to be valued more for what they do.<br><br>Quoting: <a href="#831609585937088512">831609585937088512</a>',
    likes: 6,
    retweets: 0
  },
  {
    id: '1593375091340910592',
    created: 1668725199000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/badmannersfood" rel="noopener noreferrer" target="_blank">@badmannersfood</a> Dates, bananas, cashew milk + butter, a pinch of cinnamon, and a splash of vanilla extract. Blitz. How is this so damn good?!?',
    likes: 1,
    retweets: 0
  },
  {
    id: '1593374244146057217',
    created: 1668724997000,
    type: 'post',
    text: 'When you want to contribute to open source, don\'t think about it in terms of whether or not you can code. Rather, try to think of it this way. Whatever your specialties are, they\'re most likely needed. It all applies.',
    likes: 4,
    retweets: 1
  },
  {
    id: '1593341896285265920',
    created: 1668717285000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> Awesome! Queued up and ready to play.<br><br>In reply to: <a href="https://x.com/danarestia/status/1593245615819231232" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1593245615819231232</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1593055748082520064',
    created: 1668649062000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> Basically, topic: sixes. Riff.<br><br>In reply to: <a href="#1592988163538317316">1592988163538317316</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1593055217829892096',
    created: 1668648936000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cdavies0475" rel="noopener noreferrer" target="_blank">@cdavies0475</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> I could see that as a great way to give the deep player pool a shot at the field. Interesting.<br><br>In reply to: <a href="https://x.com/cdavies0475/status/1593053863615266818" rel="noopener noreferrer" target="_blank">@cdavies0475</a> <span class="status">1593053863615266818</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1592988163538317316',
    created: 1668632949000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> I\'m very curious to hear your take (and players\' takes) on the exclusion of long poles in Sixes. Is that really field lacrosse? Are they entertaining the idea of allowing LSM at least? I just feel like a lot of players are going to feel left out (or out of their element).<br><br>In reply to: <a href="https://x.com/danarestia/status/1592872897303154688" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1592872897303154688</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1592987473671749634',
    created: 1668632784000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> The only revision I\'d make is to allow LSM (slightly extending the field size if necessary). Otherwise, you\'re really leaving out half the specialists on the field. And I don\'t think that\'s really fair for long pole players in general. But I know Sixes is still being worked on.<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1592875131897974785" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1592875131897974785</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1592957081422016512',
    created: 1668625538000,
    type: 'post',
    text: '...now on to those dozen+ projects. Let\'s go!',
    likes: 1,
    retweets: 0
  },
  {
    id: '1592956726802001920',
    created: 1668625454000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/karianna" rel="noopener noreferrer" target="_blank">@karianna</a> Tremendous work. I appreciate first hand how much passion and sweat goes into championing a commitment like this in a large company. I couldn\'t think of a better representative to sit in that spot. Best of luck! ☕️<br><br>In reply to: <a href="https://x.com/karianna/status/1592953558370258945" rel="noopener noreferrer" target="_blank">@karianna</a> <span class="status">1592953558370258945</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1592855147075768320',
    created: 1668601235000,
    type: 'post',
    text: 'I\'m working on over a dozen projects almost every single day.',
    likes: 5,
    retweets: 0
  },
  {
    id: '1592321623633580032',
    created: 1668474033000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RJKaminski" rel="noopener noreferrer" target="_blank">@RJKaminski</a> And the best part of on demand is that I can stretch it out for as long as I can bear the anticipation of starting the next game...and rewind and replay as often as I like.<br><br>In reply to: <a href="https://x.com/RJKaminski/status/1592240537645748224" rel="noopener noreferrer" target="_blank">@RJKaminski</a> <span class="status">1592240537645748224</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1591251605655281664',
    created: 1668218921000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> You\'re initial episode, and the premise of this podcast, is a feast. It\'s precisely the insight I had been looking &amp; hoping for throughout last season. So many questions and curiosities answered! A candid and impartial perspective is just what I\'m looking for. My 🥍\'s in!<br><br>In reply to: <a href="https://x.com/danarestia/status/1590740511924834310" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1590740511924834310</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1591157882325962753',
    created: 1668196576000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/shanecurcuru" rel="noopener noreferrer" target="_blank">@shanecurcuru</a> True. Though I\'ve never looked at code closer than normal and not learned at least something.<br><br>In reply to: <a href="https://x.com/shanecurcuru/status/1591080618779348992" rel="noopener noreferrer" target="_blank">@shanecurcuru</a> <span class="status">1591080618779348992</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1591024255848042498',
    created: 1668164717000,
    type: 'post',
    text: 'I just spent 24+ hours trying to debug a problem. Turns out, I had flipped true and false. If you get stuck, you are not alone. Happens to all of us. But it\'s never without value. I discovered other bugs and learned a ton too.',
    likes: 21,
    retweets: 2
  },
  {
    id: '1590495673220464642',
    created: 1668038693000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLRedwoods" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/rsgarnz50" rel="noopener noreferrer" target="_blank">@rsgarnz50</a> Saw it live! My jaw is still on the floor. That guy is a wizard!<br><br>In reply to: <a href="https://x.com/PLLRedwoods/status/1590450408392364032" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> <span class="status">1590450408392364032</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1590495395452706816',
    created: 1668038626000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLCannons" rel="noopener noreferrer" target="_blank">@PLLCannons</a> There\'s your answer, <a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a>. Signed him. My gut tells me this is a good fit for Lyle. I guess we\'ll see!<br><br>In reply to: <a href="https://x.com/PLLCannons/status/1590464719243988992" rel="noopener noreferrer" target="_blank">@PLLCannons</a> <span class="status">1590464719243988992</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1590495026349756418',
    created: 1668038538000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLNationals" rel="noopener noreferrer" target="_blank">@PLLNationals</a> <a class="mention" href="https://x.com/PLLCannons" rel="noopener noreferrer" target="_blank">@PLLCannons</a> I don\'t miss him, but I wish him luck. I think the Woods need a new strategy in that slot, and the Cannons need a partner for Lyle. This is good.<br><br>In reply to: <a href="https://x.com/PLLNationals/status/1590467082877276160" rel="noopener noreferrer" target="_blank">@PLLNationals</a> <span class="status">1590467082877276160</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1590494266656436224',
    created: 1668038357000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/shanselman" rel="noopener noreferrer" target="_blank">@shanselman</a> I really, really, really hope the US gov\'t comes to its senses and sets up a Mastodon instance for gov\'t officials. It\'s just wrong that this communication is done though a private sector company. GSA should procure and manage it, likely.<br><br>In reply to: <a href="https://x.com/shanselman/status/1590391269184147457" rel="noopener noreferrer" target="_blank">@shanselman</a> <span class="status">1590391269184147457</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1590492734565285888',
    created: 1668037992000,
    type: 'post',
    text: 'I love the fact that Coloradans voted to give all kids free school meals, regardless of income. That\'s a universal policy I wish we had in place in my childhood. To learn, you need to not be worrying about your next meal.',
    likes: 6,
    retweets: 0
  },
  {
    id: '1590491415515377665',
    created: 1668037678000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DeGette5280" rel="noopener noreferrer" target="_blank">@DeGette5280</a> <a class="mention" href="https://x.com/JenaGriswold" rel="noopener noreferrer" target="_blank">@JenaGriswold</a> 💯<br><br>In reply to: <a href="https://x.com/DeGette5280/status/1590417684013146112" rel="noopener noreferrer" target="_blank">@DeGette5280</a> <span class="status">1590417684013146112</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1590489431630548993',
    created: 1668037205000,
    type: 'post',
    text: 'I\'ve been playing Float On on loop today. Just seems to be giving me some solace.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1590486501900824576',
    created: 1668036506000,
    type: 'post',
    text: 'I just saw three different people roll though a red light, one on a school bus. People! We live in a society with rules. Show some damn restraint.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1590450755563327490',
    created: 1668027983000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BriceDutheil" rel="noopener noreferrer" target="_blank">@BriceDutheil</a> <a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <a class="mention" href="https://x.com/intellijidea" rel="noopener noreferrer" target="_blank">@intellijidea</a> Ooops, sorry I missed the joke. 🤦<br><br>In reply to: <a href="https://x.com/BriceDutheil/status/1590449998025875456" rel="noopener noreferrer" target="_blank">@BriceDutheil</a> <span class="status">1590449998025875456</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1590450626991120384',
    created: 1668027953000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <a class="mention" href="https://x.com/BriceDutheil" rel="noopener noreferrer" target="_blank">@BriceDutheil</a> <a class="mention" href="https://x.com/intellijidea" rel="noopener noreferrer" target="_blank">@intellijidea</a> Check out the talk about version control systems from Devoxx. Definitely worth a watch, even if you aren\'t interested in switching.<br><br>In reply to: <a href="https://x.com/gunnarmorling/status/1590447953478840321" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <span class="status">1590447953478840321</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1590447390573883392',
    created: 1668027181000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BriceDutheil" rel="noopener noreferrer" target="_blank">@BriceDutheil</a> <a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <a class="mention" href="https://x.com/intellijidea" rel="noopener noreferrer" target="_blank">@intellijidea</a> Yep, that would certainly work. Though I think the point here is that git should deprecate blame and replace it with a more constructive term. Pijul uses "credit", which I really like.<br><br>In reply to: <a href="https://x.com/BriceDutheil/status/1590446488454590465" rel="noopener noreferrer" target="_blank">@BriceDutheil</a> <span class="status">1590446488454590465</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1590444736539922432',
    created: 1668026548000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BriceDutheil" rel="noopener noreferrer" target="_blank">@BriceDutheil</a> <a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <a class="mention" href="https://x.com/intellijidea" rel="noopener noreferrer" target="_blank">@intellijidea</a> It\'s not an alias. annotate is a separate command (similar, but not the same).<br><br>In reply to: <a href="https://x.com/BriceDutheil/status/1590436655194079232" rel="noopener noreferrer" target="_blank">@BriceDutheil</a> <span class="status">1590436655194079232</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1590296276897501184',
    created: 1667991153000,
    type: 'post',
    text: 'I\'ve been too busy lately to make a big change, but it seems like Mastodon (both the company and the concept) more closely aligns with my values (progressive and open source). I\'ll likely give it a go in the near future.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1590175242311598080',
    created: 1667962296000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CoriBush" rel="noopener noreferrer" target="_blank">@CoriBush</a> Yes!<br><br>In reply to: <a href="https://x.com/CoriBush/status/1590175012295970819" rel="noopener noreferrer" target="_blank">@CoriBush</a> <span class="status">1590175012295970819</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1590126287712317440',
    created: 1667950624000,
    type: 'post',
    text: 'Just got an email that my ballot has been received and counted. Voting in Colorado rules!',
    likes: 4,
    retweets: 0
  },
  {
    id: '1590069500602638337',
    created: 1667937085000,
    type: 'post',
    text: 'It\'s truly sad that it has become "cool" to belittle and demean people. It\'s completely uncalled for. I feel like I\'m back in elementary school.',
    likes: 6,
    retweets: 0
  },
  {
    id: '1589887809720324096',
    created: 1667893767000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RJDickenson" rel="noopener noreferrer" target="_blank">@RJDickenson</a> Perhaps I should have said "activity" ;)<br><br>In reply to: <a href="https://x.com/RJDickenson/status/1589857319835566080" rel="noopener noreferrer" target="_blank">@RJDickenson</a> <span class="status">1589857319835566080</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1589849028581560321',
    created: 1667884521000,
    type: 'post',
    text: 'I voted.',
    likes: 5,
    retweets: 0
  },
  {
    id: '1589761698986364928',
    created: 1667863700000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> I finally had chance to listen last night. I\'d say you have a great podcast voice! I really enjoyed listening to your analysis of the Cannons/Woods trade. I agree it\'s too early to tell exactly what it means, but still so fun to get into the mind of a GM in this league.<br><br>In reply to: <a href="#1588660647369142272">1588660647369142272</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1589755722623242240',
    created: 1667862275000,
    type: 'post',
    text: 'Wow, engagement here has really dropped off.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1588732379308621824',
    created: 1667618291000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jackdoc39" rel="noopener noreferrer" target="_blank">@jackdoc39</a> <a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> Interesting. Kav was often playing on the inside, so that makes a lot of sense.<br><br>He\'s obviously absurdly good on either side....and that\'s what makes him such a threat for defenses and goalies.<br><br>In reply to: <a href="https://x.com/jackdoc39/status/1588707773893992448" rel="noopener noreferrer" target="_blank">@jackdoc39</a> <span class="status">1588707773893992448</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1588662241674416128',
    created: 1667601568000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> Btw, if you ever have guests on, I\'d love to hear you chat with <a class="mention" href="https://x.com/Ryan_Boyle14" rel="noopener noreferrer" target="_blank">@Ryan_Boyle14</a>.<br><br>In reply to: <a href="#1588660647369142272">1588660647369142272</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1588660647369142272',
    created: 1667601188000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> Super duper! I can\'t wait to listen. I love analysis. It\'s the same reason I enjoy the PLL broadcasts so much. They really get into the strategy of what\'s happening on and off the field. But there\'s always more to say (and more to learn).<br><br>In reply to: <a href="https://x.com/danarestia/status/1588660069637718018" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1588660069637718018</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1588660062704533504',
    created: 1667601049000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> The NLL is consistent about putting the dominant hand for players on their website. The PLL does not. I really wish that information was available as it would help to better understand the game strategy.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1588658423700549632',
    created: 1667600658000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> Quick question. Is Ryder right or left handed? I can\'t figure it out. If he\'s right handed, then who\'s going to play the left-handed attack position for the Woods without Kav?<br><br>In reply to: <a href="https://x.com/danarestia/status/1588361913603194880" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1588361913603194880</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1588657539805515776',
    created: 1667600447000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> Aha! I just discovered I can find the podcast in the AntennaPod app from the Google Play store. So I\'m good. Though I still recommend putting it on YouTube for reach. (Many of the DJs we listen to put their sets on YouTube for this reason).<br><br>In reply to: <a href="#1588656338519085057">1588656338519085057</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1588656338519085057',
    created: 1667600161000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> If not YouTube (which has really become a podcast platform if not by name), then Google Podcasts. (I\'m actually a YouTube Red subscriber, so, in theory, my fee should direct revenue to the channel\'s I play).<br><br>In reply to: <a href="https://x.com/danarestia/status/1588653832614989824" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1588653832614989824</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1588644288153673728',
    created: 1667597288000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> Is there any way you could put it on YouTube? For me (and perhaps many others?), Apple and Spotify are walled off.<br><br>In reply to: <a href="https://x.com/danarestia/status/1588497033605787648" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1588497033605787648</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1588639785383755776',
    created: 1667596214000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emacsen" rel="noopener noreferrer" target="_blank">@emacsen</a> <a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> Btw, after what seems like eons, Java now has a REPL (jshell). Nice to bring that lunch conversation full circle ;)<br><br>In reply to: <a href="#1588638794164862976">1588638794164862976</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1588638794164862976',
    created: 1667595978000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emacsen" rel="noopener noreferrer" target="_blank">@emacsen</a> <a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> Trust me, your challenges stuck with me for life. I always pause to check if I\'m in bubble. And often times, it turns out I am.<br><br>In reply to: <a href="https://x.com/emacsen/status/1588637233439178752" rel="noopener noreferrer" target="_blank">@emacsen</a> <span class="status">1588637233439178752</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1588636763513552897',
    created: 1667595494000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> The person who taught me to think this way is <a class="mention" href="https://x.com/emacsen" rel="noopener noreferrer" target="_blank">@emacsen</a>. I\'m not sure we ever agreed on which language was best, but we regularly agreed on what language features were important to have.<br><br>In reply to: <a href="#1588636431718969345">1588636431718969345</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1588636431718969345',
    created: 1667595415000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> Indeed. What I find more interesting is "what features of that language could this language benefit from?" The cross-pollination is what makes languages better...even great. It also provides more substance to the discussion because it gets into what makes a language good.<br><br>In reply to: <a href="https://x.com/gunnarmorling/status/1588143721341657089" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <span class="status">1588143721341657089</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1588608298877845505',
    created: 1667588708000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/andypiper" rel="noopener noreferrer" target="_blank">@andypiper</a> @TwitterDev Really sorry to hear that. Looking back, I\'m impressed by all you\'ve achieved there over nearly a decade. When I think of Twitter, yours is the first name that pops into my head. It won\'t be the same without you, that\'s for sure.<br><br>In reply to: @andypiper <span class="status">&lt;deleted&gt;</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1588604439153487872',
    created: 1667587787000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lukedary" rel="noopener noreferrer" target="_blank">@lukedary</a> <a class="mention" href="https://x.com/deno_land" rel="noopener noreferrer" target="_blank">@deno_land</a> Having an args parsing library in the stdlib is essential. Glad to see this trend.<br><br>In reply to: <a href="https://x.com/lukedary/status/1588557953506082816" rel="noopener noreferrer" target="_blank">@lukedary</a> <span class="status">1588557953506082816</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1588486801605304320',
    created: 1667559740000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fwilhe" rel="noopener noreferrer" target="_blank">@fwilhe</a> Yep! <a href="https://github.com/mojavelinux/downdoc/blob/main/lib/cli.js" rel="noopener noreferrer" target="_blank">github.com/mojavelinux/downdoc/blob/main/lib/cli.js</a> (but I also don\'t use TypeScript).<br><br>In reply to: <a href="https://x.com/fwilhe/status/1588485478596345856" rel="noopener noreferrer" target="_blank">@fwilhe</a> <span class="status">1588485478596345856</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1588484937774428160',
    created: 1667559296000,
    type: 'post',
    text: 'I should say parsing function since that\'s all it is.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1588483745312825344',
    created: 1667559012000,
    type: 'post',
    text: 'It\'s pretty cool that Node.js now has a lightweight args parsing library for quick scripts and zero-dependency packages.<br><br>require(\'node:util\').parseArgs',
    likes: 4,
    retweets: 1
  },
  {
    id: '1588372422784602113',
    created: 1667532470000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> I\'m now very interested to see how Piatelli works in into the offense with Pannell. This could be something. I\'m no fan of Kavanagh.<br><br>In reply to: <a href="https://x.com/danarestia/status/1588361913603194880" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1588361913603194880</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1587558132372340737',
    created: 1667338328000,
    type: 'post',
    text: 'Boosted! 💉',
    likes: 4,
    retweets: 0
  },
  {
    id: '1587553881109762049',
    created: 1667337315000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/relequestual" rel="noopener noreferrer" target="_blank">@relequestual</a> <a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> Indeed. Please pass it on. We\'ll be making good use of it improve tooling.<br><br>In reply to: <a href="https://x.com/relequestual/status/1587551058527944704" rel="noopener noreferrer" target="_blank">@relequestual</a> <span class="status">1587551058527944704</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1587532049878949890',
    created: 1667332110000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/relequestual" rel="noopener noreferrer" target="_blank">@relequestual</a> <a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> Btw, JSON Schema is stellar. Nice work!<br><br>In reply to: <a href="#1587527639123623936">1587527639123623936</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1587527639123623936',
    created: 1667331058000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/relequestual" rel="noopener noreferrer" target="_blank">@relequestual</a> <a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> In this particular case, we have a really good idea of what the unique key is and its purpose, so I\'m feeling confident the object/map is the right choice. And the patternProperties of JSON schema makes it possible to enforce the value type assigned to each key.<br><br>In reply to: <a href="#1587526965501624321">1587526965501624321</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1587526965501624321',
    created: 1667330898000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/relequestual" rel="noopener noreferrer" target="_blank">@relequestual</a> <a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> Salient points.<br><br>In reply to: <a href="https://x.com/relequestual/status/1587439477269549056" rel="noopener noreferrer" target="_blank">@relequestual</a> <span class="status">1587439477269549056</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1587526711876366336',
    created: 1667330837000,
    type: 'post',
    text: 'Busy, busy days.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1587351821659426816',
    created: 1667289140000,
    type: 'post',
    text: 'I think what I love most about tests...okay, *one* of the things I love about tests...is that they give me the confidence to assertively delete code knowing I don\'t need it.',
    likes: 4,
    retweets: 1
  },
  {
    id: '1587014369124048896',
    created: 1667208685000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jakekorab" rel="noopener noreferrer" target="_blank">@jakekorab</a> But regardless of which way I\'m leaning, this is certainly an agonizing choice. That\'s why I reached out. And it seems from the feedback I\'ve received that we all wrestle with this one. Just one of those no perfect solution kind of situations.<br><br>In reply to: <a href="#1587012939080351745">1587012939080351745</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1587012939080351745',
    created: 1667208344000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jakekorab" rel="noopener noreferrer" target="_blank">@jakekorab</a> I\'m definitely considering consumption and browsing. It\'s for that reason I\'m leaning towards the map. It really is what the data is trying to express. I think I\'m going to try it, but fallback to an array if it clearly isn\'t working out.<br><br>In reply to: <a href="https://x.com/jakekorab/status/1587011527177113602" rel="noopener noreferrer" target="_blank">@jakekorab</a> <span class="status">1587011527177113602</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1587011722954489856',
    created: 1667208054000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jakekorab" rel="noopener noreferrer" target="_blank">@jakekorab</a> I get what your saying. But jq for example supports it with no trouble using to_entries. I think if a tool falls down on this basic requirement, it\'s not a good tool.<br><br>In reply to: <a href="https://x.com/jakekorab/status/1587011099882360832" rel="noopener noreferrer" target="_blank">@jakekorab</a> <span class="status">1587011099882360832</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1586918238029377536',
    created: 1667185766000,
    type: 'quote',
    text: 'Finally some better news. I\'m drinking to that tonight.<br><br>Quoting: <a href="https://x.com/KateAronoff/status/1586856856034631681" rel="noopener noreferrer" target="_blank">@KateAronoff</a> <span class="status">1586856856034631681</span>',
    likes: 5,
    retweets: 0
  },
  {
    id: '1586882104599552000',
    created: 1667177151000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> Thanks. That\'s what I keep coming back to. I think it\'s worth giving it a shot. If consumers can process it, then we get the added benefit of the uniqueness (which JSON schema cannot enforce).<br><br>In reply to: <a href="https://x.com/odrotbohm/status/1586327593975947270" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <span class="status">1586327593975947270</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1586881782569349120',
    created: 1667177074000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/debugagent" rel="noopener noreferrer" target="_blank">@debugagent</a> Thanks for the feedback. I\'m leaning more map since I think this data is going to be traversed using object notation / lookup frequently.<br><br>In reply to: <a href="https://x.com/debugagent/status/1586337746444066816" rel="noopener noreferrer" target="_blank">@debugagent</a> <span class="status">1586337746444066816</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1586881464926208000',
    created: 1667176998000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jakekorab" rel="noopener noreferrer" target="_blank">@jakekorab</a> Thanks for the feedback. I\'m not buying that it impacts deserializers. They should be able to deal with a map since maps are everywhere in JSON. The question is more about whether it\'s communicating the right thing.<br><br>In reply to: <a href="https://x.com/jakekorab/status/1586352350356439040" rel="noopener noreferrer" target="_blank">@jakekorab</a> <span class="status">1586352350356439040</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1586880863626592256',
    created: 1667176855000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fanf42" rel="noopener noreferrer" target="_blank">@fanf42</a> I keep gravitating towards the map because it communicates the uniqueness of the key property (and something JSON schema cannot enforce). I\'ve been able to prove that it\'s feasible to process either structure efficiently, so I\'m less concerned about that aspect.<br><br>In reply to: <a href="https://x.com/fanf42/status/1586448496973987840" rel="noopener noreferrer" target="_blank">@fanf42</a> <span class="status">1586448496973987840</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1586880301061459969',
    created: 1667176721000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JeffJLeeDev" rel="noopener noreferrer" target="_blank">@JeffJLeeDev</a> That\'s a very excellent point. I actually like that the field name for the ID becomes a non-issue, so in my case that makes a strong case for the map.<br><br>In reply to: <a href="https://x.com/JeffJLeeDev/status/1586554443180425216" rel="noopener noreferrer" target="_blank">@JeffJLeeDev</a> <span class="status">1586554443180425216</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1586878797600632833',
    created: 1667176362000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/evanchooly" rel="noopener noreferrer" target="_blank">@evanchooly</a> <a class="mention" href="https://x.com/domdorn" rel="noopener noreferrer" target="_blank">@domdorn</a> That was my concern initially, but now I don\'t buy it. A JSON mapper can\'t map an object onto a Map in Java? An object is a fundamental data structure in JSON. Not being able to support that would render the mapper utterly useless.<br><br>In reply to: <a href="https://x.com/evanchooly/status/1586874586448158724" rel="noopener noreferrer" target="_blank">@evanchooly</a> <span class="status">1586874586448158724</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1586323677540278272',
    created: 1667044011000,
    type: 'post',
    text: 'When items have a unique identifier (such as username for a user or a sequence number for a versioned record), is there a strong case for using an array vs object/map for the items in JSON data?<br><br>"users": [{ "username": "user123", ... }]<br>vs<br>"users": { "user123": { ... } }',
    likes: 4,
    retweets: 0
  },
  {
    id: '1586280699325095936',
    created: 1667033765000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/simonbrown" rel="noopener noreferrer" target="_blank">@simonbrown</a> And speaking of conversation, my partner &amp; I have agreed to incorporate diagrams into our architecture documents (instead of just textual explanations).<br><br>In reply to: <a href="#1586280438510731265">1586280438510731265</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1586280438510731265',
    created: 1667033702000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/simonbrown" rel="noopener noreferrer" target="_blank">@simonbrown</a> There are two points that really resonated with me:<br><br>* There\'s only value in the conversation if you\'re having the right conversation 💡<br>* Know when to stop designing (when rate of return starts to trail off)<br><br>In reply to: <a href="https://x.com/simonbrown/status/1586278628287041542" rel="noopener noreferrer" target="_blank">@simonbrown</a> <span class="status">1586278628287041542</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1586190126333382657',
    created: 1667012170000,
    type: 'post',
    text: 'If you\'re voting in Colorado, be sure to check out this video with some facts and impacts about propositions 124, 125, &amp; 126 <a href="https://youtu.be/cgrvqqxxFmo" rel="noopener noreferrer" target="_blank">youtu.be/cgrvqqxxFmo</a> from an owner of a local liquor store.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1586181331154718721',
    created: 1667010073000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> Get well soon, my friend! I had exactly the same experience. I couldn\'t remember how I ever had the energy to stand up. The good news is that my energy did eventually return...perhaps even more than before. Rest up to fight another day!<br><br>In reply to: <a href="https://x.com/aalmiray/status/1585987142593175553" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">1585987142593175553</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1586177916064276480',
    created: 1667009259000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> For sure. But space is also the upsidedown at the same time (it\'s just so weird) ;)<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1586158898595041280',
    created: 1667004725000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Is that the upsidedown?<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1586157425685516290',
    created: 1667004374000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/simonbrown" rel="noopener noreferrer" target="_blank">@simonbrown</a> I really enjoyed your Devoxx talk. It\'s consistent with what I\'ve observed over the last several years. It also made me reflect on my own design process for the projects I work on. I agree that development would be easier with better architecture &amp; diagrams.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1585922185813626880',
    created: 1666948288000,
    type: 'post',
    text: 'I just used a PlantUML diagram in the comment of an issue on GitLab for the first time. Wow, where has this been my whole developer life?!?<br><br>disclaimer: GitLab has supported it for a long time. I just never thought to take advantage of it until now.',
    likes: 19,
    retweets: 3
  },
  {
    id: '1584792227624022016',
    created: 1666678885000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DewanAhmed" rel="noopener noreferrer" target="_blank">@DewanAhmed</a> <a class="mention" href="https://x.com/will_sargent" rel="noopener noreferrer" target="_blank">@will_sargent</a> Highlighting of source blocks is a core feature of AsciiDoc, though the highlighting task itself is performed by third-party syntax highlighting libraries such as Rouge, Pygments, and highlight.js. See <a href="https://docs.asciidoctor.org/asciidoc/latest/verbatim/source-highlighter/" rel="noopener noreferrer" target="_blank">docs.asciidoctor.org/asciidoc/latest/verbatim/source-highlighter/</a><br><br>In reply to: <a href="https://x.com/DewanAhmed/status/1584575619232563202" rel="noopener noreferrer" target="_blank">@DewanAhmed</a> <span class="status">1584575619232563202</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1584644681140383746',
    created: 1666643707000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <a class="mention" href="https://x.com/RedHat" rel="noopener noreferrer" target="_blank">@RedHat</a> You just won the leaving Red Hat creativity contest!<br><br>It was a joy getting to know you in the brief time we overlapped and ever since. Congrats on a successful run and good luck in your future endeavors! Signed, forever a colleague in open source.<br><br>In reply to: <a href="https://x.com/gunnarmorling/status/1584575456875057155" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <span class="status">1584575456875057155</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1583918047806431233',
    created: 1666470465000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nottycode" rel="noopener noreferrer" target="_blank">@nottycode</a> 🤣<br><br>In reply to: <a href="https://x.com/nottycode/status/1583917523116130305" rel="noopener noreferrer" target="_blank">@nottycode</a> <span class="status">1583917523116130305</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1583910149051232257',
    created: 1666468581000,
    type: 'post',
    text: 'I need to cancel my Sirius/XM account because my radio stopped working &amp; I\'m currently using alternate services. Instead of providing a Cancel button, you have to chat with an agent, who then questions your decision. I don\'t appreciate it. I made a decision. Respect it.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1583762932113887233',
    created: 1666433482000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DewanAhmed" rel="noopener noreferrer" target="_blank">@DewanAhmed</a> <a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> If you want to hear from documentation teams at the forefront of AsciiDoc adoption, I encourage you to contact the AsciiDoc WG @ <a href="https://asciidoc-wg.eclipse.org/" rel="noopener noreferrer" target="_blank">asciidoc-wg.eclipse.org/</a>. You can also join the community chat to reach members 1-on-1 @ <a href="https://chat.asciidoc.org" rel="noopener noreferrer" target="_blank">chat.asciidoc.org</a>. There\'s loads of experience there.<br><br>In reply to: <a href="https://x.com/DewanAhmed/status/1583465169006448640" rel="noopener noreferrer" target="_blank">@DewanAhmed</a> <span class="status">1583465169006448640</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1583720678267662337',
    created: 1666423408000,
    type: 'post',
    text: 'I also discovered that GitLab will render the index file if no README file is present in a given folder.',
    photos: ['<div class="item"><img class="photo" src="media/1583720678267662337.png"></div>'],
    likes: 2,
    retweets: 0
  },
  {
    id: '1583199050014150656',
    created: 1666299042000,
    type: 'post',
    text: 'Whoa! I just discovered that GitLab has integrated test coverage in the diff view of a merge request.',
    photos: ['<div class="item"><img class="photo" src="media/1583199050014150656.jpg"></div>'],
    likes: 16,
    retweets: 3
  },
  {
    id: '1583171301971087360',
    created: 1666292426000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rob_winch" rel="noopener noreferrer" target="_blank">@rob_winch</a> Wow, that\'s fancy!<br><br>In reply to: <a href="https://x.com/rob_winch/status/1583171010333134848" rel="noopener noreferrer" target="_blank">@rob_winch</a> <span class="status">1583171010333134848</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1583171027718111232',
    created: 1666292361000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rbowen" rel="noopener noreferrer" target="_blank">@rbowen</a> It finds the last 15 branches that have been modified (by checking the date of the most recent commit). With this list, I can quickly find and switch to branches that I\'m actively working on.<br><br>In reply to: <a href="https://x.com/rbowen/status/1583167769142788096" rel="noopener noreferrer" target="_blank">@rbowen</a> <span class="status">1583167769142788096</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1583167528108376064',
    created: 1666291527000,
    type: 'post',
    text: 'There\'s no way I could function without this git alias:<br><br>recent-branches = for-each-ref --sort=-committerdate --count=15 --format=\'%(refname:short)\' refs/heads/',
    likes: 10,
    retweets: 1
  },
  {
    id: '1583049038966185985',
    created: 1666263277000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/chacon" rel="noopener noreferrer" target="_blank">@chacon</a> <a class="mention" href="https://x.com/mg" rel="noopener noreferrer" target="_blank">@mg</a> The first one gives me a very Chris Cornell vibe. I like it.<br><br>In reply to: <a href="https://x.com/chacon/status/1583044684573986825" rel="noopener noreferrer" target="_blank">@chacon</a> <span class="status">1583044684573986825</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1582676131735425024',
    created: 1666174369000,
    type: 'post',
    text: 'I laughed for about an hour with my dental hygienist today. It was healing.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1582640797131759617',
    created: 1666165944000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/TamasGorbe" rel="noopener noreferrer" target="_blank">@TamasGorbe</a> <a class="mention" href="https://x.com/PeterLawrey" rel="noopener noreferrer" target="_blank">@PeterLawrey</a> For fun, I translated this to LaTeX (for use with MathJax)<br><br>\\lim_{n \\to \\infty}\\frac{n}{\\sqrt[n]{n!}} = {\\large e}<br><br>In reply to: <a href="https://x.com/TamasGorbe/status/1582071299417198594" rel="noopener noreferrer" target="_blank">@TamasGorbe</a> <span class="status">1582071299417198594</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1582622851902803968',
    created: 1666161666000,
    type: 'post',
    text: '"We turned the hallway heaters on today, so please don\'t be alarmed if you smell something similar to burning."<br><br>Unless, of course, it is burning. Then, be alarmed.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1582622294249127936',
    created: 1666161533000,
    type: 'post',
    text: 'In Colorado, not only do you receive your ballot in the mail automatically, you also get an email in advance telling you that the ballot is on the way. So there\'s no doubt as to whether you\'re registered to vote. Stay calm and vote when it arrives!',
    likes: 1,
    retweets: 0
  },
  {
    id: '1582322380268924928',
    created: 1666090028000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/USAWLax" rel="noopener noreferrer" target="_blank">@USAWLax</a> <a class="mention" href="https://x.com/LacrosseNetwork" rel="noopener noreferrer" target="_blank">@LacrosseNetwork</a> I\'m thrilled that this happened, but why was it not broadcast on any network? That\'s not growing the game, especially out here in the West.<br><br>In reply to: <a href="https://x.com/USAWLax/status/1581325752183980033" rel="noopener noreferrer" target="_blank">@USAWLax</a> <span class="status">1581325752183980033</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1581815014234935296',
    created: 1665969062000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> 👋<br><br>In reply to: <a href="https://x.com/kito99/status/1581800132785283074" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">1581800132785283074</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1581382113546207232',
    created: 1665865851000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/debugagent" rel="noopener noreferrer" target="_blank">@debugagent</a> But of course!<br><br>In reply to: <a href="https://x.com/debugagent/status/1581251996124082177" rel="noopener noreferrer" target="_blank">@debugagent</a> <span class="status">1581251996124082177</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1581226456679473154',
    created: 1665828739000,
    type: 'post',
    text: 'We always want to catch up on talks after an event, but never do. I found a solid hack for watching #Devoxx on YouTube. Grab a Belgian brew (or Belgian delicacy of your choice), pull up a comfy chair, and let yourself be transported back there. Don\'t just watch the talk. Live it!',
    likes: 10,
    retweets: 1,
    tags: ['devoxx']
  },
  {
    id: '1581220686743277568',
    created: 1665827364000,
    type: 'post',
    text: 'I may be one of few people who took a whole course on imaginary numbers. All I can remember from it is that it a) bent my brain and b) made no sense to anyone I talked to about it. And somehow, that describes my life.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1581220168725733376',
    created: 1665827240000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/dylanbeattie" rel="noopener noreferrer" target="_blank">@dylanbeattie</a> Just watched your Devoxx talk (at home). That was insane. You touched on so many things I\'ve brushed past in my life (Logo, maths, imaginary number theory, 80s music, sonic pi, language specs, presentation design) and yet it was like nothing I\'ve ever seen. Bravo!',
    likes: 3,
    retweets: 0
  },
  {
    id: '1580869618830168064',
    created: 1665743662000,
    type: 'post',
    text: 'The #Devoxx tweets on Friday were more frequent than on any prior day (or so it seems). I like the end of week energy!',
    likes: 2,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '1580643393893343233',
    created: 1665689726000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/venkat_s" rel="noopener noreferrer" target="_blank">@venkat_s</a> That would be wonderful! 🏔️<br><br>In reply to: <a href="https://x.com/venkat_s/status/1580513986612629505" rel="noopener noreferrer" target="_blank">@venkat_s</a> <span class="status">1580513986612629505</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1580503969868132352',
    created: 1665656485000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <a class="mention" href="https://x.com/venkat_s" rel="noopener noreferrer" target="_blank">@venkat_s</a> <a class="mention" href="https://x.com/mariofusco" rel="noopener noreferrer" target="_blank">@mariofusco</a> 🤣<br><br>In reply to: <a href="https://x.com/odrotbohm/status/1580502130762211329" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <span class="status">1580502130762211329</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1580498225022136320',
    created: 1665655115000,
    type: 'post',
    text: 'Doing what I can do to recreate the #Devoxx experience from home in Colorado. I can report that a <a class="mention" href="https://x.com/venkat_s" rel="noopener noreferrer" target="_blank">@venkat_s</a> export pairs well with a Chimay import. 🍻',
    photos: ['<div class="item"><img class="photo" src="media/1580498225022136320.jpg"></div>'],
    likes: 33,
    retweets: 2,
    tags: ['devoxx']
  },
  {
    id: '1580490038923567104',
    created: 1665653164000,
    type: 'reply',
    text: '@pvidasoftware Not really. It\'s very much ongoing. I\'m just being asked to do much. But I take responsibility for that for trying to take on too much. So I\'m working hard to scale back to something sustainable while still honoring my core commitments. (I\'m also trying to automate like crazy).<br><br>In reply to: @pvidasoftware <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1580407527493144576',
    created: 1665633491000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/antoine_sd" rel="noopener noreferrer" target="_blank">@antoine_sd</a> <a class="mention" href="https://x.com/sebi2706" rel="noopener noreferrer" target="_blank">@sebi2706</a> Indeed. Sarah says hi! She wants to know where your fancy glasses are. She said she almost didn\'t recognize you without them ;) 🤓<br><br>In reply to: <a href="https://x.com/antoine_sd/status/1580081805046448130" rel="noopener noreferrer" target="_blank">@antoine_sd</a> <span class="status">1580081805046448130</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1580404670115110913',
    created: 1665632810000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Devoxx" rel="noopener noreferrer" target="_blank">@Devoxx</a> 💯 That\'s why it\'s good to savor it. Talk fast, drink slow. #devoxx<br><br>In reply to: <a href="https://x.com/Devoxx/status/1580289706117300224" rel="noopener noreferrer" target="_blank">@Devoxx</a> <span class="status">1580289706117300224</span>',
    likes: 1,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '1580300310391635968',
    created: 1665607929000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mthmulders" rel="noopener noreferrer" target="_blank">@mthmulders</a> 💯<br><br>In reply to: <a href="https://x.com/mthmulders/status/1580218355801534464" rel="noopener noreferrer" target="_blank">@mthmulders</a> <span class="status">1580218355801534464</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1580021715769315328',
    created: 1665541507000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bjschrijver" rel="noopener noreferrer" target="_blank">@bjschrijver</a> Promise to drink one for me. Enjoy!<br><br>In reply to: <a href="https://x.com/bjschrijver/status/1579915809971138561" rel="noopener noreferrer" target="_blank">@bjschrijver</a> <span class="status">1579915809971138561</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1580021032974315520',
    created: 1665541344000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/antoine_sd" rel="noopener noreferrer" target="_blank">@antoine_sd</a> <a class="mention" href="https://x.com/sebi2706" rel="noopener noreferrer" target="_blank">@sebi2706</a> Hey you two! 👋<br><br>In reply to: <a href="https://x.com/antoine_sd/status/1579902766973009922" rel="noopener noreferrer" target="_blank">@antoine_sd</a> <span class="status">1579902766973009922</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1579949845863075840',
    created: 1665524371000,
    type: 'post',
    text: 'OH: "It\'s becoming blind-o\'clock hour. Time to put on the glasses. 👓"',
    likes: 2,
    retweets: 1
  },
  {
    id: '1579770558790897664',
    created: 1665481626000,
    type: 'post',
    text: 'Thanks to all your tweets and pictures, I can almost feel like I\'m at #Devoxx. Almost...I just can\'t taste the 🍺.',
    likes: 3,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '1579642219975573505',
    created: 1665451028000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fanf42" rel="noopener noreferrer" target="_blank">@fanf42</a> I think the hardest part is that I do the work of 10 people (literally), and yet I\'m constantly told that my effort is not enough.<br><br>In reply to: <a href="https://x.com/fanf42/status/1579592312014790657" rel="noopener noreferrer" target="_blank">@fanf42</a> <span class="status">1579592312014790657</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1579575923887984641',
    created: 1665435222000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fanf42" rel="noopener noreferrer" target="_blank">@fanf42</a> It helps, trust me. I just need to remind myself to be patient, take it one thing at a time, and realize I can only do what I can reasonably do. (It would really help if the world wasn\'t in totally disarray, but there\'s only so much we can control).<br><br>In reply to: <a href="https://x.com/fanf42/status/1579560751056158720" rel="noopener noreferrer" target="_blank">@fanf42</a> <span class="status">1579560751056158720</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1579436042352222209',
    created: 1665401871000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> One of my favorite views in the world.<br><br>In reply to: <a href="https://x.com/brunoborges/status/1579158216889356288" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1579158216889356288</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1579435653863190529',
    created: 1665401779000,
    type: 'post',
    text: 'I\'ve never accomplished so much and felt so behind.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1579359611827949574',
    created: 1665383649000,
    type: 'quote',
    text: 'To all my friends attending Devoxx Belgium, may you have a splendid time! 🍻 I wish I was there with you. Remember to always invite one more into your circle while you\'re there. That\'s how we grow as a community and make Devoxx a special place for a whole new generation.<br><br>Quoting: <a href="https://x.com/Devoxx/status/1579346233856233473" rel="noopener noreferrer" target="_blank">@Devoxx</a> <span class="status">1579346233856233473</span>',
    likes: 10,
    retweets: 2
  },
  {
    id: '1579235608702046210',
    created: 1665354084000,
    type: 'post',
    text: 'PSA: In my experience, the latest version of Bundler doesn\'t work on Windows. Lock the version to 2.3.22 if you are seeing problems.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1577459963718291456',
    created: 1664930737000,
    type: 'post',
    text: 'Someone in my stream has assigned themselves a version number...and now I\'m thinking what my version number is. 🤔🔢',
    likes: 1,
    retweets: 0
  },
  {
    id: '1577424263807574016',
    created: 1664922226000,
    type: 'quote',
    text: '👇<br><br>Quoting: <a href="https://x.com/garrett/status/1577421751415459840" rel="noopener noreferrer" target="_blank">@garrett</a> <span class="status">1577421751415459840</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1577226020188393474',
    created: 1664874961000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DevoxxMA" rel="noopener noreferrer" target="_blank">@DevoxxMA</a> Looks awesome (and sunny)! Wishing you a very fun, engaging, and successful event! ☕️🇲🇦<br><br>In reply to: <a href="https://x.com/DevoxxMA/status/1577222270912966656" rel="noopener noreferrer" target="_blank">@DevoxxMA</a> <span class="status">1577222270912966656</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1577059208008126466',
    created: 1664835190000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/talios" rel="noopener noreferrer" target="_blank">@talios</a> <a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> 🤣<br><br>In reply to: <a href="https://x.com/talios/status/1577057876022943745" rel="noopener noreferrer" target="_blank">@talios</a> <span class="status">1577057876022943745</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1577056480762568705',
    created: 1664834539000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/talios" rel="noopener noreferrer" target="_blank">@talios</a> <a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> Yes, but my point is that it won\'t destructor the k, v for me. I have to use e.getKey() and e.getValue() which is unlike every other language I know of that maps over a map entry set.<br><br>In reply to: <a href="https://x.com/talios/status/1577056094949474304" rel="noopener noreferrer" target="_blank">@talios</a> <span class="status">1577056094949474304</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1577056326919680000',
    created: 1664834503000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> <a class="mention" href="https://x.com/talios" rel="noopener noreferrer" target="_blank">@talios</a> ...oops, forgot myMap.entrySet().stream().map...<br><br>In reply to: <a href="#1577055723564937216">1577055723564937216</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1577055723564937216',
    created: 1664834359000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> <a class="mention" href="https://x.com/talios" rel="noopener noreferrer" target="_blank">@talios</a> Exactly. I was looking for myMap.map((k, v) -&gt; k + ": " + v).collect(Collectors.joining("\\n")) -ish.<br><br>In reply to: <a href="https://x.com/aalmiray/status/1577055243401015296" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">1577055243401015296</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1577024574247485440',
    created: 1664826932000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pvaneeckhoudt" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> Thanks! That\'s very useful to know. At least I know not to look for something that isn\'t there. 🍻<br><br>In reply to: <a href="https://x.com/pvaneeckhoudt/status/1577022433802215424" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> <span class="status">1577022433802215424</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1577014589656428544',
    created: 1664824552000,
    type: 'post',
    text: 'I seem to be incapable of figuring out how to map over a Map\'s stream in Java &gt;= 8 and get the key and value without restoring to using e.getKey() and e.getValue(). Is there a way to destructure the entry automatically. Any examples? (I can do it in Groovy, just not Java).',
    likes: 1,
    retweets: 0
  },
  {
    id: '1575933161636184070',
    created: 1664566719000,
    type: 'post',
    text: 'They seem to make a special exception for whatever branch is configured as the GitHub Pages branch, because I don\'t see the message in that case.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1575929215689465857',
    created: 1664565779000,
    type: 'post',
    text: 'I wish GitHub understood the concept of an orphan branch instead of reporting:<br><br>"This branch is a few commits ahead, a gazillion commits behind main."<br><br>No, it is not. It\'s an orphan branch. It\'s just different.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1575768356187123712',
    created: 1664527427000,
    type: 'post',
    text: 'If you publish a gem with a version like 1.0.0-alpha.1, gem install will say it can\'t be found (because it looks for 1.0.0.pre.alpha.1 instead). So broken.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1575764592675811328',
    created: 1664526529000,
    type: 'post',
    text: 'I\'m so annoyed that RubyGems rewrites 1.0.0-alpha.1 (a valid semantic version) to 1.0.0.pre.alpha.1. Can anyone explain why this is acceptable?',
    likes: 1,
    retweets: 0
  },
  {
    id: '1575554517340258304',
    created: 1664476443000,
    type: 'reply',
    text: '@GeePawHill Perhaps a collapsible block is what you\'re looking for? <a href="https://docs.asciidoctor.org/asciidoc/latest/blocks/collapsible/" rel="noopener noreferrer" target="_blank">docs.asciidoctor.org/asciidoc/latest/blocks/collapsible/</a><br><br>In reply to: @GeePawHill <span class="status">&lt;deleted&gt;</span>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1575318873045291008',
    created: 1664420262000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> In PDF, the lines automatically wrap if they don\'t fit. So I\'m still trying to understand what you want to be different. Btw, this would be a good question for the project chat.<br><br>In reply to: <a href="https://x.com/alexsotob/status/1575309305104007170" rel="noopener noreferrer" target="_blank">@alexsotob</a> <span class="status">1575309305104007170</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1575250760102617089',
    created: 1664404022000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> Do you mean you want the rendered block to hard wrap instead of scroll horizontally? Or do you actually want to break lines at a fixed column?<br><br>In reply to: <a href="https://x.com/alexsotob/status/1575211611878727680" rel="noopener noreferrer" target="_blank">@alexsotob</a> <span class="status">1575211611878727680</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1574523555349295104',
    created: 1664230643000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RJKaminski" rel="noopener noreferrer" target="_blank">@RJKaminski</a> OMG, I love L\'imperatrice!<br><br>In reply to: <a href="https://x.com/RJKaminski/status/1574442318341410819" rel="noopener noreferrer" target="_blank">@RJKaminski</a> <span class="status">1574442318341410819</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1574130606484529153',
    created: 1664136957000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/j0s3julianmart1" rel="noopener noreferrer" target="_blank">@j0s3julianmart1</a> 👍<br><br>In reply to: <a href="https://x.com/j0s3julianmart1/status/1574035327559389185" rel="noopener noreferrer" target="_blank">@j0s3julianmart1</a> <span class="status">1574035327559389185</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1573985550855335936',
    created: 1664102373000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/j0s3julianmart1" rel="noopener noreferrer" target="_blank">@j0s3julianmart1</a> I\'m referring specifically to democratic socialism. That should be clear if you\'ve followed my stream for any length of time. Capitalism is the cancer that\'s destroying this planet.<br><br>In reply to: <a href="https://x.com/j0s3julianmart1/status/1573950528291233793" rel="noopener noreferrer" target="_blank">@j0s3julianmart1</a> <span class="status">1573950528291233793</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1573831448758403072',
    created: 1664065632000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/TomHombergs" rel="noopener noreferrer" target="_blank">@TomHombergs</a> Stay tuned...research is needed.<br><br>In reply to: <a href="#1573829123872415744">1573829123872415744</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1573831122114338816',
    created: 1664065554000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/michaelsayman" rel="noopener noreferrer" target="_blank">@michaelsayman</a> 💯<br><br>In reply to: <a href="https://x.com/michaelsayman/status/1573829864259461163" rel="noopener noreferrer" target="_blank">@michaelsayman</a> <span class="status">1573829864259461163</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1573829123872415744',
    created: 1664065078000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/TomHombergs" rel="noopener noreferrer" target="_blank">@TomHombergs</a> Beer in hand. But I need to go confirm that 😋<br><br>In reply to: <a href="https://x.com/TomHombergs/status/1573828948554817536" rel="noopener noreferrer" target="_blank">@TomHombergs</a> <span class="status">1573828948554817536</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1573829006931001346',
    created: 1664065050000,
    type: 'post',
    text: 'Trust me, I just want to live and enjoy things. But it can\'t be at the expense of future generations, especially those who aren\'t privileged. We need to imagine a new world that\'s based on humanity.',
    likes: 6,
    retweets: 0
  },
  {
    id: '1573828242871504897',
    created: 1664064868000,
    type: 'post',
    text: 'In brief, we must end extraction yesterday and stop treating the global south as expendable. Get used to a new way of life. Mass transit, vegan, socialism. Stop thinking it can be another way. But that way can be pretty great.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1573827633090940928',
    created: 1664064722000,
    type: 'quote',
    text: 'A great interview that tells it like it is, but gives us hope too. The longer we think the challenges we face are scary, the scarier it\'s going to get. Let\'s be brave and make big changes so we can get back to business as usual. But not the other way around. Don\'t get it twisted.<br><br>Quoting: <a href="https://x.com/Clim8Uncensored/status/1572935815629529088" rel="noopener noreferrer" target="_blank">@Clim8Uncensored</a> <span class="status">1572935815629529088</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1573825254559862785',
    created: 1664064155000,
    type: 'post',
    text: 'There is a beer spa in Denver. How did I not know about this before?!?',
    likes: 3,
    retweets: 0
  },
  {
    id: '1573822247793438720',
    created: 1664063438000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> ...or I could have said keep them until the impending collapse of society, but that seemed less fun ;)<br><br>In reply to: <a href="#1573820868366458881">1573820868366458881</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1573820868366458881',
    created: 1664063109000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> Let them get stuck to the floor of the back seat like a respectable person.<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/1573815370783367169" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">1573815370783367169</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1573448529107746816',
    created: 1663974337000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> Feedback sent!<br><br>Summary:<br>- Most exciting sports league in my lifetime<br>- Love the progressive culture<br>- Love the touring model (helps with the last point)<br>- ESPN+ is a must<br>- Will watch next year<br>- Please consider building a women\'s league (WPLL)<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1573418943552757760" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1573418943552757760</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1573439993439309834',
    created: 1663972302000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SanneGrinovero" rel="noopener noreferrer" target="_blank">@SanneGrinovero</a> <a class="mention" href="https://x.com/Hibernate" rel="noopener noreferrer" target="_blank">@Hibernate</a> <a class="mention" href="https://x.com/JakartaEE" rel="noopener noreferrer" target="_blank">@JakartaEE</a> Congrats! 🍻<br><br>In reply to: <a href="https://x.com/SanneGrinovero/status/1573270248551518208" rel="noopener noreferrer" target="_blank">@SanneGrinovero</a> <span class="status">1573270248551518208</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1573395069910265856',
    created: 1663961591000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kwootman" rel="noopener noreferrer" target="_blank">@kwootman</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> We happen to need this functionality in Antora Assembler because includes are resolved through a different classification system in Antora. Then we combine documents. We need all that work to be done by the time we pass the AsciiDoc to Asciidoctor PDF. Just one of many use cases.<br><br>In reply to: <a href="#1573394459160891392">1573394459160891392</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1573394459160891392',
    created: 1663961445000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kwootman" rel="noopener noreferrer" target="_blank">@kwootman</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> If it\'s something you need, you\'ll probably know why. The use cases vary widely. You may want to export the content to another tool. You may want to pre-resolve includes for an AsciiDoc file stored on GitHub. Or you may want to see &amp; analyze all content that\'s being included.<br><br>In reply to: <a href="https://x.com/kwootman/status/1518124606187610112" rel="noopener noreferrer" target="_blank">@kwootman</a> <span class="status">1518124606187610112</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1573393209333776385',
    created: 1663961147000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> 💡 I\'d love to see this happen, and I\'m sure many others in the community would too!<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1573255095151640576',
    created: 1663928219000,
    type: 'post',
    text: '...but I can\'t spell, proving I\'m still human 😜',
    likes: 1,
    retweets: 0
  },
  {
    id: '1573247265271345154',
    created: 1663926352000,
    type: 'post',
    text: 'I published three software released today and fielded a security report...all on top of client work. I\'m calling it a day. 🏖️',
    likes: 17,
    retweets: 0
  },
  {
    id: '1572666326338142208',
    created: 1663787845000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> Dang. Just as I feared. Thanks for following up!<br><br>In reply to: <a href="https://x.com/aalmiray/status/1572665038640381953" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">1572665038640381953</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1572664039779729408',
    created: 1663787300000,
    type: 'post',
    text: 'Is there any way to determine if Gradle is going to use ANSI control characters in the output? I have debugged deep into Gradle and I just can\'t find where to detect it. project.gradle.startParameter.consoleOutput only tells you "Auto". I need to know what it decides.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1572393226312744962',
    created: 1663722733000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> I love your idea of Labor Day finals. It fits with the Memorial Day culture of lacrosse. The question is whether you do semis on the same weekend like NCAA.<br><br>In reply to: <a href="#1572390661370032130">1572390661370032130</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1572390661370032130',
    created: 1663722121000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> I think the finals needs to happen sooner. Move the all-star game to after the playoffs (like the NFL) and cut out the gap before the playoffs. Maybe drop the week gap before the playoffs. And perhaps squeeze in a doubleheader week somewhere.<br><br>In reply to: <a href="https://x.com/danarestia/status/1572232142792294400" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1572232142792294400</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1572386425928323072',
    created: 1663721112000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sebi2706" rel="noopener noreferrer" target="_blank">@sebi2706</a> It was a privilege having you as a colleague for a very short year. You\'re a great devrel advocate with an unmatched style. Good luck on your next adventure! (And welcome to JBoss hell. It\'s fun down here.)<br><br>In reply to: <a href="https://x.com/sebi2706/status/1572213732130062336" rel="noopener noreferrer" target="_blank">@sebi2706</a> <span class="status">1572213732130062336</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1572149333461696514',
    created: 1663664584000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PMTsportsbiz" rel="noopener noreferrer" target="_blank">@PMTsportsbiz</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> And so you were right. I remember seeing this and thinking "really?" But now it all makes sense.<br><br>In reply to: <a href="https://x.com/PMTsportsbiz/status/1565025879054508032" rel="noopener noreferrer" target="_blank">@PMTsportsbiz</a> <span class="status">1565025879054508032</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1571774307810037770',
    created: 1663575171000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/PLLWaterdogs" rel="noopener noreferrer" target="_blank">@PLLWaterdogs</a> I stand behind this statement. After now seeing the season end-to-end, it\'s clear that McArdle was the attackmen of the year, at least in my book. 9 years in the making.<br><br>In reply to: <a href="#1569580855827525633">1569580855827525633</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1571773218188566528',
    created: 1663574912000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLChaos" rel="noopener noreferrer" target="_blank">@PLLChaos</a> That was a hell of a game. Thanks for putting on a great show! It was right down to the wire. Signature PLL.<br><br>In reply to: <a href="https://x.com/PLLChaos/status/1571607489724162048" rel="noopener noreferrer" target="_blank">@PLLChaos</a> <span class="status">1571607489724162048</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1571700382065373186',
    created: 1663557546000,
    type: 'quote',
    text: 'What a game and what a season! I\'m already counting the days for the next one to start. I\'ll remember this summer for a long time. 🥍🍻 Thanks <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a>!<br><br>Quoting: <a href="https://x.com/PremierLacrosse/status/1571608205423149067" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1571608205423149067</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1571574255632142339',
    created: 1663527475000,
    type: 'quote',
    text: 'I had totally lost interest in pro sports until I discovered <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a>. I\'m so excited about watching my first championship game. Let\'s go!<br><br>Quoting: <a href="https://x.com/PremierLacrosse/status/1571569896777031681" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1571569896777031681</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1571262945472151553',
    created: 1663453253000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/apr" rel="noopener noreferrer" target="_blank">@apr</a> For code, no. But for docs. Absolutely essential. I\'m thinking of maintenance / release line branches.<br><br>In reply to: <a href="https://x.com/apr/status/1571254977460322310" rel="noopener noreferrer" target="_blank">@apr</a> <span class="status">1571254977460322310</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1571252384369889280',
    created: 1663450735000,
    type: 'post',
    text: 'I love multiple git worktrees. It\'s the VCS feature that I\'ve always missed since I switched to git.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1571040458683600896',
    created: 1663400208000,
    type: 'post',
    text: 'I found the answer:<br><br>project.gradle.gradleUserHomeDir<br><br>Quoting: <a href="#1570199416904249350">1570199416904249350</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1570973821762703360',
    created: 1663384321000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLNationals" rel="noopener noreferrer" target="_blank">@PLLNationals</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/SteadmanClinic" rel="noopener noreferrer" target="_blank">@SteadmanClinic</a> <a class="mention" href="https://x.com/PLLWaterdogs" rel="noopener noreferrer" target="_blank">@PLLWaterdogs</a> <a class="mention" href="https://x.com/CashApp" rel="noopener noreferrer" target="_blank">@CashApp</a> That\'s certainly true, but I felt like Scarpello played really, really well in his place.<br><br>In reply to: <a href="https://x.com/PLLNationals/status/1570917625127120896" rel="noopener noreferrer" target="_blank">@PLLNationals</a> <span class="status">1570917625127120896</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1570943933940924416',
    created: 1663377195000,
    type: 'quote',
    text: 'I can\'t wait. This is going to be explosive and fast.<br><br>Quoting: <a href="https://x.com/ESPNPR/status/1570881514019213313" rel="noopener noreferrer" target="_blank">@ESPNPR</a> <span class="status">1570881514019213313</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1570902214415351809',
    created: 1663367248000,
    type: 'post',
    text: 'I think the hardest thing in life is doing the thing you know you must to do, but are afraid to do because of the change it will bring.',
    likes: 4,
    retweets: 1
  },
  {
    id: '1570858622489604098',
    created: 1663356855000,
    type: 'post',
    text: 'And this even makes writing a Gradle plugin for a Node.js package very easy. So even if you don\'t use npx directly, it\'s still helping behind the scenes.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1570858456663597058',
    created: 1663356815000,
    type: 'post',
    text: 'I really think npx is the single most impactful tool that was introduced into the Node.js ecosystem. It hides away all the complexities of installing a package so you can focus on just running the program. All you need is Node.js (which bundles it). Example:<br><br>npx antora version',
    likes: 3,
    retweets: 0
  },
  {
    id: '1570849665876389888',
    created: 1663354719000,
    type: 'reply',
    text: '@Avalanche1979 <a class="mention" href="https://x.com/SteveLAnderson" rel="noopener noreferrer" target="_blank">@SteveLAnderson</a> That\'s too bad. It\'s really a useful behavior.<br><br>In reply to: @Avalanche1979 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1570847490957778945',
    created: 1663354201000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jreleaser" rel="noopener noreferrer" target="_blank">@jreleaser</a> <a class="mention" href="https://x.com/hansolo_" rel="noopener noreferrer" target="_blank">@hansolo_</a> <a class="mention" href="https://x.com/kordamp" rel="noopener noreferrer" target="_blank">@kordamp</a> Thanks for the info!<br><br>In reply to: <a href="https://x.com/jreleaser/status/1570717191397601280" rel="noopener noreferrer" target="_blank">@jreleaser</a> <span class="status">1570717191397601280</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1570710068622163976',
    created: 1663321437000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/jreleaser" rel="noopener noreferrer" target="_blank">@jreleaser</a> I\'m interested in using JReleaser to release a single (very simple) Gradle plugin to the Gradle plugin portal. Is there an example I can reference?<br><br>If successful, I\'d be happy to help update the docs for others following in my footsteps.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1570607974955249667',
    created: 1663297096000,
    type: 'post',
    text: 'I want a shirt that reads "shit happens, and then shit happens"',
    likes: 1,
    retweets: 0
  },
  {
    id: '1570607823998038018',
    created: 1663297060000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nottycode" rel="noopener noreferrer" target="_blank">@nottycode</a> An explicit check. This is how most of the other languages do it. If you just need to know if a value is set, that\'s truthy. If it\'s not set (or in the case of a boolean, false), it\'s falsy. This is well known, established, &amp; documented, so no need to really rehash it all here.<br><br>In reply to: <a href="https://x.com/nottycode/status/1570589869117952000" rel="noopener noreferrer" target="_blank">@nottycode</a> <span class="status">1570589869117952000</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1570564087406563329',
    created: 1663286632000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SteveLAnderson" rel="noopener noreferrer" target="_blank">@SteveLAnderson</a> Sure, I get the technical reason. But it\'s about sugar. A lot of other languages have the concept of truthy and falsy (even Groovy). It seems that Java doesn\'t yet have that.<br><br>In reply to: <a href="https://x.com/SteveLAnderson/status/1570562878536847360" rel="noopener noreferrer" target="_blank">@SteveLAnderson</a> <span class="status">1570562878536847360</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1570539847034753024',
    created: 1663280853000,
    type: 'post',
    text: 'Oh, and I can\'t do "if (variable)" if the value of variable is null. What\'s that all about?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1570495304612585472',
    created: 1663270233000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DailyJLee" rel="noopener noreferrer" target="_blank">@DailyJLee</a> <a class="mention" href="https://x.com/joshsimmons" rel="noopener noreferrer" target="_blank">@joshsimmons</a> 🙋<br><br>In reply to: <a href="https://x.com/DailyJLee/status/1570068606829953026" rel="noopener noreferrer" target="_blank">@DailyJLee</a> <span class="status">1570068606829953026</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1570361022845964288',
    created: 1663238218000,
    type: 'post',
    text: 'I just wrote a bunch of Java with Vim and no IDE or code assist (because I was being lazy). It\'s amazing how comfortable it is now compared to dynamic languages like Ruby &amp; JavaScript. The only thing that kept biting me is the required semi-colon at the end of each statement.',
    likes: 3,
    retweets: 1
  },
  {
    id: '1570199416904249350',
    created: 1663199688000,
    type: 'post',
    text: 'Is it possible to look up the location of the global .gradle directory (the one located at ~/.gradle on *nix systems) in a system-independent way?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1570164203700523008',
    created: 1663191293000,
    type: 'post',
    text: 'I\'m trying to make an update to the docs for each question I answer. Let\'s see how long I can keep this up. It definitely feels like the right investment.',
    likes: 10,
    retweets: 0
  },
  {
    id: '1570139237105938432',
    created: 1663185340000,
    type: 'post',
    text: 'OH: I never mean to be more than silly—but text isn\'t my first language.<br><br>🤔',
    likes: 1,
    retweets: 0
  },
  {
    id: '1569884814001700864',
    created: 1663124681000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/wintermeyer" rel="noopener noreferrer" target="_blank">@wintermeyer</a> <a class="mention" href="https://x.com/siaw23" rel="noopener noreferrer" target="_blank">@siaw23</a> <a class="mention" href="https://x.com/radrubydev" rel="noopener noreferrer" target="_blank">@radrubydev</a> <a class="mention" href="https://x.com/andrzejkrzywda" rel="noopener noreferrer" target="_blank">@andrzejkrzywda</a> <a class="mention" href="https://x.com/codefolio" rel="noopener noreferrer" target="_blank">@codefolio</a> <a class="mention" href="https://x.com/schneems" rel="noopener noreferrer" target="_blank">@schneems</a> I think Antora extensions change everything. There\'s very little that isn\'t possible now. Still a lot to do in core, but the game is different now.<br><br>In reply to: <a href="https://x.com/wintermeyer/status/1569631147532980225" rel="noopener noreferrer" target="_blank">@wintermeyer</a> <span class="status">1569631147532980225</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1569868277899460608',
    created: 1663120738000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RJKaminski" rel="noopener noreferrer" target="_blank">@RJKaminski</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> The focus on kids of all backgrounds is everything. Keep up the great work.<br><br>In reply to: <a href="https://x.com/RJKaminski/status/1569675105013309440" rel="noopener noreferrer" target="_blank">@RJKaminski</a> <span class="status">1569675105013309440</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1569867611172925440',
    created: 1663120579000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> The lighting alone in this spot has really changed my mood.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1569867427437236224',
    created: 1663120536000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/chuckfrain" rel="noopener noreferrer" target="_blank">@chuckfrain</a> Respectfully disagree.<br><br>In reply to: <a href="https://x.com/chuckfrain/status/1569866531881848837" rel="noopener noreferrer" target="_blank">@chuckfrain</a> <span class="status">1569866531881848837</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1569862745570553856',
    created: 1663119419000,
    type: 'post',
    text: 'Proof.',
    photos: ['<div class="item"><img class="photo" src="media/1569862745570553856.jpg"></div>', '<div class="item"><img class="photo" src="media/1569862745570553856-2.jpg"></div>', '<div class="item"><img class="photo" src="media/1569862745570553856-3.jpg"></div>', '<div class="item"><img class="photo" src="media/1569862745570553856-4.jpg"></div>'],
    likes: 4,
    retweets: 0
  },
  {
    id: '1569862203649695745',
    created: 1663119290000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/plaindocs" rel="noopener noreferrer" target="_blank">@plaindocs</a> <a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> It has been a huge blow to progress.<br><br>In reply to: <a href="https://x.com/plaindocs/status/1569860128215650304" rel="noopener noreferrer" target="_blank">@plaindocs</a> <span class="status">1569860128215650304</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1569861932164972545',
    created: 1663119225000,
    type: 'post',
    text: 'Back working on a Gradle plugin again, this time the Antora one started by great <a class="mention" href="https://x.com/rob_winch" rel="noopener noreferrer" target="_blank">@rob_winch</a>. Fun times. 🐘',
    likes: 3,
    retweets: 0
  },
  {
    id: '1569856604698480640',
    created: 1663117955000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> Netlify has lost the plot. Their new billing is anti-open source and anti-collaboration. Everyone knows it. It\'s no mystery. I\'m not going to waste my breath. They know what they are doing.<br><br>In reply to: <a href="https://x.com/brunoborges/status/1569855858254938114" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1569855858254938114</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1569856079596748803',
    created: 1663117830000,
    type: 'post',
    text: 'In fact, I\'m so upset I\'m at the bar drinking to try to calm myself down.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1569855628860096513',
    created: 1663117723000,
    type: 'post',
    text: 'I\'m so upset with Netlify.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1569855122481774593',
    created: 1663117602000,
    type: 'quote',
    text: 'This was my day.<br><br>Quoting: <a href="https://x.com/BerrymoreBlue/status/1569503337573228545" rel="noopener noreferrer" target="_blank">@BerrymoreBlue</a> <span class="status">1569503337573228545</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1569627485880594434',
    created: 1663063329000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLWaterdogs" rel="noopener noreferrer" target="_blank">@PLLWaterdogs</a> <a class="mention" href="https://x.com/gobidesert25" rel="noopener noreferrer" target="_blank">@gobidesert25</a> Freeze that. New logo.<br><br>In reply to: <a href="https://x.com/PLLWaterdogs/status/1569029706942496773" rel="noopener noreferrer" target="_blank">@PLLWaterdogs</a> <span class="status">1569029706942496773</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1569626341208920064',
    created: 1663063056000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/Ryan_Boyle14" rel="noopener noreferrer" target="_blank">@Ryan_Boyle14</a> Was right about the bye week.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1569580855827525633',
    created: 1663052212000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/PLLWaterdogs" rel="noopener noreferrer" target="_blank">@PLLWaterdogs</a> For my $, McArdle has been the most impactful player of the year at attack. And he\'s done it as a total unsung hero. He does with Sowers. And he does it without Sowers. Either way, you love to see it.<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1569037926733955075" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1569037926733955075</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1569578406039416832',
    created: 1663051628000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PaulRabil" rel="noopener noreferrer" target="_blank">@PaulRabil</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/AndyCopelan_PLL" rel="noopener noreferrer" target="_blank">@AndyCopelan_PLL</a> <a class="mention" href="https://x.com/PLLWaterdogs" rel="noopener noreferrer" target="_blank">@PLLWaterdogs</a> The dawgs are gritty. And they have the receipts. Broken sticks, broken legs, but no broken hearts (well, except for the teams they play).<br><br>In reply to: <a href="https://x.com/PaulRabil/status/1569044796580192257" rel="noopener noreferrer" target="_blank">@PaulRabil</a> <span class="status">1569044796580192257</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1568695721003720705',
    created: 1662841179000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RobPannell3" rel="noopener noreferrer" target="_blank">@RobPannell3</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> After the year you had, I hope the answer is "a lot of years". I just want to share how much I enjoyed watching you play this season. Your team\'s record &amp; playoff result does not reflect the 💚 you put in. I never missed an opportunity to watch you #rollwoods from X and your ?.<br><br>In reply to: <a href="https://x.com/RobPannell3/status/1568633066616143872" rel="noopener noreferrer" target="_blank">@RobPannell3</a> <span class="status">1568633066616143872</span>',
    likes: 2,
    retweets: 0,
    tags: ['rollwoods']
  },
  {
    id: '1568693233596911618',
    created: 1662840586000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MichaelRabil" rel="noopener noreferrer" target="_blank">@MichaelRabil</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> A key reason I love watching the PLL is because the people involved in the league, from the front office to the field, are such great humans. To me, it\'s something that makes the PLL truly special. I\'m confident you\'ll continue to nurture &amp; recognize this quality as you grow.<br><br>In reply to: <a href="https://x.com/MichaelRabil/status/1568440985826238464" rel="noopener noreferrer" target="_blank">@MichaelRabil</a> <span class="status">1568440985826238464</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1568691444411670528',
    created: 1662840159000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/KyleHarrison18" rel="noopener noreferrer" target="_blank">@KyleHarrison18</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/trevorbaptiste9" rel="noopener noreferrer" target="_blank">@trevorbaptiste9</a> I was quite impressed by how well he did as a coach / team leader in the all-star game. Let\'s not forget the absolute dominance of that team he put together. I know he\'s got many years ahead of him as a player, but one day he\'s going to make an awesome coach!<br><br>In reply to: <a href="https://x.com/KyleHarrison18/status/1568597391602753537" rel="noopener noreferrer" target="_blank">@KyleHarrison18</a> <span class="status">1568597391602753537</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1568458558919921665',
    created: 1662784635000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> I drank a tall one in your honor. Keep being your awesome self. I appreciate you and your leadership! 🍻<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1568432391877959680" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1568432391877959680</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1568172951475990528',
    created: 1662716541000,
    type: 'post',
    text: 'I\'m working on so many different projects right now, even I\'m starting to call the projects by the wrong name.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1567662341101330432',
    created: 1662594802000,
    type: 'quote',
    text: 'The US is a joke.<br><br>Quoting: <a href="https://x.com/GovKathyHochul/status/1567537998383779857" rel="noopener noreferrer" target="_blank">@GovKathyHochul</a> <span class="status">1567537998383779857</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1567661687138045952',
    created: 1662594646000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> There is no doubt that Blaze\'s performance was damn impressive. But Currier neutralized Baptiste at the stripe. That\'s an MVP impact.<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1567645586480070658" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1567645586480070658</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1567650401054527491',
    created: 1662591955000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcioendo" rel="noopener noreferrer" target="_blank">@marcioendo</a> It took longer than expected, but we finally have a place to chat about all things AsciiDoc, including announcing and coordinating language implementation efforts. You\'re welcome to join us at <a href="https://chat.asciidoc.org" rel="noopener noreferrer" target="_blank">chat.asciidoc.org</a>. It\'s also an ideal place to ask for help when you get stuck.<br><br>In reply to: <a href="https://x.com/marcioendo/status/1550069052860686336" rel="noopener noreferrer" target="_blank">@marcioendo</a> <span class="status">1550069052860686336</span>',
    likes: 0,
    retweets: 1
  },
  {
    id: '1567457109595521025',
    created: 1662545871000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> I\'ve noticed that the same sound package is used for every game. It was kind of cute the first time, but it\'s getting old. Next year, I recommend mixing it up so we don\'t hear "I\'ve got the power" before every single power play. It just feels a tad cheesy.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1567455675571404801',
    created: 1662545529000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/Ryan_Boyle14" rel="noopener noreferrer" target="_blank">@Ryan_Boyle14</a> <a class="mention" href="https://x.com/jaltersports" rel="noopener noreferrer" target="_blank">@jaltersports</a> <a class="mention" href="https://x.com/chantel_mccabe" rel="noopener noreferrer" target="_blank">@chantel_mccabe</a> I\'d also like to suggest presenting the second midfield line when covering the lineup. It always feels odd to have key players come onto the field that have never been introduced / pictured. Connor Fields is a shining example. No mention, then 5 points. It\'s a glaring void.<br><br>In reply to: <a href="#1567451048973074433">1567451048973074433</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1567454286350778369',
    created: 1662545198000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/ESPNPlus" rel="noopener noreferrer" target="_blank">@ESPNPlus</a> One of the reasons I think the PLL is working so well is because the video quality &amp; playbook is so crystal clear. I was very surprised when that wasn\'t the experience we were getting in the first game, and all of the quarterfinal games TBH. Sharp lacrosse deserves sharp video.<br><br>In reply to: <a href="#1567453741099655171">1567453741099655171</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1567453741099655171',
    created: 1662545068000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/ESPNPlus" rel="noopener noreferrer" target="_blank">@ESPNPlus</a> I enjoyed watching these PLL games, a league in which every game is a championship caliber game. After watching the whole season, this was my first experience with the playoffs. Honest feedback, the video quality in the first game was awful. Simply put, it wasn\'t PLL quality.<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1566093690954780672" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1566093690954780672</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1567451048973074433',
    created: 1662544426000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/Ryan_Boyle14" rel="noopener noreferrer" target="_blank">@Ryan_Boyle14</a> <a class="mention" href="https://x.com/jaltersports" rel="noopener noreferrer" target="_blank">@jaltersports</a> <a class="mention" href="https://x.com/chantel_mccabe" rel="noopener noreferrer" target="_blank">@chantel_mccabe</a> I once again really enjoyed the detailed breakdown and analysis of the games. Great work!<br><br>With that said, I found the in-game interviews to be very distracting. Could you move those to halftime or after the game? I think they really waste the action happening on the field.<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1566081602081398784" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1566081602081398784</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1567020065719201792',
    created: 1662441672000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> I think Kav is a detriment to that offense and I wouldn\'t mind seeing him go. With Pannell at X, you need finishers.<br><br>In reply to: <a href="https://x.com/danarestia/status/1566151924621139968" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1566151924621139968</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1567019687267176453',
    created: 1662441581000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> When he broke out his notes, I knew it was going to be ugly.<br><br>In reply to: <a href="https://x.com/danarestia/status/1566156885492899840" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1566156885492899840</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1567014275172167680',
    created: 1662440291000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> Is there a prize for picking all the wrong teams? If so, my bracket is looking solid!<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1566833056060620800" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1566833056060620800</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1566907208792567808',
    created: 1662414764000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> <a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> "Note that you may need to edit the results of this goal." It\'s just that I\'ve been down that road and it burned me badly. I spent way too much time trying to make a good archetype (at the time for Java EE) and hardly anyone was happy with the result.<br><br>In reply to: <a href="https://x.com/brunoborges/status/1566895161296052224" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1566895161296052224</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1566905114610114560',
    created: 1662414265000,
    type: 'reply',
    text: '@infosec812 <a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> It\'s the “you will likely want to customize and clean up your archetypes” that concerns me. If it isn\'t fully automated, it won\'t be maintainable.<br><br>In reply to: @infosec812 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1566894241594298368',
    created: 1662411673000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Don\'t get me wrong. I think archetypes are great to have. But they are simply too expensive to create and maintain. If it\'s possible to auto-generate an archetype from an example project, that might be worth considering. Let\'s continue that conversation in the project chat.<br><br>In reply to: <a href="#1566891540777037824">1566891540777037824</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1566891540777037824',
    created: 1662411029000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> The Asciidoctor project only provides examples, which can be found here: <a href="https://github.com/asciidoctor/asciidoctor-maven-examples/" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-maven-examples/</a> I\'ve tried to maintain Maven archetypes in the past, and I\'ve found the effort to have a negative ROI.<br><br>In reply to: <a href="https://x.com/agoncal/status/1566764969818996739" rel="noopener noreferrer" target="_blank">@agoncal</a> <span class="status">1566764969818996739</span>',
    likes: 9,
    retweets: 2
  },
  {
    id: '1566012943136804864',
    created: 1662201555000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> It also really matters to me that the PLL stands for progress in general. A key reason I turned off sports a decade ago is because it all disgusted me (esp NFL). I needed a progressive league to pull me back in. Even then, it\'s conditional on the league maintaining strong values.<br><br>In reply to: <a href="#1566012304969216001">1566012304969216001</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1566012304969216001',
    created: 1662201403000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> I really enjoyed your article. I only discovered the PLL this year &amp; went all in on it. A key reason (aside from the play itself) is that it promises fair equity for players. So I really connected with your last point. They need to lean in on this goal for the mountain it is.<br><br>In reply to: <a href="https://x.com/danarestia/status/1564627222266265600" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1564627222266265600</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1566004307152945152',
    created: 1662199496000,
    type: 'post',
    text: 'The <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> league truly deserves to be commended for their website. I don\'t think I\'ve ever visited the website for another sports league, except on occasion. I visit the PLL website several times a week (stats, rosters, schedules, &amp; analysis). It\'s really well done.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1565641647207038978',
    created: 1662113031000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/samaaron" rel="noopener noreferrer" target="_blank">@samaaron</a> I always love those conversations. I know I\'m blowing someone\'s mind and, as a result, they will never stop thinking about it now that they know.<br><br>In reply to: <a href="https://x.com/samaaron/status/1565636046813106179" rel="noopener noreferrer" target="_blank">@samaaron</a> <span class="status">1565636046813106179</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1565607289066639360',
    created: 1662104839000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MylesJones_15" rel="noopener noreferrer" target="_blank">@MylesJones_15</a> It\'s fair to say that you all made this one of the most entertaining summers of my life. It was amazing to see you play on TV and even more amazing to see you play in person (Denver). You are special and what you are a part of is special. To summarize...#rollwoods<br><br>In reply to: <a href="https://x.com/MylesJones_15/status/1565471362034147331" rel="noopener noreferrer" target="_blank">@MylesJones_15</a> <span class="status">1565471362034147331</span>',
    likes: 0,
    retweets: 0,
    tags: ['rollwoods']
  },
  {
    id: '1565225893789765632',
    created: 1662013908000,
    type: 'post',
    text: 'My main hope is that the women\'s game also gets the same attention and distribution. I trust that these are the people who could make that happen.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1565224993226498048',
    created: 1662013693000,
    type: 'post',
    text: 'I cannot overstate how important it was to have every game available on ESPN+ and for them to have been given ESPN\'s graphics package treatment. Every broadcast comes across as a premier event. Lacrosse is finally getting the presentation it has long deserved.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1565223935917969408',
    created: 1662013441000,
    type: 'quote',
    text: 'Just watched Fate of a Sport. The story is as spirited &amp; determined as every <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> game I\'ve watched this year. The respect this league has for its players, its fans, the game, &amp; its creator is unparalleled. I count the minutes between each broadcast. It\'s riveting!<br><br>Quoting: <a href="https://x.com/PremierLacrosse/status/1564303365193904128" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1564303365193904128</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1564679194960744448',
    created: 1661883565000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bbatsov" rel="noopener noreferrer" target="_blank">@bbatsov</a> Thanks for stepping up to lead this vital tool / mode. You\'re welcome to announce it in the Asciidoctor chat. Hope to see you there!<br><br>In reply to: <a href="https://x.com/bbatsov/status/1564643983745318913" rel="noopener noreferrer" target="_blank">@bbatsov</a> <span class="status">1564643983745318913</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1564399020675461120',
    created: 1661816766000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JenaGriswold" rel="noopener noreferrer" target="_blank">@JenaGriswold</a> Voting in Colorado rocks!<br><br>In reply to: <a href="https://x.com/JenaGriswold/status/1564396226954424321" rel="noopener noreferrer" target="_blank">@JenaGriswold</a> <span class="status">1564396226954424321</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1563957504974807042',
    created: 1661711500000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLMICD" rel="noopener noreferrer" target="_blank">@PLLMICD</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> Nope. I\'ve had coaches who were like this and I never liked it. It always made me very anxious and play like crap. (2-8 speaks for itself) I prefer coaches who touch your soul and inspire you. Those are the keepers.<br><br>In reply to: <a href="https://x.com/PLLMICD/status/1563691129962463232" rel="noopener noreferrer" target="_blank">@PLLMICD</a> <span class="status">1563691129962463232</span>',
    likes: 3,
    retweets: 1
  },
  {
    id: '1563656982631890945',
    created: 1661639850000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> Why are there no profiles for the coaches on the web site? At least, those pages don\'t come up in the search results.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1563437073675145216',
    created: 1661587420000,
    type: 'post',
    text: 'It\'s really too bad that you can\'t install a Node.js module that\'s located in a subdirectory of a git repository directly from the git repository.<br><br>(You can use a relative dependency to convince npm to install it, but it won\'t install the module\'s dependencies).',
    likes: 1,
    retweets: 0
  },
  {
    id: '1563414762922266626',
    created: 1661582100000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/netskymusic" rel="noopener noreferrer" target="_blank">@netskymusic</a> Welcome to Denver! I hope the mountains are out in full glory to inspire you.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1563293646610317312',
    created: 1661553224000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> I want to say Bertrand, but I know it should be Currier.<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1563216168613163008" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1563216168613163008</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1563293141368614912',
    created: 1661553104000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> Lyle without a doubt.<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1563224503152361473" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1563224503152361473</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1563293002377793537',
    created: 1661553071000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> Nichtern, hands down.<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1563227341538484227" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1563227341538484227</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1563292871544885253',
    created: 1661553039000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> It\'s a mistake to not have either McArdle or Nichtern in this list.<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1563228064913317891" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1563228064913317891</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1563094891214712833',
    created: 1661505837000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ChrisGSeaton" rel="noopener noreferrer" target="_blank">@ChrisGSeaton</a> I\'m specifically referring to US notes.<br><br>In reply to: <a href="#1563090828565786626">1563090828565786626</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1563090828565786626',
    created: 1661504869000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ChrisGSeaton" rel="noopener noreferrer" target="_blank">@ChrisGSeaton</a> They should also be different sizes or textures. This is an accessibility issue for people who are blind. Currently, it\'s impossible without vision to tell the difference between notes since they all feel the same (unlike coins or credit cards). My uncle uses folds as hints.<br><br>In reply to: <a href="https://x.com/ChrisGSeaton/status/1562984756400525312" rel="noopener noreferrer" target="_blank">@ChrisGSeaton</a> <span class="status">1562984756400525312</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1562596669367001088',
    created: 1661387052000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLCannons" rel="noopener noreferrer" target="_blank">@PLLCannons</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> Despite only winning one game, I thoroughly enjoyed watching the team play all season. There were many great moments, especially in Denver. The level of lacrosse in the PLL is just so high that you can be that good and still walk away with an L. Either way, it\'s a win for fans.<br><br>In reply to: <a href="https://x.com/PLLCannons/status/1562550091897978882" rel="noopener noreferrer" target="_blank">@PLLCannons</a> <span class="status">1562550091897978882</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1562384933355540480',
    created: 1661336570000,
    type: 'post',
    text: 'I can\'t exactly explain why, but watching <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> (and lacrosse in general) just makes me happy.<br><br>(Many moons ago I used to play, but it was a very different game back then).',
    likes: 0,
    retweets: 0
  },
  {
    id: '1562360181513539584',
    created: 1661330669000,
    type: 'quote',
    text: 'Throwing a temper tantrum after you purposely ignored a request to be respectful and instead threw it back in someone\'s face is incredibly unprofessional behavior. It seems I made the right decision. Yep, that behavior will get you blocked.<br><br>Quoting: <a href="https://x.com/nikitasius/status/1562358598277160960" rel="noopener noreferrer" target="_blank">@nikitasius</a> <span class="status">1562358598277160960</span>',
    likes: 31,
    retweets: 0
  },
  {
    id: '1561913570689839104',
    created: 1661224188000,
    type: 'quote',
    text: 'I don\'t want to like this tweet since that\'s the wrong sentiment. But I totally agree that this society has become addicted to hate and it sickens me.<br><br>Quoting: <a href="https://x.com/kelseyhightower/status/1561884679724158977" rel="noopener noreferrer" target="_blank">@kelseyhightower</a> <span class="status">1561884679724158977</span>',
    likes: 14,
    retweets: 3
  },
  {
    id: '1560776999345348614',
    created: 1660953209000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLCannons" rel="noopener noreferrer" target="_blank">@PLLCannons</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/lyle4thompson" rel="noopener noreferrer" target="_blank">@lyle4thompson</a> When Lyle steered clear of the scuffle that happened at the end of the last game, that action spoke louder than words about how much respect he has for the game. A true role model through and through. 🙇<br><br>In reply to: <a href="https://x.com/PLLCannons/status/1560389199345307648" rel="noopener noreferrer" target="_blank">@PLLCannons</a> <span class="status">1560389199345307648</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1560765200067833856',
    created: 1660950395000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bartmaer" rel="noopener noreferrer" target="_blank">@bartmaer</a> <a class="mention" href="https://x.com/wimdeblauwe" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> <a class="mention" href="https://x.com/ApacheHop" rel="noopener noreferrer" target="_blank">@ApacheHop</a> Thanks!<br><br>In reply to: <a href="https://x.com/bartmaer/status/1560756277793505281" rel="noopener noreferrer" target="_blank">@bartmaer</a> <span class="status">1560756277793505281</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1560765173199417344',
    created: 1660950389000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bartmaer" rel="noopener noreferrer" target="_blank">@bartmaer</a> <a class="mention" href="https://x.com/wimdeblauwe" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> I\'m very glad to hear you don\'t have to suffer like we do. I hope every day that we can catch up with Europe and, for that matter, most of the world.<br><br>In reply to: <a href="https://x.com/bartmaer/status/1560752912732676098" rel="noopener noreferrer" target="_blank">@bartmaer</a> <span class="status">1560752912732676098</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1560746018542534657',
    created: 1660945822000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bartmaer" rel="noopener noreferrer" target="_blank">@bartmaer</a> <a class="mention" href="https://x.com/wimdeblauwe" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> Just fight to keep it that way. That\'s all I can say. You don\'t want this situation here. You don\'t want it with any fiber of your being.<br><br>In reply to: <a href="https://x.com/bartmaer/status/1560734993436946434" rel="noopener noreferrer" target="_blank">@bartmaer</a> <span class="status">1560734993436946434</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1560691994577358849',
    created: 1660932942000,
    type: 'post',
    text: 'I have "health insurance". I visited my primary care doctor about pain in my shoulder, then about pain in my hand a few weeks later. Both visits were less than 1 hour. Those two visits combined cost me over $350 out of pocket on top of what I pay in "insurance".',
    likes: 5,
    retweets: 1
  },
  {
    id: '1560142948582375424',
    created: 1660802039000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/HuttonJackson" rel="noopener noreferrer" target="_blank">@HuttonJackson</a> Brilliant. Ship it!<br><br>In reply to: <a href="https://x.com/HuttonJackson/status/1559907981768245249" rel="noopener noreferrer" target="_blank">@HuttonJackson</a> <span class="status">1559907981768245249</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1560139232928075776',
    created: 1660801153000,
    type: 'post',
    text: 'I think <a class="mention" href="https://x.com/Ryan_Boyle14" rel="noopener noreferrer" target="_blank">@Ryan_Boyle14</a> is my favorite <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> commentator. He\'s very technical, so I understand more of what\'s happening on the field and why. And he avoids going meta like most of the others. No smack, just solid analysis.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1559489577420824578',
    created: 1660646263000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kabirkhan" rel="noopener noreferrer" target="_blank">@kabirkhan</a> Love it!<br><br>In reply to: <a href="https://x.com/kabirkhan/status/1559488325916000261" rel="noopener noreferrer" target="_blank">@kabirkhan</a> <span class="status">1559488325916000261</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1558188661484560385',
    created: 1660336101000,
    type: 'reply',
    text: 'This is my first experience with <a class="mention" href="https://x.com/AUProSports" rel="noopener noreferrer" target="_blank">@AUProSports</a> and I very much want to be a supporter. But now you\'ve made it so that I have no way of doing that. Is <a class="mention" href="https://x.com/espn" rel="noopener noreferrer" target="_blank">@espn</a> going to support women\'s sports equal to men\'s sports, or not?!? I\'m getting mixed messages.<br><br>In reply to: <a href="#1558188110659211264">1558188110659211264</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1558188110659211264',
    created: 1660335969000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/espn" rel="noopener noreferrer" target="_blank">@espn</a> <a class="mention" href="https://x.com/AUProSports" rel="noopener noreferrer" target="_blank">@AUProSports</a> I\'m very frustrated that Athletes Unlimited lacrosse is no longer being streamed on <a class="mention" href="https://x.com/ESPNPlus" rel="noopener noreferrer" target="_blank">@ESPNPlus</a>. I was in the middle of watching the season. I paid my subscription fee. What else do you want? It only perpetuates the problem of women\'s sports being inaccessible.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1557820656221270016',
    created: 1660248362000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> Thanks for sharing that analysis! I had a hunch it was because of the redundancy of righties. I\'m not a huge fan of Nolting\'s play, so I\'d really like to see a swap there. Also, the PLL often uses attack on midfield (e.g., Matt Moore, Mac O\'Keefe), so there\'s that option too.<br><br>In reply to: <a href="https://x.com/danarestia/status/1557694370366775296" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1557694370366775296</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1557665069474803713',
    created: 1660211267000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> Plus, Cornell players do pretty well on offense in the PLL.<br><br>In reply to: <a href="#1557664152419971072">1557664152419971072</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1557664152419971072',
    created: 1660211048000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> Do you know what the story is as to why John Piatelli is not playing? He was the nation\'s leading goal scorer in the NCAA and the Cannons need more goal scorers...so...seems like a clear choice to me.<br><br>In reply to: <a href="https://x.com/danarestia/status/1556714421606653952" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1556714421606653952</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1556541114126983168',
    created: 1659943295000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/SportsCenter" rel="noopener noreferrer" target="_blank">@SportsCenter</a> <a class="mention" href="https://x.com/espn" rel="noopener noreferrer" target="_blank">@espn</a> We need a new advanced stat. # of times in SCTopT10. Make it happen.<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1556407091748749312" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1556407091748749312</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1556540676916985856',
    created: 1659943191000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLCannons" rel="noopener noreferrer" target="_blank">@PLLCannons</a> Don\'t put down those sticks yet. There\'s still a corner open, and that\'s all you need to see. You\'ve got talent, heart, and creativity. Fire away.<br><br>In reply to: <a href="https://x.com/PLLCannons/status/1555752883563499520" rel="noopener noreferrer" target="_blank">@PLLCannons</a> <span class="status">1555752883563499520</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1555818454904672256',
    created: 1659771000000,
    type: 'post',
    text: 'Oh, and I forgot to congratulate you on passing Casey Powell in total career points! You\'re two of my favorite to ever play the game. Brings back so many memories, with so many more still to be made! Let\'s go!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1555756234195623937',
    created: 1659756165000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RobPannell3" rel="noopener noreferrer" target="_blank">@RobPannell3</a> Thanks for the post-game greet from a fellow alum! My spouse and I picked such a great game to see you play again in person. 🥍#rollwoods<br><br>In reply to: <a href="#1555724183388114944">1555724183388114944</a>',
    likes: 1,
    retweets: 0,
    tags: ['rollwoods']
  },
  {
    id: '1555724183388114944',
    created: 1659748523000,
    type: 'post',
    text: 'On the board already with a crafty goal!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1555720926448087040',
    created: 1659747747000,
    type: 'post',
    text: 'There he is. Cornell legend. Lacrosse royalty. <a class="mention" href="https://x.com/RobPannell3" rel="noopener noreferrer" target="_blank">@RobPannell3</a>',
    photos: ['<div class="item"><img class="photo" src="media/1555720926448087040.jpg"></div>'],
    likes: 1,
    retweets: 0
  },
  {
    id: '1555699421463154690',
    created: 1659742620000,
    type: 'quote',
    text: 'Let\'s go!!!<br><br>Quoting: <a href="https://x.com/PLLRedwoods/status/1555660153009078273" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> <span class="status">1555660153009078273</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1555616301657120768',
    created: 1659722802000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/michaelaye" rel="noopener noreferrer" target="_blank">@michaelaye</a> <a class="mention" href="https://x.com/ramalhoorg" rel="noopener noreferrer" target="_blank">@ramalhoorg</a> <a class="mention" href="https://x.com/sschuldenzucker" rel="noopener noreferrer" target="_blank">@sschuldenzucker</a> <a class="mention" href="https://x.com/GaelVaroquaux" rel="noopener noreferrer" target="_blank">@GaelVaroquaux</a> <a class="mention" href="https://x.com/ccanonne_" rel="noopener noreferrer" target="_blank">@ccanonne_</a> <a class="mention" href="https://x.com/OReillyMedia" rel="noopener noreferrer" target="_blank">@OReillyMedia</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a href="https://github.com/CourseOrchestra/asciidoctor-open-document" rel="noopener noreferrer" target="_blank">github.com/CourseOrchestra/asciidoctor-open-document</a><br><br>In reply to: <a href="https://x.com/michaelaye/status/1555496220323446785" rel="noopener noreferrer" target="_blank">@michaelaye</a> <span class="status">1555496220323446785</span>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1555474825052712960',
    created: 1659689072000,
    type: 'reply',
    text: 'What I love about <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> is that no matter the outcome of the game, the fans always win because the lacrosse is just that good. I just take it all in play by play. Never a dull moment.<br><br>In reply to: <a href="#1555473482091155456">1555473482091155456</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1555473482091155456',
    created: 1659688752000,
    type: 'post',
    text: '#rollwoods tomorrow in Denver!<br><br>(Except I\'ll be secretly rooting for both teams because I just love gritty matchups in <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a>!)',
    likes: 0,
    retweets: 0,
    tags: ['rollwoods']
  },
  {
    id: '1555330195636178944',
    created: 1659654589000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLCannons" rel="noopener noreferrer" target="_blank">@PLLCannons</a> The changes last week definitely made a difference, even if the team still came up short. I\'ll be watching from the stands on Fri night. Good luck!<br><br>In reply to: <a href="https://x.com/PLLCannons/status/1555267073336610818" rel="noopener noreferrer" target="_blank">@PLLCannons</a> <span class="status">1555267073336610818</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1554763327267487749',
    created: 1659519437000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> I follow this process in all my projects now. You can see an example in Asciidoctor PDF. <a href="https://github.com/asciidoctor/asciidoctor-pdf/commit/38cc77d619e9718993304137eb2d3bcba194db33" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-pdf/commit/38cc77d619e9718993304137eb2d3bcba194db33</a><br><br>In reply to: <a href="#1554763097109192705">1554763097109192705</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1554763097109192705',
    created: 1659519383000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> I feel very strongly that the changelog should be updated as part of a change. First, it helps to clarify the change, both to the reviewer and to the person studying the log after a release. Second, it eliminates any work that must be done after the fact, automated or not.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1554130717172441088" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1554130717172441088</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1553099610754715648',
    created: 1659122777000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JustinMoses2" rel="noopener noreferrer" target="_blank">@JustinMoses2</a> <a class="mention" href="https://x.com/IntlBluegrass" rel="noopener noreferrer" target="_blank">@IntlBluegrass</a> <a class="mention" href="https://x.com/JerryDouglas" rel="noopener noreferrer" target="_blank">@JerryDouglas</a> <a class="mention" href="https://x.com/AndyHallMusic" rel="noopener noreferrer" target="_blank">@AndyHallMusic</a> <a class="mention" href="https://x.com/RobandTreymusic" rel="noopener noreferrer" target="_blank">@RobandTreymusic</a> Congratulations! You and Sierra are a personification of joy.<br><br>In reply to: <a href="https://x.com/JustinMoses2/status/1553086445262446594" rel="noopener noreferrer" target="_blank">@JustinMoses2</a> <span class="status">1553086445262446594</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1552937955009781760',
    created: 1659084235000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> Chunk is back!<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1552893331197161472" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1552893331197161472</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1552732846405455872',
    created: 1659035333000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/myfear" rel="noopener noreferrer" target="_blank">@myfear</a> <a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <a class="mention" href="https://x.com/venkat_s" rel="noopener noreferrer" target="_blank">@venkat_s</a> <a class="mention" href="https://x.com/ammbra1508" rel="noopener noreferrer" target="_blank">@ammbra1508</a> <a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> <a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> <a class="mention" href="https://x.com/eMalaGupta" rel="noopener noreferrer" target="_blank">@eMalaGupta</a> <a class="mention" href="https://x.com/brjavaman" rel="noopener noreferrer" target="_blank">@brjavaman</a> <a class="mention" href="https://x.com/CGuntur" rel="noopener noreferrer" target="_blank">@CGuntur</a> <a class="mention" href="https://x.com/prpatel" rel="noopener noreferrer" target="_blank">@prpatel</a> <a class="mention" href="https://x.com/corneil" rel="noopener noreferrer" target="_blank">@corneil</a> <a class="mention" href="https://x.com/techgirl1908" rel="noopener noreferrer" target="_blank">@techgirl1908</a> <a class="mention" href="https://x.com/evanchooly" rel="noopener noreferrer" target="_blank">@evanchooly</a> <a class="mention" href="https://x.com/heinzkabutz" rel="noopener noreferrer" target="_blank">@heinzkabutz</a> <a class="mention" href="https://x.com/rotnroll666" rel="noopener noreferrer" target="_blank">@rotnroll666</a> <a class="mention" href="https://x.com/mariofusco" rel="noopener noreferrer" target="_blank">@mariofusco</a> <a class="mention" href="https://x.com/ewolff" rel="noopener noreferrer" target="_blank">@ewolff</a> <a class="mention" href="https://x.com/kittylyst" rel="noopener noreferrer" target="_blank">@kittylyst</a> <a class="mention" href="https://x.com/rafaelcodes" rel="noopener noreferrer" target="_blank">@rafaelcodes</a> <a class="mention" href="https://x.com/ixchelruiz" rel="noopener noreferrer" target="_blank">@ixchelruiz</a> <a class="mention" href="https://x.com/dblevins" rel="noopener noreferrer" target="_blank">@dblevins</a> <a class="mention" href="https://x.com/holly_cummins" rel="noopener noreferrer" target="_blank">@holly_cummins</a> <a class="mention" href="https://x.com/DaschnerS" rel="noopener noreferrer" target="_blank">@DaschnerS</a> <a class="mention" href="https://x.com/bsideup" rel="noopener noreferrer" target="_blank">@bsideup</a> <a class="mention" href="https://x.com/hansolo_" rel="noopener noreferrer" target="_blank">@hansolo_</a> That means so much. I take every interaction to heart, not only because I cherish them, but also because of what I can learn from our exchanges. In other words, we make each other better people.<br><br>In reply to: <a href="https://x.com/myfear/status/1552729633816289281" rel="noopener noreferrer" target="_blank">@myfear</a> <span class="status">1552729633816289281</span>',
    likes: 8,
    retweets: 1
  },
  {
    id: '1552099736005525504',
    created: 1658884388000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/juliaferraioli" rel="noopener noreferrer" target="_blank">@juliaferraioli</a> Either way, the witches are still the stars.<br><br>In reply to: <a href="#1552099399408435201">1552099399408435201</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1552099399408435201',
    created: 1658884308000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/juliaferraioli" rel="noopener noreferrer" target="_blank">@juliaferraioli</a> I\'ve actually seen both renditions at this same bar. Someone has a fetish, for sure. I can report that both are just as trippy while drinking.<br><br>In reply to: <a href="https://x.com/juliaferraioli/status/1552098626280644608" rel="noopener noreferrer" target="_blank">@juliaferraioli</a> <span class="status">1552098626280644608</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1552098868413829122',
    created: 1658884181000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BrianGoetz" rel="noopener noreferrer" target="_blank">@BrianGoetz</a> <a class="mention" href="https://x.com/ChrisGSeaton" rel="noopener noreferrer" target="_blank">@ChrisGSeaton</a> I\'ve actually worked pretty hard to translate code in the other direction. It\'s amazing how well these two languages play together, but by no means are they the same. Just both great tools given the circumstances.<br><br>In reply to: <a href="https://x.com/BrianGoetz/status/1552092675976880132" rel="noopener noreferrer" target="_blank">@BrianGoetz</a> <span class="status">1552092675976880132</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1552098507955310592',
    created: 1658884095000,
    type: 'post',
    text: 'Watching the Wizard of Oz in a bar is trippy.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1552095984481607680',
    created: 1658883493000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BrianGoetz" rel="noopener noreferrer" target="_blank">@BrianGoetz</a> <a class="mention" href="https://x.com/ChrisGSeaton" rel="noopener noreferrer" target="_blank">@ChrisGSeaton</a> Inquiring minds want to know.<br><br>In reply to: <a href="https://x.com/BrianGoetz/status/1552092675976880132" rel="noopener noreferrer" target="_blank">@BrianGoetz</a> <span class="status">1552092675976880132</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1552095889241636866',
    created: 1658883471000,
    type: 'post',
    text: 'Can anyone who follows <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> tell me why John Piatelli is not playing? As the #1 goal scorer on the NCAA this past year, I\'m confused.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1552085472993628160',
    created: 1658880987000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BrianGoetz" rel="noopener noreferrer" target="_blank">@BrianGoetz</a> <a class="mention" href="https://x.com/ChrisGSeaton" rel="noopener noreferrer" target="_blank">@ChrisGSeaton</a> I always knew you were in our corner ;) (Joking aside, keep doing the great work you\'re doing on Java. We need you on that wall.)<br><br>In reply to: <a href="https://x.com/BrianGoetz/status/1552081959773421568" rel="noopener noreferrer" target="_blank">@BrianGoetz</a> <span class="status">1552081959773421568</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1552084851615969281',
    created: 1658880839000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RJDickenson" rel="noopener noreferrer" target="_blank">@RJDickenson</a> If you even need help with them, I extend you an open offer for assistance. Just ping me (preferably in the Asciidoctor Zulip).<br><br>In reply to: <a href="#1552082081986949120">1552082081986949120</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1552084455774253056',
    created: 1658880745000,
    type: 'quote',
    text: 'If you live in MO-01, please appreciate what incredible representation you have and reelect Cori Bush for Congress. There\'s no room for despair. We need people who have proven they will fight for our rights. Cori will not only hold that line, but advance it.<br><br>Quoting: <a href="https://x.com/CoriBush/status/1551553339762016257" rel="noopener noreferrer" target="_blank">@CoriBush</a> <span class="status">1551553339762016257</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1552083154860531712',
    created: 1658880435000,
    type: 'reply',
    text: '@brucefranksjr <a class="mention" href="https://x.com/CoriBush" rel="noopener noreferrer" target="_blank">@CoriBush</a> What I love most about her is that she pushes us to push her...and she isn\'t afraid to push others to get change. That\'s how it should be! Not a politician, a politivist.<br><br>In reply to: @brucefranksjr <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1552082610855104512',
    created: 1658880305000,
    type: 'reply',
    text: '@brucefranksjr <a class="mention" href="https://x.com/CoriBush" rel="noopener noreferrer" target="_blank">@CoriBush</a> Cori is by far my favorite politician. She\'s such a real one. And I\'m sure she will be glad to know that we\'ll continue to push her to improve people\'s lives. That\'s just who she is.<br><br>In reply to: @brucefranksjr <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1552082081986949120',
    created: 1658880179000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RJDickenson" rel="noopener noreferrer" target="_blank">@RJDickenson</a> Congrats! Regular expressions are the hand axe of programming. I couldn\'t live without them.<br><br>In reply to: <a href="https://x.com/RJDickenson/status/1552068706250895361" rel="noopener noreferrer" target="_blank">@RJDickenson</a> <span class="status">1552068706250895361</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1552017510282670080',
    created: 1658864784000,
    type: 'post',
    text: 'And maybe try Kirst in goal?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1551809738056146946',
    created: 1658815247000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/PLLCannons" rel="noopener noreferrer" target="_blank">@PLLCannons</a> Where the hell is John Piatelli? Y\'all need to find another attack player who can score goals.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1551702108646256640',
    created: 1658789586000,
    type: 'post',
    text: 'Absolutely every day has become a fight about something. It\'s getting so tiring.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1550638814569193472',
    created: 1658536077000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/looselytyped" rel="noopener noreferrer" target="_blank">@looselytyped</a> <a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> I could say the same thing. It changed my path forever.<br><br>In reply to: <a href="https://x.com/looselytyped/status/1550633161628999684" rel="noopener noreferrer" target="_blank">@looselytyped</a> <span class="status">1550633161628999684</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1550563544738934784',
    created: 1658518131000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/evanchooly" rel="noopener noreferrer" target="_blank">@evanchooly</a> 🤣<br><br>In reply to: <a href="https://x.com/evanchooly/status/1550545421986152449" rel="noopener noreferrer" target="_blank">@evanchooly</a> <span class="status">1550545421986152449</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1550426947720605699',
    created: 1658485564000,
    type: 'reply',
    text: '@flynnwaslike <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> For the record, I\'m just as excited about women\'s lacrosse and want to see it thrive as well. I\'m definitely going to watch Athletes Unlimited, though what I really want to see is a full-season WPLL.<br><br>In reply to: <a href="#1550398366160265218">1550398366160265218</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1550401649729867776',
    created: 1658479533000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JNEU_88" rel="noopener noreferrer" target="_blank">@JNEU_88</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/AndyTowersPLL" rel="noopener noreferrer" target="_blank">@AndyTowersPLL</a> It\'s time to update your bio to read 118 MPH 😉<br><br>In reply to: <a href="https://x.com/JNEU_88/status/1549462173444890632" rel="noopener noreferrer" target="_blank">@JNEU_88</a> <span class="status">1549462173444890632</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1550398366160265218',
    created: 1658478750000,
    type: 'reply',
    text: '@flynnwaslike <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> It\'s the most excited I\'ve been about sports since I was competing in college.<br><br>In reply to: @flynnwaslike <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1550395378335227906',
    created: 1658478037000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/northwestopend1" rel="noopener noreferrer" target="_blank">@northwestopend1</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> Forage works because what the extension is doing is going out and gather up more content, perhaps in an ad-hoc, resourceful way. Pioneer works because it\'s going out into the wild in a quest for more information / content.<br><br>In reply to: <a href="#1550393759002595328">1550393759002595328</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1550394218496987141',
    created: 1658477761000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DuncanLock" rel="noopener noreferrer" target="_blank">@DuncanLock</a> I don\'t like riffing on the Ascii prefix for anything. It\'s puts the focus on the wrong term and comes across as extremely clichéd IMO.<br><br>In reply to: <a href="https://x.com/DuncanLock/status/1550393473219514369" rel="noopener noreferrer" target="_blank">@DuncanLock</a> <span class="status">1550393473219514369</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1550393759002595328',
    created: 1658477651000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/northwestopend1" rel="noopener noreferrer" target="_blank">@northwestopend1</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> This got me thinking about terms in this domain. I came up with "forage" (or "forager") and "pioneer". 🤔<br><br>In reply to: <a href="https://x.com/northwestopend1/status/1550367693878050818" rel="noopener noreferrer" target="_blank">@northwestopend1</a> <span class="status">1550367693878050818</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1550254661713571840',
    created: 1658444488000,
    type: 'quote',
    text: 'Don\'t Look Up is a documentary.<br><br>Quoting: <a href="https://x.com/benphillips76/status/1549768004233314306" rel="noopener noreferrer" target="_blank">@benphillips76</a> <span class="status">1549768004233314306</span>',
    likes: 0,
    retweets: 1
  },
  {
    id: '1550249490560323586',
    created: 1658443255000,
    type: 'post',
    text: 'I need to name a new Antora extension that runs a configurable command and imports the files it generates. The working title is "Antora make extension" (conjuring up the thought of a make command). Got any better ideas?',
    likes: 2,
    retweets: 1
  },
  {
    id: '1550248182512173056',
    created: 1658442943000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> I\'ve always liked Ramble On best because it gives me the ultimate feeling of adventure.<br><br>In reply to: <a href="https://x.com/brunoborges/status/1550235868316721153" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1550235868316721153</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1550023924825464832',
    created: 1658389476000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> Congrats! I\'m exited to follow your development (pun intended) in this new chapter! 🤖<br><br>In reply to: <a href="https://x.com/matthewmccull/status/1549839489106735104" rel="noopener noreferrer" target="_blank">@matthewmccull</a> <span class="status">1549839489106735104</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1549862194279432192',
    created: 1658350916000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcioendo" rel="noopener noreferrer" target="_blank">@marcioendo</a> If you\'re really serious (or even semi-serious) about making a parser for AsciiDoc, I\'d invite you to get involved in the AsciiDoc spec and Eclipse Austen project. As the second half of this year begins, we should start to see the activity pick up. But we need help.<br><br>In reply to: <a href="#1549861842075258880">1549861842075258880</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1549861842075258880',
    created: 1658350832000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcioendo" rel="noopener noreferrer" target="_blank">@marcioendo</a> It\'s a long outstanding issue and a key goal of the AsciiDoc spec to switch from regex parsing to a formal grammar for processing inline markup. We always knew we\'d eventually need to make that jump. We\'re proceeding with that in mind. Even the original Python impl used regex.<br><br>In reply to: <a href="https://x.com/marcioendo/status/1549352172458020864" rel="noopener noreferrer" target="_blank">@marcioendo</a> <span class="status">1549352172458020864</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1549328047412617217',
    created: 1658223566000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PaulRabil" rel="noopener noreferrer" target="_blank">@PaulRabil</a> Going into it, I wasn\'t sure. But I came away an instant fan of sixes. I still love the PLL-style field lacrosse format best (mostly because I\'ve waited decades for it to happen), but sixes is another great course for the sport. Let\'s keep it.<br><br>In reply to: <a href="https://x.com/PaulRabil/status/1547261709093548033" rel="noopener noreferrer" target="_blank">@PaulRabil</a> <span class="status">1547261709093548033</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1549327478782537728',
    created: 1658223430000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PaulRabil" rel="noopener noreferrer" target="_blank">@PaulRabil</a> <a class="mention" href="https://x.com/TWG2022" rel="noopener noreferrer" target="_blank">@TWG2022</a> <a class="mention" href="https://x.com/LA28" rel="noopener noreferrer" target="_blank">@LA28</a> The same gameplay &amp; rules (almost) for both men and women is so huge. It puts lacrosse on a level playing field for all participants. Just put helmets and gloves on the women and let\'s finish the job of leveling it.<br><br>In reply to: <a href="https://x.com/PaulRabil/status/1547261660582203392" rel="noopener noreferrer" target="_blank">@PaulRabil</a> <span class="status">1547261660582203392</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1549118529357508608',
    created: 1658173613000,
    type: 'post',
    text: 'A nationwide survey of tenants who rent from the same residential management company as us shows that 88% of residents are concerned about climate change. This idea that it\'s a fringe issue that few people care about is just BS. What more needs to happen for the gov\'t to act?!?',
    photos: ['<div class="item"><img class="photo" src="media/1549118529357508608.jpg"></div>'],
    likes: 2,
    retweets: 0
  },
  {
    id: '1548403898196455426',
    created: 1658003231000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BMOtheyCallme" rel="noopener noreferrer" target="_blank">@BMOtheyCallme</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/PardonMyTake" rel="noopener noreferrer" target="_blank">@PardonMyTake</a> I get that. I just find it odd. I\'ll enjoy it anyway, though ;)<br><br>In reply to: <a href="https://x.com/BMOtheyCallme/status/1548402465355116546" rel="noopener noreferrer" target="_blank">@BMOtheyCallme</a> <span class="status">1548402465355116546</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1548035451943956480',
    created: 1657915387000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> Yep.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1548034805085790211" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1548034805085790211</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1547874773887172609',
    created: 1657877078000,
    type: 'post',
    text: 'That said, I\'m very happy to see how far the women\'s game has come. The players are doing an amazing job of working within the limits of the rules to put on an incredible show of skill, stamina, and strategy. They deserve a ton of recognition as athletes.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1547873182710804480',
    created: 1657876699000,
    type: 'post',
    text: 'The men are not beating the crap out of each other just because they wear helmets. And those rules are very, very well established. This is not unknown territory. I think it\'s well past time to make this change.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1547872207157637123',
    created: 1657876466000,
    type: 'post',
    text: 'And with helmets, you can remove this ridiculous rule of no stick check above the head, which is where the stick is half the time. If you touch the helmet, that\'s a foul of course. But if you don\'t, it should be totally legal.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1547871886406668288',
    created: 1657876390000,
    type: 'post',
    text: 'People who claim the game will become violent are just fearmongering. There are rules and there are refs. If you do something illegal, you have a seat in the sin bin. Players aren\'t stupid. They understand how to abide by the rules.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1547871113652211716',
    created: 1657876206000,
    type: 'post',
    text: 'The fact that it puts the safety of the players at risk is proven by the fact that the men have always worn helmets. I would not play lacrosse without a helmet. That ball moves fast and in every direction. If you get hit in the head, it\'s not going to end well.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1547870800123875328',
    created: 1657876131000,
    type: 'post',
    text: 'After watching a ton of lacrosse, both men\'s &amp; women\'s NCAA, international, box, and sixes, I find it absolutely absurd that the women do not wear helmets. It makes no sense to me. It holds back the game and puts the safety of the players at risk. I hope this changes soon.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1547868361161265153',
    created: 1657875549000,
    type: 'post',
    text: 'I\'m going to refer back to this summer as the summer of lacrosse. It\'s been a lot of fun to have something to enjoy during my downtime (what little of it there is).',
    likes: 0,
    retweets: 0
  },
  {
    id: '1547654727642492933',
    created: 1657824615000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/PardonMyTake" rel="noopener noreferrer" target="_blank">@PardonMyTake</a> I don\'t like bonus ball idea. The games are already extremely close and I don\'t think there\'s a need to mess with the point values.<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1547274640359247872" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1547274640359247872</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1547654143786905605',
    created: 1657824476000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pllstats" rel="noopener noreferrer" target="_blank">@pllstats</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> I like 15.<br><br>In reply to: <a href="https://x.com/pllstats/status/1547305080847708160" rel="noopener noreferrer" target="_blank">@pllstats</a> <span class="status">1547305080847708160</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1547512408154914816',
    created: 1657790684000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RezzaBuh" rel="noopener noreferrer" target="_blank">@RezzaBuh</a> <a class="mention" href="https://x.com/RedHat" rel="noopener noreferrer" target="_blank">@RedHat</a> Keep up the great work! The longevity of Red Hat as a leader in open source is unmatched thanks to dedicated employees like you.<br><br>In reply to: <a href="https://x.com/RezzaBuh/status/1547504872412643328" rel="noopener noreferrer" target="_blank">@RezzaBuh</a> <span class="status">1547504872412643328</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1547496859991584768',
    created: 1657786977000,
    type: 'quote',
    text: 'Here\'s the goal of the season in the NLL, for reference. He shot it with his non-dominant hand...if you can even say Jeff Teat has a non-dominant hand. I heard he\'s pretty good at lacrosse.<br><br>Quoting: <a href="https://x.com/PremierLacrosse/status/1540861992331796480" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1540861992331796480</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1547494891961274368',
    created: 1657786507000,
    type: 'post',
    text: 'Here\'s a still of the microsecond the ball leaves his stick.',
    photos: ['<div class="item"><img class="photo" src="media/1547494891961274368.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1547481455718064128',
    created: 1657783304000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <a class="mention" href="https://x.com/RedHat" rel="noopener noreferrer" target="_blank">@RedHat</a> And the new logo too!<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/1546887562706419712" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">1546887562706419712</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1547475324736192512',
    created: 1657781842000,
    type: 'quote',
    text: 'Here\'s the shot from above in real time.<br><br>Quoting: <a href="https://x.com/LacrosseNetwork/status/1547044287891640320" rel="noopener noreferrer" target="_blank">@LacrosseNetwork</a> <span class="status">1547044287891640320</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1547475008535949312',
    created: 1657781767000,
    type: 'quote',
    text: 'After scoring the goal of the season in the PLL, Jeff Teat is back to top it with the goal of the tournament at the World Games. It\'s a no look, behind-the-back, one-handed, falling down shot that lands on goal and breezes by the goalie. It can\'t be done. It just can\'t be done.<br><br>Quoting: <a href="https://x.com/iamneilbarrett/status/1547389835463237632" rel="noopener noreferrer" target="_blank">@iamneilbarrett</a> <span class="status">1547389835463237632</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1547368149179215872',
    created: 1657756290000,
    type: 'quote',
    text: '🎯<br><br>Quoting: <a href="https://x.com/chrislhayes/status/1547338466966441984" rel="noopener noreferrer" target="_blank">@chrislhayes</a> <span class="status">1547338466966441984</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1547345331808903168',
    created: 1657750850000,
    type: 'quote',
    text: 'This statement by the White House absolutely disgusts me.<br><br>Quoting: <a href="https://x.com/jordanzakarin/status/1545876493263323136" rel="noopener noreferrer" target="_blank">@jordanzakarin</a> <span class="status">1545876493263323136</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1547303534604853248',
    created: 1657740884000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <a class="mention" href="https://x.com/RedHat" rel="noopener noreferrer" target="_blank">@RedHat</a> Legend.<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/1546887562706419712" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">1546887562706419712</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1547151332078809088',
    created: 1657704596000,
    type: 'post',
    text: 'Oh, and no stick compliance checks after every goal. Thank goodness!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1547100892687527936',
    created: 1657692571000,
    type: 'post',
    text: 'Watched 1st day of international competition of women\'s lacrosse "sixes" at the World Games. No free position shots. No stoppage on a foul until change of possession. 30s shot clock. This is more like it! Just add helmets and it\'s nearly aligned with the men\'s game. Progression.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1546988961553272832',
    created: 1657665884000,
    type: 'post',
    text: 'Big tech is so funny. They purport, "we move fast and break things!" In reality, glaciers are melting faster than they move.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1546975700048965632',
    created: 1657662722000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RedHat" rel="noopener noreferrer" target="_blank">@RedHat</a> <a class="mention" href="https://x.com/matthicksj" rel="noopener noreferrer" target="_blank">@matthicksj</a> Congrats and good luck, Matt! Red Hat is in very competent hands. I look forward to what\'s to come under your direction!<br><br>In reply to: <a href="https://x.com/RedHat/status/1546974018473000961" rel="noopener noreferrer" target="_blank">@RedHat</a> <span class="status">1546974018473000961</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1546388376571367424',
    created: 1657522694000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CKrger" rel="noopener noreferrer" target="_blank">@CKrger</a> Absolutely. I think it starts with understanding that the repository is not cloned, but rather built step-by-step from an empty repository. That breaks a lot of assumptions, but also provides a lot of answers.<br><br>In reply to: <a href="https://x.com/CKrger/status/1546366524058902529" rel="noopener noreferrer" target="_blank">@CKrger</a> <span class="status">1546366524058902529</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1546030795369787392',
    created: 1657437440000,
    type: 'post',
    text: 'There\'s knowing how to use git...then there\'s knowing how to use git in CI where the git repository is only partially initialized.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1546025573666865157',
    created: 1657436195000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/michaelsayman" rel="noopener noreferrer" target="_blank">@michaelsayman</a> Vegan food is fire. That\'s because all the key ingredients are the stuff you\'d otherwise have to add to make the food good for you ;)<br><br>In reply to: <a href="https://x.com/michaelsayman/status/1545860728602693638" rel="noopener noreferrer" target="_blank">@michaelsayman</a> <span class="status">1545860728602693638</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1545125186856374272',
    created: 1657221526000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/j2r2b" rel="noopener noreferrer" target="_blank">@j2r2b</a> I have searched and searched over the years to find that consistency. It just doesn\'t exist. My best advice is to look at the CSS frameworks and see how they name things. But it will frustrate you to tears that even those aren\'t consistent.<br><br>In reply to: <a href="https://x.com/j2r2b/status/1545029126842716160" rel="noopener noreferrer" target="_blank">@j2r2b</a> <span class="status">1545029126842716160</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1544791235151294464',
    created: 1657141905000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> The option was renamed to --location.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1544781370731294720',
    created: 1657139554000,
    type: 'post',
    text: 'I can\'t believe npm has deprecated the -g flag. Totally unnecessary. That\'s going to create nothing but headaches.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1544755580727832577',
    created: 1657133405000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> 💯🍉<br><br>In reply to: <a href="https://x.com/settermjd/status/1544736595663781888" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1544736595663781888</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1544546553611751424',
    created: 1657083569000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/yarmiganosca" rel="noopener noreferrer" target="_blank">@yarmiganosca</a> I\'m just pointing out that right now, there is no such line. People are still casually supporting him, and I find that repulsive.<br><br>In reply to: <a href="#1544546297855610881">1544546297855610881</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1544546297855610881',
    created: 1657083508000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/yarmiganosca" rel="noopener noreferrer" target="_blank">@yarmiganosca</a> As I said, could just be symbolism, and I don\'t like Rogan either for what he\'s done. But we need to at least be on the same page across the board that there\'s no room for Trump in our discourse. Yes, we can do so, so much better. But there has to be a line somewhere at least.<br><br>In reply to: <a href="https://x.com/yarmiganosca/status/1544531502217691136" rel="noopener noreferrer" target="_blank">@yarmiganosca</a> <span class="status">1544531502217691136</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1544507780119371777',
    created: 1657074325000,
    type: 'quote',
    text: 'I know it\'s a small thing, perhaps even just symbolic, but it matters. We need all those who are willing to say that the radical, traitorous right have no place in our discourse. We can\'t afford to be coy about this.<br><br>Quoting: <a href="https://x.com/Variety/status/1544375385810292736" rel="noopener noreferrer" target="_blank">@Variety</a> <span class="status">1544375385810292736</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1544506574202437632',
    created: 1657074037000,
    type: 'post',
    text: 'I cannot help to think that the stick check after every goal in women\'s lacrosse is sexist. It sends a message that women somehow cannot be trusted. Absolutely absurd. It\'s horrible and antiquated. Get rid of it.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1544495347438854144',
    created: 1657071360000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joshsimmons" rel="noopener noreferrer" target="_blank">@joshsimmons</a> <a class="mention" href="https://x.com/tidelift" rel="noopener noreferrer" target="_blank">@tidelift</a> And big ups to the people who support OSS.<br><br>In reply to: <a href="https://x.com/joshsimmons/status/1544490942714880000" rel="noopener noreferrer" target="_blank">@joshsimmons</a> <span class="status">1544490942714880000</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1544494687444242432',
    created: 1657071203000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joshsimmons" rel="noopener noreferrer" target="_blank">@joshsimmons</a> I\'m with you through and through. And you reminded me to call my brother. He\'s a real one and I\'ll cherish the moments I have with him, just as you are with your brother. Good family is good family, blood or no blood.<br><br>In reply to: <a href="https://x.com/joshsimmons/status/1544490943604174849" rel="noopener noreferrer" target="_blank">@joshsimmons</a> <span class="status">1544490943604174849</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1544493939931103233',
    created: 1657071025000,
    type: 'post',
    text: 'Honestly, if you\'re in to watching sports, and you\'re not watching lacrosse, you\'re really missing out. You live on the edge of your seat the entire game. And there\'s plenty of scoring to keep the game constantly in flux.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1543860600010248193',
    created: 1656920025000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> <a class="mention" href="https://x.com/myfear" rel="noopener noreferrer" target="_blank">@myfear</a> When I see fireworks in the sky tonight, I will think of them as being a celebration of your birthday. 🎆<br><br>In reply to: <a href="https://x.com/alexsotob/status/1543852874064601090" rel="noopener noreferrer" target="_blank">@alexsotob</a> <span class="status">1543852874064601090</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1543827616796254209',
    created: 1656912161000,
    type: 'post',
    text: 'In general, I would really like to see the rules for women\'s lacrosse converge with the rules for men\'s lacrosse. I don\'t really see why they have to be different.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1543827615122747392',
    created: 1656912161000,
    type: 'post',
    text: 'I\'m also not a big fan of the draw. It seems like it complicates the game. A face-off, like we see in the men\'s contest, makes a whole lot more sense to me.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1543827613352726528',
    created: 1656912160000,
    type: 'post',
    text: 'As an observer, I\'m still a bit frustrated though about some of the rules that seem to disrupt the game play. In particular, I don\'t like that a foul against the shooter can erase a goal (even though it results in a free position shot). That just doesn\'t make any sense.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1543827611993837569',
    created: 1656912160000,
    type: 'post',
    text: 'I\'m really enjoying watching the Lacrosse Women\'s World Championship games. There\'s so much talent on the field and it\'s so thrilling to watch.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1542644237186920450',
    created: 1656630021000,
    type: 'post',
    text: 'I\'m so excited to start watching the Women\'s Lacrosse World Championship. We\'re spoiled for exciting 🥍 action right now. Let\'s go!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1542632670416384001',
    created: 1656627264000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MStine" rel="noopener noreferrer" target="_blank">@MStine</a> That last one is so essential and often times covers the first 5. (Though more information is always better, so I say go for all 6 if you can).<br><br>In reply to: @mstine <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1542624060462403584',
    created: 1656625211000,
    type: 'post',
    text: 'It hopefully goes without saying, but I\'ll say it regardless...I\'m speaking about the rights of all those who have the *choice* to give birth, including trans men.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1542617219204468742',
    created: 1656623580000,
    type: 'post',
    text: 'No one needs to hold your hand.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1542617192805453824',
    created: 1656623573000,
    type: 'post',
    text: 'I have no time for foolishness. If you can\'t say you support women without precondition, or you defend men who fail to understand the gravity of the situation women are facing (losing the right to make decisions about their own bodies), you\'re feeding this crisis.',
    likes: 9,
    retweets: 1
  },
  {
    id: '1542593204339347459',
    created: 1656617854000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BryanFinster" rel="noopener noreferrer" target="_blank">@BryanFinster</a> Get lost with this absolute foolishness. This is serious shit, and you\'re just proving my point that men don\'t get just how serious this is. It\'s easy for you to stand there and say the standard is too high while you enjoy your rights and women lose theirs.<br><br>In reply to: <a href="https://x.com/BryanFinster/status/1542592677778259970" rel="noopener noreferrer" target="_blank">@BryanFinster</a> <span class="status">1542592677778259970</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1542591772781514752',
    created: 1656617513000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BryanFinster" rel="noopener noreferrer" target="_blank">@BryanFinster</a> It\'s lack of maturity on their part. Don\'t make excuses for them. This is how we have enabled such a childish society. And why women\'s rights are being eroded.<br><br>In reply to: <a href="https://x.com/BryanFinster/status/1542587452879257601" rel="noopener noreferrer" target="_blank">@BryanFinster</a> <span class="status">1542587452879257601</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1542576221988433920',
    created: 1656613805000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BryanFinster" rel="noopener noreferrer" target="_blank">@BryanFinster</a> They are adults. They should know better by now. I\'m not going to indulge them because they refuse to learn. This is not about purity. This is about common sense and growing up.<br><br>In reply to: <a href="https://x.com/BryanFinster/status/1542467732964261889" rel="noopener noreferrer" target="_blank">@BryanFinster</a> <span class="status">1542467732964261889</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1542349503268696065',
    created: 1656559751000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DomStarsiaPLL" rel="noopener noreferrer" target="_blank">@DomStarsiaPLL</a> Women\'s PLL. That\'s not only in the spirit of Title IX, but also something I\'m genuinely interested in watching.<br><br>In reply to: <a href="https://x.com/DomStarsiaPLL/status/1541114677052194816" rel="noopener noreferrer" target="_blank">@DomStarsiaPLL</a> <span class="status">1541114677052194816</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1542301579121856513',
    created: 1656548325000,
    type: 'quote',
    text: 'You can request any song you like as long as it\'s Killing in the Name by <a class="mention" href="https://x.com/RATM" rel="noopener noreferrer" target="_blank">@RATM</a>.<br><br>Quoting: <a href="https://x.com/traceylindeman/status/1542232903106629632" rel="noopener noreferrer" target="_blank">@traceylindeman</a> <span class="status">1542232903106629632</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1542298240762032128',
    created: 1656547529000,
    type: 'quote',
    text: 'This is the energy we need right now. <a href="https://www.kissradio.ca/audio/" rel="noopener noreferrer" target="_blank">www.kissradio.ca/audio/</a><br><br>Quoting: <a href="https://x.com/traceylindeman/status/1542232903106629632" rel="noopener noreferrer" target="_blank">@traceylindeman</a> <span class="status">1542232903106629632</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1542295038138339328',
    created: 1656546766000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> That issue is now resolved in 2.0.1.<br><br>In reply to: <a href="https://x.com/settermjd/status/1539193912992051201" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1539193912992051201</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1542294238297063425',
    created: 1656546575000,
    type: 'post',
    text: 'The Supreme Court is systematically taking away the civil liberties of Americans. But make no mistake, the same organizations behind it are operating in other countries to do this worldwide. Don\'t be deceived into thinking this is just a US issue. We need to rise up.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1542291754497413120',
    created: 1656545983000,
    type: 'post',
    text: 'A good comeback if you hear this lead-in would be "...and what if you didn\'t have daughters? What then?"',
    likes: 4,
    retweets: 0
  },
  {
    id: '1542290659058429952',
    created: 1656545722000,
    type: 'quote',
    text: 'The way you approach it is that you take it head on. Anything less is just enabling this kind of tomfoolery. We are what we tolerate. Don\'t tolerate it.<br><br>Quoting: <a href="https://x.com/jbeda/status/1542231321488945152" rel="noopener noreferrer" target="_blank">@jbeda</a> <span class="status">1542231321488945152</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1542290139656773633',
    created: 1656545598000,
    type: 'post',
    text: 'When I hear men wanting to show their support for women, they often pretext it by saying “I have three daughters” or “I have a wife…” That just sounds like you wouldn\'t otherwise. Don\'t do that. Support women because they deserve equal rights and benefits. Period.',
    likes: 15,
    retweets: 1
  },
  {
    id: '1542273128570552321',
    created: 1656541542000,
    type: 'post',
    text: 'Every day it\'s something.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1542072103096070146',
    created: 1656493614000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/TerpsMLax" rel="noopener noreferrer" target="_blank">@TerpsMLax</a> He\'s already killing it in the PLL too!<br><br>In reply to: <a href="https://x.com/TerpsMLax/status/1541868910844297216" rel="noopener noreferrer" target="_blank">@TerpsMLax</a> <span class="status">1541868910844297216</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1541940366152126465',
    created: 1656462205000,
    type: 'quote',
    text: 'Saying what needs to be said as clearly as possible. Got my vote.<br><br>Quoting: <a href="https://x.com/DaveYoungCO/status/1540412956164251649" rel="noopener noreferrer" target="_blank">@DaveYoungCO</a> <span class="status">1540412956164251649</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1541447549281632256',
    created: 1656344709000,
    type: 'quote',
    text: 'Absolutely captures the helplessness renters feel right now, and I know that an overwhelming number of people feel it far worse than me.<br><br>Quoting: <a href="https://x.com/LastWeekTonight/status/1538915184085028864" rel="noopener noreferrer" target="_blank">@LastWeekTonight</a> <span class="status">1538915184085028864</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1541429020545323008',
    created: 1656340291000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/domdorn" rel="noopener noreferrer" target="_blank">@domdorn</a> If memory serves me correctly, it used to be you couldn\'t touch anywhere inside the crease ever. But they changed the rule to let players gamble more.<br><br>In reply to: <a href="#1541428424832561152">1541428424832561152</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1541428424832561152',
    created: 1656340149000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/domdorn" rel="noopener noreferrer" target="_blank">@domdorn</a> The rule is that as long as the ball crosses the goal line before you touch inside the crease (circle), it\'s legal. Otherwise, it\'s a crease violation.<br><br>In reply to: <a href="https://x.com/domdorn/status/1541395897501859840" rel="noopener noreferrer" target="_blank">@domdorn</a> <span class="status">1541395897501859840</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1541307141494956032',
    created: 1656311233000,
    type: 'post',
    text: 'To add insult to injury, I have jury summons Monday morning at 8AM. Inside a judicial center is precisely the last place on the planet I want to be right now.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1541265520472731649',
    created: 1656301310000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/velodiesel" rel="noopener noreferrer" target="_blank">@velodiesel</a> <a class="mention" href="https://x.com/Avalanche" rel="noopener noreferrer" target="_blank">@Avalanche</a> <a class="mention" href="https://x.com/MammothLax" rel="noopener noreferrer" target="_blank">@MammothLax</a> Truth.<br><br>In reply to: <a href="https://x.com/velodiesel/status/1541264211283689474" rel="noopener noreferrer" target="_blank">@velodiesel</a> <span class="status">1541264211283689474</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1541263092423598081',
    created: 1656300731000,
    type: 'post',
    text: 'Two national championships in as many weeks. And both won on the road. Not a bad showing for Colorado! <a class="mention" href="https://x.com/Avalanche" rel="noopener noreferrer" target="_blank">@Avalanche</a> <a class="mention" href="https://x.com/MammothLax" rel="noopener noreferrer" target="_blank">@MammothLax</a>',
    likes: 13,
    retweets: 1
  },
  {
    id: '1541232229048676352',
    created: 1656293372000,
    type: 'post',
    text: 'What makes this shot so incredible is that he wasn\'t moving at the time he went to take the shot. That\'s what made it so surprising, especially to the goalie. It just came out of nowhere.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1541231579036430336',
    created: 1656293217000,
    type: 'quote',
    text: 'This has to be the most unbelievable shot I\'ve ever seen in my time watching lacrosse. Absolutely unreal!<br><br>Quoting: <a href="https://x.com/PremierLacrosse/status/1540861462935212033" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1540861462935212033</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1541161412914868224',
    created: 1656276488000,
    type: 'post',
    text: 'I was very glad to hear that GitHub sponsored at least one Asciidoctor maintainer (<a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a>) as part of Maintainer month. Thanks GitHub for recognizing the hard work this community puts in day in and day out to make Asciidoctor a great tool! <a href="https://github.blog/2022-06-24-thank-you-to-our-maintainers/" rel="noopener noreferrer" target="_blank">github.blog/2022-06-24-thank-you-to-our-maintainers/</a>',
    likes: 17,
    retweets: 4
  },
  {
    id: '1541126730953371648',
    created: 1656268220000,
    type: 'post',
    text: 'I firmly believe that a woman has a legal right to choose to have an abortion, or any decision about her own body for that matter. I support that right unequivocally.',
    likes: 23,
    retweets: 3
  },
  {
    id: '1541125450134462464',
    created: 1656267914000,
    type: 'post',
    text: 'Her body. Her choice.',
    likes: 10,
    retweets: 0
  },
  {
    id: '1541125392496349184',
    created: 1656267900000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tajpulo" rel="noopener noreferrer" target="_blank">@tajpulo</a> Totally agreed.<br><br>In reply to: <a href="https://x.com/tajpulo/status/1540972880107896834" rel="noopener noreferrer" target="_blank">@tajpulo</a> <span class="status">1540972880107896834</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1540771844927545344',
    created: 1656183608000,
    type: 'post',
    text: 'I have been told that if you add "noindex" to the /index.html file which itself is a meta redirect to another page, Google will not index the *entire* site. But I see no proof of this in the documentation. Can anyone provide more insight here? <a href="https://developers.google.com/search/docs/advanced/crawling/block-indexing" rel="noopener noreferrer" target="_blank">developers.google.com/search/docs/advanced/crawling/block-indexing</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1540759038853427200',
    created: 1656180555000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MStine" rel="noopener noreferrer" target="_blank">@MStine</a> Have an awesome day! 🍾<br><br>In reply to: @mstine <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1540757345956790273',
    created: 1656180151000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MStine" rel="noopener noreferrer" target="_blank">@MStine</a> What!?! Hell yes! Congratulations, sir! And also, #fuckSCOTUS<br><br>In reply to: @mstine <span class="status">&lt;deleted&gt;</span>',
    likes: 1,
    retweets: 0,
    tags: ['fuckscotus']
  },
  {
    id: '1540756914165805056',
    created: 1656180048000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dietz_meredith" rel="noopener noreferrer" target="_blank">@dietz_meredith</a> 💯 Not that I won\'t vote. I certainly will. But vote is what we do (and already have done). Now they need to stop stalling and actually do what we sent them there to do.<br><br>In reply to: <a href="https://x.com/dietz_meredith/status/1540365803148681217" rel="noopener noreferrer" target="_blank">@dietz_meredith</a> <span class="status">1540365803148681217</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1540547775863042049',
    created: 1656130186000,
    type: 'post',
    text: 'I hate dryer sheets as much as a hate cigarette smoke. Gag.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1540464699468238848',
    created: 1656110379000,
    type: 'post',
    text: 'Check on your friends. They may need your support today.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1540440121824096257',
    created: 1656104519000,
    type: 'quote',
    text: 'It\'s about control, plain and simple. There\'s no morality here. Just brazen supremacy. That\'s all it is.<br><br>Quoting: <a href="https://x.com/rbowen/status/1540423115641085953" rel="noopener noreferrer" target="_blank">@rbowen</a> <span class="status">1540423115641085953</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1540436473605812226',
    created: 1656103649000,
    type: 'post',
    text: 'Can anyone provide proof that Google will not index an entire site if /index.html is a meta-refresh redirect page that contains the noindex meta tag? Is it really not smart enough to follow the redirect and see that the target page is indexable? I find this very hard to believe.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1540237595723984896',
    created: 1656056233000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/klaritidotcom" rel="noopener noreferrer" target="_blank">@klaritidotcom</a> I don\'t use a library for the release process. The scripts are in the repository. And they are written for Linux, which the release process uses.<br><br>In reply to: <a href="https://x.com/klaritidotcom/status/1540229257112190976" rel="noopener noreferrer" target="_blank">@klaritidotcom</a> <span class="status">1540229257112190976</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1540207429308207104',
    created: 1656049041000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GeoffreyDeSmet" rel="noopener noreferrer" target="_blank">@GeoffreyDeSmet</a> Of course. <a href="https://github.com/asciidoctor/asciidoctor-pdf/blob/main/.github/workflows/release.yml" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-pdf/blob/main/.github/workflows/release.yml</a> and <a href="https://github.com/asciidoctor/asciidoctor-pdf/blob/main/release.sh" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-pdf/blob/main/release.sh</a><br><br>In reply to: <a href="https://x.com/GeoffreyDeSmet/status/1540200963721596928" rel="noopener noreferrer" target="_blank">@GeoffreyDeSmet</a> <span class="status">1540200963721596928</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1540182508850974721',
    created: 1656043099000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mairin" rel="noopener noreferrer" target="_blank">@mairin</a> I\'m a fan.<br><br>In reply to: <a href="https://x.com/mairin/status/1539773214565535744" rel="noopener noreferrer" target="_blank">@mairin</a> <span class="status">1539773214565535744</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1540107407438753793',
    created: 1656025194000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/obilodeau" rel="noopener noreferrer" target="_blank">@obilodeau</a> 💯🍻<br><br>In reply to: <a href="https://x.com/obilodeau/status/1540105669596119046" rel="noopener noreferrer" target="_blank">@obilodeau</a> <span class="status">1540105669596119046</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1540097065656602625',
    created: 1656022728000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fwilhe" rel="noopener noreferrer" target="_blank">@fwilhe</a> Yep. And it works splendidly. I abandon minor branches after 1 or 2 releases and turn off CI. I only keep the minor branches for backporting while I\'m working on the next major or minor.<br><br>In reply to: <a href="https://x.com/fwilhe/status/1540092298796621827" rel="noopener noreferrer" target="_blank">@fwilhe</a> <span class="status">1540092298796621827</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1540086260110204929',
    created: 1656020152000,
    type: 'post',
    text: 'With the NLL championship decided, the Chaos get the bulk of their starters back from the Buffalo Bandits. Who\'s the first goalie they\'ll face playing against the Waterdogs in the PLL? Dillon Ward from the Colorado Mammoth 🦣, who just defeated them for the NLL title. Rematch! 🥍',
    likes: 0,
    retweets: 0
  },
  {
    id: '1540084052270522369',
    created: 1656019626000,
    type: 'post',
    text: 'Here\'s my release process for Asciidoctor PDF. That\'s it. That\'s all I have to do.',
    photos: ['<div class="item"><img class="photo" src="media/1540084052270522369.jpg"></div>'],
    likes: 57,
    retweets: 1
  },
  {
    id: '1540066762032443392',
    created: 1656015503000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> It\'s definitely worth an article if you\'re looking for topics to cover.<br><br>In reply to: <a href="https://x.com/settermjd/status/1540065637543944194" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1540065637543944194</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1540062872545308672',
    created: 1656014576000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> I\'m very exited with what <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> &amp; friends are doing with <a href="http://jbang.dev" rel="noopener noreferrer" target="_blank">jbang.dev</a>. I think it has the potential of making Java incredibly approachable as a scripting language. And that\'s great...because it\'s great to have options.<br><br>In reply to: <a href="https://x.com/settermjd/status/1540062115704233989" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1540062115704233989</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1540062283791241216',
    created: 1656014436000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> I also use Node.js/JavaScript a lot because of Antora. It\'s nearly identical to Ruby, with some things that are better, some things that are missing. My favorite JavaScript feature is the Proxy. It\'s even better than Ruby at patching existing objects. Super fun and useful.<br><br>In reply to: <a href="#1540061803865993216">1540061803865993216</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1540061803865993216',
    created: 1656014321000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> Every since I first tried Ruby, it was clear the language makes translating pseudo-code into working code easier than any language I know. It has a very deep stdlib, simple constructs, and the ability to override just about anything. If I need something quick, I reach for Ruby.<br><br>In reply to: <a href="https://x.com/settermjd/status/1540060364603359232" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1540060364603359232</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1540061362193256448',
    created: 1656014216000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> I love languages in general. I like making a computer do what I want to do. For that, I use the language that provides the least resistance. I love scripting languages because I can run the program directly, without needing a build tool or IDE. Just my mind and the interpreter.<br><br>In reply to: <a href="https://x.com/settermjd/status/1540060416931495937" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1540060416931495937</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1540060679301767169',
    created: 1656014053000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> Yep, I started out in PHP because then it was either that or Perl to write webapps (and I actually did both). But PHP didn\'t require cgi-bin and it allowed you to do HTML templates right out of the box. So it was the clear choice.<br><br>In reply to: <a href="https://x.com/settermjd/status/1540060199578468352" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1540060199578468352</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1540040268715397120',
    created: 1656009187000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> I\'m biased, but even from the very first time I fiddled with Ruby (then a PHP programmer), I\'ve always found it to bring me joy. I\'m glad to hear you had fun as well.<br><br>In reply to: <a href="https://x.com/settermjd/status/1539928235546394624" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1539928235546394624</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1539899682096390144',
    created: 1655975668000,
    type: 'post',
    text: 'Looks like JRuby 9.3 on windows-latest is currently broken in GitHub Actions. My CI builds are breaking across the organization. <a href="https://github.com/asciidoctor/asciidoctor-reducer/runs/7016067507?check_suite_focus=true" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-reducer/runs/7016067507?check_suite_focus=true</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1539857286130061312',
    created: 1655965560000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/chacon" rel="noopener noreferrer" target="_blank">@chacon</a> What a dad!<br><br>In reply to: <a href="https://x.com/chacon/status/1539820510628007936" rel="noopener noreferrer" target="_blank">@chacon</a> <span class="status">1539820510628007936</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1539717801350287361',
    created: 1655932305000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/POTUS" rel="noopener noreferrer" target="_blank">@POTUS</a> You are fundamentally not understanding why gas prices are high. They are high because of greedy CEOs. Full stop. (We also need to eliminate our reliance on fossil fuel post-haste so we can get out of this vicious cycle).<br><br>In reply to: <a href="https://x.com/POTUS/status/1539704785162895360" rel="noopener noreferrer" target="_blank">@POTUS</a> <span class="status">1539704785162895360</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1539680910236299265',
    created: 1655923509000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/OJourdan" rel="noopener noreferrer" target="_blank">@OJourdan</a> Great choice!<br><br>In reply to: <a href="https://x.com/OJourdan/status/1539594924760383488" rel="noopener noreferrer" target="_blank">@OJourdan</a> <span class="status">1539594924760383488</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1539185068232437760',
    created: 1655805291000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLRedwoods" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> Congrats on a game well played. Keep that ball moving and I\'d be willing to bet there will be many more where that came from.<br><br>In reply to: <a href="https://x.com/PLLRedwoods/status/1538276214774845441" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> <span class="status">1538276214774845441</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1538988119537512448',
    created: 1655758335000,
    type: 'post',
    text: 'Part of that is also recognizing that slavery still exists today, disguised in our prison system, and not standing by while it continues to happen.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1538988117645852673',
    created: 1655758334000,
    type: 'post',
    text: 'Just because we\'re ashamed that slavery existed is no excuse to forget it. Rather, we must remember it to acknowledge the damage it caused, repair the long-term disparity it left behind, and reaffirm our commitment to end human rights violations in all its forms. #juneteenth2022',
    likes: 2,
    retweets: 1,
    tags: ['juneteenth2022']
  },
  {
    id: '1538714097671213057',
    created: 1655693003000,
    type: 'post',
    text: 'The <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> is the most excited I\'ve been about sports for as long as I can remember. No matter who\'s playing, it\'s guaranteed to be a wild ride. Blink and you\'ll miss something.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1538640029920002049',
    created: 1655675344000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/domdorn" rel="noopener noreferrer" target="_blank">@domdorn</a> <a class="mention" href="https://x.com/sam_brannen" rel="noopener noreferrer" target="_blank">@sam_brannen</a> Yes, if hyphens are enabled on the document.<br><br>In reply to: <a href="https://x.com/domdorn/status/1538621163349454849" rel="noopener noreferrer" target="_blank">@domdorn</a> <span class="status">1538621163349454849</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1538612913459453952',
    created: 1655668879000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sam_brannen" rel="noopener noreferrer" target="_blank">@sam_brannen</a> Understood. It turns out, though, this topic is covered in the docs. See <a href="https://docs.asciidoctor.org/pdf-converter/latest/autowidth-tables/" rel="noopener noreferrer" target="_blank">docs.asciidoctor.org/pdf-converter/latest/autowidth-tables/</a><br><br>I\'m always happy to help guide the way though.<br><br>In reply to: <a href="https://x.com/sam_brannen/status/1538527187334307842" rel="noopener noreferrer" target="_blank">@sam_brannen</a> <span class="status">1538527187334307842</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1537886605142200321',
    created: 1655495713000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sam_brannen" rel="noopener noreferrer" target="_blank">@sam_brannen</a> ...the short answer is, for good reason ;)<br><br>In reply to: <a href="https://x.com/sam_brannen/status/1537775333931470848" rel="noopener noreferrer" target="_blank">@sam_brannen</a> <span class="status">1537775333931470848</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1537874294184636426',
    created: 1655492778000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/b0rk" rel="noopener noreferrer" target="_blank">@b0rk</a> GitLab offers this feature. You can toggle just about every functional area of a project hosted there, including issues and MRs.<br><br>In reply to: <a href="https://x.com/b0rk/status/1537835478711508996" rel="noopener noreferrer" target="_blank">@b0rk</a> <span class="status">1537835478711508996</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1537863553448677376',
    created: 1655490217000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sam_brannen" rel="noopener noreferrer" target="_blank">@sam_brannen</a> I would prefer if you directed questions like this to the project chat at <a href="https://chat.asciidoctor.org" rel="noopener noreferrer" target="_blank">chat.asciidoctor.org</a>.<br><br>In reply to: <a href="#1537863472234409984">1537863472234409984</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1537863472234409984',
    created: 1655490198000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sam_brannen" rel="noopener noreferrer" target="_blank">@sam_brannen</a> Another option is to enable hyphens on the document, which will analyze words for better break opportunities.<br><br>In reply to: <a href="#1537863360040972288">1537863360040972288</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1537863360040972288',
    created: 1655490171000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sam_brannen" rel="noopener noreferrer" target="_blank">@sam_brannen</a> This happens because the word is so long that it does not fit in the allocated space and there\'s no where else to break. You either need to widen the column or offer the converter a break opportunity by inserting {zwsp} or &amp;#173;<br><br>Typesetting text is a very complicated matter.<br><br>In reply to: <a href="https://x.com/sam_brannen/status/1537775333931470848" rel="noopener noreferrer" target="_blank">@sam_brannen</a> <span class="status">1537775333931470848</span>',
    likes: 3,
    retweets: 1
  },
  {
    id: '1537711419566673920',
    created: 1655453946000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/myfear" rel="noopener noreferrer" target="_blank">@myfear</a> Absolutely the same here.<br><br>In reply to: <a href="https://x.com/myfear/status/1537687462822264832" rel="noopener noreferrer" target="_blank">@myfear</a> <span class="status">1537687462822264832</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1537651183765950465',
    created: 1655439585000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> No one\'s doubting the technical benefits of the platform. It\'s the fact that they have proven to be untrustworthy.<br><br>In reply to: <a href="#1537649500805689344">1537649500805689344</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1537649500805689344',
    created: 1655439183000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> Just wait until you get the bill.<br><br>In reply to: <a href="https://x.com/mraible/status/1537611840582209537" rel="noopener noreferrer" target="_blank">@mraible</a> <span class="status">1537611840582209537</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1537610598703239169',
    created: 1655429908000,
    type: 'post',
    text: 'All my clients and numerous prospective clients I\'ve spoken to are sprinting away from Netlify because of their unscrupulous new billing practices. Several of those have shared that they\'re limiting who can commit to repositories until they migrate away. This is damning.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1537350276289789952',
    created: 1655367843000,
    type: 'post',
    text: 'I just learned today that GitHub Actions only runs scheduled workflows on the default branch. 🤔',
    likes: 0,
    retweets: 0
  },
  {
    id: '1537176464230998016',
    created: 1655326403000,
    type: 'post',
    text: 'If you think we need to "balance the budget", then you don\'t understand the first thing about how government spending works and you\'ve been conned into sacrificing your life to benefit the wealthy.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1537014456617607168',
    created: 1655287777000,
    type: 'quote',
    text: '💯 Testing is life.<br><br>Quoting: <a href="https://x.com/techleadjournal/status/1536913927258394624" rel="noopener noreferrer" target="_blank">@techleadjournal</a> <span class="status">1536913927258394624</span>',
    likes: 6,
    retweets: 2
  },
  {
    id: '1536954838168182784',
    created: 1655273563000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RoganJoshua" rel="noopener noreferrer" target="_blank">@RoganJoshua</a> <a class="mention" href="https://x.com/ilopmar" rel="noopener noreferrer" target="_blank">@ilopmar</a> I plan to.<br><br>In reply to: @roganjoshua <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1536783265050218496',
    created: 1655232657000,
    type: 'post',
    text: 'I love when people follow up on an issue to say they tested the fix...hopefully to say it works.<br><br>(That\'s the only type of comment I see as appropriate on a closed issue).',
    likes: 5,
    retweets: 0
  },
  {
    id: '1536626841028464640',
    created: 1655195362000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CedricChampeau" rel="noopener noreferrer" target="_blank">@CedricChampeau</a> <a class="mention" href="https://x.com/ilopmar" rel="noopener noreferrer" target="_blank">@ilopmar</a> I do appreciate the feedback. Very good to know. The reason Zoom works there is likely because Zoom only shares properly with X, not Wayland. So it works more by chance since that\'s not where the deficiency is in Zoom. They should be capable of supporting Wayland correctly.<br><br>In reply to: <a href="https://x.com/CedricChampeau/status/1536625869560635392" rel="noopener noreferrer" target="_blank">@CedricChampeau</a> <span class="status">1536625869560635392</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1536624820753813504',
    created: 1655194881000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ilopmar" rel="noopener noreferrer" target="_blank">@ilopmar</a> But are you using Wayland? To me, Wayland is a far superior display server than alternatives on Linux because it supports HiDPI correctly. It\'s not something I\'d give up just because Zoom is deficient. (Though I may use a second computer without it if I have to).<br><br>In reply to: <a href="https://x.com/ilopmar/status/1536622993803288576" rel="noopener noreferrer" target="_blank">@ilopmar</a> <span class="status">1536622993803288576</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1536621072631537664',
    created: 1655193987000,
    type: 'post',
    text: 'After a bunch of fiddling, I was able to use Zoom via WebRTC to share either a Chrome tab or the entire desktop. With several rounds of trial and error, it\'s possible. But I\'ll definitely be looking for other options now.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1536613066929889280',
    created: 1655192078000,
    type: 'post',
    text: 'It\'s just ridiculous to me that we wonder where else we can go with desktop computing when we\'re still failing at the most basic tasks that you need to perform as a professional. Can we get that right first?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1536612438170165250',
    created: 1655191928000,
    type: 'post',
    text: 'Even if we move to the browser, the share selection dialog in Chrome is a mess. You have to continually ask the participants "can you see my window now?" for about 5 minutes before it works...and don\'t dare touch it after that.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1536611151642914817',
    created: 1655191622000,
    type: 'post',
    text: 'I\'m particularly frustrated with Zoom, which apparently cannot share an application window under Wayland on Linux because the Zoom developers programmed it incorrectly and show no interest in fixing it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1536556440675942401',
    created: 1655178577000,
    type: 'quote',
    text: 'I\'m team beaver. Nature\'s revenge.<br><br>Quoting: <a href="https://x.com/netblocks/status/1536416663385546756" rel="noopener noreferrer" target="_blank">@netblocks</a> <span class="status">1536416663385546756</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1536505120179032070',
    created: 1655166342000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> If they can make us work through the weekend, then it\'s only fair that we can weekend during the work week.<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/1536504486759374849" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">1536504486759374849</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1536488698543489026',
    created: 1655162426000,
    type: 'post',
    text: 'For as much as we\'ve used video conferencing tools over the last several years, screen sharing is still such a nightmare. How difficult it is to configure which window you want to share? Apparently, impossible.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1536119472037146625',
    created: 1655074396000,
    type: 'post',
    text: 'Another exciting weekend of lacrosse. The Whipsnakes / Waterdogs game was wild, but even more so was the Mammoth / Bandits game. So intense! 🥍',
    likes: 0,
    retweets: 0
  },
  {
    id: '1535835864382681089',
    created: 1655006779000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> What. A. Game!<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1535819884004794368" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1535819884004794368</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1535803355645325312',
    created: 1654999028000,
    type: 'post',
    text: 'Ahhhhhh.',
    photos: ['<div class="item"><img class="photo" src="media/1535803355645325312.jpg"></div>'],
    likes: 2,
    retweets: 0
  },
  {
    id: '1535760106196848640',
    created: 1654988717000,
    type: 'post',
    text: 'Thank you for fixing the stream.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1535749547950886912',
    created: 1654986199000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/espn" rel="noopener noreferrer" target="_blank">@espn</a> Thank you!<br><br>In reply to: <a href="#1535748839679791104">1535748839679791104</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1535749100829671424',
    created: 1654986093000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/espn" rel="noopener noreferrer" target="_blank">@espn</a> Can you please start the feed of the Atlas vs Cannons game on espn+?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1535748839679791104',
    created: 1654986030000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> Can you please start the event on ESPN+?<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1535742392728440832" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1535742392728440832</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1535435728523448320',
    created: 1654911379000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/michaelminella" rel="noopener noreferrer" target="_blank">@michaelminella</a> Magical!<br><br>In reply to: <a href="https://x.com/michaelminella/status/1535430289182314496" rel="noopener noreferrer" target="_blank">@michaelminella</a> <span class="status">1535430289182314496</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1535433350764105731',
    created: 1654910812000,
    type: 'quote',
    text: 'Damn the United States is disfunctional. We can\'t even manage to field professional athletes.<br><br>Quoting: <a href="https://x.com/danarestia/status/1534520501502091264" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1534520501502091264</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1535406501912711168',
    created: 1654904411000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> I\'ve been waiting for this all week. And this is just game 1 of 4. Let\'s go!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1535099093738893312',
    created: 1654831119000,
    type: 'post',
    text: 'It finally feels like summer. 😎',
    likes: 0,
    retweets: 0
  },
  {
    id: '1534974710420627457',
    created: 1654801464000,
    type: 'post',
    text: 'The lack of any concrete action by Biden following the Uvalde school shooting in which children were not only killed, but mutilated, has convinced me that it\'s time for him to resign. This is a defining moment and he has utterly failed to step up. He\'s not a leader.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1534972523950899200',
    created: 1654800942000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/POTUS" rel="noopener noreferrer" target="_blank">@POTUS</a> You hold the most powerful position in the world. Fucking DO SOMETHING to protect the children of this country. Issue an Executive Order mandating strict gun control now!!! Otherwise, resign from office.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1534971411021692928',
    created: 1654800677000,
    type: 'quote',
    text: 'This interview enrages me. Biden comes across as weak and callous. Among too many others, 19 children and 2 teachers were slaughtered, and he thinks it\'s a good time to back off and laugh about it. Sickening. Just sickening. 🤮<br><br>Quoting: <a href="https://x.com/jimmykimmel/status/1534745624851099649" rel="noopener noreferrer" target="_blank">@jimmykimmel</a> <span class="status">1534745624851099649</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1534686048764039173',
    created: 1654732641000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> And a total side benefit is that I have very few allergy problems now that I have a mask. It feels nice to not struggle to breathe on a nice day.<br><br>In reply to: <a href="#1534685583796187136">1534685583796187136</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1534685583796187136',
    created: 1654732530000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> And I absolutely love my mask now that I got one that\'s actually comfortable. Here\'s what I use: <a href="https://masklab.us/collections/the-minimal-collection/products/minimal-2-0-the-jetsetter-charcoal-adult-korean-style-respirator-2-0-10-pack" rel="noopener noreferrer" target="_blank">masklab.us/collections/the-minimal-collection/products/minimal-2-0-the-jetsetter-charcoal-adult-korean-style-respirator-2-0-10-pack</a><br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1534684693223706625" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1534684693223706625</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1534682780000522245',
    created: 1654731862000,
    type: 'quote',
    text: 'Having been through COVID myself, I agree. I\'ve never been so scared about being sick in my life. The waiting to find out if your body+vaccine is going to have what it takes to fight it off is harrowing. That\'s why I continue to mask up without caring what others think.<br><br>Quoting: <a href="https://x.com/Sharat_Chander/status/1534620634331770881" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1534620634331770881</span>',
    likes: 8,
    retweets: 1
  },
  {
    id: '1534681288669442048',
    created: 1654731506000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> I\'m really, really glad to hear that you\'ve made it to the other side of this mountain of mental and physical misery. While we can\'t change the past, we can be grateful that we\'re fortunate enough to see many more 🌄🌅.<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1534620657664765952" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1534620657664765952</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1533761564347465728',
    created: 1654512227000,
    type: 'post',
    text: 'I only have one request. No more punches. That\'s not what this game is about. But I know you know that already, so I\'ve got faith.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1533760963677016064',
    created: 1654512084000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> I thoroughly enjoyed the season opener. Great matchups. Superb 🥍. The experience made the start of summer something special. I\'m really looking forward for what\'s to come. You\'ve got a great thing going. First time I\'ve been excited about sports in ages.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1533536428608524288',
    created: 1654458550000,
    type: 'reply',
    text: '@sctrcdr @serverlessgirl <a class="mention" href="https://x.com/simonbrown" rel="noopener noreferrer" target="_blank">@simonbrown</a> Kroki is the creation of <a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a>, the maintainer of Asciidoctor.js and all-around star contributor to Asciidoctor. We\'re so fortunate to have his incredible talent in our corner.<br><br>In reply to: @sctrcdr <span class="status">&lt;deleted&gt;</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1533533675366731776',
    created: 1654457894000,
    type: 'reply',
    text: '@serverlessgirl @sctrcdr <a class="mention" href="https://x.com/simonbrown" rel="noopener noreferrer" target="_blank">@simonbrown</a> There are actually two diagram integrations in the Asciidoctor ecosystem, Asciidoctor Diagram and Asciidoctor Kroki. The first calls the diagram tools directly. The second delegates to a Kroki serverto manage and invoke the diagram tools. So you have options!<br><br>In reply to: @serverlessgirl <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1533529053470154752',
    created: 1654456792000,
    type: 'post',
    text: 'I add the disclaimer to make it clear that I haven\'t seen this come from the tech community in my timeline. That doesn\'t mean it couldn\'t happen...',
    likes: 0,
    retweets: 0
  },
  {
    id: '1533527841786146816',
    created: 1654456503000,
    type: 'post',
    text: 'This tweet refers to incidents outside the tech community...<br><br>I can accept and welcome productive criticism backed by a well-reasoned, sound argument. But if a person resorts to name calling, that\'s an instant block. You aren\'t worth anyone\'s time if you behave like that.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1533248815708311553',
    created: 1654389978000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> 💯<br><br>In reply to: <a href="https://x.com/danarestia/status/1533152107397890050" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1533152107397890050</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1533226256292106241',
    created: 1654384600000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JordanChariton" rel="noopener noreferrer" target="_blank">@JordanChariton</a> I\'m really disheartened that Dore has lost the plot (yet so encouraged that you have not). I really learned a lot from Dore when he was fighting the good fight. But those days have long since passed. Keep up the great work. It really matters.<br><br>In reply to: <a href="https://x.com/JordanChariton/status/1533221201564270592" rel="noopener noreferrer" target="_blank">@JordanChariton</a> <span class="status">1533221201564270592</span>',
    likes: 6,
    retweets: 2
  },
  {
    id: '1533215887284748288',
    created: 1654382127000,
    type: 'post',
    text: 'Now I just need to decide who *my* team is. Though there\'s something to be said for just rooting for the sport and a good game.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1533215399872974848',
    created: 1654382011000,
    type: 'post',
    text: 'This is truly a dream come true watching <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a>. THIS is lacrosse. But even better. It\'s all-star lacrosse.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1533198546819698688',
    created: 1654377993000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/9200feet" rel="noopener noreferrer" target="_blank">@9200feet</a> <a class="mention" href="https://x.com/AthleticBrewing" rel="noopener noreferrer" target="_blank">@AthleticBrewing</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> Awesome! Definitely give their pilot program a try too (online orders). Right now, Safe Stream IPA is among the best IMO. They are revolutionizing NA beer brewing.<br><br>In reply to: <a href="https://x.com/9200feet/status/1533168918541443072" rel="noopener noreferrer" target="_blank">@9200feet</a> <span class="status">1533168918541443072</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1533166580363841536',
    created: 1654370372000,
    type: 'post',
    text: 'I just noticed that <a class="mention" href="https://x.com/AthleticBrewing" rel="noopener noreferrer" target="_blank">@AthleticBrewing</a> is advertising at the season opener of the <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> league! What a perfect matchup. I think I\'ll grab one from the fridge now. 🍺',
    likes: 0,
    retweets: 0
  },
  {
    id: '1532983809968181248',
    created: 1654326796000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dashorst" rel="noopener noreferrer" target="_blank">@dashorst</a> That IS a good day. ☀️<br><br>In reply to: <a href="https://x.com/dashorst/status/1532977654495846405" rel="noopener noreferrer" target="_blank">@dashorst</a> <span class="status">1532977654495846405</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1532823725418065920',
    created: 1654288629000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/julbai" rel="noopener noreferrer" target="_blank">@julbai</a> <a class="mention" href="https://x.com/maciejwalkowiak" rel="noopener noreferrer" target="_blank">@maciejwalkowiak</a> <a class="mention" href="https://x.com/jreleaser" rel="noopener noreferrer" target="_blank">@jreleaser</a> I prefer a curated changelog, but to each their own.<br><br>In reply to: <a href="https://x.com/julbai/status/1532812333860397061" rel="noopener noreferrer" target="_blank">@julbai</a> <span class="status">1532812333860397061</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1532808512170258433',
    created: 1654285002000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maciejwalkowiak" rel="noopener noreferrer" target="_blank">@maciejwalkowiak</a> I maintain a CHANGELOG as part of each commit or PR. Then I generate the release notes from the top section of the CHANGELOG. See <a href="https://github.com/asciidoctor/asciidoctor-pdf/blob/main/tasks/release-notes.rb" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-pdf/blob/main/tasks/release-notes.rb</a><br><br>In reply to: <a href="https://x.com/maciejwalkowiak/status/1532797613078106134" rel="noopener noreferrer" target="_blank">@maciejwalkowiak</a> <span class="status">1532797613078106134</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1532794795407527938',
    created: 1654281731000,
    type: 'post',
    text: 'For me, the psychological impact of having a fully automated release is undeniable. Since automating the Asciidoctor PDF release (at the push of a button), I\'ve pushed out a dozen releases in just over a month. Never before have I been able to release software so quickly.',
    likes: 49,
    retweets: 5
  },
  {
    id: '1532792731809984513',
    created: 1654281239000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/styx_hcr" rel="noopener noreferrer" target="_blank">@styx_hcr</a> @serverlessgirl <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> I cannot overstate what a profound impact this has had on me as a developer. (Yes, I\'m an author of Antora, but also an author of technical docs). If the docs are with the code, then I write docs when I write code. Simple as that.<br><br>In reply to: <a href="https://x.com/styx_hcr/status/1532770905205743618" rel="noopener noreferrer" target="_blank">@styx_hcr</a> <span class="status">1532770905205743618</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1532507053322694656',
    created: 1654213128000,
    type: 'reply',
    text: '@serverlessgirl I only just started using it this week too! And I agree with you, it\'s a beautiful tool to have handy.<br><br>In reply to: @serverlessgirl <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1532486695907602432',
    created: 1654208275000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/charlesspray" rel="noopener noreferrer" target="_blank">@charlesspray</a> Precisely.<br><br>In reply to: <a href="https://x.com/charlesspray/status/1532486325856882696" rel="noopener noreferrer" target="_blank">@charlesspray</a> <span class="status">1532486325856882696</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1532486587065733121',
    created: 1654208249000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/charlesspray" rel="noopener noreferrer" target="_blank">@charlesspray</a> I happen to think that limiting the length of the commit message subject is arbitrary and I refuse to be constrained by that in my own projects because it strips away vital context from the log. But that\'s a separate issue from the title of the PR.<br><br>In reply to: <a href="#1532485879792431104">1532485879792431104</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1532485879792431104',
    created: 1654208080000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/charlesspray" rel="noopener noreferrer" target="_blank">@charlesspray</a> I get that there\'s a push to conform to a convention. I just think this is the totally wrong way to go about it. Making the text turn red would be a more effective way of communicating the overrun without forcing your hand.<br><br>In reply to: <a href="https://x.com/charlesspray/status/1532484983838986256" rel="noopener noreferrer" target="_blank">@charlesspray</a> <span class="status">1532484983838986256</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1532483526083301377',
    created: 1654207519000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/charlesspray" rel="noopener noreferrer" target="_blank">@charlesspray</a> Actually, I did say something about a commit. 😳 But my point was that I\'m talking about the title of the PR being cut off, which just makes the PR sloppy and something I have to then fix manually.<br><br>In reply to: <a href="#1532419553442947072">1532419553442947072</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1532464257559715840',
    created: 1654202925000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aheritier" rel="noopener noreferrer" target="_blank">@aheritier</a> <a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Same.<br><br>In reply to: <a href="https://x.com/aheritier/status/1532463898141532160" rel="noopener noreferrer" target="_blank">@aheritier</a> <span class="status">1532463898141532160</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1532453002254045184',
    created: 1654200241000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/charlesspray" rel="noopener noreferrer" target="_blank">@charlesspray</a> I strongly disagree with what GitHub is doing as it slows down the contribution workflow. That\'s why I wrote the tweet that I wrote. I\'m not changing my mind on this. I work with dozens of PRs every single day. The behavior I\'m calling attention to is disruptive to that work.<br><br>In reply to: <a href="https://x.com/charlesspray/status/1532446772542554112" rel="noopener noreferrer" target="_blank">@charlesspray</a> <span class="status">1532446772542554112</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1532419553442947072',
    created: 1654192267000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/charlesspray" rel="noopener noreferrer" target="_blank">@charlesspray</a> But I never said anything about commits...<br><br>In reply to: <a href="https://x.com/charlesspray/status/1532330884388511744" rel="noopener noreferrer" target="_blank">@charlesspray</a> <span class="status">1532330884388511744</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1532298767386419200',
    created: 1654163469000,
    type: 'post',
    text: 'I\'m constantly annoyed that GitHub cuts off the title of my pull request and puts the remainder<br><br>...in the description. This feels super arbitrary to me. I want the title to be as I wrote it in the commit. I wrote it that way for a reason. I don\'t want it to be any shorter.',
    likes: 50,
    retweets: 4
  },
  {
    id: '1531917375099899904',
    created: 1654072538000,
    type: 'post',
    text: 'It\'s outrageous to me that Netlify is now charging per committer to a repository for a site deployed on Netlify.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1531913700516302849',
    created: 1654071662000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AmelieLens" rel="noopener noreferrer" target="_blank">@AmelieLens</a> Happy belated birthday! Thanks again and again for filling my home with such amazing and intense sounds. Your music makes my life more energetic.<br><br>In reply to: <a href="https://x.com/AmelieLens/status/1531909785532174336" rel="noopener noreferrer" target="_blank">@AmelieLens</a> <span class="status">1531909785532174336</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1531826984837296129',
    created: 1654050987000,
    type: 'post',
    text: 'School strike for gun control.',
    likes: 7,
    retweets: 0
  },
  {
    id: '1531822095721566208',
    created: 1654049822000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ashleymcnamara" rel="noopener noreferrer" target="_blank">@ashleymcnamara</a> I certainly feel it for sure, in both directions.<br><br>In reply to: <a href="https://x.com/ashleymcnamara/status/1531819957524107264" rel="noopener noreferrer" target="_blank">@ashleymcnamara</a> <span class="status">1531819957524107264</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1531568695264194560',
    created: 1653989406000,
    type: 'post',
    text: 'Oh, and now I know about the PLL (Premier Lacrosse League), which is the authentic professional lacrosse league I\'ve always wished for. I think I\'m going to make it out to see teams (and two of the Kirst brothers) when they come into town.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1531567978721882112',
    created: 1653989235000,
    type: 'post',
    text: 'Even though it was tough to see <a class="mention" href="https://x.com/CornellLacrosse" rel="noopener noreferrer" target="_blank">@CornellLacrosse</a> come up short in the end, they played a very exciting second half against Maryland that I won\'t soon forget. And the highlight of the day was learning about the Kirst brothers and the impact they\'ve had on the sport. Great story!',
    likes: 4,
    retweets: 0
  },
  {
    id: '1531359377122152449',
    created: 1653939501000,
    type: 'reply',
    text: '@BlossomBabalola <a class="mention" href="https://x.com/eddiejaoude" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> Companies will happily pay you to compromise your values, interests, and careers plans. Whatever you decide, just make sure it\'s what you want to do, and who you want to be.<br><br>In reply to: <a href="https://x.com/blossom_babs/status/1531344547573383168" rel="noopener noreferrer" target="_blank">@blossom_babs</a> <span class="status">1531344547573383168</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1531340153746116608',
    created: 1653934918000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/eddiejaoude" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> But also don\'t sell it to the highest bidder 😉<br><br>In reply to: <a href="https://x.com/eddiejaoude/status/1531337630767620098" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> <span class="status">1531337630767620098</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1531185991171514368',
    created: 1653898163000,
    type: 'quote',
    text: '🤣<br><br>It\'s a <a href="http://caniuse.com" rel="noopener noreferrer" target="_blank">caniuse.com</a> premonition! And yes you can!<br><br>Quoting: <a href="https://x.com/AaronToponce/status/1530321955370328064" rel="noopener noreferrer" target="_blank">@AaronToponce</a> <span class="status">1530321955370328064</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1531131563169107968',
    created: 1653885186000,
    type: 'post',
    text: 'The US flag was back to full mast today and that just enrages me. Nothing has fundamentally changed, and yet the gov\'t is like "we gave you a few days to acknowledge the murder of innocent children, time to go back to our regular schedule." The hell we are!',
    likes: 7,
    retweets: 0
  },
  {
    id: '1531121162507280384',
    created: 1653882706000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/catallman" rel="noopener noreferrer" target="_blank">@catallman</a> Now we\'re talkin\'<br><br>In reply to: <a href="https://x.com/catallman/status/1531090763588595712" rel="noopener noreferrer" target="_blank">@catallman</a> <span class="status">1531090763588595712</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1531072949666451456',
    created: 1653871211000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mmilinkov" rel="noopener noreferrer" target="_blank">@mmilinkov</a> Sarah confirmed that\'s an Osprey without even hesitating. Really nice photo, btw.<br><br>In reply to: <a href="https://x.com/mmilinkov/status/1531066298217771008" rel="noopener noreferrer" target="_blank">@mmilinkov</a> <span class="status">1531066298217771008</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1531018605176840192',
    created: 1653858255000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <a class="mention" href="https://x.com/mellemcwhirter" rel="noopener noreferrer" target="_blank">@mellemcwhirter</a> That beer choice however...could be improved 👅<br><br>In reply to: <a href="#1531018381700124672">1531018381700124672</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1531018381700124672',
    created: 1653858201000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <a class="mention" href="https://x.com/mellemcwhirter" rel="noopener noreferrer" target="_blank">@mellemcwhirter</a> It has taken me a lifetime to realize that the standards I hold myself to are too high. So I know what you mean. Sometimes I have to say to myself, that\'s good enough knowing it\'s probably a lot more than that already.<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/1531011077038919680" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">1531011077038919680</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1530838464417042432',
    created: 1653815306000,
    type: 'quote',
    text: 'Hard to know who to root for here. 🤔 Cornell, my alma mater, or Maryland, my former home state? What I can say is that Cornell has never lost a natty to Maryland, so that certainly adds to the drama. I\'m just happy to see a good matchup. Go Big Red! Go Terps!<br><br>Quoting: <a href="https://x.com/NCAALAX/status/1530728861826097152" rel="noopener noreferrer" target="_blank">@NCAALAX</a> <span class="status">1530728861826097152</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1530667135868882944',
    created: 1653774458000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CUBigRedGameday" rel="noopener noreferrer" target="_blank">@CUBigRedGameday</a> <a class="mention" href="https://x.com/NCAALAX" rel="noopener noreferrer" target="_blank">@NCAALAX</a> Go Big Red!<br><br>In reply to: <a href="https://x.com/CUBigRedGameday/status/1530646370415443968" rel="noopener noreferrer" target="_blank">@CUBigRedGameday</a> <span class="status">1530646370415443968</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1530614329468395523',
    created: 1653761868000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> <a class="mention" href="https://x.com/hsablonniere" rel="noopener noreferrer" target="_blank">@hsablonniere</a> @noel_mace <a class="mention" href="https://x.com/k33g_org" rel="noopener noreferrer" target="_blank">@k33g_org</a> @blindnet_io <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/github" rel="noopener noreferrer" target="_blank">@github</a> I\'d like to add that the reason it works so well on GitLab is largely thanks to <a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> and GitLab\'s openness.<br><br>In reply to: <a href="https://x.com/ggrossetie/status/1530575766450327555" rel="noopener noreferrer" target="_blank">@ggrossetie</a> <span class="status">1530575766450327555</span>',
    likes: 10,
    retweets: 1
  },
  {
    id: '1530448052644745216',
    created: 1653722224000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/leohepis" rel="noopener noreferrer" target="_blank">@leohepis</a> Yeah! I\'m so glad to hear that! 🎉<br><br>If you\'re looking for more advice, I invite you to stop by the project chat at <a href="https://chat.asciidoctor.org" rel="noopener noreferrer" target="_blank">chat.asciidoctor.org</a>.<br><br>In reply to: <a href="https://x.com/leohepis/status/1530444693724532736" rel="noopener noreferrer" target="_blank">@leohepis</a> <span class="status">1530444693724532736</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1530310170579902466',
    created: 1653689351000,
    type: 'reply',
    text: '@patrickbkoetter What I meant to say is that we want to defund the police because they are racist, ineffective, and dangerous. And we\'re learning more each day that the system and the people upholding it cannot be reformed. It must be abolished.<br><br>In reply to: <a href="#1530121684417597440">1530121684417597440</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1530121684417597440',
    created: 1653644412000,
    type: 'reply',
    text: '@patrickbkoetter Thank you for making me aware of my 🤦‍♂️.<br><br>In reply to: @patrickbkoetter <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1530094644356272129',
    created: 1653637965000,
    type: 'quote',
    text: 'This address is very well done. It\'s what at least 89% of Americans are thinking right now (I\'d like to think all), but stated in a way few of us could put together. Thanks for representing us, Jimmy.<br><br>Quoting: <a href="https://x.com/JimmyKimmelLive/status/1529669204864950272" rel="noopener noreferrer" target="_blank">@JimmyKimmelLive</a> <span class="status">1529669204864950272</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1530084572964106241',
    created: 1653635564000,
    type: 'reply',
    text: '@patrickbkoetter The more I read my initial post, the more I\'m like 🤦‍♂️. It didn\'t come out right.<br><br>In reply to: <a href="#1530078421534904320">1530078421534904320</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1530080364437176322',
    created: 1653634561000,
    type: 'reply',
    text: '@patrickbkoetter What I didn\'t mean to suggest was that we only defund the racist police. Not that at all. Rather, we\'re defunding the policing system because it\'s inherently racist and truly lacking in any sort of moral compass. (A judgement that\'s not just based solely on this event, but many).<br><br>In reply to: <a href="#1530078421534904320">1530078421534904320</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1530078864549941253',
    created: 1653634203000,
    type: 'reply',
    text: '@patrickbkoetter In other words, it\'s not that police became racist. The system was racist from day one, and sadly the people who serve fell into that system and refused to change it from the inside. So, in effect, they have become bad just by association, plus bad behavior layered on top of it.<br><br>In reply to: <a href="#1530078421534904320">1530078421534904320</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1530078421534904320',
    created: 1653634097000,
    type: 'reply',
    text: '@patrickbkoetter I actually agree with your clarification. I see now that my initial statement is easily be misread (at least compared to how it formed in my head). I\'m saying we should emphasize that we\'re defunding a sick system, one designed with a lack of moral standards from the start.<br><br>In reply to: @patrickbkoetter <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1529977826136432644',
    created: 1653610114000,
    type: 'post',
    text: 'I\'m not saying these people are bad people. The system is so fucked they don\'t even have a chance to be good people. But are you listening anyway? Can you even grasp that nuance? The police kill Black people and refuse to save kids. Those people deserve better. That\'s the point.',
    likes: 2,
    retweets: 1
  },
  {
    id: '1529974341584265216',
    created: 1653609283000,
    type: 'quote',
    text: 'We just need to start speaking more clearly like this. The system is rigged. The reality is unacceptable. This is not normal. This is not okay. There must be change. And now is not too soon. Demand better. Don\'t fall asleep.<br><br>Quoting: <a href="https://x.com/deenoonandraws/status/1529871167619842049" rel="noopener noreferrer" target="_blank">@deenoonandraws</a> <span class="status">1529871167619842049</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1529966331499466752',
    created: 1653607373000,
    type: 'quote',
    text: '#DefundThePolice should be defund the useless racist, police.<br><br>Quoting: <a href="https://x.com/QasimRashid/status/1529847362633576451" rel="noopener noreferrer" target="_blank">@QasimRashid</a> <span class="status">1529847362633576451</span>',
    likes: 1,
    retweets: 0,
    tags: ['defundthepolice']
  },
  {
    id: '1529956039738671106',
    created: 1653604919000,
    type: 'post',
    text: 'Humankind.<br><br>Be both.',
    likes: 6,
    retweets: 0
  },
  {
    id: '1529876897970069504',
    created: 1653586050000,
    type: 'post',
    text: 'Does anyone else get nervous when you\'re selecting an emoji and you come across one you would absolutely, definitely not want to use in this context? 😬',
    likes: 1,
    retweets: 0
  },
  {
    id: '1529736840999120897',
    created: 1653552658000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/leohepis" rel="noopener noreferrer" target="_blank">@leohepis</a> (Asciidoctor.load_file \'README.adoc\').find_by.each do |block|<br>  if block.context == :paragraph<br>    puts block.lines[0]<br>  elsif block.context == :section || block.title?<br>    puts block.title<br>  end<br>end<br><br>In reply to: <a href="https://x.com/leohepis/status/1529680089910169601" rel="noopener noreferrer" target="_blank">@leohepis</a> <span class="status">1529680089910169601</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1529591222498918400',
    created: 1653517940000,
    type: 'quote',
    text: 'The US Senate is a failed institution and this is what has to happen. Start signing anything that has been passed overwhelmingly in the House but was blocked by the Senate. Do that, or resign.<br><br>Quoting: <a href="https://x.com/POTUS/status/1529528850660610048" rel="noopener noreferrer" target="_blank">@POTUS</a> <span class="status">1529528850660610048</span>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1529571718267805697',
    created: 1653513290000,
    type: 'post',
    text: '...and yes, I know how to do it. That\'s not the point. On GitHub Actions, I just run `git push`. That\'s it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1529561585005473792',
    created: 1653510874000,
    type: 'post',
    text: 'GitLab needs to make it possible to push back to a repository during a CI job without having to go through hoops and over hurdles to configure it. It\'s absurd how much effort it takes to perform such a basic task.',
    likes: 3,
    retweets: 2
  },
  {
    id: '1529409001082867712',
    created: 1653474495000,
    type: 'reply',
    text: '@Avalanche1979 <a class="mention" href="https://x.com/bessbell" rel="noopener noreferrer" target="_blank">@bessbell</a> Through an equally insidious scheme that\'s been playing out over many years if not decades. It\'s the heart of the sickness in the US. (Often referenced as "big money in politics").<br><br>In reply to: @Avalanche1979 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1529391511493697536',
    created: 1653470325000,
    type: 'quote',
    text: 'Being able to release at the push of a button makes it so much easier to keep the releases coming...and to get bug fixes into your hands sooner.<br><br>Quoting: <a href="https://x.com/RubygemsN/status/1529385657843429376" rel="noopener noreferrer" target="_blank">@RubygemsN</a> <span class="status">1529385657843429376</span>',
    likes: 6,
    retweets: 0
  },
  {
    id: '1529308934548336640',
    created: 1653450637000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lanceball" rel="noopener noreferrer" target="_blank">@lanceball</a> murderers<br><br>In reply to: <a href="#1529308525385486336">1529308525385486336</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1529308525385486336',
    created: 1653450540000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lanceball" rel="noopener noreferrer" target="_blank">@lanceball</a> I honestly don\'t know. But I\'m done with anyone who continues to tolerate and condone the farce that is the defense of the 2nd amendment in the modern era. As far as I\'m concerned, they are murders.<br><br>In reply to: <a href="https://x.com/lanceball/status/1529300295213322240" rel="noopener noreferrer" target="_blank">@lanceball</a> <span class="status">1529300295213322240</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1529299152705794049',
    created: 1653448305000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lanceball" rel="noopener noreferrer" target="_blank">@lanceball</a> I, for one, am so enraged that I can barely see straight. And to see the accomplices in so called leadership positions fan the flames only makes me more irate.<br><br>In reply to: <a href="https://x.com/lanceball/status/1529297485302210560" rel="noopener noreferrer" target="_blank">@lanceball</a> <span class="status">1529297485302210560</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1529268035382411264',
    created: 1653440886000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/michaelminella" rel="noopener noreferrer" target="_blank">@michaelminella</a> <a class="mention" href="https://x.com/Xfinity" rel="noopener noreferrer" target="_blank">@Xfinity</a> Wow, Cox once did that to an apartment I had in college...but that was a dilapidated apartment building. I can\'t imagine a company doing that to a HOUSE!<br><br>In reply to: <a href="https://x.com/michaelminella/status/1529260465641205760" rel="noopener noreferrer" target="_blank">@michaelminella</a> <span class="status">1529260465641205760</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1528657236209246208',
    created: 1653295260000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/robertsweetman" rel="noopener noreferrer" target="_blank">@robertsweetman</a> That means a lot to me for you to say that. Thank you.<br><br>In reply to: <a href="https://x.com/robertsweetman/status/1528320161949835265" rel="noopener noreferrer" target="_blank">@robertsweetman</a> <span class="status">1528320161949835265</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1528510840218931200',
    created: 1653260357000,
    type: 'post',
    text: 'Patched and released. Automated releases FTW!',
    likes: 7,
    retweets: 0
  },
  {
    id: '1528502065508405248',
    created: 1653258265000,
    type: 'reply',
    text: '@justinmwhitaker <a class="mention" href="https://x.com/AthleticBrewing" rel="noopener noreferrer" target="_blank">@AthleticBrewing</a> Nice! Tucker\'s was certainly a nice one. My big standouts so far have been the Strata-ford IPA and the Black IPA. But, honestly, they\'re all so darn good.<br><br>In reply to: @justinmwhitaker <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1528448507656347648',
    created: 1653245496000,
    type: 'reply',
    text: '@drew_m_france <a class="mention" href="https://x.com/AthleticBrewing" rel="noopener noreferrer" target="_blank">@AthleticBrewing</a> 💯🏃‍♀️<br><br>In reply to: @drew_m_france <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1528299369123852288',
    created: 1653209938000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Always more tests.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1528296307743510528" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1528296307743510528</span>',
    photos: ['<div class="item"><img class="photo" src="media/1528299369123852288.gif"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1528293557131325440',
    created: 1653208552000,
    type: 'post',
    text: 'I may have just stumbled onto a fix for the colspan bug that\'s been in Asciidoctor PDF since its inception. It turns out, the precomputed column widths were being ignored. I can\'t believe I\'m only just discovered that now. Fix en route!',
    likes: 28,
    retweets: 1
  },
  {
    id: '1528152026302951424',
    created: 1653174809000,
    type: 'post',
    text: 'I really love <a class="mention" href="https://x.com/AthleticBrewing" rel="noopener noreferrer" target="_blank">@AthleticBrewing</a>. It has been such a blessing. While I\'m not solely an NA beer drinker, I love having that option. And I particularly love that others have that option too.<br><br>I think events should stock them. I wouldn\'t even mind if that\'s all that was offered.',
    likes: 25,
    retweets: 1
  },
  {
    id: '1528093895661236224',
    created: 1653160949000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pvaneeckhoudt" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> Thanks. To the best of my knowledge, I\'m powerless in this situation. The only viable solution is to move (when I can) and/or be more successful. Fortunately, we have great support for OSS, family, &amp; friends, and we\'ll make it through. It just burns that it has to be this way.<br><br>In reply to: <a href="https://x.com/pvaneeckhoudt/status/1527906623901024256" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> <span class="status">1527906623901024256</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1527885380602589185',
    created: 1653111236000,
    type: 'reply',
    text: '@jbryant787 🤦‍♂️<br><br>In reply to: @jbryant787 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1527871394008207360',
    created: 1653107901000,
    type: 'post',
    text: 'Status update. Still snowing.',
    photos: ['<div class="item"><img class="photo" src="media/1527871394008207360.jpg"></div>'],
    likes: 1,
    retweets: 0
  },
  {
    id: '1527763634604060673',
    created: 1653082209000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fanf42" rel="noopener noreferrer" target="_blank">@fanf42</a> And that\'s an important thing to point out. Nothing about this building has changed, not one bit.<br><br>In reply to: <a href="https://x.com/fanf42/status/1527762592743272449" rel="noopener noreferrer" target="_blank">@fanf42</a> <span class="status">1527762592743272449</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1527762056937541632',
    created: 1653081833000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fanf42" rel="noopener noreferrer" target="_blank">@fanf42</a> New buildings have been put up around us with more amenities and a higher rate, so they raised the rates here to reward themselves with the same amount of money without having to lift a finger.<br><br>In reply to: <a href="https://x.com/fanf42/status/1527758430911578112" rel="noopener noreferrer" target="_blank">@fanf42</a> <span class="status">1527758430911578112</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1527749944295051265',
    created: 1653078945000,
    type: 'post',
    text: 'The reason we can\'t support using $$\\sum_{i=1}^N (f_i)^2$$ directly in the AsciiDoc source is because $$ is already has a different meaning in the language.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1527746871493423104',
    created: 1653078212000,
    type: 'post',
    text: 'Just to provide some more context here, if I were to switch to month-to-month (to start looking for another place), then the rate increase is $1,346. Only by signing a year lease can I get it down to a $405 increase. Anyone who says this is inflation doesn\'t understand inflation.<br><br>Quoting: <a href="#1527031169211977728">1527031169211977728</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1527744797707862016',
    created: 1653077718000,
    type: 'post',
    text: 'What we\'d like to be able to support is:<br><br>stem:[\\sum_{i=1}^N (f_i)^2]',
    likes: 3,
    retweets: 0
  },
  {
    id: '1527744613678653440',
    created: 1653077674000,
    type: 'quote',
    text: 'This works in AsciiDoc documents if you use an inline passthrough:<br><br>pass:[$$\\sum_{i=1}^N (f_i)^2$$]<br><br>We just need to coordinate so Asciidoctor outputs the delimiters that the GitHub renderer looks for when called by the GitHub app. Let\'s talk.<br><br>Quoting: <a href="https://x.com/github/status/1527345223898042368" rel="noopener noreferrer" target="_blank">@github</a> <span class="status">1527345223898042368</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1527743011932602368',
    created: 1653077292000,
    type: 'post',
    text: '',
    photos: ['<div class="item"><img class="photo" src="media/1527743011932602368.jpg"></div>'],
    likes: 2,
    retweets: 0
  },
  {
    id: '1527740541563088896',
    created: 1653076703000,
    type: 'post',
    text: 'Let me just say, <a class="mention" href="https://x.com/SquooshApp" rel="noopener noreferrer" target="_blank">@SquooshApp</a> is a nice tool to have handy. Optimize and resize your images in a jiffy.',
    likes: 2,
    retweets: 1
  },
  {
    id: '1527730230030311425',
    created: 1653074245000,
    type: 'post',
    text: 'To celebrate the release of Asciidoctor PDF 2, I enjoyed the St. Bernardus Christmas Ale that\'s been haunting the back of my fridge since, well, 🎄. Now, it\'s snowing in Colorado. Coincidence?',
    likes: 14,
    retweets: 0
  },
  {
    id: '1527730028884025344',
    created: 1653074197000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/neugens" rel="noopener noreferrer" target="_blank">@neugens</a> <a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> 🤣<br><br>In reply to: <a href="https://x.com/neugens/status/1527693766596956162" rel="noopener noreferrer" target="_blank">@neugens</a> <span class="status">1527693766596956162</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1527729950232498176',
    created: 1653074178000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lofidewanto" rel="noopener noreferrer" target="_blank">@lofidewanto</a> <a class="mention" href="https://x.com/rahulsom" rel="noopener noreferrer" target="_blank">@rahulsom</a> And I\'m not going to disagree because that was not at all the point of my original post.<br><br>In reply to: <a href="https://x.com/lofidewanto/status/1527717985942523904" rel="noopener noreferrer" target="_blank">@lofidewanto</a> <span class="status">1527717985942523904</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1527605790357393408',
    created: 1653044576000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CKrger" rel="noopener noreferrer" target="_blank">@CKrger</a> Trust me, the tests are good. And they have saved me thousands of times. But nothing can replace real world usage...which is why I study it endlessly.<br><br>In reply to: <a href="https://x.com/CKrger/status/1527601271888302081" rel="noopener noreferrer" target="_blank">@CKrger</a> <span class="status">1527601271888302081</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1527594633244467201',
    created: 1653041916000,
    type: 'post',
    text: 'Despite having &gt; 2,050 tests, a user discovered a rare bug in Asciidoctor PDF 2 within 24 hours of it being released. And while that torments me, it also makes me very happy. Because I know it means I need another test ;)',
    likes: 24,
    retweets: 1
  },
  {
    id: '1527568208680476674',
    created: 1653035616000,
    type: 'post',
    text: 'Learn before your criticize. The thing you don\'t know is not wrong just because you don\'t know it.<br><br>Once you learn it, go to town on it, because then you will have points backed up by experience.',
    likes: 7,
    retweets: 0
  },
  {
    id: '1527566495638618113',
    created: 1653035208000,
    type: 'reply',
    text: '@Alexey90851886 <a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> You\'d be right in saying that there are a lot of poorly constructed packages in <a href="http://npmjs.com" rel="noopener noreferrer" target="_blank">npmjs.com</a>. But that\'s because more developers are pushing packages there and thus the quality has a more dynamic range. It\'s always best to know your dependencies.<br><br>In reply to: <a href="#1527565980368441346">1527565980368441346</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1527565980368441346',
    created: 1653035085000,
    type: 'reply',
    text: '@Alexey90851886 <a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> That has absolutely nothing to do with npm. It\'s the result of a choice by the application developer to add tons of dependencies (and perhaps very careless ones). That mistake can be made in any ecosystem.<br><br>In reply to: @Alexey90851886 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1527565486501683200',
    created: 1653034967000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PavelTcholakov" rel="noopener noreferrer" target="_blank">@PavelTcholakov</a> npm has both of those things. If we\'re going to have a conversation, let\'s start with facts. All packages are cached on download. And node_modules is kept consistent with the package-lock.json file. (I\'m not making the case it\'s better than Gradle or Maven, but it\'s not rogue).<br><br>In reply to: <a href="https://x.com/PavelTcholakov/status/1527564491478683649" rel="noopener noreferrer" target="_blank">@PavelTcholakov</a> <span class="status">1527564491478683649</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1527563770817482755',
    created: 1653034558000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_JamesWard" rel="noopener noreferrer" target="_blank">@_JamesWard</a> <a href="https://docs.npmjs.com/cli/v8/commands/npm-cache#details" rel="noopener noreferrer" target="_blank">docs.npmjs.com/cli/v8/commands/npm-cache#details</a><br><br>In reply to: <a href="https://x.com/_JamesWard/status/1527430482635505664" rel="noopener noreferrer" target="_blank">@_JamesWard</a> <span class="status">1527430482635505664</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1527553508534235141',
    created: 1653032111000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DenisKuniss" rel="noopener noreferrer" target="_blank">@DenisKuniss</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Thanks!<br><br>And totally agreed...dependency graph size is a real problem. I think we should recognize that the problem is universal. I push very hard in my projects to keep that number as small as possible...then smaller.<br><br>In reply to: <a href="https://x.com/DenisKuniss/status/1527550097315225600" rel="noopener noreferrer" target="_blank">@DenisKuniss</a> <span class="status">1527550097315225600</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1527552263119466496',
    created: 1653031814000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rahulsom" rel="noopener noreferrer" target="_blank">@rahulsom</a> I\'m not making an argument that Node.js or Java is better. (I use them both). I\'m just emphasizing that we need to be honest about what the situation actually is...and not just poke fun of each other with baseless claims.<br><br>In reply to: <a href="#1527551926287491072">1527551926287491072</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1527551926287491072',
    created: 1653031734000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rahulsom" rel="noopener noreferrer" target="_blank">@rahulsom</a> And even then, it only extracts it into the project if you specifically ask it to (by using a package.json file). You can install packages globally and share them. The real difference is that Node.js does not read libraries into memory directly from the package.<br><br>In reply to: <a href="#1527551026105970689">1527551026105970689</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1527551026105970689',
    created: 1653031519000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rahulsom" rel="noopener noreferrer" target="_blank">@rahulsom</a> That\'s simply not a true statement. npm does use a shared download cache. It extracts the package from the cache into the project.<br><br>In reply to: <a href="https://x.com/rahulsom/status/1527481321571819520" rel="noopener noreferrer" target="_blank">@rahulsom</a> <span class="status">1527481321571819520</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1527463978053402624',
    created: 1653010765000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DrAmandaLMartin" rel="noopener noreferrer" target="_blank">@DrAmandaLMartin</a> 😱<br><br>In reply to: <a href="https://x.com/DrAmandaLMartin/status/1527462882585108480" rel="noopener noreferrer" target="_blank">@DrAmandaLMartin</a> <span class="status">1527462882585108480</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1527400078251421696',
    created: 1652995531000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/wimdeblauwe" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> @sormuras Like Gradle, npm does cache the packages globally, then copies them to the project (npm also unpacks them, as is required by Node.js). It\'s not downloading them each time. Even then, packages can be installed globally, it\'s just not as common of a practice.<br><br>In reply to: <a href="https://x.com/wimdeblauwe/status/1527393917557170182" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> <span class="status">1527393917557170182</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1527387697777496074',
    created: 1652992579000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> <a class="mention" href="https://x.com/JonathanGiles" rel="noopener noreferrer" target="_blank">@JonathanGiles</a> I love that JavaScript has a JSON parser / generator built in, but annoyed there isn\'t one for YAML (opinions about YAML aside, it\'s a missing feature). Ruby has both. I think language platforms need to provide these things.<br><br>In reply to: <a href="https://x.com/brunoborges/status/1527385961574715393" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1527385961574715393</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1527387137934385152',
    created: 1652992445000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> And the best part is that it encourages you to learn the full capabilities of the platform you\'re using. There are too many libraries that just do (or redo) what stdlib already does.<br><br>In reply to: <a href="#1527385233942646784">1527385233942646784</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1527385233942646784',
    created: 1652991991000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> I would advocate the same for Java development too. Try to use the stdlib until it becomes untenable not to. That\'s just a smart strategy in this ever more complicated world.<br><br>In reply to: <a href="#1527384901657300992">1527384901657300992</a>',
    likes: 5,
    retweets: 0
  },
  {
    id: '1527384901657300992',
    created: 1652991912000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> We\'ve been able to build Antora with very few dependencies (and shrinking). We need some integration libraries like a git client and a zip file reader, but other than that, we\'re mostly using stdlib (and trying to wean off lingering dependencies from the early days).<br><br>In reply to: <a href="#1527381327757987840">1527381327757987840</a>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1527382566151409673',
    created: 1652991355000,
    type: 'reply',
    text: '@sormuras And, to be clear, that\'s not worse or better than npm. The point is that it\'s not different.<br><br>In reply to: <a href="#1527381628661575680">1527381628661575680</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1527382132988858387',
    created: 1652991252000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pvaneeckhoudt" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> I wish projects didn\'t use ^ (even if it did always do the right thing, it\'s not the thing we often want). Fortunately, npm now has version overrides. Certainly not perfect, but a start. <a href="https://docs.npmjs.com/cli/v8/configuring-npm/package-json#overrides" rel="noopener noreferrer" target="_blank">docs.npmjs.com/cli/v8/configuring-npm/package-json#overrides</a><br><br>In reply to: <a href="https://x.com/pvaneeckhoudt/status/1527381443420114966" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> <span class="status">1527381443420114966</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1527381628661575680',
    created: 1652991132000,
    type: 'reply',
    text: '@sormuras I built three Spring projects starting with a fresh profile and the cache is now 2GB.<br><br>In reply to: @sormuras <span class="status">&lt;deleted&gt;</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1527381327757987840',
    created: 1652991060000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> Again, it\'s lack of knowledge on the part of developers. The API reference for Node.js is some of the most detailed I\'ve ever read. <a href="https://nodejs.org/api/fs.html" rel="noopener noreferrer" target="_blank">nodejs.org/api/fs.html</a><br><br>In reply to: <a href="https://x.com/brunoborges/status/1527380677628284931" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1527380677628284931</span>',
    likes: 5,
    retweets: 0
  },
  {
    id: '1527380960659918852',
    created: 1652990973000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pvaneeckhoudt" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> There are no doubt flaws in the resolution, though I have found it to be very reliable to avoid ^ and to use ~ instead. That\'s what I do in all my repos, and it works great. See <a href="https://gitlab.com/antora/antora/-/blob/main/package.json#L14-26" rel="noopener noreferrer" target="_blank">gitlab.com/antora/antora/-/blob/main/package.json#L14-26</a><br><br>In reply to: <a href="https://x.com/pvaneeckhoudt/status/1527380315794440193" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> <span class="status">1527380315794440193</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1527380649836826643',
    created: 1652990898000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Righteous.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1527380022389919755" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1527380022389919755</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1527380497306816512',
    created: 1652990862000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> The standard API is, in fact, incredibly deep. The fact that developers use libraries for simple things is a poor decision by those developers.<br><br>In reply to: <a href="https://x.com/brunoborges/status/1527379745100288009" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1527379745100288009</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1527379254169587729',
    created: 1652990566000,
    type: 'post',
    text: 'I find it humorous that Java devs poke fun at npm for downloading the internet. Have you not seen the size of your Gradle or Maven cache? People in glass houses should not throw stones.',
    likes: 61,
    retweets: 3
  },
  {
    id: '1527244949229408257',
    created: 1652958545000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/laurentgrangeau" rel="noopener noreferrer" target="_blank">@laurentgrangeau</a> <a class="mention" href="https://x.com/gcuisinier" rel="noopener noreferrer" target="_blank">@gcuisinier</a> Thank you.<br><br>In reply to: <a href="https://x.com/laurentgrangeau/status/1527203575272710146" rel="noopener noreferrer" target="_blank">@laurentgrangeau</a> <span class="status">1527203575272710146</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1527083235066318848',
    created: 1652919989000,
    type: 'post',
    text: 'I wrote a brief summary about this release and the journey leading up to it on the release notes page.<br><br><a href="https://github.com/asciidoctor/asciidoctor-pdf/releases/tag/v2.0.0" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-pdf/releases/tag/v2.0.0</a>',
    likes: 7,
    retweets: 3
  },
  {
    id: '1527083080145522688',
    created: 1652919952000,
    type: 'post',
    text: 'And without further ado...<br><br><a href="https://docs.asciidoctor.org/pdf-converter/latest/whats-new/" rel="noopener noreferrer" target="_blank">docs.asciidoctor.org/pdf-converter/latest/whats-new/</a>',
    likes: 29,
    retweets: 13
  },
  {
    id: '1527031169211977728',
    created: 1652907576000,
    type: 'post',
    text: 'Thanks to all who have reached out. While I\'m powerless at the moment to do anything about the situation, the show of support pulled me up from slipping into a dark place. And I\'m going to build on that positivity by pushing out a major software release.<br><br>Quoting: <a href="#1526299531544915968">1526299531544915968</a>',
    likes: 12,
    retweets: 0
  },
  {
    id: '1526820720382050304',
    created: 1652857401000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fanf42" rel="noopener noreferrer" target="_blank">@fanf42</a> The fact that you care already helps a ton. The harder this world becomes to accept, the more important community is to restore some semblance of balance.<br><br>In reply to: <a href="https://x.com/fanf42/status/1526814085710487552" rel="noopener noreferrer" target="_blank">@fanf42</a> <span class="status">1526814085710487552</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1526709988583800832',
    created: 1652831000000,
    type: 'post',
    text: 'Sarah just saw an AsciiDoc snippet that uses 80 character block delimiters and exclaimed, "what is this monstrosity?!" It\'s hard to believe that\'s even still in the language. Just, don\'t. Keep it concise.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1526707278383943680',
    created: 1652830354000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcioendo" rel="noopener noreferrer" target="_blank">@marcioendo</a> <a class="mention" href="https://x.com/nipafx" rel="noopener noreferrer" target="_blank">@nipafx</a> <a class="mention" href="https://x.com/opencollect" rel="noopener noreferrer" target="_blank">@opencollect</a> My experience writing Seam in Action is what led me to AsciiDoc and Asciidoctor. I was so horrified by the writing experience that it started a life-long journey to make a better toolchain for writing. The rest is history.<br><br>In reply to: <a href="https://x.com/marcioendo/status/1526699177438285825" rel="noopener noreferrer" target="_blank">@marcioendo</a> <span class="status">1526699177438285825</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1526694363119509505',
    created: 1652827275000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcioendo" rel="noopener noreferrer" target="_blank">@marcioendo</a> <a class="mention" href="https://x.com/nipafx" rel="noopener noreferrer" target="_blank">@nipafx</a> <a class="mention" href="https://x.com/opencollect" rel="noopener noreferrer" target="_blank">@opencollect</a> Yep, that\'s me.<br><br>In reply to: <a href="https://x.com/marcioendo/status/1526539836823306240" rel="noopener noreferrer" target="_blank">@marcioendo</a> <span class="status">1526539836823306240</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1526691090425794561',
    created: 1652826495000,
    type: 'post',
    text: 'and the first release to use semantic versioning...',
    likes: 4,
    retweets: 0
  },
  {
    id: '1526643650075078656',
    created: 1652815184000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ameliaeiras" rel="noopener noreferrer" target="_blank">@ameliaeiras</a> 🤗 We\'re nothing if not resilient. We\'ll be playing some Gloria Gaynor tonight. It\'s no coincidence that her iconic song was released the year we were born.<br><br>In reply to: <a href="https://x.com/ameliaeiras/status/1526641058724999168" rel="noopener noreferrer" target="_blank">@ameliaeiras</a> <span class="status">1526641058724999168</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1526641068774543362',
    created: 1652814569000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/chmiels" rel="noopener noreferrer" target="_blank">@chmiels</a> It\'s all artificial. This is well documented by the journalists in the area and around the country.<br><br>In reply to: <a href="https://x.com/chmiels/status/1526640292664836096" rel="noopener noreferrer" target="_blank">@chmiels</a> <span class="status">1526640292664836096</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1526637592380289024',
    created: 1652813740000,
    type: 'post',
    text: 'It\'s also really shitty that this comes on the eve of a major series of releases that I\'ve worked harder on than anything I can remember. Hard work doesn\'t solve your financial problems, trust me. If anything, hard work costs you money.',
    likes: 3,
    retweets: 1
  },
  {
    id: '1526635198133043200',
    created: 1652813169000,
    type: 'post',
    text: 'I tried to appeal to my landlord by asking for a grace or transition period to adapt gradually to the new rate. After all, I\'ve been a renter here in good standing for almost a decade. The response was a resounding "Fuck you. Pay or leave." This world is psychopathic.<br><br>Quoting: <a href="#1526299531544915968">1526299531544915968</a>',
    likes: 5,
    retweets: 3
  },
  {
    id: '1526634746419089408',
    created: 1652813061000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nipafx" rel="noopener noreferrer" target="_blank">@nipafx</a> Thank you from the bottom of my heart. You\'re truly standup person and I\'ll never forget it. 🍻<br><br>In reply to: <a href="https://x.com/nipafx/status/1526491865326952448" rel="noopener noreferrer" target="_blank">@nipafx</a> <span class="status">1526491865326952448</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1526634325646508033',
    created: 1652812961000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/j0s3julianmart1" rel="noopener noreferrer" target="_blank">@j0s3julianmart1</a> I hate to disappoint you, but you won\'t find many more rights. They\'re being dismantled each and every day.<br><br>In reply to: <a href="https://x.com/j0s3julianmart1/status/1526556270496563201" rel="noopener noreferrer" target="_blank">@j0s3julianmart1</a> <span class="status">1526556270496563201</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1526491564553342977',
    created: 1652778924000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/badrelhouari" rel="noopener noreferrer" target="_blank">@badrelhouari</a> <a class="mention" href="https://x.com/DevoxxMA" rel="noopener noreferrer" target="_blank">@DevoxxMA</a> <a class="mention" href="https://x.com/enlamp" rel="noopener noreferrer" target="_blank">@enlamp</a> 🤩<br><br>In reply to: <a href="https://x.com/badrelhouari/status/1526283023053901826" rel="noopener noreferrer" target="_blank">@badrelhouari</a> <span class="status">1526283023053901826</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1526443660069306369',
    created: 1652767503000,
    type: 'post',
    text: 'and all new docs...',
    likes: 7,
    retweets: 0
  },
  {
    id: '1526359118994284548',
    created: 1652747347000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/domdorn" rel="noopener noreferrer" target="_blank">@domdorn</a> They limit the length of the lease so that they can change the price whenever it rolls over. It can never be more than one year.<br><br>In reply to: <a href="https://x.com/domdorn/status/1526341074217738240" rel="noopener noreferrer" target="_blank">@domdorn</a> <span class="status">1526341074217738240</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1526328630162272256',
    created: 1652740077000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nipafx" rel="noopener noreferrer" target="_blank">@nipafx</a> Thanks for reaching out! The best way to support my work is through <a href="https://opencollective.com/asciidoctor" rel="noopener noreferrer" target="_blank">opencollective.com/asciidoctor</a>. That\'s what enables us to keep working on Asciidoctor every single day and what made Asciidoctor PDF 2 possible.<br><br>In reply to: <a href="https://x.com/nipafx/status/1526314246220525574" rel="noopener noreferrer" target="_blank">@nipafx</a> <span class="status">1526314246220525574</span>',
    likes: 7,
    retweets: 3
  },
  {
    id: '1526323818175684608',
    created: 1652738930000,
    type: 'reply',
    text: '@jbryant787 😿<br><br>In reply to: @jbryant787 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1526314846609780736',
    created: 1652736791000,
    type: 'post',
    text: 'I\'m so sick of project leads treating other project leads like crap. If you\'re a project lead, and you\'re doing that, grow up. Open source is one community and I have no tolerance for people who try to tear up that fabric.',
    likes: 12,
    retweets: 2
  },
  {
    id: '1526311024672272384',
    created: 1652735880000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fanf42" rel="noopener noreferrer" target="_blank">@fanf42</a> The increase is more than what anyone should have to pay for a 2 bedroom apartment. (And trust me, this is not a big unit).<br><br>In reply to: <a href="https://x.com/fanf42/status/1526306631281147904" rel="noopener noreferrer" target="_blank">@fanf42</a> <span class="status">1526306631281147904</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1526310583162634240',
    created: 1652735775000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/imdondo" rel="noopener noreferrer" target="_blank">@imdondo</a> And make no mistake, I have put into open source all the wealth I have ever accumulated.<br><br>In reply to: <a href="#1526310310532947968">1526310310532947968</a>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1526310310532947968',
    created: 1652735710000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/imdondo" rel="noopener noreferrer" target="_blank">@imdondo</a> Trust me, I\'ve been realizing it for a very long time. I was just fortunate enough to sacrifice enough to offset it. But that well is running dry.<br><br>In reply to: <a href="https://x.com/imdondo/status/1526309469663133698" rel="noopener noreferrer" target="_blank">@imdondo</a> <span class="status">1526309469663133698</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1526308465328566272',
    created: 1652735270000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fanf42" rel="noopener noreferrer" target="_blank">@fanf42</a> It\'s hard to overstate just how expensive it is to be an American. And it\'s only getting worse every year.<br><br>In reply to: <a href="https://x.com/fanf42/status/1526306352947240964" rel="noopener noreferrer" target="_blank">@fanf42</a> <span class="status">1526306352947240964</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1526304281241956352',
    created: 1652734272000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/indianatwork" rel="noopener noreferrer" target="_blank">@indianatwork</a> Yep. And the market is hot because the landlords got together and decided they\'d all raise the rates so tenants have no other choice than to pay it. Hence why I believe this is extortion through and through.<br><br>In reply to: <a href="https://x.com/indianatwork/status/1526301669889286144" rel="noopener noreferrer" target="_blank">@indianatwork</a> <span class="status">1526301669889286144</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1526301546941583360',
    created: 1652733620000,
    type: 'post',
    text: 'If you wonder why it\'s becoming harder to work on open source, this is one of the reasons. I don\'t need 10 fancy cars or even one. I just need to have an affordable place to live. This country is making that seemingly modest requirement extremely difficult.',
    likes: 9,
    retweets: 1
  },
  {
    id: '1526299531544915968',
    created: 1652733140000,
    type: 'post',
    text: 'My landlord is extorting me with a $405 increase in monthly rent without any prior warning or transition period. I hate capitalism. I hate having no tenant rights. This is bullshit.',
    likes: 15,
    retweets: 1
  },
  {
    id: '1526121369892556800',
    created: 1652690663000,
    type: 'post',
    text: 'and &gt; 4,000 commits in total...',
    likes: 4,
    retweets: 0
  },
  {
    id: '1526121181601886208',
    created: 1652690618000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fanf42" rel="noopener noreferrer" target="_blank">@fanf42</a> I accept that assertion.<br><br>In reply to: <a href="https://x.com/fanf42/status/1525923522446606337" rel="noopener noreferrer" target="_blank">@fanf42</a> <span class="status">1525923522446606337</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1525913013449961473',
    created: 1652640987000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sam_brannen" rel="noopener noreferrer" target="_blank">@sam_brannen</a> <a class="mention" href="https://x.com/mp911de" rel="noopener noreferrer" target="_blank">@mp911de</a> <a class="mention" href="https://x.com/marcphilipp" rel="noopener noreferrer" target="_blank">@marcphilipp</a> I know that feeling, and it\'s a great one to have! Congrats!<br><br>In reply to: <a href="https://x.com/sam_brannen/status/1525907635714805760" rel="noopener noreferrer" target="_blank">@sam_brannen</a> <span class="status">1525907635714805760</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1525912710424064000',
    created: 1652640914000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/venkat_s" rel="noopener noreferrer" target="_blank">@venkat_s</a> I definitely code subconsciously when I\'m not at the keyboard coding, so absolutely!<br><br>In reply to: <a href="https://x.com/venkat_s/status/1525828170112442368" rel="noopener noreferrer" target="_blank">@venkat_s</a> <span class="status">1525828170112442368</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1525753763293581317',
    created: 1652603018000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pvaneeckhoudt" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> I\'m starting to see it too.<br><br>In reply to: <a href="https://x.com/pvaneeckhoudt/status/1525747898625019906" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> <span class="status">1525747898625019906</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1525746213798019073',
    created: 1652601219000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/knacht" rel="noopener noreferrer" target="_blank">@knacht</a> I use a Razer Book 13 by Razer.<br><br>In reply to: <a href="https://x.com/knacht/status/1525728896192323585" rel="noopener noreferrer" target="_blank">@knacht</a> <span class="status">1525728896192323585</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1525718375036465153',
    created: 1652594581000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/knacht" rel="noopener noreferrer" target="_blank">@knacht</a> Do you mean which OS?<br><br>In reply to: <a href="https://x.com/knacht/status/1525710860508966912" rel="noopener noreferrer" target="_blank">@knacht</a> <span class="status">1525710860508966912</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1525665606468374528',
    created: 1652582000000,
    type: 'post',
    text: 'Eat. Sleep. Code. 🔁<br>Eat. Sleep. Code. 🔁',
    likes: 2,
    retweets: 0
  },
  {
    id: '1525659390467969029',
    created: 1652580518000,
    type: 'post',
    text: 'Trendy software tends to ignore the incredibly hard work put in to stable, well-tested software that has matured and proven itself over the period of a decade or longer. Be careful what (empty) promises you believe in.',
    likes: 6,
    retweets: 0
  },
  {
    id: '1525657502074871808',
    created: 1652580068000,
    type: 'post',
    text: 'Open Source might be good for your career, but it\'s not great for your back. I need to get outside more.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1525636844867952640',
    created: 1652575143000,
    type: 'post',
    text: 'Having written a bunch of code and docs lately, I stand by this statement 💯.<br><br>Quoting: <a href="#831609585937088512">831609585937088512</a>',
    likes: 7,
    retweets: 1
  },
  {
    id: '1525634830121070592',
    created: 1652574663000,
    type: 'quote',
    text: 'Stand by those words and help them get into the EU. Otherwise, those words are just empty.<br><br>Quoting: <a href="https://x.com/EU_Commission/status/1525612390401814528" rel="noopener noreferrer" target="_blank">@EU_Commission</a> <span class="status">1525612390401814528</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1525634014932979713',
    created: 1652574468000,
    type: 'post',
    text: 'Someone at a crosswalk just said, "you know, if we press this button, it will let us cross the street" and I was just like "...that\'s true, actually."',
    likes: 1,
    retweets: 0
  },
  {
    id: '1525632784907108352',
    created: 1652574175000,
    type: 'post',
    text: 'I think I\'m about to go through curling withdrawal. 🥌',
    likes: 0,
    retweets: 0
  },
  {
    id: '1525632571391954944',
    created: 1652574124000,
    type: 'post',
    text: 'OH: In golf, it\'s real easy to hit the ball far. It\'s really hard to hit the ball straight.<br><br>Ain\'t the the truth.',
    likes: 0,
    retweets: 1
  },
  {
    id: '1525628168320667648',
    created: 1652573074000,
    type: 'post',
    text: 'People sometimes give RedHat a hard time for pushing Podman. But it\'s justified. I love it. And you should too. It just works. No deamon. No stress.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1525627165198340096',
    created: 1652572835000,
    type: 'post',
    text: 'I also want to add that every release of OSS I push out owes part of its success to Fedora. It makes me a more efficient OSS developer.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1525626510194814976',
    created: 1652572679000,
    type: 'quote',
    text: 'I deeply love Fedora. I appreciate every bit of effort that goes into it. In a complicated world, it makes my daily life less complicated. Serious ups.<br><br>Quoting: <a href="https://x.com/myhatisred/status/1525465119731421184" rel="noopener noreferrer" target="_blank">@myhatisred</a> <span class="status">1525465119731421184</span>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1525625897423818752',
    created: 1652572533000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jack" rel="noopener noreferrer" target="_blank">@jack</a> <a class="mention" href="https://x.com/elonmusk" rel="noopener noreferrer" target="_blank">@elonmusk</a> Oh go to hell. It was not designed to save you time. It was designed to force you to engage with shit that pisses you off. Don\'t lie about it.<br><br>In reply to: <a href="https://x.com/jack/status/1525615420278833152" rel="noopener noreferrer" target="_blank">@jack</a> <span class="status">1525615420278833152</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1525625362335510528',
    created: 1652572405000,
    type: 'quote',
    text: 'See if I vote for a Democrat again. You care nothing about Black lives, and for me that\'s the line. I\'m sick of this. 🤮<br><br>Quoting: <a href="https://x.com/POTUS/status/1525257398163759110" rel="noopener noreferrer" target="_blank">@POTUS</a> <span class="status">1525257398163759110</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1525584847867064320',
    created: 1652562746000,
    type: 'post',
    text: 'and &gt; 200 resolved issues...',
    likes: 10,
    retweets: 0
  },
  {
    id: '1525583351758721024',
    created: 1652562389000,
    type: 'post',
    text: 'If you have a GitHub Actions workflow that uses JRuby 9.3 on windows-latest, add this environment variable to fix the "Permission denied NUL" error when running bundler:<br><br>JRUBY_OPTS: \'-J-Djdk.io.File.enableADS=true\'',
    likes: 3,
    retweets: 3
  },
  {
    id: '1525581791041425408',
    created: 1652562017000,
    type: 'post',
    text: 'This still really, really annoys me.<br><br>Quoting: <a href="#1517245894315298816">1517245894315298816</a>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1524703921926643713',
    created: 1652352717000,
    type: 'post',
    text: 'with &gt; 2,000 tests...',
    likes: 10,
    retweets: 1
  },
  {
    id: '1524669835958886401',
    created: 1652344590000,
    type: 'quote',
    text: 'This is what I love about IDEs, and one of the things I think they do best. You really can\'t match this kind of functionality using a basic text editor. Level up.<br><br>Quoting: <a href="https://x.com/intellijidea/status/1524660701104467968" rel="noopener noreferrer" target="_blank">@intellijidea</a> <span class="status">1524660701104467968</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1524558112367120384',
    created: 1652317953000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ThomasG77" rel="noopener noreferrer" target="_blank">@ThomasG77</a> I should clarify I\'m referring to the source of the docs that are available on a public site.<br><br>In reply to: <a href="https://x.com/ThomasG77/status/1524533978979569665" rel="noopener noreferrer" target="_blank">@ThomasG77</a> <span class="status">1524533978979569665</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1524532461392175104',
    created: 1652311837000,
    type: 'post',
    text: 'I don\'t get open source projects or companies that have private documentation repositories. What is that about?',
    likes: 10,
    retweets: 1
  },
  {
    id: '1524512998726914048',
    created: 1652307197000,
    type: 'post',
    text: 'Every day the world is like, "I wonder how much I can upset you until you\'re not able to function properly..."',
    likes: 2,
    retweets: 0
  },
  {
    id: '1524503158549979138',
    created: 1652304851000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> <a class="mention" href="https://x.com/github" rel="noopener noreferrer" target="_blank">@github</a> Nice idea. Add a notification to provide a chance to defer and I\'d be in for that 💯.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1524116142159134720',
    created: 1652212579000,
    type: 'post',
    text: 'More than anything else, creating software is about making decisions (then writing tests that verify the software carries out those decisions).<br><br>And there are so many decisions to make.',
    likes: 9,
    retweets: 1
  },
  {
    id: '1524106331895975936',
    created: 1652210240000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Like every day ;)<br><br>In reply to: <a href="https://x.com/kito99/status/1524030269757575169" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">1524030269757575169</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1523854034636992513',
    created: 1652150088000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danhillenbrand" rel="noopener noreferrer" target="_blank">@danhillenbrand</a> @jbryant787 Quick pickled onions is from here: <a href="https://www.badmanners.com/recipes/quick-pickled-red-onions" rel="noopener noreferrer" target="_blank">www.badmanners.com/recipes/quick-pickled-red-onions</a> The sandwich is from <a href="https://www.badmanners.com/book/brave-new-meal" rel="noopener noreferrer" target="_blank">www.badmanners.com/book/brave-new-meal</a><br><br>In reply to: <a href="https://x.com/danhillenbrand/status/1523844850839400452" rel="noopener noreferrer" target="_blank">@danhillenbrand</a> <span class="status">1523844850839400452</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1523795239386845184',
    created: 1652136070000,
    type: 'reply',
    text: '@jbryant787 How we discovered them is in a recipe for a sandwich that also includes pear, cashew cheese, smoked tempeh, arugula, and mustard. It\'s an absolute melody of flavors with lots of zing!<br><br>In reply to: @jbryant787 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1523548520753426432',
    created: 1652077248000,
    type: 'post',
    text: 'Who knew that pickled onions are so delicious? (and so easy to make, too)!',
    likes: 1,
    retweets: 0
  },
  {
    id: '1523475087910858752',
    created: 1652059740000,
    type: 'post',
    text: 'I think I actually breathe better in a KF94 mask. No dust or pollen. Plenty of space. I like it! 😷',
    likes: 2,
    retweets: 0
  },
  {
    id: '1523474421817561088',
    created: 1652059581000,
    type: 'post',
    text: 'Smoking weed is okay-ish. Totally different beast.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1523473258703515648',
    created: 1652059304000,
    type: 'post',
    text: 'Smoking in public is violence.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1523473122002804737',
    created: 1652059271000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/EmmaVigeland" rel="noopener noreferrer" target="_blank">@EmmaVigeland</a> I would do it every day of the week. Smoking is violence.<br><br>In reply to: <a href="https://x.com/EmmaVigeland/status/1523472646637162496" rel="noopener noreferrer" target="_blank">@EmmaVigeland</a> <span class="status">1523472646637162496</span>',
    likes: 5,
    retweets: 0
  },
  {
    id: '1523470987903193089',
    created: 1652058762000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iphigenie" rel="noopener noreferrer" target="_blank">@iphigenie</a> We must keep training the "algorithm"<br><br>In reply to: <a href="https://x.com/iphigenie/status/1523454943415267328" rel="noopener noreferrer" target="_blank">@iphigenie</a> <span class="status">1523454943415267328</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1523470770227191809',
    created: 1652058710000,
    type: 'post',
    text: 'Autocorrect just changed fucking to fuckin\' and I was like "that\'s alright, now we\'re talkin\'"',
    likes: 7,
    retweets: 0
  },
  {
    id: '1523453633412534273',
    created: 1652054625000,
    type: 'post',
    text: 'Every time Twitter switches my timeline to Home, I\'m just going to switch it back to Latest Tweets.',
    likes: 7,
    retweets: 0
  },
  {
    id: '1523453028375834624',
    created: 1652054481000,
    type: 'quote',
    text: '2.0.0 is coming...<br><br>Quoting: <a href="https://x.com/RubygemsN/status/1521971462823112704" rel="noopener noreferrer" target="_blank">@RubygemsN</a> <span class="status">1521971462823112704</span>',
    likes: 53,
    retweets: 10
  },
  {
    id: '1523151198429212673',
    created: 1651982519000,
    type: 'quote',
    text: '💯×💯 👥<br><br>Quoting: <a href="https://x.com/oleg_nenashev/status/1522939977075761156" rel="noopener noreferrer" target="_blank">@oleg_nenashev</a> <span class="status">1522939977075761156</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1522501839035330561',
    created: 1651827699000,
    type: 'quote',
    text: 'This is a much faster alternative to Edit As HTML in the Inspector to make minor text changes while designing.<br><br>Quoting: <a href="https://x.com/AmeliasBrain/status/1521146127327801345" rel="noopener noreferrer" target="_blank">@AmeliasBrain</a> <span class="status">1521146127327801345</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1522422100056674304',
    created: 1651808688000,
    type: 'post',
    text: 'If you aren\'t into <a class="mention" href="https://x.com/badmannersfood" rel="noopener noreferrer" target="_blank">@badmannersfood</a> (which, I mean, why aren\'t you? but okay), then <a class="mention" href="https://x.com/AvantGardeVegan" rel="noopener noreferrer" target="_blank">@AvantGardeVegan</a> lays down some serious vegan sorcery in his cookbooks.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1522335470549540864',
    created: 1651788034000,
    type: 'quote',
    text: 'I\'m 💯 in favor. This strategy has improved the Node.js / npm ecosystem dramatically, and I anticipate it having a similar impact for the Ruby / RubyGems ecosystem.<br><br>Quoting: <a href="https://x.com/sbellware/status/1522278696526262272" rel="noopener noreferrer" target="_blank">@sbellware</a> <span class="status">1522278696526262272</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1522115866925494273',
    created: 1651735676000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/garethemery" rel="noopener noreferrer" target="_blank">@garethemery</a> I thoroughly enjoyed watching and listening to your ANALOG performance at Ultra. It was spectacular. 🔥 I\'d say never change, except change is precisely what makes you you, so keep changing and taking risks! I appreciate you.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1522010520122892288',
    created: 1651710560000,
    type: 'post',
    text: 'Stop calling these people leaders because they are anything but. And I\'m over it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1522009224364003328',
    created: 1651710251000,
    type: 'post',
    text: 'Clyburn is also a terrible human being for exactly the same reasons. Among many, these two stand in the way of progress purposefully and without regret. They are hollow to the core of their useless, shameful selves. They have zero remorse for supporting an anti-choice candidate.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1521966098161176576',
    created: 1651699969000,
    type: 'quote',
    text: '💯<br><br>Quoting: <a href="https://x.com/github/status/1521928433630121984" rel="noopener noreferrer" target="_blank">@github</a> <span class="status">1521928433630121984</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1521965427605221376',
    created: 1651699809000,
    type: 'post',
    text: 'You don\'t get to be for woman on festival days, then turn your back on them when it matters. She\'s the worst kind of anti-feminist...because she could make a difference, but chooses to be a traitor instead.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1521964853442715648',
    created: 1651699672000,
    type: 'post',
    text: 'Pelosi is a terrible human being, and I hope that one day she realizes just how much damage she has done to this country, its citizens, and the world. It\'s a shameful legacy she leaves behind.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1521963910319935488',
    created: 1651699447000,
    type: 'post',
    text: 'I\'m also SICK of all the money that\'s being spent on proxy battles at the primary level. 🤮 It must stop. The Democrat Party spends more time and effort fighting itself than...well, that\'s all it does. They ask people for money, then burn it on this stupid infighting.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1521962274541383680',
    created: 1651699057000,
    type: 'quote',
    text: 'Supporting Cuellar is unforgivable. I don\'t want to hear a single word about how the Democrats fight for the people because they absolutely do not. They only know how to protect themselves and their interests. Screw them and their corrupt politics every day of the week.<br><br>Quoting: <a href="https://x.com/OurRevolution/status/1519079493373149184" rel="noopener noreferrer" target="_blank">@OurRevolution</a> <span class="status">1519079493373149184</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1521958183639535616',
    created: 1651698082000,
    type: 'post',
    text: 'Netlify has been having some sort of outage on their edge network. I\'ve received reports from visitors about nearly every site I host there. Rough start to the day.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1521422387962544128',
    created: 1651570338000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> I\'ve had both (work computers) and really, really disliked the 16". It was just too big and too heavy. It made my monitor arm sag. I sent it back and got the 14". Perfect size and weight in my opinion.<br><br>In reply to: <a href="https://x.com/agoncal/status/1521421079989923840" rel="noopener noreferrer" target="_blank">@agoncal</a> <span class="status">1521421079989923840</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1521317967056703488',
    created: 1651545442000,
    type: 'quote',
    text: 'Fuck this shit!<br><br>Quoting: <a href="https://x.com/politico/status/1521288272021901312" rel="noopener noreferrer" target="_blank">@politico</a> <span class="status">1521288272021901312</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1521221572010291200',
    created: 1651522460000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/codepitbull" rel="noopener noreferrer" target="_blank">@codepitbull</a> Definitely the right move. Congrats!<br><br>In reply to: <a href="https://x.com/codepitbull/status/1521142249089863680" rel="noopener noreferrer" target="_blank">@codepitbull</a> <span class="status">1521142249089863680</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1520339089500766208',
    created: 1651312060000,
    type: 'quote',
    text: 'Don\'t just laugh. Laugh, learn, and donate (if not here, to a humanitarian aid).<br><br>Quoting: <a href="https://x.com/_Tymoshenko/status/1519651401173020672" rel="noopener noreferrer" target="_blank">@_Tymoshenko</a> <span class="status">1519651401173020672</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1520337043930292224',
    created: 1651311572000,
    type: 'post',
    text: 'This is also my answer to "this issue has been open for n years, why hasn\'t it been fixed yet?"',
    likes: 3,
    retweets: 1
  },
  {
    id: '1520306151023284225',
    created: 1651304206000,
    type: 'post',
    text: 'I\'d say I\'ve had a pretty busy week. Even GitHub was like "I think that\'s enough to show for now."',
    photos: ['<div class="item"><img class="photo" src="media/1520306151023284225.jpg"></div>'],
    likes: 26,
    retweets: 1
  },
  {
    id: '1519968352982421504',
    created: 1651223669000,
    type: 'post',
    text: 'I just tested the InfoQ Mini-Book template with the upcoming Asciidoctor PDF 2 release. I\'m pleased to say that, even with its extensive customizations, it almost works without any changes. <a href="https://github.com/mraible/infoq-mini-book" rel="noopener noreferrer" target="_blank">github.com/mraible/infoq-mini-book</a> I\'m working to make the transition even more seamless.',
    likes: 9,
    retweets: 4
  },
  {
    id: '1519749521940439040',
    created: 1651171496000,
    type: 'quote',
    text: 'I love that screen! 🤩 I\'m sure <a class="mention" href="https://x.com/hsablonniere" rel="noopener noreferrer" target="_blank">@hsablonniere</a> loves it too.<br><br>Quoting: <a href="https://x.com/snafuz/status/1519631098636115968" rel="noopener noreferrer" target="_blank">@snafuz</a> <span class="status">1519631098636115968</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1519410147637948416',
    created: 1651090583000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Thanks for your participation! Your timing has been perfect since I\'ve been totally focused on that project in the last few weeks. And I love the challenges. They always lead to more tests!<br><br>In reply to: <a href="https://x.com/odrotbohm/status/1519401561885229058" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <span class="status">1519401561885229058</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1519390088219660289',
    created: 1651085800000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/wimdeblauwe" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> <a class="mention" href="https://x.com/maciejwalkowiak" rel="noopener noreferrer" target="_blank">@maciejwalkowiak</a> <a class="mention" href="https://x.com/zulip" rel="noopener noreferrer" target="_blank">@zulip</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Extremely happy.<br><br>In reply to: <a href="https://x.com/wimdeblauwe/status/1519278517862772738" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> <span class="status">1519278517862772738</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1519061056747831298',
    created: 1651007353000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mmilinkov" rel="noopener noreferrer" target="_blank">@mmilinkov</a> Best just to stay busy ;)<br><br>In reply to: <a href="https://x.com/mmilinkov/status/1519056354002014210" rel="noopener noreferrer" target="_blank">@mmilinkov</a> <span class="status">1519056354002014210</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1518835821591236608',
    created: 1650953653000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> <a class="mention" href="https://x.com/intel" rel="noopener noreferrer" target="_blank">@intel</a> <a class="mention" href="https://x.com/PGelsinger" rel="noopener noreferrer" target="_blank">@PGelsinger</a> Congrats on the new role and best of luck! One thing I\'m sure of is that you\'ll make big waves that will carry far and wide. For that reason, we\'re all lucky to have you in that role. Let\'s go!<br><br>In reply to: <a href="https://x.com/arungupta/status/1518762554167726083" rel="noopener noreferrer" target="_blank">@arungupta</a> <span class="status">1518762554167726083</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1518477159752343553',
    created: 1650868141000,
    type: 'post',
    text: 'The activity in my email over the weekend no longer looks different than it does any other day of the week. In other words, I can no longer tell the difference between the work week and the weekend.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1517602252684357632',
    created: 1650659547000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> Happy Birthday! I\'m so glad to see you\'re back up and humming and that you had a chance to really celebrate this one. Cheers, mate! 🍻<br><br>In reply to: <a href="https://x.com/settermjd/status/1517584872377925634" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1517584872377925634</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1517414116788301824',
    created: 1650614692000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nmartignole" rel="noopener noreferrer" target="_blank">@nmartignole</a> There are a lot of 2s on that slide ;) (I like the symmetry).<br><br>In reply to: <a href="https://x.com/nmartignole/status/1517399275617099777" rel="noopener noreferrer" target="_blank">@nmartignole</a> <span class="status">1517399275617099777</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1517254504994902017',
    created: 1650576637000,
    type: 'post',
    text: 'Fun meta fact: The body of the page is composed entirely in AsciiDoc. See <a href="https://gitlab.eclipse.org/eclipse-wg/asciidoc-wg/asciidoc.org/-/blob/main/pages/index.adoc" rel="noopener noreferrer" target="_blank">gitlab.eclipse.org/eclipse-wg/asciidoc-wg/asciidoc.org/-/blob/main/pages/index.adoc</a> The secret sauce is that the layout engine rearranges chunks of content so everything slides into place.<br><br>Quoting: <a href="#1516856697020555264">1516856697020555264</a>',
    likes: 9,
    retweets: 2
  },
  {
    id: '1517245894315298816',
    created: 1650574584000,
    type: 'post',
    text: 'I really don\'t get how <a href="http://semver.org" rel="noopener noreferrer" target="_blank">semver.org</a> emerged from the Ruby ecosystem, and yet the gem command doesn\'t strictly follow it. Gem::Version replaces the hyphen at the start of the prerelease version with ".pre.", which makes absolutely no sense.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1517062373038714884',
    created: 1650530830000,
    type: 'reply',
    text: '@remoquete <a class="mention" href="https://x.com/bryanwklein" rel="noopener noreferrer" target="_blank">@bryanwklein</a> AsciiDoc is surely not perfect, and part of why we have gotten together is to sort our what can be improved about it. But our mission still remains to make a concise and consistent language. That\'s in our charter.<br><br>In reply to: <a href="#1517061835765129216">1517061835765129216</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1517061835765129216',
    created: 1650530701000,
    type: 'reply',
    text: '@remoquete <a class="mention" href="https://x.com/bryanwklein" rel="noopener noreferrer" target="_blank">@bryanwklein</a> Left up to someone else to speak for AsciiDoc? I don\'t agree. We speak for AsciiDoc. And this is what we\'re saying. We\'re explaining why we dedicate parts of our lives to work on this format. We\'re reducing ceremony and making a clearer format. That\'s not harsh or unfair.<br><br>In reply to: @remoquete <span class="status">&lt;deleted&gt;</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1517061244179546112',
    created: 1650530560000,
    type: 'reply',
    text: '@remoquete <a class="mention" href="https://x.com/bryanwklein" rel="noopener noreferrer" target="_blank">@bryanwklein</a> I agree we should not dismiss other formats and I don\'t believe we\'re doing that. Many people have asked to see a comparison. We\'re presenting it on this site to inform visitors of the choice they have. It\'s up to them to take that information and use it to make a decision.<br><br>In reply to: @remoquete <span class="status">&lt;deleted&gt;</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1517060494258962432',
    created: 1650530382000,
    type: 'reply',
    text: '@remoquete <a class="mention" href="https://x.com/bryanwklein" rel="noopener noreferrer" target="_blank">@bryanwklein</a> I don\'t see anywhere where we\'re dismissing other formats. We\'re showing how AsciiDoc compares in the most honest way we can. And to be fair, we emphasize both the strengths and weaknesses of each format. If anything is inaccurate or unfair, I\'m open to rephrasing.<br><br>In reply to: @remoquete <span class="status">&lt;deleted&gt;</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1516920594758987776',
    created: 1650497027000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/leifgruenwoldt" rel="noopener noreferrer" target="_blank">@leifgruenwoldt</a> Thanks! That means a whole lot coming from you. I\'m very proud of the progress we\'re making to help improve the perception of the language.<br><br>In reply to: <a href="https://x.com/leifgruenwoldt/status/1516917806549381120" rel="noopener noreferrer" target="_blank">@leifgruenwoldt</a> <span class="status">1516917806549381120</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1516856698559901696',
    created: 1650481793000,
    type: 'post',
    text: 'You can read more about the relaunch, what it entails, and the history behind it in this asciidoc-wg mailing list post. <a href="https://www.eclipse.org/lists/asciidoc-wg/msg00580.html" rel="noopener noreferrer" target="_blank">www.eclipse.org/lists/asciidoc-wg/msg00580.html</a>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1516856697020555264',
    created: 1650481793000,
    type: 'post',
    text: 'The AsciiDoc WG is excited to announce the relaunch <a href="http://asciidoc.org" rel="noopener noreferrer" target="_blank">asciidoc.org</a>! This launch completes the first deliverable of the AsciiDoc WG, which is to provide an entry point into the AsciiDoc ecosystem that reflects the current state, direction, and brand identity of AsciiDoc.',
    likes: 73,
    retweets: 22
  },
  {
    id: '1516262142688727042',
    created: 1650340040000,
    type: 'post',
    text: 'Preparing high quality screenshots is extremely difficult. If you feel the same, just know that you\'re not alone.',
    likes: 16,
    retweets: 2
  },
  {
    id: '1515646705525436422',
    created: 1650193308000,
    type: 'post',
    text: 'I really wish GitHub would stop spamming my inbox with notifications of canceled builds (particularly redundant builds canceled to make way for an incoming build).',
    likes: 4,
    retweets: 0
  },
  {
    id: '1515425977538539522',
    created: 1650140682000,
    type: 'post',
    text: 'Oh, and I forgot to mention. Thanks to Sarah, the docs for it will finally be available at <a href="https://docs.asciidoctor.org" rel="noopener noreferrer" target="_blank">docs.asciidoctor.org</a>!',
    likes: 16,
    retweets: 0
  },
  {
    id: '1515419614087327748',
    created: 1650139165000,
    type: 'post',
    text: 'For the last i\'ve-lost-count weeks, Sarah and I have been pushing hard to get Asciidoctor PDF 2 finished. We\'re now over the big hurdles and are starting to move towards the first alpha. The final release won\'t be far behind, but just getting something out there will be huge.',
    likes: 41,
    retweets: 3
  },
  {
    id: '1515160688104157188',
    created: 1650077433000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/speakjava" rel="noopener noreferrer" target="_blank">@speakjava</a> Self publish. The tools are out there and you won\'t sacrifice any reward (whether it be financial or personal achievement).<br><br>In reply to: <a href="https://x.com/speakjava/status/1515066560850845696" rel="noopener noreferrer" target="_blank">@speakjava</a> <span class="status">1515066560850845696</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1515046716386414595',
    created: 1650050260000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/connolly_s" rel="noopener noreferrer" target="_blank">@connolly_s</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Now I feel seen ;)<br><br>In reply to: <a href="https://x.com/connolly_s/status/1514944327377686530" rel="noopener noreferrer" target="_blank">@connolly_s</a> <span class="status">1514944327377686530</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1514893999181500416',
    created: 1650013849000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AthleticBrewing" rel="noopener noreferrer" target="_blank">@AthleticBrewing</a> The beautiful thing about Athletic is that it\'s never too early to drink on any day. That\'s one of the reasons I keep it stocked.🍻<br><br>In reply to: <a href="https://x.com/AthleticBrewing/status/1504515479565742083" rel="noopener noreferrer" target="_blank">@AthleticBrewing</a> <span class="status">1504515479565742083</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1514799966681661448',
    created: 1649991430000,
    type: 'post',
    text: 'I\'ve gotten so into non-alcoholic beers. I think they\'re such a wonderful concept. It makes the experience of finding, discovering, and relaxing over a pint or can accessible to all. And <a class="mention" href="https://x.com/AthleticBrewing" rel="noopener noreferrer" target="_blank">@AthleticBrewing</a> is making it the real deal.',
    photos: ['<div class="item"><img class="photo" src="media/1514799966681661448.jpg"></div>'],
    likes: 5,
    retweets: 0
  },
  {
    id: '1514798904058867715',
    created: 1649991176000,
    type: 'post',
    text: '',
    photos: ['<div class="item"><img class="photo" src="media/1514798904058867715.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1514796517499949067',
    created: 1649990607000,
    type: 'post',
    text: '',
    photos: ['<div class="item"><img class="photo" src="media/1514796517499949067.png"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1514796368946114562',
    created: 1649990572000,
    type: 'post',
    text: '',
    photos: ['<div class="item"><img class="photo" src="media/1514796368946114562.png"></div>'],
    likes: 1,
    retweets: 0
  },
  {
    id: '1514765665273479170',
    created: 1649983252000,
    type: 'post',
    text: 'Make this world a better place by blocking people who try to make it a worse place. Honestly, I\'m so stunned that people embrace hatred, but there\'s no denying that they exist and need to be blocked without hesitation.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1514760909985525788',
    created: 1649982118000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MarcusBertrand5" rel="noopener noreferrer" target="_blank">@MarcusBertrand5</a> <a class="mention" href="https://x.com/sunrisemvmt" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a> He violated workers rights and fired those fighting to protect them. That very likely means he stole potential wages too. Don\'t side with billionaires. It\'s not it a good look.<br><br>In reply to: <a href="https://x.com/MarcusBertrand5/status/1514758127240982549" rel="noopener noreferrer" target="_blank">@MarcusBertrand5</a> <span class="status">1514758127240982549</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1514754360063930373',
    created: 1649980556000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sunrisemvmt" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a> If we want to get technical, it\'s not Elon\'s money because he stole it.<br><br>In reply to: <a href="https://x.com/sunrisemvmt/status/1514752615514521603" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a> <span class="status">1514752615514521603</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1514742435091341314',
    created: 1649977713000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasonbrooks" rel="noopener noreferrer" target="_blank">@jasonbrooks</a> <a class="mention" href="https://x.com/rbowen" rel="noopener noreferrer" target="_blank">@rbowen</a> Asciidoctor, Antora, Quarkus, to name a few. Also, our case study for Asciidoctor is published here: <a href="https://zulip.com/case-studies/asciidoctor/" rel="noopener noreferrer" target="_blank">zulip.com/case-studies/asciidoctor/</a><br><br>In reply to: <a href="https://x.com/jasonbrooks/status/1514739682063515649" rel="noopener noreferrer" target="_blank">@jasonbrooks</a> <span class="status">1514739682063515649</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1514738144863682590',
    created: 1649976690000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mattdm" rel="noopener noreferrer" target="_blank">@mattdm</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/rbowen" rel="noopener noreferrer" target="_blank">@rbowen</a> As we\'ve been saying, Zulip is not strictly chat. It\'s a mailinglist that works like a chat. So you don\'t need all those other communication tools. This tool provides archived, topical discussions with real-time interaction when you need it. It\'s decidedly not Slack.<br><br>In reply to: <a href="https://x.com/mattdm/status/1514734812782866441" rel="noopener noreferrer" target="_blank">@mattdm</a> <span class="status">1514734812782866441</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1514732577088700432',
    created: 1649975363000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/anthonny_q" rel="noopener noreferrer" target="_blank">@anthonny_q</a> <a class="mention" href="https://x.com/MathildeLemee" rel="noopener noreferrer" target="_blank">@MathildeLemee</a> Wow!<br><br>In reply to: <a href="https://x.com/anthonny_q/status/1514692133822865419" rel="noopener noreferrer" target="_blank">@anthonny_q</a> <span class="status">1514692133822865419</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1514522298706903040',
    created: 1649925229000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> Nice! That\'s encouraging to hear! You\'re on the right track.<br><br>In reply to: <a href="https://x.com/settermjd/status/1514520252306149377" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1514520252306149377</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1514521949807910913',
    created: 1649925145000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rbowen" rel="noopener noreferrer" target="_blank">@rbowen</a> Having used Zulip for over a year now, I can say from experience that it feels more like a mailing list than it does real-time chat. That\'s because topics are organized like email threads...they just happen to not get routed through email.<br><br>In reply to: <a href="#1514521534135562242">1514521534135562242</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1514521534135562242',
    created: 1649925046000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rbowen" rel="noopener noreferrer" target="_blank">@rbowen</a> I feel strongly the Zulip bridges this gap, offering both real-time chat and permanent, web public archives with search. Of course, it\'s not perfect, but as an OSS project itself, it\'s a comms platform worth getting behind. And we can critique it just like any other OSS project.<br><br>In reply to: <a href="https://x.com/rbowen/status/1514258958483177477" rel="noopener noreferrer" target="_blank">@rbowen</a> <span class="status">1514258958483177477</span>',
    likes: 5,
    retweets: 2
  },
  {
    id: '1514378754663272454',
    created: 1649891005000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <a class="mention" href="https://x.com/RedHatEvents" rel="noopener noreferrer" target="_blank">@RedHatEvents</a> <a class="mention" href="https://x.com/QuarkusIO" rel="noopener noreferrer" target="_blank">@QuarkusIO</a> <a class="mention" href="https://x.com/OReillyMedia" rel="noopener noreferrer" target="_blank">@OReillyMedia</a> <a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> OMG! Rock on! 🎸<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1514371541055721473" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1514371541055721473</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1514378435698704386',
    created: 1649890929000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> <a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> <a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> <a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> <a class="mention" href="https://x.com/devnexus" rel="noopener noreferrer" target="_blank">@devnexus</a> This absolutely made my day. Thank you for sharing!<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1514370797581783049" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1514370797581783049</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1514363071157788672',
    created: 1649887266000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> <a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> <a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> <a class="mention" href="https://x.com/devnexus" rel="noopener noreferrer" target="_blank">@devnexus</a> <a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> My tears are those of joy.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1514362580214636553" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1514362580214636553</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1514362342464647169',
    created: 1649887092000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> <a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> <a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> <a class="mention" href="https://x.com/devnexus" rel="noopener noreferrer" target="_blank">@devnexus</a> Now this is becoming too much to bear! ♥️ to all my besties!!<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1514359937744310278',
    created: 1649886519000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> <a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> <a class="mention" href="https://x.com/devnexus" rel="noopener noreferrer" target="_blank">@devnexus</a><br><br>In reply to: <a href="https://x.com/lightguardjp/status/1514355887590285321" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1514355887590285321</span>',
    photos: ['<div class="item"><img class="photo" src="media/1514359937744310278.gif"></div>'],
    likes: 2,
    retweets: 0
  },
  {
    id: '1514355263523807232',
    created: 1649885404000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> <a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> <a class="mention" href="https://x.com/devnexus" rel="noopener noreferrer" target="_blank">@devnexus</a> 😱 Now I\'m really missing out!! The gang really is all together!<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1514352891909312527" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1514352891909312527</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1514349541939589121',
    created: 1649884040000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> <a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> <a class="mention" href="https://x.com/devnexus" rel="noopener noreferrer" target="_blank">@devnexus</a> 😻<br><br>In reply to: <a href="https://x.com/alexsotob/status/1514341303231864836" rel="noopener noreferrer" target="_blank">@alexsotob</a> <span class="status">1514341303231864836</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1514340882522140675',
    created: 1649881976000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> <a class="mention" href="https://x.com/devnexus" rel="noopener noreferrer" target="_blank">@devnexus</a> I can\'t believe I\'m missing both you and <a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> at DevNexus! 🥲 I hope you\'re enjoying yourself and that your talk went well. I\'m going to be on watch for a picture of the two of you together! Go AsciidoctorJ!<br><br>In reply to: <a href="https://x.com/abelsromero/status/1513933690711285760" rel="noopener noreferrer" target="_blank">@abelsromero</a> <span class="status">1513933690711285760</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1514168325751848966',
    created: 1649840835000,
    type: 'post',
    text: 'The so-called viewers are either also narcissists or, more likely, fictional. No conscious person is looking at this thinking that if only the activists would protest in a designated area off to the side, everything would be fine. If you think that, read the damn IPCC report.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1514166860446535680',
    created: 1649840486000,
    type: 'post',
    text: 'I have much, much stronger words, but I leave it up to the reader to imagine me cursing at the screen in dismay.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1514166715474620416',
    created: 1649840451000,
    type: 'quote',
    text: 'These interviewers are infuriating! More so, they\'re criminals for being a mouthpiece of a system that\'s rapidly destroying the future of this Earth. The complete disregard for the inevitable cost of human life is sickening. It once again proves that #DontLookUp is a documentary.<br><br>Quoting: <a href="https://x.com/GreenpeaceUK/status/1513835066928951301" rel="noopener noreferrer" target="_blank">@GreenpeaceUK</a> <span class="status">1513835066928951301</span>',
    likes: 2,
    retweets: 1,
    tags: ['dontlookup']
  },
  {
    id: '1514161891202850816',
    created: 1649839301000,
    type: 'post',
    text: 'dependabot\'s greatest success is training me to reach Dependency Zero.',
    likes: 10,
    retweets: 1
  },
  {
    id: '1513984925543804928',
    created: 1649797109000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MStine" rel="noopener noreferrer" target="_blank">@MStine</a> s/You\'re/Your/<br><br>In reply to: <a href="#1513984165405880320">1513984165405880320</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1513984674820866048',
    created: 1649797049000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> <a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <a class="mention" href="https://x.com/Doctor_Astro" rel="noopener noreferrer" target="_blank">@Doctor_Astro</a> <a class="mention" href="https://x.com/DonaldOJDK" rel="noopener noreferrer" target="_blank">@DonaldOJDK</a> <a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> Sarah would be very displeased (because she loves sours). But she also understands why others don\'t. More for her. 🍻<br><br>In reply to: <a href="https://x.com/brunoborges/status/1513982828836319234" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1513982828836319234</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1513984165405880320',
    created: 1649796928000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MStine" rel="noopener noreferrer" target="_blank">@MStine</a> You\'re first chat with them is usually your exit interview.<br><br>In reply to: @mstine <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1513983768800813060',
    created: 1649796833000,
    type: 'quote',
    text: 'Figure out which project the peg is for you and support it. That\'s how we sure this up.<br><br>Quoting: <a href="https://x.com/BastilleBSD/status/1513758819423334403" rel="noopener noreferrer" target="_blank">@BastilleBSD</a> <span class="status">1513758819423334403</span>',
    likes: 7,
    retweets: 3
  },
  {
    id: '1513982376367386624',
    created: 1649796501000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <a class="mention" href="https://x.com/Doctor_Astro" rel="noopener noreferrer" target="_blank">@Doctor_Astro</a> <a class="mention" href="https://x.com/DonaldOJDK" rel="noopener noreferrer" target="_blank">@DonaldOJDK</a> <a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> The Cold IPA is a new style. Just read about it but haven\'t yet tried it.<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1513979906429968397" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1513979906429968397</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1513982179428106242',
    created: 1649796454000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <a class="mention" href="https://x.com/Doctor_Astro" rel="noopener noreferrer" target="_blank">@Doctor_Astro</a> <a class="mention" href="https://x.com/DonaldOJDK" rel="noopener noreferrer" target="_blank">@DonaldOJDK</a> <a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> 🤩<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1513979906429968397" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1513979906429968397</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1513666817692106753',
    created: 1649721266000,
    type: 'post',
    text: 'Listen <a class="mention" href="https://x.com/github" rel="noopener noreferrer" target="_blank">@github</a>. When a build is cancelled because another one has come in to replace it, I don\'t need a notification about it. A canceled build it not a failure!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1513632328366452738',
    created: 1649713043000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fanf42" rel="noopener noreferrer" target="_blank">@fanf42</a> <a class="mention" href="https://x.com/Infosec_Taylor" rel="noopener noreferrer" target="_blank">@Infosec_Taylor</a> The idea that employers provide heath care subsidies also leads to some really awkward workspace situations. I\'ve been in meeting at a company where people are detailing their health care problems and needs (like birthing plans) to get the company to select a better plan.<br><br>In reply to: <a href="#1513631785594200066">1513631785594200066</a>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1513631785594200066',
    created: 1649712914000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fanf42" rel="noopener noreferrer" target="_blank">@fanf42</a> <a class="mention" href="https://x.com/Infosec_Taylor" rel="noopener noreferrer" target="_blank">@Infosec_Taylor</a> And I cannot emphasize enough that nothing in this thread is exaggerated. It really is like that (and could even be a lot worse for some people). Not only is it expensive, you spend *a lot* of time dealing with it all.<br><br>In reply to: <a href="https://x.com/fanf42/status/1513605266616508420" rel="noopener noreferrer" target="_blank">@fanf42</a> <span class="status">1513605266616508420</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1513489929346723843',
    created: 1649679093000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/benignbala" rel="noopener noreferrer" target="_blank">@benignbala</a> <a class="mention" href="https://x.com/Infosec_Taylor" rel="noopener noreferrer" target="_blank">@Infosec_Taylor</a> Very scary. And my message to the citizens of the world. You don\'t want this. Trust me. Even if you are perfectly healthy, your top expense is medical. Fight it all the way.<br><br>In reply to: <a href="https://x.com/benignbala/status/1513488411042402304" rel="noopener noreferrer" target="_blank">@benignbala</a> <span class="status">1513488411042402304</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1513489444493475849',
    created: 1649678977000,
    type: 'quote',
    text: 'This is a painfully accurate thread that perfectly explains my own experience. And if you have your own small business, it\'s $500 per family member per month.<br><br>Quoting: <a href="https://x.com/Infosec_Taylor/status/1513179369325436935" rel="noopener noreferrer" target="_blank">@Infosec_Taylor</a> <span class="status">1513179369325436935</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1513116972304801793',
    created: 1649590173000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rbowen" rel="noopener noreferrer" target="_blank">@rbowen</a> <a class="mention" href="https://x.com/geerlingguy" rel="noopener noreferrer" target="_blank">@geerlingguy</a> <a class="mention" href="https://x.com/RedHat" rel="noopener noreferrer" target="_blank">@RedHat</a> 💯<br><br>In reply to: <a href="https://x.com/rbowen/status/1512898788217733123" rel="noopener noreferrer" target="_blank">@rbowen</a> <span class="status">1512898788217733123</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1513110044044234754',
    created: 1649588521000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> <a class="mention" href="https://x.com/jimmac" rel="noopener noreferrer" target="_blank">@jimmac</a> I\'m very happy to confirm that this issue is now fixed on Fedora (as of at least Firefox 98). Just wanted to follow-up to say thank you.<br><br>In reply to: <a href="https://x.com/garrett/status/1351456872985989121" rel="noopener noreferrer" target="_blank">@garrett</a> <span class="status">1351456872985989121</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1513063683710087169',
    created: 1649577468000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jreleaser" rel="noopener noreferrer" target="_blank">@jreleaser</a> <a class="mention" href="https://x.com/github" rel="noopener noreferrer" target="_blank">@github</a> <a class="mention" href="https://x.com/gitlab" rel="noopener noreferrer" target="_blank">@gitlab</a> <a class="mention" href="https://x.com/GiteA" rel="noopener noreferrer" target="_blank">@GiteA</a> That\'s excellent news. Having a single release process across language platforms would be a dream.<br><br>In reply to: <a href="https://x.com/jreleaser/status/1513062560257478657" rel="noopener noreferrer" target="_blank">@jreleaser</a> <span class="status">1513062560257478657</span>',
    likes: 1,
    retweets: 2
  },
  {
    id: '1513054952188305411',
    created: 1649575386000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jreleaser" rel="noopener noreferrer" target="_blank">@jreleaser</a> <a class="mention" href="https://x.com/github" rel="noopener noreferrer" target="_blank">@github</a> <a class="mention" href="https://x.com/gitlab" rel="noopener noreferrer" target="_blank">@gitlab</a> <a class="mention" href="https://x.com/GiteA" rel="noopener noreferrer" target="_blank">@GiteA</a> Do you envision JReleaser expanding to release packages for other language platforms such as RubyGems or npm packages too? (Naturally, the release process itself would require Java, which is reasonable).<br><br>In reply to: <a href="https://x.com/jreleaser/status/1513030820646330370" rel="noopener noreferrer" target="_blank">@jreleaser</a> <span class="status">1513030820646330370</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1512733608775090179',
    created: 1649498772000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> <a class="mention" href="https://x.com/rickygervais" rel="noopener noreferrer" target="_blank">@rickygervais</a> At times, you may start to question whether you\'ll ever have energy again, and that can feel terrifying. Just know that your strength will return and this is just your body\'s way of encouraging rest. In many ways, fighting it made me stronger.<br><br>In reply to: <a href="https://x.com/settermjd/status/1512720565437620229" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1512720565437620229</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1512698333441142786',
    created: 1649490361000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> <a class="mention" href="https://x.com/rickygervais" rel="noopener noreferrer" target="_blank">@rickygervais</a> I hope that you recover quicker than I did. I\'m not sure what advise to give, other than move around when you can, and rest when your body wants it.<br><br>In reply to: <a href="#1512697742090330112">1512697742090330112</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1512697742090330112',
    created: 1649490220000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> <a class="mention" href="https://x.com/rickygervais" rel="noopener noreferrer" target="_blank">@rickygervais</a> My experience with COVID was one of utter exhaustion. Sounds very close to what you described. I had other symptoms, but it\'s hard to remember when all you want to do is lay down and sleep. It took me at least a month to recover fully. I was not boosted at the time (but am now).<br><br>In reply to: <a href="https://x.com/settermjd/status/1512695546783289346" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1512695546783289346</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1511821263949291522',
    created: 1649281252000,
    type: 'post',
    text: 'I should add that it can be very powerful when they are combined.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1511820877456846855',
    created: 1649281160000,
    type: 'post',
    text: 'Open source is not open governance. They can be used together. But nothing about the first implies the second. Please don\'t confuse these.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1511819356736417797',
    created: 1649280797000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ProJavaOrlovsky" rel="noopener noreferrer" target="_blank">@ProJavaOrlovsky</a> Always has been, always will be. That\'s how it works.<br><br>In reply to: <a href="https://x.com/ProJavaOrlovsky/status/1511818380407410690" rel="noopener noreferrer" target="_blank">@ProJavaOrlovsky</a> <span class="status">1511818380407410690</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1511812858442489861',
    created: 1649279248000,
    type: 'post',
    text: 'Solving 7 year old issues like a boss this week. 💪',
    likes: 15,
    retweets: 0
  },
  {
    id: '1511634996049547268',
    created: 1649236842000,
    type: 'post',
    text: 'It\'s just so profoundly unfair because it jumps the line. In doing so, it takes time away from other issues being worked if the maintainer feels compelled to reply. "You\'re not working on this, can you?" (If the issue is scheduled, that\'s an entirely different conversation).',
    likes: 1,
    retweets: 1
  },
  {
    id: '1511620750775447555',
    created: 1649233446000,
    type: 'post',
    text: 'Vegan food is also damn delicious.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1511619038895431681',
    created: 1649233038000,
    type: 'post',
    text: 'A key reason I advocate for becoming vegan is because, if we all do it, it will make a huge impact. But more important, it gets you into the habit of thinking about your impact on this world on a daily basis. It\'s a concrete starting point for climate &amp; environmental activism.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1511618043448684545',
    created: 1649232800000,
    type: 'quote',
    text: 'We upended the world once already for the pandemic (almost). We have it in ourselves to do it again, this time for the long-term survival of the world we know. And this time, you can\'t say that you didn\'t get a warning that it was coming.<br><br>Quoting: <a href="https://x.com/sellieyoung/status/1511139539456299008" rel="noopener noreferrer" target="_blank">@sellieyoung</a> <span class="status">1511139539456299008</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1511616749610815491',
    created: 1649232492000,
    type: 'post',
    text: 'A far better approach is "I can help get this issue over the finish line. I understand the blockers and have ideas about how to get past them. Would you like me to help? If so, what can I do?"',
    likes: 6,
    retweets: 2
  },
  {
    id: '1511616448984014849',
    created: 1649232420000,
    type: 'post',
    text: 'Please don\'t come into an open source project and say "this issue has been open for &lt;n&gt; years, can it be dealt with now?" unless you\'re willing to help with the project management. There are thousands of issues and only 365 days in a year. Lots of initiatives are going to idle.',
    likes: 43,
    retweets: 7
  },
  {
    id: '1511477991838081027',
    created: 1649199409000,
    type: 'post',
    text: 'In Firefox, if you click the &lt;html&gt; tag in the Inspector, then switch to the Fonts tab, when you hover over the name of a font, it will show you all the places it\'s used in the page. Check it out!',
    photos: ['<div class="item"><img class="photo" src="media/1511477991838081027.png"></div>'],
    likes: 16,
    retweets: 4
  },
  {
    id: '1511093277432287241',
    created: 1649107686000,
    type: 'quote',
    text: 'If you aren\'t on a plant-based diet by now, knowing what we all know about what\'s occurring, we simply aren\'t on the same page about caring for the planet we share.<br><br>Quoting: <a href="https://x.com/ajplus/status/1511012426317275141" rel="noopener noreferrer" target="_blank">@ajplus</a> <span class="status">1511012426317275141</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1510950329562505223',
    created: 1649073605000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> The support ticket is from a security company 😱<br><br>In reply to: <a href="https://x.com/garrett/status/1510941287524012035" rel="noopener noreferrer" target="_blank">@garrett</a> <span class="status">1510941287524012035</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1510942049784147975',
    created: 1649071631000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> I think you just composed my reply for me. 💯🙏<br><br>In reply to: <a href="https://x.com/garrett/status/1510941287524012035" rel="noopener noreferrer" target="_blank">@garrett</a> <span class="status">1510941287524012035</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1510937788081852418',
    created: 1649070615000,
    type: 'post',
    text: 'Just when I get comfortable accepting that IE 11 is no longer a browser I should worry about, a client submits a support ticket that the new code doesn\'t work in IE 11. It\'s endless.',
    likes: 8,
    retweets: 0
  },
  {
    id: '1510788342194978819',
    created: 1649034984000,
    type: 'post',
    text: 'Honestly, why do the protocols established after the Geneva Conventions even exist if the protecting powers weren\'t prepared to first prevent, then stop what\'s going on?? This is a failure at the highest levels of leadership. I hope all the world leaders pay a price for this.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1510787089859678210',
    created: 1649034685000,
    type: 'post',
    text: 'I\'m so enraged by the news coming out of Ukraine that I\'m practically delirious. The world leaders, who\'ve had nearly 80 years to make sure genocide never happened again, have utterly failed us. This is on them as much as it is on the criminals doing it. No excuses. Fuck war.',
    likes: 7,
    retweets: 1
  },
  {
    id: '1510784914228097025',
    created: 1649034167000,
    type: 'quote',
    text: 'Putin\'s price hike? FFS, his army is slaughtering people! We shouldn\'t even be getting anything from that regime at all, and certainly not crud that destroys the environment. Wake the FUCK up and do something other than back room deals and handshakes!<br><br>Quoting: <a href="https://x.com/POTUS/status/1509686541127495687" rel="noopener noreferrer" target="_blank">@POTUS</a> <span class="status">1509686541127495687</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1510733873805623298',
    created: 1649021998000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mattdm" rel="noopener noreferrer" target="_blank">@mattdm</a> I refuse to buy a computer that uses Nvidia hardware. That\'s the first filter I apply when looking for a new computer.<br><br>In reply to: <a href="https://x.com/mattdm/status/1510664974556880900" rel="noopener noreferrer" target="_blank">@mattdm</a> <span class="status">1510664974556880900</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1510453968211652613',
    created: 1648955263000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jzb" rel="noopener noreferrer" target="_blank">@jzb</a> <a class="mention" href="https://x.com/spotfoss" rel="noopener noreferrer" target="_blank">@spotfoss</a> 🤣<br><br>In reply to: <a href="https://x.com/jzb/status/1510452792665821191" rel="noopener noreferrer" target="_blank">@jzb</a> <span class="status">1510452792665821191</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1510452268973273091',
    created: 1648954858000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jzb" rel="noopener noreferrer" target="_blank">@jzb</a> <a class="mention" href="https://x.com/spotfoss" rel="noopener noreferrer" target="_blank">@spotfoss</a> You know, this got me thinking. This should REALLY be a question that the first run wizard asks. It could present both scenarios and ask which one is natural for you. No judgement. That way, it doesn\'t make you feel abnormal, either way.<br><br>In reply to: <a href="#1510450917266526218">1510450917266526218</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1510450917266526218',
    created: 1648954536000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jzb" rel="noopener noreferrer" target="_blank">@jzb</a> <a class="mention" href="https://x.com/spotfoss" rel="noopener noreferrer" target="_blank">@spotfoss</a> You know what\'s best for you, so I\'m def not questioning that. I\'m just pointing out that for me, the switch came very quickly and now doing it the other way seems backwards. But, like handedness, there\'s no right way. Depends on the person.<br><br>In reply to: <a href="https://x.com/jzb/status/1510428376854585351" rel="noopener noreferrer" target="_blank">@jzb</a> <span class="status">1510428376854585351</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1510426247146328065',
    created: 1648948654000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/spotfoss" rel="noopener noreferrer" target="_blank">@spotfoss</a> <a class="mention" href="https://x.com/jzb" rel="noopener noreferrer" target="_blank">@jzb</a> As a person who first resisted, I can assure you that you\'ll probably wonder why we ever did it the other way once you switch. Short of that, your brain will mostly like adapt very quickly. Mine certainly did.<br><br>In reply to: <a href="https://x.com/spotfoss/status/1510420015949963268" rel="noopener noreferrer" target="_blank">@spotfoss</a> <span class="status">1510420015949963268</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1510388402121691138',
    created: 1648939631000,
    type: 'post',
    text: 'I wish they would actually take this shit seriously instead of adding more bad jokes into this world.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1510387695402438659',
    created: 1648939462000,
    type: 'quote',
    text: '😱<br><br>Quoting: <a href="https://x.com/X/status/1509951255388504066" rel="noopener noreferrer" target="_blank">@X</a> <span class="status">1509951255388504066</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1510063923730345984',
    created: 1648862269000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/techgirl1908" rel="noopener noreferrer" target="_blank">@techgirl1908</a> I totally agree. I also believe in being generous about what you classify as a win. My signature phrase lately: "I\'m calling that a win." :) Following your advice, I need to follow up by celebrating it.<br><br>In reply to: <a href="https://x.com/techgirl1908/status/1510045464946786304" rel="noopener noreferrer" target="_blank">@techgirl1908</a> <span class="status">1510045464946786304</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1509991489916395521',
    created: 1648845000000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JavaScript" rel="noopener noreferrer" target="_blank">@JavaScript</a> <a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> I\'m part of both the Java and JavaScript communities, and I found this tweet to be incredibly awkward and out of line. Furthermore, trying to call out Sharat directly was plain stupid. Just stop.<br><br>In reply to: <a href="https://x.com/JavaScript/status/1509711745874595840" rel="noopener noreferrer" target="_blank">@JavaScript</a> <span class="status">1509711745874595840</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1509619776704106499',
    created: 1648756376000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/barraganc" rel="noopener noreferrer" target="_blank">@barraganc</a> YES YES YES! 🎉🎶🎛️<br><br>In reply to: <a href="https://x.com/barraganc/status/1509604601858871301" rel="noopener noreferrer" target="_blank">@barraganc</a> <span class="status">1509604601858871301</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1508916289460445185',
    created: 1648588652000,
    type: 'quote',
    text: 'Speaking tips and techniques never go out of style. 👇<br><br>Quoting: <a href="https://x.com/mstine/status/1482518233815162883" rel="noopener noreferrer" target="_blank">@mstine</a> <span class="status">1482518233815162883</span>',
    likes: 4,
    retweets: 1
  },
  {
    id: '1507971139615698950',
    created: 1648363311000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I hope you at least got to enjoy the trip to Disneyland with the kids. I find the best way to return to adulting is to keep riding on the memories, no matter how crazy they are. It\'s our adventures (physical and virtual) that make us who we are. Laugh about the silly stuff.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1507958269188345858" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1507958269188345858</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1507871523150839808',
    created: 1648339560000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/barraganc" rel="noopener noreferrer" target="_blank">@barraganc</a> Indeed. She has really caught my ear in the last two years. She has such incredible patience, and that just makes the resolutions so much sweeter.<br><br>In reply to: <a href="https://x.com/barraganc/status/1507854560756486150" rel="noopener noreferrer" target="_blank">@barraganc</a> <span class="status">1507854560756486150</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1507853136295346180',
    created: 1648335176000,
    type: 'post',
    text: 'And now ANNA is crushing it from the same stage. #ULTRALIVE',
    likes: 0,
    retweets: 0,
    tags: ['ultralive']
  },
  {
    id: '1507829324879175687',
    created: 1648329499000,
    type: 'post',
    text: 'cc: <a class="mention" href="https://x.com/sixpacksound" rel="noopener noreferrer" target="_blank">@sixpacksound</a><br><br>Quoting: <a href="#1507828189728546819">1507828189728546819</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1507828777715384322',
    created: 1648329369000,
    type: 'quote',
    text: '💯🤣<br><br>Quoting: <a href="https://x.com/eddiejaoude/status/1507825719749091333" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> <span class="status">1507825719749091333</span>',
    likes: 3,
    retweets: 2
  },
  {
    id: '1507828189728546819',
    created: 1648329229000,
    type: 'quote',
    text: 'I can\'t even make sense of this news. The Foo Fighters were a significant part of my teenage years &amp; friendships, and have been ever since. One of the first concerts I attended was during their first tour, while still processing the loss of Cobain. It\'s shock on top of shock.<br><br>Quoting: <a href="https://x.com/foofighters/status/1507552958988255234" rel="noopener noreferrer" target="_blank">@foofighters</a> <span class="status">1507552958988255234</span>',
    likes: 5,
    retweets: 0
  },
  {
    id: '1507792065416945664',
    created: 1648320616000,
    type: 'reply',
    text: '@darthkali_ <a class="mention" href="https://x.com/codecentric" rel="noopener noreferrer" target="_blank">@codecentric</a> <a class="mention" href="https://x.com/DK_1977" rel="noopener noreferrer" target="_blank">@DK_1977</a> AsciiDoc under the sun! ☀️<br><br>In reply to: @darthkali_ <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1507565008451825668',
    created: 1648266481000,
    type: 'reply',
    text: '@sxmElectro <a class="mention" href="https://x.com/SIRIUSXM" rel="noopener noreferrer" target="_blank">@SIRIUSXM</a> <a class="mention" href="https://x.com/ultra" rel="noopener noreferrer" target="_blank">@ultra</a> I\'d like to have the channel in my subscription without paying a fortune and supporting broadcasts that I don\'t think deserve a platform. Until then, I\'ll just watch on YouTube, thanks.<br><br>In reply to: <a href="https://x.com/SiriusXMElectro/status/1507027841468477441" rel="noopener noreferrer" target="_blank">@SiriusXMElectro</a> <span class="status">1507027841468477441</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1507559934460051457',
    created: 1648265272000,
    type: 'post',
    text: 'Carl Cox is absolutely crushing it at Ultra Music Festival. Wow, I\'m beyond impressed. 🎧',
    likes: 3,
    retweets: 0
  },
  {
    id: '1507486803036581888',
    created: 1648247836000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/zulip" rel="noopener noreferrer" target="_blank">@zulip</a> See <a href="https://zulip.com/help/web-public-streams" rel="noopener noreferrer" target="_blank">zulip.com/help/web-public-streams</a><br><br>In reply to: <a href="#1507486700326465536">1507486700326465536</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1507486700326465536',
    created: 1648247811000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/zulip" rel="noopener noreferrer" target="_blank">@zulip</a> They\'re going to announce general availability for the feature very soon. Until then, reach out to support (<a href="https://zulip.com/help/contact-support" rel="noopener noreferrer" target="_blank">zulip.com/help/contact-support</a>) to request that it be enabled on your instance.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1507474729426464770" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1507474729426464770</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1507470169349656578',
    created: 1648243870000,
    type: 'quote',
    text: 'Recent days has turned into one month. (And, if we\'re honest, all the way back to 2014). I disagree that it\'s happening in a far off place. We\'re a globally-connected society. It\'s hurting people who *we know personally*, no matter where we are.<br><br>Quoting: <a href="https://x.com/FLOTUS/status/1498106186922704896" rel="noopener noreferrer" target="_blank">@FLOTUS</a> <span class="status">1498106186922704896</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1507048662668435464',
    created: 1648143375000,
    type: 'post',
    text: 'You can now follow organizations on GitHub. Neat!',
    photos: ['<div class="item"><img class="photo" src="media/1507048662668435464.jpg"></div>'],
    likes: 5,
    retweets: 1
  },
  {
    id: '1506771073353850884',
    created: 1648077192000,
    type: 'quote',
    text: 'I don\'t get why the "human race" loves to tackle huge challenges (like the going to space), yet refuses to take on this one. None of the other successes mean a damn thing if we pass on this one.<br><br>Quoting: <a href="https://x.com/antonioguterres/status/1505992640843685899" rel="noopener noreferrer" target="_blank">@antonioguterres</a> <span class="status">1505992640843685899</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1506418792154468352',
    created: 1647993202000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/venkat_s" rel="noopener noreferrer" target="_blank">@venkat_s</a> <a class="mention" href="https://x.com/looselytyped" rel="noopener noreferrer" target="_blank">@looselytyped</a> <a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> 🎶 Nobody does it better. 🎶<br><br>In reply to: <a href="https://x.com/venkat_s/status/1506416972782788610" rel="noopener noreferrer" target="_blank">@venkat_s</a> <span class="status">1506416972782788610</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1505973443283488769',
    created: 1647887023000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fanf42" rel="noopener noreferrer" target="_blank">@fanf42</a> Look in the replies. Already on it ;)<br><br>In reply to: <a href="https://x.com/fanf42/status/1505971416860086276" rel="noopener noreferrer" target="_blank">@fanf42</a> <span class="status">1505971416860086276</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1505871579137425413',
    created: 1647862736000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> <a class="mention" href="https://x.com/aheritier" rel="noopener noreferrer" target="_blank">@aheritier</a> <a class="mention" href="https://x.com/PlantUML" rel="noopener noreferrer" target="_blank">@PlantUML</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> And you can use PlantUML via asciidoctor-kroki in Antora.<br><br>For Antora\'s own docs, we just use a good, old-fashioned literal block with some emoji bling. Could be scripted using a block macro.<br><br>In reply to: <a href="https://x.com/agoncal/status/1505855537593958401" rel="noopener noreferrer" target="_blank">@agoncal</a> <span class="status">1505855537593958401</span>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1505840272332140546',
    created: 1647855272000,
    type: 'post',
    text: 'attn: <a class="mention" href="https://x.com/usacurl" rel="noopener noreferrer" target="_blank">@usacurl</a><br><br>Quoting: <a href="#1505839641223520256">1505839641223520256</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1505839648001593345',
    created: 1647855123000,
    type: 'post',
    text: 'This change still gives a huge advantage to the team with last rock in the 1st EE. It\'s not enough for the other team to answer w/ a layup in the 2nd EE. They must get 2+. That\'s still really hard. And there\'s no chance for a tie. The game concludes in either the 1st or 2nd EE.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1505839646206431232',
    created: 1647855123000,
    type: 'post',
    text: 'The team that steals to force the EE could still steal in the EE to win. Nothing changes there. The rule change only addresses what happens if the team w/ last rock in the EE only gets 1 point. The rule says that\'s not a conclusive end. A 2nd EE is needed to resolve it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1505839644474175491',
    created: 1647855123000,
    type: 'post',
    text: 'I think this gives a steal in the 10th to force the EE more respect. Right now, the only way to win in that scenario is to steal twice in a row. It\'s a moonshot. It also makes the outcome increasingly a forgone conclusion. This rule change would make every throw in the EEs count.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1505839642930606086',
    created: 1647855122000,
    type: 'post',
    text: 'The reason I\'m floating this idea is because getting a point w/ last rock in curling is almost a guarantee, especially for elite competitors. The EE is just a layup. Not really fair. A 2nd EE gives the other team a chance to prove they\'re 1 better. Consistent w/ the other ends.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1505839641223520256',
    created: 1647855122000,
    type: 'post',
    text: 'I\'ve watched enough curling to be dangerous (w/ an opinion). I don\'t think the EE is fair. I admit I\'m an armchair curler, so take this from a spectator\'s viewpoint.<br><br>New EE rules: If team w/ last rock scores 2+, they win. Otherwise, other team must score 2+ in 2nd EE to win.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1505806591936790529',
    created: 1647847242000,
    type: 'quote',
    text: '💯<br><br>Quoting: <a href="https://x.com/mulindwa_guy/status/1505228799008555010" rel="noopener noreferrer" target="_blank">@mulindwa_guy</a> <span class="status">1505228799008555010</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1505654220183138305',
    created: 1647810914000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/laytoun" rel="noopener noreferrer" target="_blank">@laytoun</a> We\'ll do more than bump. We\'ll hug. We\'ll dance. And we\'ll celebrate like it\'s the first time.<br><br>In reply to: <a href="https://x.com/laytoun/status/1505648504034967552" rel="noopener noreferrer" target="_blank">@laytoun</a> <span class="status">1505648504034967552</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1505645255504191488',
    created: 1647808777000,
    type: 'post',
    text: 'After experiencing our first pandemic birthdays 2 years ago, we decided to do away with Happy Birthday in favor of Happy Birthweek. We don\'t celebrate a birth minute or birth hour, so why stop at a day? It\'s just one hack we use to help us get through all this. 🥳',
    likes: 3,
    retweets: 1
  },
  {
    id: '1505644091178061824',
    created: 1647808499000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobbytank42" rel="noopener noreferrer" target="_blank">@bobbytank42</a> Thanks Robert! I\'ve got a birthday-size 🍺 in the fridge ready to do just that. But then I\'ll be right back to doing what I love, which is working with and for the community on OSS! ⌨️<br><br>In reply to: <a href="https://x.com/bobbytank42/status/1505611205406306308" rel="noopener noreferrer" target="_blank">@bobbytank42</a> <span class="status">1505611205406306308</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1505643332109623298',
    created: 1647808318000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ameliaeiras" rel="noopener noreferrer" target="_blank">@ameliaeiras</a> Thanks Amelia! Birthday month, I love it!<br><br>If there\'s one thing we have going for us, it\'s that we\'ve been strong in the face of adversity. One way to think of it is that we upgraded to stronger 🔋🔋. Let\'s go! 🚀<br><br>In reply to: <a href="https://x.com/ameliaeiras/status/1505588090487803907" rel="noopener noreferrer" target="_blank">@ameliaeiras</a> <span class="status">1505588090487803907</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1505642682206490627',
    created: 1647808163000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> <a class="mention" href="https://x.com/DevoxxUK" rel="noopener noreferrer" target="_blank">@DevoxxUK</a> Far too long indeed. When we meet again, we\'ll cherish our time together more than ever. What\'s most important is that we stay connected in whatever way we can now. Thanks for thinking of me!<br><br>In reply to: <a href="https://x.com/alexsotob/status/1505587123700830213" rel="noopener noreferrer" target="_blank">@alexsotob</a> <span class="status">1505587123700830213</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1505642280316649474',
    created: 1647808067000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kenfinnigan" rel="noopener noreferrer" target="_blank">@kenfinnigan</a> Thanks Ken! And a belated congratulations on the release of your new book and your new role at Workday. It\'s been truly amazing to watch your journey at Red Hat. You\'ve done such amazing work and I know there\'s more where that came from. 🎉💻📚<br><br>In reply to: <a href="https://x.com/kenfinnigan/status/1505513857313353731" rel="noopener noreferrer" target="_blank">@kenfinnigan</a> <span class="status">1505513857313353731</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1505640230421872641',
    created: 1647807579000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/laytoun" rel="noopener noreferrer" target="_blank">@laytoun</a> <a class="mention" href="https://x.com/myfear" rel="noopener noreferrer" target="_blank">@myfear</a> Thanks Mohammed! I\'m ready for another trip around the sun, no matter what this world throws at us.<br><br>In reply to: <a href="https://x.com/laytoun/status/1505509115904339969" rel="noopener noreferrer" target="_blank">@laytoun</a> <span class="status">1505509115904339969</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1505639888862932993',
    created: 1647807497000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/__Cloudia" rel="noopener noreferrer" target="_blank">@__Cloudia</a> <a class="mention" href="https://x.com/pilhuhn" rel="noopener noreferrer" target="_blank">@pilhuhn</a> I\'d say it\'s a pretty great week for birthdays 😉. You can be sure I\'m going to make the most of the whole week.<br><br>In reply to: <a href="https://x.com/__Cloudia/status/1505491391408918529" rel="noopener noreferrer" target="_blank">@__Cloudia</a> <span class="status">1505491391408918529</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1505639604497498112',
    created: 1647807429000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pilhuhn" rel="noopener noreferrer" target="_blank">@pilhuhn</a> Thanks Heiko! I\'m saving up my beer tokens for when we get to once again share a 🍻 together at Oktoberfest.<br><br>In reply to: <a href="https://x.com/pilhuhn/status/1505477186530254851" rel="noopener noreferrer" target="_blank">@pilhuhn</a> <span class="status">1505477186530254851</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1505639169065816067',
    created: 1647807325000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Hanh08678053" rel="noopener noreferrer" target="_blank">@Hanh08678053</a> <a class="mention" href="https://x.com/myfear" rel="noopener noreferrer" target="_blank">@myfear</a> Wow, that\'s really nice of you to say. Thanks! I look forward to another year of lots and lots of OSS dev spent with great people from around the world. 🧑‍💻<br><br>In reply to: <a href="https://x.com/Hanh08678053/status/1505455728789708800" rel="noopener noreferrer" target="_blank">@Hanh08678053</a> <span class="status">1505455728789708800</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1505638880371904514',
    created: 1647807257000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AndyBai59873550" rel="noopener noreferrer" target="_blank">@AndyBai59873550</a> <a class="mention" href="https://x.com/myfear" rel="noopener noreferrer" target="_blank">@myfear</a> Thanks Andy! Nothing makes me happier than getting out some software releases, so that\'s what I\'ll be working on...with 🍺 in hand, of course!<br><br>In reply to: <a href="https://x.com/AndyBai59873550/status/1505453325248086022" rel="noopener noreferrer" target="_blank">@AndyBai59873550</a> <span class="status">1505453325248086022</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1505638473817939968',
    created: 1647807160000,
    type: 'reply',
    text: 'And <a class="mention" href="https://x.com/Nastia_DJ" rel="noopener noreferrer" target="_blank">@Nastia_DJ</a>,who\'s been using her platform to help get information out there about what\'s happening.<br><br>In reply to: <a href="#1499498690418864133">1499498690418864133</a>',
    photos: ['<div class="item"><img class="photo" src="media/1505638473817939968.jpg"></div>'],
    likes: 1,
    retweets: 0
  },
  {
    id: '1505635949975248896',
    created: 1647806558000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/myfear" rel="noopener noreferrer" target="_blank">@myfear</a> I know these feelings all too well as that\'s exactly what happened to us. It took a long time for our bodies to fight it off, but eventually we got past it (as far as we can tell). Still, the whole experience just sucks. Sending you lots of empathy and hugs from both of us.<br><br>In reply to: <a href="https://x.com/myfear/status/1504753103421296664" rel="noopener noreferrer" target="_blank">@myfear</a> <span class="status">1504753103421296664</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1505634955174416384',
    created: 1647806321000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/myfear" rel="noopener noreferrer" target="_blank">@myfear</a> First post! You\'re the best, Markus. I\'ll be sure to make it count. Thanks for thinking of me. I hope you and your family get well soon.<br><br>In reply to: <a href="https://x.com/myfear/status/1505452150041858050" rel="noopener noreferrer" target="_blank">@myfear</a> <span class="status">1505452150041858050</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1505314461741051906',
    created: 1647729909000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> ...the default practice, that is (meaning package-lock.json + npm ci). An argument could be made for enforcing it further, but that has rippling effects and would have to be done incrementally.<br><br>In reply to: <a href="#1505299917832523777">1505299917832523777</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1505299917832523777',
    created: 1647726442000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> Node.js already has these tools, and I would argue locking versions is the default. You can only lead a horse to water. Hard earned experience will teach the rest.<br><br>In reply to: <a href="https://x.com/brunoborges/status/1505238672169308161" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1505238672169308161</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1505239522878976003',
    created: 1647712042000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jponge" rel="noopener noreferrer" target="_blank">@jponge</a> I really liked that set. Gave me similar feels as Chris Stussy. I\'m connected to Ukraine through music and proud to show my support through the same channel.<br><br>In reply to: <a href="https://x.com/jponge/status/1503440375582765058" rel="noopener noreferrer" target="_blank">@jponge</a> <span class="status">1503440375582765058</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1504941954458554369',
    created: 1647641097000,
    type: 'post',
    text: 'And for context, my spouse almost certainly picked it up at the store. There\'s no other place we\'ve been for two years now.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1504941439075045381',
    created: 1647640974000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/__Cloudia" rel="noopener noreferrer" target="_blank">@__Cloudia</a> Thanks! And Happy Birthday!! 🎉🥳<br><br>In reply to: <a href="https://x.com/__Cloudia/status/1504940032293056519" rel="noopener noreferrer" target="_blank">@__Cloudia</a> <span class="status">1504940032293056519</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1504938512973373440',
    created: 1647640276000,
    type: 'post',
    text: 'I now consider myself back to full strength. I\'m doing my full workday and workouts without any trouble. But I still get a lingering irritation in my upper lungs every so often. As if we didn\'t already know it, COVID-19 sucks. Big time.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1504938511568232448',
    created: 1647640276000,
    type: 'post',
    text: 'We had to cancel our booster shots and wait until we recovered. So how do we arrive at today? That was the longest sickness I\'ve ever had in my entire life. It took well over a month to recover. I went through all the respiratory symptoms and did a hell of a lot of sleeping.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1504938510356123648',
    created: 1647640275000,
    type: 'post',
    text: 'A little backstory on this. A few days into the new year, I was scheduling our booster shots. The next morning, my spouse felt unwell and decided to get a PCR test. Positive. I went for mine. Negative. Fast forward a week, I started feeling unwell. PCR test again, positive.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1504919226766794753',
    created: 1647635678000,
    type: 'post',
    text: 'Boosted. 💉💉💉',
    likes: 13,
    retweets: 0
  },
  {
    id: '1504705377601945601',
    created: 1647584692000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fanf42" rel="noopener noreferrer" target="_blank">@fanf42</a> That\'s the danger (dare I say cancer) of these damn dependency bots like dependabot. They treat it as a bug to not update immediately. They promote a terrible practice. Now people are finding out the consequences. It\'s one thing to provide CVE notifications. They far overreach.<br><br>In reply to: <a href="https://x.com/fanf42/status/1504703832449859604" rel="noopener noreferrer" target="_blank">@fanf42</a> <span class="status">1504703832449859604</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1504702353106096129',
    created: 1647583971000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fanf42" rel="noopener noreferrer" target="_blank">@fanf42</a> I think it\'s a) size of ecosystem and b) smaller libraries whose maintainers don\'t seem to mind career suicide. But it\'s also the fault of users putting blind trust in any library that looks good. It\'s an important wake-up call. Not every library has the same credibility.<br><br>In reply to: <a href="https://x.com/fanf42/status/1504698999827218432" rel="noopener noreferrer" target="_blank">@fanf42</a> <span class="status">1504698999827218432</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1504584682733813760',
    created: 1647555916000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fanf42" rel="noopener noreferrer" target="_blank">@fanf42</a> I\'m not sure why you\'re singling out Node.js. This could be done in any package repository. It just happens to be an npm package.<br><br>I\'ve also said countless times that your dependencies are part of your code, so you must know &amp; trust them. This is a fundamental software principle.<br><br>In reply to: <a href="https://x.com/fanf42/status/1504572859666079748" rel="noopener noreferrer" target="_blank">@fanf42</a> <span class="status">1504572859666079748</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1504583701274894336',
    created: 1647555682000,
    type: 'quote',
    text: 'I condemn Russia\'s invasion of Ukraine unequivocally. I DO NOT support acts of terrorism as a retaliation measure. You accomplish nothing by fighting evil with evil. It only further erodes the fabric of our society. There are ways to protest. Hurting people by geography ain\'t it.<br><br>Quoting: <a href="https://x.com/snyksec/status/1504131791053111296" rel="noopener noreferrer" target="_blank">@snyksec</a> <span class="status">1504131791053111296</span>',
    likes: 17,
    retweets: 4
  },
  {
    id: '1504579719223672833',
    created: 1647554733000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> ♥️<br><br>In reply to: <a href="https://x.com/headius/status/1504572130301722625" rel="noopener noreferrer" target="_blank">@headius</a> <span class="status">1504572130301722625</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1504512889704501260',
    created: 1647538800000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/waynebeaton" rel="noopener noreferrer" target="_blank">@waynebeaton</a> I remember doing my first online presentation long before the pandemic. What I found to be true then, which likely applies now, is just to bring your passion for the subject. The audience, no matter how remote, will pick up on that energy. (Still a bummer to be far away, though).<br><br>In reply to: <a href="https://x.com/waynebeaton/status/1504506207385628690" rel="noopener noreferrer" target="_blank">@waynebeaton</a> <span class="status">1504506207385628690</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1504511498093154334',
    created: 1647538468000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <a class="mention" href="https://x.com/devnexus" rel="noopener noreferrer" target="_blank">@devnexus</a> I\'m very happy for you. Good luck, have fun, and enjoy the company!<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1504509869499437066" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1504509869499437066</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1504193872389320705',
    created: 1647462740000,
    type: 'post',
    text: 'It\'s finally possible to rerun only failed jobs in a GitHub Actions pipeline. This is huge for projects that must be tested on multiple runtimes and with different library combinations (aka matrix testing). <a href="https://github.blog/2022-03-16-save-time-partial-re-runs-github-actions/" rel="noopener noreferrer" target="_blank">github.blog/2022-03-16-save-time-partial-re-runs-github-actions/</a>',
    likes: 5,
    retweets: 0
  },
  {
    id: '1504191217105457153',
    created: 1647462107000,
    type: 'quote',
    text: '<a class="mention" href="https://x.com/jennifercord" rel="noopener noreferrer" target="_blank">@jennifercord</a> I thought you might relate to this one. 👇<br><br>Quoting: <a href="https://x.com/DCHammslice/status/1503876351207555076" rel="noopener noreferrer" target="_blank">@DCHammslice</a> <span class="status">1503876351207555076</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1504190408057712644',
    created: 1647461914000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DCHammslice" rel="noopener noreferrer" target="_blank">@DCHammslice</a> This is exactly the heartwarming story I needed today. #BornOnTheBay<br><br>In reply to: <a href="https://x.com/DCHammslice/status/1503876351207555076" rel="noopener noreferrer" target="_blank">@DCHammslice</a> <span class="status">1503876351207555076</span>',
    likes: 1,
    retweets: 0,
    tags: ['bornonthebay']
  },
  {
    id: '1504144311893774338',
    created: 1647450924000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/j2r2b" rel="noopener noreferrer" target="_blank">@j2r2b</a> It\'s not so much a decision as it is about being faithful to the syntax. AsciiDoc has worked this way for two decades.<br><br>That said, in Java, true becomes empty string (set) and false becomes unset. That\'s a mapping choice we made in the API to bridge the two domains.<br><br>In reply to: <a href="https://x.com/j2r2b/status/1504087108218204162" rel="noopener noreferrer" target="_blank">@j2r2b</a> <span class="status">1504087108218204162</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1504143067229655040',
    created: 1647450627000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/j2r2b" rel="noopener noreferrer" target="_blank">@j2r2b</a> Yes. We are succeeding tremendously with <a class="mention" href="https://x.com/zulip" rel="noopener noreferrer" target="_blank">@zulip</a>. There\'s no looking back now.<br><br>In reply to: <a href="https://x.com/j2r2b/status/1504087335474024457" rel="noopener noreferrer" target="_blank">@j2r2b</a> <span class="status">1504087335474024457</span>',
    likes: 4,
    retweets: 1
  },
  {
    id: '1504060764474146823',
    created: 1647431005000,
    type: 'post',
    text: 'The people who believe that anti-war demonstrations by Russian citizens are fake are probably the same people who believe that gas prices are real. #NoWar #StandWithUkraine 🇺🇦',
    likes: 3,
    retweets: 0,
    tags: ['nowar', 'standwithukraine']
  },
  {
    id: '1504050793606877184',
    created: 1647428627000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/j2r2b" rel="noopener noreferrer" target="_blank">@j2r2b</a> I encourage you to join us in the project chat so you can reach out for help when you get stuck on things like this. <a href="https://asciidoctor.zulipchat.com" rel="noopener noreferrer" target="_blank">asciidoctor.zulipchat.com</a>.<br><br>In reply to: <a href="#1504050476043542531">1504050476043542531</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1504050476043542531',
    created: 1647428552000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/j2r2b" rel="noopener noreferrer" target="_blank">@j2r2b</a> The correct syntax is:<br><br>:!sectnums:<br><br>In reply to: <a href="#1504050380597956609">1504050380597956609</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1504050380597956609',
    created: 1647428529000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/j2r2b" rel="noopener noreferrer" target="_blank">@j2r2b</a> That\'s correct. The AsciiDoc language does not recognize true and false values. Those are a programming construct. If the attribute is set, it is set. If the attribute is unset, it is unset. See <a href="https://docs.asciidoctor.org/asciidoc/latest/attributes/boolean-attributes/" rel="noopener noreferrer" target="_blank">docs.asciidoctor.org/asciidoc/latest/attributes/boolean-attributes/</a><br><br>In reply to: <a href="https://x.com/j2r2b/status/1504039976132128771" rel="noopener noreferrer" target="_blank">@j2r2b</a> <span class="status">1504039976132128771</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1503774721434742786',
    created: 1647362807000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/LeaVerou" rel="noopener noreferrer" target="_blank">@LeaVerou</a> I\'m partial to <a href="http://JavaScript.info" rel="noopener noreferrer" target="_blank">JavaScript.info</a>.<br><br>In reply to: <a href="https://x.com/LeaVerou/status/1503774178176028685" rel="noopener noreferrer" target="_blank">@LeaVerou</a> <span class="status">1503774178176028685</span>',
    likes: 7,
    retweets: 0
  },
  {
    id: '1503293416523788290',
    created: 1647248054000,
    type: 'quote',
    text: 'Thank you <a class="mention" href="https://x.com/MarkusSchulz" rel="noopener noreferrer" target="_blank">@MarkusSchulz</a> for this gesture and for raising awareness of this crime against humanity. Your words of hope calling for peace were meaningful and touching. The music reminded us of the tremendous contribution Ukraine citizens have made to the global EDM sound. 🎶🇺🇦<br><br>Quoting: <a href="https://x.com/MarkusSchulz/status/1501996695164489730" rel="noopener noreferrer" target="_blank">@MarkusSchulz</a> <span class="status">1501996695164489730</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1503292183905902594',
    created: 1647247761000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MarkusSchulz" rel="noopener noreferrer" target="_blank">@MarkusSchulz</a> <a class="mention" href="https://x.com/AdinaButar" rel="noopener noreferrer" target="_blank">@AdinaButar</a> Congrats! This is nature\'s way of thanking you for blessing this world with so many gifts. Your words and your music are always so genuine. May music continue to fill your hearts and your souls during this precious time for your family. That child will certainly be loved.<br><br>In reply to: <a href="https://x.com/MarkusSchulz/status/1499412442337660933" rel="noopener noreferrer" target="_blank">@MarkusSchulz</a> <span class="status">1499412442337660933</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1503290512941334530',
    created: 1647247362000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matkar" rel="noopener noreferrer" target="_blank">@matkar</a> <a class="mention" href="https://x.com/JavaLandConf" rel="noopener noreferrer" target="_blank">@JavaLandConf</a> <a class="mention" href="https://x.com/antonarhipov" rel="noopener noreferrer" target="_blank">@antonarhipov</a> I\'m holding my beer up to my screen right now. Skål ! 🍻<br><br>In reply to: <a href="https://x.com/matkar/status/1503285712317259776" rel="noopener noreferrer" target="_blank">@matkar</a> <span class="status">1503285712317259776</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1502717477611970560',
    created: 1647110740000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bsideup" rel="noopener noreferrer" target="_blank">@bsideup</a> <a class="mention" href="https://x.com/myfear" rel="noopener noreferrer" target="_blank">@myfear</a> Count me among those people who understand the difference. I stand with you. While I, too, am firmly against all war, I also speak out against xenophobia. That\'s not who we are and those of us without a blood relation to this conflict can and should be better than that.<br><br>In reply to: <a href="https://x.com/bsideup/status/1502696867137835013" rel="noopener noreferrer" target="_blank">@bsideup</a> <span class="status">1502696867137835013</span>',
    likes: 5,
    retweets: 0
  },
  {
    id: '1502050407857942531',
    created: 1646951698000,
    type: 'post',
    text: 'Just know that the powers-that-be desperately want you to want war. Be defiant. ☮️🇺🇦',
    likes: 3,
    retweets: 0
  },
  {
    id: '1501767875962433537',
    created: 1646884337000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DrDooleyMD" rel="noopener noreferrer" target="_blank">@DrDooleyMD</a> That we still don\'t have universal healthcare and that our government has failed us.<br><br>In reply to: <a href="https://x.com/DrDooleyMD/status/1501765764281475084" rel="noopener noreferrer" target="_blank">@DrDooleyMD</a> <span class="status">1501765764281475084</span>',
    likes: 12,
    retweets: 0
  },
  {
    id: '1501767627668017152',
    created: 1646884278000,
    type: 'post',
    text: 'If you think that fighter jets are the solution, you are dead wrong. That will only create more death and destruction in the near term and even more in the long run. The path to peace is the higher road we must pursue, even when it hurts to our very core.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1501767046920486914',
    created: 1646884140000,
    type: 'post',
    text: 'We just spent 2 years focused on saving lives. That should give us all the more reason to find a path to peace (and then serious, serious reconcilation).',
    likes: 6,
    retweets: 0
  },
  {
    id: '1501289837717979137',
    created: 1646770364000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> <a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> That\'s sweet! Just wait until we finish the PDF assembler extension for Antora. Then it will be truly seamless!<br><br>In reply to: <a href="https://x.com/mraible/status/1501288835216510976" rel="noopener noreferrer" target="_blank">@mraible</a> <span class="status">1501288835216510976</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1501289402181451776',
    created: 1646770260000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/trisha_gee" rel="noopener noreferrer" target="_blank">@trisha_gee</a> <a class="mention" href="https://x.com/fhinkel" rel="noopener noreferrer" target="_blank">@fhinkel</a> <a class="mention" href="https://x.com/intellijidea" rel="noopener noreferrer" target="_blank">@intellijidea</a> I\'m just so impressed by the work <a class="mention" href="https://x.com/ahus1de" rel="noopener noreferrer" target="_blank">@ahus1de</a> and team are doing on this plugin. It\'s definitely changing the conversation about what\'s possible when using an IDE for writing. It gets to the heart of what an IDE is there for. Not just for code, but for docs too.<br><br>In reply to: <a href="https://x.com/trisha_gee/status/1501076171974496257" rel="noopener noreferrer" target="_blank">@trisha_gee</a> <span class="status">1501076171974496257</span>',
    likes: 6,
    retweets: 0
  },
  {
    id: '1501175404241068036',
    created: 1646743081000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bmuskalla" rel="noopener noreferrer" target="_blank">@bmuskalla</a> <a class="mention" href="https://x.com/venkat_s" rel="noopener noreferrer" target="_blank">@venkat_s</a> 💯<br><br>In reply to: <a href="https://x.com/bmuskalla/status/1501165016699047938" rel="noopener noreferrer" target="_blank">@bmuskalla</a> <span class="status">1501165016699047938</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1501169046267510790',
    created: 1646741565000,
    type: 'quote',
    text: '"mobilising for feminist(-led) peace this International Women’s Day is both an ideological imperative and a practical reality"<br><br>Quoting: <a href="https://x.com/tribunemagazine/status/1501155718673707009" rel="noopener noreferrer" target="_blank">@tribunemagazine</a> <span class="status">1501155718673707009</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1501099216638074881',
    created: 1646724916000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/trisha_gee" rel="noopener noreferrer" target="_blank">@trisha_gee</a> <a class="mention" href="https://x.com/DaliaShea" rel="noopener noreferrer" target="_blank">@DaliaShea</a> You all have done amazing work together. It\'s been a joy to witness.<br><br>In reply to: <a href="https://x.com/trisha_gee/status/1499798873065328648" rel="noopener noreferrer" target="_blank">@trisha_gee</a> <span class="status">1499798873065328648</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1500080422604128256',
    created: 1646482017000,
    type: 'post',
    text: 'The climate movement is a peace movement. 🌍☮️',
    likes: 1,
    retweets: 0
  },
  {
    id: '1500079785673900036',
    created: 1646481865000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SilvesteR747" rel="noopener noreferrer" target="_blank">@SilvesteR747</a> 👍 Seems to be the consensus.<br><br>In reply to: <a href="https://x.com/SilvesteR747/status/1500065778837639170" rel="noopener noreferrer" target="_blank">@SilvesteR747</a> <span class="status">1500065778837639170</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1500044106474541056',
    created: 1646473359000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SilvesteR747" rel="noopener noreferrer" target="_blank">@SilvesteR747</a> But what do you call them when published?<br><br>In reply to: <a href="https://x.com/SilvesteR747/status/1500041321897156609" rel="noopener noreferrer" target="_blank">@SilvesteR747</a> <span class="status">1500041321897156609</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1499909890436784129',
    created: 1646441359000,
    type: 'post',
    text: '...and it\'s going to have a ridiculous number of tests. 🧪',
    likes: 3,
    retweets: 0
  },
  {
    id: '1499900881495461891',
    created: 1646439211000,
    type: 'post',
    text: 'Woah! And the site just got a major design overhaul!<br><br>Quoting: <a href="#1496789224183386112">1496789224183386112</a>',
    photos: ['<div class="item"><img class="photo" src="media/1499900881495461891.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1499856554895425536',
    created: 1646428643000,
    type: 'post',
    text: 'It\'s kind of amazing that even at their top speeds, downhill racers barely outpace a cheetah in the flats. Cheetahs are freakin\' fast. Cheetahs also don\'t crash.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1499851834004938756',
    created: 1646427517000,
    type: 'post',
    text: 'I\'ll admit I\'ve been rather obsessed lately with watching 🥌. I almost don\'t even care who wins. I just find the strategy of each end to be fascinating...and to see the position of the stones develop. There have been some real 💅 and 🥌 biters!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1499851281162145795',
    created: 1646427385000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/blairnangle" rel="noopener noreferrer" target="_blank">@blairnangle</a> Interesting. 🤔<br><br>In reply to: <a href="https://x.com/blairnangle/status/1499851084030005249" rel="noopener noreferrer" target="_blank">@blairnangle</a> <span class="status">1499851084030005249</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1499851213398937601',
    created: 1646427369000,
    type: 'post',
    text: 'To help me get through this trying time mentally, I\'ve decide to solve an issue that\'s been hanging over my head for years. I\'m going to finally get Asciidoctor PDF 2 released. It\'s not an easy task, but I\'m committed to finishing it.',
    likes: 35,
    retweets: 1
  },
  {
    id: '1499850000976670721',
    created: 1646427080000,
    type: 'post',
    text: 'If the US doesn\'t end the fossil fuel industry posthaste, then it\'s show support for Ukraine is baseless. It\'s the money and power in that industry that fuels wars. We know this. And yet, we\'re somehow fine with doing nothing about it. And even subsidizing it. Unacceptable.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1499846400137306114',
    created: 1646426222000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/michbarsinai" rel="noopener noreferrer" target="_blank">@michbarsinai</a> Let\'s take back the term!<br><br>In reply to: <a href="https://x.com/michbarsinai/status/1499846227558641675" rel="noopener noreferrer" target="_blank">@michbarsinai</a> <span class="status">1499846227558641675</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1499845184695062529',
    created: 1646425932000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/michbarsinai" rel="noopener noreferrer" target="_blank">@michbarsinai</a> I was kind of looking more for a term rather than a description...something I can use in a URL for example.<br><br>In reply to: <a href="https://x.com/michbarsinai/status/1499843053628145666" rel="noopener noreferrer" target="_blank">@michbarsinai</a> <span class="status">1499843053628145666</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1499845011717775363',
    created: 1646425891000,
    type: 'post',
    text: 'His reply: “I\'ve shared your message with my family. What you said is very heart-touching. Your words mean that we\'re not put against the whole world; that there are people outside who understand the huge gap between government &amp; individuals who happen to live here. Thank you.”',
    likes: 6,
    retweets: 0
  },
  {
    id: '1499842957884604420',
    created: 1646425401000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> 💯<br><br>In reply to: <a href="https://x.com/maxandersen/status/1499839938040406017" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1499839938040406017</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1499838149240901638',
    created: 1646424255000,
    type: 'post',
    text: 'What\'s the common name for Javadoc, RubyDoc, JSDoc, etc. Is it "apidoc"? How is it not confused with documentation for REST endpoints?',
    likes: 2,
    retweets: 0
  },
  {
    id: '1499698120539529217',
    created: 1646390869000,
    type: 'post',
    text: 'I denounce the actions of the Russian state &amp; military, and I pray for peace for Ukraine &amp; its citizens (where I also have friends), but I will not allow the world leaders to divide us in OSS. I know my cohorts in Russia want peace too. We can work on both fronts to achieve it.',
    likes: 5,
    retweets: 0
  },
  {
    id: '1499696439240237060',
    created: 1646390468000,
    type: 'post',
    text: 'I had to send my first ever correspondence to a friend on the other side of dividing lines. And it felt absolutely terrible. It puts the video I\'ve seen of the curtain being pulled through Berlin in ’61 in a personal context. We must stop this. In Open Source, we develop as one.',
    likes: 6,
    retweets: 0
  },
  {
    id: '1499498690418864133',
    created: 1646343321000,
    type: 'post',
    text: 'I was remiss not to mention Daria Kolosova as well.',
    photos: ['<div class="item"><img class="photo" src="media/1499498690418864133.jpg"></div>'],
    likes: 2,
    retweets: 0
  },
  {
    id: '1499496437578534913',
    created: 1646342784000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SteveLAnderson" rel="noopener noreferrer" target="_blank">@SteveLAnderson</a> For Ruby I used RuboCop (which uses Antora for docs, btw), and for JavaScript I use ESLint with JS Standard and Prettier (a bit more complex of a setup atm). Then, I just run it with the test suite.<br><br>In reply to: <a href="https://x.com/SteveLAnderson/status/1499480836764155922" rel="noopener noreferrer" target="_blank">@SteveLAnderson</a> <span class="status">1499480836764155922</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1499477022929670144',
    created: 1646338155000,
    type: 'post',
    text: 'I\'ve become a huge fan of automatic code formatting (given I can tune the rules to my liking). Since I set up this task in my projects, I\'ve stopped thinking about code formatting entirely. I focus on the logic I want to write and let the task reflow the code before I commit it.',
    likes: 7,
    retweets: 0
  },
  {
    id: '1499309870905847809',
    created: 1646298303000,
    type: 'post',
    text: 'I really wish the platform would just support using SVG (and all platforms, for that matter). SVG is how we can scale logos without any loss in quality. And yet, platforms still insist on using raster images.',
    likes: 3,
    retweets: 1
  },
  {
    id: '1499309190531088389',
    created: 1646298141000,
    type: 'post',
    text: 'Does anyone know a trick for how to get the project avatar on <a class="mention" href="https://x.com/gitlab" rel="noopener noreferrer" target="_blank">@gitlab</a> to not look blurry? No matter how large the file I upload is, it always results in the same low quality downsampling.<br><br>Here\'s an example: <a href="https://gitlab.com/gitlab-org/gitlab" rel="noopener noreferrer" target="_blank">gitlab.com/gitlab-org/gitlab</a>',
    likes: 0,
    retweets: 1
  },
  {
    id: '1498748789846249472',
    created: 1646164531000,
    type: 'post',
    text: '',
    photos: ['<div class="item"><img class="photo" src="media/1498748789846249472.jpg"></div>', '<div class="item"><img class="photo" src="media/1498748789846249472-2.jpg"></div>'],
    likes: 3,
    retweets: 1
  },
  {
    id: '1498746130565255170',
    created: 1646163897000,
    type: 'post',
    text: 'Their sets entertained us, took us to far off places, &amp; made us feel less alone. They\'ve now been forced out of their studios because their world is under siege. It\'s absolutely 💔 that they gave so much of themselves &amp; now they have to suffer. This is the cost of war. 😡 (2/2)',
    likes: 2,
    retweets: 0
  },
  {
    id: '1498746129290174466',
    created: 1646163897000,
    type: 'post',
    text: 'I want to share a personal connection I have with the invasion of Ukraine. Since the pandemic began, my partner &amp; I discovered so many amazing woman DJs from Ukraine through Radio Intense, including Xenia UA, Miss Monique, &amp; Korolova. 🇺🇦🎧 (1/2)',
    likes: 10,
    retweets: 0
  },
  {
    id: '1498639787443453958',
    created: 1646138543000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CedricChampeau" rel="noopener noreferrer" target="_blank">@CedricChampeau</a> <a class="mention" href="https://x.com/sebi2706" rel="noopener noreferrer" target="_blank">@sebi2706</a> Extremely. And I\'m half way around the world. I just can\'t fathom the suffering others are going though (but I\'m certainly trying).<br><br>In reply to: <a href="https://x.com/CedricChampeau/status/1498637075792502793" rel="noopener noreferrer" target="_blank">@CedricChampeau</a> <span class="status">1498637075792502793</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1498581671729319940',
    created: 1646124687000,
    type: 'post',
    text: 'I\'ve seen a number of people apologize here for posting about current events. You do not have to apologize for being you. You are more than the person other people think you are. Don\'t let them put you in a box.',
    likes: 7,
    retweets: 0
  },
  {
    id: '1498466806616379392',
    created: 1646097301000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/shaunm" rel="noopener noreferrer" target="_blank">@shaunm</a> GitHub Pages is the weakest part of GitHub. It lacks a rewrite/redirects engine, requires a GitHub Action to publish (whereas GitLab Pages has this out of the box), and doesn\'t provide any sort of integration to do deploy previews (which you can do on GitLab using review apps).<br><br>In reply to: <a href="https://x.com/shaunm/status/1498465349414846465" rel="noopener noreferrer" target="_blank">@shaunm</a> <span class="status">1498465349414846465</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1498466269464391682',
    created: 1646097173000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/shaunm" rel="noopener noreferrer" target="_blank">@shaunm</a> It\'s a misconception that you have to host the repository on GitLab to use GitLab CI and GitLab Pages. GitLab CI can use a repository on GitHub and deploy to GitLab Pages.<br><br>In reply to: <a href="https://x.com/shaunm/status/1498465349414846465" rel="noopener noreferrer" target="_blank">@shaunm</a> <span class="status">1498465349414846465</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1498458912013443077',
    created: 1646095419000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fanf42" rel="noopener noreferrer" target="_blank">@fanf42</a> Like we learned with the pandemic, a lot of strategic improvements only lacked political will. It was never a technical challenge.<br><br>In reply to: <a href="https://x.com/fanf42/status/1498448058459295746" rel="noopener noreferrer" target="_blank">@fanf42</a> <span class="status">1498448058459295746</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1498413868933992448',
    created: 1646084680000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/domdorn" rel="noopener noreferrer" target="_blank">@domdorn</a> Oh shoot. I was hoping if I switched to the Managed Services, it could at least control it there. Supposedly it\'s this option <a href="https://docs.openshift.com/container-platform/4.9/authentication/configuring-oauth-clients.html#oauth-register-additional-client_configuring-oauth-clients" rel="noopener noreferrer" target="_blank">docs.openshift.com/container-platform/4.9/authentication/configuring-oauth-clients.html#oauth-register-additional-client_configuring-oauth-clients</a>, which cannot be run in the DevSandbox.<br><br>In reply to: <a href="https://x.com/domdorn/status/1498405811713937419" rel="noopener noreferrer" target="_blank">@domdorn</a> <span class="status">1498405811713937419</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1498395043299684352',
    created: 1646080191000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/shaunm" rel="noopener noreferrer" target="_blank">@shaunm</a> GitLab / GitLab CI / GitLab Pages. It can go toe to toe with Netlify.<br><br>In reply to: <a href="https://x.com/shaunm/status/1498341918023069699" rel="noopener noreferrer" target="_blank">@shaunm</a> <span class="status">1498341918023069699</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1498119503825104900',
    created: 1646014498000,
    type: 'post',
    text: 'Is it true that in the OpenShift Developer sandbox, I can only get an API token for using `oc` that lasts 24 hours? Is there any way to change that?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1498068774951407619',
    created: 1646002403000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <a class="mention" href="https://x.com/ZelenskyyUa" rel="noopener noreferrer" target="_blank">@ZelenskyyUa</a> 💯<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1498064743730323460" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1498064743730323460</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1497796574021177346',
    created: 1645937505000,
    type: 'post',
    text: 'The GitLab container registry is great, and is very easy to use from a CI job within the same project. However, the fact that it does not allow anonymous reads is a real drawback.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1497656840603127810',
    created: 1645904190000,
    type: 'quote',
    text: 'Twitter is acknowledging here that Tweet recommendations help spread abusive content. Just think about that.<br><br>Quoting: <a href="https://x.com/Safety/status/1497353973375799296" rel="noopener noreferrer" target="_blank">@Safety</a> <span class="status">1497353973375799296</span>',
    likes: 7,
    retweets: 7
  },
  {
    id: '1497656456396509186',
    created: 1645904098000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> 💯<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/1497582536691748866" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">1497582536691748866</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1497170810137501696',
    created: 1645788311000,
    type: 'post',
    text: 'And it\'s never ever too late for a diplomatic resolution.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1497170293239910403',
    created: 1645788188000,
    type: 'post',
    text: 'The one beacon of hope is that young people and their allies, across borders, are once again rising up and rejecting this cycle of imperialism, despotism, narcissism, and destruction. My message to them, and to all citizens of the world, is to demand peace.',
    likes: 4,
    retweets: 1
  },
  {
    id: '1497170292145213443',
    created: 1645788188000,
    type: 'post',
    text: 'Among those impacted are people who I care about and respect on a personal and professional level. It sickens me to the core. I stand with the people of Ukraine as long as they stand for peace. There\'s no place in our world for war. Not now. Not ever again. 🇺🇦☮️',
    likes: 5,
    retweets: 2
  },
  {
    id: '1497170290396184576',
    created: 1645788187000,
    type: 'post',
    text: 'This has been one of the most dreadful weeks I can remember in decades. Above all, my heart aches for the people whose lives are being unnecessarily destroyed by an indefensible act of aggression thrust on them by Putin.',
    likes: 6,
    retweets: 1
  },
  {
    id: '1496996404564095010',
    created: 1645746730000,
    type: 'post',
    text: '',
    photos: ['<div class="item"><img class="photo" src="media/1496996404564095010.jpg"></div>'],
    likes: 7,
    retweets: 1
  },
  {
    id: '1496956293453647874',
    created: 1645737167000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/buildsghost" rel="noopener noreferrer" target="_blank">@buildsghost</a> 💯<br><br>In reply to: <a href="https://x.com/buildsghost/status/1496634518752530432" rel="noopener noreferrer" target="_blank">@buildsghost</a> <span class="status">1496634518752530432</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1496945948110426119',
    created: 1645734700000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/manekinekko" rel="noopener noreferrer" target="_blank">@manekinekko</a> <a class="mention" href="https://x.com/dSebastien" rel="noopener noreferrer" target="_blank">@dSebastien</a> I encourage you to join us in the project chat at <a href="https://asciidoctor.zulipchat.com" rel="noopener noreferrer" target="_blank">asciidoctor.zulipchat.com</a>. It provides a lot of opportunity to learn from others that are on a similar journey as you. Best of luck with your book!<br><br>In reply to: <a href="https://x.com/manekinekko/status/1496757960810188806" rel="noopener noreferrer" target="_blank">@manekinekko</a> <span class="status">1496757960810188806</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1496801338352562179',
    created: 1645700222000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/voxpelli" rel="noopener noreferrer" target="_blank">@voxpelli</a> <a class="mention" href="https://x.com/MozDevNet" rel="noopener noreferrer" target="_blank">@MozDevNet</a> <a class="mention" href="https://x.com/OpenWebDocs" rel="noopener noreferrer" target="_blank">@OpenWebDocs</a> <a class="mention" href="https://x.com/opencollect" rel="noopener noreferrer" target="_blank">@opencollect</a> Great idea!<br><br>In reply to: <a href="https://x.com/voxpelli/status/1496797129171509250" rel="noopener noreferrer" target="_blank">@voxpelli</a> <span class="status">1496797129171509250</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1496789224183386112',
    created: 1645697334000,
    type: 'post',
    text: 'I couldn\'t do my job effectively (if at all on some days) without the web docs on MDN (<a class="mention" href="https://x.com/MozDevNet" rel="noopener noreferrer" target="_blank">@MozDevNet</a>). I want to give a shout out to the people who make that site the excellent source of information that it is. 👏',
    likes: 71,
    retweets: 7
  },
  {
    id: '1496685622190301185',
    created: 1645672634000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> This only affirms that we truly are friends.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1496673332996964353',
    created: 1645669704000,
    type: 'post',
    text: 'If you put someone down so you can feel big, you are, in fact, small.',
    likes: 4,
    retweets: 1
  },
  {
    id: '1496654357483323395',
    created: 1645665179000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CJLovesData1" rel="noopener noreferrer" target="_blank">@CJLovesData1</a> Thanks for the super helpful feedback. It\'s really sad to see this comment coming from someone who represents Neo4j, an organization which has been a steadfast advocate of AsciiDoc over the years. Honestly, it just hurts. It really hurts.<br><br>In reply to: <a href="https://x.com/CJLovesData1/status/1496599552580747264" rel="noopener noreferrer" target="_blank">@CJLovesData1</a> <span class="status">1496599552580747264</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1496389335620505603',
    created: 1645601993000,
    type: 'post',
    text: 'I post to a public issue tracker and there the issue is. I can link to it straight away. I post to a mailing list and hours later it still isn\'t in the archive. I\'m done with mailing lists. They are dinosaurs of collaboration.',
    likes: 5,
    retweets: 0
  },
  {
    id: '1495912424951922688',
    created: 1645488289000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> You\'ll like this. I joined Twitter in preparation for JavaOne 2008.<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1495775082651729921" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1495775082651729921</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1495829539410386949',
    created: 1645468528000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nithyaruff" rel="noopener noreferrer" target="_blank">@nithyaruff</a> <a class="mention" href="https://x.com/ashleymcnamara" rel="noopener noreferrer" target="_blank">@ashleymcnamara</a> Thank you for asking this question. It\'s such an important one. I\'m interested to hear what other feedback you receive.<br><br>In reply to: <a href="https://x.com/nithyaruff/status/1495743046834655232" rel="noopener noreferrer" target="_blank">@nithyaruff</a> <span class="status">1495743046834655232</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1495720516174647296',
    created: 1645442534000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nithyaruff" rel="noopener noreferrer" target="_blank">@nithyaruff</a> <a class="mention" href="https://x.com/ashleymcnamara" rel="noopener noreferrer" target="_blank">@ashleymcnamara</a> The company itself isn\'t really going to know how to contribute, except financially. The employees who participate in the projects do. The companies just need to let it happen.<br><br>In reply to: <a href="#1495720299530272773">1495720299530272773</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1495720299530272773',
    created: 1645442483000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nithyaruff" rel="noopener noreferrer" target="_blank">@nithyaruff</a> <a class="mention" href="https://x.com/ashleymcnamara" rel="noopener noreferrer" target="_blank">@ashleymcnamara</a> Give their employees time to work on open source projects (a few hours a week). More times than not, I hear "my employer doesn\'t allow me to work on open source projects". This has to change.<br><br>In reply to: <a href="https://x.com/nithyaruff/status/1495497604574158848" rel="noopener noreferrer" target="_blank">@nithyaruff</a> <span class="status">1495497604574158848</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1494760401367486465',
    created: 1645213625000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mundacho" rel="noopener noreferrer" target="_blank">@mundacho</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/dejcup" rel="noopener noreferrer" target="_blank">@dejcup</a> That\'s great to hear. I really need to spend some time using it. We definitely need to keep moving tooling for AsciiDoc forward.<br><br>In reply to: <a href="https://x.com/mundacho/status/1494689059272331264" rel="noopener noreferrer" target="_blank">@mundacho</a> <span class="status">1494689059272331264</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1494066963282284550',
    created: 1645048297000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bbohmann" rel="noopener noreferrer" target="_blank">@bbohmann</a> Doppelgänger!<br><br>In reply to: <a href="https://x.com/bbohmann/status/974673476211695617" rel="noopener noreferrer" target="_blank">@bbohmann</a> <span class="status">974673476211695617</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1494060074980962307',
    created: 1645046654000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/hsablonniere" rel="noopener noreferrer" target="_blank">@hsablonniere</a> I\'ll never forget the look on Bruno\'s face when you made him aware of this fact.<br><br>In reply to: <a href="https://x.com/hsablonniere/status/1494059230055309314" rel="noopener noreferrer" target="_blank">@hsablonniere</a> <span class="status">1494059230055309314</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1494021740032262144',
    created: 1645037515000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ecoreply" rel="noopener noreferrer" target="_blank">@ecoreply</a> You could have just stopped at "People have to read" ;)<br><br>In reply to: <a href="https://x.com/ecoreply/status/1493983990293798912" rel="noopener noreferrer" target="_blank">@ecoreply</a> <span class="status">1493983990293798912</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1493876537266823173',
    created: 1645002896000,
    type: 'post',
    text: 'One way you can get your voice heard is to offer to write up what you learned to help the next person who comes along with the same question. The project leads don\'t want to answer the same questions over and over, so that makes helping you worthwhile.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1493874432854487044',
    created: 1645002394000,
    type: 'post',
    text: 'For projects I maintain, I monitor questions in the chat closely and try to improve the docs in response. But that process takes time and you shouldn\'t rely on it as an immediate solution. Your question might trigger a series of events that leads to an improvement down the road.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1493873856120954886',
    created: 1645002256000,
    type: 'post',
    text: 'Projects will typically offer a community forum or chat to help with these situations. That\'s another place you can turn. But you have to remember that everyone there is participating at will. You might get immediate help or none at all. It\'s give and take (aka quid pro quo).',
    likes: 1,
    retweets: 0
  },
  {
    id: '1493873206259699715',
    created: 1645002101000,
    type: 'post',
    text: 'There are those who need to be reminded that an issue tracker for an open source project is not a support portal. If you can\'t figure out how to configure the software, and you need it working now, you likely need to hire help. (And yes, it\'s a sign the docs could be better).',
    likes: 7,
    retweets: 0
  },
  {
    id: '1493739836464721920',
    created: 1644970304000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ixchelruiz" rel="noopener noreferrer" target="_blank">@ixchelruiz</a> OMG! Sending you massive bear hugs. ♥️🫂♥️ I\'ll 🙏 that it heals up as good as new!!<br><br>In reply to: <a href="https://x.com/ixchelruiz/status/1493175779467075585" rel="noopener noreferrer" target="_blank">@ixchelruiz</a> <span class="status">1493175779467075585</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1493163208424652802',
    created: 1644832825000,
    type: 'post',
    text: 'It frustrates me that GitHub is sitting on this code. It would be a game changer for the technical writing trade if they opened it up.',
    likes: 6,
    retweets: 1
  },
  {
    id: '1493160607238868992',
    created: 1644832205000,
    type: 'post',
    text: 'Are there any good (active) libraries that mark up differences in the rendered output of two HTML files like GitHub\'s rich diff view?',
    likes: 9,
    retweets: 2
  },
  {
    id: '1492762826410053634',
    created: 1644737366000,
    type: 'reply',
    text: '@jbryant787 It turns out that celery root is not the root of celery that we eat. But it\'s in the same family. <a href="https://thecookful.com/celery-root-the-root-of-celery/" rel="noopener noreferrer" target="_blank">thecookful.com/celery-root-the-root-of-celery/</a><br><br>In reply to: <a href="#1492762102586437640">1492762102586437640</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1492762102586437640',
    created: 1644737194000,
    type: 'reply',
    text: '@jbryant787 Neither did I until a few months ago. The root tastes like celery extract. It boosts the flavor of soups and works amazing well as a meat substitute in a roast. It has a mouth watering texture (akin to tender carrot). Truly an underrated gift from nature.<br><br>In reply to: @jbryant787 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1492450274383831045',
    created: 1644662848000,
    type: 'post',
    text: 'Where has celery root been all my life, and why did no one ever tell me how awesome it tastes?!?',
    likes: 1,
    retweets: 0
  },
  {
    id: '1491954510511542272',
    created: 1644544649000,
    type: 'post',
    text: 'I found the answer here: <a href="https://gitlab.gnome.org/GNOME/gnome-screenshot/-/issues/116#note_898536" rel="noopener noreferrer" target="_blank">gitlab.gnome.org/GNOME/gnome-screenshot/-/issues/116#note_898536</a> In fact, it even allows me to customize the drop shadow before taking the screenshot.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1491894229555052549',
    created: 1644530277000,
    type: 'post',
    text: 'Does anyone know how to configure gnome-screenshot so it does not add the drop shadow border effect when taking a screenshot of a window? This used to be an option, but it seems to have been removed.',
    likes: 1,
    retweets: 1
  },
  {
    id: '1491612839844278273',
    created: 1644463188000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> <a class="mention" href="https://x.com/TriviaMafia" rel="noopener noreferrer" target="_blank">@TriviaMafia</a> 🤣<br><br>In reply to: <a href="https://x.com/headius/status/1491608416586022912" rel="noopener noreferrer" target="_blank">@headius</a> <span class="status">1491608416586022912</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1490782598242570241',
    created: 1644265243000,
    type: 'post',
    text: 'As I observe some of the events at the Olympics, I\'m astounded by the consistency of mask wearing. Had we done this from the very start, we wouldn\'t be in the situation we are in today and countless lives would have been saved. It shows humans had the will power all along. 😷',
    likes: 4,
    retweets: 0
  },
  {
    id: '1489356409464496129',
    created: 1643925213000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_Mark_Atwood" rel="noopener noreferrer" target="_blank">@_Mark_Atwood</a> <a class="mention" href="https://x.com/gkrizek" rel="noopener noreferrer" target="_blank">@gkrizek</a> Zulip, hands down.<br><br>In reply to: <a href="https://x.com/_Mark_Atwood/status/1489351863904321536" rel="noopener noreferrer" target="_blank">@_Mark_Atwood</a> <span class="status">1489351863904321536</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1489355774648147971',
    created: 1643925062000,
    type: 'reply',
    text: '@monicaexplains 🙋🏼‍♂️<br><br>In reply to: <a href="https://x.com/mariemoni44/status/1489259565417828356" rel="noopener noreferrer" target="_blank">@mariemoni44</a> <span class="status">1489259565417828356</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1489180856925097987',
    created: 1643883358000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DaschnerS" rel="noopener noreferrer" target="_blank">@DaschnerS</a> That\'s the approach I\'m leaning towards. I just wonder if I\'m relying on Vim as a crutch and should learn the Emacs bindings instead. (I can\'t use arrow keys motion, so it has to be Vim or Emacs).<br><br>In reply to: <a href="https://x.com/DaschnerS/status/1489179236686520326" rel="noopener noreferrer" target="_blank">@DaschnerS</a> <span class="status">1489179236686520326</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1489034858240233473',
    created: 1643848549000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SteveLAnderson" rel="noopener noreferrer" target="_blank">@SteveLAnderson</a> The struggle is real. It\'s tough when every keystroke ends up being mapped to some action. The good ones are always taken.<br><br>In reply to: <a href="https://x.com/SteveLAnderson/status/1489028910297669637" rel="noopener noreferrer" target="_blank">@SteveLAnderson</a> <span class="status">1489028910297669637</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1489020492539314179',
    created: 1643845124000,
    type: 'post',
    text: 'Should I build the muscle memory to learn Emacs shortcuts in IntelliJ IDEA, or should I rely on my familiarity with VIM keybindings? Decisions, decisions.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1488811357864562693',
    created: 1643795263000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SturtIson" rel="noopener noreferrer" target="_blank">@SturtIson</a> <a class="mention" href="https://x.com/intellijidea" rel="noopener noreferrer" target="_blank">@intellijidea</a> What I decided to do instead was to only enable the soft wrap sign at the start of the wrapped line, modify the symbol,<br><br>idea.editor.wrap.soft.before.code=FEFF<br>idea.editor.wrap.soft.after.code=2937<br><br>then assign it a muted color. Here\'s how that looks:<br><br>In reply to: <a href="#1488810687665106944">1488810687665106944</a>',
    photos: ['<div class="item"><img class="photo" src="media/1488811357864562693.png"></div>'],
    likes: 6,
    retweets: 1
  },
  {
    id: '1488810687665106944',
    created: 1643795103000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SturtIson" rel="noopener noreferrer" target="_blank">@SturtIson</a> <a class="mention" href="https://x.com/intellijidea" rel="noopener noreferrer" target="_blank">@intellijidea</a> However, I noticed that the editor would still show the space between words at the start of the wrapped line in some cases, causing the indentation to be ragged.<br><br>In reply to: <a href="#1488809746509434882">1488809746509434882</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1488809746509434882',
    created: 1643794878000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SturtIson" rel="noopener noreferrer" target="_blank">@SturtIson</a> <a class="mention" href="https://x.com/intellijidea" rel="noopener noreferrer" target="_blank">@intellijidea</a> We discovered the following hack using custom properties:<br><br>idea.editor.wrap.soft.before.code=FEFF<br>idea.editor.wrap.soft.after.code=FEFF<br><br>In reply to: <a href="https://x.com/SturtIson/status/1488741536342941697" rel="noopener noreferrer" target="_blank">@SturtIson</a> <span class="status">1488741536342941697</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1488737960887402496',
    created: 1643777763000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SturtIson" rel="noopener noreferrer" target="_blank">@SturtIson</a> Even with 0, it still indents, presumably to leave room for the wrap indicator.<br><br>In reply to: <a href="https://x.com/SturtIson/status/1488737258454794241" rel="noopener noreferrer" target="_blank">@SturtIson</a> <span class="status">1488737258454794241</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1488683797671395329',
    created: 1643764850000,
    type: 'post',
    text: 'Does anyone know how to remove the hanging indent that IntelliJ IDEA applies to lines that are soft wrapped? I find the indentation to be incredibly distracting.',
    photos: ['<div class="item"><img class="photo" src="media/1488683797671395329.jpg"></div>'],
    likes: 4,
    retweets: 1
  },
  {
    id: '1488260852310589454',
    created: 1643664012000,
    type: 'quote',
    text: 'Amazing story. You know who else is amazing? <a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a>, that\'s who. Happy Birthday my friend!<br><br>Quoting: <a href="https://x.com/starbuxman/status/1487929994315665409" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">1487929994315665409</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1488250336204627973',
    created: 1643661505000,
    type: 'post',
    text: 'Simple pro tip: View the source of an AsciiDoc document on GitHub by appending ?plain=1 to the URL. See <a href="https://github.com/asciidoctor/asciidoctor/blob/main/README.adoc?plain=1" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor/blob/main/README.adoc?plain=1</a>',
    likes: 11,
    retweets: 8
  },
  {
    id: '1486837041006624768',
    created: 1643324549000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/looselytyped" rel="noopener noreferrer" target="_blank">@looselytyped</a> My first memory is configuring a webserver using the Sams teach yourself in 24 hours series. It was so thrilling.<br><br>In reply to: <a href="https://x.com/looselytyped/status/1486824535336833033" rel="noopener noreferrer" target="_blank">@looselytyped</a> <span class="status">1486824535336833033</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1486826233778225155',
    created: 1643321972000,
    type: 'post',
    text: 'I\'m loving npm 8, but it\'s driving me nuts that it does not honor --no-progress. <a href="https://github.com/npm/cli/issues/4259" rel="noopener noreferrer" target="_blank">github.com/npm/cli/issues/4259</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1486799635242893314',
    created: 1643315631000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/p_brunenberg" rel="noopener noreferrer" target="_blank">@p_brunenberg</a> <a class="mention" href="https://x.com/DaschnerS" rel="noopener noreferrer" target="_blank">@DaschnerS</a> I\'ve always found a technical workout solves it (like swimming or cardio strength). The workout forces my brain to stop being logical, and that gives the separation I need to let something go. It usually takes a good 30 minutes to an hour for it to happen.<br><br>In reply to: <a href="https://x.com/p_brunenberg/status/1486770096144007174" rel="noopener noreferrer" target="_blank">@p_brunenberg</a> <span class="status">1486770096144007174</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1486794191128580103',
    created: 1643314333000,
    type: 'post',
    text: 'Sitting in the bookstore, I can remember thinking "I don\'t know how to do this, but I know if I follow these steps exactly, I will make it work." The book is the program and your mind is the interpreter. Follow the steps and you will soon be observing yourself doing it.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1486792118802681857',
    created: 1643313839000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/looselytyped" rel="noopener noreferrer" target="_blank">@looselytyped</a> <a class="mention" href="https://x.com/kenkousen" rel="noopener noreferrer" target="_blank">@kenkousen</a> <a class="mention" href="https://x.com/OReillyMedia" rel="noopener noreferrer" target="_blank">@OReillyMedia</a> Looks spot on to me...and ready to teach!<br><br>In reply to: <a href="https://x.com/looselytyped/status/1486788919194787841" rel="noopener noreferrer" target="_blank">@looselytyped</a> <span class="status">1486788919194787841</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1486791872458625025',
    created: 1643313780000,
    type: 'quote',
    text: 'In the early days of my career, it was books like this that made tech not feel scary &amp; helped me believe in my abilities. These books are so important to onboard the next generation (regardless of age) into our community. If you know someone who thinks git is scary, point them 👇<br><br>Quoting: <a href="https://x.com/OReillyMedia/status/1486761010497269764" rel="noopener noreferrer" target="_blank">@OReillyMedia</a> <span class="status">1486761010497269764</span>',
    likes: 9,
    retweets: 1
  },
  {
    id: '1486072280094429187',
    created: 1643142216000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/benignbala" rel="noopener noreferrer" target="_blank">@benignbala</a> <a class="mention" href="https://x.com/GParSystem" rel="noopener noreferrer" target="_blank">@GParSystem</a> <a class="mention" href="https://x.com/ApacheGroovy" rel="noopener noreferrer" target="_blank">@ApacheGroovy</a> <a class="mention" href="https://x.com/PyConUK" rel="noopener noreferrer" target="_blank">@PyConUK</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> He will be with us in spirit forever. His memory lives on.<br><br>In reply to: <a href="https://x.com/benignbala/status/1486005977367584772" rel="noopener noreferrer" target="_blank">@benignbala</a> <span class="status">1486005977367584772</span>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1483728454130434050',
    created: 1642583404000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/k33g_org" rel="noopener noreferrer" target="_blank">@k33g_org</a> 🙌<br><br>(That\'s me, cheering you on!)<br><br>In reply to: <a href="https://x.com/k33g_org/status/1483711478179975178" rel="noopener noreferrer" target="_blank">@k33g_org</a> <span class="status">1483711478179975178</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1483640319367254018',
    created: 1642562391000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CityofLoneTree" rel="noopener noreferrer" target="_blank">@CityofLoneTree</a> Thanks for the notification.<br><br>In reply to: <a href="https://x.com/CityofLoneTree/status/1483621494873808905" rel="noopener noreferrer" target="_blank">@CityofLoneTree</a> <span class="status">1483621494873808905</span>',
    likes: 0,
    retweets: 1
  },
  {
    id: '1482513129946845185',
    created: 1642293648000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kittylyst" rel="noopener noreferrer" target="_blank">@kittylyst</a> <a class="mention" href="https://x.com/ellenallien" rel="noopener noreferrer" target="_blank">@ellenallien</a> Love me some <a class="mention" href="https://x.com/ellenallien" rel="noopener noreferrer" target="_blank">@ellenallien</a>. Her balcony sets are the best.<br><br>In reply to: <a href="https://x.com/kittylyst/status/1482497359045177346" rel="noopener noreferrer" target="_blank">@kittylyst</a> <span class="status">1482497359045177346</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1480836950747598849',
    created: 1641894016000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/vwbusguy" rel="noopener noreferrer" target="_blank">@vwbusguy</a> <a class="mention" href="https://x.com/slack" rel="noopener noreferrer" target="_blank">@slack</a> <a class="mention" href="https://x.com/fedora" rel="noopener noreferrer" target="_blank">@fedora</a> Zulip. They get open source and deserve our attention and support.<br><br>In reply to: <a href="https://x.com/vwbusguy/status/1480631237421060096" rel="noopener noreferrer" target="_blank">@vwbusguy</a> <span class="status">1480631237421060096</span>',
    likes: 3,
    retweets: 1
  },
  {
    id: '1480705029795291138',
    created: 1641862563000,
    type: 'quote',
    text: 'Peak broken health care(less) system right here. 👇<br><br>Quoting: <a href="https://x.com/WhiteHouse/status/1480639293756522502" rel="noopener noreferrer" target="_blank">@WhiteHouse</a> <span class="status">1480639293756522502</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1480690469747060737',
    created: 1641859092000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sienaluke" rel="noopener noreferrer" target="_blank">@sienaluke</a> I appreciate the work you and your team are doing. I especially want to thank you all for the pino recommendation in the Node.js reference architecture. It came at such a perfect time as we were looking for a logging solution for Antora. And it\'s really worked out!<br><br>In reply to: <a href="https://x.com/sienaluke/status/1480686465545908241" rel="noopener noreferrer" target="_blank">@sienaluke</a> <span class="status">1480686465545908241</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1480654846231408642',
    created: 1641850599000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> So forever. Super. Just make sure we reserve plenty of money to burn.<br><br>In reply to: <a href="https://x.com/brunoborges/status/1480654230499196928" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1480654230499196928</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1480653565488074752',
    created: 1641850293000,
    type: 'post',
    text: 'The fact that the Windows OS refuses to align with the Unix path system (by continuing to rely on drive letters and UNC paths) costs the tech industry an obscene amount of money. How long do we have to tolerate this?',
    likes: 26,
    retweets: 4
  },
  {
    id: '1480127062748712961',
    created: 1641724765000,
    type: 'post',
    text: 'Anyone else forget how to put on shoes?',
    likes: 1,
    retweets: 0
  },
  {
    id: '1479407871796285440',
    created: 1641553297000,
    type: 'post',
    text: 'I\'m most proud of my documentation when I can answer questions with links to it.',
    likes: 66,
    retweets: 4
  },
  {
    id: '1479178000335708161',
    created: 1641498491000,
    type: 'post',
    text: 'Stop calling it an anniversary. An anniversary is something you celebrate. Today is about a shameful event in US history led by cowards. It left a stain on this country that we cannot allow to become permanent.',
    likes: 5,
    retweets: 1
  },
  {
    id: '1479044435128111104',
    created: 1641466647000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jakekorab" rel="noopener noreferrer" target="_blank">@jakekorab</a><br><br>In reply to: <a href="https://x.com/jakekorab/status/1478999163006332933" rel="noopener noreferrer" target="_blank">@jakekorab</a> <span class="status">1478999163006332933</span>',
    photos: ['<div class="item"><img class="photo" src="media/1479044435128111104.gif"></div>'],
    likes: 1,
    retweets: 0
  },
  {
    id: '1479020881221324800',
    created: 1641461031000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mrunesson" rel="noopener noreferrer" target="_blank">@mrunesson</a> Sorry, but the only people I know who use matrix do nothing but tell me that I need to use a different platform that works for them. That\'s just not how this works.<br><br>In reply to: <a href="https://x.com/mrunesson/status/1479013654192349184" rel="noopener noreferrer" target="_blank">@mrunesson</a> <span class="status">1479013654192349184</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1479009158301057024',
    created: 1641458236000,
    type: 'post',
    text: 'People who use matrix are like "you have to chat with me on my platform!" Um, no.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1478889670180171778',
    created: 1641429748000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/odeke_et" rel="noopener noreferrer" target="_blank">@odeke_et</a> What a suit! You\'re starting the new year off right.<br><br>In reply to: <a href="https://x.com/odeke_et/status/1478886209870913539" rel="noopener noreferrer" target="_blank">@odeke_et</a> <span class="status">1478886209870913539</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1478858424146292736',
    created: 1641422298000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/elioqoshi" rel="noopener noreferrer" target="_blank">@elioqoshi</a> On more than one occasion, I\'ve definitely thought to myself, "damn, I think the parley messed up this dish." It has it\'s place, but that place isn\'t on everything.<br><br>In reply to: <a href="https://x.com/elioqoshi/status/1478853066686353413" rel="noopener noreferrer" target="_blank">@elioqoshi</a> <span class="status">1478853066686353413</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1478819686066233347',
    created: 1641413062000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MStine" rel="noopener noreferrer" target="_blank">@MStine</a> And take out the commute, and everything in life just gets better. I\'ll be honest. Driving home on the DC beltway, I often thought of just driving off the side of the bridge. I hated my life. And traffic was 80% of it.<br><br>In reply to: <a href="#1478819403642724352">1478819403642724352</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1478819403642724352',
    created: 1641412995000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MStine" rel="noopener noreferrer" target="_blank">@MStine</a> In my opinion, it\'s both. To me, going to an office is my version of hell. There\'s nothing I like about it and I get nothing done. I always used to work from 5 - 8 PM when I did go to an office, after everyone had left.<br><br>In reply to: @mstine <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1478331508867821568',
    created: 1641296672000,
    type: 'reply',
    text: '@ntgussoni <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> We just released Antora 3 with a ton of new capabilities, including an extension facility. Whether it not it works for you, it\'s the solution I have to offer and will be focused on for the foreseeable future.<br><br>In reply to: <a href="https://x.com/ntorresdev/status/1478324872317214721" rel="noopener noreferrer" target="_blank">@ntorresdev</a> <span class="status">1478324872317214721</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1478323221602603009',
    created: 1641294696000,
    type: 'reply',
    text: '@ntgussoni <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> That is exactly why we made <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a>.<br><br>In reply to: <a href="https://x.com/ntorresdev/status/1478310427939508226" rel="noopener noreferrer" target="_blank">@ntorresdev</a> <span class="status">1478310427939508226</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1478205686651654144',
    created: 1641266674000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MStine" rel="noopener noreferrer" target="_blank">@MStine</a> Life?<br><br>In reply to: @mstine <span class="status">&lt;deleted&gt;</span>',
    likes: 8,
    retweets: 0
  },
  {
    id: '1477172824968073220',
    created: 1641020420000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/venkat_s" rel="noopener noreferrer" target="_blank">@venkat_s</a> While I\'m heartbroken by the devastation to our neighbors lives, I\'m grateful to hear you\'re okay. I raise a glass to your health and safety.<br><br>In reply to: <a href="https://x.com/venkat_s/status/1476734837562118145" rel="noopener noreferrer" target="_blank">@venkat_s</a> <span class="status">1476734837562118145</span>',
    likes: 2,
    retweets: 0
  }
])
