'use strict'

inflateTweets([
  {
    id: '7262568635',
    created: 1262328457000,
    type: 'post',
    text: 'Happy New Year! Banged in the New Year playing Rock Band. Easter Egg message: "get more awesome and get better at Rock Band" 2010 Baby!',
    likes: 0,
    retweets: 0
  },
  {
    id: '7262565670',
    created: 1262328448000,
    type: 'post',
    text: 'Had to make a clutch electronics repair on my drum set to play Rock Band for New Year\'s. Nothing some duck tape couldn\'t fix.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7246784996',
    created: 1262291819000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cagataycivici" rel="noopener noreferrer" target="_blank">@cagataycivici</a> Nice! Rock on!<br><br>In reply to: @cagataycivici <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7227366699',
    created: 1262246477000,
    type: 'post',
    text: 'OpenWebBeans also supports using CDI (JSR-299) in a Servlet container. So there is really no reason to be uneasy about using it ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '7226721914',
    created: 1262244651000,
    type: 'post',
    text: 'I also think that apt-get really needs to have an audit log built in. I know I could use aptitude, but it should be part of apt-get.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7226638488',
    created: 1262244427000,
    type: 'post',
    text: 'I\'d like to implement Papa John\'s pizza builder as a sample app. I enjoy the interface and it would model well as an enterprise app.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7226352842',
    created: 1262243666000,
    type: 'post',
    text: 'The backup of my laptop == 40GB. That\'s the main reason backing it up is so difficult. No match for my 1TB backup drive I got 4 xmas ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '7226314480',
    created: 1262243564000,
    type: 'post',
    text: 'Let me just say, backup software is crap. I tried tons and most suck at speed or exclusion filters. Finally found safekeep. Amazing.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7226279626',
    created: 1262243473000,
    type: 'post',
    text: 'The notification bubble placement is really annoying, though. I still miss amarok 1.4 and a way to theme KDE apps from Gnome.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7226253973',
    created: 1262243407000,
    type: 'post',
    text: 'Ubuntu Karmic seems much cleaner. I\'ve noticed a lot of nice new utilities, like the installer, disk utility, log file viewer &amp; Ubuntu One.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7226209788',
    created: 1262243291000,
    type: 'post',
    text: 'I said last year that I was really impressed by Ubuntu Intrepid. However, I had *lots* of stability problems with it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7226196218',
    created: 1262243256000,
    type: 'post',
    text: 'I\'ve been working on migrating all my computers to Ubuntu 9.10 (Karmic). Takes a lot of concentration (hence no tweets).',
    likes: 0,
    retweets: 0
  },
  {
    id: '7174031085',
    created: 1262125635000,
    type: 'post',
    text: 'Overall, I picked out these themes in my tweets: hope milestone nostalgia progress java ee 6',
    likes: 0,
    retweets: 0
  },
  {
    id: '7173989441',
    created: 1262125546000,
    type: 'post',
    text: 'devoxx jsfsummit jsf2next collaboration rock band cherry blossom zody android record broken engagement reunion wedding funeral cdi java ee 6',
    likes: 0,
    retweets: 0
  },
  {
    id: '7173981225',
    created: 1262125528000,
    type: 'post',
    text: 'xmp3 intrepid obama google apps snowboard hookey speaking jbve tssjs lasik poker face 31 mystere 0 inbox ete jug javaone rockstar jbossworld',
    likes: 0,
    retweets: 0
  },
  {
    id: '7173978123',
    created: 1262125521000,
    type: 'post',
    text: 'Here comes 2009 in 280 characters (I just couldn\'t compress it to 140)',
    likes: 0,
    retweets: 0
  },
  {
    id: '7172752150',
    created: 1262122904000,
    type: 'post',
    text: 'I\'m reading through all my posts from 2009. Twitter is basically my diary.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7172713591',
    created: 1262122824000,
    type: 'post',
    text: 'My Bose Quiet Comfort headphones allow me to turn off the world around me. I love it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7135185662',
    created: 1262035259000,
    type: 'post',
    text: 'I\'m not familiar with the tradition of leaving presents under the car.',
    photos: ['<div class="entry"><img class="photo" src="media/7135185662.jpg"></div>'],
    likes: 0,
    retweets: 1
  },
  {
    id: '7135065332',
    created: 1262035004000,
    type: 'post',
    text: 'Christmas tree at family xmas party #4. It\'s been 4 xmases for us this year.',
    photos: ['<div class="entry"><img class="photo" src="media/7135065332.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '7132657870',
    created: 1262029752000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I wonder if GlassFish has a user-editable FAQ/Knowledgebase like Seam. I have to say that it\'s the best feature of swfk.org.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/7132588237" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">7132588237</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7132446213',
    created: 1262029277000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Install the Hibernate plugin using the GF update center in the admin console. Then use &lt;provider&gt; in persistence.xml.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/7116718762" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">7116718762</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7132372384',
    created: 1262029111000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> For what it\'s worth, I\'m of the same opinion. The quality of the docs from Sun doesn\'t match the software. It needs to be good.<br><br>In reply to: <a href="https://x.com/jasondlee/status/7130770360" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">7130770360</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7113054968',
    created: 1261976712000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> Isn\'t great how Rock Band bridges generations in play? We played mine w/ until one of my drum heads broke ;)<br><br>In reply to: <a href="https://x.com/aschwart/status/7107400678" rel="noopener noreferrer" target="_blank">@aschwart</a> <span class="status">7107400678</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7100810325',
    created: 1261949042000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexismp" rel="noopener noreferrer" target="_blank">@alexismp</a> I get your point. But the NetBeans 6.8 bundle with GlassFish V3 is much more than a kernel. It\'s very much end-to-end dev.<br><br>In reply to: <a href="https://x.com/alexismp/status/7097424271" rel="noopener noreferrer" target="_blank">@alexismp</a> <span class="status">7097424271</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7100662558',
    created: 1261948673000,
    type: 'post',
    text: 'The Ravens are just hurting. My wife is going nuts.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7096740191',
    created: 1261938783000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> +1000 That\'s what I\'ve been saying. Let\'s stop bastardizing Java EE and use the platform. Until Tomcat impl the Web Profile.<br><br>In reply to: <a href="https://x.com/jasondlee/status/7096353609" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">7096353609</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7096649770',
    created: 1261938559000,
    type: 'post',
    text: 'Family Christmas party marathon today. Cousins gift exchange (family #3) and father\'s family (family #4).',
    likes: 0,
    retweets: 0
  },
  {
    id: '7084824345',
    created: 1261900153000,
    type: 'post',
    text: 'Two great tutorials on CDI and JSF 2 by Andy Gibson: <a href="http://www.andygibson.net/blog/index.php/2009/12/16/getting-started-with-jsf-2-0-and-cdi-in-jee-6-part-1/" rel="noopener noreferrer" target="_blank">www.andygibson.net/blog/index.php/2009/12/16/getting-started-with-jsf-2-0-and-cdi-in-jee-6-part-1/</a> <a href="http://www.andygibson.net/blog/index.php/2009/12/22/getting-started-with-cdi-part-2-injection/" rel="noopener noreferrer" target="_blank">www.andygibson.net/blog/index.php/2009/12/22/getting-started-with-cdi-part-2-injection/</a> #cdi #jsf2',
    likes: 0,
    retweets: 1,
    tags: ['cdi', 'jsf2']
  },
  {
    id: '7081318693',
    created: 1261890198000,
    type: 'post',
    text: 'Perhaps obvious we should call the next year twenty-ten: <a href="http://www.tampabay.com/news/perspective/why-we-should-call-the-new-year-twenty-ten/1061075" rel="noopener noreferrer" target="_blank">www.tampabay.com/news/perspective/why-we-should-call-the-new-year-twenty-ten/1061075</a> So why did we deviate from 2000 -&gt; 2009?',
    likes: 0,
    retweets: 0
  },
  {
    id: '7074025655',
    created: 1261872006000,
    type: 'post',
    text: 'Attempt to see Avatar 3D tonight was shot down. That\'s okay, because IMAX seats were sold out, so I\'m holding out for the best experience.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7074006791',
    created: 1261871959000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/hackernewsbot" rel="noopener noreferrer" target="_blank">@hackernewsbot</a> If I\'m not allowed to listen to music on a flight, I\'ll do it anyway. Arrest me or throw me off. I won\'t be repressed.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7073407597',
    created: 1261870327000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> I\'ve said the same thing for a long time. Editing contacts in Gmail is braindead in some cases: copying or resolving a duplicate.<br><br>In reply to: <a href="https://x.com/kito99/status/7073240274" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">7073240274</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7073385926',
    created: 1261870268000,
    type: 'post',
    text: 'The good news is that I got Arquillian building and verified that tests run on JBoss AS 6 using JUnit 4 and Test NG. Great job Aslak!',
    likes: 0,
    retweets: 0
  },
  {
    id: '7073373322',
    created: 1261870234000,
    type: 'post',
    text: 'I almost broke my computer screen trying to edit a document on JBoss Wiki today with Firefox. Not the stress I need on vacation.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7068705229',
    created: 1261857017000,
    type: 'post',
    text: 'I can\'t get over how cool Google Navigation is, or the fact that my phone can do that. The navigation lady directed us through #dc xmas eve.',
    likes: 0,
    retweets: 0,
    tags: ['dc']
  },
  {
    id: '7054272437',
    created: 1261807694000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/utbrown" rel="noopener noreferrer" target="_blank">@utbrown</a> <a href="http://www.1up.com/do/newsStory?cId=3169929" rel="noopener noreferrer" target="_blank">www.1up.com/do/newsStory?cId=3169929</a> Unlock song code and explanation.<br><br>In reply to: <a href="https://x.com/utbrown/status/7054174466" rel="noopener noreferrer" target="_blank">@utbrown</a> <span class="status">7054174466</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7054169158',
    created: 1261807379000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/edburns" rel="noopener noreferrer" target="_blank">@edburns</a> Yep, Bose qc15<br><br>In reply to: <a href="https://x.com/edburns/status/7052708119" rel="noopener noreferrer" target="_blank">@edburns</a> <span class="status">7052708119</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7054130094',
    created: 1261807268000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/utbrown" rel="noopener noreferrer" target="_blank">@utbrown</a> I was confused at first too. You have to succeed at all those, then many more unlock. I\'m still unlocking.<br><br>In reply to: <a href="https://x.com/utbrown/status/7054061775" rel="noopener noreferrer" target="_blank">@utbrown</a> <span class="status">7054061775</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7054104935',
    created: 1261807204000,
    type: 'post',
    text: 'The first CD album I owned was Pearl Jam\'s Vs. I listened to the beginning of track #1 over and over. Awesome tones.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7053957683',
    created: 1261806751000,
    type: 'post',
    text: 'I got a Wii to go with my Rock Band 2. Then we played RB at family #2 xmas party. Got some real talent there ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '7053858251',
    created: 1261806461000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I didn\'t realize that tweet went throught. That was the relatives playing with my phone ;)<br><br>In reply to: <a href="https://x.com/lightguardjp/status/7048160672" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">7048160672</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7047660177',
    created: 1261790545000,
    type: 'post',
    text: 'Success, here I come',
    photos: ['<div class="entry"><img class="photo" src="media/7047660177.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '7046147696',
    created: 1261786828000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a><br><br>In reply to: <a href="https://x.com/lightguardjp/status/6974884589" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6974884589</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7045126888',
    created: 1261784132000,
    type: 'post',
    text: 'I proceeded to reminisce about the 1st xmas I experienced CD technology. It was a present for my mom. We played with it before giving.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7044767420',
    created: 1261783185000,
    type: 'post',
    text: 'Bose made a fortune on us this year. I received the noise canceling headphones for my plane rides and I got the CD/radio unit for my wife.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7039866703',
    created: 1261768947000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> Me too! I just woke up from my post gift exchange nap. Lazy days are the best.<br><br>In reply to: <a href="https://x.com/aschwart/status/7038579555" rel="noopener noreferrer" target="_blank">@aschwart</a> <span class="status">7038579555</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '7033901412',
    created: 1261753256000,
    type: 'post',
    text: 'It\'s official. It\'s a White Christmas in Maryland. The snow blanket held out.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7025661598',
    created: 1261723096000,
    type: 'post',
    text: 'Merry Christmas! If you\'ve been good, life will repay you generously.',
    likes: 0,
    retweets: 0
  },
  {
    id: '7025630763',
    created: 1261722999000,
    type: 'post',
    text: 'Priest said at homily "You can\'t be just sort of Christian. That would be like Jesus sort of saving man." I am fully my own theology ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '7025556438',
    created: 1261722780000,
    type: 'post',
    text: 'My and I joined my family at Christmas Eve mass to scope out church for my sister\'s wedding: St. Peter\'s in #dc.',
    likes: 0,
    retweets: 0,
    tags: ['dc']
  },
  {
    id: '7017897845',
    created: 1261704415000,
    type: 'post',
    text: 'Christmas tree at house of family #1. All gifts were a go!',
    photos: ['<div class="entry"><img class="photo" src="media/7017897845.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '7009645268',
    created: 1261684273000,
    type: 'post',
    text: 'I Christmas-ed myself with some Belgium beer: Troubadour stout. Doubles as gift for my father-in-law. Unwrapping tomorrow ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '7009607911',
    created: 1261684189000,
    type: 'post',
    text: 'Presents wrapped, heading to first family Christmas get together 2nite. Then off to see the other \'rents and siblings tomorrow.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6992846866',
    created: 1261639067000,
    type: 'post',
    text: 'Is it possible to override the goalPrefix for a #maven plugin in the project using it? Perhaps to use egf rather than embedded-glassfish.',
    likes: 0,
    retweets: 0,
    tags: ['maven']
  },
  {
    id: '6992715636',
    created: 1261638571000,
    type: 'post',
    text: 'I always forget how Maven determines the goal prefix for a plugin. This page tells all: <a href="http://maven.apache.org/guides/introduction/introduction-to-plugin-prefix-mapping.html" rel="noopener noreferrer" target="_blank">maven.apache.org/guides/introduction/introduction-to-plugin-prefix-mapping.html</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6989409042',
    created: 1261630617000,
    type: 'post',
    text: 'The more public I am with communication, the simpler life gets. I don\'t need to compartmentalize information. It\'s just out in the open.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6989219867',
    created: 1261630224000,
    type: 'post',
    text: 'I had to do an emergency run for wrapping paper, gift tags and boxes. Seems we under-estimated our coverage requirements ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '6989198724',
    created: 1261630180000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> Glad to hear you had a good day on the slopes. There is no better way to clear your head than with fresh, crisp air!<br><br>In reply to: <a href="https://x.com/aschwart/status/6973577931" rel="noopener noreferrer" target="_blank">@aschwart</a> <span class="status">6973577931</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6989172820',
    created: 1261630127000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> I\'m jealous. I could use some Rock Band right about now ;)<br><br>In reply to: <a href="https://x.com/aschwart/status/6988265323" rel="noopener noreferrer" target="_blank">@aschwart</a> <span class="status">6988265323</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6979079539',
    created: 1261608078000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Indeed.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/6979011212" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6979011212</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6978992935',
    created: 1261607883000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Yes, you can bind any object you want.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/6978391747" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6978391747</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6978336039',
    created: 1261606395000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> In one sense it is like outjection. In another sense, you can think of it like <a class="mention" href="https://x.com/Produces" rel="noopener noreferrer" target="_blank">@Produces</a> except for JNDI binding.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6978087654',
    created: 1261605815000,
    type: 'post',
    text: 'Gotcha tip: Don\'t confuse <a class="mention" href="https://x.com/JavAx" rel="noopener noreferrer" target="_blank">@JavAx</a>.inject.Singleton with <a class="mention" href="https://x.com/JavAx" rel="noopener noreferrer" target="_blank">@JavAx</a>.ejb.Singleton!',
    likes: 0,
    retweets: 0
  },
  {
    id: '6977721160',
    created: 1261604976000,
    type: 'post',
    text: 'Just checked in a new Seam 3 module by Matt Corey: envconfig Let\'s you bind to a JNDI value using @Bind("msg") String msg = "Hello World!"',
    likes: 0,
    retweets: 0
  },
  {
    id: '6975990617',
    created: 1261600976000,
    type: 'post',
    text: 'I\'m getting some traction on my archetype:import idea for Maven. <a href="http://jira.codehaus.org/browse/ARCHETYPE-273" rel="noopener noreferrer" target="_blank">jira.codehaus.org/browse/ARCHETYPE-273</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '6974830218',
    created: 1261598324000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Especially given the rebirth of JNDI in Java EE 6, we really miss a decent JNDI browser. Anyone want a holiday project?<br><br>In reply to: <a href="https://x.com/lightguardjp/status/6973756603" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6973756603</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6974814731',
    created: 1261598288000,
    type: 'post',
    text: 'Jim Driscoll,Tedd Goddard,Lincoln Baxter III,David Geary,Ed Burns,Kito Mann,Andy Schwartz,Dan Allen -&gt; #jsfsummit',
    photos: ['<div class="entry"><img class="photo" src="media/6974814731.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6974763254',
    created: 1261598165000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jclingan" rel="noopener noreferrer" target="_blank">@jclingan</a> Posted <a href="http://forums.java.net/jive/thread.jspa?threadID=71112" rel="noopener noreferrer" target="_blank">forums.java.net/jive/thread.jspa?threadID=71112</a> I\'m not the first person who reported it either (see reference)<br><br>In reply to: <a href="https://x.com/jclingan/status/6973830865" rel="noopener noreferrer" target="_blank">@jclingan</a> <span class="status">6973830865</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6974520570',
    created: 1261597607000,
    type: 'reply',
    text: '@brianleathem It seems to be missing in GlassFish V3.<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6973711291',
    created: 1261595778000,
    type: 'post',
    text: 'The Resources &gt; JNDI node in the GlassFish V3 Admin console doesn\'t reveal any JNDI entries, so it is useless to me.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6973682546',
    created: 1261595713000,
    type: 'post',
    text: 'Does anyone know how to browse the JNDI tree in GlassFish V3? I want to be able to browse what gets registered.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6957052890',
    created: 1261551049000,
    type: 'post',
    text: 'Borders and other retailers are eBlasting me with anything to entice me to add presents under my tree. They\'ll even walk it to my house ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '6956921531',
    created: 1261550647000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AndreasEK" rel="noopener noreferrer" target="_blank">@AndreasEK</a> I won\'t even mention J2EE 1.4.<br><br>In reply to: <a href="https://x.com/AndreasEK/status/6955846727" rel="noopener noreferrer" target="_blank">@AndreasEK</a> <span class="status">6955846727</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6956911284',
    created: 1261550616000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AndreasEK" rel="noopener noreferrer" target="_blank">@AndreasEK</a> Much more sane than Java EE 5. When you see the amount configuration that has just been erased in EE 6, you just smile.<br><br>In reply to: <a href="https://x.com/AndreasEK/status/6955846727" rel="noopener noreferrer" target="_blank">@AndreasEK</a> <span class="status">6955846727</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6956104293',
    created: 1261548266000,
    type: 'post',
    text: 'Nickname for the JSR-299 extension SPI: "Tweak EE" (get the reference?)',
    likes: 0,
    retweets: 0
  },
  {
    id: '6954697601',
    created: 1261544648000,
    type: 'post',
    text: 'JAX-RS 1.1 configuration in Java EE 6 == @ApplicationPath("service") public class JaxRsConfig extends Application {} (no web.xml)',
    likes: 0,
    retweets: 1
  },
  {
    id: '6954615890',
    created: 1261544458000,
    type: 'post',
    text: 'The more I look at Java EE 6, the more I realize we just must get folks migrated to it. It\'s a much more sane and consistent environment.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6954555160',
    created: 1261544319000,
    type: 'post',
    text: 'Tip: Don\'t map your JAX-RS servlet to /resources if you plan to use JSF 2 in the same application. /resources is a reserved path in JSF 2.',
    likes: 1,
    retweets: 0
  },
  {
    id: '6945737368',
    created: 1261524928000,
    type: 'post',
    text: 'I recommend the JPA 2 talk. JPA 2 hasn\'t gotten as much press as CDI, EJB 3.1 and JSF 2, but it is certainly worthy of it. Check it out!',
    likes: 0,
    retweets: 0
  },
  {
    id: '6945716734',
    created: 1261524882000,
    type: 'post',
    text: 'Java EE 6 and GlassFish Virtual Conference replays: <a href="http://www.sun.com/events/javaee6glassfishv3/virtualconference/" rel="noopener noreferrer" target="_blank">www.sun.com/events/javaee6glassfishv3/virtualconference/</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '6919908318',
    created: 1261460011000,
    type: 'post',
    text: 'Thought I woke up this morning in an Italian airport. Then realized my brother had just figured out how to use my house\'s intercom system.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6897818981',
    created: 1261412839000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> Man, we need to get together for some snowboarding ;) Maybe when I have a house in Boulder you\'ll visit.<br><br>In reply to: <a href="https://x.com/aschwart/status/6891941101" rel="noopener noreferrer" target="_blank">@aschwart</a> <span class="status">6891941101</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6897524244',
    created: 1261412218000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> Great point! I know that the RESTEasy project did a great job w/ their client API, so perhaps we can build on that work.<br><br>In reply to: <a href="https://x.com/JohnAment/status/6890512389" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">6890512389</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6885198862',
    created: 1261375291000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> Ah. I didn\'t think of it from that perspective. I\'ll note that down for discussion.<br><br>In reply to: <a href="https://x.com/JohnAment/status/6878925100" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">6878925100</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6875602930',
    created: 1261353767000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> We\'ll definitely address in #jsf2next. We\'ll publish a proposal/blog entry soon explaining it\'s really addressable resources.<br><br>In reply to: <a href="https://x.com/JohnAment/status/6870411114" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">6870411114</span>',
    likes: 0,
    retweets: 0,
    tags: ['jsf2next']
  },
  {
    id: '6875406213',
    created: 1261353329000,
    type: 'post',
    text: 'I don\'t understand why Flip won\'t make a video camera that includes a microSD slot for expansion.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6852645682',
    created: 1261288082000,
    type: 'post',
    text: 'The performance of Google Sites in Firefox is down right abysmal. And it doesn\'t get much better with Opera. Perhaps Chrome?',
    likes: 1,
    retweets: 0
  },
  {
    id: '6845811895',
    created: 1261272261000,
    type: 'post',
    text: 'This is not Colorado folks.',
    photos: ['<div class="entry"><img class="photo" src="media/6845811895.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '6844656838',
    created: 1261269501000,
    type: 'post',
    text: 'At the bottom of <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a>\'s post are the links to the other two entries in the trilogy <a href="http://ocpsoft.com/opensource/what-a-wild-ride-my-journey-through-opensource-jsf/" rel="noopener noreferrer" target="_blank">ocpsoft.com/opensource/what-a-wild-ride-my-journey-through-opensource-jsf/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6844618643',
    created: 1261269406000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> offering yet another inspiring testimony of JSF Summit 09: <a href="http://ocpsoft.com/opensource/what-a-wild-ride-my-journey-through-opensource-jsf/" rel="noopener noreferrer" target="_blank">ocpsoft.com/opensource/what-a-wild-ride-my-journey-through-opensource-jsf/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6843867883',
    created: 1261267592000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> It had to be done ;)<br><br>In reply to: <a href="https://x.com/lincolnthree/status/6842997854" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">6842997854</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6843859090',
    created: 1261267570000,
    type: 'post',
    text: 'As we look to 2010, let\'s keep in mind these Paul Van Dyk lyrics: Let\'s enjoy with you. We are alive. Take a deep breath. We are alive.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6832975625',
    created: 1261240664000,
    type: 'post',
    text: 'Looks like we are going to have a White Christmas here in Maryland. Savor it. #snow',
    photos: ['<div class="entry"><img class="photo" src="media/6832975625.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['snow']
  },
  {
    id: '6832860135',
    created: 1261240392000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> It\'s called extra work. I want to BCC recipients and let them know why they are getting a random message.<br><br>In reply to: <a href="https://x.com/maxandersen/status/6824267677" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">6824267677</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6832836478',
    created: 1261240336000,
    type: 'post',
    text: 'Hey Google. Freakin\' fix e-mail replying on #android for f%@$ sake! <a href="http://code.google.com/p/android/issues/detail?id=1136" rel="noopener noreferrer" target="_blank">code.google.com/p/android/issues/detail?id=1136</a>',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '6823367330',
    created: 1261207371000,
    type: 'post',
    text: 'I\'ve always wished there would be a way to attach a private message to the BCC recipients of an e-mail. #idea',
    likes: 0,
    retweets: 1,
    tags: ['idea']
  },
  {
    id: '6822039277',
    created: 1261202931000,
    type: 'post',
    text: 'It never fails that I end up at the mall at the busiest possible time, and when it is snowing. That\'s what you get for not slacking off.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6812125932',
    created: 1261179087000,
    type: 'post',
    text: 'I watched GI Joe last night with my wife. Brought back childhood memories (however vague). Definitely a fun flick!',
    likes: 0,
    retweets: 0
  },
  {
    id: '6812022991',
    created: 1261178849000,
    type: 'post',
    text: 'Feature request to import a Maven archetype catalog: <a href="http://jira.codehaus.org/browse/ARCHETYPE-273" rel="noopener noreferrer" target="_blank">jira.codehaus.org/browse/ARCHETYPE-273</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6810807228',
    created: 1261176021000,
    type: 'post',
    text: 'I\'m really excited about 2010! Just a generally good feeling ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '6806911783',
    created: 1261167039000,
    type: 'post',
    text: 'Get running on CDI &amp; JSF 2 in a jiffy using the Weld Maven archetypes: <a href="http://in.relation.to/13649.lace" rel="noopener noreferrer" target="_blank">in.relation.to/13649.lace</a> (now available in Maven central)',
    likes: 1,
    retweets: 2
  },
  {
    id: '6778350108',
    created: 1261093027000,
    type: 'post',
    text: 'What Maven needs is archetype:import -DarchetypeCatalog=http://example.com/maven2/archetype-catalog.xml',
    likes: 0,
    retweets: 0
  },
  {
    id: '6778164714',
    created: 1261092618000,
    type: 'post',
    text: 'Wow, Maven archetypes have a real chicken/egg problem. There is no way to "import" an archetype catalog locally before using an archetype.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6772913638',
    created: 1261080633000,
    type: 'post',
    text: '"T-Mobile is the only United States carrier with phones running Android 1.6" :( #upgrademe',
    likes: 0,
    retweets: 0,
    tags: ['upgrademe']
  },
  {
    id: '6772352824',
    created: 1261079304000,
    type: 'post',
    text: 'GlassFish V3 doesn\'t activate CDI if WAR classpath has META-INF/beans.xml. Must have WEB-INF/beans.xml. <a href="https://jira.jboss.org/jira/browse/WELD-347" rel="noopener noreferrer" target="_blank">jira.jboss.org/jira/browse/WELD-347</a> #bug',
    likes: 0,
    retweets: 0,
    tags: ['bug']
  },
  {
    id: '6767486191',
    created: 1261066699000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> What Sun should do is encourage developers to try Java EE 6 just by itself. Then, they can try to take slices of it and run alone.<br><br>In reply to: <a href="https://x.com/kito99/status/6757623776" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">6757623776</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6767463276',
    created: 1261066639000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> My main motivation in defending this point is that I think Java EE\'s biggest downfall is that people don\'t give it a full trial.<br><br>In reply to: <a href="https://x.com/kito99/status/6757623776" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">6757623776</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6757225610',
    created: 1261032914000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Personally, I think that is where the mistake is. If you are reading a Java EE 6 tutorial, you should use it on Java EE 6.<br><br>In reply to: <a href="https://x.com/kito99/status/6756982541" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">6756982541</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6756894927',
    created: 1261031804000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Naturally, then you go with <a class="mention" href="https://x.com/ManagedBean" rel="noopener noreferrer" target="_blank">@ManagedBean</a>. Keep in mind I\'m speaking strictly about Java EE 6. I\'m going to draft an email.<br><br>In reply to: <a href="https://x.com/kito99/status/6756721134" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">6756721134</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6756356583',
    created: 1261030116000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jvanzyl" rel="noopener noreferrer" target="_blank">@jvanzyl</a> I can follow up on the mailinglist tomorrow. Let\'s say I want to include a Jetty web fragment if they need Jetty support.<br><br>In reply to: <a href="https://x.com/jvanzyl/status/6756127124" rel="noopener noreferrer" target="_blank">@jvanzyl</a> <span class="status">6756127124</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6756337898',
    created: 1261030058000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> Here\'s my stackoverflow question about conditional #maven archetype includes. <a href="http://stackoverflow.com/questions/1919704/how-do-i-conditionally-include-or-exclude-a-file-from-an-archetype-when-project-i" rel="noopener noreferrer" target="_blank">stackoverflow.com/questions/1919704/how-do-i-conditionally-include-or-exclude-a-file-from-an-archetype-when-project-i</a><br><br>In reply to: <a href="https://x.com/matthewmccull/status/6756014743" rel="noopener noreferrer" target="_blank">@matthewmccull</a> <span class="status">6756014743</span>',
    likes: 0,
    retweets: 0,
    tags: ['maven']
  },
  {
    id: '6756004660',
    created: 1261029071000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> With the Web Profile, there is hardly any reason to break up any of those technologies (okay, maybe EJB 3.1, but that is a stretch).<br><br>In reply to: <a href="https://x.com/kito99/status/6755942922" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">6755942922</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6755991586',
    created: 1261029032000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> You could make the same argument for any of the Java EE technologies. The main issue is that people try to use Java EE in parts.<br><br>In reply to: <a href="https://x.com/kito99/status/6755942922" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">6755942922</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6755680452',
    created: 1261028125000,
    type: 'post',
    text: '#maven gurus. Is there a way to conditionally include/exclude a file from an archetype at generation time based on the value of property?',
    likes: 0,
    retweets: 0,
    tags: ['maven']
  },
  {
    id: '6741791607',
    created: 1260996993000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> I think with JAX-RS, it finally has a strong use case (or perhaps I should said limelight).<br><br>In reply to: <a href="https://x.com/jeremynorris/status/6741334440" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">6741334440</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6740690887',
    created: 1260994487000,
    type: 'post',
    text: 'Victory! I got the JCP to update the name of JSR-299 on the spec page: <a href="http://jcp.org/en/jsr/detail?id=299" rel="noopener noreferrer" target="_blank">jcp.org/en/jsr/detail?id=299</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6735791174',
    created: 1260983269000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> That\'s a whole other issue entirely ;)<br><br>In reply to: <a href="https://x.com/lincolnthree/status/6735561037" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">6735561037</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6734667262',
    created: 1260980809000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> We are really hoping to make these archetypes a go to resource for JSF 2.<br><br>In reply to: <a href="https://x.com/jasondlee/status/6734505179" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">6734505179</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6734493664',
    created: 1260980422000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Btw, you should check out <a href="http://sfwk.org/Documentation/WeldQuickstartForMavenUsers" rel="noopener noreferrer" target="_blank">sfwk.org/Documentation/WeldQuickstartForMavenUsers</a> to see simple CDI + JSF 2 setup for Tomcat/Jetty 6. Feedback welcome<br><br>In reply to: <a href="https://x.com/jasondlee/status/6734194186" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">6734194186</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6734435148',
    created: 1260980291000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> "Offline for maintenance" Same BS we used when jboss.org was down. Nobody does maintenance during business hours.<br><br>In reply to: <a href="https://x.com/matthewmccull/status/6734370974" rel="noopener noreferrer" target="_blank">@matthewmccull</a> <span class="status">6734370974</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6734400289',
    created: 1260980212000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> I\'m sympathetic to that position. I\'m just saying for the Java EE 6 tutorial (think about context) we should use what is there.<br><br>In reply to: <a href="https://x.com/jasondlee/status/6734194186" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">6734194186</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6733975657',
    created: 1260979288000,
    type: 'post',
    text: 'The java.net outage is reminding me of the jboss.org outage that we had a couple months ago. Trading downtime are we?',
    likes: 0,
    retweets: 1
  },
  {
    id: '6733800241',
    created: 1260978918000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Keep in mind this isn\'t "us" vs "them". CDI is part of our platform. We should all be comfortable getting behind it.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/6727667561" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6727667561</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6733746394',
    created: 1260978804000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I\'m not suggesting <a class="mention" href="https://x.com/ManagedBean" rel="noopener noreferrer" target="_blank">@ManagedBean</a> is a conflict. It is just a formality that isn\'t necessary when using Java EE 6.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/6727667561" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6727667561</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6733712099',
    created: 1260978730000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> I don\'t think the Java EE 6 tutorial should be mentioning using a pure servlet environment. (Because it is a Java EE 6 tutorial)<br><br>In reply to: <a href="https://x.com/kito99/status/6723617493" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">6723617493</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6733689862',
    created: 1260978681000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexismp" rel="noopener noreferrer" target="_blank">@alexismp</a> JSR-299 has no dependency on Servlet 3.0. And JSR-299 is available out of the box in Java EE 6. Weld has support for Tomcat/Jetty.<br><br>In reply to: <a href="https://x.com/alexismp/status/6724126171" rel="noopener noreferrer" target="_blank">@alexismp</a> <span class="status">6724126171</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6733662077',
    created: 1260978621000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> There is a common misconception that CDI is more complex. It\'s the out of the box behavior we want to espouse.<br><br>In reply to: <a href="https://x.com/jasondlee/status/6733603949" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">6733603949</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6733629660',
    created: 1260978551000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> I should have pointed out "create implicitly from &lt;head&gt; and &lt;body&gt;; just an alias per se"<br><br>In reply to: <a href="https://x.com/metacosm/status/6726269439" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">6726269439</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6733600577',
    created: 1260978489000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> I just don\'t think we should show it because it\'s more of a formality. CDI makes all classes in archive managed beans.<br><br>In reply to: <a href="https://x.com/JohnAment/status/6727671157" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">6727671157</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6733574660',
    created: 1260978433000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> I know it is in the spec, but conceptually we should just start right out of the gate with an understanding about CDI.<br><br>In reply to: <a href="https://x.com/jasondlee/status/6730415730" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">6730415730</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6723493429',
    created: 1260947570000,
    type: 'post',
    text: 'I really which the Java EE 6 tutorial wouldn\'t show the use of <a class="mention" href="https://x.com/ManagedBean" rel="noopener noreferrer" target="_blank">@ManagedBean</a> in the JSF section, rather <a class="mention" href="https://x.com/Named" rel="noopener noreferrer" target="_blank">@Named</a> for platform consistency.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6723400257',
    created: 1260947238000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/singleton" rel="noopener noreferrer" target="_blank">@singleton</a>: Developer is freed from using Java SE synchronization primitives to protect instance state <a href="http://blogs.sun.com/kensaks/entry/singletons" rel="noopener noreferrer" target="_blank">blogs.sun.com/kensaks/entry/singletons</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6722910035',
    created: 1260945573000,
    type: 'post',
    text: '"Gradle...will redefine the Java build scene, moving Ant/Maven antagonism to the history of computer flame wars" <a href="http://www.javaexpress.pl/article/show/Gradle__a_powerful_build_system" rel="noopener noreferrer" target="_blank">www.javaexpress.pl/article/show/Gradle__a_powerful_build_system</a>',
    likes: 0,
    retweets: 1
  },
  {
    id: '6718273856',
    created: 1260933548000,
    type: 'post',
    text: 'Yeah! We have our first threads created by community members on the JSR-314 public forums! <a href="http://wiki.jcp.org/boards/index.php?b=958" rel="noopener noreferrer" target="_blank">wiki.jcp.org/boards/index.php?b=958</a> We are underway!',
    likes: 0,
    retweets: 0
  },
  {
    id: '6717257820',
    created: 1260931437000,
    type: 'post',
    text: 'Implicit creation of &lt;h:head&gt; and &lt;h:body&gt; for #jsf2next <a href="https://github.com/javaee/javaserverfaces-spec/issues/700" rel="noopener noreferrer" target="_blank">github.com/javaee/javaserverfaces-spec/issues/700</a>',
    likes: 0,
    retweets: 0,
    tags: ['jsf2next']
  },
  {
    id: '6715655936',
    created: 1260928126000,
    type: 'post',
    text: 'I\'m just different like that. Hehehe.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6715646713',
    created: 1260928107000,
    type: 'post',
    text: 'My Santa comes from the South Pole ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '6711417945',
    created: 1260919309000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> I knew I would get myself in trouble with that reference. I should have kept it at "non-American" pronunciation :)<br><br>In reply to: <a href="https://x.com/metacosm/status/6710843637" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">6710843637</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6709106779',
    created: 1260914275000,
    type: 'post',
    text: 'We\'ll assume JSF 2 is perfect if we don\'t hear from you here: <a href="http://wiki.jcp.org/boards/index.php?b=958" rel="noopener noreferrer" target="_blank">wiki.jcp.org/boards/index.php?b=958</a> (j/k of course, but do speak up!)',
    likes: 0,
    retweets: 0
  },
  {
    id: '6708277463',
    created: 1260912429000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/rogerk09" rel="noopener noreferrer" target="_blank">@rogerk09</a> Good job on JSF 2 presentation. Tough crowd in there, but hopefully it will lead to some feedback on forums ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '6708224069',
    created: 1260912307000,
    type: 'post',
    text: 'Kind of a hostile bunch of participants over on the GlassFish Virtual Conference. Don\'t look back in anger folks ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '6708111710',
    created: 1260912044000,
    type: 'post',
    text: 'Audience asking about migration manual from JSF 1.2 to 2.0 at GlassFish Virtual Conference.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6707586039',
    created: 1260910851000,
    type: 'post',
    text: 'Remember, you can post ideas for #jsf2next using hash tag or in forum <a href="http://wiki.jcp.org/boards/index.php?t=2744" rel="noopener noreferrer" target="_blank">wiki.jcp.org/boards/index.php?t=2744</a>',
    likes: 0,
    retweets: 0,
    tags: ['jsf2next']
  },
  {
    id: '6707562270',
    created: 1260910802000,
    type: 'post',
    text: 'JSF 2 session starting now at GlassFish Virtual Conference: <a href="http://www.sun.com/events/javaee6glassfishv3/virtualconference/" rel="noopener noreferrer" target="_blank">www.sun.com/events/javaee6glassfishv3/virtualconference/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6707544135',
    created: 1260910761000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/edburns" rel="noopener noreferrer" target="_blank">@edburns</a> Check out the <a class="mention" href="https://x.com/glassfish" rel="noopener noreferrer" target="_blank">@glassfish</a> timeline for IRC login info<br><br>In reply to: <a href="https://x.com/edburns/status/6707484572" rel="noopener noreferrer" target="_blank">@edburns</a> <span class="status">6707484572</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6706862520',
    created: 1260909172000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/glassfish" rel="noopener noreferrer" target="_blank">@glassfish</a> Are the chat logs being archived from this conference?',
    likes: 0,
    retweets: 0
  },
  {
    id: '6706840542',
    created: 1260909122000,
    type: 'post',
    text: 'I never knew how to pronounce Linda DeMichiel\'s last name. Think French. De-Ma-Kel.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6705912474',
    created: 1260906978000,
    type: 'post',
    text: 'I have just been on the run all day today. Trying to tune into GlassFish conference when I catch a moment here and there.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6705879119',
    created: 1260906901000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Of course, it won\'t always apply, but you don\'t want to have mine it out of JS.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/6705455653" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6705455653</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6705846425',
    created: 1260906821000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> I disagree. I think esp w/ HTML and PDF, resolution is a critical aspect of the document. Think lanscape vs portrait ratio.<br><br>In reply to: @pelegri <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6704570654',
    created: 1260903699000,
    type: 'post',
    text: 'I think that screen resolution should be a REST/browser header. It\'s just as important as language or format for negotiating the resource.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6704532231',
    created: 1260903617000,
    type: 'post',
    text: 'I realize the public JSF 2 mailinglist archives are still broken. I won\'t vacation until I can get someone to fix them.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6702927750',
    created: 1260899886000,
    type: 'post',
    text: 'Seems like the Mojarra team is open to putting the JSF 2 JARs in the Maven central repo. Yeah! I\'ll be helping out if they need me ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '6688495439',
    created: 1260858202000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Of course. The first year of many ;) Now where I live is another story. That will hopefully change soon.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/6688463213" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">6688463213</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6688478617',
    created: 1260858151000,
    type: 'post',
    text: 'Just pushed my first release out to Maven central repo using the Sonatype\'s OSS repository manager. Pretty nice.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6688344951',
    created: 1260857749000,
    type: 'post',
    text: 'This is my new GPG public key: F30CC1C1',
    likes: 0,
    retweets: 0
  },
  {
    id: '6687633358',
    created: 1260855737000,
    type: 'post',
    text: 'I forgot the passphrase for my GPG key. Stupid brain. Creating a new key. This time, I will remember to write down the passphrase.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6682125709',
    created: 1260843637000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> Should I say my sponsor? The one who brings tidings to the accounting gods?<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/6677792384" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">6677792384</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6677341775',
    created: 1260833585000,
    type: 'post',
    text: 'Just had long chat with my manager. Nice to look back on my first 1yr 2mo @ Red Hat. It\'s been an rewarding and productive journey so far.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6677151014',
    created: 1260833167000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/richsharples" rel="noopener noreferrer" target="_blank">@richsharples</a> Red Hat\'s contribution to the evolution of Java EE (eWeek\'s Darryl Taft): <a href="http://www.eweek.com/c/a/Application-Development/Red-Hat-Guides-Guns-for-Java-EE-Developers-699631/" rel="noopener noreferrer" target="_blank">www.eweek.com/c/a/Application-Development/Red-Hat-Guides-Guns-for-Java-EE-Developers-699631/</a> (Sad no mention of #jsf2)<br><br>In reply to: <a href="https://x.com/richsharples/status/6671286917" rel="noopener noreferrer" target="_blank">@richsharples</a> <span class="status">6671286917</span>',
    likes: 0,
    retweets: 0,
    tags: ['jsf2']
  },
  {
    id: '6652754546',
    created: 1260767336000,
    type: 'post',
    text: 'Got a #cdi (JSR-299) or #weld tutorial? Promote it here: <a href="http://seamframework.org/Documentation/LinksToExternalCDIAndWeldDocumentation" rel="noopener noreferrer" target="_blank">seamframework.org/Documentation/LinksToExternalCDIAndWeldDocumentation</a>',
    likes: 0,
    retweets: 0,
    tags: ['cdi', 'weld']
  },
  {
    id: '6648426110',
    created: 1260757568000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/jganoff" rel="noopener noreferrer" target="_blank">@jganoff</a> I noticed you are a Digitally Imported fan. Woot!',
    likes: 0,
    retweets: 0
  },
  {
    id: '6648393922',
    created: 1260757501000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Great! We really want a Twitter clone in either the Weld or Seam 3 examples. Eagerly awaiting to clone (Git it?).<br><br>In reply to: <a href="https://x.com/lightguardjp/status/6647940717" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6647940717</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6648336211',
    created: 1260757379000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> You are my hero, master ;)<br><br>In reply to: <a href="https://x.com/ALRubinger/status/6647085688" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">6647085688</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6643743870',
    created: 1260747501000,
    type: 'post',
    text: 'The new jboss.org community forums are live! <a href="http://community.jboss.org/" rel="noopener noreferrer" target="_blank">community.jboss.org/</a> User Guide about what changed -&gt; <a href="http://jboss.org/help/newforums" rel="noopener noreferrer" target="_blank">jboss.org/help/newforums</a>',
    likes: 0,
    retweets: 1
  },
  {
    id: '6633061804',
    created: 1260723226000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Yes, GlassFish for one important reason. You should start off using Java EE exactly to spec. No confusion this time around.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/6632675437" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">6632675437</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6623196184',
    created: 1260688169000,
    type: 'post',
    text: 'Followers, sorry about the tweet storm this week. I\'ve just been really churning out stuff and had to get my thoughts down and out.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6623183683',
    created: 1260688124000,
    type: 'post',
    text: 'I\'m trying to get the JCP to update the name of the JSR-299 spec on jcp.org. Doh!',
    likes: 0,
    retweets: 0
  },
  {
    id: '6623178419',
    created: 1260688104000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> The frame around jboss.org changed, but this is an actual migration to Clearspace. You\'ll see when they come back up.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/6622990944" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6622990944</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6622974130',
    created: 1260687402000,
    type: 'post',
    text: 'JBoss.org is down because we are going to the new forums! A little pain for a lot of gain ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '6622420678',
    created: 1260685559000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Weld is not yet a candidate for moving to Git. It is Seam that is going to be put into Git. Weld may will follow after that.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/6622187522" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6622187522</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6622038200',
    created: 1260684387000,
    type: 'post',
    text: 'Looks like I\'m headed to Vienna for JSFDays 2010. Still need to work out exact details, but likely I\'ll be giving the CDI presentation.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6621663357',
    created: 1260683310000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> EJB 3.1 is available in embedded mode. That should grow to other specs in Java EE 7 to be an embedded bean container.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/6619922825" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6619922825</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6621443013',
    created: 1260682700000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> I took action and updated the Weld doc with a new section that explains the beans.xml file in detail. Thanks for the hint!<br><br>In reply to: <a href="https://x.com/JohnAment/status/6615632063" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">6615632063</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6615275954',
    created: 1260667402000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> Here\'s the beans.xml schema, which has documentation: <a href="https://anonsvn.jboss.org/repos/weld/api/trunk/cdi/src/main/resources/beans.xsd" rel="noopener noreferrer" target="_blank">anonsvn.jboss.org/repos/weld/api/trunk/cdi/src/main/resources/beans.xsd</a><br><br>In reply to: <a href="https://x.com/JohnAment/status/6614624691" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">6614624691</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6615100881',
    created: 1260666972000,
    type: 'post',
    text: 'I just finished submitting $4,800 in expenses. That should help pay some bills ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '6613983758',
    created: 1260664194000,
    type: 'post',
    text: 'Wow, TestNG actually works in NetBeans 6.8 (at least for Maven 2 projects). That was a real showstopper for me before.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6613457773',
    created: 1260662843000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> It would be like going to a restaurant, but being required to bring your own plate and utensils.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/6612594678" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6612594678</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6613447318',
    created: 1260662816000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Understand that I don\'t dislike Jetty. I just think there are important things missing that I have to bring myself.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/6612594678" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6612594678</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6613424511',
    created: 1260662756000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Now "Jetty Web Profile", if such a thing existed, would be a true Christmas gift.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/6612594678" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6612594678</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6613414785',
    created: 1260662731000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> No, Jetty is just as problematic as Tomcat. Gavin lays it out well here: <a href="http://in.relation.to/Bloggers/TheNewEEEcosystem" rel="noopener noreferrer" target="_blank">in.relation.to/Bloggers/TheNewEEEcosystem</a><br><br>In reply to: <a href="https://x.com/lightguardjp/status/6612594678" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6612594678</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6613348605',
    created: 1260662559000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Yes, GlassFish is the RI. Pretty lightweight. I\'d just like to see an alt Web Profile impl that is only that. Jetty WP?<br><br>In reply to: <a href="https://x.com/lightguardjp/status/6612647834" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6612647834</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6611733315',
    created: 1260658383000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> This is your big moment. Destiny has called on you. Go for Android! #must #have #android<br><br>In reply to: <a href="https://x.com/jeremynorris/status/6610470548" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">6610470548</span>',
    likes: 0,
    retweets: 0,
    tags: ['must', 'have', 'android']
  },
  {
    id: '6611716648',
    created: 1260658339000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Yep, JSP is dead. Dead to me. Dead to all of us ;)<br><br>In reply to: <a href="https://x.com/jasondlee/status/6609546427" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">6609546427</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6611700831',
    created: 1260658298000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DavidGeary" rel="noopener noreferrer" target="_blank">@DavidGeary</a> One phrase "Web Profile" That is going to be solution that will save us. Now we just need a good impl. They are on the way.<br><br>In reply to: @davidgeary <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6606969507',
    created: 1260646051000,
    type: 'post',
    text: 'I\'m not filing out surveys anymore for our internal travel system because they don\'t listen to me. Hence, I won\'t waste my time.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6604955277',
    created: 1260640924000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matkar" rel="noopener noreferrer" target="_blank">@matkar</a> I sent an enraged response. I\'ll give them time to respond. Then, I\'ll consider communicating the ludicrous point to the community.<br><br>In reply to: <a href="https://x.com/matkar/status/6604702641" rel="noopener noreferrer" target="_blank">@matkar</a> <span class="status">6604702641</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6604624301',
    created: 1260640089000,
    type: 'post',
    text: 'I\'m going to pop my lid reading an internal e-mail about why Sun won\'t put the Java EE JARs in Maven central and I\'m tempted to publish it!',
    likes: 0,
    retweets: 0
  },
  {
    id: '6593009158',
    created: 1260599936000,
    type: 'post',
    text: 'Parameterized EL methods are now included in the approved EL spec PDF document: <a href="http://jcp.org/en/jsr/detail?id=245" rel="noopener noreferrer" target="_blank">jcp.org/en/jsr/detail?id=245</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6592796026',
    created: 1260599227000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> Absolutely! CDI is definitely part of the web profile!<br><br>In reply to: <a href="https://x.com/JohnAment/status/6577317446" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">6577317446</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6592765515',
    created: 1260599125000,
    type: 'post',
    text: 'I overheard that JAX-RS is going to get CDI injections after all. Need to check the RI to confirm, but that is awesome if true! #cdi #rest',
    likes: 0,
    retweets: 0,
    tags: ['cdi', 'rest']
  },
  {
    id: '6592742475',
    created: 1260599049000,
    type: 'post',
    text: 'Transformers I and II are both awesome. My favorite line. "I bought a car...Turned out the be an alien robot...Who knew?"',
    likes: 0,
    retweets: 0
  },
  {
    id: '6588803552',
    created: 1260588408000,
    type: 'post',
    text: 'Workin on a transformers marathon. Episode 1 last night for refresh, revenge tonight. Ch ch ch cha.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6585003548',
    created: 1260579739000,
    type: 'post',
    text: 'My wife organized her books today. That\'s the overflow from the bookshelves.',
    photos: ['<div class="entry"><img class="photo" src="media/6585003548.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '6582843590',
    created: 1260574887000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Woot! A lot of small voices have come together in support of Git.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/6582428683" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6582428683</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6582831297',
    created: 1260574859000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> No, this is #jsf2next, which means JSF 2.1. But Mojarra can implement stuff in advance (just not portable until JSF 2.1).<br><br>In reply to: <a href="https://x.com/lightguardjp/status/6582455421" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6582455421</span>',
    likes: 0,
    retweets: 0,
    tags: ['jsf2next']
  },
  {
    id: '6582806521',
    created: 1260574804000,
    type: 'post',
    text: 'I\'m about to go ape shit (I love that saying) on the JCP if our mailinglists for JSR-314 don\'t get fixed soon.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6580172513',
    created: 1260568869000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> At this point there is heavy, heavy support for Git and I think we should just try it. If we all hate it, we will reevaluate.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/6580019538" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6580019538</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6579105097',
    created: 1260566418000,
    type: 'post',
    text: '...and we\'re back!',
    likes: 0,
    retweets: 0
  },
  {
    id: '6578956747',
    created: 1260566081000,
    type: 'post',
    text: 'Lunch over, still no power.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6577795496',
    created: 1260563383000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Actually that is incorrect. JBoss EL is not EL 2.2. Just a prototype of it (predecessor).<br><br>In reply to: <a href="https://x.com/kito99/status/6573835459" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">6573835459</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6577636268',
    created: 1260563008000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cwash" rel="noopener noreferrer" target="_blank">@cwash</a> Actually, the future happened a couple days ago.<br><br>In reply to: <a href="https://x.com/cwash/status/6577464134" rel="noopener noreferrer" target="_blank">@cwash</a> <span class="status">6577464134</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6577595438',
    created: 1260562913000,
    type: 'post',
    text: 'Power just went out in the house. Fortunately, I still have my phone for internet backup ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '6576745790',
    created: 1260560940000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Good support for IDEs including Eclipse. So we are just going for it.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/6575970302" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">6575970302</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6575925427',
    created: 1260559062000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> I don\'t like to get into the politics, but I have to agree. SpringSource is just being counter productive w/ this stance.<br><br>In reply to: <a href="https://x.com/jasondlee/status/6575669318" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">6575669318</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6575893839',
    created: 1260558991000,
    type: 'post',
    text: 'I\'m pushing to get Seam 3 moved to Git asap. The build is borked anyway, so no time like the present to make the change.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6575001631',
    created: 1260557006000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Absolutely. I agree.<br><br>In reply to: <a href="https://x.com/kito99/status/6574889653" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">6574889653</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6574822065',
    created: 1260556607000,
    type: 'post',
    text: 'Message to community: The JSF EG listens. Please spread the word. Communicate here: <a href="http://wiki.jcp.org/boards/index.php?t=2744" rel="noopener noreferrer" target="_blank">wiki.jcp.org/boards/index.php?t=2744</a>',
    likes: 0,
    retweets: 1
  },
  {
    id: '6574761369',
    created: 1260556474000,
    type: 'post',
    text: 'JavaDoc for all of Java EE 6: <a href="http://java.sun.com/javaee/6/docs/api/" rel="noopener noreferrer" target="_blank">java.sun.com/javaee/6/docs/api/</a>',
    likes: 0,
    retweets: 2
  },
  {
    id: '6570318028',
    created: 1260547141000,
    type: 'post',
    text: 'I used to pass the time same way w/ LUG radio. <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Found a new way to pass driving time in the car: JBoss Community Asylum.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6570250506',
    created: 1260547003000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/edburns" rel="noopener noreferrer" target="_blank">@edburns</a> redbox rocks! DVD rentals for a $1 baby.<br><br>In reply to: <a href="https://x.com/edburns/status/6570158066" rel="noopener noreferrer" target="_blank">@edburns</a> <span class="status">6570158066</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6570134447',
    created: 1260546760000,
    type: 'post',
    text: 'redbox machines don\'t work with gloves. Bad news since they are all outside in the freezing cold right now. #redbox #fail #coldweather',
    likes: 0,
    retweets: 0,
    tags: ['redbox', 'fail', 'coldweather']
  },
  {
    id: '6569732625',
    created: 1260545933000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> 5.0 and 5.1 ;) <a href="http://www.jboss.org/jbossas/downloads/" rel="noopener noreferrer" target="_blank">www.jboss.org/jbossas/downloads/</a> There\'s even an EAP (supported version)<br><br>In reply to: <a href="https://x.com/jasondlee/status/6569601078" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">6569601078</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6569697681',
    created: 1260545860000,
    type: 'post',
    text: 'It\'s Friday and I\'m pumped to get stuff done.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6569573828',
    created: 1260545603000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/cbeams" rel="noopener noreferrer" target="_blank">@cbeams</a> The problem I have with tripit is that I enter in all the information and can\'t see it on my Android phone. #effort #fail',
    likes: 0,
    retweets: 0,
    tags: ['effort', 'fail']
  },
  {
    id: '6569542320',
    created: 1260545537000,
    type: 'post',
    text: 'Woot! <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Successful day @ work, got promoted, a huge bonus and huge raise. Successful day @ home <a href="http://scrumshark.com" rel="noopener noreferrer" target="_blank">scrumshark.com</a> 4000 views',
    likes: 0,
    retweets: 0
  },
  {
    id: '6569480112',
    created: 1260545409000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> JBoss AS 5 is Java EE 5. JBoss AS 6 is Java EE 6 (currently Web Profile + extras, soon to be full)<br><br>In reply to: <a href="https://x.com/jasondlee/status/6566745873" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">6566745873</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6558817101',
    created: 1260511880000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Currently part of Weld SE, which is a UE.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/6558585334" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6558585334</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6557201245',
    created: 1260507380000,
    type: 'post',
    text: 'Nice! RT <a class="mention" href="https://x.com/cbredesen" rel="noopener noreferrer" target="_blank">@cbredesen</a> JBoss AS 6.0.0.M1 with CDI - deploying a full-featured modern webapp with no WEB-INF/lib directory feels a little odd!',
    likes: 0,
    retweets: 0
  },
  {
    id: '6557172061',
    created: 1260507310000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> Amen! I was just saying that in my talk at JSF Summit. Time for a new JSR.<br><br>In reply to: <a href="https://x.com/jeremynorris/status/6555785481" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">6555785481</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6557134840',
    created: 1260507217000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Let\'s fix #2. Anything you don\'t like, just fire me an e-mail. We\'ll lower that barrier.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/6550399767" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6550399767</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6557116515',
    created: 1260507172000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Boya! I noticed you are using twidroid. Personally I dig TwitterRide. If you pick the gray theme, looks pretty nice ;)<br><br>In reply to: <a href="https://x.com/ALRubinger/status/6553710430" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">6553710430</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6551145450',
    created: 1260494949000,
    type: 'post',
    text: 'I got my haircut and sold my stylist on an #android. Killed two birds with one stone. It was the synced calendar w/ my wife that sold her.',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '6551084222',
    created: 1260494826000,
    type: 'post',
    text: 'Peter Royale implemented @ThreadScoped for Weld SE. How cool is that? Brings formality to the ThreadLocal. Rock on. #cdi',
    likes: 0,
    retweets: 1,
    tags: ['cdi']
  },
  {
    id: '6549363447',
    created: 1260491379000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Seems to be making quite an impact. Gotta hand it to that group for such a great idea.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/6547984858" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">6547984858</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6546332916',
    created: 1260485162000,
    type: 'post',
    text: 'There is so much activity on in.relation.to and seamframework.org that I can\'t even keep track of it myself. Crazy #progress',
    likes: 0,
    retweets: 1,
    tags: ['progress']
  },
  {
    id: '6545139431',
    created: 1260482634000,
    type: 'post',
    text: 'Santa Claus is now following me. Super. Santa, can I get a Playstation 3 for Christmas? If that\'s to pricey, a Wii will do (for RockBand 2).',
    likes: 0,
    retweets: 0
  },
  {
    id: '6545064644',
    created: 1260482477000,
    type: 'post',
    text: 'Target date for new JBoss.org community forums: 2009.12.14. A Happy Holidays present to you.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6542494454',
    created: 1260476681000,
    type: 'post',
    text: 'I got a personal thank you from the GlassFish team for reporting and updating bug reports in Mojarra that affected GlassFish V3. Sweet!',
    likes: 0,
    retweets: 0
  },
  {
    id: '6541585313',
    created: 1260474470000,
    type: 'post',
    text: 'IntelliJ IDEA 9 just got released. Reminds me that I haven\'t used IntelliJ in a while.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6539719381',
    created: 1260470397000,
    type: 'post',
    text: 'Offline mode in Firefox has to be the stupidest setting of any software I have ever used. #design #fail',
    likes: 0,
    retweets: 0,
    tags: ['design', 'fail']
  },
  {
    id: '6538323410',
    created: 1260467373000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/edburns" rel="noopener noreferrer" target="_blank">@edburns</a> I\'ll call it, "releasing against adversity"<br><br>In reply to: <a href="https://x.com/edburns/status/6538029203" rel="noopener noreferrer" target="_blank">@edburns</a> <span class="status">6538029203</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6538312832',
    created: 1260467350000,
    type: 'post',
    text: 'Congrats! RT <a class="mention" href="https://x.com/edburns" rel="noopener noreferrer" target="_blank">@edburns</a> Today is a huge day for Sun engineers. In spite of all the merger nonsense, they shipped NetBeans 6.8 &amp; Glassfish V3.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6523278352',
    created: 1260424449000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/richsharples" rel="noopener noreferrer" target="_blank">@richsharples</a> You\'ll get over it. Then some twisted part of you will miss it.<br><br>In reply to: <a href="https://x.com/richsharples/status/6522409167" rel="noopener noreferrer" target="_blank">@richsharples</a> <span class="status">6522409167</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6523262759',
    created: 1260424401000,
    type: 'post',
    text: 'Gmail finally supports embedding images in an e-mail message (via labs). First time I\'ve had that ability since Eudora ;) #progress',
    likes: 0,
    retweets: 0,
    tags: ['progress']
  },
  {
    id: '6519166279',
    created: 1260414632000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Yep, a cactus for mojavelinux.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/6518821351" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6518821351</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6518782168',
    created: 1260413899000,
    type: 'post',
    text: 'About 50% of my ornaments have penguins in them. Even the mojavelinux mascot gets represented.',
    photos: ['<div class="entry"><img class="photo" src="media/6518782168.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '6518722714',
    created: 1260413777000,
    type: 'post',
    text: 'Christmas tree deployed.',
    photos: ['<div class="entry"><img class="photo" src="media/6518722714.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '6509928782',
    created: 1260395735000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Yep.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/6509491099" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6509491099</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6509451750',
    created: 1260394711000,
    type: 'post',
    text: 'Ant is the type of ex-wife (or ex-husband) that always welcomes you home. Then you hate yourself for coming back.',
    likes: 1,
    retweets: 2
  },
  {
    id: '6507255412',
    created: 1260389868000,
    type: 'post',
    text: 'We need EL 2.2 in the Maven central to move forward <a href="http://jira.codehaus.org/browse/MAVENUPLOAD-2686" rel="noopener noreferrer" target="_blank">jira.codehaus.org/browse/MAVENUPLOAD-2686</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6505037031',
    created: 1260384987000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> I\'ll wish bad luck for you then ;)<br><br>In reply to: <a href="https://x.com/ALRubinger/status/6501317754" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">6501317754</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6505026228',
    created: 1260384964000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cagataycivici" rel="noopener noreferrer" target="_blank">@cagataycivici</a> I\'ll create sample app with PrimeFaces on JSF 2, Weld, JPA2 and JSR303. <a class="mention" href="https://x.com/mojavelinux" rel="noopener noreferrer" target="_blank">@mojavelinux</a>\'s talk on CDI was inspiring #jsfsummit.<br><br>In reply to: @cagataycivici <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 1,
    tags: ['jsfsummit']
  },
  {
    id: '6505003306',
    created: 1260384913000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cagataycivici" rel="noopener noreferrer" target="_blank">@cagataycivici</a> <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> releases ScrumShark. <a href="http://ocpsoft.com/scrumshark/" rel="noopener noreferrer" target="_blank">ocpsoft.com/scrumshark/</a><br><br>In reply to: @cagataycivici <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6485704964',
    created: 1260329614000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SBryzak" rel="noopener noreferrer" target="_blank">@SBryzak</a> Awesome. Wiki page?<br><br>In reply to: @sbryzak <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6481147649',
    created: 1260320534000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/NFJSMag" rel="noopener noreferrer" target="_blank">@NFJSMag</a> is now following me. You should definitely look into NFJS Magazine if you want some light reading over the holidays.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6480382061',
    created: 1260318955000,
    type: 'post',
    text: 'Hmm, so which is it? #false #advertising',
    photos: ['<div class="entry"><img class="photo" src="media/6480382061.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['false', 'advertising']
  },
  {
    id: '6478873426',
    created: 1260315702000,
    type: 'post',
    text: 'Vote for #jsf2next to support h:inputFile. <a href="https://github.com/javaee/javaserverfaces-spec/issues/690" rel="noopener noreferrer" target="_blank">github.com/javaee/javaserverfaces-spec/issues/690</a> Design ideas welcome.',
    likes: 0,
    retweets: 0,
    tags: ['jsf2next']
  },
  {
    id: '6477324795',
    created: 1260312040000,
    type: 'post',
    text: 'Aha! The presenter screen is an extension for OO.org 3. That explains why NeoOffice has one. It just bundles the extension.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6477179968',
    created: 1260311673000,
    type: 'reply',
    text: '@netdance Great point. Action Item ;)<br><br>In reply to: @netdance <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6477166384',
    created: 1260311639000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> I\'m not implying that you work on it. I\'m just saying whoever does.<br><br>In reply to: <a href="https://x.com/jasondlee/status/6476696927" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">6476696927</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6477148760',
    created: 1260311594000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> As my wife puts it, "I don\'t think the people who create this software actually use it."<br><br>In reply to: <a href="https://x.com/jasondlee/status/6476696927" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">6476696927</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6476236635',
    created: 1260309329000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Don\'t even get me started with OpenOffice.org 3. Why can\'t OO support notes on laptop when presenting on projector?<br><br>In reply to: <a href="https://x.com/jasondlee/status/6475520711" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">6475520711</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6475316156',
    created: 1260307268000,
    type: 'post',
    text: 'While giving my keynote at JSF Summit, NeoOffice on the machine running the slides allowed the screensaver to turn on. WTF?',
    likes: 0,
    retweets: 0
  },
  {
    id: '6473418234',
    created: 1260302821000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jbossnews" rel="noopener noreferrer" target="_blank">@jbossnews</a> Call for presentations for #JBossWorld is now open. Get yours in by Jan. 22, 2010: <em>&lt;expired link&gt;</em><br><br>In reply to: <a href="https://x.com/RHMiddleware/status/6466814986" rel="noopener noreferrer" target="_blank">@RHMiddleware</a> <span class="status">6466814986</span>',
    likes: 1,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '6447023585',
    created: 1260231332000,
    type: 'post',
    text: 'We can breathe easier. Parametrized EL method expressions made it into Java EE 6. Just not working in JBoss AS 6 yet. #jsfsummit #followup',
    likes: 0,
    retweets: 1,
    tags: ['jsfsummit', 'followup']
  },
  {
    id: '6446215561',
    created: 1260229680000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Other databases are good, but it would be nice to have a relational database that at least behaved itself ;) MySQL is a bad boy.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6445561397',
    created: 1260228285000,
    type: 'post',
    text: 'While at the post-JSF Summit party, I joked with @netdance about what a screwy database MySQL is. That\'s how I got to suggesting H2.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6444951000',
    created: 1260226996000,
    type: 'post',
    text: 'I almost fell over when I saw The Thirsty Fish at Portofino. Too bad it doesn\'t have beer selection of The Thirsty Bear. #jsfsummit #javaone',
    likes: 0,
    retweets: 2,
    tags: ['jsfsummit', 'javaone']
  },
  {
    id: '6444893815',
    created: 1260226877000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rayme" rel="noopener noreferrer" target="_blank">@rayme</a> Added to the JSF Summit 2009 postback entry.<br><br>In reply to: <a href="https://x.com/rayme/status/6443520655" rel="noopener noreferrer" target="_blank">@rayme</a> <span class="status">6443520655</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6444856692',
    created: 1260226805000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Props to you as well. You make a good team ;)<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/6443587322" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">6443587322</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6444844586',
    created: 1260226781000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jganoff" rel="noopener noreferrer" target="_blank">@jganoff</a> Got it. Will reply soon.<br><br>In reply to: <a href="https://x.com/jganoff/status/6444518748" rel="noopener noreferrer" target="_blank">@jganoff</a> <span class="status">6444518748</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6443031914',
    created: 1260222833000,
    type: 'post',
    text: 'Twitter, why the limit on search results? You are really throwing a wrench in our plans for #jsf2next tracking! #twitter #search #fail',
    likes: 0,
    retweets: 0,
    tags: ['jsf2next', 'twitter', 'search', 'fail']
  },
  {
    id: '6443005754',
    created: 1260222774000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> I mean something that\'ll pick up a post with #jsf2next and retweet it so it is archived in an account timeline, @jsf2next perhaps.<br><br>In reply to: <a href="https://x.com/kito99/status/6441192780" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">6441192780</span>',
    likes: 0,
    retweets: 0,
    tags: ['jsf2next']
  },
  {
    id: '6442919664',
    created: 1260222585000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/SanneGrinovero" rel="noopener noreferrer" target="_blank">@SanneGrinovero</a> makes inaugural post on in.relation.to <a href="https://in.relation.to/2009/12/07/hibernate-search-32-fast-index-rebuild/" rel="noopener noreferrer" target="_blank">in.relation.to/2009/12/07/hibernate-search-32-fast-index-rebuild/</a> It even includes diagrams! Covers Hibernate Search indexing.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6437928056',
    created: 1260210306000,
    type: 'post',
    text: 'JSF Summit 2009 postback: <a href="http://in.relation.to/Bloggers/JSFSummit2009Postback" rel="noopener noreferrer" target="_blank">in.relation.to/Bloggers/JSFSummit2009Postback</a> #jsfsummit #wrapup',
    likes: 0,
    retweets: 1,
    tags: ['jsfsummit', 'wrapup']
  },
  {
    id: '6436739054',
    created: 1260207512000,
    type: 'post',
    text: 'Does anyone have a picture of the Thirsty Fish bar at #jsfsummit?',
    likes: 0,
    retweets: 1,
    tags: ['jsfsummit']
  },
  {
    id: '6436322526',
    created: 1260206538000,
    type: 'post',
    text: 'Winter Wonderland at my wife\'s family farm: <a href="http://decorology.blogspot.com/2009/12/winter-wonderland-at-my-parents-farm.html" rel="noopener noreferrer" target="_blank">decorology.blogspot.com/2009/12/winter-wonderland-at-my-parents-farm.html</a> #snow',
    likes: 0,
    retweets: 0,
    tags: ['snow']
  },
  {
    id: '6423669247',
    created: 1260167229000,
    type: 'post',
    text: 'I also look forward to a Gradle console (CLI) to execute commands w/o having to start Gradle in between. #gradle',
    likes: 0,
    retweets: 0,
    tags: ['gradle']
  },
  {
    id: '6423604236',
    created: 1260167010000,
    type: 'post',
    text: 'If Gradle can accommodate tooling (meaning IDE can parse/modify script) then it will become the most dominant build tool in Java. #gradle',
    likes: 0,
    retweets: 0,
    tags: ['gradle']
  },
  {
    id: '6423564194',
    created: 1260166875000,
    type: 'reply',
    text: '@brianleathem Seam in Action provides an critical foundation of the problem to be solved. The core has changed, but extensions still apply.<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6423535993',
    created: 1260166784000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> Sure. We just want to give people an idea of where JSF is in use. Simple as that.<br><br>In reply to: <a href="https://x.com/JohnAment/status/6420820398" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">6420820398</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6423466079',
    created: 1260166547000,
    type: 'post',
    text: '#jsfsummit attendees, if I had promised to introduce you to someone, whatever the circumstance, and I forget, kindly remind me.',
    likes: 0,
    retweets: 1,
    tags: ['jsfsummit']
  },
  {
    id: '6420468637',
    created: 1260158109000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Hahaha, I\'m involved in these projects and even I can barely keep up ;) I\'ll take it as a sign of huge progress.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/6420353959" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6420353959</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6413821333',
    created: 1260142767000,
    type: 'post',
    text: 'Pete expands on the point I made at #jsfsummit about the new ecosystem for extensions in Java EE 6 <a href="http://java.dzone.com/articles/coming-java-ee-extensions-jsr" rel="noopener noreferrer" target="_blank">java.dzone.com/articles/coming-java-ee-extensions-jsr</a>',
    likes: 0,
    retweets: 2,
    tags: ['jsfsummit']
  },
  {
    id: '6413341052',
    created: 1260140030000,
    type: 'post',
    text: 'I got a lot of questions at #jsfsummit about whether Seam 2 works on JSF 2. Answer: Yes. FAQ here: <a href="http://seamframework.org/Documentation/DoesSeamWorkWithJSF2" rel="noopener noreferrer" target="_blank">seamframework.org/Documentation/DoesSeamWorkWithJSF2</a> #seam #jsf',
    likes: 0,
    retweets: 1,
    tags: ['jsfsummit', 'seam', 'jsf']
  },
  {
    id: '6412689972',
    created: 1260138547000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jganoff" rel="noopener noreferrer" target="_blank">@jganoff</a> Drop me a line in a PM with your e-mail address so that I can set you up w/ Seam 3 dev instructions. Wiki needs updating.<br><br>In reply to: <a href="https://x.com/jganoff/status/6335796722" rel="noopener noreferrer" target="_blank">@jganoff</a> <span class="status">6335796722</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6411798052',
    created: 1260136556000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Client-side state saving too. They could benefit from partial state saving. Btw, quite a wishlist there <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> ;)<br><br>In reply to: <a href="https://x.com/lincolnthree/status/6409042127" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">6409042127</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6410336338',
    created: 1260133656000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> Yeah, we need to setup a realtime archive script to an account. <a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> how do you do this for jsfcentral? #jsf2next<br><br>In reply to: <a href="https://x.com/aschwart/status/6405367621" rel="noopener noreferrer" target="_blank">@aschwart</a> <span class="status">6405367621</span>',
    likes: 0,
    retweets: 0,
    tags: ['jsf2next']
  },
  {
    id: '6409218077',
    created: 1260131449000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> I registered a 14 hour hibernate if you count the nap before dinner.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/6404565191" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">6404565191</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6409146489',
    created: 1260131300000,
    type: 'post',
    text: 'Had a scare when we first got home. Furnace wouldn\'t turn on. While waiting for the tech, all of a sudden it kicked into action.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6391870597',
    created: 1260076708000,
    type: 'post',
    text: 'That\'s <a class="mention" href="https://x.com/StevenBoscarine" rel="noopener noreferrer" target="_blank">@StevenBoscarine</a> to the right.',
    photos: ['<div class="entry"><img class="photo" src="media/6391870597.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '6391644538',
    created: 1260076098000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> I\'d be very skeptical about any such statement. JSF jobs are abound, even if not mentioned. It\'s Java EE at the end of the day.<br><br>In reply to: <a href="https://x.com/JohnAment/status/6370932547" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">6370932547</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6391437810',
    created: 1260075566000,
    type: 'post',
    text: 'I found out that NFJS is coming to my home town in Aug 2010 for the Greater Maryland Software, Columbia, MD <a href="http://www.nofluffjuststuff.com/conference/columbia/2010/08/home" rel="noopener noreferrer" target="_blank">www.nofluffjuststuff.com/conference/columbia/2010/08/home</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6379767102',
    created: 1260047390000,
    type: 'post',
    text: 'The kids on plane were sobing over leaving Disney. I was choked up for having to bid farewell to my comrades in the JSF community.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6376681491',
    created: 1260039851000,
    type: 'post',
    text: 'This is what we returned to from (not so sunny) Orlando, FL.',
    photos: ['<div class="entry"><img class="photo" src="media/6376681491.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '6375074112',
    created: 1260035874000,
    type: 'post',
    text: '"Sometimes you jump off the cliff, then build your wings."',
    likes: 0,
    retweets: 0
  },
  {
    id: '6371949060',
    created: 1260028045000,
    type: 'post',
    text: 'Several people got bitten by the Maven puts the hotel welcome page in your repository as a pom or jar bug this week. #maven #fail',
    likes: 0,
    retweets: 0,
    tags: ['maven', 'fail']
  },
  {
    id: '6371761857',
    created: 1260027551000,
    type: 'post',
    text: 'Proof that the free wifi isn\'t just a myth.',
    photos: ['<div class="entry"><img class="photo" src="media/6371761857.png"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '6370781255',
    created: 1260024923000,
    type: 'post',
    text: 'Googe maps needs to switch to the mall or airport directory once you get inside. #idea',
    likes: 0,
    retweets: 0,
    tags: ['idea']
  },
  {
    id: '6370105062',
    created: 1260022983000,
    type: 'post',
    text: 'Google gave me a Garmin for Christmas. #android',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '6363072392',
    created: 1259994954000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> Cool. I would love to hear more about your experience, etc. Care to do a blog entry w/ details?<br><br>In reply to: <a href="https://x.com/JohnAment/status/6354796790" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">6354796790</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6362934754',
    created: 1259994586000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Here is a list of the sessions that were presented: <a href="http://www.jsfsummit.com/conference/orlando/2009/12/sessions" rel="noopener noreferrer" target="_blank">www.jsfsummit.com/conference/orlando/2009/12/sessions</a><br><br>In reply to: <a href="https://x.com/lightguardjp/status/6362141972" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6362141972</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6362020954',
    created: 1259991730000,
    type: 'post',
    text: 'NFJS is about personalizing the experience. I\'m feel pretty confident that we accomplished that goal at this #jsfsummit.',
    likes: 0,
    retweets: 1,
    tags: ['jsfsummit']
  },
  {
    id: '6360851169',
    created: 1259988448000,
    type: 'post',
    text: 'Had many engaging conversations with @netdance (Jim Driscoll) today. We even discussed non-tech issues like dancing, Burmese food and travel',
    likes: 0,
    retweets: 0
  },
  {
    id: '6360789449',
    created: 1259988284000,
    type: 'post',
    text: 'The ever patient, but naggingly persistent navigation lady led @netdance, my wife and I to Ed Burn\'s post-#jsfsummit party. #android',
    likes: 0,
    retweets: 1,
    tags: ['jsfsummit', 'android']
  },
  {
    id: '6359797439',
    created: 1259985705000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> For conference attendees. Tell me what you want and I will hook you up.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/6353128947" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6353128947</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6359727532',
    created: 1259985540000,
    type: 'post',
    text: 'I turned Jim Driscoll on to H2 database and must have said "I love that guy" about Hans Dockter of Grade 2 dozen times at Ed Burn\'s party.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6358449860',
    created: 1259982415000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/edburns" rel="noopener noreferrer" target="_blank">@edburns</a> getting things done while staying in shape in his home office.',
    photos: ['<div class="entry"><img class="photo" src="media/6358449860.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '6349910404',
    created: 1259962343000,
    type: 'post',
    text: 'If you saw my Java EE 6 keynote at #jsfsummit, you might be interested in this seminar as the next step: <a href="http://blogs.sun.com/theaquarium/entry/launching_glassfish_v3_virtual_conference" rel="noopener noreferrer" target="_blank">blogs.sun.com/theaquarium/entry/launching_glassfish_v3_virtual_conference</a>',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6349855228',
    created: 1259962218000,
    type: 'post',
    text: 'School was finally out at #jsfsummit and it was pouring down rain :(',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6349836065',
    created: 1259962175000,
    type: 'post',
    text: 'Got and idea for an example app? Share it here: <a href="http://sfwk.org/Documentation/ExampleIdeas" rel="noopener noreferrer" target="_blank">sfwk.org/Documentation/ExampleIdeas</a> #weld #seam #jsfsummit',
    likes: 0,
    retweets: 0,
    tags: ['weld', 'seam', 'jsfsummit']
  },
  {
    id: '6349407619',
    created: 1259961194000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/timoreilly" rel="noopener noreferrer" target="_blank">@timoreilly</a> You force close app on #android using Taskiller (or similar). Holding down Home key shows recently used apps, not running apps.<br><br>In reply to: <a href="https://x.com/timoreilly/status/6345857955" rel="noopener noreferrer" target="_blank">@timoreilly</a> <span class="status">6345857955</span>',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '6349350138',
    created: 1259961061000,
    type: 'post',
    text: 'Great idea for a sample application: <a href="http://swagswap.appspot.com/jsf/home.jsf" rel="noopener noreferrer" target="_blank">swagswap.appspot.com/jsf/home.jsf</a> Even has multi-implementations already.',
    likes: 1,
    retweets: 0
  },
  {
    id: '6349278669',
    created: 1259960896000,
    type: 'post',
    text: 'When people register for a conference, they should have a box to check as to whether they want the bag or not. #jsfsummit #nfjs',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit', 'nfjs']
  },
  {
    id: '6348814610',
    created: 1259959823000,
    type: 'post',
    text: 'Wow! I just discovered Google Navigator on my phone after the Google Maps update. Full GPS on my phone w/ voice commands! #android',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '6345394571',
    created: 1259952027000,
    type: 'post',
    text: 'Find a void, fill the void, standardize the fill. <a class="mention" href="https://x.com/jbalunas" rel="noopener noreferrer" target="_blank">@jbalunas</a> #jsfsummit',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6345331967',
    created: 1259951886000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> Yeah it was a traffic jam at checkout.<br><br>In reply to: <a href="https://x.com/aschwart/status/6340892857" rel="noopener noreferrer" target="_blank">@aschwart</a> <span class="status">6340892857</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6345310071',
    created: 1259951838000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Haha I was in the wrong room. I found #myarmy in the adjacent room #jsfsummit<br><br>In reply to: <a href="https://x.com/lightguardjp/status/6342778633" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6342778633</span>',
    likes: 0,
    retweets: 0,
    tags: ['myarmy', 'jsfsummit']
  },
  {
    id: '6345279191',
    created: 1259951773000,
    type: 'post',
    text: 'At the last talk of the conference, Jay Balunas talking about the road to standards in JSF. #jsfsummit',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6340776629',
    created: 1259942573000,
    type: 'post',
    text: 'I\'m lonely in the CDI and Seam BOF. Come join me. We\'ll even talk about religion and philosophy if you want. #jsfsummit',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6339224443',
    created: 1259939330000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> Great! Thx!<br><br>In reply to: @pelegri <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6339098326',
    created: 1259939064000,
    type: 'post',
    text: 'Okay, WARlets are like plugins to the main WAR file. Start page, parameterized template, etc.Distributed Facelets? #jsfsummit',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6339030403',
    created: 1259938924000,
    type: 'post',
    text: 'Stan Silvert presenting WARlet to mix WAR files at runtime time. Shared context, etc. Still fuzzy on it #jsfsummit',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6338690206',
    created: 1259938191000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> Yep, that\'s all this is. Split out EL so that it can mature on it\'s own. #futurejsr<br><br>In reply to: @pelegri <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['futurejsr']
  },
  {
    id: '6337749306',
    created: 1259936069000,
    type: 'post',
    text: 'Made it up in time for the first session. Participating in live demo in Stan Silvert\'s session Dumping JSF (debugging) #jsfsummit.',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6337205245',
    created: 1259934755000,
    type: 'reply',
    text: '@brianleathem Great question. I will find out and tweet. Perhaps we should start a petition to the jcp. #jsfsummit<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6337109179',
    created: 1259934513000,
    type: 'post',
    text: 'Last month I was in reall Europe (Antwerp). This month I\'m in fake Europe (Loews Portofino). #jsfsummit',
    photos: ['<div class="entry"><img class="photo" src="media/6337109179.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6329168391',
    created: 1259906467000,
    type: 'post',
    text: 'I may have inspired <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> to file an EL spec in resp to my latest call to action #jsfsummit #myarmy',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit', 'myarmy']
  },
  {
    id: '6329115636',
    created: 1259906316000,
    type: 'reply',
    text: '@brianleathem Yes, we need to get that changed! Please speak out. <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> had the same barrier when becoming JCP member #jsfsummit<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6329053844',
    created: 1259906124000,
    type: 'post',
    text: 'It\'s hard to go to your buddy\'s talk when you have to give a talk at the same time. Sorry I couldn\'t see you speak <a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> #jsfsummit',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6328986911',
    created: 1259905921000,
    type: 'post',
    text: 'Wow, my 7 presenations at #jsfsummit come to a close. I can finally pause to enjoy the Orlando sunshine. I hope everyone enjoyed the conf!',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6328407140',
    created: 1259904228000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> presented only his second conference talk in front of a crowd with 8 EG members. And he made it look easy ;)',
    likes: 0,
    retweets: 1
  },
  {
    id: '6328272063',
    created: 1259903854000,
    type: 'post',
    text: 'Go to jcp.org and register for an account. First step to paticipating in the Java platorm &amp; process. #jsfsummit',
    likes: 0,
    retweets: 1,
    tags: ['jsfsummit']
  },
  {
    id: '6316716367',
    created: 1259878185000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> speaking about #prettyfaces at #jsfsummit. Off to a great start!',
    photos: ['<div class="entry"><img class="photo" src="media/6391870597.jpg"></div>'],
    likes: 0,
    retweets: 1,
    tags: ['prettyfaces', 'jsfsummit']
  },
  {
    id: '6311611187',
    created: 1259866379000,
    type: 'post',
    text: 'My one criticism from a 10,000 ft view. HTML seems to the wrong acronym for what HTML 5 is tackling. #richweb',
    likes: 0,
    retweets: 0,
    tags: ['richweb']
  },
  {
    id: '6311561395',
    created: 1259866264000,
    type: 'post',
    text: 'HTML 5 is going to be a crucial concern of #jsf2next. We are now positioned to be ready for it w/ so much activity. #jsfsummit',
    likes: 0,
    retweets: 0,
    tags: ['jsf2next', 'jsfsummit']
  },
  {
    id: '6311468208',
    created: 1259866050000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ClaireLeah" rel="noopener noreferrer" target="_blank">@ClaireLeah</a> Us folks in Java EE constantly have to tip toe around legal issues, so yeah, interested for sure.<br><br>In reply to: <a href="https://x.com/ClaireLeah/status/6309087242" rel="noopener noreferrer" target="_blank">@ClaireLeah</a> <span class="status">6309087242</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6311402715',
    created: 1259865900000,
    type: 'post',
    text: 'Watching a very interesting expert panel discussion at #jsfsummit #richweb about HTML5 and its importance in the future.',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit', 'richweb']
  },
  {
    id: '6308836444',
    created: 1259860040000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ClaireLeah" rel="noopener noreferrer" target="_blank">@ClaireLeah</a> Aha, a name to a face. It was great to meet you!<br><br>In reply to: <a href="https://x.com/ClaireLeah/status/6308497734" rel="noopener noreferrer" target="_blank">@ClaireLeah</a> <span class="status">6308497734</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6308733851',
    created: 1259859820000,
    type: 'post',
    text: 'Neil Griffin: "I put JBoss EL in every project I create." (Testimony for how necessary parameterized EL is to productivity) #jsfsummit',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6308506619',
    created: 1259859340000,
    type: 'post',
    text: 'At the first possible opportunty we must get &lt;h:inputFile&gt; standardized (both Ajax and non-Ajax) #jsfsummit #jsf2next',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit', 'jsf2next']
  },
  {
    id: '6308299214',
    created: 1259858876000,
    type: 'post',
    text: 'I may have said this before, but it is a technology #fail that my laptop can\'t use my phone\'s reliable 3G connection (out of box) #android',
    likes: 0,
    retweets: 0,
    tags: ['fail', 'android']
  },
  {
    id: '6308244842',
    created: 1259858754000,
    type: 'post',
    text: 'At Micha Kiener\'s talk on edoras. Cameo by Neil Griffin. #jsfsummit',
    photos: ['<div class="entry"><img class="photo" src="media/6308244842.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6307290489',
    created: 1259856688000,
    type: 'post',
    text: 'At Micha Kiener\'s talk on edoras. Camo by Neil Griffin.',
    photos: ['<div class="entry"><img class="photo" src="media/6308244842.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '6306151873',
    created: 1259854254000,
    type: 'post',
    text: 'Come to dinner tonight and hear my keynote - Java EE 6: Here and Now (my last hurrah before my voice goes) #jsfsummit',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6306088153',
    created: 1259854117000,
    type: 'post',
    text: 'Part of reason I didn\'t get up was b/c of great night at Thirsty Fish w/ JSF EG and friends. Great comradery and debate #jsfsummit',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6306004772',
    created: 1259853935000,
    type: 'post',
    text: 'I admit I didn\'t make it up in time to attend the first talk of the day. I did catch end of breakfast &amp; chatted w/ Neil Griffin #jsfsummit',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6305425504',
    created: 1259852657000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/mollydotcom" rel="noopener noreferrer" target="_blank">@mollydotcom</a> is following me, one of "the JSF characters"! I\'m honored!',
    likes: 0,
    retweets: 0
  },
  {
    id: '6295960844',
    created: 1259821081000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> More like at half way through talk #4 of the day (marathon being a metaphor for 4 back-to-back 90 min talks at #jsfsummit)<br><br>In reply to: <a href="https://x.com/arungupta/status/6295771471" rel="noopener noreferrer" target="_blank">@arungupta</a> <span class="status">6295771471</span>',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6295728113',
    created: 1259820342000,
    type: 'post',
    text: 'I now know what it must feel like to run a marathon. I could not have asked for the day to go any better. What a great day. #jsfsummit',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6295686366',
    created: 1259820213000,
    type: 'post',
    text: 'M$ according to <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a>. "I notice you are using your computer. Would you like a problem today?"',
    likes: 0,
    retweets: 0
  },
  {
    id: '6277813844',
    created: 1259780659000,
    type: 'post',
    text: 'Had some great laughs over lunch with <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a>, <a class="mention" href="https://x.com/mollydotcom" rel="noopener noreferrer" target="_blank">@mollydotcom</a> and friend. #jsfsummit',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6277636444',
    created: 1259780260000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/edburns" rel="noopener noreferrer" target="_blank">@edburns</a> Believe me, I am relieved. Some times, these things just have a way of working themselves out.<br><br>In reply to: <a href="https://x.com/edburns/status/6277570738" rel="noopener noreferrer" target="_blank">@edburns</a> <span class="status">6277570738</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6277619896',
    created: 1259780222000,
    type: 'post',
    text: 'Get socialize source code from Seam+RESTEasy talk here: <a href="http://code.google.com/p/seaminaction/source/browse/#svn/demos/presentations/jsfsummit09" rel="noopener noreferrer" target="_blank">code.google.com/p/seaminaction/source/browse/#svn/demos/presentations/jsfsummit09</a> #jsfsummit',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6277526445',
    created: 1259780007000,
    type: 'post',
    text: 'My keynote titled "Java EE 6: Here and Now" is being moved to tomorrow night\'s dinner. There was some mixup. #jsfsummit',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6277510604',
    created: 1259779970000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> did an excellent job in the Seam+RESTEasy talk presenting architectural principles of REST. Good participation too! #jsfsummit',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6277428994',
    created: 1259779781000,
    type: 'reply',
    text: '@mwessendorf Sorry about that Matthias, that was a misunderstanding on my part. To be honest, I\'m still trying to work out who is on the EG.<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6272482918',
    created: 1259768616000,
    type: 'post',
    text: 'Let\'s hear your ideas for #jsf2next. Go to www.javaserverfaces.org to find link to forums. #jsfsummit',
    likes: 0,
    retweets: 0,
    tags: ['jsf2next', 'jsfsummit']
  },
  {
    id: '6270064043',
    created: 1259763184000,
    type: 'reply',
    text: '@mwessendorf We just had breakfast outside in Orlando at #jsfsummit! Quite a contrast.<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6270037400',
    created: 1259763118000,
    type: 'post',
    text: 'Beginning my marathon day at #jsfsummit 4 talks, a keynote and sitting on a panel. Come on mind, don\'t let me down. First talk #jsf2next.',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit', 'jsf2next']
  },
  {
    id: '6254648679',
    created: 1259717524000,
    type: 'post',
    text: 'mercury1: JSF+Seam+Facelets is incredibly easy and productive. (Just wait until EE 6) #jsfsummit',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6254532201',
    created: 1259717299000,
    type: 'post',
    text: 'Ed playing intervirew w/ mercury1; they use Seam and also JBoss Dev Studio because it has the best autocompletion in xhtml. #jsfsummit',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6254072656',
    created: 1259716407000,
    type: 'post',
    text: 'Feels like the band just got together at #jsfsummit. We aren\'t getting *back* together because many of us are meeting for the first time.',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6253380405',
    created: 1259715067000,
    type: 'post',
    text: 'Just finished up a nice personal workshop on Seam at #jsfsummit. I focused on giving advice for both present and future.',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6239345807',
    created: 1259684528000,
    type: 'post',
    text: 'Once again, SpringSource did not vote on a JSR.',
    likes: 0,
    retweets: 1
  },
  {
    id: '6239300302',
    created: 1259684428000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> Take the taxi if you want to get there efficiently. Take the van if you don\'t mind doing some touring of Orlando to save $.<br><br>In reply to: <a href="https://x.com/aschwart/status/6238716477" rel="noopener noreferrer" target="_blank">@aschwart</a> <span class="status">6238716477</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6239275775',
    created: 1259684377000,
    type: 'post',
    text: 'JSR-299 is approved: <a href="http://jcp.org/en/jsr/results?id=5017" rel="noopener noreferrer" target="_blank">jcp.org/en/jsr/results?id=5017</a> #cdi #jcp #javaee',
    likes: 0,
    retweets: 2,
    tags: ['cdi', 'jcp', 'javaee']
  },
  {
    id: '6218616899',
    created: 1259627900000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Nope, still in van touring the Orlando business district. Dinner still in my future. Probably in 30 minutes or so.<br><br>In reply to: <a href="https://x.com/kito99/status/6218242037" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">6218242037</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6218027475',
    created: 1259626669000,
    type: 'post',
    text: 'I\'m working on getting a JSF EG meeting setup after the panel discussion on Wed #jsfsummit',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '6217994574',
    created: 1259626603000,
    type: 'post',
    text: 'This whole van thing to the airport is so bizarre after getting used to always being able to depend on a train system in Europe.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6217918299',
    created: 1259626445000,
    type: 'post',
    text: 'Frankly, I was just too busy for Black Friday and Cyber Monday this year. I\'ll be more interested in Last Minute Tuesday ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '6217790784',
    created: 1259626179000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> I\'m in the van right now on the way to the hotel. My wife is joining me on this trip, so she\'ll get to meet you too.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/6214381748" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">6214381748</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6217707452',
    created: 1259626005000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> I love my logitech keyboard. Rarely lets me down.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6212149175',
    created: 1259613012000,
    type: 'post',
    text: 'Free wifi in BWI airport courtesy of #google. Internet as a public service FTW.',
    likes: 0,
    retweets: 0,
    tags: ['google']
  },
  {
    id: '6210160269',
    created: 1259608378000,
    type: 'post',
    text: 'I\'m shipping off to Orlando!',
    likes: 0,
    retweets: 0
  },
  {
    id: '6209386388',
    created: 1259606620000,
    type: 'post',
    text: 'I think Pete handled the community feedback for the Weld logo perfectly: <a href="http://in.relation.to/Bloggers/WeldLogoFeedbackFollowup" rel="noopener noreferrer" target="_blank">in.relation.to/Bloggers/WeldLogoFeedbackFollowup</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6209231419',
    created: 1259606256000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Could you blast this out to the weld-dev list? I\'m not going to have time, but I know the Weld devs would be interested.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/6208989934" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">6208989934</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6202879230',
    created: 1259592036000,
    type: 'post',
    text: 'I have been going strong all weekend getting ready for JSF Summit and trying to stay afloat on active initiatives. And it\'s Monday :(',
    likes: 0,
    retweets: 0
  },
  {
    id: '6182462822',
    created: 1259534773000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Um, no. It is twice as slow. Its speed reduced by half ;) I think my brain is also moving slower now.<br><br>In reply to: <a href="https://x.com/maxandersen/status/6181495960" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">6181495960</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6181030222',
    created: 1259531326000,
    type: 'post',
    text: 'I upgraded to Open Office 3.1 and Impress slowed down by about half :(',
    likes: 0,
    retweets: 0
  },
  {
    id: '6176938587',
    created: 1259521725000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> I\'d love to switch to Gradle, but may not be the right time just yet.<br><br>In reply to: <a href="https://x.com/JohnAment/status/6172899338" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">6172899338</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6172864757',
    created: 1259510913000,
    type: 'post',
    text: 'Trying to figure out how Maven archetypes are supposed to be setup is like chasing a rabbit down a hole. A deep, dark hole. Crappy plugin.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6143902997',
    created: 1259423959000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> Personally, I like the physical keyboard. One of my favorite things about the G1. I guess it is a personal choice.<br><br>In reply to: <a href="https://x.com/jeremynorris/status/6143889997" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">6143889997</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6143306449',
    created: 1259422289000,
    type: 'post',
    text: 'Finally got a chance to play around with the Droid. My buddy <a class="mention" href="https://x.com/Cmoose" rel="noopener noreferrer" target="_blank">@Cmoose</a> picked one up. Love the extra screen real estate. #android',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '6143242248',
    created: 1259422112000,
    type: 'reply',
    text: '@mwessendorf That\'s the call to action that gets me fired up. We are going to make it happen ;)<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6142994329',
    created: 1259421405000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> Amen. I will be doing a segment in my JSF 2 and Java EE 6 talks on that exact subject. We also need to align the flash scope.<br><br>In reply to: <a href="https://x.com/JohnAment/status/6138809601" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">6138809601</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6134177487',
    created: 1259386159000,
    type: 'post',
    text: 'You can also use #jsf2next to tweet about your vision for the future of JSF.',
    likes: 0,
    retweets: 0,
    tags: ['jsf2next']
  },
  {
    id: '6134145183',
    created: 1259386057000,
    type: 'post',
    text: 'I have so much work to do tomorrow that I\'m going to go cry myself to sleep.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6134125895',
    created: 1259385997000,
    type: 'post',
    text: 'I wanted to take a picture of <a class="mention" href="https://x.com/Cmoose" rel="noopener noreferrer" target="_blank">@Cmoose</a> taking a picture of the ATM machine with the guy looking at him like he just left an insane asylum.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6121443303',
    created: 1259352978000,
    type: 'post',
    text: 'What the heck happened to the Giants? Denver lost two bad ones in a row, surprising to see them school Manning\'s Giants.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6092773694',
    created: 1259270318000,
    type: 'post',
    text: 'Enjoying a Duvel with my father-in-law in glasses I brought back from Belgium.',
    photos: ['<div class="entry"><img class="photo" src="media/6092773694.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '6085324590',
    created: 1259252422000,
    type: 'post',
    text: 'Just downloaded Unlocking Android in epub and mobi format to read on my Android. Self hosting!',
    likes: 0,
    retweets: 0
  },
  {
    id: '6085227125',
    created: 1259252206000,
    type: 'reply',
    text: '@mwessendorf Weld is the open source reference implementation of JSR-299 developed by Red Hat &amp; its (awesome) community.<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6085186746',
    created: 1259252118000,
    type: 'post',
    text: 'Q&amp;A with Gavin King on JSR-299 (CDI): <a href="http://www.infoq.com/news/2009/11/weld10" rel="noopener noreferrer" target="_blank">www.infoq.com/news/2009/11/weld10</a> He talks about inclusion in GlassFish V3.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6084799949',
    created: 1259251258000,
    type: 'post',
    text: 'Christian Bauer shaved his head since I last saw him. When he first showed up at the JBoss booth, I didn\'t realize it was him at first.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6084482437',
    created: 1259250543000,
    type: 'reply',
    text: '@mwessendorf Yep, Seam is going to be extremely modular, so we need to make it easy to contribute. SVN is terrible at supporting that.<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6084464103',
    created: 1259250500000,
    type: 'post',
    text: 'The Weld logo is about to be selected. Here\'s your last chance to give feedback. Vote here: <a href="http://spreadsheets.google.com/viewform?formkey=dDFmTUlqS3NrNlRDUWhMbGRTUWRMdFE6MA" rel="noopener noreferrer" target="_blank">spreadsheets.google.com/viewform?formkey=dDFmTUlqS3NrNlRDUWhMbGRTUWRMdFE6MA</a>',
    likes: 0,
    retweets: 1
  },
  {
    id: '6084236169',
    created: 1259249987000,
    type: 'post',
    text: 'The #seam and #weld projects are considering moving to either Git or Mercurial. Voice your opinion on the Weld forums!',
    likes: 0,
    retweets: 1,
    tags: ['seam', 'weld']
  },
  {
    id: '6084214438',
    created: 1259249937000,
    type: 'post',
    text: 'Pete imported Weld core into GitHub <a href="http://github.com/pmuir/weld-test" rel="noopener noreferrer" target="_blank">github.com/pmuir/weld-test</a> and BitBucket <a href="http://bitbucket.org/pmuir/weld-test/" rel="noopener noreferrer" target="_blank">bitbucket.org/pmuir/weld-test/</a> for sandboxing.',
    likes: 0,
    retweets: 1
  },
  {
    id: '6072067364',
    created: 1259209662000,
    type: 'post',
    text: 'It\'s nice to see some movement on JBoss AS embedded. Good job ALR! Can\'t wait to start using this in every stage of development!',
    likes: 0,
    retweets: 1
  },
  {
    id: '6065095944',
    created: 1259192563000,
    type: 'post',
    text: 'If I don\'t remember to say it, "happy turkey day". I\'ll be trying to avoid work by drinking Belgian beer and playing rock band ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '6065080205',
    created: 1259192527000,
    type: 'post',
    text: 'I made one talk at #devoxx. Can you guess the topic? #android ;) The other talk I went to doesn\'t count since I dozed off from exhaustion.',
    likes: 0,
    retweets: 0,
    tags: ['devoxx', 'android']
  },
  {
    id: '6056072642',
    created: 1259171353000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> Yep, the pattern jsr-xxx-public@jcp.org should end up in the appropriate forum. I\'m not sure if it validates the e-mail address.<br><br>In reply to: @pelegri <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6053049870',
    created: 1259164515000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Yep. That will be a portable extension and likely into CDI at some point in the future. That\'s the beauty of this process.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/6051485359" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6051485359</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6049944968',
    created: 1259157231000,
    type: 'post',
    text: 'Emails sent to jsr-314-public@jcp.org end up here: <a href="http://wiki.jcp.org/boards/index.php?b=958" rel="noopener noreferrer" target="_blank">wiki.jcp.org/boards/index.php?b=958</a> You must register at jcp.org first.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6011982650',
    created: 1259079676000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Hahaha. You just have to tell them you were being thorough in your QA.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/6009902247" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">6009902247</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6011938959',
    created: 1259079576000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> Either from iPhone or Blackberry.<br><br>In reply to: @pelegri <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6007834114',
    created: 1259069732000,
    type: 'post',
    text: 'Here\'s the test site for the new jboss.org community platform (forums/wiki): <a href="http://community.jboss.org" rel="noopener noreferrer" target="_blank">community.jboss.org</a> Stomp on it.',
    likes: 0,
    retweets: 1
  },
  {
    id: '6007807162',
    created: 1259069657000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Hibernate will go sooner. It\'s included in the migration from the current jboss.org forums and wiki to clearspace.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/6007686650" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">6007686650</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '6006447649',
    created: 1259065634000,
    type: 'post',
    text: 'It\'s very likely that seamframework.org will join the greater JBoss community at jboss.org in early 2010. Details to come.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6006413640',
    created: 1259065523000,
    type: 'post',
    text: 'I\'ve convinced a lot of people to switch to #android. And all I have to show for it is this tweet ;)',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '6006354670',
    created: 1259065330000,
    type: 'post',
    text: 'My 33 hour nap just proves my earlier statement that #devoxx 09 was the most exhausting conference I\'ve ever done. Good times.',
    likes: 0,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '6006312244',
    created: 1259065192000,
    type: 'post',
    text: 'I don\'t get why they make you turn off your music player on takeoff. I\'ve heard the safety spiel 100s of times and I\'m trying not to hurl.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6006239755',
    created: 1259064948000,
    type: 'post',
    text: 'After arriving home Sunday, I went to bed at 21:00 (9:00pm) and woke up this morning (Tuesday) at 06:00 (6:00am). 33 hours of sleep.',
    likes: 0,
    retweets: 0
  },
  {
    id: '6006213856',
    created: 1259064861000,
    type: 'post',
    text: 'Just filled out #devoxx survey: <a href="http://questionpro.com/t/ABpcwZGSsw" rel="noopener noreferrer" target="_blank">questionpro.com/t/ABpcwZGSsw</a> I suggested having more breakout rooms for quiet discussions.',
    likes: 0,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '6006049135',
    created: 1259064299000,
    type: 'post',
    text: 'The first time you hear the radio after getting back from Europe, it sounds like the radio DJs are speaking a foreign language. Weird.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5957413675',
    created: 1258928374000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rayme" rel="noopener noreferrer" target="_blank">@rayme</a> Those American bastards are just jealous that I like Belgian beer better. I was robbed.<br><br>In reply to: <a href="https://x.com/rayme/status/5953018927" rel="noopener noreferrer" target="_blank">@rayme</a> <span class="status">5953018927</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5955026948',
    created: 1258922786000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> and #javaee, #android, #jcp, #shrinkwrap and #arquillian...and probably more. The guy next to me said to me "you talk a lot" :)<br><br>In reply to: <a href="https://x.com/kito99/status/5949953671" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">5949953671</span>',
    likes: 0,
    retweets: 0,
    tags: ['javaee', 'android', 'jcp', 'shrinkwrap', 'arquillian']
  },
  {
    id: '5953373457',
    created: 1258918795000,
    type: 'post',
    text: 'If you can believe the odds, <a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> and I were on the same flight, in the same row, and we weren\'t even coming from the same event!',
    likes: 0,
    retweets: 0
  },
  {
    id: '5952269284',
    created: 1258915992000,
    type: 'post',
    text: 'Learned from Christian best way to communicate w/ annoying neighbors is using SSID. The example scenario was excessive bedroom noise.',
    likes: 0,
    retweets: 1
  },
  {
    id: '5952135343',
    created: 1258915645000,
    type: 'post',
    text: 'Duty free #fail. Liquids didn\'t make it through transfer in NY. Kicking myself because they would have fit in duffel.',
    likes: 0,
    retweets: 0,
    tags: ['fail']
  },
  {
    id: '5951601232',
    created: 1258914248000,
    type: 'post',
    text: 'This neckband pouch for passport and tickets is a true handsaver on int\'l flights.',
    photos: ['<div class="entry"><img class="photo" src="media/5951601232.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '5938005264',
    created: 1258864823000,
    type: 'post',
    text: 'You need a special thought process to be able to drive in Flanders. We ended up driving on a sidewalk or closed st. a few times last night.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5937929048',
    created: 1258864591000,
    type: 'post',
    text: 'No missing planes today. I\'m leaving extra early. Consider me reformed :)',
    likes: 0,
    retweets: 0
  },
  {
    id: '5921839400',
    created: 1258820531000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> No way! I\'m on my way to Brussels right now too. Of course, I\'ll see you for a whole week at JSFSummit ;)<br><br>In reply to: <a href="https://x.com/kito99/status/5920170563" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">5920170563</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5915351398',
    created: 1258796364000,
    type: 'post',
    text: 'Manik got me thinking that we should look into how #arquillian can offer parallelization for tests. That would really be nice.',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '5915330118',
    created: 1258796252000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> Don\'t tell me you guys were out all night.<br><br>In reply to: <a href="https://x.com/rruss/status/5912413233" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">5912413233</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5907081270',
    created: 1258767742000,
    type: 'post',
    text: 'The #devoxx week is officially over. Now, I\'m off to find some chocolate in Ghent with my brother and his friend Luigi. After some sleep ;)',
    likes: 0,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '5906967461',
    created: 1258767442000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sduskis" rel="noopener noreferrer" target="_blank">@sduskis</a> Cool! That would be great. Any ideas to make the Seam + RESTeasy integration smoother, we are all ears.<br><br>In reply to: <a href="https://x.com/sduskis/status/5897292113" rel="noopener noreferrer" target="_blank">@sduskis</a> <span class="status">5897292113</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5897258320',
    created: 1258743410000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sduskis" rel="noopener noreferrer" target="_blank">@sduskis</a> We\'ll take you up on that challenge. <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a>, are you up for the challenge?<br><br>In reply to: <a href="https://x.com/sduskis/status/5897142670" rel="noopener noreferrer" target="_blank">@sduskis</a> <span class="status">5897142670</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5897177079',
    created: 1258743207000,
    type: 'post',
    text: 'Pete speaking at the #seam community event. Ironically he is speaking about #cdi.',
    photos: ['<div class="entry"><img class="photo" src="media/5897177079.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['seam', 'cdi']
  },
  {
    id: '5897107314',
    created: 1258743034000,
    type: 'post',
    text: 'Audience at #seam community event in Antwerp.',
    photos: ['<div class="entry"><img class="photo" src="media/5897107314.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['seam']
  },
  {
    id: '5896686917',
    created: 1258741997000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> We are still working on him.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/5896656390" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">5896656390</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5893632846',
    created: 1258734864000,
    type: 'post',
    text: 'Manual autodiscovery. An oxymoron? #jopr',
    likes: 0,
    retweets: 0,
    tags: ['jopr']
  },
  {
    id: '5888971817',
    created: 1258724150000,
    type: 'post',
    text: 'Just arrived at ViaVia in preperation for #seam and #infinispan community event. Lots of room on upper levels! And, free weefee! FTW',
    likes: 0,
    retweets: 0,
    tags: ['seam', 'infinispan']
  },
  {
    id: '5884886485',
    created: 1258709160000,
    type: 'post',
    text: 'Every conference talk should have a slide to recommend/remind which twitter hashtag should be used to talk about the technology.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5884875802',
    created: 1258709111000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> Yes! The demo of #arquillian worked splendidly. The only hiccup was that Pete had to fill time while embedded jboss started ;)<br><br>In reply to: <a href="https://x.com/rruss/status/5883849439" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">5883849439</span>',
    likes: 0,
    retweets: 0,
    tags: ['arquillian']
  },
  {
    id: '5884849911',
    created: 1258708996000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Hans mentioned your name! I completely forgot that you two had been chatting. He was definitely excited to have you as a user.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/5880554263" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">5880554263</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '5875635439',
    created: 1258680355000,
    type: 'post',
    text: 'How to start learning Java EE 6 by Gavin King: <a href="https://in.relation.to/2009/11/17/how-to-start-learning-java-ee-6/" rel="noopener noreferrer" target="_blank">in.relation.to/2009/11/17/how-to-start-learning-java-ee-6/</a> #jsf #weld #wicket',
    likes: 0,
    retweets: 3,
    tags: ['jsf', 'weld', 'wicket']
  },
  {
    id: '5875588069',
    created: 1258680246000,
    type: 'post',
    text: 'No question, #devoxx 09 has been the most taxing conference ever for me. 5 days down, one to go - #seam + #infinispan community day.',
    likes: 0,
    retweets: 0,
    tags: ['devoxx', 'seam', 'infinispan']
  },
  {
    id: '5875553323',
    created: 1258680165000,
    type: 'post',
    text: 'I think we\'ve fitted Pete Muir with a good use case for using Twitter. But trust me, you\'d rather not know.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5875520894',
    created: 1258680090000,
    type: 'post',
    text: 'Pete Muir did a great job giving his "quickie" talk on #shrinkwrap and #arquillian. I played the roll of assistant to demo the code.',
    likes: 0,
    retweets: 1,
    tags: ['shrinkwrap', 'arquillian']
  },
  {
    id: '5875453618',
    created: 1258679935000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> We can compare puddles. Whenever I use the shower, I end up with a lake on the floor that I have to mop up.<br><br>In reply to: <a href="https://x.com/rruss/status/5820721543" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">5820721543</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5875407701',
    created: 1258679828000,
    type: 'post',
    text: 'Saw a great presentation on #android by Romain Guy today. He explained screen resolution independence and a new C bridge (JNI simplified)',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '5875379198',
    created: 1258679763000,
    type: 'post',
    text: 'Found out this morning that Hans Dockter of #gradle is in my hotel. Had lots of good chats. He\'s so passionate and interesting to talk to.',
    likes: 0,
    retweets: 0,
    tags: ['gradle']
  },
  {
    id: '5875323151',
    created: 1258679634000,
    type: 'post',
    text: 'I\'m happy to say that I finally have light in my bathroom! Perhaps it is a sign my fate is brightening.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5820555086',
    created: 1258528054000,
    type: 'post',
    text: 'To give you an idea of how busy I\'ve been + what bad luck I\'ve had, my bathroom light doesn\'t work, but I haven\'t been able to deal with it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5781810867',
    created: 1258419421000,
    type: 'post',
    text: 'It never fails that I start conferences in about 5th gear and then have to muster the energy to press on. Time to call it a night. #devoxx',
    likes: 0,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '5781755839',
    created: 1258419296000,
    type: 'post',
    text: 'Pete Muir did an overview of #cdi at the #jsf2 and beyond preso in under 10 minutes flat (or your money back). #devoxx',
    likes: 0,
    retweets: 0,
    tags: ['cdi', 'jsf2', 'devoxx']
  },
  {
    id: '5781735878',
    created: 1258419251000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> did an awesome job covering the UI part of #jsf2. I\'m amazed how empowered I felt listening about the new features.',
    likes: 0,
    retweets: 0,
    tags: ['jsf2']
  },
  {
    id: '5780822166',
    created: 1258417182000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/apemberton" rel="noopener noreferrer" target="_blank">@apemberton</a> The ideas are coming fast and furious.<br><br>In reply to: <a href="https://x.com/apemberton/status/5780771195" rel="noopener noreferrer" target="_blank">@apemberton</a> <span class="status">5780771195</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5780804088',
    created: 1258417141000,
    type: 'post',
    text: 'Our #jsf2 and beyond talk almost went well beyond the time slot. We just burned through the 3 hours. Action packed. #devoxx',
    likes: 1,
    retweets: 0,
    tags: ['jsf2', 'devoxx']
  },
  {
    id: '5780700142',
    created: 1258416914000,
    type: 'post',
    text: 'JSF 2 link craze (con\'t): <a href="http://www.ibm.com/developerworks/views/java/libraryview.jsp?search_by=JSF+2+fu" rel="noopener noreferrer" target="_blank">www.ibm.com/developerworks/views/java/libraryview.jsp?search_by=JSF+2+fu</a> <a href="http://www.java.net/blogs/driscoll/" rel="noopener noreferrer" target="_blank">www.java.net/blogs/driscoll/</a> <a href="http://blogs.sun.com/rlubke/" rel="noopener noreferrer" target="_blank">blogs.sun.com/rlubke/</a> (all links from talk)',
    likes: 0,
    retweets: 0
  },
  {
    id: '5780673583',
    created: 1258416854000,
    type: 'post',
    text: 'JSF 2 link craze. <a href="https://javaserverfaces.dev.java.net" rel="noopener noreferrer" target="_blank">javaserverfaces.dev.java.net</a> <a href="https://github.com/javaee/javaserverfaces-spec/issues" rel="noopener noreferrer" target="_blank">github.com/javaee/javaserverfaces-spec/issues</a> <a href="http://andyschwartz.wordpress.com/2009/07/31/whats-new-in-jsf-2" rel="noopener noreferrer" target="_blank">andyschwartz.wordpress.com/2009/07/31/whats-new-in-jsf-2</a> <a href="https://dzone.com/articles/bookmarkability-jsf-2" rel="noopener noreferrer" target="_blank">dzone.com/articles/bookmarkability-jsf-2</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5780635881',
    created: 1258416768000,
    type: 'post',
    text: 'The conversation has been started: #jsf2next Let us know what you think about JSF 2. Or, post on the forum: <a href="http://wiki.jcp.org/boards/index.php?t=2744" rel="noopener noreferrer" target="_blank">wiki.jcp.org/boards/index.php?t=2744</a>',
    likes: 0,
    retweets: 0,
    tags: ['jsf2next']
  },
  {
    id: '5780568362',
    created: 1258416619000,
    type: 'post',
    text: 'Today was a ridiculously long day. But a very good one at that. I\'m just about ready to crash so that I can get up and do it again :)',
    likes: 0,
    retweets: 0
  },
  {
    id: '5760839181',
    created: 1258362210000,
    type: 'post',
    text: 'If you remember my last brick phone couldn\'t even show me the time in Europe. Glad to see phone and OS separated w/ Android.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5760833195',
    created: 1258362182000,
    type: 'post',
    text: 'My time spent Saturday to get my phone "Europe-ready" was for naught. It\'s a brick (actually, a pretty useful brick, but still a brick).',
    likes: 0,
    retweets: 0
  },
  {
    id: '5760810757',
    created: 1258362080000,
    type: 'post',
    text: 'Glad to be back and Europe to have my ceremonial cheese and bread for breakfast. Much better than it sounds. Mmmm.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5760494435',
    created: 1258360537000,
    type: 'post',
    text: 'Had a nice long dinner, discussion and talk prep with Andy, Pete last night, with Shane and Rodney joining us. On the menu: JSF 2 &amp; JCP.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5760454915',
    created: 1258360353000,
    type: 'post',
    text: 'My plane to Amsterdam must have landed in a different county than the terminal. I was on a bus before I even got off the plane!',
    likes: 0,
    retweets: 0
  },
  {
    id: '5750183090',
    created: 1258329915000,
    type: 'post',
    text: 'Here\'s the big announcement. Tweet your ideas, suggestions or clarification requests to #jsf2next during tomorrow\'s JSF uni talk #devoxx',
    likes: 0,
    retweets: 0,
    tags: ['jsf2next', 'devoxx']
  },
  {
    id: '5746783804',
    created: 1258321751000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rayploski" rel="noopener noreferrer" target="_blank">@rayploski</a> Send \'em over. I\'ve still got slides to do, I just wanted the experience of hacking together my own idea in RESTeasy+Seam.<br><br>In reply to: <a href="https://x.com/rayploski/status/5723234146" rel="noopener noreferrer" target="_blank">@rayploski</a> <span class="status">5723234146</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5739784052',
    created: 1258303497000,
    type: 'post',
    text: '19 hours of travel. Not what I had planned. DC -&gt; London -&gt; Amsterdam -&gt; Rotterdam -&gt; Antwerp. Many planes &amp; trains later. Finally! #devoxx',
    likes: 0,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '5721741063',
    created: 1258242012000,
    type: 'post',
    text: 'Mobile lounges at IAD finally reach EOL. The future is here.',
    photos: ['<div class="entry"><img class="photo" src="media/5721741063.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '5717061557',
    created: 1258229256000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> Awesome! I look forward to seeing you at #devoxx. I\'ve got some tricks up my sleeve for the preso.<br><br>In reply to: <a href="https://x.com/aschwart/status/5712130455" rel="noopener noreferrer" target="_blank">@aschwart</a> <span class="status">5712130455</span>',
    likes: 0,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '5717039880',
    created: 1258229196000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Stephan007" rel="noopener noreferrer" target="_blank">@Stephan007</a> I\'ll jump in on the Java EE 6 BOF too. I\'ve definitely got a thing or two to say about it (positive).<br><br>In reply to: <a href="https://x.com/Stephan007/status/5714545448" rel="noopener noreferrer" target="_blank">@Stephan007</a> <span class="status">5714545448</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5717010162',
    created: 1258229115000,
    type: 'post',
    text: 'Getting packed up ready to ship out to #devoxx. I sure wish I had an extra day or three, but ready or not, here we come!',
    likes: 0,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '5694910535',
    created: 1258155857000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> That made me nuts too. And my wife.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/5690812231" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">5690812231</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5694622648',
    created: 1258155155000,
    type: 'post',
    text: 'First cut of Seam RESTeasy demo is ready for JSF Summit talk. <a href="http://code.google.com/p/seaminaction/source/browse/#svn/demos/presentations/jsfsummit09" rel="noopener noreferrer" target="_blank">code.google.com/p/seaminaction/source/browse/#svn/demos/presentations/jsfsummit09</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5689908562',
    created: 1258144096000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> We appreciate every contribution, big or small. It\'s the participation and attention that counts ;)<br><br>In reply to: <a href="https://x.com/lightguardjp/status/5689459984" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">5689459984</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5686955640',
    created: 1258137116000,
    type: 'post',
    text: 'Hey JBoss AS team. Could you please name the download jboss-as-x.xx instead of jboss-x.xx? After all, that\'s the project name.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5686896669',
    created: 1258136982000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SKohler" rel="noopener noreferrer" target="_blank">@SKohler</a> Templates pretty much just don\'t work. And if it can find a way to screw up bullets or layout, it will. And Keynote barfs on it.<br><br>In reply to: @skohler <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5686864909',
    created: 1258136908000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/michaelg" rel="noopener noreferrer" target="_blank">@michaelg</a> Thanks for the pointer. I might have to try that.<br><br>In reply to: <a href="https://x.com/michaelg/status/5686273622" rel="noopener noreferrer" target="_blank">@michaelg</a> <span class="status">5686273622</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5686197586',
    created: 1258135361000,
    type: 'post',
    text: 'I hate that Linux gets beat up \'cause the presentation software sucks. The sad truth is, only Apple seems to get it right and it ain\'t open.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5683500709',
    created: 1258129314000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Thanks! Actually, that\'s my first invite. I\'m finally in the door!<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5669484338',
    created: 1258082622000,
    type: 'post',
    text: 'The design team is one of the most important groups @ JBoss. Doesn\'t matter how good software is if no one thinks it is! Appearance matters.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5669429005',
    created: 1258082482000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> Thanks! My fingers are tied for the whole platform. It\'s an exciting when things come together like this.<br><br>In reply to: @pelegri <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5664151133',
    created: 1258070281000,
    type: 'post',
    text: 'Weld must be popular. JBoss.org is choking. On the other hand, given the track record of our servers lately, it\'s probably something else.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5663675980',
    created: 1258069236000,
    type: 'post',
    text: 'Some Maven 2 archetypes for Weld 1.0 are in the works. Stay tuned!',
    likes: 0,
    retweets: 0
  },
  {
    id: '5663658466',
    created: 1258069199000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Another excellent point? Care to make some JIRAs?<br><br>In reply to: <a href="https://x.com/lightguardjp/status/5662745958" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">5662745958</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5663640087',
    created: 1258069158000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> Good point. The JSR-299 RI isn\'t officially "final" until the JCP approves the spec. It\'s our submitted candidate ;)<br><br>In reply to: @pelegri <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5662259191',
    created: 1258066059000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Eventually it will be on java.sun.com. Just not there yet. Good point about the lack of docs. I\'ll mention that to Gavin.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/5662105727" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">5662105727</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5662116299',
    created: 1258065731000,
    type: 'post',
    text: 'RI of JSR-299 CDI (Contexts and Dependency Injection): Weld 1.0.0 has been released <a href="https://in.relation.to/2009/11/12/weld-100/" rel="noopener noreferrer" target="_blank">in.relation.to/2009/11/12/weld-100/</a> #weld',
    likes: 0,
    retweets: 0,
    tags: ['weld']
  },
  {
    id: '5662026856',
    created: 1258065528000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> That\'s a great question. <a href="https://anonsvn.jboss.org/repos/weld/api/trunk/cdi/src/main/resources/beans.xsd" rel="noopener noreferrer" target="_blank">anonsvn.jboss.org/repos/weld/api/trunk/cdi/src/main/resources/beans.xsd</a> (beans.xsd)<br><br>In reply to: <a href="https://x.com/lightguardjp/status/5661896092" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">5661896092</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5660721869',
    created: 1258062525000,
    type: 'post',
    text: 'I gave the Weld reference guide a thorough cleansing for 1.0.0. Check it out here: <a href="http://docs.jboss.org/weld/reference/1.0.0/en-US/html/" rel="noopener noreferrer" target="_blank">docs.jboss.org/weld/reference/1.0.0/en-US/html/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5660642793',
    created: 1258062342000,
    type: 'post',
    text: '100 Carats of Weld. Weld 1.0.0 is released! <a href="http://in.relation.to/Bloggers/Weld100" rel="noopener noreferrer" target="_blank">in.relation.to/Bloggers/Weld100</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5659654909',
    created: 1258060022000,
    type: 'post',
    text: 'JSR-299 Draft Submitted, TCK and RI ready to roll, congratulations to Gavin and everyone else who played a part: <a href="https://in.relation.to/2009/11/11/jsr-299-final-draft-submitted/#comment12941" rel="noopener noreferrer" target="_blank">in.relation.to/2009/11/11/jsr-299-final-draft-submitted/#comment12941</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5659630701',
    created: 1258059964000,
    type: 'post',
    text: 'Tomorrow is Friday the 13th. I can only think to myself what Lewis Black might say about someone who believes the day will bring bad luck.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5638547170',
    created: 1257994673000,
    type: 'post',
    text: 'Who the hell uses the term "discussion boards" any more? Apparently the JCP. This is 2009. We call them forums.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5633995560',
    created: 1257984356000,
    type: 'post',
    text: '"If New Zealand wants to be a part of our world, they need to get off their island and push it closer." - Lewis Black',
    likes: 0,
    retweets: 0
  },
  {
    id: '5631043122',
    created: 1257977725000,
    type: 'post',
    text: 'I watched Lewis Black on HBO perform his stand-up routine. Holy crap that guy is funny as hell. My side hurts. He nearly had a stroke.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5630670972',
    created: 1257976858000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> Have you figured out how to "close" the e-mail door?<br><br>In reply to: <a href="https://x.com/tech4j/status/5622175546" rel="noopener noreferrer" target="_blank">@tech4j</a> <span class="status">5622175546</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5630529763',
    created: 1257976531000,
    type: 'post',
    text: 'Get a $200 discount off JSF Summit from Exadel with this code: exadel200jsf',
    likes: 0,
    retweets: 0
  },
  {
    id: '5593240875',
    created: 1257872053000,
    type: 'post',
    text: 'Btw, is it just me or is the text on jcp.org impossible to see? Trust me, it ain\'t my vision.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5593186946',
    created: 1257871921000,
    type: 'post',
    text: 'At the last JCP EC meeting, they discussed "How can we encourage more member participation?" Hmm, how about ask the community directly?',
    likes: 0,
    retweets: 0
  },
  {
    id: '5592564327',
    created: 1257870425000,
    type: 'post',
    text: 'Note from bank: "The Euro are in the mail". I\'ll quickly turn that into chocolate and beer as soon as I get to #devoxx.',
    likes: 0,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '5592010334',
    created: 1257869096000,
    type: 'post',
    text: 'Vishal cites the main problem as the JCP being dominated by Sun. I think the main problem is that deliberations happen in private. No "C".',
    likes: 0,
    retweets: 0
  },
  {
    id: '5591964326',
    created: 1257868985000,
    type: 'post',
    text: 'Here is the call by SAP CTO Vishal Sikka to #freejava by turning the JCP into an independent foundation. <a href="http://www.sdn.sap.com/irj/scn/weblogs?blog=/pub/wlg/16648" rel="noopener noreferrer" target="_blank">www.sdn.sap.com/irj/scn/weblogs?blog=/pub/wlg/16648</a>',
    likes: 0,
    retweets: 0,
    tags: ['freejava']
  },
  {
    id: '5591791583',
    created: 1257868580000,
    type: 'post',
    text: 'Hey, I think it would be a huge step if we could just get open mailinglists for the JSRs. #freejava #jcp',
    likes: 0,
    retweets: 0,
    tags: ['freejava', 'jcp']
  },
  {
    id: '5591669953',
    created: 1257868284000,
    type: 'post',
    text: 'Oracle said in 2007 "It\'s the sense of the EC that the JCP become an open independent vendor-neutral Standards Organization" +1 #freejava',
    likes: 0,
    retweets: 0,
    tags: ['freejava']
  },
  {
    id: '5591634315',
    created: 1257868199000,
    type: 'post',
    text: 'SAP is calling to open up Java. I\'ve been saying for a while that I\'d like to see the JCP split off as a foundation, somewhat like Eclipse.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5580140462',
    created: 1257827201000,
    type: 'post',
    text: 'Only one week before 2500+ developers from 36 countries will travel and attend #Devoxx.... can\'t wait! Wow, good numbers!',
    likes: 0,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '5580115444',
    created: 1257827133000,
    type: 'post',
    text: 'I\'m fightin\' for the people, trying to get the #jsf2 (JSR-314-OPEN) mailinglist archives fixed. I\'m praying it can happen before #devoxx.',
    likes: 0,
    retweets: 0,
    tags: ['jsf2', 'devoxx']
  },
  {
    id: '5566547870',
    created: 1257794702000,
    type: 'post',
    text: 'Now is your chance to show support for JSF 2! Please reconsider if you are not signed up for JSF Summit: <a href="http://www.jsfsummit.com" rel="noopener noreferrer" target="_blank">www.jsfsummit.com</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5566405839',
    created: 1257794331000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Really, #4? The one that looks like the coming of our savior?<br><br>In reply to: <a href="https://x.com/lightguardjp/status/5551463876" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">5551463876</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5566367284',
    created: 1257794231000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Right, I\'m saying that threads are broken because no correlation id and subjects are different, so it just breaks period.<br><br>In reply to: <a href="https://x.com/maxandersen/status/5553349399" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">5553349399</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5566336800',
    created: 1257794152000,
    type: 'post',
    text: 'Weld 1.0 is coming!',
    likes: 0,
    retweets: 0
  },
  {
    id: '5564242561',
    created: 1257788701000,
    type: 'post',
    text: 'Planning on attending the Seam meetup on Friday night after Devoxx? Sign up here! <a href="http://spreadsheets.google.com/viewform?formkey=dDNYVUNnNG5TSV9TbzZicHZBTjg4S3c6MA" rel="noopener noreferrer" target="_blank">spreadsheets.google.com/viewform?formkey=dDNYVUNnNG5TSV9TbzZicHZBTjg4S3c6MA</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5563869557',
    created: 1257787744000,
    type: 'post',
    text: 'I\'m starting to see my chair (Zody) in almost every tv episode I watch that picture an office...movies too.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5551369547',
    created: 1257742599000,
    type: 'post',
    text: 'Round #2 of Weld icons. <a href="http://www.jboss.org/files/jbosslabs/design/weld/index.htm" rel="noopener noreferrer" target="_blank">www.jboss.org/files/jbosslabs/design/weld/index.htm</a> Let us know what you think.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5550549004',
    created: 1257740073000,
    type: 'post',
    text: 'Btw, one reason the e-mail client can\'t thread JIRA e-mail messages properly is b/c the operation is included in the subject.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5550508561',
    created: 1257739953000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Killer as in "good shit". We still have some ironing to do, but I think the Maven 2 project build for Weld is well separated.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/5549116996" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">5549116996</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5547526474',
    created: 1257732028000,
    type: 'post',
    text: 'Want to see a sick #maven 2 project setup? Check out the #weld source. The result of consulting w/ Sonatype + lots of OCD. <a href="http://sfwk.org" rel="noopener noreferrer" target="_blank">sfwk.org</a>',
    likes: 0,
    retweets: 1,
    tags: ['maven', 'weld']
  },
  {
    id: '5523563238',
    created: 1257652788000,
    type: 'post',
    text: 'Just backed up my twitter timeline manually until I can get a real solution in place. Not that I\'m close to the 3,200 cutoff point yet.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5523219252',
    created: 1257651761000,
    type: 'post',
    text: 'For convenience, I made a URL alias for David Geary\'s JSF 2 series at IBM developerWorks: <a href="http://www.ibm.com/developerworks/views/java/libraryview.jsp?search_by=JSF+2+fu" rel="noopener noreferrer" target="_blank">www.ibm.com/developerworks/views/java/libraryview.jsp?search_by=JSF+2+fu</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5517669402',
    created: 1257635744000,
    type: 'post',
    text: 'It makes me insane when issue trackers don\'t generate proper threaded messages. Both Google Code and JBoss\' JIRA screw it up.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5510373474',
    created: 1257614318000,
    type: 'post',
    text: 'Concise article on TheServerSide about #jsr299 (#cdi) from the perspective of #ejb. <a href="http://www.facebook.com/permalink.php?story_fbid=165861049354&amp;id=8671323790" rel="noopener noreferrer" target="_blank">www.facebook.com/permalink.php?story_fbid=165861049354&amp;id=8671323790</a>',
    likes: 0,
    retweets: 0,
    tags: ['jsr299', 'cdi', 'ejb']
  },
  {
    id: '5499610526',
    created: 1257570348000,
    type: 'post',
    text: 'Sweet, twitter now has retweet in one click. Certainly a time saver, but not something you want to abuse.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5490255668',
    created: 1257545052000,
    type: 'post',
    text: '@mwessendorf could you give me a soundbyte regarding the concern over field of use restriction that Apache raised about JSR-314?',
    likes: 0,
    retweets: 0
  },
  {
    id: '5489382702',
    created: 1257542687000,
    type: 'post',
    text: 'Or I should say, "the Droid walks"',
    likes: 0,
    retweets: 0
  },
  {
    id: '5489340323',
    created: 1257542569000,
    type: 'post',
    text: 'Verizon fans, the Droid has been unleashed: <a href="http://www.google.com/mobile/partners/verizon/search.html" rel="noopener noreferrer" target="_blank">www.google.com/mobile/partners/verizon/search.html</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5487728053',
    created: 1257538334000,
    type: 'post',
    text: 'Getting a lot of views on the JSF 2 DZone series. <a href="https://dzone.com/articles/bookmarkability-jsf-2" rel="noopener noreferrer" target="_blank">dzone.com/articles/bookmarkability-jsf-2</a> 10,000 so far. FTW!',
    likes: 0,
    retweets: 0
  },
  {
    id: '5486537524',
    created: 1257535192000,
    type: 'reply',
    text: '@mwessendorf We need to document these annoyances. Perhaps record them here: <a href="http://wiki.jcp.org/wiki/index.php?structure_id=16" rel="noopener noreferrer" target="_blank">wiki.jcp.org/wiki/index.php?structure_id=16</a><br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5469065733',
    created: 1257477620000,
    type: 'post',
    text: 'I think more new people have joined the JSF 2.0 EG since it went final than at any other range of time (or at least since inception).',
    likes: 0,
    retweets: 0
  },
  {
    id: '5464706351',
    created: 1257466789000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> Yeah, that\'s a real pain. After you type the quote, immediately type Ctrl-Z (undo) and it will put the quote back to normal.<br><br>In reply to: <a href="https://x.com/aschwart/status/5463614679" rel="noopener noreferrer" target="_blank">@aschwart</a> <span class="status">5463614679</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5464676703',
    created: 1257466714000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> E-mail mistakes in JSR-299 to jsr299-comments@jcp.org or just post to weld-dev.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/5464064108" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">5464064108</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5462552673',
    created: 1257461528000,
    type: 'post',
    text: 'Gavin is asking all to proofread JSR-299 spec. It gets submitted for final voting on Nov 11! <a href="http://lists.jboss.org/pipermail/weld-dev/attachments/20091106/3e6818fb/attachment-0001.pdf" rel="noopener noreferrer" target="_blank">lists.jboss.org/pipermail/weld-dev/attachments/20091106/3e6818fb/attachment-0001.pdf</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5462471810',
    created: 1257461331000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Right, if it\'s going to cut the link short, then it should at least create an alias...not just break the link (visually).<br><br>In reply to: <a href="https://x.com/lightguardjp/status/5461168599" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">5461168599</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5461100521',
    created: 1257458116000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Let\'s roll that matrix into JSF central.<br><br>In reply to: <a href="https://x.com/kito99/status/5460187636" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">5460187636</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5461065470',
    created: 1257458039000,
    type: 'post',
    text: 'Why the hell does twitter cut off links when they fit within 140 characters!?!? Damn you twitter. Stop being so stingy!',
    likes: 0,
    retweets: 0
  },
  {
    id: '5461035989',
    created: 1257457976000,
    type: 'post',
    text: 'Weld Maven 2 archetypes are in the works: <a href="http://sfwk.org/Community/WeldMaven2ArchetypeProposal" rel="noopener noreferrer" target="_blank">sfwk.org/Community/WeldMaven2ArchetypeProposal</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5459181772',
    created: 1257453524000,
    type: 'post',
    text: 'JCP EC election results final. Tim Peierls got open seat. <a href="http://jcp.org/en/whatsnew/elections" rel="noopener noreferrer" target="_blank">jcp.org/en/whatsnew/elections</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5457013709',
    created: 1257447895000,
    type: 'post',
    text: 'The previous tweet about the free JSF Summit pass raffle was borked. Use this one instead: <a href="http://www.jsfcentral.com/jsfsummit_raffle.html" rel="noopener noreferrer" target="_blank">www.jsfcentral.com/jsfsummit_raffle.html</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5437950157',
    created: 1257386885000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SKohler" rel="noopener noreferrer" target="_blank">@SKohler</a> Yep, just tried to do Ctrl-D to delete a line in my presentation.<br><br>In reply to: @skohler <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5431082207',
    created: 1257370644000,
    type: 'post',
    text: 'You know you\'ve been doing a lot of programming when you right-click in file manager and look for "Refactor..." the rename a file.',
    likes: 1,
    retweets: 0
  },
  {
    id: '5427115184',
    created: 1257360317000,
    type: 'post',
    text: 'With help from the Weld team, Gavin has filled in the JavaDocs for all JSR-299 API classes: <a href="http://lists.jboss.org/pipermail/weld-dev/2009-October/001486.html" rel="noopener noreferrer" target="_blank">lists.jboss.org/pipermail/weld-dev/2009-October/001486.html</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5426195618',
    created: 1257357844000,
    type: 'post',
    text: 'Win free pass + lodging to JSF Summit: <a href="http://www.jsfsummit.com/conference/orlando/2009/12/raffle_entry" rel="noopener noreferrer" target="_blank">www.jsfsummit.com/conference/orlando/2009/12/raffle_entry</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5403280886',
    created: 1257286526000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Got it. Just corrected the Weld ref guide, which was claiming it was required.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/5402917629" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">5402917629</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5402304023',
    created: 1257284073000,
    type: 'post',
    text: 'Ah, @Remove was never required. It was needed by Seam 2 so that it would have a way to destroy the bean explicitly.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5402058501',
    created: 1257283464000,
    type: 'post',
    text: 'Are @Remove methods for stateful session beans still required in EJB 3.1?',
    likes: 0,
    retweets: 0
  },
  {
    id: '5396043098',
    created: 1257267819000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Submit ideas for Weld logo here: <a href="https://jira.jboss.org/jira/browse/DESIGN-47" rel="noopener noreferrer" target="_blank">jira.jboss.org/jira/browse/DESIGN-47</a> So far, #5 is looking good.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/5395446810" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">5395446810</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5396019588',
    created: 1257267760000,
    type: 'post',
    text: 'While working on an app for Android, I discovered a low-level bug in the Dalvik VM: <a href="http://code.google.com/p/android/issues/detail?id=4237" rel="noopener noreferrer" target="_blank">code.google.com/p/android/issues/detail?id=4237</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5395383369',
    created: 1257266184000,
    type: 'post',
    text: 'Contributions, ideas and opinions for the Weld logo are welcome.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5395370962',
    created: 1257266153000,
    type: 'post',
    text: 'Want to participate in choosing a logo for Weld? <a href="https://jira.jboss.org/jira/browse/DESIGN-47" rel="noopener noreferrer" target="_blank">jira.jboss.org/jira/browse/DESIGN-47</a> <a href="http://www.jboss.org/files/jbosslabs/design/weld/index.htm" rel="noopener noreferrer" target="_blank">www.jboss.org/files/jbosslabs/design/weld/index.htm</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5395055699',
    created: 1257265379000,
    type: 'post',
    text: 'Old man are allowed to say anything: <a href="https://x.com/shitmydadsays" rel="noopener noreferrer" target="_blank">x.com/shitmydadsays</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5376486100',
    created: 1257205033000,
    type: 'post',
    text: 'The third article in the JSF 2.0 series at DZone, on navigation enhancements, has been published. Follow <a href="https://dzone.com/articles/bookmarkability-jsf-2" rel="noopener noreferrer" target="_blank">dzone.com/articles/bookmarkability-jsf-2</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5375727731',
    created: 1257203168000,
    type: 'post',
    text: '"While there is VPN support in later versions of the Android OS, it is still not yet compatible with Red Hat\'s VPN" damn :(',
    likes: 0,
    retweets: 0
  },
  {
    id: '5375476492',
    created: 1257202555000,
    type: 'post',
    text: 'Attempt to connect to VPN with Android #fail. Maybe I\'m just dumb, but VPN is like voodoo magic to me. Too many damn fields and configs.',
    likes: 0,
    retweets: 0,
    tags: ['fail']
  },
  {
    id: '5373584492',
    created: 1257197782000,
    type: 'post',
    text: 'I spent last week rewriting the Weld reference guide. It\'s now ready for community review as part of upcoming Weld CR2 release.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5371449374',
    created: 1257192151000,
    type: 'post',
    text: 'All external dependencies for Weld listed here: <a href="https://jira.jboss.org/jira/browse/WELD-222" rel="noopener noreferrer" target="_blank">jira.jboss.org/jira/browse/WELD-222</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5369080513',
    created: 1257185686000,
    type: 'post',
    text: 'One really nice thing about Android, though, is that factory reset is very easy.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5369052408',
    created: 1257185607000,
    type: 'post',
    text: 'Android apps shouldn\'t write data to primary HD without sync option to \'net or SD card. Android can\'t be backed manually (w/o root).',
    likes: 0,
    retweets: 0
  },
  {
    id: '5367486622',
    created: 1257181394000,
    type: 'post',
    text: 'I\'m looking forward to tonight for a rare opportunity to see New Orleans play, at home no less. #nfl',
    likes: 0,
    retweets: 0,
    tags: ['nfl']
  },
  {
    id: '5349434018',
    created: 1257119108000,
    type: 'post',
    text: 'Having my phone data wiped made me appreciate the value of being primarily web-based (email, calendar, contacts) and what isn\'t...Ouch.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5349351114',
    created: 1257118868000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> That\'s the first problem I have had with my Android G1 phone in 6 months. I hate to complain, could have been something I did.<br><br>In reply to: @pelegri <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5329743848',
    created: 1257045685000,
    type: 'post',
    text: 'My phone got possessed on Halloween. Kept restarting endlessly. Finally had to do factory reset. Some application was freaking out.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5322491385',
    created: 1257022682000,
    type: 'post',
    text: 'If time was running out, and someone had to plug a USB cable into a mini-USB slot to save us, we would all surely die.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5294840167',
    created: 1256928981000,
    type: 'post',
    text: 'To give you an example of how explosive Kentucky Thunder is, give this a listen: <a href="http://popup.lala.com/popup/432627069324984031" rel="noopener noreferrer" target="_blank">popup.lala.com/popup/432627069324984031</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5278208160',
    created: 1256872876000,
    type: 'post',
    text: 'Ricky Skaggs and Kentucky Thunder at Strathmore Hall',
    photos: ['<div class="entry"><img class="photo" src="media/5278208160.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '5278163120',
    created: 1256872751000,
    type: 'post',
    text: 'Ricky Skaggs and Kentucky Thunder are phenomenal! Saw them tonight at #strathmore. Still trying to locate my jaw. Wow!',
    likes: 0,
    retweets: 0,
    tags: ['strathmore']
  },
  {
    id: '5267253625',
    created: 1256845307000,
    type: 'post',
    text: 'CDI (JSR-299) and Weld 1.0 are almost a reality! <em>&lt;expired link&gt;</em>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5265027509',
    created: 1256839536000,
    type: 'post',
    text: 'These articles are coming out faster than I expected. There will be a burst, then a break, then another burst. One more on its way.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5265009556',
    created: 1256839490000,
    type: 'post',
    text: 'The second article in the JSF 2.0 series at DZone, on bookmarkable URLs, has been published. Follow <a href="https://dzone.com/articles/bookmarkability-jsf-2" rel="noopener noreferrer" target="_blank">dzone.com/articles/bookmarkability-jsf-2</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5264343825',
    created: 1256837758000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/edburns" rel="noopener noreferrer" target="_blank">@edburns</a> I\'m not interested in sizing up participants. I just want fluent and open communication in the JCP. Simple as that.<br><br>In reply to: <a href="https://x.com/edburns/status/5262471423" rel="noopener noreferrer" target="_blank">@edburns</a> <span class="status">5262471423</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5264245083',
    created: 1256837500000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> It\'s Linux. Who\'s counting distro d/ls?<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/5260510141" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">5260510141</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5264223266',
    created: 1256837444000,
    type: 'post',
    text: 'Firefox is really irritating me, or maybe it\'s Flash to blame. After a couple of days, I have to restart because it\'s craaaaaaaaawling.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5260633074',
    created: 1256828484000,
    type: 'post',
    text: 'Another shout-out to my cuz, who just joined my followership on Twitter. Look forward to seeing you 2nite at the Skaggs concert.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5260382828',
    created: 1256827865000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> I don\'t know, you\'d be surprised how many people in the US on twitter are not even interested in technology.<br><br>In reply to: <a href="https://x.com/maxandersen/status/5260343720" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">5260343720</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5260270830',
    created: 1256827580000,
    type: 'post',
    text: 'Of course, it should be Ubuntu 9.10, which was just released, but we\'ll take what we can get ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '5260253921',
    created: 1256827539000,
    type: 'post',
    text: 'Wow, Ubuntu 9 is trending in twitter. I don\'t see that new M$ operating system trending. That really says something.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5260141843',
    created: 1256827255000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Russian exports then ;)<br><br>In reply to: <a href="https://x.com/lightguardjp/status/5260108111" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">5260108111</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5260049067',
    created: 1256827018000,
    type: 'post',
    text: '"It was 40 yrs ago today: ARPANET transmitted first message between computers." It was an ad for viagra.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5248130034',
    created: 1256782358000,
    type: 'post',
    text: 'A shout-out to my sis, who is now following me on Twitter. Go fam!',
    likes: 0,
    retweets: 0
  },
  {
    id: '5243577429',
    created: 1256771661000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pelegri" rel="noopener noreferrer" target="_blank">@pelegri</a> Absolutely. Now we just have to convince the community of that with some action ;)<br><br>In reply to: @pelegri <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5241260559',
    created: 1256765959000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/edburns" rel="noopener noreferrer" target="_blank">@edburns</a> Yep, a lot of people have the understanding that Seam just morphed into JSR-299. But Seam had a lot of JSF 2 bits as well.<br><br>In reply to: <a href="https://x.com/edburns/status/5241218174" rel="noopener noreferrer" target="_blank">@edburns</a> <span class="status">5241218174</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5240248361',
    created: 1256763392000,
    type: 'post',
    text: 'Although JSF 2 has been a long time coming, it has a lot to show for it\'s time spent on the drafting table.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5240226773',
    created: 1256763337000,
    type: 'post',
    text: 'Given how important the Java EE platform is to businesses, it\'s crucial that consumers be allowed to participate in how [it] is defined.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5240185493',
    created: 1256763231000,
    type: 'post',
    text: 'Here\'s our call for openness in the JCP: <a href="http://java.dzone.com/articles/jsf-2-seam-standardization?page=0,1#m55h" rel="noopener noreferrer" target="_blank">java.dzone.com/articles/jsf-2-seam-standardization?page=0,1#m55h</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5240124165',
    created: 1256763069000,
    type: 'post',
    text: 'I also broadcast our vow to uncork the JCP - to make it open to the community...really, truly open.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5240074084',
    created: 1256762933000,
    type: 'post',
    text: 'The intro of our JSF 2 DZone series seeks to set the record straight that ideas from Seam went into JSF 2, in addition to JSR-299.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5239677192',
    created: 1256761867000,
    type: 'post',
    text: 'That\'s pretty cool. Had GlassFish V3 running and it just notified me of available update via the system tray. One click and I\'m upgraded.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5235791882',
    created: 1256751357000,
    type: 'post',
    text: '"JSF 2: Seam\'s other avenue to standardization" published on DZone <a href="http://java.dzone.com/articles/jsf-2-seam-standardization" rel="noopener noreferrer" target="_blank">java.dzone.com/articles/jsf-2-seam-standardization</a> RichFaces a key player as well.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5223200353',
    created: 1256707264000,
    type: 'post',
    text: 'Got some #jsf2 articles coming your way. A 7 (or 8) part series on DZone. Parts 1 - 3 are written, the rest in the works. Stay tuned!',
    likes: 0,
    retweets: 0,
    tags: ['jsf2']
  },
  {
    id: '5223154263',
    created: 1256707079000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> That\'s a bug, I\'m pretty sure. I noticed that too. In fact, I was going to e-mail Gavin but couldn\'t remember where it was.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/5220848597" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">5220848597</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5223125978',
    created: 1256706964000,
    type: 'post',
    text: 'Redskins #fail.',
    likes: 0,
    retweets: 0,
    tags: ['fail']
  },
  {
    id: '5216737704',
    created: 1256690682000,
    type: 'post',
    text: 'An iPhone user wondering what life would be like with the #android Donut release all her friends are talking about.',
    photos: ['<div class="entry"><img class="photo" src="media/5216737704.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '5210782104',
    created: 1256677145000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> We need to get you hooked on Seam security. The possibilities are far greater, I believe. Not that I\'m biased or anything :)<br><br>In reply to: <a href="https://x.com/lincolnthree/status/5210679991" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">5210679991</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5164977301',
    created: 1256529795000,
    type: 'reply',
    text: '@mwessendorf You aren\'t the first to be concerned. PERF tests still need to be done. The impl can be optimized, but it might not be yet.<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5130487008',
    created: 1256415711000,
    type: 'post',
    text: 'Isn\'t it strange that you hear fire engines when it\'s pouring rain? I\'m sure there\'s a logical reason, but the mind can\'t make sense of it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5129818140',
    created: 1256413712000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> Haha, the comedian was saying that he choose the side away from the door so that the boogie man could get his wife, not him.<br><br>In reply to: <a href="https://x.com/metacosm/status/5129430742" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">5129430742</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5129785626',
    created: 1256413615000,
    type: 'post',
    text: 'I\'ve been listening to DJ Markski the last few days. Without a doubt, my favorite (and probably least well know) DJ. Ski Mix baby!',
    likes: 0,
    retweets: 0
  },
  {
    id: '5129768778',
    created: 1256413566000,
    type: 'post',
    text: 'Paul Rudd is absolutely brilliant in "I Can Never Be Your Woman". New favorite actor. Michelle Pfeiffer was also very good in it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5129024910',
    created: 1256411343000,
    type: 'post',
    text: 'It\'s so bizarre that my wife and I subconsciously swap the side of the bed we choose away from home. A stand-up act made me realize it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5079555490',
    created: 1256246075000,
    type: 'post',
    text: 'Hotel booked for Devoxx. I\'ll be at the Astoria Hotel. Good reviews, wireless and near Antwerp-Central tram station. Sweet.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5078322611',
    created: 1256242744000,
    type: 'post',
    text: 'I\'ve got so much going on I\'m almost reaching the point of freezing. Some things are going to drop on the floor as I press through it all.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5062309686',
    created: 1256187713000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Great news. If you have any questions, comments or suggestions, please speak up. Also, any secrets I can put in the refdoc.<br><br>In reply to: <a href="https://x.com/jasondlee/status/5061904227" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">5061904227</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5060468242',
    created: 1256181647000,
    type: 'post',
    text: 'The Weld release made TheServerSide: <a href="http://www.theserverside.com/news/thread.tss?thread_id=58251" rel="noopener noreferrer" target="_blank">www.theserverside.com/news/thread.tss?thread_id=58251</a> Well received it seems.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5058919576',
    created: 1256177392000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> People have complained, and Apache says they aren\'t changing the name. They get to keep OpenWebBeans. Fine with me.<br><br>In reply to: <a href="https://x.com/JohnAment/status/5054873689" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">5054873689</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5058901056',
    created: 1256177342000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> Ask <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a>. I could tell you, but he could give you specifics. I slipped into JCP under Red Hat.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/5053372752" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">5053372752</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5058764487',
    created: 1256176985000,
    type: 'post',
    text: 'Crap, hotels in Antwerpen have booked up fast because of Devoxx. Do me a favor. Don\'t book until after tomorrow ;) I need your room.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5053570576',
    created: 1256162570000,
    type: 'post',
    text: 'One of first Weld tutorials from the community available: <a href="http://info.rmatics.org/2009/10/17/jsf2_tutorial_0/" rel="noopener noreferrer" target="_blank">info.rmatics.org/2009/10/17/jsf2_tutorial_0/</a> #weld #maven #jsf2',
    likes: 0,
    retweets: 0,
    tags: ['weld', 'maven', 'jsf2']
  },
  {
    id: '5053304389',
    created: 1256161828000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> You should become an individual JCP member. It requires some red tape, but <a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> made it through, so you can too.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/5053013926" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">5053013926</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5053266825',
    created: 1256161724000,
    type: 'post',
    text: 'Another clarification. Weld and OpenWebBeans are both JSR-299 implementations released under the Apache 2.0 License.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5052964618',
    created: 1256160861000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> Supposedly JCP members get an e-mail from the JCP. I didn\'t say it was easy :) I\'m just figuring this JCP thing out myself.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/5051482584" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">5051482584</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5052951162',
    created: 1256160823000,
    type: 'post',
    text: 'Tweets are now in Google search results. The jury is still out on how well it will work as a search result <a href="http://googleblog.blogspot.com/2009/10/rt-google-tweets-and-updates-and-search.html" rel="noopener noreferrer" target="_blank">googleblog.blogspot.com/2009/10/rt-google-tweets-and-updates-and-search.html</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5051278675',
    created: 1256156093000,
    type: 'post',
    text: 'I still haven\'t setup VPN on my phone. When I want to make sure things are setup just right, I tend to defer the task until I can focus.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5051194682',
    created: 1256155843000,
    type: 'post',
    text: 'You could certainly vote for Liferay and Terracotta too, but I think we need more individuals in there from the open source community.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5051163128',
    created: 1256155752000,
    type: 'post',
    text: 'If you are a JCP member, vote for Matthew McCullough to fill an open seat on the JCP. It\'s a vote for openness.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5050055170',
    created: 1256152351000,
    type: 'post',
    text: 'I really hate that song by the Black Eyed Peas "I\'ve got a feeling" It is such a tired and monotonous song. Blah!',
    likes: 0,
    retweets: 0
  },
  {
    id: '5050043098',
    created: 1256152314000,
    type: 'post',
    text: 'I\'m having a tough time locating a decisive statement about whether Gracelets supports JSF 2.0. Anyone know?',
    likes: 0,
    retweets: 0
  },
  {
    id: '5030242572',
    created: 1256080785000,
    type: 'post',
    text: 'I\'d like to see a tutorial, or perhaps I\'ll write one, on how to built just the Music or Browser application.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5030225675',
    created: 1256080739000,
    type: 'post',
    text: 'But Android seems really difficult to build. For instance, it\'s not easy to build individual modules, such as the Music application.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5030187585',
    created: 1256080638000,
    type: 'post',
    text: 'I\'m happy to say that I\'ve actually gotten involved in Android, so far just submitting bug reports.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5029934621',
    created: 1256079957000,
    type: 'post',
    text: 'To clarify: CDI == official acronym of JSR-299 (Contexts and Dependency Injection for Java EE). Weld == CDI reference implementation.',
    likes: 0,
    retweets: 0
  },
  {
    id: '5025701286',
    created: 1256067513000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> That was just a sample picture to show that they can even look tacky at the beach ;)<br><br>In reply to: <a href="https://x.com/metacosm/status/5025227117" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">5025227117</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5025694209',
    created: 1256067489000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Working on it right now.<br><br>In reply to: <a href="https://x.com/jasondlee/status/5021695249" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">5021695249</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5025686549',
    created: 1256067465000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rayploski" rel="noopener noreferrer" target="_blank">@rayploski</a> Sorry I couldn\'t catch up with you. It was a short and sweet visit. We made it to Rosebud for steaks on Sunday. Huge portions!<br><br>In reply to: <a href="https://x.com/rayploski/status/4982487645" rel="noopener noreferrer" target="_blank">@rayploski</a> <span class="status">4982487645</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '5023694486',
    created: 1256061170000,
    type: 'post',
    text: 'Why do wedding receptions always have to be accented by those horribly tacky golden chairs? You know the ones:',
    photos: ['<div class="entry"><img class="photo" src="media/5023694486.gif"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '4981658257',
    created: 1255920925000,
    type: 'post',
    text: 'Outside the famous Chicago theater all lit up.',
    photos: ['<div class="entry"><img class="photo" src="media/4981658257.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '4981481725',
    created: 1255920414000,
    type: 'post',
    text: 'All I have to say about Chicago is little red cakes. mmmm.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4971637135',
    created: 1255892301000,
    type: 'post',
    text: 'Nespresso at the coffee bar. I\'m in heaven. Now we just need to get some St. Bernardus.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4970106159',
    created: 1255887773000,
    type: 'post',
    text: 'Justin and Brooke in their first dance as a married couple!',
    photos: ['<div class="entry"><img class="photo" src="media/4970106159.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '4948046247',
    created: 1255804715000,
    type: 'post',
    text: 'DZone\'s site on a mobile browser is total #fail. I bet we\'re going to see a trend away from this sloppiness.',
    likes: 0,
    retweets: 0,
    tags: ['fail']
  },
  {
    id: '4947893636',
    created: 1255804272000,
    type: 'post',
    text: 'Just arrived in Chicago for my college roommate\'s wedding this weekend. It\'s at The Ritz. Nice. I can\'t wait.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4927193918',
    created: 1255730115000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> epub is the closest thing we can get to XML for ebooks. It is zipped XHTML. That seems to be all anyone can agree on.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/4927147666" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">4927147666</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4927082146',
    created: 1255729814000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> I think they get it since they\'ve invested in epub. In an ideal world, we\'d all publish to formats that maintain structure.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/4924668075" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">4924668075</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4927040818',
    created: 1255729702000,
    type: 'post',
    text: 'Damn, both TheServerSide and InfoQ have a really slow process for posting news. The whole idea of news is that it is *new*. Sigh.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4924647743',
    created: 1255723595000,
    type: 'post',
    text: 'The problem with PDF on Android is really a fault with PDF. PDF cannot reflow =&gt; can\'t support different screen sizes, making it useless.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4923638906',
    created: 1255720983000,
    type: 'post',
    text: 'PDFs are a real sore spot on Android. There are some converter but they don\'t really address reflow for the narrower screen and are slow.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4922882076',
    created: 1255718923000,
    type: 'post',
    text: 'It\'s Friday, work getting you down? Perfect time to try something new.<br>Weld (formerly Web Beans) is available! <em>&lt;expired link&gt;</em>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4899021038',
    created: 1255642056000,
    type: 'post',
    text: 'Weld 1.0.0.CR1 (JSR-299) is forged! <a href="https://in.relation.to/2009/10/16/weld-100-cr-1-available/" rel="noopener noreferrer" target="_blank">in.relation.to/2009/10/16/weld-100-cr-1-available/</a> If you\'ve been waiting for something stable enough to stand on, step up!',
    likes: 0,
    retweets: 0
  },
  {
    id: '4896732194',
    created: 1255635602000,
    type: 'post',
    text: 'I hate that Android refreshes the web page when you resume the browser app. It\'s uncalled for! There\'s no reason for it!',
    likes: 0,
    retweets: 0
  },
  {
    id: '4896196586',
    created: 1255634261000,
    type: 'post',
    text: 'Wow, Sunday River Ski Resort in Maine is open. That\'s just nuts.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4896055220',
    created: 1255633904000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> Then my previous post stands.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/4895926809" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">4895926809</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4895986216',
    created: 1255633724000,
    type: 'reply',
    text: '@mwessendorf Just wait, we aren\'t as open as we will be. Still need indexable archive and community list.<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4895860340',
    created: 1255633404000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Oops, didn\'t read enough tweets. No promo needed anymore ;)<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4895822647',
    created: 1255633310000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> No Seam just renewed another year on the OSS project promo.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4891900520',
    created: 1255622734000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> The final draft of JSR-299 is ready and the voting is soon to be underway. The spec is bulletproof and the TCK equally so.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/4891381989" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">4891381989</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4891421118',
    created: 1255621555000,
    type: 'post',
    text: 'Just bought two new suits, one I\'ll wear at my college roommate\'s wedding this weekend. Both are very sharp. Can\'t wait to pick them up.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4891326974',
    created: 1255621323000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> I\'m with you.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/4882134208" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">4882134208</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4880139392',
    created: 1255577356000,
    type: 'post',
    text: 'I\'ve got a crap load of writing to do in the next month, esp if you count presentations as writing. Lots of dates with Panera on the books.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4877757878',
    created: 1255570555000,
    type: 'post',
    text: 'Must see Seam stuff at Devoxx 09: <a href="http://in.relation.to/Bloggers/SeamTeamDevoxx" rel="noopener noreferrer" target="_blank">in.relation.to/Bloggers/SeamTeamDevoxx</a> Definitely check out the talk on Arquillian!',
    likes: 0,
    retweets: 0
  },
  {
    id: '4869217145',
    created: 1255547584000,
    type: 'post',
    text: 'I was in the middle of composing an e-mail in Gmail yesterday and my computer shut off (yes, I ignored its whining). E-mail, saved!',
    likes: 0,
    retweets: 0
  },
  {
    id: '4868574756',
    created: 1255545758000,
    type: 'post',
    text: 'The Google Docs export to Word sucks. Why all the empty lines between paragraphs? And can you please export HTML without zipping it?',
    likes: 0,
    retweets: 0
  },
  {
    id: '4868549325',
    created: 1255545681000,
    type: 'post',
    text: 'Hi Google. Why have you no Google Wave invite for me? You hooked me up with Google Voice almost immediately. What must I do to please you?',
    likes: 0,
    retweets: 0
  },
  {
    id: '4868245370',
    created: 1255544757000,
    type: 'post',
    text: 'Kito just announced my full-day workshop on the first day of JSFSummit. I guess I need to do a post now ;) <a href="http://weblogs.java.net/blog/kito75/archive/2009/10/13/jsf-summit-2009-new-workshops-and-sessions" rel="noopener noreferrer" target="_blank">weblogs.java.net/blog/kito75/archive/2009/10/13/jsf-summit-2009-new-workshops-and-sessions</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4866997258',
    created: 1255541279000,
    type: 'post',
    text: 'Saw Wanda Sykes last night doing her stand-up special I\'MA BE ME. Damn that woman is hilarious. I was laughing until my lungs failed.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4822560491',
    created: 1255395255000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> A quick google search reveals the SSID char limit is 32. Can you get a message across in 32 chars? Perhaps a URL.<br><br>In reply to: <a href="https://x.com/metacosm/status/4821080964" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">4821080964</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4822512765',
    created: 1255395135000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> To clarify, what I meant is that you take advantage of the ability to show text on someone else\'s computer and type message in SSID.<br><br>In reply to: <a href="https://x.com/rruss/status/4820687292" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">4820687292</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4820250899',
    created: 1255389273000,
    type: 'post',
    text: 'Growing trend. Advertising via wireless SSIDs. First saw in Vegas: ImperialPalaceIsADump. Near my house: powerofjesus. What is char limit?',
    likes: 0,
    retweets: 0
  },
  {
    id: '4820160561',
    created: 1255389022000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Team_SDN" rel="noopener noreferrer" target="_blank">@Team_SDN</a> I wonder how many downloads it needs to save it. I am pulling for it.<br><br>In reply to: <a href="https://x.com/Team_SDN/status/4820080756" rel="noopener noreferrer" target="_blank">@Team_SDN</a> <span class="status">4820080756</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4820112303',
    created: 1255388886000,
    type: 'reply',
    text: 'Congrats JSR-303 EG! RT <a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a>: Will email the JCP later tonight or tomorrow with JSR-303 final :)<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/4811270265" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">4811270265</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4820001360',
    created: 1255388589000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> No way, not yet. Incremental release with Weld name.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/4816588655" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">4816588655</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4819949168',
    created: 1255388454000,
    type: 'reply',
    text: '@patrickrmcgowan Next up, Maven needs to download the internet.<br><br>In reply to: <a href="https://x.com/prm8686/status/4817364591" rel="noopener noreferrer" target="_blank">@prm8686</a> <span class="status">4817364591</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4816506653',
    created: 1255379009000,
    type: 'post',
    text: 'Wow, it must have been too early in the afternoon when I made that last post. *A release* of Weld is in the works. Ah, now it makes sense.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4814846870',
    created: 1255373858000,
    type: 'post',
    text: 'I realize of Weld is in the works. Stay tuned!',
    likes: 0,
    retweets: 0
  },
  {
    id: '4800773083',
    created: 1255319284000,
    type: 'post',
    text: 'I was really disappointed to find out the jaudiotagger doesn\'t work properly on Android. Put a wrench in my plans for some id3 tag editing.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4800760258',
    created: 1255319246000,
    type: 'post',
    text: 'Gaelic Storm engaged and entertained the crowd in a very rare way. I love their carefree and lighthearted approach to their music.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4794825710',
    created: 1255302165000,
    type: 'post',
    text: 'At Strathmore Hall awaiting start of Gaelic Storm show.',
    photos: ['<div class="entry"><img class="photo" src="media/4794825710.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '4746705831',
    created: 1255127910000,
    type: 'post',
    text: 'Homeland security agent recognized my JBoss shirt, said he installed Red Hat Linux on his PS3. That is the world I envisioned :)',
    likes: 0,
    retweets: 0
  },
  {
    id: '4746675676',
    created: 1255127825000,
    type: 'post',
    text: 'Lots of great conversations about the future of JSF with <a class="mention" href="https://x.com/aschwartz" rel="noopener noreferrer" target="_blank">@aschwartz</a> in preparation for our collaborative talks at #devoxx and #jsfsummit.',
    likes: 0,
    retweets: 0,
    tags: ['devoxx', 'jsfsummit']
  },
  {
    id: '4734467216',
    created: 1255095770000,
    type: 'post',
    text: 'Congrats to Obama on winning the Nobel Peace Prize!',
    likes: 0,
    retweets: 0
  },
  {
    id: '4733526976',
    created: 1255092920000,
    type: 'post',
    text: 'Our waitress last night looked like Ashley from Entourage. I just put my finger on the connection.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4726893357',
    created: 1255062418000,
    type: 'post',
    text: 'My JSF 2 talk at NEJUG went ok, but not stellar. Got late start because Gavin went long, so had to tap into my second wind.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4726755839',
    created: 1255061958000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cwash" rel="noopener noreferrer" target="_blank">@cwash</a> They didn\'t have a recording system and I forgot my camera. How about we come to Richmond JUG?<br><br>In reply to: <a href="https://x.com/cwash/status/4720184776" rel="noopener noreferrer" target="_blank">@cwash</a> <span class="status">4720184776</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4726707653',
    created: 1255061799000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Are the invites viral? If so, hook me up.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4719163746',
    created: 1255041911000,
    type: 'post',
    text: 'I suspect we\'ll find this marquee on the wall of a local Boston area T.G.I. Friday\'s sometime in the near future.',
    photos: ['<div class="entry"><img class="photo" src="media/4719163746.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '4718474919',
    created: 1255040060000,
    type: 'post',
    text: 'The new camera software in Android 1.6 is *much* better than the camera in earlier versions. On screen controls and fast switching.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4718427271',
    created: 1255039927000,
    type: 'post',
    text: 'Gavin speaking about JSR-299 in front of full house at NEJUG.',
    photos: ['<div class="entry"><img class="photo" src="media/4718427271.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '4711468067',
    created: 1255017784000,
    type: 'post',
    text: 'RT: <a class="mention" href="https://x.com/ManningBooks" rel="noopener noreferrer" target="_blank">@ManningBooks</a>: Congratulations to <a class="mention" href="https://x.com/mcqueeney" rel="noopener noreferrer" target="_blank">@mcqueeney</a>, the Manning Pop Quiz Winner! Got 30/30 questions right. Wow. Super geek ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '4711322276',
    created: 1255017224000,
    type: 'post',
    text: 'Feel free to post/edit issues in the WELD project<br><a href="http://jira.jboss.org/jira/browse/WELD" rel="noopener noreferrer" target="_blank">jira.jboss.org/jira/browse/WELD</a>. Issues have been migrated from WBRI project.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4711277724',
    created: 1255017054000,
    type: 'post',
    text: 'Venarc is looking for a Seam dev in Burbank, CA. I know the CTO, Drew Kutcharian, great guy to work for. <a href="http://www.linkedin.com/groupAnswers?viewQuestionAndAnswers=&amp;discussionID=7874959&amp;gid=93682" rel="noopener noreferrer" target="_blank">www.linkedin.com/groupAnswers?viewQuestionAndAnswers=&amp;discussionID=7874959&amp;gid=93682</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4665549203',
    created: 1254865418000,
    type: 'post',
    text: 'Hey New England, we\'re comin\' your way! Come see Gavin\'s talk on JSR-299 and my talk on JSF 2 at NEJUG Thursday: <a href="http://www.nejug.org/events/show/104" rel="noopener noreferrer" target="_blank">www.nejug.org/events/show/104</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4659513761',
    created: 1254848490000,
    type: 'post',
    text: 'I made the contributing to Seam page (<a href="http://sfwk.org/Community/ContributingToSeam)" rel="noopener noreferrer" target="_blank">sfwk.org/Community/ContributingToSeam)</a> easier to follow.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4658301722',
    created: 1254845238000,
    type: 'post',
    text: 'I got my android phone hooked up to Eclipse through USB. Now I can deploy, view files and logs, and debug directly on my phone. Incredible.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4647816517',
    created: 1254803193000,
    type: 'post',
    text: 'The pool at Cornell now has a mounted High Def TV and Tivo playback system for practices. Damn, I wish I had that when I was diving there.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4634335050',
    created: 1254767753000,
    type: 'post',
    text: 'Follow <a href="http://in.relation.to" rel="noopener noreferrer" target="_blank">in.relation.to</a> for updates on the name transition from WebBeans to Weld, as several important resources will be moving!',
    likes: 0,
    retweets: 0
  },
  {
    id: '4634314322',
    created: 1254767699000,
    type: 'post',
    text: 'It\'s official! WebBeans is now Weld. Weld is the open source reference implementation of JSR-299 developed by Red Hat &amp; its community.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4611700379',
    created: 1254690687000,
    type: 'post',
    text: 'I often forget that Dire Straits is one of my all-time favorite bands.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4583916184',
    created: 1254592964000,
    type: 'post',
    text: 'Android 1.6 update just got pushed down to my phone. VPN support FTW!! Only thing I don\'t like is alignment of new search widget.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4562340416',
    created: 1254515212000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/richsharples" rel="noopener noreferrer" target="_blank">@richsharples</a> Muse is breaking out like a champ right now. Almost like modern power ballads.<br><br>In reply to: <a href="https://x.com/richsharples/status/4559168683" rel="noopener noreferrer" target="_blank">@richsharples</a> <span class="status">4559168683</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4562296997',
    created: 1254515093000,
    type: 'post',
    text: 'Wheels by Foo Fighters is Learning to Fly by Tom Petty. At least, that\'s what I keep hearing.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4534603694',
    created: 1254430764000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> I will use RESTeasy when I get to networking. Right now I am doing local stuff. Announcement to come.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/4532688963" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">4532688963</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4534592165',
    created: 1254430731000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emacsen" rel="noopener noreferrer" target="_blank">@emacsen</a> You really don\'t need an Android phone to write apps. You can play w/ app on the emulator. You only need one to enjoy on the go.<br><br>In reply to: <a href="https://x.com/emacsen/status/4532448880" rel="noopener noreferrer" target="_blank">@emacsen</a> <span class="status">4532448880</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4532201693',
    created: 1254424392000,
    type: 'post',
    text: 'I\'m now proficient enough with Android to create simple apps for myself. I\'m working on a really useful app which I\'ll open source soon.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4531729543',
    created: 1254423125000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> I totally dig that hotel, though. I would definitely stay again. Probably because of the candy on the pillow ;)<br><br>In reply to: <a href="https://x.com/matthewmccull/status/4506821747" rel="noopener noreferrer" target="_blank">@matthewmccull</a> <span class="status">4506821747</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4531696683',
    created: 1254423037000,
    type: 'post',
    text: 'When someone watches you use a smartphone and says "wow, that\'s really fast" you know their home computer is in bad shape.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4531202278',
    created: 1254421704000,
    type: 'post',
    text: 'In comparison, the G1 is the most rock solid 1st generation device I have ever seen, had, or used.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4531178180',
    created: 1254421634000,
    type: 'post',
    text: 'Just requested a second RMA (return) for my Pioneer XMP3 player. Seriously, this thing is a poster child for #fail.',
    likes: 0,
    retweets: 0,
    tags: ['fail']
  },
  {
    id: '4510813562',
    created: 1254354605000,
    type: 'post',
    text: 'At the Screw Cancer event for the Ulman Cancer Fund in MD. Inspiring stories.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4503178944',
    created: 1254334011000,
    type: 'post',
    text: 'Listening to the story about the original "tweeter": <a href="http://en.wikipedia.org/wiki/Tweeter_and_the_Monkey_Man" rel="noopener noreferrer" target="_blank">en.wikipedia.org/wiki/Tweeter_and_the_Monkey_Man</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4445669708',
    created: 1254153437000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Which is why I skipped the first 3 weeks of the NFL season. I\'m ready to jump in next week, but still skipping the \'skins game.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4445584835',
    created: 1254153217000,
    type: 'post',
    text: 'Just purchased ebook of Unlocking Android using discount code pop0928. I\'ve got Activities and Intents down, on to ContentProviders.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4411891335',
    created: 1254032370000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> I\'m going to nail some to my wall ;)<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/4409963219" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">4409963219</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4411879887',
    created: 1254032316000,
    type: 'post',
    text: 'I wanted the market for my #android emulator to get a task killer application to test when app is killed. I\'m sure there are other ways.',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '4411861325',
    created: 1254032231000,
    type: 'post',
    text: 'Want to know what it\'s like to have an #android phone? Download the SDK, create&amp;boot a device, add SlideME package to /data/app and play!',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '4411832994',
    created: 1254032107000,
    type: 'post',
    text: 'Disappointed that Android Market isn\'t available in emulator. However, I found an alternative! <a href="http://slideme.org" rel="noopener noreferrer" target="_blank">slideme.org</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4409323684',
    created: 1254022793000,
    type: 'post',
    text: 'I\'m really getting psyched about #android as a dev platform. Finally I have a platform other than web that I look forward to develop on.',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '4409304646',
    created: 1254022731000,
    type: 'post',
    text: 'I was also on stay-cation last week. That\'s also why I haven\'t been twitter-taining you. I won\'t let you down this week ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '4409268753',
    created: 1254022614000,
    type: 'post',
    text: 'As a followup to my last post, I really did have a good reason to tint my windows. I\'m really not that bored. The sun bakes me &gt; noon.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4409246185',
    created: 1254022544000,
    type: 'post',
    text: 'I spent the last two days tinting my office windows. Now my office is pimped out and I\'m ready to cruise. Jealous?',
    likes: 0,
    retweets: 0
  },
  {
    id: '4352859801',
    created: 1253830397000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/apemberton" rel="noopener noreferrer" target="_blank">@apemberton</a> Actually, at that point I just hit 0. Worse is when that leads to "sorry, that is not an option." Grrr<br><br>In reply to: <a href="https://x.com/apemberton/status/4350819103" rel="noopener noreferrer" target="_blank">@apemberton</a> <span class="status">4350819103</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4347472730',
    created: 1253815004000,
    type: 'post',
    text: 'Is it only me, or do you zone out in automated phone systems and then not know which number to press? Damn options are so long winded.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4320224305',
    created: 1253726372000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Awesome. I might need to try RTM again now that it has an Android app. Good my calendar going, but still working on task mgmt.<br><br>In reply to: <a href="https://x.com/kito99/status/4319944941" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">4319944941</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4156556300',
    created: 1253570146000,
    type: 'post',
    text: 'Lately, accepting an autocomplete entry in Firefox has been causing the form to submit. WTF?',
    likes: 0,
    retweets: 0
  },
  {
    id: '4156126834',
    created: 1253568924000,
    type: 'post',
    text: 'Proposed Final Draft (#2) of JSR-299 is available: <a href="https://in.relation.to/2009/09/22/jsr-299-proposed-final-draft-2/" rel="noopener noreferrer" target="_blank">in.relation.to/2009/09/22/jsr-299-proposed-final-draft-2/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4152241344',
    created: 1253557918000,
    type: 'post',
    text: 'Now #devoxx has two speakers that wear big red hats: <a href="http://www.devoxx.com/display/DV09/Speakers" rel="noopener noreferrer" target="_blank">www.devoxx.com/display/DV09/Speakers</a>',
    likes: 0,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '4140014352',
    created: 1253507581000,
    type: 'post',
    text: 'The day also brought sad news. Long time family friend &amp; priest Fr. Twohig died. He traveled from Ireland to oversee many family ceremonies.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4139903035',
    created: 1253507143000,
    type: 'post',
    text: 'Spent beautiful day at VA Wine festival with my wife. Excellent live music, more wines than we could possibly sample (and stay standing).',
    likes: 0,
    retweets: 0
  },
  {
    id: '4127378532',
    created: 1253468020000,
    type: 'post',
    text: 'At the Virginia Wine Festival getting wasted in the sticks.',
    photos: ['<div class="entry"><img class="photo" src="media/4127378532.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '4097644631',
    created: 1253340565000,
    type: 'post',
    text: 'Amazon can speak dirty to you. Preview track #10 from this album: <a href="https://www.amazon.com/Dusk-Till-Music-Motion-Picture/dp/B0013D8CSA/ref?pd_sim_dmusic_2" rel="noopener noreferrer" target="_blank">www.amazon.com/Dusk-Till-Music-Motion-Picture/dp/B0013D8CSA/ref?pd_sim_dmusic_2</a> #dirty #amazon',
    likes: 0,
    retweets: 0,
    tags: ['dirty', 'amazon']
  },
  {
    id: '4089860043',
    created: 1253313666000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> You could make a fortune on a wedding planning SAAS application. A wedding never met a budget it couldn\'t blow.<br><br>In reply to: <a href="https://x.com/kito99/status/4089767250" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">4089767250</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4088358501',
    created: 1253308934000,
    type: 'post',
    text: 'NEJUG event scheduled: <a href="http://www.nejug.org/events/show/104" rel="noopener noreferrer" target="_blank">www.nejug.org/events/show/104</a> Making arrangements now.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4086706807',
    created: 1253304195000,
    type: 'post',
    text: 'I need to commit The Art of Community by Jono Bacon to heart <a href="http://digg.com/linux_unix/The_Art_of_Community_Available_For_Free_Download" rel="noopener noreferrer" target="_blank">digg.com/linux_unix/The_Art_of_Community_Available_For_Free_Download</a> I\'d like to be able to call myself community leader.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4086669726',
    created: 1253304092000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/apemberton" rel="noopener noreferrer" target="_blank">@apemberton</a> Congrats!<br><br>In reply to: <a href="https://x.com/apemberton/status/4086387668" rel="noopener noreferrer" target="_blank">@apemberton</a> <span class="status">4086387668</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4086656512',
    created: 1253304056000,
    type: 'post',
    text: 'Check out this elevator pitch cartoon for metawidget: <a href="http://metawidget.sourceforge.net/elevator.html" rel="noopener noreferrer" target="_blank">metawidget.sourceforge.net/elevator.html</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4085691671',
    created: 1253301246000,
    type: 'post',
    text: 'Just updated my website with the speaking badge for #jsfsummit. It\'s slated to be a power-packed conference. <a href="http://jsfsummit.com" rel="noopener noreferrer" target="_blank">jsfsummit.com</a>',
    likes: 0,
    retweets: 0,
    tags: ['jsfsummit']
  },
  {
    id: '4083965137',
    created: 1253296215000,
    type: 'post',
    text: 'I got *burned* last night so bad. I was ripping CDs and at the end, I realized SoundJuicer was leaving a pause in every 1st song I ripped.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4083936836',
    created: 1253296135000,
    type: 'post',
    text: 'Yeah, my 2,000 post had a reference open source software. I really am hardcore I guess :)',
    likes: 0,
    retweets: 0
  },
  {
    id: '4083924742',
    created: 1253296100000,
    type: 'post',
    text: 'So I hear you guys want books! Sweet. How about books about open source software so you don\'t need those IntelliJ Licenses :) j/k',
    likes: 0,
    retweets: 0
  },
  {
    id: '4083834431',
    created: 1253295847000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SUBZERO66" rel="noopener noreferrer" target="_blank">@SUBZERO66</a> Belgians are nuts about fries...with mayonnaise. Yuk, not my favorite Belgian speciality.<br><br>In reply to: <a href="https://x.com/bmuschko/status/4083806702" rel="noopener noreferrer" target="_blank">@bmuschko</a> <span class="status">4083806702</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4068575029',
    created: 1253237984000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> Well, yes primitive. I\'m just saying as a trend people rely on it as such. I look forward to using the rich text editor in Wave ;)<br><br>In reply to: <a href="https://x.com/dhanji/status/4065444930" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">4065444930</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4068534206',
    created: 1253237866000,
    type: 'post',
    text: 'Besides IntelliJ licenses, what door prizes would you want if you were at a JUG or conference? Just curious.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4068517908',
    created: 1253237819000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> I\'m eagerly awaiting the acceptance of my invite.<br><br>In reply to: <a href="https://x.com/kito99/status/4068329341" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">4068329341</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4065360817',
    created: 1253228083000,
    type: 'post',
    text: 'Webmail (e.g., gmail) is the modern word processor. I\'m a wrong?',
    likes: 0,
    retweets: 0
  },
  {
    id: '4062801552',
    created: 1253220529000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> If the PrettyFaces creator is at JSFSummit, I will personally promote his presentation. No pressure Lincoln ;)<br><br>In reply to: <a href="https://x.com/lincolnthree/status/4062229821" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">4062229821</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4061934865',
    created: 1253217966000,
    type: 'post',
    text: 'I\'m helping EPA, through TetraTech, coordinate a community leader from Red Hat to speak about community collaboration at EPA conference.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4049424619',
    created: 1253171374000,
    type: 'post',
    text: 'Up too late discovering my own library of CDs. Just dawned on me that I can tap into what I own. Not slow, just organizationally challenged.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4049397842',
    created: 1253171223000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Ah, thanks for the clarification.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/4043243443" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">4043243443</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4043081249',
    created: 1253149108000,
    type: 'post',
    text: 'Convincing? E-mail runs my life. Like swimming in the Ocean. I pray each day for a wave to come so I can ride it to shore. Please save me.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4043009338',
    created: 1253148911000,
    type: 'post',
    text: 'For those asking, Web Beans has to change it\'s name because a) Red Hat was "asked" to rename it and b) because it has "bean" in it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4042947740',
    created: 1253148740000,
    type: 'post',
    text: 'From my previous post, ShrinkWrap originated from JBoss Test Harness, which was built as the foundation of the JSR-299 TCK.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4042927934',
    created: 1253148684000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Assemble JARs in code, declaratively: <a href="http://www.jboss.org/community/wiki/ShrinkWrap" rel="noopener noreferrer" target="_blank">www.jboss.org/community/wiki/ShrinkWrap</a> No build step required!<br><br>In reply to: <a href="https://x.com/ALRubinger/status/4041742163" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">4041742163</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4038262800',
    created: 1253135091000,
    type: 'post',
    text: 'Hey followers, what do you think of the name Chain maille for the JSR-299 RI? Yes, we have to change the name again.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4038045687',
    created: 1253134478000,
    type: 'post',
    text: 'All the free PDF viewers on Android pretty much suck. Strategy is ok: Convert PDF to image on remote server then pan image. Impl bad.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4037306231',
    created: 1253132387000,
    type: 'post',
    text: 'Evidence the cleaning people have been here. All the towels are straight and the pictures are crooked. Hmm.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4037181023',
    created: 1253132024000,
    type: 'post',
    text: 'Silicon Valley, we have a problem. The JSR-314-open list archives cuts off at the end of June. Burned up in the ozone layer? Time to triage.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4036064095',
    created: 1253128780000,
    type: 'post',
    text: 'Vote for DocBook export in #Google Docs. <a href="http://productideas.appspot.com/#9/e=220cb&amp;t=docbook" rel="noopener noreferrer" target="_blank">productideas.appspot.com/#9/e=220cb&amp;t=docbook</a>',
    likes: 0,
    retweets: 0,
    tags: ['google']
  },
  {
    id: '4035585153',
    created: 1253127444000,
    type: 'post',
    text: 'JSF 2.0 MR1 proposed changelog: <a href="http://wiki.java.net/bin/view/Projects/Jsf2MR1ChangeLog" rel="noopener noreferrer" target="_blank">wiki.java.net/bin/view/Projects/Jsf2MR1ChangeLog</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4035419676',
    created: 1253126978000,
    type: 'reply',
    text: '@mwessendorf The #jsf public archive is at <a href="http://archives.java.sun.com/jsr-314-open.html" rel="noopener noreferrer" target="_blank">archives.java.sun.com/jsr-314-open.html</a>, linked from <a href="http://wiki.jcp.org/wiki/index.php?structure_id=6" rel="noopener noreferrer" target="_blank">wiki.jcp.org/wiki/index.php?structure_id=6</a><br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0,
    tags: ['jsf']
  },
  {
    id: '4032456236',
    created: 1253118379000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> Personally I would choose src/itest/java (or simply itest/java to follow up on my previous Maven suggestion).<br><br>In reply to: <a href="https://x.com/mraible/status/4031798089" rel="noopener noreferrer" target="_blank">@mraible</a> <span class="status">4031798089</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4032412689',
    created: 1253118253000,
    type: 'post',
    text: 'Want to follow key #jsf people? A directory has already been assembled for you <a href="http://blog.rainer.eschen.name/2009/07/01/first-contact-the-faces-of-jsf/" rel="noopener noreferrer" target="_blank">blog.rainer.eschen.name/2009/07/01/first-contact-the-faces-of-jsf/</a> courtesy of Rainer Eschen.',
    likes: 2,
    retweets: 0,
    tags: ['jsf']
  },
  {
    id: '4030682131',
    created: 1253113239000,
    type: 'post',
    text: 'Good to see next version of Android will make it easier to toggle between picture view and camera capture. Takes about 3 clicks now.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4030561868',
    created: 1253112880000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> I\'ve made a similar objection, exception I think "src" alone is wrong. Tests are source code too. I propose target, main, and test.<br><br>In reply to: <a href="https://x.com/dhanji/status/4023159279" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">4023159279</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4029094249',
    created: 1253108211000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Meditate<br><br>In reply to: <a href="https://x.com/maxandersen/status/4028469410" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">4028469410</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4019757414',
    created: 1253066735000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Every Belgian beer must have a special glass. It\'s written in the Belgian law somewhere ;)<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4019743437',
    created: 1253066700000,
    type: 'post',
    text: 'I wish I could split part of an e-mail into a new thread w/ custom subject in #gmail. I feel constrained by what I can do with my data.',
    likes: 0,
    retweets: 0,
    tags: ['gmail']
  },
  {
    id: '4018902741',
    created: 1253064390000,
    type: 'post',
    text: 'I\'m sampling La Fin du Monde, a Belgian style ale brewed in Quebec. A bit light for my taste, but then again, I don\'t have the right glass.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4014036336',
    created: 1253050362000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> That\'s because you have to tell Hibernate Validator "What did the 5 fingers say to the face?" *Slap* "I\'m Rick James, bitch!"<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4013972272',
    created: 1253050186000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mosabua" rel="noopener noreferrer" target="_blank">@mosabua</a> I should have clarified what I meant by "editor". Okular can edit annotations in PDFs. Producing PDFs is entirely different.<br><br>In reply to: <a href="https://x.com/mosabua/status/4013682926" rel="noopener noreferrer" target="_blank">@mosabua</a> <span class="status">4013682926</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4013432998',
    created: 1253048683000,
    type: 'post',
    text: 'Finally, a decent PDF editor for Linux is emerging. The app is named Okular. It\'s based on KPDF for KDE 4. <a href="http://okular.kde.org" rel="noopener noreferrer" target="_blank">okular.kde.org</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4009444627',
    created: 1253037467000,
    type: 'post',
    text: 'Seam in Action goes way beyond the framework itself by giving you a strong foundation of the practical side of Java EE. <a class="mention" href="https://x.com/ManningBooks" rel="noopener noreferrer" target="_blank">@ManningBooks</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '4009357786',
    created: 1253037206000,
    type: 'post',
    text: 'I tried the McCafe last weekend (no other option). I wasn\'t expecting much, but doesn\'t match Dunkin\' Donuts.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4005698271',
    created: 1253026028000,
    type: 'post',
    text: '<a href="http://fastflip.googlelabs.com/" rel="noopener noreferrer" target="_blank">fastflip.googlelabs.com/</a> A drastic improvement over Google News, which is by far Google\'s lamest interface. Let\'s see how it goes.',
    likes: 0,
    retweets: 0
  },
  {
    id: '4005397835',
    created: 1253025064000,
    type: 'post',
    text: 'Finally getting this JSF 2 series for DZone off the ground. Will primarily cover enhancements that Red Hat focused on. Stay tuned!',
    likes: 0,
    retweets: 0
  },
  {
    id: '4005319324',
    created: 1253024804000,
    type: 'post',
    text: 'My state of mind has improved significantly since I\'ve started using Google Calendar synced between my phone and web browser. #organized',
    likes: 0,
    retweets: 0,
    tags: ['organized']
  },
  {
    id: '3992016571',
    created: 1252972405000,
    type: 'post',
    text: 'Why doesn\'t Google Docs offer an option to export as DocBook? HTML is such a terrible markup language for portable documents.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3988194097',
    created: 1252961112000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> Oops!! That link to should have been <a href="http://javabooks.org" rel="noopener noreferrer" target="_blank">javabooks.org</a>. Got bitten by the ".com was squatted" trap.<br><br>In reply to: <a href="https://x.com/starbuxman/status/3986510683" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">3986510683</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3983457107',
    created: 1252946545000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Exactly.<br><br>In reply to: <a href="https://x.com/maxandersen/status/3982725174" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">3982725174</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3982595234',
    created: 1252943936000,
    type: 'post',
    text: 'I\'ve been reading the NFJS Magazine the last couple of nights. Good writeups on Drools 5, the role of architect and popularity math.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3982559104',
    created: 1252943822000,
    type: 'post',
    text: 'Had discussions with friends about Twitter over weekend. I let people keep their opinion if they think Twitter is silly. Works for me.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3982293297',
    created: 1252942991000,
    type: 'post',
    text: 'Devoxx 08 was my most successful book signing by leaps and bounds. I wonder if this had something to do with it.',
    photos: ['<div class="entry"><img class="photo" src="media/3982293297.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '3982090438',
    created: 1252942347000,
    type: 'post',
    text: 'JBoss by Red Hat is a premium sponsor of Devoxx. We ♥ Devoxx. The new JBoss logo is displayed on devoxx.com. Black &amp; red, my favorite ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '3982046650',
    created: 1252942212000,
    type: 'post',
    text: 'News flash: the combined Oracle/Red Hat university talk titled "JSF 2 and beyond: Keep progress coming" got accepted at Devoxx.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3981926438',
    created: 1252941830000,
    type: 'post',
    text: 'Random want: I eagerly await the day I\'m given a branding iron for my steaks from SkyMall. Ridiculous, yes. That\'s the fun of it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3955679324',
    created: 1252854250000,
    type: 'post',
    text: 'Up close and personal w/ police at wife\'s 5k race sponsored by local force',
    photos: ['<div class="entry"><img class="photo" src="media/3955679324.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '3918391738',
    created: 1252699312000,
    type: 'post',
    text: 'The HTC Android family: <a href="http://www.androidcentral.com/sites/androidcentral.com/files/articleimage/Casey Chan/2009/07/Picture 4.png" rel="noopener noreferrer" target="_blank">www.androidcentral.com/sites/androidcentral.com/files/articleimage/Casey Chan/2009/07/Picture 4.png</a> My family on has 2 out of the 3.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3916259023',
    created: 1252692557000,
    type: 'post',
    text: 'While on the CTA in Chicago I saw a faded mural on the side of a building that read "We will not forget" for 9/11 and 7/41. True that.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3915822161',
    created: 1252691190000,
    type: 'post',
    text: 'Seam Recipes #1 from resident Seam field expert Christian Bauer (of Hibernate fame) <a href="http://in.relation.to/Bloggers/SeamRecipesPart1" rel="noopener noreferrer" target="_blank">in.relation.to/Bloggers/SeamRecipesPart1</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3915734444',
    created: 1252690915000,
    type: 'post',
    text: 'I\'m looking at the Google home page wondering if they did an update or I\'m just having a crazy moment. I love that type of upgrade :(',
    likes: 0,
    retweets: 0
  },
  {
    id: '3914819443',
    created: 1252688128000,
    type: 'post',
    text: 'Slides from #jbossworld are available: <a href="http://www.redhat.com/promo/summit/2009/downloads/#jbw" rel="noopener noreferrer" target="_blank">www.redhat.com/promo/summit/2009/downloads/#jbw</a> including Best Kept Secrets of Seam, RichFaces, JSF &amp; Facelets',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3903394960',
    created: 1252637929000,
    type: 'post',
    text: 'Upgraded my G1 phone from a 1GB to an 8GB memory card. Plenty of breathing room to expand my music library now!',
    likes: 0,
    retweets: 0
  },
  {
    id: '3897452976',
    created: 1252620877000,
    type: 'post',
    text: 'Pandora customer support is awesome. Personal messages. I love when people actually write me back.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3894074548',
    created: 1252610707000,
    type: 'post',
    text: 'I think Android clients for JOPR (JBoss Admin Console) are a must have? Who\'s with me?',
    likes: 0,
    retweets: 0
  },
  {
    id: '3894028771',
    created: 1252610564000,
    type: 'post',
    text: 'Pandora app just launched on Android yesterday. Woot!',
    likes: 0,
    retweets: 0
  },
  {
    id: '3893911621',
    created: 1252610202000,
    type: 'post',
    text: 'I was feeling good about my RockBand drum skills until someone introduce me to Tony Royster Jr. <a href="https://youtu.be/WPncumXZExo" rel="noopener noreferrer" target="_blank">youtu.be/WPncumXZExo</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3893871262',
    created: 1252610081000,
    type: 'post',
    text: 'I admit I\'m working against my own cause in trying to hush the buzz about Web OS in favor of Android. Too many options hurts the cause.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3893258311',
    created: 1252608189000,
    type: 'post',
    text: 'Asked Google to donate Android phones for Manning promo. They said that they don\'t give away phones. WTF? #marketingfail #nolove #google',
    likes: 0,
    retweets: 0,
    tags: ['marketingfail', 'nolove', 'google']
  },
  {
    id: '3893196324',
    created: 1252607996000,
    type: 'post',
    text: 'Have you ever noticed how every bathroom uses a different interface for dispensing towels or air for drying hands? Sobriety test perhaps?',
    likes: 0,
    retweets: 0
  },
  {
    id: '3887295642',
    created: 1252589953000,
    type: 'post',
    text: 'A Silicon Valley JUG member recorded my JSR-299 (CDI) and JSF 2.0 talks. <a href="http://caffeinatedcode.com/wsup/cafe/entry/jsr_299_contexts_and_dependency" rel="noopener noreferrer" target="_blank">caffeinatedcode.com/wsup/cafe/entry/jsr_299_contexts_and_dependency</a> <a href="http://seamframework.org/Community/SeamCommunityEvents" rel="noopener noreferrer" target="_blank">seamframework.org/Community/SeamCommunityEvents</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '3871290853',
    created: 1252529478000,
    type: 'post',
    text: 'RHEL 5.4 has arrived. Just passing on the news.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3847793467',
    created: 1252442106000,
    type: 'post',
    text: 'I met two other passionate Android users at #jbossworld. In the near future such and encounter won\'t be so rare.',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3847763819',
    created: 1252442014000,
    type: 'post',
    text: 'I left out the main highlight of the weekend. My sis and I organized a Maryland Blue Crab feast with the family. A true Maryland tradition.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3844460903',
    created: 1252431378000,
    type: 'post',
    text: 'Spent the weekend at family "farmhouse" visiting my sister and entourage as well as family from MD. #rockband #boating #wakeboarding',
    likes: 0,
    retweets: 0,
    tags: ['rockband', 'boating', 'wakeboarding']
  },
  {
    id: '3844420873',
    created: 1252431249000,
    type: 'post',
    text: 'I\'ve been recovering from conference lag. #jbossworld really took a lot out of me, partially because I arrived travel weary as it was.',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3764131833',
    created: 1252094501000,
    type: 'post',
    text: 'I had to loosen the belt on my bag to fit in the extra cargo, but with some shoving I got it all squeezed in there.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3761856312',
    created: 1252087449000,
    type: 'post',
    text: 'Bags packed, check. Shades, check. Red swag, check. Ad-hoc consulting, check. Feelin good about getting things wrapped up. #jbossworld',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3761806144',
    created: 1252087295000,
    type: 'post',
    text: 'Finally reunited with my sunglasses! Thanks Mark and Twitter! A lost and found system that actually works FTW #jbossworld',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3752399677',
    created: 1252048302000,
    type: 'post',
    text: 'JBoss is abolutly hardcore. 5 bars in and still debating spec issues. We are dedicated about Java EE no matter what hour of the night.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3751380736',
    created: 1252042888000,
    type: 'post',
    text: '#jbossworld inside joke #2. Can I have a veggie burger? Make sure it is fried in vegetable oil...and can I get some bacon on that?',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3751355770',
    created: 1252042772000,
    type: 'post',
    text: 'Found myself a St Bernadus on the pub crawl and introduced <a class="mention" href="https://x.com/Rrus" rel="noopener noreferrer" target="_blank">@Rrus</a> and Rob to the art of Belgian Beer.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3749925367',
    created: 1252036909000,
    type: 'post',
    text: 'I got word my shades have been found. Just need to meet up to get them into my hands. #jbossworld',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3749126074',
    created: 1252034169000,
    type: 'post',
    text: 'I found Seam in the Chicago Museum of Industry',
    photos: ['<div class="entry"><img class="photo" src="media/3749126074.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '3746778414',
    created: 1252027066000,
    type: 'post',
    text: 'Max mulling over a fine American dining menu #jbossworld',
    photos: ['<div class="entry"><img class="photo" src="media/3746778414.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3744277861',
    created: 1252019095000,
    type: 'post',
    text: 'Fancy water at #jbossworld',
    photos: ['<div class="entry"><img class="photo" src="media/3744277861.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3743798309',
    created: 1252017549000,
    type: 'post',
    text: 'Time is running out to track down my Oakleys. Leave at JBoss booth if you can\'t find me. #jbossworld',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3743760086',
    created: 1252017426000,
    type: 'post',
    text: 'Max is showing off RichFaces layout, modal panel and skinning at #jbossworld.',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3743698799',
    created: 1252017231000,
    type: 'post',
    text: 'JBoss AS (maybe JBoss tools) should have network latency emulator. Developers see an unreal view of world during dev esp w/ Ajax.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3743623116',
    created: 1252016993000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/atonse" rel="noopener noreferrer" target="_blank">@atonse</a> My Dad loves black (but not like goth or anything). He would always pick black for our t-ball and baseball jerseys.<br><br>In reply to: @atonse <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3743320996',
    created: 1252016032000,
    type: 'reply',
    text: 'Amen! <a class="mention" href="https://x.com/RT" rel="noopener noreferrer" target="_blank">@RT</a> <a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a>: Clean house makes spirits rise. Worth paying for home cleaning once in a while.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3743304447',
    created: 1252015980000,
    type: 'post',
    text: 'New JBoss shirts. Black. My dad would be thrilled.',
    photos: ['<div class="entry"><img class="photo" src="media/3743304447.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '3743038644',
    created: 1252015153000,
    type: 'post',
    text: 'Android is about to break out like a tapered athlete. At least, I hope it does.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3742998543',
    created: 1252015023000,
    type: 'post',
    text: 'Yes, I still owe you all slides from my recent presentations. Haven\'t had a minute to sit down to update the Seam wiki yet.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3742991779',
    created: 1252015001000,
    type: 'post',
    text: 'Someone caught my JSR-299 and JSF 2.0 talks at SVJUG on tape. Sweet! Will post link to audio file soon.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3741176435',
    created: 1252009271000,
    type: 'post',
    text: 'I would argue that creating a new annotation in Java is not easy. It should be two words + package. It ain\'t that simple.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3741102649',
    created: 1252009046000,
    type: 'post',
    text: 'The default retention policy of annotations (i.e., not runtime) is absolutely ridiculous and asinine.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3740724370',
    created: 1252007898000,
    type: 'post',
    text: 'Gavin and Pete on stage at #jbossworld',
    photos: ['<div class="entry"><img class="photo" src="media/3740724370.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3740628961',
    created: 1252007599000,
    type: 'post',
    text: '&gt; 100 people in JSR 299 talk at #jbossworld Not enough chairs',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3740069046',
    created: 1252005815000,
    type: 'post',
    text: 'OfficeMax says "We love JON. Please make it more stable." #jbossworld',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3739801920',
    created: 1252004964000,
    type: 'post',
    text: 'Design improvement: Put name tag on both sides of conference badge. #jbossworld',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3739532728',
    created: 1252004119000,
    type: 'post',
    text: 'Question came up last night in BOF whether any non-Red Hat partners provides enterprise Seam support. Speak up. #jbossworld',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3739136842',
    created: 1252002814000,
    type: 'post',
    text: 'Looking for tall guy with dark hair who has my shades (hopefully). Still holding reward book.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3729347568',
    created: 1251962326000,
    type: 'post',
    text: 'I got seperated from my shades (Oakley Straights) while dancing at #jbossworld. Free book for whomever can track them down for me.',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3729148566',
    created: 1251961179000,
    type: 'post',
    text: 'DJ was laying down the old skool tracks at afterparty. First time I ever danced at a conference. Tone Loc, Will Smith, Beastie Boys...',
    likes: 0,
    retweets: 0
  },
  {
    id: '3729109756',
    created: 1251960954000,
    type: 'post',
    text: 'Fantastic double party at #jbossworld. I loved the swing music. Took me back to my wedding and made me miss my wife all the more.',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3729058420',
    created: 1251960655000,
    type: 'post',
    text: 'The new man in town at the #jbossworld afterparty',
    photos: ['<div class="entry"><img class="photo" src="media/3729058420.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3729007743',
    created: 1251960360000,
    type: 'post',
    text: 'Scene from outside #jbossworld afterparty.',
    photos: ['<div class="entry"><img class="photo" src="media/3729007743.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3718519229',
    created: 1251924431000,
    type: 'post',
    text: 'Every session survey that you complete enters you to win a Flip MinoHD camcorder per day. #jbossworld',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3718232219',
    created: 1251923548000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Yep. That\'s why Seam has to patch it. That was a tip in the secrets talk.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3718201757',
    created: 1251923457000,
    type: 'post',
    text: 'Great use case for iPhone/Android app. Session survey for conference. Option to post directly to twitter after submitting. #jbossworld',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3714093182',
    created: 1251909156000,
    type: 'post',
    text: 'RedHat JBoss booth at #jbossworld',
    photos: ['<div class="entry"><img class="photo" src="media/3714093182.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3714050227',
    created: 1251909007000,
    type: 'post',
    text: 'SA from RedHat recounted story about deploying software to Vatican. They had issue about installing "daemons" in their infrastructure.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3713971611',
    created: 1251908723000,
    type: 'post',
    text: 'Secrets of Seam and RichFaces are out of the bag at #jbossworld. Great audience and we managed to fit it all in. Catch us around for more.',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3711675788',
    created: 1251900378000,
    type: 'post',
    text: 'Come to Best Kept Secrets 9:40 @ #jbossworld; win a copy of Seam in Action. Have a bet w/ <a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> on who can pull a bigger crowd.',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3711491287',
    created: 1251899685000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> I\'m at Caribou Coffee with Emmanuel chillax\' before our presentations at 9:40. Should be in room about 9:30.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3711451499',
    created: 1251899530000,
    type: 'post',
    text: 'Hard to believe #jbossworld is just starting. I feel like I\'ve already been to a whole conference. Got to pace myself a bit more ;)',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3705740884',
    created: 1251868868000,
    type: 'post',
    text: '#jbossworld diehards. Shake off that last drink and get ready to be up early for unveiling of best kept Seam/RichFaces secrets at 9:40.',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3705572635',
    created: 1251868103000,
    type: 'post',
    text: 'Thanks for the killer dinner Ray!',
    likes: 0,
    retweets: 0
  },
  {
    id: '3705523136',
    created: 1251867885000,
    type: 'post',
    text: '#jbossworld inside joke #1. Hi, I\'m Max, T-y-r-e-l-l, Max.',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3702928988',
    created: 1251858588000,
    type: 'post',
    text: 'Caught a shwanky party at Zen 451; customers, good wine and little red cakes. Now somewhere else I just followed Ray.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3701714364',
    created: 1251854861000,
    type: 'post',
    text: 'Welcome reception at #jbossworld receives A+. Lots of red. Does it for me.',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3698512930',
    created: 1251844663000,
    type: 'post',
    text: 'Just had long Maven conversation w/ Jason van Zyl. Now off to hang out at welcome reception at #jbossworld',
    likes: 0,
    retweets: 0,
    tags: ['jbossworld']
  },
  {
    id: '3694597518',
    created: 1251832406000,
    type: 'post',
    text: 'Maven discussions are endless. Never fails.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3693570166',
    created: 1251829147000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Brick view for me too.<br><br>In reply to: <a href="https://x.com/maxandersen/status/3678622257" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">3678622257</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3689469983',
    created: 1251816332000,
    type: 'post',
    text: 'The fact that OpenOffice can\'t import a KeyNote presentation is really frustrating. I can\'t believe this feature was overlooked.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3688997726',
    created: 1251814790000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/apemberton" rel="noopener noreferrer" target="_blank">@apemberton</a> I believe that JPA should cater more to lookup tables, which is a heavy use case in business apps.<br><br>In reply to: <a href="https://x.com/apemberton/status/3679734873" rel="noopener noreferrer" target="_blank">@apemberton</a> <span class="status">3679734873</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3688971752',
    created: 1251814703000,
    type: 'post',
    text: 'At a customer side conference today in Chicago. We are "preconferencing". Pete and I are doing our CDI and Seam 3 talk.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3688942616',
    created: 1251814604000,
    type: 'post',
    text: 'First official day of Manning Pop Quiz promotion! Participate daily to win a Kindle or iPhone <a href="http://manning.com/popquiz/" rel="noopener noreferrer" target="_blank">manning.com/popquiz/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3678758215',
    created: 1251768903000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/apemberton" rel="noopener noreferrer" target="_blank">@apemberton</a> Good analysis. Experience has proved to me that enums are next to useless. There are a very small number of practical uses.<br><br>In reply to: <a href="https://x.com/apemberton/status/3678420526" rel="noopener noreferrer" target="_blank">@apemberton</a> <span class="status">3678420526</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3678563751',
    created: 1251768323000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/crazybob" rel="noopener noreferrer" target="_blank">@crazybob</a> Same in Baltimore. SouthWest is spreading the love everywhere. Finally, an airline not on my ban list!<br><br>In reply to: <a href="https://x.com/crazybob/status/3677144854" rel="noopener noreferrer" target="_blank">@crazybob</a> <span class="status">3677144854</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3678152371',
    created: 1251767107000,
    type: 'post',
    text: 'First time I have ever seen canned water; on SouthWest flight.',
    photos: ['<div class="entry"><img class="photo" src="media/3678152371.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '3675478942',
    created: 1251758875000,
    type: 'post',
    text: 'Scary. I went through security and thought my SD card got erased. Just needed to eject and reinsert. With that, I\'m off to JBoss World.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3660919927',
    created: 1251699438000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Yeah, I might start using Picassa since that is where the photos are coming from. As for phone, it would be Android.<br><br>In reply to: <a href="https://x.com/maxandersen/status/3660241048" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">3660241048</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3659603448',
    created: 1251693020000,
    type: 'post',
    text: 'Bought an 8GB SDHC card for Rock Band only to find out Rock Band can\'t use SDHC cards. Geez.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3659592904',
    created: 1251692979000,
    type: 'post',
    text: 'Just ordered tickets tonight to see 1) Gaelic Storm and 2) Ricky Skaggs and Kentucky Thunder at Strathmore. <a href="http://strathmore.org" rel="noopener noreferrer" target="_blank">strathmore.org</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3658819523',
    created: 1251689933000,
    type: 'post',
    text: 'The Java applet photo uploader for FaceBook should just die.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3648043143',
    created: 1251652068000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> MLB doesn\'t even phase me. Waaaaaay to long.<br><br>In reply to: <a href="https://x.com/metacosm/status/3639940381" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">3639940381</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3648033786',
    created: 1251652034000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> It\'s a personal thing. If I start at week #1, by the last week I just won\'t have any energy or voice left to finish out the season.<br><br>In reply to: <a href="https://x.com/tech4j/status/3644030750" rel="noopener noreferrer" target="_blank">@tech4j</a> <span class="status">3644030750</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3648018109',
    created: 1251651976000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Yeah, but few other sports are so punishing on the athletes. There are just too many injuries by the end of the season.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3639541502',
    created: 1251607015000,
    type: 'post',
    text: 'I\'ve decided to boycott the first two weeks of the NFL season because I think the season is too long. I love NFL, don\'t get me wrong.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3615188106',
    created: 1251505906000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> I dropped it in college for Dan. However, I couldn\'t retrain people who knew me before that ;)<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3613074301',
    created: 1251499116000,
    type: 'post',
    text: '"East Coast on alert for Danny" That\'s right, watch out. I\'m comin\' through!',
    likes: 0,
    retweets: 0
  },
  {
    id: '3609591602',
    created: 1251488225000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> WTF?<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3609583593',
    created: 1251488201000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Naturally, they won\'t pay until determine value. Then, they pay out of free will. Works for non-profits. Why not for profit.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/3608564529" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">3608564529</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3609513713',
    created: 1251487988000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Of course, because Seam has a support structure already. The idea is for projects that don\'t. Freedom includes freedom to pay.<br><br>In reply to: <a href="https://x.com/kito99/status/3608722068" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">3608722068</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3609488156',
    created: 1251487911000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> The key is with anything else that\'s successful. Make it easy. Make it sanctioned. Make it feel natural. Then it will happen.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/3608564529" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">3608564529</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3609223744',
    created: 1251487103000,
    type: 'post',
    text: 'myfamily.com got a nice upgrade. My family has used it for years, but to be honest facebook has been taking over its role.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3608389603',
    created: 1251484555000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Paying would always be a choice. Banking on good will, but I wonder if it would work. You could give incentive to those that pay.<br><br>In reply to: <a href="https://x.com/kito99/status/3607585805" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">3607585805</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3608353863',
    created: 1251484447000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> You are paying someone to create it, not paying for them to hide the source from you and lock you out of hacking on it.<br><br>In reply to: <a href="https://x.com/kito99/status/3607585805" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">3607585805</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3608340444',
    created: 1251484406000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> I would challenge the word donation though. I would pay for something that is open source. Its just as valuable as closed source.<br><br>In reply to: <a href="https://x.com/kito99/status/3607585805" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">3607585805</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3607051106',
    created: 1251480534000,
    type: 'post',
    text: 'I would love to see someone integrate Bean Validation into an Android UI. That would be a nice non-Java EE use case.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3605417886',
    created: 1251475746000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/richsharples" rel="noopener noreferrer" target="_blank">@richsharples</a> Good, I\'m not the only one. It is coming up on us fast. Probably because it follows summer, not that I vacationed.<br><br>In reply to: <a href="https://x.com/richsharples/status/3605299262" rel="noopener noreferrer" target="_blank">@richsharples</a> <span class="status">3605299262</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3605400840',
    created: 1251475694000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Contributors would get paid if they "committed" to a project, meaning they help maintain it, rather than just participate.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/3604482737" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">3604482737</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3605356527',
    created: 1251475560000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> I\'m working on this theory that people may pay even if the source code is open. Is that a crazy idea? Pay by free will.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/3604482737" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">3604482737</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3605319373',
    created: 1251475448000,
    type: 'post',
    text: 'Looking for open positions at Red Hat? <a href="https://redhat.ats.hrsmart.com/cgi-bin/a/alljobs.cgi?qty=25&amp;order=jobs.timedate%20DESC" rel="noopener noreferrer" target="_blank">redhat.ats.hrsmart.com/cgi-bin/a/alljobs.cgi?qty=25&amp;order=jobs.timedate%20DESC</a> Submit your CV here <a href="https://redhat.ats.hrsmart.com/cgi-bin/a/editprofile.cgi" rel="noopener noreferrer" target="_blank">redhat.ats.hrsmart.com/cgi-bin/a/editprofile.cgi</a> (not via e-mail)',
    likes: 0,
    retweets: 0
  },
  {
    id: '3604315181',
    created: 1251472419000,
    type: 'post',
    text: 'How do I convince someone to open source code and understand that by doing so they won\'t jeopardize their ability to make money?',
    likes: 0,
    retweets: 0
  },
  {
    id: '3595100302',
    created: 1251429787000,
    type: 'post',
    text: 'Android uses the concept of a long click as an event (perhaps iPhone does too). This would be a great feature for a JSF component library.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3594587751',
    created: 1251428095000,
    type: 'post',
    text: 'Fascinating talk about bacteria and group communication: <a href="https://www.ted.com/talks/bonnie_bassler_how_bacteria_talk" rel="noopener noreferrer" target="_blank">www.ted.com/talks/bonnie_bassler_how_bacteria_talk</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3594157643',
    created: 1251426714000,
    type: 'post',
    text: '<a href="http://wefollow.com" rel="noopener noreferrer" target="_blank">wefollow.com</a> Good way to track down prominent twitter feeds. I\'m already following more than I can handle ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '3594036942',
    created: 1251426341000,
    type: 'post',
    text: 'Just added myself to the <a href="http://wefollow.com" rel="noopener noreferrer" target="_blank">wefollow.com</a> twitter directory under: #laurel_md #java #javaee #seam #programming #author',
    likes: 0,
    retweets: 0,
    tags: ['laurel_md', 'java', 'javaee', 'seam', 'programming', 'author']
  },
  {
    id: '3589001157',
    created: 1251410857000,
    type: 'post',
    text: 'Java Web Developer position at Manning available: <a href="http://www.linkedin.com/e/svj/737516/216789/" rel="noopener noreferrer" target="_blank">www.linkedin.com/e/svj/737516/216789/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3588720378',
    created: 1251410030000,
    type: 'post',
    text: 'Cool! Google Calendar has a world clock sidebar widget. When you click on event, it shows time in other time zones and night/day icon.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3587688382',
    created: 1251406942000,
    type: 'post',
    text: 'Tweetup: Red Hat Summit &amp; JBoss World Tweetup on Sep 2, 2009 RSVP here: <a href="http://twtvite.com/0f5nj5" rel="noopener noreferrer" target="_blank">twtvite.com/0f5nj5</a> #rhtweetup #rhsummit #jbossworld',
    likes: 0,
    retweets: 0,
    tags: ['rhtweetup', 'rhsummit', 'jbossworld']
  },
  {
    id: '3587059274',
    created: 1251405068000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Exactly. The strange part is, the number is hyperlinked visually. It just doesn\'t work. Seems to me like a bug.<br><br>In reply to: <a href="https://x.com/kito99/status/3583203687" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">3583203687</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3581599625',
    created: 1251388336000,
    type: 'post',
    text: 'The way around the minutes problem with Google Voice is to call your own GV number then dial using 3rd party GV app and make GV a myfav.',
    likes: 1,
    retweets: 0
  },
  {
    id: '3581578337',
    created: 1251388270000,
    type: 'post',
    text: 'Why can\'t you reply to yourself on Twitter? #missingfeature',
    likes: 0,
    retweets: 0,
    tags: ['missingfeature']
  },
  {
    id: '3581570353',
    created: 1251388245000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> You need an Android phone :)<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/3579827105" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">3579827105</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3581553876',
    created: 1251388197000,
    type: 'post',
    text: 'Ah, I got burned by my first experience with Google Voice. I didn\'t realize it was using a bridge number. Used up my minutes that way.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3579632629',
    created: 1251382015000,
    type: 'post',
    text: 'A Google Calendar event should have a field for a contact number. That way you can click on event and the phone can dial away.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3578920322',
    created: 1251379432000,
    type: 'post',
    text: 'This has been a week of logistics. Planning for JUG talks down the road, polishing slides for JBoss World, expenses, and the sort.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3578832743',
    created: 1251379088000,
    type: 'post',
    text: 'Hey Seam fans. Anyone test Seam 2 with JSF 2 yet? Try with and without RichFaces because that might be another obstacle.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3564585238',
    created: 1251320829000,
    type: 'post',
    text: 'My e-mail is burying me. I just can\'t seem to find my way out from under it. 0 inbox is slipping away. Need to get out machete.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3564550059',
    created: 1251320720000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rayme" rel="noopener noreferrer" target="_blank">@rayme</a> Seriously though. You have to consciously use the caps lock key. Sentence capitalization people. It proves easier to read.<br><br>In reply to: <a href="https://x.com/rayme/status/3561881130" rel="noopener noreferrer" target="_blank">@rayme</a> <span class="status">3561881130</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3564530338',
    created: 1251320659000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> Sorry, can\'t say that I have. Really my first computer was freshman year at college. We listened to mp3s and wrote e-mail ;)<br><br>In reply to: <a href="https://x.com/rruss/status/3561586436" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">3561586436</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3561286818',
    created: 1251310339000,
    type: 'post',
    text: 'Why do older generations insist typing in all caps? It\'s so unnatural; can\'t understand the instinct. Would they write a letter in all caps?',
    likes: 0,
    retweets: 0
  },
  {
    id: '3560943028',
    created: 1251309227000,
    type: 'post',
    text: 'Social networking applications turn "what could have been" into "what was". I\'m enjoying connecting with old friends and clubs.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3542777878',
    created: 1251235374000,
    type: 'post',
    text: 'WTF FaceBook? Why can\'t the member list for a group be sorted. Kind of difficult to scan a random list of names.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3542276360',
    created: 1251233750000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> I just can\'t win, can I?<br><br>In reply to: <a href="https://x.com/jasondlee/status/3541866148" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">3541866148</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3541755103',
    created: 1251230767000,
    type: 'post',
    text: 'Custom aliases for bit.ly links are not recognized in statistics. So that service is pretty much useless. Giving tr.im a try.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3541739399',
    created: 1251230717000,
    type: 'post',
    text: 'SSO in Seam using JBoss Identity. Like other JBoss projects, JBoss Identity does not require JBoss AS. <a href="http://tr.im/seamsso" rel="noopener noreferrer" target="_blank">tr.im/seamsso</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '3540061899',
    created: 1251225304000,
    type: 'post',
    text: 'Women married recently are shifting former last name to middle name and including full name in FaceBook profile. Much easier to find them.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3539828017',
    created: 1251224526000,
    type: 'post',
    text: 'Manning is launching a contest tomorrow that seems like a lot of fun. Pop quiz questions from authors to win iPhone, Kindle, etc.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3538832004',
    created: 1251221253000,
    type: 'post',
    text: 'Parleys.com just published the videos of Jazoon talks. You get video, audio and slides. <a href="http://beta.parleys.com" rel="noopener noreferrer" target="_blank">beta.parleys.com</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '3527835591',
    created: 1251172157000,
    type: 'post',
    text: 'FaceBook photo albums don\'t have a place for a date. #missingfeature',
    likes: 0,
    retweets: 0,
    tags: ['missingfeature']
  },
  {
    id: '3527339077',
    created: 1251170440000,
    type: 'post',
    text: 'Facebook falls apart when you lean on it. More annoying than twitter.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3523368681',
    created: 1251157972000,
    type: 'post',
    text: 'Sometimes expenses take 1 hour. Sometimes they take 1 day. Today was eaten up wrestling with these systems. Very agitated.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3522101321',
    created: 1251153903000,
    type: 'post',
    text: 'Is there such thing as a streaming media server (for Linux) through which you can edit id3 tags? So annoying to see a mistake you can\'t fix.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3520404077',
    created: 1251148545000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/crazybob" rel="noopener noreferrer" target="_blank">@crazybob</a> Awesome. I\'ll test it out next week on my way to Chicago. All we want is entertainment, why don\'t most airlines get that?<br><br>In reply to: <a href="https://x.com/crazybob/status/3517359995" rel="noopener noreferrer" target="_blank">@crazybob</a> <span class="status">3517359995</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3516349695',
    created: 1251134401000,
    type: 'post',
    text: 'MCT has really good 1m diving boards, so I decided to attempt some dives after a 9 year break. I did all but one. Now I\'m paying for it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3516311702',
    created: 1251134277000,
    type: 'post',
    text: 'Had an enjoyable day yesterday at MCT reunion party. Reconnected with my coach/idle and former swimmers, their parents and kids.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3461411151',
    created: 1250898728000,
    type: 'post',
    text: 'Caught a rainbow over the stadium at the beginning of the game',
    photos: ['<div class="entry"><img class="photo" src="media/3461411151.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '3461379333',
    created: 1250898618000,
    type: 'post',
    text: 'At Nationals game with Mom',
    photos: ['<div class="entry"><img class="photo" src="media/3461379333.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '3458641357',
    created: 1250889702000,
    type: 'post',
    text: 'Finally booked flight for JBoss World. I\'m going Southwest. Now I just need to be early to get a good seat.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3456837130',
    created: 1250884240000,
    type: 'post',
    text: 'I have a big bone to pick with my corp travel agency. I had no paper tickets, requiring counter check-in, and I sat bitch on the flights.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3435798442',
    created: 1250803562000,
    type: 'post',
    text: 'Epic fail getting home today. I\'m looking at about 2hr flight delay when all said and done. American is getting put on my airline ban list.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3433636116',
    created: 1250796594000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> hot water<br><br>In reply to: <a href="https://x.com/lightguardjp/status/3432379986" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">3432379986</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3433196448',
    created: 1250795176000,
    type: 'post',
    text: 'This calendar time zone bug on Android is a traveler\'s nightmare. Switched to a manual clock.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3433157687',
    created: 1250795052000,
    type: 'post',
    text: 'I took some pictures at Dinah\'s Garden Hotel this morning. Here\'s one. Such a unique hotel.',
    photos: ['<div class="entry"><img class="photo" src="media/3433157687.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '3429345774',
    created: 1250781992000,
    type: 'post',
    text: 'Double WTF. Starbucks in San Jose airport was calculating prices with pen and scrap paper...complete with carrying numbers. #starbucksfail',
    likes: 0,
    retweets: 0,
    tags: ['starbucksfail']
  },
  {
    id: '3422968979',
    created: 1250750768000,
    type: 'post',
    text: 'When I requested a compact car, it didn\'t mean I wanted a cheap car. I\'m surprised the damn thing has power steering. #cheapenterprise',
    likes: 0,
    retweets: 0,
    tags: ['cheapenterprise']
  },
  {
    id: '3422881327',
    created: 1250750290000,
    type: 'post',
    text: 'These days the only way to get lost is to ask for directions.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3422854284',
    created: 1250750143000,
    type: 'post',
    text: 'I dig the capabilities of my Android phone, but I must say that the Garmin did a great job getting me around Atlanta. Super easy to use.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3422657680',
    created: 1250749127000,
    type: 'post',
    text: 'Thanks to everyone that came to hear about #jsr299 at #svjug! I hope you took away a lot of good info. Sorry for cutting JSF 2 short.',
    likes: 0,
    retweets: 0,
    tags: ['jsr299', 'svjug']
  },
  {
    id: '3422565647',
    created: 1250748668000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Shoot, we should have meet up. I\'m out 1st thing in morning though.<br><br>In reply to: <a href="https://x.com/kito99/status/3416998730" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">3416998730</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3422503125',
    created: 1250748342000,
    type: 'post',
    text: 'As you can tell from the previous post, the JUG meeting went long, or I should say I went long. We\'ll it was just a long day.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3422474263',
    created: 1250748190000,
    type: 'post',
    text: 'For f#$% sakes, if don\'t get something to eat in Mnt View before 10pm you are screwed! It is Chili\'s or die of starvation.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3414554444',
    created: 1250720739000,
    type: 'post',
    text: 'The San Jose airport looks like a bomb went off...actually, multiple bombs. Under major renovation...without much effort hiding it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3414530904',
    created: 1250720662000,
    type: 'post',
    text: 'The front desk agent at the hotel is a Fedora (and in turn Linux) user. Another reason to award them a 5 star review ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '3414485592',
    created: 1250720512000,
    type: 'post',
    text: 'My phone died on me on the way to the hotel though...and I forgot to memorize the hotel address. Got lucky and found it by chance.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3414474318',
    created: 1250720475000,
    type: 'post',
    text: 'Staying at Dinah\'s Garden Hotel in Mountain View. The oxygen you inhale from the plants alone is enough for a 5 star review.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3414435908',
    created: 1250720353000,
    type: 'post',
    text: 'Calendar app for Android has complete #timezonefail. When I got to California, all my appts moved back by 3 hours! Hope to find workaround.',
    likes: 0,
    retweets: 0,
    tags: ['timezonefail']
  },
  {
    id: '3408412830',
    created: 1250700742000,
    type: 'post',
    text: 'Google should show gate w/ flight status in search results. Google should also have a feedback app for Android.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3405080277',
    created: 1250689457000,
    type: 'post',
    text: 'Ah, I hate planes that don\'t fit a full size carry-on bag. #baggagesizefail #delta',
    likes: 0,
    retweets: 0,
    tags: ['baggagesizefail', 'delta']
  },
  {
    id: '3404848232',
    created: 1250688533000,
    type: 'post',
    text: 'Had a long dinner with Gavin and Emmanuel last night. Nice to get a chance to chat away from bustle of a conference.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3404227179',
    created: 1250685859000,
    type: 'post',
    text: 'I\'m now charging up both computer and self in prep for phase two of August mini JUG tour. See you at Google tonight!',
    likes: 0,
    retweets: 0
  },
  {
    id: '3400208733',
    created: 1250661666000,
    type: 'post',
    text: 'WTF? When I booked my flights for the JUG tour using the Red Hat travel site, I didn\'t get any seat assignments. Now I can\'t check in. Grrr',
    likes: 0,
    retweets: 0
  },
  {
    id: '3400133251',
    created: 1250661300000,
    type: 'post',
    text: 'My mom met Gavin tonight. Both her sons share at least one common interest with Gavin. Me = Seam = Gavin. Kevin = Motorcycles = Gavin.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3400079686',
    created: 1250661034000,
    type: 'post',
    text: 'Drove through downtown Atlanta tonight after the meeting. Really spectacular views as you tunnel through the city.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3395235857',
    created: 1250644170000,
    type: 'post',
    text: 'Slides from Atlanta JUG will be on sfwk.org and ajug.org soon.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3395105367',
    created: 1250643763000,
    type: 'post',
    text: 'Emmanuel speaking at the Atlanta JUG',
    photos: ['<div class="entry"><img class="photo" src="media/3395105367.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '3395005841',
    created: 1250643459000,
    type: 'post',
    text: 'Gavin speaking at Atlanta JUG I should say speaking and cursing :)',
    photos: ['<div class="entry"><img class="photo" src="media/3395005841.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '3390357943',
    created: 1250628644000,
    type: 'post',
    text: 'I\'m off to speak at "big hitters" night at the Atlanta JUG. Gavin: JSR-299, Dan: JSF 2.0 Red Hat contributions, Emmanuel: JSR-303.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3356788376',
    created: 1250486042000,
    type: 'post',
    text: 'There\'s no way Vick is sorry for what he did. Maybe sorry he had to sit his ass in jail, but not for what he did. I don\'t buy it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3346439736',
    created: 1250441770000,
    type: 'post',
    text: 'At my sis\'s house-warming party last night in Newnan. After 12AM it revealed the red on its neck as it turned into an arm wrestling contest.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3323779505',
    created: 1250314594000,
    type: 'post',
    text: 'Tiger Woods is totally following me on twitter because he listened to my challenge. He\'s led the first two days in the PGA tournament.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3323421554',
    created: 1250312884000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/TheBloggess" rel="noopener noreferrer" target="_blank">@TheBloggess</a>, while you are getting on the Shat\'s case, you should ask why he has a "Verified Account" and you don\'t. Is he more special?',
    likes: 0,
    retweets: 0
  },
  {
    id: '3314285616',
    created: 1250279553000,
    type: 'post',
    text: 'I splurged and got a new driver (Cleveland XLS HiCore), new golf shoes (Green Joys) and a new sand wedge (Cleveland G10 56) last night.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3314251016',
    created: 1250279441000,
    type: 'post',
    text: 'After using a \'net phone for a while, you realize that the desktop browser is screwed up because you have to manually sign in constantly.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3312514358',
    created: 1250273800000,
    type: 'post',
    text: 'Last night I put together a playlist of anthems from my summer swimming past. <a href="http://www.imeem.com/people/b2lex48/playlist/1rrXEY1y/mct-anthems-music-playlist/" rel="noopener noreferrer" target="_blank">www.imeem.com/people/b2lex48/playlist/1rrXEY1y/mct-anthems-music-playlist/</a> I found a good use for imeem.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3311325206',
    created: 1250270042000,
    type: 'post',
    text: 'I\'ve switched from tinyurl to bit.ly. I had no idea I could get stats, etc. Crap, wish I knew that sooner.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3311310557',
    created: 1250269996000,
    type: 'post',
    text: 'For those curious, there is now an FAQ on how to inject w/ JSR-299 in 3rd-party frameworks. <a href="http://www.seamframework.org/Documentation/HowDoIDoNoncontextualInjectionForAThirdpartyFramework" rel="noopener noreferrer" target="_blank">www.seamframework.org/Documentation/HowDoIDoNoncontextualInjectionForAThirdpartyFramework</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3309715543',
    created: 1250264962000,
    type: 'post',
    text: 'Linux Magazine is running an article titled "Looking Ahead to Firefox 3.6: Speed Matters" I would sure hope so.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3287564318',
    created: 1250176398000,
    type: 'post',
    text: 'I just had to type the following CATPCHA on Facebook: "sadducce fatherinlaw" WTF?',
    likes: 0,
    retweets: 0
  },
  {
    id: '3287400227',
    created: 1250175818000,
    type: 'post',
    text: 'I determined that imeem is crap and last.fm is totally awesome! imeem kept trying play ABBA and had limited suggestions. last.fm FTW!',
    likes: 0,
    retweets: 0
  },
  {
    id: '3287383645',
    created: 1250175759000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/apemberton" rel="noopener noreferrer" target="_blank">@apemberton</a> Richmond JUG (and hopefully Utah) are on my tour list ;) JSF 2 and CDI (JSR-299). In the fall sometime. Rayme is working it out.<br><br>In reply to: <a href="https://x.com/apemberton/status/3271925947" rel="noopener noreferrer" target="_blank">@apemberton</a> <span class="status">3271925947</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3287369133',
    created: 1250175707000,
    type: 'post',
    text: 'Here\'s an example of my photogenic sister at age 6.',
    photos: ['<div class="entry"><img class="photo" src="media/3287369133.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '3287306027',
    created: 1250175487000,
    type: 'post',
    text: 'My sister was easily the most photogenic child I have ever seen. Seriously, in every picture she has the perfect expression. Still true.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3286399193',
    created: 1250172104000,
    type: 'post',
    text: 'I was excited about using Google Voice to record team meeting today, but there is a bug that results in #conferececallrecordfail',
    likes: 0,
    retweets: 0,
    tags: ['conferececallrecordfail']
  },
  {
    id: '3286352149',
    created: 1250171927000,
    type: 'post',
    text: 'I had a blast playing RockBand 2 last night with my cousins. I haven\'t had that much pure fun in a long time. It makes you feel 8 again.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3267118029',
    created: 1250091622000,
    type: 'post',
    text: 'I\'m speaking at the Atlanta JUG <a href="http://www.ajug.org/confluence/display/AJUG/Home" rel="noopener noreferrer" target="_blank">www.ajug.org/confluence/display/AJUG/Home</a> and Silicon Valley JUG <a href="https://www.meetup.com/sv-jug/events/10877034/" rel="noopener noreferrer" target="_blank">www.meetup.com/sv-jug/events/10877034/</a> next week on JSF 2 &amp; 299.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3257829330',
    created: 1250045236000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> There isn\'t a VPN app yet in the Android Market. There are a couple companies working on apps, but nothing yet. Huge hole.<br><br>In reply to: <a href="https://x.com/ALRubinger/status/3254180649" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">3254180649</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3253139063',
    created: 1250028505000,
    type: 'post',
    text: 'Pragmatic Programmers is doing a magazine now. Small press baby! <a href="http://www.pragprog.com/magazines" rel="noopener noreferrer" target="_blank">www.pragprog.com/magazines</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '3253120956',
    created: 1250028441000,
    type: 'post',
    text: 'Linux magazine is running a series on developing an Android application. Only background: <a href="http://www.linux-mag.com/cache/7463/1.html" rel="noopener noreferrer" target="_blank">www.linux-mag.com/cache/7463/1.html</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3251120456',
    created: 1250020938000,
    type: 'post',
    text: 'I\'m test driving imeem. I had no idea what it was until last night. Now I get it is similar to Pandora. imeem app for Android is slick.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3251083406',
    created: 1250020806000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> Great job to the RichFaces team on the new website release. Wow, that project has really come along in its professional poise.<br><br>In reply to: <a href="https://x.com/tech4j/status/3249672685" rel="noopener noreferrer" target="_blank">@tech4j</a> <span class="status">3249672685</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3251045448',
    created: 1250020669000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Whoohooo! Happy Birthday! Here\'s to spoiling ourselves like big kids (I got Rock Band 2, though it\'s no where near my birthday).<br><br>In reply to: <a href="https://x.com/kito99/status/3250950358" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">3250950358</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3250997041',
    created: 1250020500000,
    type: 'post',
    text: 'I can\'t understand why there is no native Picasa app for Android. Hello, that was like Google\'s first non-web program.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3247792742',
    created: 1250007430000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> Shoot, didn\'t realize you guys weren\'t in the know about the JSF Summit CFP. <a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a>, our "Best Kept Secrets" talk is in.<br><br>In reply to: <a href="https://x.com/tech4j/status/3247217850" rel="noopener noreferrer" target="_blank">@tech4j</a> <span class="status">3247217850</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3247753389',
    created: 1250007283000,
    type: 'post',
    text: 'Dig it. JBossWorld / RedHat Summit has its own video. <a href="http://www.jbossworld.com/v/swf/SummitJbossWorldPromo2.flv" rel="noopener noreferrer" target="_blank">www.jbossworld.com/v/swf/SummitJbossWorldPromo2.flv</a> Created by Red Hat video team. Good job, crew!',
    likes: 1,
    retweets: 0
  },
  {
    id: '3234255297',
    created: 1249945573000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ManningBooks" rel="noopener noreferrer" target="_blank">@ManningBooks</a> Emmanuel Bernard and Dan Allen will be speaking at JBoss World Sep 1-4. Likely other Manning authors as well.<br><br>In reply to: <a href="https://x.com/ManningBooks/status/3168159317" rel="noopener noreferrer" target="_blank">@ManningBooks</a> <span class="status">3168159317</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3234201652',
    created: 1249945380000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Yes. But last week was just awful. Proves they can\'t easily sidestep an attack. Surely a Google service would not suffer like that.<br><br>In reply to: <a href="https://x.com/kito99/status/3233928808" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">3233928808</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3233734155',
    created: 1249943660000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Because it is down constantly. And conversations are flawed. And it gets attacked. And I\'m just pissy today.<br><br>In reply to: <a href="https://x.com/maxandersen/status/3233590419" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">3233590419</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3233719535',
    created: 1249943605000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/Zimbra" rel="noopener noreferrer" target="_blank">@Zimbra</a>, now that you are following me, could you implement the archive feature from Gmail? Archive removes from Inbox. (attempt 2)',
    likes: 0,
    retweets: 0
  },
  {
    id: '3233705820',
    created: 1249943555000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Hahaha, I know. I was just giving you a hard time ;)<br><br>In reply to: <a href="https://x.com/kito99/status/3233603118" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">3233603118</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3233553200',
    created: 1249942990000,
    type: 'post',
    text: 'Damn, I\'m kind of edgy today. I think <a class="mention" href="https://x.com/TheBloggess" rel="noopener noreferrer" target="_blank">@TheBloggess</a> is rubbing off on me.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3233534615',
    created: 1249942920000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Jon_E" rel="noopener noreferrer" target="_blank">@Jon_E</a> My Android phone rocks too. It\'s a orthogonal argument. The issue is that e-books are not published in an open format. It\'s a farce.<br><br>In reply to: <a href="https://x.com/Jon_E/status/3231727877" rel="noopener noreferrer" target="_blank">@Jon_E</a> <span class="status">3231727877</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3233493017',
    created: 1249942767000,
    type: 'post',
    text: 'Here\'s why I can\'t just use FaceBook to give my status updates. I don\'t want to have to friend someone for them to be able to follow me.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3233464910',
    created: 1249942662000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Twitter is always down. It rarely has to do with popularity. The key to being popular is staying up!<br><br>In reply to: <a href="https://x.com/kito99/status/3232663928" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">3232663928</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3233407475',
    created: 1249942450000,
    type: 'post',
    text: 'Should we just jump ship from Twitter at this point? Where are people willing to follow us?',
    likes: 0,
    retweets: 0
  },
  {
    id: '3233396249',
    created: 1249942409000,
    type: 'post',
    text: 'Always uses id attributes in your HTML pages at notable sections (i.e., headings). No one likes to be linked to the top of a document.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3233309734',
    created: 1249942092000,
    type: 'post',
    text: 'At least 4 new contributors just added to Web Beans. Welcome aboard!',
    likes: 0,
    retweets: 0
  },
  {
    id: '3233278678',
    created: 1249941979000,
    type: 'post',
    text: 'Today is presentation rush for JBoss World. Of course, we all submit our presentations on the last day.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3232543470',
    created: 1249939321000,
    type: 'post',
    text: 'Sweet! I won $50 for my post about adding subversion support to EasyApache in WHM! <a href="http://community.eapps.com/showthread.php?t=271" rel="noopener noreferrer" target="_blank">community.eapps.com/showthread.php?t=271</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3231887811',
    created: 1249936998000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Problem is, no major publishing houses are publishing books you want to read in those formats. It\'s not about the reader.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/3231066867" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">3231066867</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3229565450',
    created: 1249928831000,
    type: 'post',
    text: 'Sorry, I feel sort of bitter today. It irks me when industries don\'t trust consumers.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3229557037',
    created: 1249928800000,
    type: 'post',
    text: 'My wife have been in heated discussions about ereaders at least a dozen times. Reason? Secure ebooks are fool\'s gold. A deception.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3229429593',
    created: 1249928334000,
    type: 'post',
    text: 'Click on a place and jump to the first time it was described. Reset the scenary in your head.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3229423582',
    created: 1249928313000,
    type: 'post',
    text: 'Click on a person\'s name and jump to first or last time introduced.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3229412860',
    created: 1249928274000,
    type: 'post',
    text: 'ereaders are shit. Where are the unique features. I\'ll tweek a couple of ideas to spark some real innovation.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3221086544',
    created: 1249884321000,
    type: 'post',
    text: 'I have a new challenge for Tiger Woods. Try to be in the lead before the last day. I\'m just sayin\'...',
    likes: 0,
    retweets: 0
  },
  {
    id: '3220222045',
    created: 1249879097000,
    type: 'post',
    text: 'Irishmen need to learn from the Americans how to complain about the judges. It\'s baked into our culture. #pgatour',
    likes: 0,
    retweets: 0,
    tags: ['pgatour']
  },
  {
    id: '3220174293',
    created: 1249878816000,
    type: 'post',
    text: 'I agree with Mike Arrington (<a href="http://www.techcrunch.com/2009/08/09/how-i-learned-to-quit-the-iphone-and-love-google-voice/" rel="noopener noreferrer" target="_blank">www.techcrunch.com/2009/08/09/how-i-learned-to-quit-the-iphone-and-love-google-voice/</a>), the myTouch is better than iPhone. Not the least of which is standardizing on USB.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3220156533',
    created: 1249878720000,
    type: 'post',
    text: 'What is the story, is Mojarra ever going to get a logo? JSF desperately needs a logo too.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3210968831',
    created: 1249841307000,
    type: 'post',
    text: 'I tried out Chromium on my Ubuntu (Jaunty) desktop. Crap that is fast. No packages for RHEL though.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3210959934',
    created: 1249841263000,
    type: 'post',
    text: 'I feel like Hitler when he can\'t tweet about his dead dog.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3210948424',
    created: 1249841207000,
    type: 'post',
    text: 'The Ajax post doesn\'t seem to work either, so I\'m using m.twitter.com.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3210945649',
    created: 1249841193000,
    type: 'post',
    text: 'Hmm, Firefox on my desktop logs into Twitter just fine. Who knows. I\'m just going to use Opera for now.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3210910161',
    created: 1249841026000,
    type: 'post',
    text: 'Doing some pop nostagia browsing, I rediscovered Speak&amp;Spell. <a href="https://www.speaknspell.co.uk/speaknspell.html" rel="noopener noreferrer" target="_blank">www.speaknspell.co.uk/speaknspell.html</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3210825206',
    created: 1249840622000,
    type: 'post',
    text: 'I was saying that the myTouch doesn\'t have a keyboard, but really it does. Turn it sideways, type using on-screen keyboard with your thumbs.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3210820705',
    created: 1249840600000,
    type: 'post',
    text: 'Got a burning tip about Seam, RichFaces or JSF? Share it and it will be put on display at JBoss World.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3210812615',
    created: 1249840560000,
    type: 'post',
    text: 'My wife now has a better phone than I do. The balance of power has been restored. #mytouch',
    likes: 0,
    retweets: 0,
    tags: ['mytouch']
  },
  {
    id: '3210792923',
    created: 1249840465000,
    type: 'post',
    text: 'Argh. Rhythmbox crashes if I use my multimedia keyboard keys. The keys work in with Amarok, but Amarok plays every song twice. I can\'t win!',
    likes: 0,
    retweets: 0
  },
  {
    id: '3210785534',
    created: 1249840430000,
    type: 'post',
    text: 'Sweet, I just got my Google Voice invite. I\'ll have to play around with it a bit and decide how best to integrate it into my calling.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3210703040',
    created: 1249840029000,
    type: 'post',
    text: 'Twitter is officially dead from my home network. I have to shell into my webserver instead.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3151025595',
    created: 1249499828000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> I moved your comment into the document. Please update the document rather than comment. Comments are impossible to follow.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/3149662390" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">3149662390</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3149448243',
    created: 1249494486000,
    type: 'post',
    text: 'I put together an EL 2.1 wishlist on sfwk.org. I realized that we never formally stated these goals. <a href="http://seamframework.org/Documentation/EL21" rel="noopener noreferrer" target="_blank">seamframework.org/Documentation/EL21</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3148567143',
    created: 1249491530000,
    type: 'post',
    text: 'Fill out this form: <a href="https://services.google.com/fb/forms/googlevoiceinvite/" rel="noopener noreferrer" target="_blank">services.google.com/fb/forms/googlevoiceinvite/</a> Might take a couple of days to get activated.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3148552363',
    created: 1249491481000,
    type: 'post',
    text: 'Tech Happy Hour @ 5:30PM on Thursday Aug 5 @ Gordon Biersch, Rockville, MD.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3148474021',
    created: 1249491218000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rayme" rel="noopener noreferrer" target="_blank">@rayme</a> What\'s the fix. My sinuses are pretty much defective too. Does the creator do RMA numbers?<br><br>In reply to: <a href="https://x.com/rayme/status/3147157856" rel="noopener noreferrer" target="_blank">@rayme</a> <span class="status">3147157856</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3148153624',
    created: 1249490141000,
    type: 'post',
    text: 'Thanks for the tips on getting a Google Voice invite. I\'ll post when my invite arrives.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3148124599',
    created: 1249490045000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/Zimbra" rel="noopener noreferrer" target="_blank">@Zimbra</a>, now that you are following me, could you implement the archive feature from Gmail? Archive moves out of Inbox.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3147880891',
    created: 1249489229000,
    type: 'post',
    text: 'How do I get an invite for Google Voice?',
    likes: 0,
    retweets: 0
  },
  {
    id: '3147041937',
    created: 1249486389000,
    type: 'post',
    text: 'I said to my wife "but you already have my touch" ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '3147000525',
    created: 1249486247000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> That\'s a good question. I am looking forward to a post from Gavin. I don\'t want to speculate.<br><br>In reply to: <a href="https://x.com/maxandersen/status/3134295646" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">3134295646</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3146976417',
    created: 1249486166000,
    type: 'post',
    text: 'Just went to t-mobile store w/ my wife to get her mytouch, but had to leave because the system was down. #purchasefail',
    likes: 0,
    retweets: 0,
    tags: ['purchasefail']
  },
  {
    id: '3134063829',
    created: 1249427926000,
    type: 'post',
    text: 'Oh my word, JSR-299 is changing again. I\'m going to lose my mind. I\'m now hearing that <a class="mention" href="https://x.com/inject" rel="noopener noreferrer" target="_blank">@inject</a> will be required at injection points.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3130433206',
    created: 1249415165000,
    type: 'post',
    text: 'Google\'s Places Directory for Android now supports Location Selection. Before it was relative to where you were. Not good for planning.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3130314538',
    created: 1249414771000,
    type: 'post',
    text: 'Look here for links to all the JSF 2.0 and related JavaDocs: <a href="https://javaserverfaces.dev.java.net/nonav/rlnotes/2.0.0/" rel="noopener noreferrer" target="_blank">javaserverfaces.dev.java.net/nonav/rlnotes/2.0.0/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3129762981',
    created: 1249412918000,
    type: 'post',
    text: 'Here\'s the story. A managed bean is defined with <a class="mention" href="https://x.com/ManagedBean" rel="noopener noreferrer" target="_blank">@ManagedBean</a>. Specifications, such as JSR-299, can then relax this requirement.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3129748643',
    created: 1249412868000,
    type: 'post',
    text: 'A draft of the managed beans specification has been completed by Roberto Chinnici. It should be publicized soon.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3128895202',
    created: 1249409935000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jsfcentral" rel="noopener noreferrer" target="_blank">@jsfcentral</a> Make sure that it isn\'t just links to news. Consider hosting the JSF JavaDocs, FAQs about JSF, a book section, best practices.<br><br>In reply to: <a href="https://x.com/jsfcentral/status/3128618514" rel="noopener noreferrer" target="_blank">@jsfcentral</a> <span class="status">3128618514</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3128862376',
    created: 1249409822000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> It\'s overrated. Go back to vacation.<br><br>In reply to: <a href="https://x.com/kito99/status/3128631303" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">3128631303</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3128855491',
    created: 1249409798000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jennifercord" rel="noopener noreferrer" target="_blank">@jennifercord</a> I\'m finally getting the list thing going. I\'m struggling to fit lifelong friends into any one list. Perhaps I just did.<br><br>In reply to: <a href="https://x.com/jennifercord/status/3117767999" rel="noopener noreferrer" target="_blank">@jennifercord</a> <span class="status">3117767999</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3117543132',
    created: 1249356313000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jennifercord" rel="noopener noreferrer" target="_blank">@jennifercord</a> That\'s the other thing. It\'s not always clear when to friend someone. If you were once a friend, are you still a friend now?<br><br>In reply to: <a href="https://x.com/jennifercord/status/3113847116" rel="noopener noreferrer" target="_blank">@jennifercord</a> <span class="status">3113847116</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3116615143',
    created: 1249352930000,
    type: 'post',
    text: 'I finally decided to change my FaceBook picture. I figured the snowboard look was a little out of season ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '3112505492',
    created: 1249337969000,
    type: 'post',
    text: 'Two things that bother me on FaceBook. Friend details: never does relationship justice. Finding people: lack of identifying information.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3112491761',
    created: 1249337919000,
    type: 'post',
    text: 'Ah, my Inbox is overflowing. Why are you people not on vacation? Go on vacation!',
    likes: 0,
    retweets: 0
  },
  {
    id: '3110908489',
    created: 1249332004000,
    type: 'post',
    text: 'Deep subject matter for a 5-year old: <a href="http://blogs.chron.com/goodmombadmom/2009/08/your_kid_is_a_loser.html" rel="noopener noreferrer" target="_blank">blogs.chron.com/goodmombadmom/2009/08/your_kid_is_a_loser.html</a> Oh, and the fact that the kid is 5 and performing live, well, hell.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3106304969',
    created: 1249315249000,
    type: 'post',
    text: 'I wrote to Pandora to ask if and when they would release an Android port. They said it was in the works. ~1 month until release.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3098720441',
    created: 1249274207000,
    type: 'post',
    text: 'Essential links about the EL update in Java EE 6 <a href="https://mojavelinux.com/blog/archives/2009/08/why_you_didnt_know_the_unified_el_is_being_updated/" rel="noopener noreferrer" target="_blank">mojavelinux.com/blog/archives/2009/08/why_you_didnt_know_the_unified_el_is_being_updated/</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '3082423882',
    created: 1249190835000,
    type: 'post',
    text: 'I just realized that my contact page (mojavelinux.com) was broken and that mail originating from the server wasn\'t getting to me. Doh! Fixed',
    likes: 0,
    retweets: 0
  },
  {
    id: '3079125961',
    created: 1249175871000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Nope, I was just kidding about playing with Tiger. In the spirit of the golf tournaments, I went out to play some golf of my own.<br><br>In reply to: <a href="https://x.com/kito99/status/3076149331" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">3076149331</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3075687237',
    created: 1249160331000,
    type: 'post',
    text: 'Damn, at the turn to 10 we got drenched. I called it a day.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3075678733',
    created: 1249160292000,
    type: 'post',
    text: 'I am playing golf with Tiger Woods and his brother. No, not really. But that\'s about how we size up.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3065031125',
    created: 1249100795000,
    type: 'post',
    text: 'I love when you can\'t even get close enough to the spelling of a word to get the spell check to recognize what word you want. We suck.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3064746935',
    created: 1249099584000,
    type: 'post',
    text: 'Tiger was just a tiger today. -9 to go from 95th to 3rd. He may have a hitch in his stroke, but he will still kick your ass at golf.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3064611058',
    created: 1249099030000,
    type: 'post',
    text: 'Nothing describes how most people feel about having neighbors quite like this: <a href="http://thebloggess.com/?p=3506" rel="noopener noreferrer" target="_blank">thebloggess.com/?p=3506</a> True.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3064481729',
    created: 1249098490000,
    type: 'post',
    text: 'I\'m trying to beat down my Inbox. What I way to spend a Friday night, right? Lots and lots of JSF and JCP activity.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3062487236',
    created: 1249090608000,
    type: 'post',
    text: 'I\'m definitely nominating Gavin and Emmanuel for next year.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3062479084',
    created: 1249090575000,
    type: 'post',
    text: 'For the record, here were the JCP 2009 awards winners: <a href="http://jcp.org/en/press/news/2009JCPawardwinnersPR" rel="noopener noreferrer" target="_blank">jcp.org/en/press/news/2009JCPawardwinnersPR</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3062006922',
    created: 1249088740000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> I was just poking fun ;) Not that FINA really monitors people swimming to Alcatraz. 50 yrs ago, they were swimming the other way.<br><br>In reply to: <a href="https://x.com/metacosm/status/3058820866" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">3058820866</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3056884832',
    created: 1249069584000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/crazybob" rel="noopener noreferrer" target="_blank">@crazybob</a> Due to the FINA ban on full body suits, next year you might be stuck with the Speedo either way :) hahaha<br><br>In reply to: <a href="https://x.com/crazybob/status/3055163668" rel="noopener noreferrer" target="_blank">@crazybob</a> <span class="status">3055163668</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3056856887',
    created: 1249069490000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> Now that would be a great plate! MVNLUVR The wheels would show up on the car when you started driving it.<br><br>In reply to: <a href="https://x.com/matthewmccull/status/3056221200" rel="noopener noreferrer" target="_blank">@matthewmccull</a> <span class="status">3056221200</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '3056802206',
    created: 1249069299000,
    type: 'post',
    text: 'I replaced the default Android media app with Meridian, but then discovered Mixzing, definitely a better choice and development is active.',
    likes: 0,
    retweets: 0
  },
  {
    id: '3056731293',
    created: 1249069053000,
    type: 'post',
    text: 'I just put together my Google Profile. Still some blanks to fill in, such as my superpowers. <a href="http://www.google.com/profiles/dan.j.allen" rel="noopener noreferrer" target="_blank">www.google.com/profiles/dan.j.allen</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2952119889',
    created: 1249051677000,
    type: 'post',
    text: 'Way to bring home the bacon! RT <a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a>: My book is finally done!',
    likes: 0,
    retweets: 0
  },
  {
    id: '2952090882',
    created: 1249051572000,
    type: 'post',
    text: 'A shout out to all the ladies in the house that can give their men root access: <a href="https://youtu.be/B-m6JDYRFvk" rel="noopener noreferrer" target="_blank">youtu.be/B-m6JDYRFvk</a><br> ...or just sudo',
    likes: 0,
    retweets: 0
  },
  {
    id: '2951991921',
    created: 1249051214000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jonobacon" rel="noopener noreferrer" target="_blank">@jonobacon</a> I\'ll take up your challenge. Andy Schwartz is an #epiccommunityhero for this JSF 2.0 overview post. <a href="http://andyschwartz.wordpress.com/2009/07/31/whats-new-in-jsf-2/" rel="noopener noreferrer" target="_blank">andyschwartz.wordpress.com/2009/07/31/whats-new-in-jsf-2/</a><br><br>In reply to: <a href="https://x.com/jonobacon/status/2940355361" rel="noopener noreferrer" target="_blank">@jonobacon</a> <span class="status">2940355361</span>',
    likes: 0,
    retweets: 0,
    tags: ['epiccommunityhero']
  },
  {
    id: '2936614472',
    created: 1248983618000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/mojavelinux" rel="noopener noreferrer" target="_blank">@mojavelinux</a> From reports I have uncovered, XMP3 devices around the world seem to be dying all at once. Conspiracy? Or just #designfail?',
    likes: 0,
    retweets: 0,
    tags: ['designfail']
  },
  {
    id: '2936587737',
    created: 1248983528000,
    type: 'post',
    text: 'I\'m going through the RMA process for my Pioneer XMP3 (portable XM + MP3). One day it was alive, the next, dead.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2936555287',
    created: 1248983418000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Have fun in the sun!<br><br>In reply to: <a href="https://x.com/lincolnthree/status/2934466408" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">2934466408</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2924479800',
    created: 1248928856000,
    type: 'post',
    text: 'I had no idea that Google released Chrome as open source. Man, that went over my head. Well, I\'m excited about the V8 JS engine.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2924130203',
    created: 1248927509000,
    type: 'post',
    text: 'Khaled\'s music is known as rai - the modern form of Algerian desert blues.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2924106676',
    created: 1248927421000,
    type: 'post',
    text: 'When I first started programming, I literally fueled myself on 1 2 3 Soleils Disc 2 featuring Khaled. Amazing stuff. Go check it out.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2916804041',
    created: 1248901232000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cwash" rel="noopener noreferrer" target="_blank">@cwash</a> webbeans-dev, seam-dev, richfaces and jboss-dev on freenode Holler<br><br>In reply to: <a href="https://x.com/cwash/status/2915931312" rel="noopener noreferrer" target="_blank">@cwash</a> <span class="status">2915931312</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2915661802',
    created: 1248897150000,
    type: 'post',
    text: 'I\'ve been converted to IRC. I know, hard to believe I\'ve only used IRC a few rare times. I was an IM rat back in the day, then just e-mail.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2895463537',
    created: 1248809213000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Hahaha. Well, I\'m just saying to avoid pain of managed entities in the view, you need something like Seam. Or clone objects.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/2894796154" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">2894796154</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2893212277',
    created: 1248801033000,
    type: 'post',
    text: 'Firefox got updated via the package manager and when I restart, my session got erased. Session manager failed to write on close. Grr.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2893149429',
    created: 1248800810000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> That\'s why Seam exists. Yes, odd to have a framework to solve the problem, but it\'s necessary until JSR-299 is final.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/2890941752" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">2890941752</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2893128076',
    created: 1248800735000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> I think it\'s the flash plugin. Firefox comes to a crawl on all of my computers eventually when flash is enabled.<br><br>In reply to: <a href="https://x.com/jasondlee/status/2891843281" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">2891843281</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2893104816',
    created: 1248800653000,
    type: 'post',
    text: 'That\'s strange. I replied to my cousin on twitter and now the reply is linked to some random account. #twitterbug',
    likes: 0,
    retweets: 0,
    tags: ['twitterbug']
  },
  {
    id: '2893046820',
    created: 1248800452000,
    type: 'post',
    text: 'I remember saying a long time back that I work so that I have an excuse to listen to music. Kind of forgot about that, but it\'s still true.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2893032329',
    created: 1248800406000,
    type: 'post',
    text: 'I\'ve discovered that when I got the G1 I gained an mp3 player. My life now has a soundtrack (again).',
    likes: 0,
    retweets: 0
  },
  {
    id: '2863707705',
    created: 1248664074000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SKohler" rel="noopener noreferrer" target="_blank">@SKohler</a> I really enjoyed The Half-Blood Prince. Good tension. In fact, this one motivated me to finally decide to read the series.<br><br>In reply to: @skohler <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2859412641',
    created: 1248646009000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jennifercord" rel="noopener noreferrer" target="_blank">@jennifercord</a> I\'ll always feel very connected to the MCT swim team. For now, I still know the coach, the 15-18 swimmers and many parents.<br><br>In reply to: <a href="https://x.com/jennifercord/status/2859246223" rel="noopener noreferrer" target="_blank">@jennifercord</a> <span class="status">2859246223</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2846300838',
    created: 1248572538000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Jon_E" rel="noopener noreferrer" target="_blank">@Jon_E</a> As they say, records were made to be broken. Then again, I didn\'t have one of those fancy body suits either. Just heart.<br><br>In reply to: <a href="https://x.com/Jon_E/status/2844736767" rel="noopener noreferrer" target="_blank">@Jon_E</a> <span class="status">2844736767</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2846275251',
    created: 1248572416000,
    type: 'post',
    text: 'On a whim my wife and I decided to duck in to see Harry Potter. Our plans to mini golf got rained out. Theatre isn\'t too crowded FTW',
    likes: 0,
    retweets: 0
  },
  {
    id: '2843890843',
    created: 1248560962000,
    type: 'post',
    text: 'I no longer own the men\'s 15-18 100m breaststroke team record at MCT, but I was at least there to see it broken <a href="https://youtu.be/_UzU_7KpV2I" rel="noopener noreferrer" target="_blank">youtu.be/_UzU_7KpV2I</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2830978731',
    created: 1248489880000,
    type: 'post',
    text: 'Best Buy is the first company I have seen flash a twitter handle instead of their website during a commercial. Times are changing.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2827646690',
    created: 1248476162000,
    type: 'post',
    text: 'I hold the Mill Creek Towne team record in 15-18 100m breaststroke (1:10:60). One of few old records. <a href="http://www.mctsa.com/custpage.php?cid=88" rel="noopener noreferrer" target="_blank">www.mctsa.com/custpage.php?cid=88</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2827594338',
    created: 1248475955000,
    type: 'post',
    text: 'I\'m going over to root for my old swim team, Mill Creek Towne, @ the MSCL division A meet tomorrow. Doubles as a reunion. Go MCT!',
    likes: 0,
    retweets: 0
  },
  {
    id: '2819053827',
    created: 1248445354000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> That was probably one hell of a reception if that was the entrance. It probably squashed all of those awkward shy moments.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/2818546305" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">2818546305</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2810745289',
    created: 1248402490000,
    type: 'post',
    text: 'Last week I got upgraded from RHEL 5.2 to RHEL 5.3. It\'s been working very well...extremely stable. Only downside...the RPM repo situation.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2810667969',
    created: 1248402193000,
    type: 'post',
    text: 'shaolang is continuing to refactor the Maven CLI plugin so that we can make room for new features. Good thing, because I\'ve been busy.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2806143163',
    created: 1248385005000,
    type: 'post',
    text: 'At first I couldn\'t figure out why Android would show album covers for only some mp3s. Turns out, they are embedded in the id3 tag.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2794004375',
    created: 1248330714000,
    type: 'post',
    text: 'I\'m discovering that many of my favorite dance artists are from Belgium. 3 of my favorite things come from Belgium. Why don\'t I live there?',
    likes: 0,
    retweets: 0
  },
  {
    id: '2790893410',
    created: 1248317367000,
    type: 'post',
    text: 'My wife and I discussed the awkward relationship between publishers and ebooks over dinner. The key word is "lock-in" When will they learn?',
    likes: 0,
    retweets: 0
  },
  {
    id: '2790859166',
    created: 1248317243000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> A lot of times, it\'s the only thing that people use to choose a session, unfortunately. Too much chatting, not enough planning.<br><br>In reply to: <a href="https://x.com/aschwart/status/2780579234" rel="noopener noreferrer" target="_blank">@aschwart</a> <span class="status">2780579234</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2790772604',
    created: 1248316938000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Be careful, that type of behavior will give Eclipse some competition in the #fail space.<br><br>In reply to: <a href="https://x.com/jasondlee/status/2785851362" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">2785851362</span>',
    likes: 0,
    retweets: 0,
    tags: ['fail']
  },
  {
    id: '2790744856',
    created: 1248316840000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emacsen" rel="noopener noreferrer" target="_blank">@emacsen</a> Well, I guess I should have said the accidental sysadmin...you know the one who has more failing harddrives than not at home ;)<br><br>In reply to: <a href="https://x.com/emacsen/status/2789578387" rel="noopener noreferrer" target="_blank">@emacsen</a> <span class="status">2789578387</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2790729124',
    created: 1248316785000,
    type: 'post',
    text: 'Finally got out to enjoy some of the summer by playing 9-holes of golf. Huffing and puffing a bit, but overall it was a good first outing.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2784997628',
    created: 1248295784000,
    type: 'post',
    text: 'I was in MicroCenter and came across this interesting device. A harddrive docking station for quick swap. Essential for sysadmins.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2766662881',
    created: 1248216537000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> Hey now, I\'m working on that. I\'ve been trying to shake my vampire tendencies.<br><br>In reply to: <a href="https://x.com/tech4j/status/2760035682" rel="noopener noreferrer" target="_blank">@tech4j</a> <span class="status">2760035682</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2759966208',
    created: 1248191851000,
    type: 'post',
    text: 'I didn\'t realize the Starbucks Pastry promotion was only until 10:30am. Well, it\'s good to know the food is better because it sucked before.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2751111567',
    created: 1248145757000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Speakeasy clocks me at download: 16554 kbps (2069.3 KB/sec), upload: 4463 kbps (557.9 KB/sec). Sick fast.<br><br>In reply to: <a href="https://x.com/kito99/status/2750908066" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">2750908066</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '2751024014',
    created: 1248145420000,
    type: 'post',
    text: 'Whoa! The sourceforge.net site completely changed its look again. I feel like I am window shopping (not that I window shop much).',
    likes: 0,
    retweets: 0
  },
  {
    id: '2750926927',
    created: 1248145059000,
    type: 'post',
    text: 'I wrote the 299 TCK reference guide using a docbook editor recently turned open source: Serna. <a href="http://www.syntext.com/products/serna-free/" rel="noopener noreferrer" target="_blank">www.syntext.com/products/serna-free/</a> grade = B',
    likes: 0,
    retweets: 0
  },
  {
    id: '2750891748',
    created: 1248144924000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> Nope, HBO does not allow it\'s shows to be displayed on Hulu.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/2744063359" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">2744063359</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2750876823',
    created: 1248144867000,
    type: 'post',
    text: 'Have a St. Bernardus Abt 12 chilling in my fridge. First discovered it after my talk at Richmond JUG. It\'s the quintessential trappist beer.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2744012757',
    created: 1248118338000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> I know. But I have vices called Entourage &amp; True Blood and I just...can\'t...wait...to...watch. Like a drug I\'m ashamed to say.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/2743954141" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">2743954141</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2743875987',
    created: 1248117822000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> The problem was there were all these extra services I didn\'t asked to be charged for. Internet is 52.99/mo. Granted, very fast.<br><br>In reply to: <a href="https://x.com/kito99/status/2743855712" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">2743855712</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2743858486',
    created: 1248117754000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> With work you can run most anything on Tomcat. After all, JBoss AS bundles Tomcat. The point is what you get out of the box.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/2722899297" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">2722899297</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2743844076',
    created: 1248117699000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Minibiti" rel="noopener noreferrer" target="_blank">@Minibiti</a> I just got a new Syste76 computer with an ATI Radeon HD 4800 series (generic brand) graphics card.<br><br>In reply to: <a href="https://x.com/Minibiti/status/2739125336" rel="noopener noreferrer" target="_blank">@Minibiti</a> <span class="status">2739125336</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2743832924',
    created: 1248117657000,
    type: 'post',
    text: 'I got to tell you, you really get screwed if you need to get HBO. One channel and it costs 15.99/mo. Grrr. How many movies could I rent?',
    likes: 0,
    retweets: 0
  },
  {
    id: '2743820591',
    created: 1248117610000,
    type: 'post',
    text: 'Thankfully, Verizon was very gracious on the phone and I got my plan resolved to what it should be give what I use.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2743808159',
    created: 1248117562000,
    type: 'post',
    text: 'I was doing my expense report last night and realized I was getting #$% by Verizon for my FIOS service. Bill was 175/mo.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2742038601',
    created: 1248110848000,
    type: 'post',
    text: 'I rediscovered a band I listened to often in my early programming days, The Beautiful South. One of my favorite songs is The Table.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2718062024',
    created: 1247982447000,
    type: 'post',
    text: 'I\'m definitely going to get Rock Band 2. I don\'t have a Wii so I\'ll just bring it to someone that does (e.g., family)',
    likes: 0,
    retweets: 0
  },
  {
    id: '2718041768',
    created: 1247982341000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Got to correct the terminology. Tomcat == servlet container. JBoss, GlassFish are app servers. The key is a lean app server.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/2713116887" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">2713116887</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2717984697',
    created: 1247982036000,
    type: 'post',
    text: 'Spent the day at the pool as part of neighbor\'s baby shower. Had fun entertaining kids in pool and giving mind a break.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2717951485',
    created: 1247981853000,
    type: 'post',
    text: 'The fglrx driver (ATI gfx cards) that ships w/ Ubuntu 9.04 is total #fail. Rebuilt packages from official version on ATI site. Perfect.',
    likes: 0,
    retweets: 0,
    tags: ['fail']
  },
  {
    id: '2696998009',
    created: 1247871267000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jbossnews" rel="noopener noreferrer" target="_blank">@jbossnews</a> We\'ll get some more information out there soon on how to throw all this stuff into a JBoss AS 5 install cleanly.<br><br>In reply to: <a href="https://x.com/RHMiddleware/status/2695511592" rel="noopener noreferrer" target="_blank">@RHMiddleware</a> <span class="status">2695511592</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2696986124',
    created: 1247871217000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jbossnews" rel="noopener noreferrer" target="_blank">@jbossnews</a> Web Beans has a deployer, part of the project build. JSF 2 just drop in JAR files. In the future, JSF 2 int will get deeper.<br><br>In reply to: <a href="https://x.com/RHMiddleware/status/2695511592" rel="noopener noreferrer" target="_blank">@RHMiddleware</a> <span class="status">2695511592</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2691842142',
    created: 1247851512000,
    type: 'post',
    text: 'The NFJS magazine download procedure leaves a lot to be desired.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2689791111',
    created: 1247844086000,
    type: 'post',
    text: 'Long story short, if you are using subversion 1.6, get your subclipse from here: <a href="http://subclipse.tigris.org/update_1.6.x" rel="noopener noreferrer" target="_blank">subclipse.tigris.org/update_1.6.x</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2689784525',
    created: 1247844062000,
    type: 'post',
    text: 'I discovered the source of my svn problems. cli client == 1.6 and subclipse == 1.4. 1.6 silently migrates working repo, breaks 1.4 client.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2681795037',
    created: 1247800831000,
    type: 'post',
    text: 'The British Open, now called the Open Championship, is this weekend. The couch might be calling on Sunday.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2678073966',
    created: 1247785835000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Lukefx" rel="noopener noreferrer" target="_blank">@Lukefx</a> Perhaps it will be one of my motions at the Red Hat Summit. It\'s not rpm I dislike, it\'s not having proper repository hierarchy.<br><br>In reply to: <a href="https://x.com/Lukefx/status/2677415012" rel="noopener noreferrer" target="_blank">@Lukefx</a> <span class="status">2677415012</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2677952129',
    created: 1247785355000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> Nope, I\'m going to keep giving them a hard time. It\'s nothing personal, I just think Ubuntu understands the need better.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/2677669215" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">2677669215</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2677662901',
    created: 1247784197000,
    type: 'post',
    text: 'Red Hat is playing the social networking card for the Red Hat Summit / JBoss World: <a href="http://redhatsummit09.eventvue.com/events" rel="noopener noreferrer" target="_blank">redhatsummit09.eventvue.com/events</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2677279663',
    created: 1247782639000,
    type: 'post',
    text: 'I told Red Hat IT that Ubuntu was kicking Red Hat\'s ass with regard to system updates. apt-get is just a rock. FYI, my work laptop is RHEL.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2676825646',
    created: 1247780852000,
    type: 'post',
    text: 'FYI, the Twitter event hash tags are #rhsummit and #jbossworld for Red Hat Summit and JBoss World, respectively.',
    likes: 0,
    retweets: 0,
    tags: ['rhsummit', 'jbossworld']
  },
  {
    id: '2676593483',
    created: 1247779941000,
    type: 'post',
    text: 'The shuffle feature of the music player on Android is screwed up. It doesn\'t exclude songs already played. No one wants true shuffle.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2675344038',
    created: 1247775129000,
    type: 'post',
    text: 'Wow, the Google Docs PDF viewer spawned from an e-mail is really nice. No need to download the document anymore.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2675143921',
    created: 1247774376000,
    type: 'post',
    text: 'Shoot, no VPN client for Android?',
    likes: 0,
    retweets: 0
  },
  {
    id: '2674950788',
    created: 1247773629000,
    type: 'post',
    text: 'One thing that annoys me in #android (one of few) is that you can\'t simply stop the media player. You have two options: play and pause.',
    likes: 0,
    retweets: 0,
    tags: ['android']
  },
  {
    id: '2673384286',
    created: 1247767612000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> Currently Amazon is very much on my good side. They liberated MP3 sales. Sure, OGG would be better, but it\'s a step forward.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/2673361098" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">2673361098</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2673363356',
    created: 1247767532000,
    type: 'post',
    text: 'I subscribed to NFJS magazine last night. I\'ll do some reading and report back with my impressions.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2673240734',
    created: 1247767062000,
    type: 'post',
    text: 'Amazon is smart enough that if you try to download the same MP3 from two different clients w/ the same account, it charges only once.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2663431422',
    created: 1247714854000,
    type: 'post',
    text: 'I\'m testing whether Amazon charges me twice if I buy the same song from the website and from my Android phone.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2663096550',
    created: 1247713464000,
    type: 'post',
    text: 'The concept of an album is dead. That\'s like going to the store to get milk and you get a box of couscous with it. Just the milk, please.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2658615377',
    created: 1247694967000,
    type: 'post',
    text: 'Several other channels are doing the same. But BPM isn\'t on board. Come on BPM! #xmradio',
    likes: 0,
    retweets: 0,
    tags: ['xmradio']
  },
  {
    id: '2658596532',
    created: 1247694893000,
    type: 'post',
    text: 'The Sirius XM Alt Nation channel (<a class="mention" href="https://x.com/altnation" rel="noopener noreferrer" target="_blank">@altnation</a>) posts the current song on Twitter, opening huge possibilities for syndication. Awesome!',
    likes: 0,
    retweets: 0
  },
  {
    id: '2654495703',
    created: 1247679123000,
    type: 'post',
    text: 'Given that I pretty much hate taking unexpected phone calls (hey, that\'s just the way I am) I think Google Voice may be quite essential.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2654420091',
    created: 1247678833000,
    type: 'post',
    text: 'XM DJ: "Two things for sure this year. Jessica Simpson won\'t be wearing a wedding ring and Tony Romo won\'t be wearing a Super Bowl ring"',
    likes: 0,
    retweets: 0
  },
  {
    id: '2653958726',
    created: 1247677096000,
    type: 'post',
    text: 'I went on a rampage the other night, downloaded all the songs I\'ve been wanting to get from Amazon mp3. Even picked up Nirvana\'s Love Buzz.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2608993846',
    created: 1247458341000,
    type: 'post',
    text: 'NFJS magazine: <a href="http://www.nofluffjuststuff.com/home/magazine_subscribe" rel="noopener noreferrer" target="_blank">www.nofluffjuststuff.com/home/magazine_subscribe</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2608979964',
    created: 1247458271000,
    type: 'post',
    text: 'Just discovered that NFJS puts out a magazine. I\'ve been looking for a good magazine to read in my rare downtime. Might have to subscribe.',
    likes: 1,
    retweets: 0
  },
  {
    id: '2608482556',
    created: 1247455961000,
    type: 'post',
    text: 'Just watched Bottleshocked, the story of how the California wines trumped the French wines in a blind taste test...twice ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '2599790073',
    created: 1247414172000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/apemberton" rel="noopener noreferrer" target="_blank">@apemberton</a> Looks nice. Good job!<br><br>In reply to: <a href="https://x.com/apemberton/status/2594462486" rel="noopener noreferrer" target="_blank">@apemberton</a> <span class="status">2594462486</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2590950932',
    created: 1247356930000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> I\'m a big swimmer, but have always been uneasy swimming in a lake. Just don\'t like to swim in water where I can\'t see bottom.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/2582573013" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">2582573013</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2590878833',
    created: 1247356563000,
    type: 'post',
    text: 'Hanging out with the family at the farm.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2577949983',
    created: 1247279364000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Congrats Max. Get some R&amp;R. You deserve it and I\'m sure you really need it too.<br><br>In reply to: <a href="https://x.com/maxandersen/status/2575354621" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">2575354621</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2577916112',
    created: 1247279197000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> That\'s one of my pet peeves about Twitter. The RT symbol counts in your 140 characters, as well as the user\'s handle.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/2575173434" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">2575173434</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2577901475',
    created: 1247279126000,
    type: 'post',
    text: 'Hm, seems that the myTouch doesn\'t have a flash for the camera either. Wonder why that keeps getting left out. Sure would be nice at times.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2577831312',
    created: 1247278791000,
    type: 'post',
    text: 'I love the symbolism of the penguin wallpaper in the MyTouch flash demo.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2577809389',
    created: 1247278685000,
    type: 'post',
    text: 't-mobile has set up a website dedicated to the MyTouch (i.e. G2) <a href="http://www.t-mobilemytouch.com" rel="noopener noreferrer" target="_blank">www.t-mobilemytouch.com</a> Enticing.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2574363771',
    created: 1247262884000,
    type: 'post',
    text: 'If you\'re up for some learning this weekend, checkout my Seam Security talk at the #javaone JBoss theater <a href="http://www.jboss.org/events/javaone.html" rel="noopener noreferrer" target="_blank">www.jboss.org/events/javaone.html</a>',
    likes: 2,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '2574232777',
    created: 1247262309000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> Congrats! I wish I could have seen it, but I was giving my Rock Star presentation simultaneously ;) Good mojo in the air.<br><br>In reply to: <a href="https://x.com/dhanji/status/2560377186" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">2560377186</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2571493456',
    created: 1247249353000,
    type: 'post',
    text: 'I just found out that I was named a JavaOne Rock Star for my talk this year. Holy S#$t!',
    likes: 0,
    retweets: 0
  },
  {
    id: '2556608040',
    created: 1247173110000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> I said the same thing about the Blackberry Pearl. It\'s almost a joke compared to these phones that double as portable computers.<br><br>In reply to: <a href="https://x.com/jasondlee/status/2556038896" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">2556038896</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2556008840',
    created: 1247170668000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> You have the right plan. My wife is holding out for G2 for same reason. She has to keep her streak of staying ahead of "the man".<br><br>In reply to: <a href="https://x.com/jasondlee/status/2555819867" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">2555819867</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2555761659',
    created: 1247169663000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/richsharples" rel="noopener noreferrer" target="_blank">@richsharples</a> What? You mean I won my argument? Sweet!<br><br>In reply to: <a href="https://x.com/richsharples/status/2555056145" rel="noopener noreferrer" target="_blank">@richsharples</a> <span class="status">2555056145</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2555750888',
    created: 1247169623000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Sure, just saying that likely it is going to be awesome because what is out there is already awesome? So look forward to it ;)<br><br>In reply to: <a href="https://x.com/jasondlee/status/2554941434" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">2554941434</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2554914303',
    created: 1247166220000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maeste" rel="noopener noreferrer" target="_blank">@maeste</a> I might need to pick that up. More battery is always good.<br><br>In reply to: <a href="https://x.com/maeste/status/2553846401" rel="noopener noreferrer" target="_blank">@maeste</a> <span class="status">2553846401</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2554910434',
    created: 1247166205000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> I can honestly say I have no interest in G2. Maybe I don\'t know what I\'m missing, but it doesn\'t feel like I\'m missing anything.<br><br>In reply to: <a href="https://x.com/jasondlee/status/2553816994" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">2553816994</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2554898298',
    created: 1247166157000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/richsharples" rel="noopener noreferrer" target="_blank">@richsharples</a> Besides, if I just use the phone, it lasts for days no problem.<br><br>In reply to: <a href="https://x.com/richsharples/status/2554104466" rel="noopener noreferrer" target="_blank">@richsharples</a> <span class="status">2554104466</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2554890686',
    created: 1247166127000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/richsharples" rel="noopener noreferrer" target="_blank">@richsharples</a> Yeah, but we didn\'t use them to download songs, browse the internet, read mail, play sounds through speakers, and use GPS.<br><br>In reply to: <a href="https://x.com/richsharples/status/2554104466" rel="noopener noreferrer" target="_blank">@richsharples</a> <span class="status">2554104466</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2554871116',
    created: 1247166049000,
    type: 'post',
    text: 'One individual I met in Peachtree City couldn\'t stop bragging about the new rims for his tires. Car companies are missing a big op here.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2554845999',
    created: 1247165950000,
    type: 'post',
    text: 'The use of golf carts in Peachtree City really is fascinating <a href="http://www.associatedcontent.com/article/392683/the_growing_trend_of_golf_cart_communities.html" rel="noopener noreferrer" target="_blank">www.associatedcontent.com/article/392683/the_growing_trend_of_golf_cart_communities.html</a> 12,000 carts, 100 miles of path',
    likes: 0,
    retweets: 0
  },
  {
    id: '2554735774',
    created: 1247165498000,
    type: 'post',
    text: '"Being wrong is a feature, not a bug, if it helps evolve a model that works"; about disruption in industry: <a href="http://michaelnielsen.org/blog/is-scientific-publishing-about-to-be-disrupted/" rel="noopener noreferrer" target="_blank">michaelnielsen.org/blog/is-scientific-publishing-about-to-be-disrupted/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2553867526',
    created: 1247162021000,
    type: 'post',
    text: 'The Kindle has been reduced to $299. I read an article recently about folks using on the beach. Connection? <a href="https://www.amazon.com/gp/product/B00154JDAI?tag=mojacomopenso-20" rel="noopener noreferrer" target="_blank">www.amazon.com/gp/product/B00154JDAI?tag=mojacomopenso-20</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2553774891',
    created: 1247161653000,
    type: 'post',
    text: 'I\'m pretty impressed with the battery in the G1 phone. I gave it a thorough lashing on my trip and it holds up to a solid day of usage.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2545653544',
    created: 1247116071000,
    type: 'post',
    text: 'Chester Bennington is a champion. If David ever took on Goliath again, you can be sure it\'s because he was listening to "New Divide".',
    likes: 0,
    retweets: 0
  },
  {
    id: '2545631022',
    created: 1247115961000,
    type: 'post',
    text: 'I can\'t believe the KDE team went and messed with my rock. The beauty of what was Amarok has vaporized. Oh the horror! Damn iTunes.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2535624454',
    created: 1247074184000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> As much as we trash Twitter for being overloaded, it seems to have proved reliable at critical times. So it\'s kind of balanced.<br><br>In reply to: <a href="https://x.com/jasondlee/status/2535185178" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">2535185178</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2535612622',
    created: 1247074138000,
    type: 'post',
    text: 'Haha, how ironic. Hot News on #verizon homepage reads "Verizon high speed internet beats cable in reliability" Hmm, not today.',
    likes: 0,
    retweets: 0,
    tags: ['verizon']
  },
  {
    id: '2535159861',
    created: 1247072398000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/senorgreaser" rel="noopener noreferrer" target="_blank">@senorgreaser</a> Yep, that\'s consistent with what\'s going on here. I\'m not wasting any time on the phone, trust me. Just doing offline stuff.<br><br>In reply to: <a href="https://x.com/WilhelmBayles/status/2533887437" rel="noopener noreferrer" target="_blank">@WilhelmBayles</a> <span class="status">2533887437</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2535123233',
    created: 1247072252000,
    type: 'post',
    text: 'How nice that #verizon has not interrupted traffic to twitter so that we can publicly shame #verizon as much as possible while we wait.',
    likes: 0,
    retweets: 0,
    tags: ['verizon', 'verizon']
  },
  {
    id: '2533922852',
    created: 1247067507000,
    type: 'post',
    text: 'Great, this #verizonfail goes back more than a week. Not good. At least I can access #gmail so I can at least communicate.',
    likes: 0,
    retweets: 0,
    tags: ['verizonfail', 'gmail']
  },
  {
    id: '2533890780',
    created: 1247067376000,
    type: 'post',
    text: 'Btw, I have the Verizon FIOS in Maryland and it seems to be down as well (I see reports that DSL is also out). #verizon #verizonfios',
    likes: 0,
    retweets: 0,
    tags: ['verizon', 'verizonfios']
  },
  {
    id: '2533864272',
    created: 1247067270000,
    type: 'post',
    text: 'Hahaha, thanks to Twitter I see that the outage is affecting much of the DC area. Verizon, you can\'t hide your outages anymore!',
    likes: 0,
    retweets: 0
  },
  {
    id: '2533839278',
    created: 1247067168000,
    type: 'post',
    text: 'Oh, and I almost forgot. #verizonfail',
    likes: 0,
    retweets: 0,
    tags: ['verizonfail']
  },
  {
    id: '2533832933',
    created: 1247067143000,
    type: 'post',
    text: 'Argh! Verizon network outage today in Maryland. Navigating through Verizon customer services system is painful as hell. Repeat questions.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2532775544',
    created: 1247062643000,
    type: 'post',
    text: 'After the return from my trip, I got my e-mail &lt; 10 in just one day...sticking to my ruthless policy of slashing messages. Feels good.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2532750096',
    created: 1247062527000,
    type: 'post',
    text: 'So what I am saying is that it all depends on what you are used to. But it\'s clear I have need to keep my chops up with IntelliJ.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2532739212',
    created: 1247062478000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> I tried to switch from Eclipse to IntelliJ while working on TCK, but had to go back to Eclipse because I wasn\'t comfortable yet.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/2532542076" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">2532542076</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2531364866',
    created: 1247055165000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> The mosquitoes in the NE are vampires. I got bitten quite a number of times in the low country and surprisingly hardly left a mark.<br><br>In reply to: <a href="https://x.com/tech4j/status/2530889759" rel="noopener noreferrer" target="_blank">@tech4j</a> <span class="status">2530889759</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2527327044',
    created: 1247026927000,
    type: 'post',
    text: 'No lie, I got offered $200 to sit for an interview. Hmmm, I could make a living just doing interviews ;) Interesting job title.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2527308490',
    created: 1247026844000,
    type: 'post',
    text: 'My session evaluations for #javaone and #communityone are in. 4.32 for my Seam on GlassFish talk and 4.56 for my page flow talk. Not bad.',
    likes: 0,
    retweets: 0,
    tags: ['javaone', 'communityone']
  },
  {
    id: '2519530201',
    created: 1246994712000,
    type: 'post',
    text: 'I feel so exhausted. The trip to the low country really wore me out...either that or I got sick. Trying to get myself back in "work" shape.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2519523417',
    created: 1246994689000,
    type: 'post',
    text: 'It would be nice of Gmail offered contact/email completion inside the body of the message, for when you want to refer someone.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2516769974',
    created: 1246985599000,
    type: 'post',
    text: 'The Beluga whales at the Georgia Aquarium are beautiful <a href="http://georgiaaquarium.org/media/images/webcams/gallery/12.jpg" rel="noopener noreferrer" target="_blank">georgiaaquarium.org/media/images/webcams/gallery/12.jpg</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2492983526',
    created: 1246858154000,
    type: 'post',
    text: 'Looks like Gmail did some polishing while I was out. I\'m eager to explore the enhancements.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2492962283',
    created: 1246858020000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> Oh, I meant on the Twiteroid app for Android. Come to think of it, maybe the feature is there. Just need to look closer.<br><br>In reply to: <a href="https://x.com/jeremynorris/status/2492491733" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">2492491733</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2492368491',
    created: 1246854670000,
    type: 'post',
    text: 'There should be a feature to star a tweet for later research.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2492340465',
    created: 1246854524000,
    type: 'post',
    text: 'I have a new policy for flying. Luxury coach. Virgin, JetBlue, etc.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2492327412',
    created: 1246854452000,
    type: 'post',
    text: 'Just had the flight from hell. 5 1/2 hours from Atlanta to BWI. #airtranfail',
    likes: 0,
    retweets: 0,
    tags: ['airtranfail']
  },
  {
    id: '2492176787',
    created: 1246853663000,
    type: 'post',
    text: 'One thing you have to get used to with touch screen phones is smudges. Maybe it is just me. I try to keep my hands washed.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2488004399',
    created: 1246832704000,
    type: 'post',
    text: 'Just toured the Georgia Aquarium. The whale shark is amazingly magestic.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2487977842',
    created: 1246832564000,
    type: 'post',
    text: 'Rotating in the Sundial in the heart of downtown Atlanta.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2478910855',
    created: 1246770813000,
    type: 'post',
    text: 'Somewhat belated at this point, but Happy 4th US of A! Cheers to freedom everywhere.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2478853443',
    created: 1246770435000,
    type: 'post',
    text: 'I traveled an hour round trip on the back of a golf cart to see the fireworks. This is a glimpse of our micro auto future I think.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2478828121',
    created: 1246770273000,
    type: 'post',
    text: 'Wow, I am outright shocked about the death of Steve McNair. He was a great asset to the Ravens.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2476691444',
    created: 1246757410000,
    type: 'post',
    text: 'At Peachtree City with my sis to watch the fireworks on golf course. Peachtree City is crazy. 90 miles of golf cart trails to get around.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2460973184',
    created: 1246660392000,
    type: 'post',
    text: 'I have never seen a beach experience like Tybee Beach. It was like a tailgate party transplanted onto the shoreline. Drinking, games, events',
    likes: 0,
    retweets: 0
  },
  {
    id: '2460948123',
    created: 1246660277000,
    type: 'post',
    text: 'Ended our scouting with a day at Tybee Beach.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2460919200',
    created: 1246660143000,
    type: 'post',
    text: 'Last night hit The Distillery for late night drinks. Owner is from Baltimore. Had a Strong Cali-Belgique and Chimay. Even has the glasses.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2460883994',
    created: 1246659970000,
    type: 'post',
    text: 'Our dinner at The Old Pink House was amazing. True what they say about great food in Savannah. Even the calzone at the beach was tasty.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2446720584',
    created: 1246584208000,
    type: 'post',
    text: 'Having an elegant diner at The Old Pink House in historic Savannah.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2432393113',
    created: 1246510098000,
    type: 'post',
    text: 'The southern hospitality and style definitely stepped up a notch when we dropped into Savannah.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2432363171',
    created: 1246509942000,
    type: 'post',
    text: 'I can\'t explain how amazingly vital Google maps and places on the G1 phone during this trip. Totally changes the experience.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2432335566',
    created: 1246509797000,
    type: 'post',
    text: 'The main grocery store chain in coastal SC is called Piggly Wiggly. Seriously? Got to be comfortable w/ your manhood to admit working there.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2424299328',
    created: 1246474267000,
    type: 'post',
    text: 'Currently enjoying waterfront lunch in Beaufort (Plum\'s)',
    likes: 0,
    retweets: 0
  },
  {
    id: '2424281307',
    created: 1246474185000,
    type: 'post',
    text: 'Just finished the long very monotonus drive from John\'s Island (charleston) to Beaufort. Not much going on in between.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2398599722',
    created: 1246333875000,
    type: 'post',
    text: 'My wife and I invented a new appertif tonight. Chocolate milkshake with Sambuca liquior. Unreal. We call it the Market St Mousse.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2398567469',
    created: 1246333713000,
    type: 'post',
    text: 'Charleston welcomed us with a big nasty rain storm accented with 60 mi/hr winds and hail. Got soaked. Did I say welcomed?',
    likes: 0,
    retweets: 0
  },
  {
    id: '2398534870',
    created: 1246333558000,
    type: 'post',
    text: 'Employees at a prison are likely more enthusiastic about their jobs than the flight attendant on our US Airways flight to Charleston.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2398511065',
    created: 1246333443000,
    type: 'post',
    text: 'Started off the day by missing our flight. Cut it to close and US Air wouldn\'t allow us to check in with a bag. Hit next flight.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2389084473',
    created: 1246290800000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> What makes it so difficult is that Java doesn\'t always do the intuitive thing for purposes of performance. Unexpected in fact.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2389064414',
    created: 1246290710000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> yup, that pretty much sums it up. I can cite violation after violation based on what I am being reminded of.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2388431430',
    created: 1246287843000,
    type: 'post',
    text: 'I\'m currently reading Java Concurrency in Practice. Very interesting and also very scary.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2388418902',
    created: 1246287785000,
    type: 'post',
    text: 'The Gmail interface for Android needs to have suggest for assigning labels. Takes too long right now.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2388394331',
    created: 1246287671000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/cwash" rel="noopener noreferrer" target="_blank">@cwash</a> Hell yes. I have the Places app for Android to track them down for me, see how well the area scores.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2387591069',
    created: 1246283684000,
    type: 'post',
    text: 'Our first relocation scouting trip is underway. We\'ve identified a number of conveniences we will be using to evaluate.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2383354195',
    created: 1246250872000,
    type: 'post',
    text: 'Also, some XM channels are not available online, and 20/20, my wife\'s #1 station, won\'t play. XM is just making me sick. Fools.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2383344359',
    created: 1246250814000,
    type: 'post',
    text: 'XM is on my ice cold list. My XMP3 died today, had to call customer support to activate different radio, then found out the SkyFi3 is lame.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2383333221',
    created: 1246250751000,
    type: 'post',
    text: 'Screw LimeWire and their stupid TrialPay crippled downloads. Not that I won\'t buy songs. I just use LimeWire to find the version I want.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2373998775',
    created: 1246211375000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SanneGrinovero" rel="noopener noreferrer" target="_blank">@SanneGrinovero</a> Right, but I want to search on the web so that when I go pick up my phone I can find the app immediately.<br><br>In reply to: <a href="https://x.com/SanneGrinovero/status/2361839604" rel="noopener noreferrer" target="_blank">@SanneGrinovero</a> <span class="status">2361839604</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2363920692',
    created: 1246139208000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rayme" rel="noopener noreferrer" target="_blank">@rayme</a> No shit, I just picked up a pack of Harpoon. Man, this is getting scary. Problem is, I have no porch or deck. Jealous.<br><br>In reply to: <a href="https://x.com/rayme/status/2362310343" rel="noopener noreferrer" target="_blank">@rayme</a> <span class="status">2362310343</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2361674417',
    created: 1246126302000,
    type: 'post',
    text: 'I can\'t understand why the official Android market website does not have a search. I know about Cyrket, but seriously.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2361524872',
    created: 1246125479000,
    type: 'post',
    text: 'I finally decided to check out Safari now that I have some time for reading. Their mobile interface is very nice for reading on a phone.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2361501775',
    created: 1246125346000,
    type: 'post',
    text: 'Zimbra has tags, which I really can\'t live without. However, it has no concept of archive, so I have to move to an archive folder manually.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2349364566',
    created: 1246047924000,
    type: 'post',
    text: 'There\'s no other way to explain it except to say Eclipse plugins get stupid. Subversion plugin refuses to recognize files as checked in.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2347567742',
    created: 1246040103000,
    type: 'post',
    text: 'According to the Gmail tips page <a href="https://www.google.com/mail/help/tips.html" rel="noopener noreferrer" target="_blank">www.google.com/mail/help/tips.html</a>, I\'m a few kicks short of Gmail master. No super stars or embedded.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2338256392',
    created: 1245985865000,
    type: 'post',
    text: 'Just attached my wife\'s widescreen LCD monitor to a NeoFlex stand <a href="https://www.amazon.com/Ergotron-33-310-060-Neoflex-Stand-Black/dp/B000FLXW90" rel="noopener noreferrer" target="_blank">www.amazon.com/Ergotron-33-310-060-Neoflex-Stand-Black/dp/B000FLXW90</a> Totally changes the dynamics of screen.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2331037952',
    created: 1245958989000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> Unfortunately I don\'t know what shaolang\'s twitter acct is, or if he has one. I\'m thrilled to see the Maven 2.1 fix.<br><br>In reply to: <a href="https://x.com/matthewmccull/status/2289201898" rel="noopener noreferrer" target="_blank">@matthewmccull</a> <span class="status">2289201898</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2330664122',
    created: 1245957334000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Amen. Death to CVS.<br><br>In reply to: <a href="https://x.com/jasondlee/status/2329867303" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">2329867303</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2321598605',
    created: 1245903432000,
    type: 'post',
    text: 'Got my wife\'s new system76.com Ubuntu desktop setup (to replace the desktop that died). The fan on the graphics card is huge! Pics soon.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2319445975',
    created: 1245893024000,
    type: 'post',
    text: 'Excellent! My wife was getting wavy lines of interference on her laptop-&gt;lcd monitor. Got an UXGA shielded cable. No more wavy lines.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2315787405',
    created: 1245875319000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> The Zimbra installation is internal. It\'s Red Hat\'s webmail interface...and actually more since it\'s the mail server too.<br><br>In reply to: <a href="https://x.com/jasondlee/status/2315490435" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">2315490435</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2315494211',
    created: 1245874100000,
    type: 'post',
    text: 'My wife is experiencing pains from a crick in her neck. As she put it, #pillowfail.',
    likes: 0,
    retweets: 0,
    tags: ['pillowfail']
  },
  {
    id: '2315470943',
    created: 1245873998000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cmaki" rel="noopener noreferrer" target="_blank">@cmaki</a> Rayme should be able to work that out with you. He\'ll be e-mailing soon. I\'d say Sept - Nov time frame.<br><br>In reply to: <a href="https://x.com/cmaki/status/2314405617" rel="noopener noreferrer" target="_blank">@cmaki</a> <span class="status">2314405617</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2315463821',
    created: 1245873968000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> 1st requirement, must be a Linux or Java client. Second, I need to upgrade from RHEL 5.2 to something newer. Ubuntu or Fedora.<br><br>In reply to: <a href="https://x.com/metacosm/status/2313901786" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">2313901786</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2315421952',
    created: 1245873799000,
    type: 'post',
    text: 'I\'ve switched from Thunderbird to Zimbra for work e-mail. So far, I feel much more in touch with things. I much prefer Ajax-driven webmail.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2313793483',
    created: 1245866610000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> I only use the web UI. Sometimes, I\'m a very slow adopter ;) I do use the Android-client on the road, but at home, the web UI.<br><br>In reply to: <a href="https://x.com/metacosm/status/2307959766" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">2307959766</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2313781422',
    created: 1245866555000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cmaki" rel="noopener noreferrer" target="_blank">@cmaki</a> Oh, I forgot to add your handle to that list. Yes, it\'s UJUG. <a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a>, can you forward the message when it arrives?<br><br>In reply to: <a href="https://x.com/cmaki/status/2311335417" rel="noopener noreferrer" target="_blank">@cmaki</a> <span class="status">2311335417</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2307044593',
    created: 1245824989000,
    type: 'post',
    text: 'Crashed Cornell Alum party 2nite at the Center Club in Baltimore MD. Sweet views of Inner Harbor. Talk was on photographer Mathew Brady.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2306038800',
    created: 1245818491000,
    type: 'post',
    text: 'I wish the Twitter web UI offered completion for follower names. I also wish there was a RT button.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2306026774',
    created: 1245818424000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/cwash" rel="noopener noreferrer" target="_blank">@cwash</a> <a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> <a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> you should be hearing from Rayme from JBoss to arrange a date for me to speak about JSF 2 at your JUG.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2283442514',
    created: 1245699480000,
    type: 'post',
    text: 'Mickelson records 5th runner-up finish at the US Open. He must have convinced himself that 1st is the worst and 2nd is the best.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2283366913',
    created: 1245699136000,
    type: 'post',
    text: 'I tried out the Guitar Hero demo for Android. Incredible how far devices have come to be able to handle all that activity and rendering.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2283347620',
    created: 1245699051000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> I agree it\'s all about the apps. The Android Market has a sick number of apps. Sirius XM is just being ignorant.<br><br>In reply to: <a href="https://x.com/jeremynorris/status/2267002229" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">2267002229</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2283330250',
    created: 1245698975000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/atonse" rel="noopener noreferrer" target="_blank">@atonse</a> Perhaps we can make use of the RewriteFilter in Seam to move the component names into the path info.<br><br>In reply to: @atonse <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2266598047',
    created: 1245599214000,
    type: 'post',
    text: 'The US Open has been a total #eventfail due to rain in the NE this weekend. Tee times are sliding worse than software release dates.',
    likes: 0,
    retweets: 0,
    tags: ['eventfail']
  },
  {
    id: '2266564216',
    created: 1245599008000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emacsen" rel="noopener noreferrer" target="_blank">@emacsen</a> Yep, I\'m in full agreement with you. They want control at a time when subscribers want to enjoy more and more freedom.<br><br>In reply to: <a href="https://x.com/emacsen/status/2265173468" rel="noopener noreferrer" target="_blank">@emacsen</a> <span class="status">2265173468</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2266544420',
    created: 1245598886000,
    type: 'post',
    text: 'Damn, I just got a response from Siruis/XM saying that they have no plans to make an app for Android phones like the iPhone app. Sucks.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2262281816',
    created: 1245561171000,
    type: 'post',
    text: 'Met a guy from Verizon Wireless; talked phones, Linux, and why Verizon doesn\'t have Android-phones. They are cautious about their network.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2262250281',
    created: 1245560962000,
    type: 'post',
    text: 'Had an awesome night at party with my wife\'s friends and friends of friends. I discovered my prowess with the drums in Rock Band. Rock on!',
    likes: 0,
    retweets: 0
  },
  {
    id: '2246155605',
    created: 1245456627000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/atonse" rel="noopener noreferrer" target="_blank">@atonse</a> No, I mean that I hope JOPR is a good example by JBoss of Seam in use.<br><br>In reply to: @atonse <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2246150697',
    created: 1245456603000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> The attendees of the Google I/O conference got the G2 phones, a soft release. It isn\'t officially out yet.<br><br>In reply to: <a href="https://x.com/jasondlee/status/2242982545" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">2242982545</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2242762179',
    created: 1245440152000,
    type: 'post',
    text: 'Think my Prius is trying to tell me something?',
    photos: ['<div class="entry"><img class="photo" src="media/2242762179.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '2241358379',
    created: 1245433873000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/atonse" rel="noopener noreferrer" target="_blank">@atonse</a> Yeah, that really should have been left for a blog entry. We are having the docs edited and likely that will come up for revision.<br><br>In reply to: @atonse <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2241311928',
    created: 1245433671000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> It\'s not that I think BigTable can\'t work. Innovation is not a bad thing. It\'s just that most people can\'t run that type of infra.<br><br>In reply to: <a href="https://x.com/dhanji/status/2234510137" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">2234510137</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2233627521',
    created: 1245386335000,
    type: 'post',
    text: 'For those that haven\'t seen it, a beta of Chrome is now available for Linux. I\'m interested to hear how well it runs.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2233565290',
    created: 1245385982000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gscottshaw" rel="noopener noreferrer" target="_blank">@gscottshaw</a> Remember I also recommended Google Apps and ranted about how broken Comcast and Verizon "On Demand" is.<br><br>In reply to: <a href="https://x.com/gscottshaw/status/2232890835" rel="noopener noreferrer" target="_blank">@gscottshaw</a> <span class="status">2232890835</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2233191665',
    created: 1245383881000,
    type: 'post',
    text: 'Looking forward to seeing an XM/Sirius app in the Android market after <a class="mention" href="https://x.com/Cmoose" rel="noopener noreferrer" target="_blank">@Cmoose</a> showed me the iPhone app.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2233182000',
    created: 1245383828000,
    type: 'post',
    text: 'Just got back from hanging with <a class="mention" href="https://x.com/gscottshaw" rel="noopener noreferrer" target="_blank">@gscottshaw</a> and <a class="mention" href="https://x.com/Cmoose" rel="noopener noreferrer" target="_blank">@Cmoose</a> at DuClaw. Back in the day we hit every DuClaw beer release. This time: reunion.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2228795667',
    created: 1245362029000,
    type: 'post',
    text: 'On the one hand I\'m a little peeved I won\'t be at Jazoon. On the other hand, I\'m glad to get some time to get some shit done. Finally.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2226344972',
    created: 1245351017000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> A former colleague would refuse to eat lunch anywhere but Taco Bell. He said "what doesn\'t kill me makes me stronger" Fool.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/2226177598" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">2226177598</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2225430323',
    created: 1245346958000,
    type: 'post',
    text: 'Read a few pages of the Frank\'s Economic Naturalist last night. Interesting perspective on how the benefits of lowering taxes is a fallacy.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2225374146',
    created: 1245346712000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> Haha, like I said, it was just senseless banter. I\'m just so shocked because I never realized that Android was so slick.<br><br>In reply to: <a href="https://x.com/jeremynorris/status/2218628031" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">2218628031</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2217974934',
    created: 1245300050000,
    type: 'post',
    text: 'Time to banter the iSheep. If you\'re pleased over your iPhone 3.0 OS, know that us Android cats were already enjoying those features. G1 FTW',
    likes: 0,
    retweets: 0
  },
  {
    id: '2217904697',
    created: 1245299648000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> The pull of the ocean is too strong in this one. Must...be...near...frothy...waves.<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/2215277004" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">2215277004</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2217889404',
    created: 1245299557000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> The long and short of it is that seamframework.org is just not a good example of Seam. Hopefully JOPR can be that solid example.<br><br>In reply to: <a href="https://x.com/kito99/status/2214577832" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">2214577832</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2213474532',
    created: 1245277522000,
    type: 'post',
    text: 'It makes me livid that Eclipse forgets what you had open when it crashes. Saving the f-ing settings periodically you dumb sloth.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2213460934',
    created: 1245277457000,
    type: 'post',
    text: 'Need to pick hotels for my trip to Charleston, SC -&gt; Beaufort, SC -&gt; Savannah, GA -&gt; Newnan, GA. Scouting trip for future home / visit sis.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2213445825',
    created: 1245277382000,
    type: 'post',
    text: 'Eclipse just died a horrible death again. I need to upgrade to 3.5; perhaps this is a sign (disregarding the fact that it shouldn\'t crash).',
    likes: 0,
    retweets: 0
  },
  {
    id: '2213395931',
    created: 1245277145000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> I just notified Ed about the JSR-314 bounce issue.<br><br>In reply to: <a href="https://x.com/kito99/status/2213002966" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">2213002966</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2213378353',
    created: 1245277060000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Yeah, this is an ongoing discussion/debate between the community and the webmaster (Christian). We need a USA mirror.<br><br>In reply to: <a href="https://x.com/kito99/status/2213171238" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">2213171238</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2211633745',
    created: 1245269138000,
    type: 'post',
    text: 'In related news, the new jcp.org website is live! Go check it out!',
    likes: 0,
    retweets: 0
  },
  {
    id: '2211628676',
    created: 1245269116000,
    type: 'post',
    text: 'It appears that the JCP mailinglists are currently down. At least, the 314 list is now rejecting my messages. #launchfail',
    likes: 0,
    retweets: 0,
    tags: ['launchfail']
  },
  {
    id: '2210272455',
    created: 1245262974000,
    type: 'post',
    text: 'I\'m currently working on TCK tests for JSR-299. I\'m going to be able to cite section numbers in the spec by the time I\'m through.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2203239726',
    created: 1245219523000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Yes, working sets is how I group the projects when I sync. But the pinned sync name is the project names separated by commas.<br><br>In reply to: <a href="https://x.com/maxandersen/status/2203132918" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">2203132918</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2201955661',
    created: 1245211281000,
    type: 'reply',
    text: '@mwessendorf JOPR is the JBoss administration console project (JON is the supported complement). <a href="http://jboss.org/jopr" rel="noopener noreferrer" target="_blank">jboss.org/jopr</a><br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2201931937',
    created: 1245211159000,
    type: 'post',
    text: 'The TestNG plugin for Eclipse is really flaky. It often caches a classpath and won\'t let it go until you remove the run configs.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2199379941',
    created: 1245198307000,
    type: 'post',
    text: 'Embedded JOPR is built with Seam and RichFaces.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2199354153',
    created: 1245198172000,
    type: 'post',
    text: 'I\'m looking at the JBoss administration console (embedded JOPR) first the first time on my own machine. Just booted JBoss AS 5.1.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2198273908',
    created: 1245192648000,
    type: 'post',
    text: 'I wish there was a way to name a team synchronization in Eclipse. It\'s useful when you are synchronizing 10+ projects in a pinned group.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2196276652',
    created: 1245179727000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Hey, trust me, I\'m not one to talk. The Seam 2 build is even scarier. Clean builds do not come easy.<br><br>In reply to: <a href="https://x.com/jasondlee/status/2188241803" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">2188241803</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2195687578',
    created: 1245176887000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> So an iMac can make even scary Ant builds look pretty. Wow, they should put that in the commercials ;)<br><br>In reply to: <a href="https://x.com/aschwart/status/2192598215" rel="noopener noreferrer" target="_blank">@aschwart</a> <span class="status">2192598215</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2195649285',
    created: 1245176702000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> NetBeans can tell. It says the line is an invalid break point for the code being debugged. Only works if the line is invalid.<br><br>In reply to: <a href="https://x.com/maxandersen/status/2194895586" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">2194895586</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2188144243',
    created: 1245126157000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a> Yeah, it\'s not too bad. Underneath it\'s a scary Ant script, but I argue that if the commands are simple, it gets the job done.<br><br>In reply to: <a href="https://x.com/aschwart/status/2186343651" rel="noopener noreferrer" target="_blank">@aschwart</a> <span class="status">2186343651</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2183123359',
    created: 1245099878000,
    type: 'post',
    text: 'String#isEmpty() is the most half-assed addition to JDK ever. So now String sucks a tiny bit less and builds break the world over.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2182321898',
    created: 1245096058000,
    type: 'post',
    text: 'JBoss Developer Studio 2.0 got a shout out in the JSF 2 refcard that was released today: <a href="http://refcardz.dzone.com/refcardz/javaserver-faces-20?oid=hom11109" rel="noopener noreferrer" target="_blank">refcardz.dzone.com/refcardz/javaserver-faces-20?oid=hom11109</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2181181088',
    created: 1245090523000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> You are going to get the centralized knowledge base you have been asking for. What better place to have it?<br><br>In reply to: <a href="https://x.com/lincolnthree/status/2180999775" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">2180999775</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2181172700',
    created: 1245090480000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Amen to that. I was skeptical about 0 Inbox, until I did it. Now I try to get there as often as I can. Congrats.<br><br>In reply to: <a href="https://x.com/kito99/status/2181083805" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">2181083805</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2181160623',
    created: 1245090421000,
    type: 'post',
    text: 'EasyApache simply rocks. It gives you all the power of compiling Apache/PHP manually but w/o the missteps. Check some boxes, then build.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2180874062',
    created: 1245089018000,
    type: 'post',
    text: 'Got word that the new JCP website is about to be unveiled. Keep your eyes open.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2180851805',
    created: 1245088913000,
    type: 'post',
    text: 'I really dig the theme song from True Blood: "Bad Things" by Jace Everett. Really captures that southern mythology theme well.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2180780478',
    created: 1245088568000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> I can only handle one service, and LinkedIn is filling that need. Of course, <a class="mention" href="https://x.com/timoreilly" rel="noopener noreferrer" target="_blank">@timoreilly</a> envisions a future of autolinking.<br><br>In reply to: <a href="https://x.com/kito99/status/2180598675" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">2180598675</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2180478912',
    created: 1245087095000,
    type: 'post',
    text: 'If you appreciate Office Space, this spin by DJ Mike Relm is fawesome. <a href="https://youtu.be/bk170vq2Fws" rel="noopener noreferrer" target="_blank">youtu.be/bk170vq2Fws</a><br> Kids are funny too.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2179784485',
    created: 1245083684000,
    type: 'post',
    text: 'Definitely check out this video interview with <a class="mention" href="https://x.com/timoreilly" rel="noopener noreferrer" target="_blank">@timoreilly</a> at FLOSS. <a href="https://twit.tv/shows/floss-weekly/episodes/73" rel="noopener noreferrer" target="_blank">twit.tv/shows/floss-weekly/episodes/73</a> He gets your opinions flowing.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2179675600',
    created: 1245083147000,
    type: 'post',
    text: 'eApps offers Plesk and WHM/cPanel plans. Plesk has a simple and clean interface. WHM/cPanel is rough and ragged, but offers reseller accts.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2179648506',
    created: 1245083015000,
    type: 'post',
    text: 'My weekend was dedicated to migrating my websites to a new server to upgrade to CentOS 5.2 and switch to a reseller configuration.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2179631184',
    created: 1245082927000,
    type: 'post',
    text: 'I finally sent my previous employee\'s laptop back to its homeland. I was beginning to wonder whether they even cared to have it back.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2158254569',
    created: 1244930379000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/rayme" rel="noopener noreferrer" target="_blank">@rayme</a> We\'ll have to get together some time for a beer. We have very similar tastes...for beer and relaxing with one.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2137202248',
    created: 1244843371000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Yeah, exactly my reaction. But with money at stake, one can\'t complain too much. Though trust me, I complained.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2136052963',
    created: 1244837559000,
    type: 'post',
    text: 'I\'ve nailed down my expense report process. Line items auto added from corp card, Firebug to trim page, print to PDF, faxzero.com to send.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2135118538',
    created: 1244833027000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> I know how you feel. I was saying that what US students know of that region is mostly gloom and doom...makes us naturally uneasy.<br><br>In reply to: <a href="https://x.com/tech4j/status/2133793399" rel="noopener noreferrer" target="_blank">@tech4j</a> <span class="status">2133793399</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2133695339',
    created: 1244826242000,
    type: 'post',
    text: 'I\'ve held true to my word. If there was an OS independent way to buy digital music, I would use it. Thanks Amazon for stepping up.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2132864996',
    created: 1244822351000,
    type: 'post',
    text: 'Are we allowed to post our #javaone slides on our blogs?',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '2132603524',
    created: 1244821108000,
    type: 'post',
    text: 'During J1, wife informed me that desktop killed the graphics card (again). No graphics card = dead computer bc wireless connects on login.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2122366133',
    created: 1244755599000,
    type: 'post',
    text: 'Would a dance song from 1992 be categorized as "Classic Dance"?',
    likes: 0,
    retweets: 0
  },
  {
    id: '2120828186',
    created: 1244747896000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Yes, welcome <a class="mention" href="https://x.com/aschwart" rel="noopener noreferrer" target="_blank">@aschwart</a>! We\'ve been waiting for you (evil laugh).<br><br>In reply to: <a href="https://x.com/jasondlee/status/2120450112" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">2120450112</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2120345532',
    created: 1244745479000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> Sure, it\'s just a plugin for the IRC client. So I presume I would need a plugin on my G1 chat client. It\'s got to be out there.<br><br>In reply to: <a href="https://x.com/rruss/status/2119780172" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">2119780172</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2120336369',
    created: 1244745430000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> Hahaha! I wish. Either that or they are making space for DI in Action ;)<br><br>In reply to: <a href="https://x.com/dhanji/status/2120172673" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">2120172673</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2119532366',
    created: 1244741371000,
    type: 'post',
    text: 'Btw, this wasn\'t the only shelf that looked like this. Otherwise, the picture would be misleading.',
    photos: ['<div class="entry"><img class="photo" src="media/2119532366.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '2119526557',
    created: 1244741341000,
    type: 'post',
    text: 'This picture was taken in the Borders store in Columbia, MD.',
    photos: ['<div class="entry"><img class="photo" src="media/2119532366.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '2118124651',
    created: 1244734342000,
    type: 'post',
    text: 'Here\'s evidence of Border\'s trimming their inventory.',
    photos: ['<div class="entry"><img class="photo" src="media/2119532366.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '2117748479',
    created: 1244732429000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/richsharples" rel="noopener noreferrer" target="_blank">@richsharples</a> really!? I was just thinking about making the switch back to Ubuntu, but you may have given me reason to try Fedora.<br><br>In reply to: <a href="https://x.com/richsharples/status/2116501977" rel="noopener noreferrer" target="_blank">@richsharples</a> <span class="status">2116501977</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2112723234',
    created: 1244692085000,
    type: 'post',
    text: 'I just pushed out the 0.6.4 release of maven-cli-plugin to the JBoss Maven repository. No new features, but now on an official release.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2112257592',
    created: 1244689457000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/apemberton" rel="noopener noreferrer" target="_blank">@apemberton</a> Yep, that\'s all the indication you get from Eclipse. Couldn\'t it freakin\' tell me that the source code doesn\'t match?<br><br>In reply to: <a href="https://x.com/apemberton/status/2112228934" rel="noopener noreferrer" target="_blank">@apemberton</a> <span class="status">2112228934</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2112240407',
    created: 1244689362000,
    type: 'post',
    text: 'A referendum vote on upcoming MD ballot puts traffic cameras at work/school zones. They have cameras, we have street signs. Is that fair?',
    likes: 0,
    retweets: 0
  },
  {
    id: '2111781521',
    created: 1244686868000,
    type: 'post',
    text: 'It\'s been a very twitter-focused day for me. Listened to Tim O\'Reilly discuss, had discusssion with wife and now reading Time article.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2111241597',
    created: 1244683939000,
    type: 'post',
    text: 'Wow, I\'m in my local Borders and the are *significantly* less books here. Whole shelves are empty; Java section is lean. No Seam in Action.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2109364070',
    created: 1244673352000,
    type: 'post',
    text: 'I must admit I have no idea what this FriendFeed is all about. Sounds like an aggregator of status updates.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2109314512',
    created: 1244673076000,
    type: 'post',
    text: 'There was a demo along those lines at JavaOne where your address book was established dynamically based on interaction.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2109291458',
    created: 1244672950000,
    type: 'post',
    text: 'Tim predicts the the whole friend model will die. Instead, it will be based on following. The hard link is irrelevant.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2109277917',
    created: 1244672873000,
    type: 'post',
    text: 'Discovered twit.tv today. Listened to Jono Bacon and Tim O\'Reilly. Jono gave nice overview of Ubunut. Tim challenging what open means.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2109213636',
    created: 1244672515000,
    type: 'post',
    text: 'What\'s the best way to export/archive your twitter history? I\'ve got lots of notes to myself that I want to take offline.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2108775140',
    created: 1244670178000,
    type: 'post',
    text: 'Duh, I was debugging a server library and the old version was still deployed. The IDE doesn\'t exactly help you figure that out.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2108554359',
    created: 1244669031000,
    type: 'post',
    text: 'Argh! I can\'t get the debugger to catch on a breakpoint. WTF??? Both Eclipse and NetBeans #debugfail.',
    likes: 0,
    retweets: 0,
    tags: ['debugfail']
  },
  {
    id: '2107245479',
    created: 1244662396000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> Yes, you are correct. My left ear is doing just fine ;) But I was nervous that I was going to intercept a fist from Mr. Brock.<br><br>In reply to: <a href="https://x.com/dhanji/status/2106994526" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">2106994526</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2098872684',
    created: 1244604896000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/edburns" rel="noopener noreferrer" target="_blank">@edburns</a> At the beginning of the EG get together/meeting at the Sun offices. Andy gave the JSF 2 status, followed on by JSP 2 and Servlet 3.<br><br>In reply to: <a href="https://x.com/edburns/status/2098373926" rel="noopener noreferrer" target="_blank">@edburns</a> <span class="status">2098373926</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2098846550',
    created: 1244604751000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> Cool man. I\'m just trying to mediate the peace. I\'m still partially deaf in one ear from the debate, though it was entertaining ;)<br><br>In reply to: <a href="https://x.com/dhanji/status/2096775896" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">2096775896</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2095483140',
    created: 1244586777000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremyg484" rel="noopener noreferrer" target="_blank">@jeremyg484</a> No kidding! I was worried I would run out of steam before the end. But it certainly filled the ideas tank for coming year.<br><br>In reply to: <a href="https://x.com/jeremyg484/status/2049015011" rel="noopener noreferrer" target="_blank">@jeremyg484</a> <span class="status">2049015011</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2095466115',
    created: 1244586687000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremyg484" rel="noopener noreferrer" target="_blank">@jeremyg484</a> Andy did a great job at itemizing the features in JSF 2.0. Amazing how long the list is. I\'ll hit him up for meeting notes.<br><br>In reply to: <a href="https://x.com/jeremyg484/status/2047808265" rel="noopener noreferrer" target="_blank">@jeremyg484</a> <span class="status">2047808265</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2095248103',
    created: 1244585518000,
    type: 'post',
    text: 'Google recently announced their alternative to YSlow for Firebug called Page Speed. The more the merrier. <a href="http://google-code-updates.blogspot.com/2009/06/introducing-page-speed.html" rel="noopener noreferrer" target="_blank">google-code-updates.blogspot.com/2009/06/introducing-page-speed.html</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2094595997',
    created: 1244582139000,
    type: 'post',
    text: 'More progress from JCP discussions. It appears there will be public message boards that are readable without a login. +1 for openness.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2094165218',
    created: 1244579909000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/scottdavis99" rel="noopener noreferrer" target="_blank">@scottdavis99</a> I envision a future where carriers and phones are no longer linked. You choose a carrier for service, period.<br><br>In reply to: <a href="https://x.com/scottdavis99/status/2094124839" rel="noopener noreferrer" target="_blank">@scottdavis99</a> <span class="status">2094124839</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2094144245',
    created: 1244579799000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> I\'m glad that you recognize that there is room to innovate in DI. I wish others would take the time to realize the same.<br><br>In reply to: <a href="https://x.com/jeremynorris/status/2049051223" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">2049051223</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2094133554',
    created: 1244579743000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rainwebs" rel="noopener noreferrer" target="_blank">@rainwebs</a> No offense, but this article is naive. If you even browse JSR-299, you will quickly see that there is significant innovation.<br><br>In reply to: <a href="https://x.com/rainwebs/status/2070105706" rel="noopener noreferrer" target="_blank">@rainwebs</a> <span class="status">2070105706</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2094019236',
    created: 1244579146000,
    type: 'post',
    text: 'If you have an iPhone, stop reading. If you have some other phone, like a CrackBerry, here\'s the 1st-hand experience you need. Get the G1!',
    likes: 0,
    retweets: 0
  },
  {
    id: '2093766615',
    created: 1244577835000,
    type: 'post',
    text: 'Btw, nothing personal. <a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> is a very nice guy and is clearly doing great work at Google on Wave. But promises are promises ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '2093744517',
    created: 1244577722000,
    type: 'post',
    text: 'I\'m following whether <a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> is going to remove himself from the JSR-316 spec like he promised Gavin. There were witnesses.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2093621683',
    created: 1244577081000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> This is the real deal this time. Besides, I wasn\'t there before to lean on them. So now you have my word that I\'ll hold them to it.<br><br>In reply to: <a href="https://x.com/kito99/status/2090091174" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">2090091174</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2093456037',
    created: 1244576206000,
    type: 'post',
    text: 'Linux Mag asks "Is Android The Perfect Mobile Software Platform?" <a href="http://www.linux-mag.com/id/7355" rel="noopener noreferrer" target="_blank">www.linux-mag.com/id/7355</a> I vote yes ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '2086517280',
    created: 1244526908000,
    type: 'post',
    text: 'JCP is working on a new website and the focus is openness. I\'m leaning on them to hold true to the principle.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2081859846',
    created: 1244498548000,
    type: 'post',
    text: 'Slides for all #javaone technical sessions are available as well. <a href="http://developers.sun.com/learning/javaoneonline/j1online.jsp?track=javaee&amp;yr=2009" rel="noopener noreferrer" target="_blank">developers.sun.com/learning/javaoneonline/j1online.jsp?track=javaee&amp;yr=2009</a>',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '2081845680',
    created: 1244498472000,
    type: 'post',
    text: 'My talk on conversations and page flows for JSF at #javaone is now online: <a href="http://developers.sun.com/learning/javaoneonline/sessions/2009/pdf/TS-5045.pdf" rel="noopener noreferrer" target="_blank">developers.sun.com/learning/javaoneonline/sessions/2009/pdf/TS-5045.pdf</a> Requires free SDN account.',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '2081572960',
    created: 1244496981000,
    type: 'post',
    text: 'For a fairly comprehensive tech conference schedule, check out <a href="http://devtownstation.com" rel="noopener noreferrer" target="_blank">devtownstation.com</a> #devoxx was just added.',
    likes: 0,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '2080980434',
    created: 1244493913000,
    type: 'post',
    text: '...and here is the corresponding JBoss corporate logo: <a href="http://www.jboss.com/images/common/jbosscorp_logo.png" rel="noopener noreferrer" target="_blank">www.jboss.com/images/common/jbosscorp_logo.png</a> I like the red ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '2080946136',
    created: 1244493738000,
    type: 'post',
    text: 'So for those of you who weren\'t at JavaOne and didn\'t read elsewhere, JBoss is now "by Red Hat" instead of "a division of Red Hat" OK, sure.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2080244027',
    created: 1244490141000,
    type: 'post',
    text: 'My wife\'s monitor attached to laptop is flickering due to ground loop interference. Stops when laptop is unplugged. Any cheap solutions?',
    likes: 0,
    retweets: 0
  },
  {
    id: '2078783413',
    created: 1244483255000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maeste" rel="noopener noreferrer" target="_blank">@maeste</a> I went with Twidroid. Working great so far. Wish it were open source though so I could meddle with it.<br><br>In reply to: <a href="https://x.com/maeste/status/2073053883" rel="noopener noreferrer" target="_blank">@maeste</a> <span class="status">2073053883</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2078753631',
    created: 1244483111000,
    type: 'post',
    text: 'I really enjoyed the Parc 55 hotel in SF...EXCEPT for the internet cost. $12.95/24hrs. I loath being ripped off for internet. Grrr.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2078734620',
    created: 1244483017000,
    type: 'post',
    text: 'I wish the Android market place would list the license of an application rather than just "FREE". I\'m still all for donating.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2078592088',
    created: 1244482313000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> Now you are talking. Pie. Mmmmm.<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/2060123517" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">2060123517</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2072337119',
    created: 1244432999000,
    type: 'post',
    text: 'Before I left for SF we caught Slumdog Millnaire (yes you have to say it that way). Must see, but NOT while eating.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2072320704',
    created: 1244432888000,
    type: 'post',
    text: 'My wife and I just watched Bolt and the delusional hamster. Super Rhino, wacha!',
    likes: 0,
    retweets: 0
  },
  {
    id: '2072305736',
    created: 1244432785000,
    type: 'post',
    text: 'Now I\'m trying to find my tweet voice for my g1. No obvious winner yet.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2069249658',
    created: 1244413919000,
    type: 'post',
    text: 'I didn\'t wait for the Google Ion because apparently it\'s not an Android phone...just supports Android apps. Plus, I want the real keyboard.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2069236721',
    created: 1244413835000,
    type: 'post',
    text: 'Aside from spending a lot of money (for good purpose), I am taking it easy this week. I was very happy to see/spend time my wife again ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '2069221216',
    created: 1244413739000,
    type: 'post',
    text: 'I made a gigantic leap toward supporting open source and small(er) business today. Got a system76.com desktop and the G1 phone. Linux baby!',
    likes: 0,
    retweets: 0
  },
  {
    id: '2069210668',
    created: 1244413673000,
    type: 'post',
    text: 'Just got the G1 phone from t-mobile. Why didn\'t you people slap me for not having this phone already! Sweet! Sweet! Sweet!',
    likes: 0,
    retweets: 0
  },
  {
    id: '2058496716',
    created: 1244325810000,
    type: 'post',
    text: 'I\'ve made up my mind. I\'m def getting a gphone. Question is, should I wait for the next version? Guess it depends on the timeframe.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2055352017',
    created: 1244304575000,
    type: 'post',
    text: 'I met a fellow cornellian on the BART and first time #javaone attendee. SCott from JavaRanch.',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '2055335615',
    created: 1244304467000,
    type: 'post',
    text: 'It\'s a freakin miracle that I made my flight. I don\'t even want to think about had I not made it. BART slower than I expected.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2051946226',
    created: 1244268794000,
    type: 'post',
    text: 'Oh my goodness paying in groups is soooooooo painful. Why can\'t we figure out a solution for this problem.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2051501490',
    created: 1244265073000,
    type: 'post',
    text: 'Gavin is in full force at the dinner table. It\'s hard to tell who is in the right, but it is funny as hell.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2050315753',
    created: 1244251628000,
    type: 'post',
    text: 'Just had a long Seam 3 meeting w/ Gavin and Alex. I will be uploading a mindmap soon. Now for some food.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2050301530',
    created: 1244251537000,
    type: 'post',
    text: 'One #javaone speaking experience is in the bag and it went flawlessly! Apparently I am famous in Asia. I\'ll take what I can get.',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '2045229103',
    created: 1244222727000,
    type: 'post',
    text: 'I\'m at the Sun office awaiting the start of a JSF meeting. Very nice facility.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2044596255',
    created: 1244219680000,
    type: 'post',
    text: 'I\'m all prepped and ready for my talk from 2:50 - 3:50 in Hall 134 on conversations and page flows for JSF. #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '2044228410',
    created: 1244217798000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> If JNDI is the solution, the correct way to do it is use the new global JNDI prefixes: java:module, java:global, etc.<br><br>In reply to: <a href="https://x.com/JohnAment/status/2041511645" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">2041511645</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2040814751',
    created: 1244191640000,
    type: 'post',
    text: 'Just read this fantastic entry on giving a great talk: <a href="http://www.barryeisler.com/forum/index.php?topic=2569.0" rel="noopener noreferrer" target="_blank">www.barryeisler.com/forum/index.php?topic=2569.0</a> You want to persuade audience to research further.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2040273463',
    created: 1244185624000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> Yes, but you have to be careful because depending on what module the class is in, you may not get the manager you expect.<br><br>In reply to: <a href="https://x.com/JohnAment/status/2036364188" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">2036364188</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2040252436',
    created: 1244185420000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> I hung out for a while with your brother (though we didn\'t formally get introduced).<br><br>In reply to: <a href="https://x.com/jasondlee/status/2039621890" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">2039621890</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2039582617',
    created: 1244179123000,
    type: 'post',
    text: 'Took brief nap and almost missed my own party at #javaone. Again had some good talks in which ideas emerged. Info forthcoming.',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '2036152143',
    created: 1244158819000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> Plus, we may not want to rely on JNDI as the bridge to get the bean manager in case it is absent.<br><br>In reply to: <a href="https://x.com/JohnAment/status/2035292695" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">2035292695</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2036147871',
    created: 1244158795000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> The question is not how to use JNDI, but how to ensure you get back the right manager for the current module (EAR, EJB-JAR).<br><br>In reply to: <a href="https://x.com/JohnAment/status/2035292695" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">2035292695</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2036137839',
    created: 1244158736000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BasZero" rel="noopener noreferrer" target="_blank">@BasZero</a> Gavin is really the one that has to address 299 vs 330. I\'ve been hesitant to get involved w/ the debate.<br><br>In reply to: <a href="https://x.com/geekZeroZH/status/2035766812" rel="noopener noreferrer" target="_blank">@geekZeroZH</a> <span class="status">2035766812</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2034548981',
    created: 1244149982000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> showed off Seam 3 with JSF 2 bean validation AND Maven cli plugin! #javaone',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '2032532472',
    created: 1244139821000,
    type: 'post',
    text: 'Last night Gavin and I were discussing how to get \'into\' the 299 managed env from a non-managed class (not via EL) Tough problem.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2032499663',
    created: 1244139645000,
    type: 'post',
    text: 'Hmm is it possible to package JSF composite components? I need to see what the story with that is.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2032398184',
    created: 1244139122000,
    type: 'post',
    text: 'Super slow getting out of the gate this morning. In Kito\'s Facelets talk #javaone. He is pimping JBoss Tools visual editor again.',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '2027117015',
    created: 1244101716000,
    type: 'post',
    text: 'Part of JSF 2 EG and Mojarra developers at #javaone',
    photos: ['<div class="entry"><img class="photo" src="media/2027117015.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '2026831394',
    created: 1244098818000,
    type: 'post',
    text: 'A number of folks have wondered why all the rush to finish JSF 2 when Java EE 6 ~ Sept. For one, JSF is having its day(s) at #javaone.',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '2026735289',
    created: 1244097882000,
    type: 'post',
    text: 'I regret that I missed the JSF 2 BOF about Ajax. You just lose track of time at this conference. There need to be session start alarms ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '2026726464',
    created: 1244097797000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BasZero" rel="noopener noreferrer" target="_blank">@BasZero</a> Gavin just can\'t go on record saying it, but I can tell you that unless the sky falls, JSR-299 will be in Java EE 6.<br><br>In reply to: <a href="https://x.com/geekZeroZH/status/2025009487" rel="noopener noreferrer" target="_blank">@geekZeroZH</a> <span class="status">2025009487</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '2026675347',
    created: 1244097313000,
    type: 'post',
    text: 'I haven\'t gotten many videos. I have to admit that while I am not camera shy, I am a shy cameraman. Hmm, have two days to break out.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2026670760',
    created: 1244097272000,
    type: 'post',
    text: 'I must admit that I definitely had book signing #fail. Thankfully, Felipe Gaucho bought a book and I gave him an inspiring note ;)',
    likes: 0,
    retweets: 0,
    tags: ['fail']
  },
  {
    id: '2026661154',
    created: 1244097184000,
    type: 'post',
    text: 'Headed over to the Google party and chatted briefly with the Spring Web Flow guys (Keith and Jeremy) and met Nitin (DZone).',
    likes: 0,
    retweets: 0
  },
  {
    id: '2026655210',
    created: 1244097127000,
    type: 'post',
    text: 'Had long talks with Greg Hinkle (JOPR) about this thread <a href="https://mojavelinux.com/blog/archives/2006/06/hibernate_get_out_of_my_pojo/" rel="noopener noreferrer" target="_blank">mojavelinux.com/blog/archives/2006/06/hibernate_get_out_of_my_pojo/</a>, Seam 3, RichFaces, and admin consoles.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2026584538',
    created: 1244096460000,
    type: 'post',
    text: 'Had dinner with <a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> and Gavin King (twitter holdout). Sweated a bit through questions from Gavin.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2026576480',
    created: 1244096388000,
    type: 'post',
    text: 'It\'s torture knowing that there are people near you that you\'d like to hang out with, but you have to bump into them by chance.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2022515931',
    created: 1244070437000,
    type: 'post',
    text: 'Heading to the jsr-299 session at #javaone to hear Gavin talk for the first time.',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '2021448397',
    created: 1244064410000,
    type: 'post',
    text: 'I was hanging at the JBoss booth and <a class="mention" href="https://x.com/aaronwalker" rel="noopener noreferrer" target="_blank">@aaronwalker</a> was impressing attendees with his diverse knowledge of JBoss projects. I was all ears.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2021390708',
    created: 1244064096000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> Indeed it has. I can attest to that. Eclipse autobuillder stills bothers me, but at least it\'s all functional.<br><br>In reply to: <a href="https://x.com/jeremynorris/status/2018548290" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">2018548290</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2021378420',
    created: 1244064030000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> You need to use the maven war overlay, standard part of war plugin.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/2018205752" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">2018205752</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2021366737',
    created: 1244063967000,
    type: 'post',
    text: 'Here\'s a shot of me doing my presentation at the JBoss booth at #javaone thanks <a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a>!',
    photos: ['<div class="entry"><img class="photo" src="media/2021366737.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '2021304693',
    created: 1244063635000,
    type: 'post',
    text: 'Catch me at the Seam in Action book signing today from 3:00 - 3:30 at the #javaone book store.',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '2021223356',
    created: 1244063206000,
    type: 'post',
    text: 'Just got done with speaker training at #javaone. Nice service to offer and I got some great feedback.',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '2018195719',
    created: 1244046914000,
    type: 'post',
    text: 'I\'m off to do my talk on Seam Security in the #javaone pavilion. 10:30 at the JBoss booth. Be there are be insecure (haha, j/k).',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '2018188333',
    created: 1244046873000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/derekhollis" rel="noopener noreferrer" target="_blank">@derekhollis</a> Sometimes I\'m not sure people agree on what it is. My vision, on the other hand, is quite clear. Open.<br><br>In reply to: @derekhollis <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2018175070',
    created: 1244046797000,
    type: 'post',
    text: 'Emmanuel Bernard will be demoing a Seam 3 application in his Bean Validation talk at #javaone on Thurs.',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '2017382635',
    created: 1244042284000,
    type: 'post',
    text: 'The black #JavaOne 2009 backpacks are back in black. Check them out in this #j1video <a href="https://youtu.be/krSCY_yXF8Y" rel="noopener noreferrer" target="_blank">youtu.be/krSCY_yXF8Y</a>',
    likes: 0,
    retweets: 0,
    tags: ['javaone', 'j1video']
  },
  {
    id: '2014642980',
    created: 1244020346000,
    type: 'post',
    text: 'For you Linux fans out there, Click and Run for Linspire turned Xandros is built with Seam (and sexy!). See <a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> for inside scoop.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2013896373',
    created: 1244011972000,
    type: 'post',
    text: 'I finally managed to get my first video on YouTube. Beware, I might point and shoot: <a href="https://youtu.be/OyAsS-hnAgY" rel="noopener noreferrer" target="_blank">youtu.be/OyAsS-hnAgY</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2013370358',
    created: 1244007053000,
    type: 'post',
    text: 'Dilemma: haven\'t had dinner vs booth talk not done for tomorrow. Hmm, time to splurge at hotel?',
    likes: 0,
    retweets: 0
  },
  {
    id: '2013365848',
    created: 1244007016000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> There was a NullPointerException when using the flash scope. I fixed it long ago, but now I have commit access ;)<br><br>In reply to: <a href="https://x.com/lightguardjp/status/2010954353" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">2010954353</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2013286750',
    created: 1244006353000,
    type: 'post',
    text: 'I used the analogy that EG group members are like senators. We must represent the voice of the people and find ways for them to be heard.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2013253145',
    created: 1244006085000,
    type: 'post',
    text: 'Just left the jcp party at #javaone Had good conversations including a sit down with Ed Burns. I continue to lobby for open jcp lists w/ ...',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '2011543352',
    created: 1243995064000,
    type: 'post',
    text: 'It was also the source of my slowness problem. Resolved.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2011539921',
    created: 1243995045000,
    type: 'post',
    text: 'The gnome session manager (in RHEL 5.2) is the most unintuitive piece of crap. It makes no sense to me and starts crap I don\'t want.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2011402015',
    created: 1243994287000,
    type: 'post',
    text: 'I don\'t what it with my computers and #javaone. They always seem to know when it\'s time to fail. My computer is crawling for no reason.',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '2011333570',
    created: 1243993885000,
    type: 'post',
    text: 'I retreated back to my room and quite literally just passed out from exhaustion. Just hit a wall. But now I\'m fresh!',
    likes: 0,
    retweets: 0
  },
  {
    id: '2010119834',
    created: 1243986639000,
    type: 'post',
    text: 'When the wireless internet was flaky in the GAE session, it was interesting to see even Google engineers hit google.com to test the network.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2009749288',
    created: 1243984375000,
    type: 'post',
    text: 'Participated in my first ever interactive demo in the GAE talk. We played a translation game. I was dreadful.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2009624129',
    created: 1243983621000,
    type: 'post',
    text: 'Had to hot patch Mojarra for <a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> to get his demo working. Don\'t worry, I\'ll get the fix checked in officially very soon ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '2009616536',
    created: 1243983581000,
    type: 'post',
    text: 'Met David Blevins, co-founder of the OpenEJB project and a founder of Apache Geronimo, in speaker lounge.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2009513947',
    created: 1243982960000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> GAE is not for deploying a portable Java EE app so much as writing a webapp in Java to leverage Google infra (gmail, accts).<br><br>In reply to: <a href="https://x.com/matthewmccull/status/2009408285" rel="noopener noreferrer" target="_blank">@matthewmccull</a> <span class="status">2009408285</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2009487754',
    created: 1243982803000,
    type: 'post',
    text: '"Google app engine will always be free to get started" Okay, so that sounds like I can be a stupid new user for a while for free ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '2009475470',
    created: 1243982729000,
    type: 'post',
    text: 'Second favorite word: "rules"',
    likes: 0,
    retweets: 0
  },
  {
    id: '2009459130',
    created: 1243982632000,
    type: 'post',
    text: 'Google App Engine\'s favorite word: "custom"',
    likes: 0,
    retweets: 0
  },
  {
    id: '2009398156',
    created: 1243982266000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> The JBoss party is Thurs 7 - 10pm. If we cross paths and you don\'t have an invite yet, I have one in my pocket for you ;)<br><br>In reply to: <a href="https://x.com/jasondlee/status/2009371638" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">2009371638</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2009351979',
    created: 1243981994000,
    type: 'post',
    text: 'If you want to attend the JBoss Party, and didn\'t sign up, be sure to visit the JBoss booth, else get turned away at the door.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2009342676',
    created: 1243981940000,
    type: 'post',
    text: 'I\'m checking out the Google App Engine talk at #javaone I want to see how they spin the "we don\'t really support Java EE" point.',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '2007620936',
    created: 1243972471000,
    type: 'post',
    text: 'I\'m in the clojure talk at #javaone A certain friend will be happy :)',
    likes: 1,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '2006635348',
    created: 1243967250000,
    type: 'post',
    text: 'I did not like the premature announcement that Java EE 6 is available. Misleading. The platform is not final.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2006597048',
    created: 1243967047000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Exactly. That\'s what I was thinking. It didn\'t provide any reassurance.<br><br>In reply to: <a href="https://x.com/kito99/status/2005967145" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">2005967145</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2006560872',
    created: 1243966854000,
    type: 'post',
    text: 'I\'m curious if you need one of those obtuse and foreign jndi.properties files. That\'s were things get scary to newcomers.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2006544962',
    created: 1243966770000,
    type: 'post',
    text: 'EJBContainer container = EJBContainer.createEJBContainer(); container.getContext().lookup("java:global/MyBean"); for testing',
    likes: 0,
    retweets: 0
  },
  {
    id: '2006469806',
    created: 1243966371000,
    type: 'post',
    text: 'Great. They aren\'t just defining a JNDI naming standard. They are widening visibility of resources so you don\'t need resource references.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2006426732',
    created: 1243966148000,
    type: 'post',
    text: 'I duked into the EJB 3.1 session being given by Ken Saks. Currently talking about JNDI names, a key issue for me.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2006158775',
    created: 1243964690000,
    type: 'post',
    text: 'I\'m looking at the line to get into the EJB 3.1 talk. Reminds me of my first #javaone session years ago about EJB 3.0.',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '2005656416',
    created: 1243962030000,
    type: 'post',
    text: 'Interesting to see Jonathan Schwartz as the MC at #javaone A bit rough and quirky.',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '2004894995',
    created: 1243958068000,
    type: 'post',
    text: 'The general session at #javaone is sufficiently packed. Much larger scale than yesterday.',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '2004792280',
    created: 1243957503000,
    type: 'post',
    text: 'I\'m hurtin\' for certain. Too hard on the first day of #javaone No time to stop at Starbucks, no collecting 200 calories.',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '2001544664',
    created: 1243930458000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> snapped a shot of me with @AdamBiem and Reza Rahman after my GlassFish talk <a href="https://www.flickr.com/photos/22798066@N04/3588325252/" rel="noopener noreferrer" target="_blank">www.flickr.com/photos/22798066@N04/3588325252/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2001537940',
    created: 1243930375000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jonobacon" rel="noopener noreferrer" target="_blank">@jonobacon</a> Great meeting you! Thanks for the great work. I can\'t wait to read your book. In fact, I\'m glad I know about it now.<br><br>In reply to: <a href="https://x.com/jonobacon/status/2000880819" rel="noopener noreferrer" target="_blank">@jonobacon</a> <span class="status">2000880819</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '2001529357',
    created: 1243930268000,
    type: 'post',
    text: 'Yeah! The schema export bug in Hibernate that would create duplicate foreign keys is fixed! <a href="https://hibernate.atlassian.net/browse/HHH-3532" rel="noopener noreferrer" target="_blank">hibernate.atlassian.net/browse/HHH-3532</a> Finally!',
    likes: 0,
    retweets: 0
  },
  {
    id: '2001390259',
    created: 1243928628000,
    type: 'post',
    text: 'I had dinner and hung out with the JSF teams all night. Mojarra and ADF Faces. It was great conversation.',
    likes: 0,
    retweets: 0
  },
  {
    id: '2001368464',
    created: 1243928371000,
    type: 'post',
    text: 'Second time in two weeks a restaurant has found my walet. I need to count my blessings and keep my walet in my front pocket.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1997837064',
    created: 1243902750000,
    type: 'post',
    text: '"Free software doesn\'t have to be partisan." Amen.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1997816609',
    created: 1243902625000,
    type: 'post',
    text: 'Btw, Jono is speaking about building belonging (community), something important to me and critical to my role on the Seam team.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1997639862',
    created: 1243901523000,
    type: 'post',
    text: 'I\'m having dreadful luck crossing paths with people I know or want to meet at #communityone. Sometimes you just aren\'t in the right place.',
    likes: 0,
    retweets: 0,
    tags: ['communityone']
  },
  {
    id: '1997609442',
    created: 1243901333000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/scottdavis99" rel="noopener noreferrer" target="_blank">@scottdavis99</a> That\'s a daily struggle for me, trying to rationalize whether I am having lunch or dinner.<br><br>In reply to: <a href="https://x.com/scottdavis99/status/1996878226" rel="noopener noreferrer" target="_blank">@scottdavis99</a> <span class="status">1996878226</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1997597554',
    created: 1243901258000,
    type: 'post',
    text: 'I discovered a hidden gem at #communityone - Jono Bacon from Ubuntu. I\'ve been a long time listener of LUG radio.',
    likes: 0,
    retweets: 0,
    tags: ['communityone']
  },
  {
    id: '1997584572',
    created: 1243901176000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Hey, I put one on the board for Linux (RHEL).<br><br>In reply to: <a href="https://x.com/jasondlee/status/1997549688" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">1997549688</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1996198329',
    created: 1243892487000,
    type: 'post',
    text: 'I was quite impressed with the OpenSolaris discussion. I think a lot of the issued they addressed needed addressing.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1996165337',
    created: 1243892297000,
    type: 'post',
    text: 'I\'ve realized the key for me in preparing a talk is getting a good lead in. Once I\'m up and running, I just cruise.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1996154851',
    created: 1243892235000,
    type: 'post',
    text: 'I just finished my talk about running Seam on GlassFish. I\'m psyched with how it went, though I had no trouble flying through 50 minutes ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '1996142988',
    created: 1243892166000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> Thanks for attending. I didn\'t get a chance to say "hi" afterwards, but I\'m sure I\'ll catch you around.<br><br>In reply to: <a href="https://x.com/jeremynorris/status/1994756834" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">1994756834</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1996125510',
    created: 1243892066000,
    type: 'post',
    text: 'Just met <a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> in person for the first time. Good to meet you. I didn\'t recognize you though from your Twitter picture.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1992852733',
    created: 1243873541000,
    type: 'post',
    text: 'Sun is announcing their cloud at #javaone This is a good thing because you know they will support Java EE right.',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '1992757521',
    created: 1243872983000,
    type: 'post',
    text: 'As if twittering wasn\'t consuming enough, now I\'m trying to record video too. I need to remember to look up.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1988800638',
    created: 1243837486000,
    type: 'post',
    text: 'When in Rome, do as the Romans do. I\'m in SF. So, I\'m eating Ghirardelli chocolate. Hey, I needed a midnight snack.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1988786689',
    created: 1243837340000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> It\'s $5.35 now, but still a bargain. Last year, our Russian mafia SuperShuttle driver nearly kill us w/ his is driving.<br><br>In reply to: <a href="https://x.com/aalmiray/status/1988010628" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">1988010628</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1988765289',
    created: 1243837115000,
    type: 'post',
    text: 'Made sick time coming across the states. I think we touched down 45 minutes early. Comfortable and fast. Virgin knows the meaning of happy.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1987929428',
    created: 1243829723000,
    type: 'post',
    text: 'For the first time I am taking BART from SFO to downtown. Never thought of it before.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1984535615',
    created: 1243808276000,
    type: 'post',
    text: 'I freakin\' love flying in style w/ Virgin American. Best feature: leather seats no doubt. See you on the west coast!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1983960062',
    created: 1243804206000,
    type: 'post',
    text: 'Just picked up a Flip Mino HD for #javaone Smile, you\'re going to be on youtube :)',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '1982830741',
    created: 1243796363000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Profile it and see if there is a single culprit or whether it is just generally busy. Could be an impl problem.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/1980938913" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">1980938913</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1982166814',
    created: 1243791585000,
    type: 'post',
    text: 'I\'ll be at the #JCP Party at #JavaOne - <a href="http://jcpparty.eventbrite.com" rel="noopener noreferrer" target="_blank">jcpparty.eventbrite.com</a> Let\'s celebrate all the long threads ;)',
    likes: 0,
    retweets: 0,
    tags: ['jcp', 'javaone']
  },
  {
    id: '1977731441',
    created: 1243744505000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> And the post is about your life ;) Cheers to life.<br><br>In reply to: <a href="https://x.com/mraible/status/1974087824" rel="noopener noreferrer" target="_blank">@mraible</a> <span class="status">1974087824</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1977044988',
    created: 1243738650000,
    type: 'post',
    text: 'Amazon has revamped Author Connect and has begun rolling out Author Central. It has a much cleaner admin interface.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1976531137',
    created: 1243732937000,
    type: 'post',
    text: 'Summer has definitely arrived. After mucho rain I finally got in a round at the driving range, classic rock in the background. Nice.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1973575935',
    created: 1243707915000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RobertFischer" rel="noopener noreferrer" target="_blank">@RobertFischer</a> I haven\'t yet. Any info out there on the Java Bloggers Meetup? Anyway, I think it should be renamed. Twitter is king.<br><br>In reply to: <a href="https://x.com/RobertFischer/status/1968443444" rel="noopener noreferrer" target="_blank">@RobertFischer</a> <span class="status">1968443444</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1968407988',
    created: 1243657351000,
    type: 'post',
    text: 'One of the standing events I always make it to at #javaone is the Java Bloggers Meetup. How about a Java Tweeters meetup this year?',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '1964971817',
    created: 1243634811000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> I hear ya.<br><br>In reply to: <a href="https://x.com/jasondlee/status/1964925688" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">1964925688</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1964916165',
    created: 1243634478000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> My philosophy is: love the music, not the artist. They\'ll only let you down. Still, you may decide not to send money their way.<br><br>In reply to: <a href="https://x.com/jasondlee/status/1964164831" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">1964164831</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1964059656',
    created: 1243629585000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> No kidding, I didn\'t realize you were so close. It was setting off car alarms in my neighborhood.<br><br>In reply to: <a href="https://x.com/metacosm/status/1962886971" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">1962886971</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1962870368',
    created: 1243622993000,
    type: 'post',
    text: 'All hell is breaking loose in the Maryland sky. As folks used to say when I was growing up, "the angels are bowling". Yeah, drunk angels.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1962493870',
    created: 1243620473000,
    type: 'post',
    text: 'No power or wifi for you. How true this story is when applied to the #javaone conference: <a href="http://www.smartbitchestrashybooks.com/index.php/weblog/comments/conference-wishlist/" rel="noopener noreferrer" target="_blank">www.smartbitchestrashybooks.com/index.php/weblog/comments/conference-wishlist/</a>',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '1956513941',
    created: 1243573355000,
    type: 'post',
    text: 'I\'m starting to get pumped about #javaone. I just love being in SF and getting my fill of tech talk. So many people to see ;)',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '1956022327',
    created: 1243569751000,
    type: 'post',
    text: 'Finally got my speaking badge up on mojavelinux.com. Better late than never I guess. Had to create a little hybrid for both conferences.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1955753739',
    created: 1243568058000,
    type: 'post',
    text: 'I\'ll be doing a book signing at #javaone Wed, Jun 03 03:00-03:30pm. Right before the JSR-299 talk (starts at 4:10).',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '1947683533',
    created: 1243521134000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Use H2 in TCP mode. Not often used, but extremely powerful and capable.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1947676589',
    created: 1243521093000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> My Seam in Action enthusiasts should know and love H2 by now ;) I brainwashed you all! j/k I introduced you to a powerful tool.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1939437609" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1939437609</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1947448139',
    created: 1243519733000,
    type: 'post',
    text: 'If ever I needed an excuse for upgrading my phone, I now have one. Was embarrassed by my phone\'s feedback while on conf call w/ customer.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1937596873',
    created: 1243443762000,
    type: 'post',
    text: 'Hmm, it appears that is how the m2eclipse plugin is supposed to work. Only, the Maven builder isn\'t processing resources. Argh.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1937413555',
    created: 1243442628000,
    type: 'post',
    text: 'I just lost an hour because Eclipse screwed me. It decided to add the exclusion filter ** to all my resources folders =&gt; bizarre run errors.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1930147226',
    created: 1243385219000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Yep, which is why we have to be willing to break compatibility if it\'s the right thing to do. Resistance to change = obsolete.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/1925651974" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">1925651974</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1920309564',
    created: 1243312222000,
    type: 'post',
    text: 'Honestly, I don\'t know why I can\'t just record a channel for a fixed number of hours like in the old days of VCRs. At least I know it works.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1920305244',
    created: 1243312190000,
    type: 'post',
    text: 'I just don\'t get why DVRs have to be so stupid about dealing with broadcasts that run over. At least one recording a day gets screwed up.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1920297585',
    created: 1243312129000,
    type: 'post',
    text: 'I was confused about the #javaone schedule builder because I didn\'t realize it was shared with the #communityone schedule builder.',
    likes: 0,
    retweets: 0,
    tags: ['javaone', 'communityone']
  },
  {
    id: '1920290918',
    created: 1243312076000,
    type: 'post',
    text: 'And a quick reminder, my #communityone talk about running Seam on GlassFish: S305065 Mon, Jun 01 11:50-12:40.',
    likes: 0,
    retweets: 0,
    tags: ['communityone']
  },
  {
    id: '1920280508',
    created: 1243311993000,
    type: 'post',
    text: 'Finally got #javaone schedule builder to work. Here\'s my conversation &amp; pageflow for JSF talk: TS-5045 Fri, Jun 05 14:50-15:50.',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '1919875388',
    created: 1243309257000,
    type: 'post',
    text: 'The GlassFish TV episode on JSR-299 given by Pete Muir has been published: <a href="http://wikis.sun.com/display/TheAquarium/JCDI+-+JSR299" rel="noopener noreferrer" target="_blank">wikis.sun.com/display/TheAquarium/JCDI+-+JSR299</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1917662068',
    created: 1243294694000,
    type: 'post',
    text: 'JBoss finally made an official statement to the community about the outage earlier in the month: <a href="http://jboss.org" rel="noopener noreferrer" target="_blank">jboss.org</a> (see masthead).',
    likes: 0,
    retweets: 0
  },
  {
    id: '1916576676',
    created: 1243287120000,
    type: 'post',
    text: 'Oh, that hurts. Iced in overtime. No amount of booze can cover up the pain of that loss right now. I can only hope the numbness takes over.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1915062309',
    created: 1243277481000,
    type: 'post',
    text: 'Turn the knife Big Red. This is your moment. Keep up the domination!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1914776259',
    created: 1243275667000,
    type: 'post',
    text: '6-4 Cornell up at the half. Cornell is putting Syracuse into the sleeper hold. If you don\'t have it on, switch to ESPN. Great game!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1913545325',
    created: 1243267580000,
    type: 'post',
    text: 'Bring it on \'cuse! I saw a red sky last night. This is Cornell\'s day. Go Big Red! #lax',
    likes: 0,
    retweets: 0,
    tags: ['lax']
  },
  {
    id: '1907044621',
    created: 1243207799000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rayme" rel="noopener noreferrer" target="_blank">@rayme</a> Awesome. I just had a Chimay Grande Reserve yesterday while watching and celebrating the Cornell victory.<br><br>In reply to: <a href="https://x.com/rayme/status/1906484472" rel="noopener noreferrer" target="_blank">@rayme</a> <span class="status">1906484472</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1905094307',
    created: 1243193239000,
    type: 'post',
    text: 'I got myself so worked up over my talks last week that I gave myself lock jaw (TMJ). Hurts to eat, though recovering today.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1899622077',
    created: 1243136421000,
    type: 'post',
    text: 'For a more tangible update: Cornell 15, Virginia 6. #cornell to play #syracuse in NCAA #lax finals on Memorial Day.',
    likes: 0,
    retweets: 0,
    tags: ['cornell', 'syracuse', 'lax']
  },
  {
    id: '1899604971',
    created: 1243136287000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/trustin" rel="noopener noreferrer" target="_blank">@trustin</a> Building on my last post, I don\'t want a global change. I want to "fix" a given e-mail I\'m reading. Hence the need for a button.<br><br>In reply to: <a href="https://x.com/trustin/status/1899302885" rel="noopener noreferrer" target="_blank">@trustin</a> <span class="status">1899302885</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1899592158',
    created: 1243136189000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> I want to enlarge the e-mail font, but not resize the entire page. Plus, it shouldn\'t affect other e-mails.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1897196458" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1897196458</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1899579268',
    created: 1243136092000,
    type: 'post',
    text: 'I\'ve got a bet going with my sister, who is an alumni of Syracuse. Too bad she\'ll lose! Hahaha.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1899573466',
    created: 1243136049000,
    type: 'post',
    text: 'Woot! Cornell is going to the big show. Made Virginia look like they were sleeping on the field today. Syracuse is toast next! Go Big Red!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1895566611',
    created: 1243104707000,
    type: 'post',
    text: 'Gmail needs to add a "enlarge text" button for those people that like to send HTML e-mails in 10pt.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1895353731',
    created: 1243103087000,
    type: 'post',
    text: 'Go Big Red!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1894785946',
    created: 1243098825000,
    type: 'post',
    text: 'Rooting for my alma mater today in NCAA Lacrosse Final 4. Cornell lost a heart breaker against Duke 2 years ago @ 0:17 in quarterfinals.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1894370990',
    created: 1243095736000,
    type: 'post',
    text: 'Nominate Maven CLI plugin for the SF community choice awards. <a href="http://sourceforge.net/community/cca09/nominate/?project_name=Maven CLI Plugin&amp;project_url=http://wiki.github.com/mrdon/maven-cli-plugin/" rel="noopener noreferrer" target="_blank">sourceforge.net/community/cca09/nominate/?project_name=Maven CLI Plugin&amp;project_url=http://wiki.github.com/mrdon/maven-cli-plugin/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1889805981',
    created: 1243047415000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/apemberton" rel="noopener noreferrer" target="_blank">@apemberton</a> 7 stars. And you know it\'s funny because that\'s what we all wanted to do when we learned the tests were screwed.<br><br>In reply to: <a href="https://x.com/apemberton/status/1877562212" rel="noopener noreferrer" target="_blank">@apemberton</a> <span class="status">1877562212</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1888344805',
    created: 1243037004000,
    type: 'post',
    text: 'I used this video in my talk: <a href="http://www.redhat.com/magazine/008jun05/features/truth_happens/" rel="noopener noreferrer" target="_blank">www.redhat.com/magazine/008jun05/features/truth_happens/</a> It was one of the films I watched during my Red Hat orientation.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1888307938',
    created: 1243036756000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Forgot to mention, the school is Northwood High School in Silver Spring, MD. <a href="http://www.montgomeryschoolsmd.org/schools/northwoodhs/" rel="noopener noreferrer" target="_blank">www.montgomeryschoolsmd.org/schools/northwoodhs/</a><br><br>In reply to: <a href="https://x.com/kito99/status/1888187402" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">1888187402</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1888101224',
    created: 1243035287000,
    type: 'post',
    text: 'I gave the same talk three times back to back. It was a perfect crowd to prepare me for #javaone. Tough. Many told me I inspired them. FTW!',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '1888091110',
    created: 1243035212000,
    type: 'post',
    text: 'Inspired high school students today with my talk for career day at a local high school. Shared open source ideals and believing in yourself.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1868342531',
    created: 1242884884000,
    type: 'post',
    text: 'If you want a killer comparison between Seam and Spring Web Flow, dig this article <a href="http://www.andygibson.net/articles/seam_spring_comparison/html_single/" rel="noopener noreferrer" target="_blank">www.andygibson.net/articles/seam_spring_comparison/html_single/</a> by Andy Gibson.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1868309003',
    created: 1242884541000,
    type: 'post',
    text: 'As the saying goes "I know God loves me because..." she created Belgium Beer and let me discover it. Life will always be better now ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '1868302291',
    created: 1242884474000,
    type: 'post',
    text: 'I made out like a f*n bandit. &lt; 2 hours both ways between Richmond and Washington. You could say I timed it right. Killer.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1868289126',
    created: 1242884344000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/apemberton" rel="noopener noreferrer" target="_blank">@apemberton</a> Very interested. I will check that out. Wow, I would have never picked you out from your twitter pic ;) Maybe it\'s a secret you.<br><br>In reply to: <a href="https://x.com/apemberton/status/1867660783" rel="noopener noreferrer" target="_blank">@apemberton</a> <span class="status">1867660783</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1867469946',
    created: 1242877455000,
    type: 'post',
    text: 'Heading back home from giving talk at Richmond JUG. Awesome awesome time. What stellar hosts! Still need to cut talk though. Too much bonus.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1856267342',
    created: 1242796043000,
    type: 'post',
    text: 'I stumbled over the movie about the Enron Scandal last night and I was just in total disbelief about how horrible people and corps can be.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1856258284',
    created: 1242795966000,
    type: 'post',
    text: 'I was pretty happy with my talk at NovaJUG, but I also know now what needs to be refined before J1. Too much intro, for one.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1849579261',
    created: 1242755145000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SKohler" rel="noopener noreferrer" target="_blank">@SKohler</a> I have to modify the slides when I release them to remove Sun branding. When I do post-J1, they will be on seamframework.org.<br><br>In reply to: @skohler <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1849543792',
    created: 1242754927000,
    type: 'post',
    text: 'I love chatting during webinars ;) Hopefully the participants like it too ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '1848153439',
    created: 1242746673000,
    type: 'post',
    text: 'I\'ll also be sitting in on a webinar/chat today about JSR-299 give by Pete Muir and hosted on GlassFish TV <a href="http://wikis.sun.com/display/TheAquarium/JCDI+-+JSR299" rel="noopener noreferrer" target="_blank">wikis.sun.com/display/TheAquarium/JCDI+-+JSR299</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1848129392',
    created: 1242746524000,
    type: 'post',
    text: 'Then I\'ll be off to Richmond tomorrow to give the same talk to the Richmond JUG <a href="http://www.richmondjug.com/200905Meeting" rel="noopener noreferrer" target="_blank">www.richmondjug.com/200905Meeting</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1848119214',
    created: 1242746461000,
    type: 'post',
    text: 'I\'ll be speaking about conversations and page flows for JSF (Seam/Spring) 2night at NovaJUG <a href="http://novajug.wordpress.com/2009/05/06/conversations-and-page-flows-on-the-javaserver-faces-platform-by-dan-allen" rel="noopener noreferrer" target="_blank">novajug.wordpress.com/2009/05/06/conversations-and-page-flows-on-the-javaserver-faces-platform-by-dan-allen</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1839237076',
    created: 1242676379000,
    type: 'post',
    text: 'If you can\'t catch them in the forums, here\'s your chance for some face time with JBoss folks: <a href="http://jboss.org/events/javaone.html" rel="noopener noreferrer" target="_blank">jboss.org/events/javaone.html</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1837363247',
    created: 1242664570000,
    type: 'reply',
    text: '@laixer Yeah, the maven plugin is a bit of a pig, but not too bad really. Trust me, Eclipse has no trouble failing even w/o maven\'s help.<br><br>In reply to: @laixer <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1837354561',
    created: 1242664515000,
    type: 'post',
    text: 'I signed up for #communityone before #javaone and confused the system. Now I have to struggle to get registered for #javaone. Worse.',
    likes: 0,
    retweets: 0,
    tags: ['communityone', 'javaone', 'javaone']
  },
  {
    id: '1831069479',
    created: 1242608946000,
    type: 'post',
    text: 'Here\'s another take on that photo. "How do you keep an Eclipse user busy for hours?"',
    photos: ['<div class="entry"><img class="photo" src="media/1831069479.png"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1831058533',
    created: 1242608868000,
    type: 'post',
    text: 'At least when Eclipse is failing, it looks busy. That\'s the key. Just put on a good show. #eclipsefail',
    photos: ['<div class="entry"><img class="photo" src="media/1831069479.png"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['eclipsefail']
  },
  {
    id: '1830994283',
    created: 1242608407000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/richsharples" rel="noopener noreferrer" target="_blank">@richsharples</a> brought back memories of Tuscany <a href="https://www.flickr.com/photos/sharples/3540316510/in/set-72157618368227066/" rel="noopener noreferrer" target="_blank">www.flickr.com/photos/sharples/3540316510/in/set-72157618368227066/</a> where all meetings should be held (or no meetings at all)',
    likes: 0,
    retweets: 0
  },
  {
    id: '1830967351',
    created: 1242608208000,
    type: 'post',
    text: 'When running war:inplace in Maven, there\'s no way to prevent it copying dependent JARs to WEB-INF/lib, messing up jetty:run. 1 pt to Maven.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1817914535',
    created: 1242493680000,
    type: 'post',
    text: 'I\'m not sue I get the twibes deal. You need to have the twibe name (i.e. JSF) in the text of the message? Why not use the hash convention?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1817175347',
    created: 1242488001000,
    type: 'post',
    text: 'Went with my wife to see Star Trek last night. I have never been a Trekkie, but I was enthralled by the movie, effects, and storyline. A+.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1811035246',
    created: 1242427355000,
    type: 'post',
    text: 'Sign up for my #communityone session. Here\'s the info: S305065 Jun 1 11:50 - 12:40 Running Seam on the GlassFish application server',
    likes: 0,
    retweets: 0,
    tags: ['communityone']
  },
  {
    id: '1810926087',
    created: 1242426576000,
    type: 'post',
    text: 'wonders why the content builder only shows a task list for #communityone. Seems like they are more organized.',
    likes: 0,
    retweets: 0,
    tags: ['communityone']
  },
  {
    id: '1810913645',
    created: 1242426485000,
    type: 'post',
    text: 'Just uploaded my presentation for #communityone - Running Seam on GlassFish. Made significant improvements thanks to <a class="mention" href="https://x.com/gscottshaw" rel="noopener noreferrer" target="_blank">@gscottshaw</a>.',
    likes: 0,
    retweets: 0,
    tags: ['communityone']
  },
  {
    id: '1810645317',
    created: 1242424482000,
    type: 'post',
    text: 'I get this strange behavior in Firefox after a while where it experiences pauses. I won\'t be able to click a link right away, for example.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1809767366',
    created: 1242418541000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gscottshaw" rel="noopener noreferrer" target="_blank">@gscottshaw</a> Thanks for the advice! I\'m going to weave in what I can before uploading my C1 presentation. Key point. Less bullets. Bang!<br><br>In reply to: <a href="https://x.com/gscottshaw/status/1809755592" rel="noopener noreferrer" target="_blank">@gscottshaw</a> <span class="status">1809755592</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1807954201',
    created: 1242407203000,
    type: 'post',
    text: 'I need some use cases as to when you would want to remove a bean from a context. JSR-299 is proposing that there is no remove method.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1807529921',
    created: 1242404629000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Hmm, now it picks it up immediately. I\'ll have to pay close attention the next time it happens (if it ever does again).<br><br>In reply to: <a href="https://x.com/lincolnthree/status/1807396035" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">1807396035</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1807276841',
    created: 1242403108000,
    type: 'post',
    text: 'I\'ve long wondered why when I replace a character and redeploy a Facelets template, it doesn\'t get picked up. Have to make a line change.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1807190547',
    created: 1242402590000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> Looks like Peter Thomas is doing what he does best again, flinging mud. I like your stance, there is no one right framework ;)<br><br>In reply to: <a href="https://x.com/mraible/status/1806597552" rel="noopener noreferrer" target="_blank">@mraible</a> <span class="status">1806597552</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1807168136',
    created: 1242402455000,
    type: 'post',
    text: 'Had the first of three Chimays last night that came in a set that included this chalice: Red, white, then blue.',
    photos: ['<div class="entry"><img class="photo" src="media/1807168136.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1806253613',
    created: 1242396757000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mohammed_" rel="noopener noreferrer" target="_blank">@mohammed_</a> Definitely check out Seam\'s PDF tag library for JSF. <a href="http://seamframework.org" rel="noopener noreferrer" target="_blank">seamframework.org</a><br><br>In reply to: <a href="https://x.com/mohammed_/status/1741016718" rel="noopener noreferrer" target="_blank">@mohammed_</a> <span class="status">1741016718</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1806234928',
    created: 1242396634000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> Awesome!!! I\'ve been waiting for that post. You\'ve been making me nervous for you with your extended recovery.<br><br>In reply to: <a href="https://x.com/mraible/status/1806207404" rel="noopener noreferrer" target="_blank">@mraible</a> <span class="status">1806207404</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1806230636',
    created: 1242396606000,
    type: 'post',
    text: 'I\'m jumping on the JBoss twibe: <a href="http://twibes.com/Jboss" rel="noopener noreferrer" target="_blank">twibes.com/Jboss</a> Seems easier than reading e-mail about work.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1802705840',
    created: 1242360203000,
    type: 'post',
    text: 'Just upgraded the metawidget examples being included in upcoming Seam 2.1.2.GA release. The key feature. Less typing.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1802669137',
    created: 1242359939000,
    type: 'post',
    text: 'My C1 presentation is just about ready, but I sure had to wrestle OO.org Impress to get it there. Damn that tool irritates me.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1802019278',
    created: 1242355521000,
    type: 'post',
    text: 'Wow, sourceforge.net has the most drastic redesign I have seen in its history.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1801972586',
    created: 1242355204000,
    type: 'post',
    text: 'In JSF 2, your bean can be @NoneScoped. Is that what you choose when hope is lost? Does it come with a ruler? Why that and not <a class="mention" href="https://x.com/PopeScoped" rel="noopener noreferrer" target="_blank">@PopeScoped</a>?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1801957421',
    created: 1242355104000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> Yep, I\'m already getting WiFi (me thinks free) on Virgin American to J1. I\'m just about ready to start picking airlines, not prices.<br><br>In reply to: <a href="https://x.com/kito99/status/1800614306" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">1800614306</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1800429434',
    created: 1242344376000,
    type: 'post',
    text: '"It\'s official. WiFi on every AirTran flight." FTW! gogoinflight.com (not free thought)',
    likes: 0,
    retweets: 0
  },
  {
    id: '1800222195',
    created: 1242342916000,
    type: 'post',
    text: 'Here you go, Google on JSR-299 EG <a href="http://web.archive.org/web/20070206215114/http://jcp.org/en/jsr/detail?id=299" rel="noopener noreferrer" target="_blank">web.archive.org/web/20070206215114/http://jcp.org/en/jsr/detail?id=299</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1800206567',
    created: 1242342808000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> I get the gist of the history from talking to Gavin &amp; Pete. Bob Lee influenced the type-safety in JSR-299, then bailed on it.<br><br>In reply to: <a href="https://x.com/kito99/status/1799994149" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">1799994149</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1799953197',
    created: 1242341118000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> Interesting. Could be a good example for Seam. However, look at the rabbit hole that sent Christian down with Seam wiki ;)<br><br>In reply to: <a href="https://x.com/metacosm/status/1799874772" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">1799874772</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1799925673',
    created: 1242340937000,
    type: 'post',
    text: 'Interesting. Google is no longer listed on the JSR-299 EG. Using archive.org you can see that they were at one point.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1799802164',
    created: 1242340129000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Yeah, probably what we will do. Would need to be short. And the usernames should be commit usernames. Easier to follow.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1799690495" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1799690495</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1799583850',
    created: 1242338711000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> Not sure. I guess my main concern is to keep things separated from general posts, and to group the posts by project.<br><br>In reply to: <a href="https://x.com/metacosm/status/1799442056" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">1799442056</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1799415087',
    created: 1242337647000,
    type: 'post',
    text: 'And for those asking, IRC is available via...IRC. Twitter is available via the web. Huge difference. Twitter/Twibes may work. 2nd account?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1799362342',
    created: 1242337316000,
    type: 'post',
    text: '"Everyone wants to know. What feature are you coding?"',
    likes: 0,
    retweets: 0
  },
  {
    id: '1799345994',
    created: 1242337215000,
    type: 'post',
    text: 'I\'m looking for a twitter-like tool I can use to make open source development more social. It would fill the place of IRC chat. Suggestions?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1799322486',
    created: 1242337070000,
    type: 'post',
    text: 'developerWorks is coming out of its shell and growing a community around it\'s popular online tech journal: <a href="http://download.boulder.ibm.com/ibmdl/pub/software/dw/ibm/mydw/overview/overview.html" rel="noopener noreferrer" target="_blank">download.boulder.ibm.com/ibmdl/pub/software/dw/ibm/mydw/overview/overview.html</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1798333093',
    created: 1242329970000,
    type: 'post',
    text: 'Sponsored links for JSF 2 e-mail from JSF EG: Electronics Component, JSF database, transistor data sheet, Lane Bryant String Bikini??',
    likes: 0,
    retweets: 0
  },
  {
    id: '1797120225',
    created: 1242322578000,
    type: 'post',
    text: 'Skype is good, but not great. Call quality is great. What pulls it down is completely deficient conferencing capabilities.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1797102322',
    created: 1242322466000,
    type: 'reply',
    text: '@translucent_eye I\'m right there with you. I code my HTML by hand. I don\'t agree with XML formatters. They never do what I want.<br><br>In reply to: <a href="https://x.com/davidsachdev/status/1796893172" rel="noopener noreferrer" target="_blank">@davidsachdev</a> <span class="status">1796893172</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1796960478',
    created: 1242321580000,
    type: 'post',
    text: 'Last chance to sign up for CommunityOne (free) and have $10 donated to FSF. code: W1321390 <a href="http://www.cplan.com/communityone2009/west/externalregistration" rel="noopener noreferrer" target="_blank">www.cplan.com/communityone2009/west/externalregistration</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1796012668',
    created: 1242315872000,
    type: 'post',
    text: 'JSF 2 Webinar by Ed Burns to start in 15 minutes. Get your phone charged! <a href="http://weblogs.java.net/blog/edburns/archive/2009/05/jsf2_webinar_ne.html" rel="noopener noreferrer" target="_blank">weblogs.java.net/blog/edburns/archive/2009/05/jsf2_webinar_ne.html</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1795338397',
    created: 1242311744000,
    type: 'post',
    text: 'I have been dead silent while working on my CommunityOne presentation. I love giving them, but I hate creating them.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1770755412',
    created: 1242104976000,
    type: 'reply',
    text: '@uilcrw You can get a newer version of the spec document on the webbeans-dev list: <a href="http://lists.jboss.org/pipermail/webbeans-dev/2009-May/000877.html" rel="noopener noreferrer" target="_blank">lists.jboss.org/pipermail/webbeans-dev/2009-May/000877.html</a><br><br>In reply to: @uilcrw <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1770614701',
    created: 1242103668000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> What do you mean by looking through other groups of code. Care to start a thread on seamframework.org in the WB forum?<br><br>In reply to: <a href="https://x.com/JohnAment/status/1768845637" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">1768845637</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1770038447',
    created: 1242099019000,
    type: 'post',
    text: 'I wonder if you could design/develop with Twitter. It would be an interesting experiment. And it would be very open.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1770033392',
    created: 1242098984000,
    type: 'post',
    text: 'I think twitter is to a mailinglist what IM is to e-mail, in a way. You don\'t sound an alarm w/ every post, nor do you need a subject.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1768707116',
    created: 1242089720000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> So far, I think I have the upper hand with Maven this time around :)<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1767387497" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1767387497</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1766874387',
    created: 1242076378000,
    type: 'post',
    text: 'I should have said Maven waits while I tweet about it ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '1766869856',
    created: 1242076348000,
    type: 'post',
    text: 'Maven waits while you attach a debugger when using mvndebug. How nice. I gain +1 on Maven today. -DforkMode=none to debug tests.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1766851851',
    created: 1242076226000,
    type: 'post',
    text: 'I completely missed the Players yesterday while working on a website for the \'rents. I miss my lazy Sunday afternoons :(',
    likes: 0,
    retweets: 0
  },
  {
    id: '1766649956',
    created: 1242074853000,
    type: 'post',
    text: 'My little sis just bought her first house! I am so excited for her. She got a great rate too.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1755658737',
    created: 1241973720000,
    type: 'post',
    text: '9 years after graduating college, I have finally hung my diploma. More progress.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1755203684',
    created: 1241969512000,
    type: 'post',
    text: 'Made my first margarita last night. It must have been the warm ocean-like breeze that inspired me. Close eyes, imagine islands.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1755185062',
    created: 1241969339000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> You\'re just a pioneer. The real problem, of course, is Eclipse\'s lack of support for project modules; m2eclipse works around it<br><br>In reply to: <a href="https://x.com/maxandersen/status/1753526360" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1753526360</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1743436558',
    created: 1241839481000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kito99" rel="noopener noreferrer" target="_blank">@kito99</a> That\'s a catch 22. If you turn off build automatically, Eclipse works very poorly. I had it that way for a while and it sucked.<br><br>In reply to: <a href="https://x.com/kito99/status/1742003143" rel="noopener noreferrer" target="_blank">@kito99</a> <span class="status">1742003143</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1741992249',
    created: 1241828450000,
    type: 'post',
    text: 'Why does Eclipse insist on building my entire workspace when I make a change to a project? That\'s what irritates me so much about Eclipse.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1740754108',
    created: 1241812377000,
    type: 'post',
    text: 'In JSF 2, you can have fine-grained faces-config.xml files by convention that end in .faces-config.xml. The catch: they must be in META-INF.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1740603697',
    created: 1241811334000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> Go with LHR. Charles De Gaulle is a nightmare of an airport. You have to go through security again at LHR, but you get rewarded.<br><br>In reply to: <a href="https://x.com/tech4j/status/1740222343" rel="noopener noreferrer" target="_blank">@tech4j</a> <span class="status">1740222343</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1740591213',
    created: 1241811246000,
    type: 'post',
    text: 'I am like a bug filing machine today. What can I say? I had a backlog.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1740346619',
    created: 1241809540000,
    type: 'post',
    text: 'Now that\'s a coupon worth clipping!',
    photos: ['<div class="entry"><img class="photo" src="media/1740346619.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1740200353',
    created: 1241808519000,
    type: 'post',
    text: 'It\'s bad design for APIs to treat URLs as strings. A custom object (not even java.net.URL) should be used until the URL needs to be printed.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1735321396',
    created: 1241763990000,
    type: 'post',
    text: 'Ed Burns will be giving a webinar on JSF 2 next Thurs (May 14) <a href="http://weblogs.java.net/blog/edburns/archive/2009/05/jsf2_webinar_ne.html" rel="noopener noreferrer" target="_blank">weblogs.java.net/blog/edburns/archive/2009/05/jsf2_webinar_ne.html</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1735040532',
    created: 1241760845000,
    type: 'post',
    text: 'I just love developing against APIs that are under active development. It\'s like trying to find something that\'s spontaneously teleporting.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1734982234',
    created: 1241760234000,
    type: 'post',
    text: 'Why do people insist on checking in generated files. It makes for busy work when doing RCS updates.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1734969313',
    created: 1241760098000,
    type: 'post',
    text: 'I have been banging my head on several problems all day. I am finally making some breakthroughs. Now I am reporting/committing.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1724579831',
    created: 1241673513000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> Maven 2.1 is out. GA baby. Unfortunately, there is a bug that breaks maven-cli-plugin. But you can patch Maven to make it work.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1724144337" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1724144337</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1724425615',
    created: 1241672137000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> It\'s not big thing really. You\'ll always remember those first few minutes after you wake up...when you can see the clock ;)<br><br>In reply to: <a href="https://x.com/mraible/status/1723652669" rel="noopener noreferrer" target="_blank">@mraible</a> <span class="status">1723652669</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1723338812',
    created: 1241663643000,
    type: 'post',
    text: 'Wow, Maven 2.1 is significantly faster than Maven 2.0. If I can get all my builds to work with Maven 2.1, I\'m not looking back!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1723225635',
    created: 1241662865000,
    type: 'post',
    text: '"In Maven 2.0.9 we made the dependency order deterministic using linkedHashMaps" Wow, took them that long to realize that?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1722619048',
    created: 1241658839000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> JSFUnit is a little more of a client whereas FacesTester is along the lines of SeamTest as an integration/assembly test tool.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1720962080" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1720962080</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1720117069',
    created: 1241641031000,
    type: 'post',
    text: 'Uh oh, #buildfail due to <a href="http://jira.codehaus.org/browse/MENFORCER-55" rel="noopener noreferrer" target="_blank">jira.codehaus.org/browse/MENFORCER-55</a><br>. Had to upgrade enforcer plugin, then it works. JSR-299 RI built.',
    likes: 0,
    retweets: 0,
    tags: ['buildfail']
  },
  {
    id: '1719960896',
    created: 1241639999000,
    type: 'post',
    text: 'I\'m never a big fan of visual graphs. Here\'s the dependency graph for Seam 3 booking: A 3-D graph would be better.',
    photos: ['<div class="entry"><img class="photo" src="media/1719960896.png"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1719950507',
    created: 1241639929000,
    type: 'post',
    text: 'm2eclipse dependency graph',
    photos: ['<div class="entry"><img class="photo" src="media/1719960896.png"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1719873601',
    created: 1241639397000,
    type: 'post',
    text: 'Ooooooh, a visual POM editor. Nice. +1 for Eclipse.',
    photos: ['<div class="entry"><img class="photo" src="media/1719873601.png"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1719788591',
    created: 1241638814000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> Awesome. Good to hear! I\'ve still got some stuff up my sleeve.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/1718082547" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">1718082547</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1719782991',
    created: 1241638777000,
    type: 'post',
    text: 'Had a long talk with the Manning publisher about the future of books. Not e-books, but something that better leverages the web medium.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1719765972',
    created: 1241638664000,
    type: 'post',
    text: 'I\'m giving m2eclipse a whirl. Another thing motivating me to start Eclipse is shitty debugging in NetBeans. Let\'s compare Maven 2 support.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1719747300',
    created: 1241638538000,
    type: 'post',
    text: 'If you are using JSF, check out FacesTester: <a href="http://kenai.com/projects/facestester" rel="noopener noreferrer" target="_blank">kenai.com/projects/facestester</a> I\'ll likely using it in Seam 3...more info to come.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1712944786',
    created: 1241577973000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> I\'ve been a close Maven pro. Now I just get to perform out in the open ;)<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1712940579',
    created: 1241577943000,
    type: 'post',
    text: 'Come on, there\'s no maven plugin to touch a file? Maven scores one on me.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1710346839',
    created: 1241559826000,
    type: 'post',
    text: 'Did I get unknowingly transported in my sleep to a replica city staged in the middle of a rainforest? What\'s with all this rain in May?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1702855094',
    created: 1241494671000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JustinMFox" rel="noopener noreferrer" target="_blank">@JustinMFox</a> Welcome to the club ;)<br><br>In reply to: <a href="https://x.com/JustinMFox/status/1702520595" rel="noopener noreferrer" target="_blank">@JustinMFox</a> <span class="status">1702520595</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1702850710',
    created: 1241494637000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> That is my dad\'s favorite word...when he\'s mad at his kids. We get accused of being lackadaisical.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1700577990',
    created: 1241478971000,
    type: 'post',
    text: 'Just got the invitation for the wedding of one of my college roommates...the last one of us to get married. The wedding is at the Ritz ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '1699182193',
    created: 1241469133000,
    type: 'post',
    text: 'A Maven 2 plugin that will change your life (or at least save some of it): <a href="https://mojavelinux.com/blog/archives/2009/05/a_gamechanging_maven_2_plugin_you_absolutely_must_use/" rel="noopener noreferrer" target="_blank">mojavelinux.com/blog/archives/2009/05/a_gamechanging_maven_2_plugin_you_absolutely_must_use/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1692651385',
    created: 1241407687000,
    type: 'post',
    text: 'Today I hung a picture that has been sitting on the floor of my office for 4 years. Yeah. Progress.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1691142185',
    created: 1241395884000,
    type: 'post',
    text: 'There is this new development near me called Arundel Preserve. I can tell you that when they bulit it, they didn\'t preserve shit.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1685238538',
    created: 1241332464000,
    type: 'post',
    text: 'Spent several hours agonizing over hotel choice for #javaone. Had to balance amenities, price, and walking distance. The winner: Parc 55.',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '1683956252',
    created: 1241319622000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> Very slick. I\'m officially a git fan.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1674703215" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1674703215</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1683950453',
    created: 1241319582000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> Hopefully the\'ll finally have auto imports for static types and enums. So annoying to have to type in manually every time.<br><br>In reply to: <a href="https://x.com/jeremynorris/status/1681507910" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <span class="status">1681507910</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1683939789',
    created: 1241319495000,
    type: 'post',
    text: 'I\'m flying Virgin America to #javaone again this year. They are my official #javaone airline. Even better, WIFI this year!',
    likes: 0,
    retweets: 0,
    tags: ['javaone', 'javaone']
  },
  {
    id: '1674126770',
    created: 1241221143000,
    type: 'post',
    text: 'Just used git for the 1st time to fix maven-cli-plugin. Awesome. Actually, it\'s github.com that\'s awesome. It spoon feeds you instructions.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1671634686',
    created: 1241203073000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jsfcentral" rel="noopener noreferrer" target="_blank">@jsfcentral</a> Sorry, but that article on Seam sucked.<br><br>In reply to: <a href="https://x.com/jsfcentral/status/1662507505" rel="noopener noreferrer" target="_blank">@jsfcentral</a> <span class="status">1662507505</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1671593648',
    created: 1241202781000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> Maven is nothing without profiles, so you are about to open new doors. I got Maven profiles down, so I get one up on Maven there.<br><br>In reply to: <a href="https://x.com/jasondlee/status/1671145984" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">1671145984</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1662527978',
    created: 1241123412000,
    type: 'post',
    text: 'Somehow my whitelist of e-mail addresses that can display images inline in Gmail account got erased. Google, what are you not telling me?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1660511964',
    created: 1241109720000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ManningBooks" rel="noopener noreferrer" target="_blank">@ManningBooks</a> hit 300 followers and is celebrating. Join the party by getting 50% books until May 1 with code twtr0501 at manning.com.<br><br>In reply to: <a href="https://x.com/ManningBooks/status/1660402721" rel="noopener noreferrer" target="_blank">@ManningBooks</a> <span class="status">1660402721</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1660501791',
    created: 1241109653000,
    type: 'post',
    text: 'Besides, NetBeans totally kicks ass at Maven 2 projects. In fact, I would argue you should just leave it up whenever you are using Maven 2.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1660428587',
    created: 1241109174000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> So you are saying just create a custom property for src/main/webapp? Good idea. jetty/tomcat plugins need to reference too.<br><br>In reply to: <a href="https://x.com/matthewmccull/status/1618148557" rel="noopener noreferrer" target="_blank">@matthewmccull</a> <span class="status">1618148557</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1660419393',
    created: 1241109113000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> The JBoss Web Beans team is excited about having Open WebBeans (both are open of course). It\'s good to have different approaches.<br><br>In reply to: <a href="https://x.com/JohnAment/status/1617975029" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">1617975029</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1660370314',
    created: 1241108796000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> As I\'ve said before, JBoss Tools is awesome. But when I need something &gt; VIM but &lt; super-charged IDE, I keep NetBeans on retainer.<br><br>In reply to: <a href="https://x.com/rruss/status/1659855642" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">1659855642</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1659134883',
    created: 1241100593000,
    type: 'post',
    text: 'It\'s incredibly annoying that NetBeans does not have a TestNG runner. That\'s what often motivates me to fire up Eclipse in the morning.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1658917008',
    created: 1241099057000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> Both Jetty and Tomcat are a dream to start as embedded containers in Maven. That\'s how you get one back from Maven.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1639086601" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1639086601</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1658909755',
    created: 1241099006000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Too much typing to create Maven 2 POMs is giving me hand aches too. I guess Maven wins again.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1658896823',
    created: 1241098913000,
    type: 'post',
    text: 'Here we go, jumping on the JSF twibe bandwagon <a href="http://twibes.com/JSF" rel="noopener noreferrer" target="_blank">twibes.com/JSF</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1656504668',
    created: 1241067606000,
    type: 'post',
    text: 'I had high hopes for the maven-cli-plugin to speed up and simplify execution <a href="http://github.com/mrdon/maven-cli-plugin" rel="noopener noreferrer" target="_blank">github.com/mrdon/maven-cli-plugin</a> Maven won today.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1656498452',
    created: 1241067547000,
    type: 'post',
    text: 'Got the first three modules in Seam 3 building: el, international, and faces. It\'s just somewhere to start. Long way to go still.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1639005282',
    created: 1240927089000,
    type: 'post',
    text: 'Oh and I\'m trying to be like a normal person and see family on the weekend. Two weekends of manual labor. Hmmm :( j/k I enjoyed helping.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1638981351',
    created: 1240926904000,
    type: 'post',
    text: 'I\'ve been quiet for a couple of days because I\'ve had my head emersed in the Seam 3 build. Still early but it\'s starting to take shape.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1617967679',
    created: 1240715953000,
    type: 'reply',
    text: '@puckyloucks If it\'s about Seam 3+Web Beans. We needed to lay new tracks to be able to adopt a modular structure, which Maven requires.<br><br>In reply to: @puckyloucks <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1617950590',
    created: 1240715790000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> Even better. It acts as a test assembler/packager rather than a test runner. The running is abstracted to an API. Check it out.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1608468580" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1608468580</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1617933838',
    created: 1240715628000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matthewmccull" rel="noopener noreferrer" target="_blank">@matthewmccull</a> No dude, I am looking for a property for src/main/webapp. You know how many times that path gets repeated?!?<br><br>In reply to: <a href="https://x.com/matthewmccull/status/1601036186" rel="noopener noreferrer" target="_blank">@matthewmccull</a> <span class="status">1601036186</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1617907079',
    created: 1240715370000,
    type: 'post',
    text: 'Wrapped my head around the JBoss test harness Pete et al wrote for 299 TCK. Creative use of TestNG <a href="http://sfwk.org/WebBeans/JSR299TCKHarness" rel="noopener noreferrer" target="_blank">sfwk.org/WebBeans/JSR299TCKHarness</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1617898675',
    created: 1240715289000,
    type: 'post',
    text: 'NetBeans supports loading code formatting rules from pom.xml properties, which can be inherited from a parent project. Sweet.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1617890722',
    created: 1240715212000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> Doh! Sucks when you are trying to text message a friend and post it to twitter instead ;) 40404 is just habit for me by now.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1617688742" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1617688742</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1606377693',
    created: 1240599667000,
    type: 'post',
    text: 'I\'m trying to wrap my head around the test harness Pete wrote for Web Beans so I can use it to write tests for the Seam examples.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1606377665',
    created: 1240599667000,
    type: 'post',
    text: 'I\'m trying to wrap my head around the test harness Pete wrote for Web Beans so I can use it to write tests for the Seam examples.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1606344948',
    created: 1240599438000,
    type: 'post',
    text: 'Just joined the Seam twibe. Visit <a href="http://twibes.com/jbossseam" rel="noopener noreferrer" target="_blank">twibes.com/jbossseam</a> to join.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1606331900',
    created: 1240599342000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> f:setPropertyActionListener is especially useful for Ajax. Max makes use of it often in Practical RichFaces.<br><br>In reply to: <a href="https://x.com/lincolnthree/status/1604322994" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">1604322994</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1606313415',
    created: 1240599209000,
    type: 'post',
    text: 'My Twitter personality:likeable sociable fair; style:chatty coherent GURU <a href="http://twanalyst.com/mojavelinux" rel="noopener noreferrer" target="_blank">twanalyst.com/mojavelinux</a> Exactly what I\'m striving for ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '1606274752',
    created: 1240598936000,
    type: 'post',
    text: 'Many tweet replies sent from a Twitter client do not maintain the thread. It\'s annoying to only see one end of a conversation.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1600476660',
    created: 1240542479000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> I\'m a closet Maven maven. Meaning I\'ve created tons of Maven project configs, but never been able to share them, until now ;)<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1599228049',
    created: 1240532772000,
    type: 'post',
    text: 'Damn you Maven. Why is src/main/webapp not stored as a property so I can reference it as ${project.build.warSourceDirectory}!?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1587630727',
    created: 1240430231000,
    type: 'post',
    text: 'Happy Earth Day. We should think of this day as merely a chance to reflect on our green progress. We need to think about the Earth always.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1587612235',
    created: 1240430089000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Pics of Red Zody chair: front #1: front #2: back:<br><br>In reply to: <a href="https://x.com/maxandersen/status/1582762671" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1582762671</span>',
    photos: ['<div class="entry"><img class="photo" src="media/1587612235.jpg"></div>', '<div class="entry"><img class="photo" src="media/1587612235-2.jpg"></div>', '<div class="entry"><img class="photo" src="media/1587612235-3.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1582441099',
    created: 1240378471000,
    type: 'post',
    text: 'If you missed the first post, I got the Zody chair by Haworth. Waited 8 weeks to get a red seat because, of course, I always get red ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '1582393366',
    created: 1240377915000,
    type: 'post',
    text: 'Couple of days so far in my new chair and I know it\'s the one. It was featured on last episode of Chuck when Emmett takes over for Big Mike.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1581567979',
    created: 1240370387000,
    type: 'post',
    text: 'Maven 2, did you miss me when I was gone? Well, now I\'m back, beating my head against your wall. Today I won. EAR deployed.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1569192436',
    created: 1240263470000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> I am speaking about conversations and page flows on the JavaServer Faces platform. Covering Seam and Spring Web Flow.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1554586536" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1554586536</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1569160864',
    created: 1240263234000,
    type: 'post',
    text: 'Someone at the w3c uploaded a bum loose.dtd file and it\'s wrecking Java deployments today that use validating XML parsers that hit the net.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1565587037',
    created: 1240236377000,
    type: 'post',
    text: 'Many folks have mentioned that Oracle will own MySQL, but what about Oracle now owning another app server, GlassFish. Was a growing rival.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1548630040',
    created: 1240029005000,
    type: 'post',
    text: 'Does your hosting provider tweet for you? <a class="mention" href="https://x.com/eApps_Status" rel="noopener noreferrer" target="_blank">@eApps_Status</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1547430064',
    created: 1240017917000,
    type: 'post',
    text: 'Whose twisted idea was it to make the JavaOne presentations due 2 days after the tax deadline? Fortunately, my taxes were done early!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1547425819',
    created: 1240017882000,
    type: 'post',
    text: 'I\'ve been dark for a couple of days working on my JavaOne presentation. I just submitted it, just in time. Now, time to eat and sleep!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1528310097',
    created: 1239829162000,
    type: 'post',
    text: 'The search continued for the deadliest catch last night! I\'ve got it queued up. #deadliestcatch',
    likes: 0,
    retweets: 0,
    tags: ['deadliestcatch']
  },
  {
    id: '1518601759',
    created: 1239733446000,
    type: 'post',
    text: 'DZone posted a review of Seam in Action, rewarding it 5 stars. <a href="http://books.dzone.com/reviews/win-your-copy-seam-action" rel="noopener noreferrer" target="_blank">books.dzone.com/reviews/win-your-copy-seam-action</a> Comment and enter to win a copy.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1510816466',
    created: 1239647621000,
    type: 'post',
    text: 'Yesterday was my wife\'s birthday. 31 is going to be her year. She\'s a prime number type of person.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1510698551',
    created: 1239646509000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxkatz" rel="noopener noreferrer" target="_blank">@maxkatz</a> I do use Gmail for personal, I was talking about corporate mail.<br><br>In reply to: <a href="https://x.com/maxkatz/status/1495722616" rel="noopener noreferrer" target="_blank">@maxkatz</a> <span class="status">1495722616</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1510694441',
    created: 1239646470000,
    type: 'post',
    text: 'Everyone in the top 6 was really a winner in that Masters; kept us all on the edge of our seats. More time to look at that beautiful course.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1510675999',
    created: 1239646299000,
    type: 'post',
    text: 'The Masters was incredible! Sadly, my recording ran out when Cabrera was behind tree in 1st playoff. I was dumbfounded when I saw he won.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1507738349',
    created: 1239604340000,
    type: 'post',
    text: 'I did it!!! 0 INBOX!!! No unread, no read, and no starred messages!!! I\'m having a Chimay Triple to celebrate ;)',
    photos: ['<div class="entry"><img class="photo" src="media/1507738349.png"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1493913079',
    created: 1239407066000,
    type: 'post',
    text: 'I\'m trying out Kmail instead of Thunderbird for my work e-mail. Thunderbird was just frustrating me.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1493903374',
    created: 1239406948000,
    type: 'post',
    text: 'A woman in the parking lot located her vehicle by pressing the panic button on her smart key. Hey, whatever gets the job done.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1492832337',
    created: 1239394918000,
    type: 'post',
    text: 'Command-line literacy is essential: <a href="http://www.linux-mag.com/id/7096" rel="noopener noreferrer" target="_blank">www.linux-mag.com/id/7096</a> That\'s my principle. It pays, it saves, it\'s reliable, it\'s steadfast.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1492276850',
    created: 1239389472000,
    type: 'post',
    text: 'As if there weren\'t enough choice in Linux already, you can now swap out the Linux kernel with the FreeBSD one Debian. Is it still Linux?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1488910890',
    created: 1239343951000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lincolnthree" rel="noopener noreferrer" target="_blank">@lincolnthree</a> I got the fully loaded Zody chair by Haworth. Tremendous reviews. Mine is black with a red seat ;)<br><br>In reply to: <a href="https://x.com/lincolnthree/status/1486136071" rel="noopener noreferrer" target="_blank">@lincolnthree</a> <span class="status">1486136071</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1488904059',
    created: 1239343828000,
    type: 'post',
    text: '"Light and liberty go together." The freedom of knowledge is of the utmost importance, Jefferson believed. To me, that is what OSS is about.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1488859681',
    created: 1239343075000,
    type: 'post',
    text: 'Thomas Jefferson is an inspiration to me now; ironic I\'d never visited the Memorial. He believed strongly in the dissemination of knowledge.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1488766804',
    created: 1239341553000,
    type: 'post',
    text: 'I would never sin with windows: <a href="http://www.linux-mag.com/id/7155" rel="noopener noreferrer" target="_blank">www.linux-mag.com/id/7155</a> Like a mother with her children, I don\'t trade my commitment to OSS for anything.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1488744472',
    created: 1239341211000,
    type: 'post',
    text: 'Linux Runs on Text: Understanding &amp; Handling Text <a href="http://www.linux-mag.com/id/7285" rel="noopener noreferrer" target="_blank">www.linux-mag.com/id/7285</a> Funny that behind all great software, there is just text.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1486022661',
    created: 1239310704000,
    type: 'post',
    text: 'My new chair just arrived today. My back is very excited (no more pain).',
    likes: 0,
    retweets: 0
  },
  {
    id: '1481969308',
    created: 1239258401000,
    type: 'post',
    text: 'I forgot to mention that I was impressed by recent changes to metro. Handles (instead of just bars) and no carpet in cars. New chimes too.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1481514042',
    created: 1239251404000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> Nope, seems like the rights are solely for marketing and content delivery. But that slide deck becomes essentially owned by Sun.<br><br>In reply to: <a href="https://x.com/JohnAment/status/1480943134" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">1480943134</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1480908499',
    created: 1239244110000,
    type: 'post',
    text: 'I\'m currently signing my presentation away to Sun :( What we do in the name of limelight.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1480899041',
    created: 1239244001000,
    type: 'post',
    text: 'Sun uses the most arcane systems for managing their conferences. It doesn\'t matter what technology is underneath. A bad UI is a bad UI.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1477893526',
    created: 1239211903000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rayploski" rel="noopener noreferrer" target="_blank">@rayploski</a> Wow, what a small world! I loved working with Andy and Manan. They are really passionate and always very positive. Sad to go.<br><br>In reply to: <a href="https://x.com/rayploski/status/1473359381" rel="noopener noreferrer" target="_blank">@rayploski</a> <span class="status">1473359381</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1473220074',
    created: 1239150621000,
    type: 'post',
    text: 'Presentations from #phillyete, including mine on Seam Security, are now available: <a href="http://chariotsolutions.com/downloads/presentations" rel="noopener noreferrer" target="_blank">chariotsolutions.com/downloads/presentations</a>',
    likes: 0,
    retweets: 0,
    tags: ['phillyete']
  },
  {
    id: '1473204935',
    created: 1239150465000,
    type: 'post',
    text: 'If you are an #obama fan or dem, definitely check out keynote given at #phillyete by Blue State Digital CTO: <a href="http://techcast.chariotsolutions.com/index.php?post_id=451968" rel="noopener noreferrer" target="_blank">techcast.chariotsolutions.com/index.php?post_id=451968</a>',
    likes: 0,
    retweets: 0,
    tags: ['obama', 'phillyete']
  },
  {
    id: '1471461827',
    created: 1239132170000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> The world is just that way. Maven should be validating that it has a valid document, or at least that the content type is XML.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1471101833" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1471101833</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1470906662',
    created: 1239126383000,
    type: 'post',
    text: 'Maven is braindead. When I was in the hotel with a proxy throwing back a "connect to internet page" Maven copied that to my repository poms.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1470706529',
    created: 1239124193000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/richsharples" rel="noopener noreferrer" target="_blank">@richsharples</a> Congrats Rich! 1 year is always a big landmark, at least it has been in my career (probably goes for tech in general).<br><br>In reply to: <a href="https://x.com/richsharples/status/1469889413" rel="noopener noreferrer" target="_blank">@richsharples</a> <span class="status">1469889413</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1467738828',
    created: 1239080520000,
    type: 'post',
    text: 'Btw, the Seam metawidget examples are available in the Seam trunk. Will be included in the 2.1.2 release.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1467091943',
    created: 1239072850000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> How come? You don\'t like DC? I have to remind myself to like it since I tend to take it for granted.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1458472624',
    created: 1238960406000,
    type: 'post',
    text: 'I can\'t even think about how many kids are going to muffed up because of the song "I love college". The power of suggestion.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1458209648',
    created: 1238957113000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/atonse" rel="noopener noreferrer" target="_blank">@atonse</a> Got down to DC yesterday just after noon and it was packed! We walked our legs off until ~ 5 then grabbed dinner at Gordon Biersch.<br><br>In reply to: @atonse <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1458198891',
    created: 1238956979000,
    type: 'post',
    text: 'My review of Pratical RichFaces just posted to Amazon: <a href="http://www.amazon.com/review/R2BOFNIEE0XPAM" rel="noopener noreferrer" target="_blank">www.amazon.com/review/R2BOFNIEE0XPAM</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1457144877',
    created: 1238942986000,
    type: 'post',
    text: 'Lived in DC all my life, entered Jefferson Memorial for the first time yesterday. Also visited FDR, WWII memorial, and front of White House.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1452407474',
    created: 1238865585000,
    type: 'post',
    text: 'It\'s a cherry blossom craze in DC!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1450287261',
    created: 1238824719000,
    type: 'post',
    text: 'Finally prepared a MySQL version of schema for Seam in Action example application: <a href="http://code.google.com/p/seaminaction/wiki/GettingStarted#Using_MySQL_instead_of_H2" rel="noopener noreferrer" target="_blank">code.google.com/p/seaminaction/wiki/GettingStarted#Using_MySQL_instead_of_H2</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1446974472',
    created: 1238783589000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iamroberthanson" rel="noopener noreferrer" target="_blank">@iamroberthanson</a> I want to be clear that when I say Spring, I am talking about the bean container in this context, not the whole stack.<br><br>In reply to: <a href="https://x.com/iamroberthanson/status/1444962081" rel="noopener noreferrer" target="_blank">@iamroberthanson</a> <span class="status">1444962081</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1446512138',
    created: 1238778633000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iamroberthanson" rel="noopener noreferrer" target="_blank">@iamroberthanson</a> In Java EE 6 EJB just becomes an impl detail of your business object. It\'s JCDI that is compelling and better than Spring.<br><br>In reply to: <a href="https://x.com/iamroberthanson/status/1444962081" rel="noopener noreferrer" target="_blank">@iamroberthanson</a> <span class="status">1444962081</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1443997218',
    created: 1238743916000,
    type: 'post',
    text: 'Some great insight about Spring vs JEE 6 by <a class="mention" href="https://x.com/jeremynorris" rel="noopener noreferrer" target="_blank">@jeremynorris</a> <a href="http://groups.google.com/group/javaposse/browse_thread/thread/6927c2828cf2301a#ee909b33ed656e47" rel="noopener noreferrer" target="_blank">groups.google.com/group/javaposse/browse_thread/thread/6927c2828cf2301a#ee909b33ed656e47</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1443163441',
    created: 1238730593000,
    type: 'post',
    text: 'Patience paid off. The icon for <a class="mention" href="https://x.com/jbossseam" rel="noopener noreferrer" target="_blank">@jbossseam</a> has now propagated. I also tuned the theme to match the sfwk site. Now we can focus on news.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1440985237',
    created: 1238706754000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/larryklug" rel="noopener noreferrer" target="_blank">@larryklug</a> Best solution is to print to PDF. You can use the CUPS PDF driver. Tricks Reader into thinking it is going to a real printer.<br><br>In reply to: <a href="https://x.com/larryklug/status/1413092851" rel="noopener noreferrer" target="_blank">@larryklug</a> <span class="status">1413092851</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1440884888',
    created: 1238705752000,
    type: 'post',
    text: 'The guy from railsenvy.com did a summary video of the Emerging Tech conference: <a href="http://www.viddler.com/explore/chariot/videos/3/" rel="noopener noreferrer" target="_blank">www.viddler.com/explore/chariot/videos/3/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1440786313',
    created: 1238704760000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> The icon upload in twitter is very flaky. I\'m guessing the transfer of the file to S3 is to blame.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1440015518" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1440015518</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1439712367',
    created: 1238693858000,
    type: 'post',
    text: 'Seam Framework is now on Twitter. Follow <a class="mention" href="https://x.com/jbossseam" rel="noopener noreferrer" target="_blank">@jbossseam</a> to get updates.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1439346833',
    created: 1238690157000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/richsharples" rel="noopener noreferrer" target="_blank">@richsharples</a> Chris Richardson jokes in his cloud talk about forgetting to turn off his instance and getting charged "just 100 bucks".<br><br>In reply to: <a href="https://x.com/richsharples/status/1438621877" rel="noopener noreferrer" target="_blank">@richsharples</a> <span class="status">1438621877</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1436892621',
    created: 1238653733000,
    type: 'post',
    text: 'Watched Madagascar 2 last night. The folks who worked on King Julian\'s character are insane and borderline schizophrenic. I love it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1436578558',
    created: 1238648324000,
    type: 'reply',
    text: '@puckyloucks Thanks! What\'s most important to me is to know that the book is helping people.<br><br>In reply to: @puckyloucks <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1436563913',
    created: 1238648105000,
    type: 'post',
    text: 'I\'m now a mentor for Google Summer of Code 2009 representing Red Hat. I\'ll be proctoring the seam-gen encore proposal.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1436428367',
    created: 1238646160000,
    type: 'post',
    text: 'Just put together a Google Summer of Code proposal for seam-gen encore: <a href="http://www.jboss.org/community/docs/DOC-13401#seam_wb_jbt" rel="noopener noreferrer" target="_blank">www.jboss.org/community/docs/DOC-13401#seam_wb_jbt</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1433675515',
    created: 1238616104000,
    type: 'post',
    text: 'April Fool\'s Day is the one day you can believe everything you read on the internet. Just stay away from the TV.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1433365642',
    created: 1238613079000,
    type: 'post',
    text: 'I\'ve been on a bug parade with Seam, fixing little issues here and there and sprucing up seam-gen. Part of my zero Inbox initiative.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1433362649',
    created: 1238613051000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> I was speaking of the rounded corners ;) ...and the various features like the word of the day, reply change, and trending topics<br><br>In reply to: <a href="https://x.com/dhanji/status/1429301091" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">1429301091</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1429294405',
    created: 1238558838000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> Situations like this are exactly why I am still rooted in VIM. I\'m actually trying to be less hardcore.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1428649344" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1428649344</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1429288946',
    created: 1238558769000,
    type: 'post',
    text: 'Twitter\'s web interface is changing like the weather. Can\'t make up their mind?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1429283502',
    created: 1238558704000,
    type: 'post',
    text: 'Latest review of Seam in Action: "For the first time, I\'m understanding what Seam is and how to use it. Very good job. Highly recommended."',
    likes: 0,
    retweets: 0
  },
  {
    id: '1427731808',
    created: 1238541777000,
    type: 'post',
    text: 'The battery died in my laptop and now Eclipse is behaving like a 2 year old, refuses to let me save a file because of infinite build loop.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1426194893',
    created: 1238525647000,
    type: 'post',
    text: 'Hot damn! 1000 tweets! I guess you could say I am enjoying this exercise. Whatever gets me to write more.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1426185323',
    created: 1238525546000,
    type: 'post',
    text: 'Visited t-mobile to begin research in broadband access for laptop and web-enabled phone. G1 wasn\'t setup right. Was impressed by Sidekick.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1425396669',
    created: 1238517660000,
    type: 'post',
    text: 'It appears MyFaces is putting itself up against Bean Validation for JSF: <a href="http://wiki.apache.org/myfaces/Extensions/Validator/" rel="noopener noreferrer" target="_blank">wiki.apache.org/myfaces/Extensions/Validator/</a> Why duplicate?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1422449683',
    created: 1238473219000,
    type: 'post',
    text: 'I dig really big fonts in login screens. Just feels more vivid and welcoming.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1422435706',
    created: 1238473029000,
    type: 'post',
    text: 'My sister-in-law is raising money for leukemia. Asking for donations. <a href="http://pages.teamintraining.org/nyc/anchor09/awhite1fr5" rel="noopener noreferrer" target="_blank">pages.teamintraining.org/nyc/anchor09/awhite1fr5</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1421065327',
    created: 1238456620000,
    type: 'post',
    text: 'Tiger Woods\' victory at Arnold Palmer Invitational produced the highest overnight television rating since the U.S. Open.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1420091996',
    created: 1238445796000,
    type: 'post',
    text: 'Speaker presentations from TSSJS 2009 available: <a href="http://javasymposium.techtarget.com/html/resources.html#presentations" rel="noopener noreferrer" target="_blank">javasymposium.techtarget.com/html/resources.html#presentations</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1418680512',
    created: 1238430568000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/jbossnews" rel="noopener noreferrer" target="_blank">@jbossnews</a> RT After 8 years at JBoss - Sacha Labourey is moving on :( <a href="http://sacha.labourey.com/2009/03/29/i-am-leaving-red-hat-onward/" rel="noopener noreferrer" target="_blank">sacha.labourey.com/2009/03/29/i-am-leaving-red-hat-onward/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1418018043',
    created: 1238423671000,
    type: 'post',
    text: 'Btw, I had to install DB2 to check the validity of a query. I lot of work for a simple answer. It\'s like cross-browser testing. Sigh.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1416283105',
    created: 1238391701000,
    type: 'post',
    text: 'It took me a loooong time tonight to install DB2. What a pain. Had to do lots of Googling to find the magic set of commands. Finally got it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1414988951',
    created: 1238373774000,
    type: 'post',
    text: 'Another late Saturday afternoon. Another clutch put. Another Tiger Woods victory. If that doesn\'t make you pumped about golf, nothing will.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1410990007',
    created: 1238305644000,
    type: 'post',
    text: 'Part 2 of my JSF performance article is finally up! It\'s all about the Ajax! <a href="http://www.jsfcentral.com/articles/speed_up_your_jsf_app_2.html" rel="noopener noreferrer" target="_blank">www.jsfcentral.com/articles/speed_up_your_jsf_app_2.html</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1408680317',
    created: 1238272517000,
    type: 'post',
    text: 'I can go to Panera Bread and get internet for free. But when I go to hotel, which specializes in guest services, I have to pay $15/day. WTF?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1406495329',
    created: 1238240019000,
    type: 'post',
    text: 'Just got done playing with Metawidget for the first time. It\'s a great time saver for creating UI forms. Examples will be in Seam soon.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1404922533',
    created: 1238206244000,
    type: 'post',
    text: 'Good thing my wife was driving home. I zonked out after 10 minutes.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1404910413',
    created: 1238206083000,
    type: 'post',
    text: '#phillyete has become a major player in conferences with the success of this year\'s event. I\'d like to see a more spacious venue next year.',
    likes: 0,
    retweets: 0,
    tags: ['phillyete']
  },
  {
    id: '1402652978',
    created: 1238179620000,
    type: 'post',
    text: 'I keep getting in discussions with people about next gen Java. Scala, Fan, Groovy all discussed. My idea is Groovy in typesafe mood.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1402639886',
    created: 1238179477000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/ChariotSolution" rel="noopener noreferrer" target="_blank">@ChariotSolution</a> RT Talk by Jascha Franklin-Hodge was brilliant. Technologists working with politicians. #phillyete',
    likes: 0,
    retweets: 0,
    tags: ['phillyete']
  },
  {
    id: '1402600568',
    created: 1238179059000,
    type: 'post',
    text: 'Darn, I went over again on my talk. I just get so excited about the rule-based and ACL security demo that I put together.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1401387504',
    created: 1238166507000,
    type: 'post',
    text: 'Played Guitar Hero for the first time ever last night. Did surprisingly well on easy. Beat Robert Hanson but got killed by Kim from Manning.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1401362731',
    created: 1238166254000,
    type: 'post',
    text: 'I spent the night debating why JCDI (JSR-299) is critical for Java EE and why it\'s worth investing in standards. Uphill battle sometimes.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1401341962',
    created: 1238166046000,
    type: 'post',
    text: 'Getting ready for my talk at #phillyete on Seam security. I trimmed it slightly so that I can fit it all, but it\'s still going to move fast.',
    likes: 0,
    retweets: 0,
    tags: ['phillyete']
  },
  {
    id: '1396830433',
    created: 1238101699000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> I was very impressed. I liked the slick animations. I told him he should add a social networking aspect to it. No pressure ;)<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1396779261',
    created: 1238101181000,
    type: 'post',
    text: 'Marjan, publisher of Manning, showed me early version of my.manning website. We brainstormed how to make a book more interactive.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1396740455',
    created: 1238100771000,
    type: 'post',
    text: 'The sample code for my demos at TSSJS 2009 are now available in SVN: <a href="http://seaminaction.googlecode.com/svn/demos/presentations" rel="noopener noreferrer" target="_blank">seaminaction.googlecode.com/svn/demos/presentations</a> Same repository for all future demos.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1396706593',
    created: 1238100412000,
    type: 'post',
    text: 'My Community One proposal about running Seam on GlassFish was accepted! Yeah, two talks at J1!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1396653761',
    created: 1238099847000,
    type: 'post',
    text: 'What I really want to see is a RichFaces time zone selector component like you find in an operating system. <a href="http://static.opensuse.org/hosts/www.o.o/images/screenshots/zoom/10.jpg" rel="noopener noreferrer" target="_blank">static.opensuse.org/hosts/www.o.o/images/screenshots/zoom/10.jpg</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1396632834',
    created: 1238099623000,
    type: 'post',
    text: 'Just added a timezone list component to Seam contributed by Peter Hilton. It\'s a small thing, but should have a big impact.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1396628994',
    created: 1238099584000,
    type: 'post',
    text: 'So I missed my first due date for my corporate credit card. Doh! Crept up on me faster than I realized and I don\'t yet have autopay setup.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1396616656',
    created: 1238099454000,
    type: 'post',
    text: 'The panel went OK. I didn\'t quite get the right questions to get my point across. I should write a blog entry about my writing experience.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1395244715',
    created: 1238084399000,
    type: 'post',
    text: 'About to join the panel discussion at #phillyete about being a Manning author. I have plenty to say, believe me.',
    likes: 0,
    retweets: 0,
    tags: ['phillyete']
  },
  {
    id: '1394927468',
    created: 1238081175000,
    type: 'post',
    text: 'Listening to Michael was a joy. I could talk to him for hours about open source, in particular listening to his stories. He reads so much.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1394845829',
    created: 1238080345000,
    type: 'post',
    text: 'Asked the panel on OSS licenses what we can learn from Sun fumbling to transistion to open source.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1394828922',
    created: 1238080176000,
    type: 'post',
    text: 'Glad to here Michael admit that the initial Fedora initiative was a disaster and led to the rise of Ubuntu.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1394119062',
    created: 1238072231000,
    type: 'post',
    text: 'Thrilled to be listening to Michael Tiemann, VP of Open Source Affairs at Red Hat, at #phillyete',
    likes: 0,
    retweets: 0,
    tags: ['phillyete']
  },
  {
    id: '1388240034',
    created: 1237992130000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rayploski" rel="noopener noreferrer" target="_blank">@rayploski</a> Yeah, considering I had to pay $45 for in-room internet while at TSSJS 2009, I\'m sure I can find a cheaper arrangement.<br><br>In reply to: <a href="https://x.com/rayploski/status/1387496637" rel="noopener noreferrer" target="_blank">@rayploski</a> <span class="status">1387496637</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1386498723',
    created: 1237960030000,
    type: 'post',
    text: 'Read Practical RichFaces on the plane coming back from Vegas. The missing manual has been found! You have to read around some rough spots.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1386488560',
    created: 1237959839000,
    type: 'post',
    text: 'I\'m adamant about solving my lack of internet on the road issue. Time for gPhone perhaps? I would like my laptop to be part of the plan.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1386482182',
    created: 1237959712000,
    type: 'post',
    text: 'Suffering from jet laaaaaaaaaag. Okay, perhaps a little post-Vegas hangover too (lack of sleep and too much excitement, not too much booze).',
    likes: 0,
    retweets: 0
  },
  {
    id: '1386478999',
    created: 1237959650000,
    type: 'post',
    text: 'Round two of my Seam Security presentation starts on Friday at #phillyete. I\'ll also be participating in the Manning Luncheon Gab.',
    likes: 0,
    retweets: 0,
    tags: ['phillyete']
  },
  {
    id: '1382825622',
    created: 1237914904000,
    type: 'post',
    text: 'Post a question to be answered by the Manning authors at the Luncheon Gab at #phillyete <a href="http://phillyemergingtech.com/comment.php" rel="noopener noreferrer" target="_blank">phillyemergingtech.com/comment.php</a>',
    likes: 1,
    retweets: 0,
    tags: ['phillyete']
  },
  {
    id: '1377911148',
    created: 1237843160000,
    type: 'post',
    text: 'It\'s definitely easier to get to Vegas than it is to leave. The airport situation is shakey.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1377889543',
    created: 1237842914000,
    type: 'post',
    text: 'Mystere was a bit strange at times, but certainly entertaining. Tip: avoid section 100 unless you want to be the show :) We were safe in ...',
    likes: 0,
    retweets: 0
  },
  {
    id: '1373951481',
    created: 1237782609000,
    type: 'post',
    text: 'About to be dazzled by Mystere at Teasure Island. One last night on the Strip.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1368360713',
    created: 1237682960000,
    type: 'post',
    text: 'Just walked the entire lenght of the strip on one drink. Need to refuel.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1367654544',
    created: 1237671964000,
    type: 'post',
    text: 'Hunting down tickets for a show after a good lunch at he Venetian. Need to get more pictures today too.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1365188265',
    created: 1237625766000,
    type: 'post',
    text: 'Twitter proved more reliable than sms on the strip',
    likes: 0,
    retweets: 0
  },
  {
    id: '1363317169',
    created: 1237591350000,
    type: 'post',
    text: 'And that\'s a wrap! Time to go lounge at the pool with some post conference elixir.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1363301680',
    created: 1237591145000,
    type: 'post',
    text: 'I was getting phone calls and text messages during my talk from folks saying happy b-day hahaha',
    likes: 0,
    retweets: 0
  },
  {
    id: '1362255600',
    created: 1237578688000,
    type: 'post',
    text: 'Just finished my talk on Seam Security at #tssjs. I packed in a lot of info, probably too much, but I had *lots* of demos! Links coming.',
    likes: 0,
    retweets: 0,
    tags: ['tssjs']
  },
  {
    id: '1357600561',
    created: 1237509437000,
    type: 'post',
    text: 'I am super excited about Java EE 6. It really is going to be good.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1356221411',
    created: 1237491550000,
    type: 'post',
    text: 'REST is a great interface for JMS. We could have used this at CodeRyte as we tried to interoperate between Perl and Java.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1356061732',
    created: 1237489565000,
    type: 'post',
    text: 'Bill Burke is saying that stateful application\'s don\'t scale. That\'s different from Seam\'s message. Also denounces conversation+REST.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1355975816',
    created: 1237488512000,
    type: 'post',
    text: 'I\'m in Bill Burke\'s talk on REST at #tssjs. I finally tried out Seam\'s REST integration w/ RESTeasy and it is amazingly straightforward.',
    likes: 0,
    retweets: 0,
    tags: ['tssjs']
  },
  {
    id: '1355954801',
    created: 1237488294000,
    type: 'post',
    text: 'Just announced the seam-gen command that adds an identity management front-end to a seam-gen project: <a href="https://in.relation.to/2009/03/19/put-seams-identity-management-to-practice-using-seamgen/" rel="noopener noreferrer" target="_blank">in.relation.to/2009/03/19/put-seams-identity-management-to-practice-using-seamgen/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1355915923',
    created: 1237487797000,
    type: 'post',
    text: 'Maven follows convention instead of configuration, as opposed to convention over configuration. If you don\'t like it, too damn bad.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1355909240',
    created: 1237487700000,
    type: 'post',
    text: 'After learning Ivy, I envisioned something equivalent that would add a software build life cycle into Ant. Gradle seems to fill that void.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1355903846',
    created: 1237487627000,
    type: 'post',
    text: 'Just learned that Hans was the founder of JBoss-IDE.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1355903001',
    created: 1237487616000,
    type: 'post',
    text: 'I\'m in Hans Dockter\'s talk about Gradle at #tssjs2009. By show of hands, he\'s speaking to a room of mostly Maven 2 users. Target audience.',
    likes: 0,
    retweets: 0,
    tags: ['tssjs2009']
  },
  {
    id: '1353373902',
    created: 1237451637000,
    type: 'post',
    text: 'Last night had a smoking room as Caesars. Sucked. Tonight, 10 floors up, no smoking, and killer view of the strip.',
    photos: ['<div class="entry"><img class="photo" src="media/1353373902.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1353005292',
    created: 1237442454000,
    type: 'post',
    text: 'Eating across the street from the Bellagio at the Paris. Stunning view of the fountains.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1352073108',
    created: 1237428269000,
    type: 'post',
    text: 'Just spoke with the creator of Gradel, Hans. Finally someone who gets real world build environments.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1351078200',
    created: 1237415788000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> We just need to revisit JSF messages in JSF 2.1. Something is just not right about them in my mind.<br><br>In reply to: <a href="https://x.com/jasondlee/status/1350090935" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">1350090935</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1351068648',
    created: 1237415678000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Kito mentioned the Bean Validation and urged everyone in the room to attend your talk. Forgot to mention that.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/1350314310" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">1350314310</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1349949699',
    created: 1237402909000,
    type: 'post',
    text: 'Adding a status message in JSF is still ugly.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1349934774',
    created: 1237402751000,
    type: 'post',
    text: 'I\'ll never use the JSF managed bean annotations. They are child\'s play next to Web Beans.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1349906066',
    created: 1237402428000,
    type: 'post',
    text: 'Kito is running JBDS',
    likes: 0,
    retweets: 0
  },
  {
    id: '1349902165',
    created: 1237402385000,
    type: 'post',
    text: 'It\'s incredible how many features in Kito\'s list were added after the first draft released in nov',
    likes: 0,
    retweets: 0
  },
  {
    id: '1349811830',
    created: 1237401373000,
    type: 'post',
    text: 'I\'m listening in on Kito\'s talk about the upcoming features in JSF 2.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1347044725',
    created: 1237357814000,
    type: 'post',
    text: 'Just took in my first walk through of a Vegas Casino. Madness.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1346713009',
    created: 1237351158000,
    type: 'post',
    text: 'The MGM is wearing a green skin. Way cool!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1346705508',
    created: 1237351023000,
    type: 'post',
    text: 'Free drink of choice on Southwest to celebrate green day. Cheers to us.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1345055345',
    created: 1237329247000,
    type: 'post',
    text: 'My wife and I have a very different idea of what packing is. For me there is an optimal place for everything. My wife just throws everyt ...',
    likes: 0,
    retweets: 0
  },
  {
    id: '1344406947',
    created: 1237322052000,
    type: 'post',
    text: 'Getting ready to head out the door for Vegas. It always takes me much longer than I want it to take to get packed.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1342512754',
    created: 1237302239000,
    type: 'post',
    text: '...and part 2 of that Seam interview is now online: <a href="http://techcast.chariotsolutions.com/index.php?post_id=444317" rel="noopener noreferrer" target="_blank">techcast.chariotsolutions.com/index.php?post_id=444317</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1340994691',
    created: 1237274204000,
    type: 'post',
    text: 'I just dipped below 100 starred messages too.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1340993987',
    created: 1237274185000,
    type: 'post',
    text: 'Just made the 100th post on my blog. Add that to the long list of major milestones I\'ve hit while 30.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1340906329',
    created: 1237271916000,
    type: 'post',
    text: 'First part of my podcast about Seam, moderated by Ken Rimple from Chariot Solutions, published here <a href="http://techcast.chariotsolutions.com/index.php?post_id=443923" rel="noopener noreferrer" target="_blank">techcast.chariotsolutions.com/index.php?post_id=443923</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1339968711',
    created: 1237256015000,
    type: 'post',
    text: 'I\'m convinced that my computer\'s speed is inversely proportional to my desire to get things done. No matter how big a processor I choose.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1337913641',
    created: 1237230873000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> clued me into the JPA Details view in Eclipse (Dali plugin). An extremely useful form view for setting up a JPA entity class.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1329559512',
    created: 1237080064000,
    type: 'post',
    text: 'Just picked up a pair of black Oakley Straights to go with my new eyes. Seeing rocks.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1328243764',
    created: 1237059184000,
    type: 'post',
    text: 'IT adversizing is starting to pick up in golf. CA is sponsoring the tournament this weekend. Accenture getting lots of face time too.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1327799149',
    created: 1237052446000,
    type: 'post',
    text: 'Alternative energy will consist of a diverse profile, many of which have yet to be invented. Those who lack hope try to generalize.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1327796972',
    created: 1237052413000,
    type: 'post',
    text: 'I watched Earth: The Sequel. Definitely worth watching. Pays tribute to the entrepreneurial spirit. <a href="http://blogs.edf.org/greenroom/2009/03/12/did-you-watch-earth-the-sequel-on-discovery-last-night/" rel="noopener noreferrer" target="_blank">blogs.edf.org/greenroom/2009/03/12/did-you-watch-earth-the-sequel-on-discovery-last-night/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1317931501',
    created: 1236888356000,
    type: 'post',
    text: 'Just finished up a podcast with Ken Rimple @ Chariot Solutions. I discussed how I got into Seam, how it improves Java EE, and what\'s next.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1317705352',
    created: 1236885711000,
    type: 'post',
    text: 'I\'m officially a member of the JSF Expert Group, for better or for worse ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '1302093986',
    created: 1236630379000,
    type: 'post',
    text: 'First beta of Web Beans is out! <a href="http://in.relation.to/Bloggers/FirstBetaOfWebBeansAvailable" rel="noopener noreferrer" target="_blank">in.relation.to/Bloggers/FirstBetaOfWebBeansAvailable</a> Web Beans is the JSR-299 reference impl',
    likes: 0,
    retweets: 0
  },
  {
    id: '1301983309',
    created: 1236628905000,
    type: 'post',
    text: 'Also check out the interview with Jay Balunas, lead of the RichFaces project. <a href="http://java.dzone.com/articles/richfaces-101-qa-jay-balunas" rel="noopener noreferrer" target="_blank">java.dzone.com/articles/richfaces-101-qa-jay-balunas</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1301980023',
    created: 1236628861000,
    type: 'post',
    text: 'RichFaces Refcard released today! <a href="http://refcardz.dzone.com/refcardz/richfaces" rel="noopener noreferrer" target="_blank">refcardz.dzone.com/refcardz/richfaces</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1294435920',
    created: 1236467787000,
    type: 'post',
    text: 'The #watchmen that was seen touring the 2008 European Java conference circuit.',
    photos: ['<div class="entry"><img class="photo" src="media/1294435920.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['watchmen']
  },
  {
    id: '1294308450',
    created: 1236465252000,
    type: 'post',
    text: 'Checked out the movie In Bruges since I just visited. Atrocious movie. Just watch it with the sound off and skip end to see the sites.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1289865337',
    created: 1236370177000,
    type: 'post',
    text: 'The JSF EG is moving out into the open. Yeah!!! <a href="http://weblogs.java.net/blog/edburns/archive/2009/03/response_to_a_c.html" rel="noopener noreferrer" target="_blank">weblogs.java.net/blog/edburns/archive/2009/03/response_to_a_c.html</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1289856669',
    created: 1236370065000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/atonse" rel="noopener noreferrer" target="_blank">@atonse</a> There will be plenty more meetups in the future. Drop me an email and we\'ll add you to the list.<br><br>In reply to: @atonse <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1286948866',
    created: 1236314334000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/crichardson" rel="noopener noreferrer" target="_blank">@crichardson</a> Levent Gurses praised cloudfoundry.com tonight at the tech meetup. He said the tool works great! You have fans!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1285939894',
    created: 1236297256000,
    type: 'post',
    text: 'MD and VA might as well be three states apart for how big a barrier 495-W is.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1285363369',
    created: 1236289029000,
    type: 'post',
    text: 'Tech Hour tonight @ Gordon Biersch in Tysons, 7PM. If you\'re in the area, be there.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1285356282',
    created: 1236288936000,
    type: 'post',
    text: 'Irritated I didn\'t get the Golf Channel, I called Verizon. They switched me to a new bundle at less cost. Got upgraded to 20Mbps upload too.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1285156066',
    created: 1236286292000,
    type: 'post',
    text: 'Today CNN had a women from Canada on Skype debating with a senator over the health care initiative. That\'s one way to talk to your senator.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1285151676',
    created: 1236286236000,
    type: 'post',
    text: 'It\'s amazing how CNN is integrating the social web into their live broadcasts. Facebook, Twitter, MySpace, and even Skype.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1279848579',
    created: 1236193849000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Could be a good candidate for the Undocumented JSF list. It\'s documented, but not well known. I didn\'t know about it for a while.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1279844582',
    created: 1236193796000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> I just remove the Inbox label. Then it is in "archived" mode, which is like trash but not vaporized.<br><br>In reply to: <a href="https://x.com/tech4j/status/1279836622" rel="noopener noreferrer" target="_blank">@tech4j</a> <span class="status">1279836622</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1279711314',
    created: 1236191967000,
    type: 'post',
    text: 'I\'m liking FaceBook for sharing photos. First online sharing photo app that I\'ve been happy with, both for sharing and viewing.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1279698843',
    created: 1236191791000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> I love labels in Gmail. Frankly, I think folders should die. I tag some messages with up to 5 labels.<br><br>In reply to: <a href="https://x.com/tech4j/status/1273480200" rel="noopener noreferrer" target="_blank">@tech4j</a> <span class="status">1273480200</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1279685530',
    created: 1236191606000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> I didn\'t realize that the .xhtml extension isn\'t required in implicit navigation. Very cool!<br><br>In reply to: <a href="https://x.com/jasondlee/status/1275621903" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">1275621903</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1279678780',
    created: 1236191511000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> We\'ve been saying that for a long time (i.e., that you don\'t need faces-config.xml).<br><br>In reply to: <a href="https://x.com/jasondlee/status/1275621903" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">1275621903</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1279664276',
    created: 1236191315000,
    type: 'post',
    text: 'Liberty is like a big backyard where you go sledding. Edges of trails not well defined, no high speed lift. Good terrain park though.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1279656136',
    created: 1236191203000,
    type: 'post',
    text: 'Tried out Liberty Mountain for first time on Monday. Only took me 5 min to determine that I like Whitetail much better. Instinct == correct.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1279647149',
    created: 1236191081000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> A 3 hour nap and the eye fairy leaves 20/20 vision under your pillow. You should mostly avoid computer screens for 2 days.<br><br>In reply to: <a href="https://x.com/tech4j/status/1278327622" rel="noopener noreferrer" target="_blank">@tech4j</a> <span class="status">1278327622</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1279496432',
    created: 1236189001000,
    type: 'post',
    text: 'However, I was one of the 25% that gets a bruise on the white part of my eye from the pressure. Should be all clear in a couple of days.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1279491353',
    created: 1236188930000,
    type: 'post',
    text: '20/20 vision baby!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1278321079',
    created: 1236171069000,
    type: 'post',
    text: 'So far, so good.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1273458172',
    created: 1236082356000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> I was digging out of the trenches. I will never keep it exactly at 0, but I started at 500 with the oldest unread being 3+ years.<br><br>In reply to: <a href="https://x.com/tech4j/status/1273214376" rel="noopener noreferrer" target="_blank">@tech4j</a> <span class="status">1273214376</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1272718082',
    created: 1236060396000,
    type: 'post',
    text: 'Next milestone: 0 starred messages. Those are tougher to complete.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1272717014',
    created: 1236060372000,
    type: 'post',
    text: 'Finally! 0 unread messages in my INBOX! First time in nearly a decade. And I didn\'t cheat!',
    photos: ['<div class="entry"><img class="photo" src="media/1272717014.png"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1272421395',
    created: 1236054079000,
    type: 'post',
    text: 'Tomorrow is the day my eyes meet laser! I\'ve been a good patient and done all my pre-op, so my fingers are crossed that it goes smoothly.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1272286387',
    created: 1236051755000,
    type: 'post',
    text: 'I was hoping winter would hang on for one last tour of the slopes. I never imagined that snow would be an *obstacle* for going.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1272290156',
    created: 1236033766000,
    type: 'post',
    text: 'You need to stretch before any other exercise. Why would snowboarding be different. Pulled a muscle in my right leg in the first hour. Ouch.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1269057645',
    created: 1235998526000,
    type: 'post',
    text: 'I have 1 unread message in my inbox. I\'m so close. Waiting for the right time to have the big moment...',
    likes: 0,
    retweets: 0
  },
  {
    id: '1267332708',
    created: 1235955167000,
    type: 'post',
    text: 'I can\'t believe it, it\'s snowing again. No doubt about it, the groundhog saw his shadow.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1267179081',
    created: 1235952114000,
    type: 'post',
    text: 'I envy CNN\'s magic wall.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1264437921',
    created: 1235881464000,
    type: 'post',
    text: 'According to Robin Williams: "America is officially out of rehab" <a href="https://youtu.be/puMz1Q3E000" rel="noopener noreferrer" target="_blank">youtu.be/puMz1Q3E000</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1263957706',
    created: 1235870442000,
    type: 'post',
    text: 'JBoss Tools is very good at respecting that relationship. The approach it takes is "project archives" which I hope to study in more depth.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1263954957',
    created: 1235870381000,
    type: 'post',
    text: 'I\'ve always said that what most IDE vendors don\'t get is that an IDE does not own the build or libraries. That\'s the role of the build tool.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1263707426',
    created: 1235864994000,
    type: 'post',
    text: 'I just found out my brother is currently in Melbourne, Australia. He saw the Phillip Island penguins last night. I\'m jealous.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1263681028',
    created: 1235864431000,
    type: 'post',
    text: 'I\'ve been posting a lot, but not digging into the archives. Definitely need to make use of this tool: <a href="http://www.twingly.com/microblogsearch" rel="noopener noreferrer" target="_blank">www.twingly.com/microblogsearch</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1263671796',
    created: 1235864241000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danfusion" rel="noopener noreferrer" target="_blank">@danfusion</a> I look forward to your review of Seam in Action. Thanks in advance ;)<br><br>In reply to: <a href="https://x.com/danfusion/status/1224005427" rel="noopener noreferrer" target="_blank">@danfusion</a> <span class="status">1224005427</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1261448405',
    created: 1235806515000,
    type: 'post',
    text: 'Here\'s my favorite commercial from the Live Earth concert on 7/7/07: Think, Change Habit. <a href="https://youtu.be/oBiDCEXYWFw" rel="noopener noreferrer" target="_blank">youtu.be/oBiDCEXYWFw</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1261380143',
    created: 1235804032000,
    type: 'post',
    text: 'Ah, I see the problem. In the Starred folder, the y key removes the star but doesn\'t remove the Inbox label, the later being the default.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1261356790',
    created: 1235803220000,
    type: 'post',
    text: 'There is a really annoying bug in Gmail. If you are in Starred or Spam messages and remove Star or Spam label, it goes back to the Inbox.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1261207688',
    created: 1235798702000,
    type: 'post',
    text: 'Now that Facelets is set to become a standard in JSF 2, I wonder if interest in Gracelets will explode: <a href="http://gracelets.sourceforge.net" rel="noopener noreferrer" target="_blank">gracelets.sourceforge.net</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1261016471',
    created: 1235793861000,
    type: 'post',
    text: 'I just added a link to the JSF 2.1 proposal page on the <a href="http://seamframework.org" rel="noopener noreferrer" target="_blank">seamframework.org</a> home page.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1260500782',
    created: 1235782536000,
    type: 'post',
    text: 'One major void that still exists in the open source hosted space is a place to run a sample application. Not just a hosted app. Your app.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1260493596',
    created: 1235782382000,
    type: 'post',
    text: 'In the latest newsletter, SourceForge.net just announced that it has added support for Git due to overwhelmingly popular demand.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1259804020',
    created: 1235770085000,
    type: 'post',
    text: 'It just hit me that today is Friday. All day I was thinking it was Thursday, even though the radio DJ\'s said "the weekend is here" Hmmm.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1259592821',
    created: 1235766799000,
    type: 'post',
    text: 'Conceptualizing a sample app is difficult. It\'s like looking at a toolbox and trying to think of something to build that uses all the tools.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1259378667',
    created: 1235763520000,
    type: 'post',
    text: 'I\'ve started a FAQ that lays out the difference between SeamTest and JSFUnit. Feel free to expand: <a href="http://seamframework.org/Documentation/WhatIsTheDifferenceBetweenSeamTestAndJSFUnit" rel="noopener noreferrer" target="_blank">seamframework.org/Documentation/WhatIsTheDifferenceBetweenSeamTestAndJSFUnit</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1259279481',
    created: 1235761993000,
    type: 'post',
    text: 'It\'s time for Facelets to use XSD. Read my call to action here: <a href="http://in.relation.to/10752.lace" rel="noopener noreferrer" target="_blank">in.relation.to/10752.lace</a> I include an implementation to play with.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1259271484',
    created: 1235761861000,
    type: 'post',
    text: 'Want to play a role in Web Beans? JBoss has a 6 week gig open to implement the XML config: <a href="http://seamframework.org/Community/ContractorNeededForWebBeansRI6WeekContract" rel="noopener noreferrer" target="_blank">seamframework.org/Community/ContractorNeededForWebBeansRI6WeekContract</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1258735276',
    created: 1235753539000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> Yep, that\'s where I was going with that. Life cycles need to be rigid and accommodating. Usually they are more of the first.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1258182506" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1258182506</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1256901616',
    created: 1235710211000,
    type: 'post',
    text: 'A life cycle (JSF, Maven 2) feels rigid at first. You trade freedom for productivity. First cut is often incomplete, you must stick with it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1256600626',
    created: 1235704220000,
    type: 'post',
    text: 'I just finished the "my talk got accepted at #javaone dance" Conversations and page flows in JSF. Now I\'m tired. Time to go to sleep.',
    likes: 0,
    retweets: 0,
    tags: ['javaone']
  },
  {
    id: '1256594022',
    created: 1235704093000,
    type: 'post',
    text: 'My JSF performance article got highlighted in the most recent JBoss Developer. Thanks Sacha! Part 2 is on its way!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1255934200',
    created: 1235692134000,
    type: 'post',
    text: 'Super rough sketch of proposal for multi-field validation in JSF w/ Bean Validation: <a href="http://seamframework.org/Documentation/JSF21#H-MulticomponentValidationUsingBeanValidationP1" rel="noopener noreferrer" target="_blank">seamframework.org/Documentation/JSF21#H-MulticomponentValidationUsingBeanValidationP1</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1255526268',
    created: 1235685579000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Lukefx" rel="noopener noreferrer" target="_blank">@Lukefx</a> Yeah, it\'s pretty lame that the Seam Wiki doesn\'t render in Opera 9.6. I don\'t know the whole story, I just provided my solution ;)<br><br>In reply to: <a href="https://x.com/Lukefx/status/1251235235" rel="noopener noreferrer" target="_blank">@Lukefx</a> <span class="status">1251235235</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1255521117',
    created: 1235685504000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maeste" rel="noopener noreferrer" target="_blank">@maeste</a> Nope, Seam serializes access to conversation &amp; session, so you are covered either way.<br><br>In reply to: <a href="https://x.com/maeste/status/1255392462" rel="noopener noreferrer" target="_blank">@maeste</a> <span class="status">1255392462</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1255510408',
    created: 1235685340000,
    type: 'post',
    text: 'I\'m all booked for TSS 2009 and ready for the Vegas invasion! 4 nights at Caesar\'s Palace then a weekend vacation (honeymoon in Vegas?)',
    likes: 0,
    retweets: 0
  },
  {
    id: '1255486169',
    created: 1235684980000,
    type: 'post',
    text: 'Added proposal for UIData to support java.util.Collection. It\'s already been accepted unofficially: <a href="http://seamframework.org/Documentation/JSF21#H-SupportForCollectionInterfaceInUIDataP1" rel="noopener noreferrer" target="_blank">seamframework.org/Documentation/JSF21#H-SupportForCollectionInterfaceInUIDataP1</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1255175880',
    created: 1235680385000,
    type: 'post',
    text: 'My proposal for view actions is available: <a href="http://seamframework.org/Documentation/JSF21#H-ViewActionsP1" rel="noopener noreferrer" target="_blank">seamframework.org/Documentation/JSF21#H-ViewActionsP1</a> I wish it could have made JSF 2.0.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1254508139',
    created: 1235670406000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fridgebuzz" rel="noopener noreferrer" target="_blank">@fridgebuzz</a> Thanks! I love hearing that the book made a difference. That\'s all I want to know, really.<br><br>In reply to: <a href="https://x.com/fridgebuzz/status/1254473417" rel="noopener noreferrer" target="_blank">@fridgebuzz</a> <span class="status">1254473417</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1254386696',
    created: 1235668582000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fridgebuzz" rel="noopener noreferrer" target="_blank">@fridgebuzz</a> I\'m glad your experience with the Seam community has been good. Stay patient w/ JSF, we are working hard to improve it.<br><br>In reply to: <a href="https://x.com/fridgebuzz/status/1254051262" rel="noopener noreferrer" target="_blank">@fridgebuzz</a> <span class="status">1254051262</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1250594947',
    created: 1235592024000,
    type: 'post',
    text: 'Not wanting to waste a minute, we have started a JSF 2.1 enhancement page on the Seam Wiki. <a href="http://seamframework.org/Documentation/JSF21" rel="noopener noreferrer" target="_blank">seamframework.org/Documentation/JSF21</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1250457132',
    created: 1235589937000,
    type: 'post',
    text: 'I love the new quick labeling feature in Gmail. However, it works *painfully* slow in Opera, thus making Gmail unusable in Opera.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1250438403',
    created: 1235589645000,
    type: 'post',
    text: 'Just documented how to get seamframework.org working in Opera <a href="http://seamframework.org/Documentation/HowDoIGetSeamframeworkorgToWorkInOpera" rel="noopener noreferrer" target="_blank">seamframework.org/Documentation/HowDoIGetSeamframeworkorgToWorkInOpera</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1250215814',
    created: 1235586176000,
    type: 'post',
    text: 'JSFUnit has turned GA! <a href="http://jsfunit.blogspot.com/" rel="noopener noreferrer" target="_blank">jsfunit.blogspot.com/</a> Now I really have no excuse to put off learning and using this framework regularly.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1249803567',
    created: 1235579887000,
    type: 'post',
    text: 'Am I misinterpreting, or did Gov Bobby Jindal essentially say "Break the law when it is convenient"? Damn those law enforcing bureaucrats.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1248062697',
    created: 1235537268000,
    type: 'post',
    text: 'Soaked up the whole Presidential address to Congress. Obama is a hardworking and ambitious president. I\'m on the edge of my seat!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1247139831',
    created: 1235521164000,
    type: 'post',
    text: 'I add some book covers and the Seam UI refcard to <a href="http://seamframework.org/Documentation" rel="noopener noreferrer" target="_blank">seamframework.org/Documentation</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1247132297',
    created: 1235521030000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> It\'s just funny how a library is shunned by some developers, regardless of merit, if it doesn\'t reside in a repo.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1246727612" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1246727612</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1247126828',
    created: 1235520930000,
    type: 'post',
    text: 'Time to get time zones right in JSF: <a href="http://in.relation.to/Bloggers/StepRightUpAndSelectYourTimeZone" rel="noopener noreferrer" target="_blank">in.relation.to/Bloggers/StepRightUpAndSelectYourTimeZone</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1246723997',
    created: 1235514036000,
    type: 'post',
    text: 'Developers who use Maven need JARs in their repositoires like alcoholics need their bagged bottle. Without it, they can\'t function.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1246719688',
    created: 1235513969000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Sorry I had to miss the preso today. I had an appt I couldn\'t change at the same time. We will likely be talking soon anyway.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1246651241" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1246651241</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1243836625',
    created: 1235453474000,
    type: 'post',
    text: 'I just placed an order for a red Fully Loaded Zody Chair. My back has been a huge obstacle to getting things done lately. Time to sit right.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1243833643',
    created: 1235453399000,
    type: 'post',
    text: 'JBoss AS 5 gets it\'s first checkup, 5.0.1.GA is out! <a href="http://www.jboss.org/jbossas/downloads/" rel="noopener noreferrer" target="_blank">www.jboss.org/jbossas/downloads/</a> The release process is very streamlined now.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1243482953',
    created: 1235445575000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> A surprise is on the way to you. Hint: It\'s signed :)',
    likes: 0,
    retweets: 0
  },
  {
    id: '1243478473',
    created: 1235445484000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jbandi" rel="noopener noreferrer" target="_blank">@jbandi</a> I should have also said that I decided to buy a helmet too. I don\'t know how I snowboarded without it and lived to tell. No jumps ;)<br><br>In reply to: <a href="https://x.com/jbandi/status/1241407012" rel="noopener noreferrer" target="_blank">@jbandi</a> <span class="status">1241407012</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1243473143',
    created: 1235445375000,
    type: 'post',
    text: 'Woke up to a conference call with the JSF 2 EG to straighten out the page parameters (*eh* view parameters) proposal. Must, get, right.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1243466985',
    created: 1235445258000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> Congrats! Be sure to add a link to it here when it\'s out: <a href="http://seamframework.org/Documentation/SeamDocumentation" rel="noopener noreferrer" target="_blank">seamframework.org/Documentation/SeamDocumentation</a><br><br>In reply to: <a href="https://x.com/tech4j/status/1242151356" rel="noopener noreferrer" target="_blank">@tech4j</a> <span class="status">1242151356</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1243458879',
    created: 1235445092000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/atonse" rel="noopener noreferrer" target="_blank">@atonse</a> I see you are a local Seam hacker. Are you interested in getting involved with the project? We are always open to contributors.<br><br>In reply to: @atonse <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1241375305',
    created: 1235407543000,
    type: 'post',
    text: 'This year I decided to buy goggles to wear while snowboarding. I honestly don\'t know how I ever snowboarded or skied without them.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1241330816',
    created: 1235406779000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> I was so happy the first time I figured that out. One less pain I had to deal with when using Java.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1241058533" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1241058533</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1240026845',
    created: 1235370410000,
    type: 'post',
    text: 'View params were page params. Now it\'s view params, not page params. Why Ed changed it, I can\'t say. Ed just liked it better that way.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1240019689',
    created: 1235370169000,
    type: 'post',
    text: 'I finished revising part 2 of my JSF performance series for JSFCentral. This part highlights the use of partial page rendering w/ RichFaces.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1240017561',
    created: 1235370099000,
    type: 'post',
    text: 'Lately I\'ve been shedding tasks like a company going out of business sheds debt. I\'ve just got to get my agenda under control.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1237319859',
    created: 1235315845000,
    type: 'post',
    text: 'I have less than 30 days of being 30. It\'s been a very notable &amp; life changing year. I\'m hoping to squeeze in a few more things.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1236454882',
    created: 1235281632000,
    type: 'post',
    text: 'Nothing is more annoying than a channel that is running the same 4 ads all day long. Why do I watch live TV again?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1236453646',
    created: 1235281592000,
    type: 'post',
    text: 'Watching the Dew Action Sports Tour. Not sure I like the look of tricks done on skis. Doesn\'t look as clean as snowboarding. Still amazing.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232314108',
    created: 1235168612000,
    type: 'post',
    text: 'I\'m so sore I can\'t even bend over to tie my shoes...and I love it! I hit the ollie, the air to fakie, jump grabs, and rails. Jumps rule!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1228001626',
    created: 1235075764000,
    type: 'post',
    text: 'It is flat out snowing at Sunday River. People call this powder. HELL I\'m playing in the snow!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1215846928',
    created: 1234803905000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> I didn\'t switch from Gmail to Google Apps. No reason to go in that direction. My wife moved to Google Apps from self hosted.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1214490901" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1214490901</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1215843516',
    created: 1234803842000,
    type: 'post',
    text: 'I want to emphasize that the Ruby script I used was absolutely perfect. It did exactly what I wanted because I could easily hack it. Yeah!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1215839699',
    created: 1234803770000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/trustin" rel="noopener noreferrer" target="_blank">@trustin</a> The whole point is that I don\'t want my own SMTP server. I want e-mail administration off my plate entirely. God bless Google.<br><br>In reply to: <a href="https://x.com/trustin/status/1214512277" rel="noopener noreferrer" target="_blank">@trustin</a> <span class="status">1214512277</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1215835923',
    created: 1234803696000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/trustin" rel="noopener noreferrer" target="_blank">@trustin</a> Given my experience with having to setup the account, I didn\'t want to trust Google with switching me from premium -&gt; free later.<br><br>In reply to: <a href="https://x.com/trustin/status/1214507447" rel="noopener noreferrer" target="_blank">@trustin</a> <span class="status">1214507447</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1215833563',
    created: 1234803650000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Lukefx" rel="noopener noreferrer" target="_blank">@Lukefx</a> Yeah, except the pop3 complete destroys the integrity of all the messages. Didn\'t match my requirements == useless.<br><br>In reply to: <a href="https://x.com/Lukefx/status/1214615120" rel="noopener noreferrer" target="_blank">@Lukefx</a> <span class="status">1214615120</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1215831309',
    created: 1234803609000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maeste" rel="noopener noreferrer" target="_blank">@maeste</a> Yep, that\'s exactly why I choose to use that quote in my book.<br><br>In reply to: <a href="https://x.com/maeste/status/1206089092" rel="noopener noreferrer" target="_blank">@maeste</a> <span class="status">1206089092</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1215827703',
    created: 1234803545000,
    type: 'post',
    text: 'I saw a girl wearing the shirt "Love Pink". There wasn\'t a single pink thread in the shirt. I guess she doesn\'t love it that much.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1215825175',
    created: 1234803498000,
    type: 'post',
    text: 'My requirement was that messages needed to be in the *exact* same state when they got moved. Same labels. Same status. Very tricky task.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1215823528',
    created: 1234803465000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> I tried for hours to get imapsync to work; keep choking on gmail auth. This worked the 1st time: <a href="http://wonko.com/post/ruby_script_to_sync_email_from_any_imap_server_to_gmail" rel="noopener noreferrer" target="_blank">wonko.com/post/ruby_script_to_sync_email_from_any_imap_server_to_gmail</a><br><br>In reply to: <a href="https://x.com/maxandersen/status/1215120534" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1215120534</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1214494193',
    created: 1234764553000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> writing last chapter of book == extreme pain. Revising and editing book == goodbye to sanity. Publishing == rewarding as hell.<br><br>In reply to: <a href="https://x.com/aalmiray/status/1213343071" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">1213343071</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1214489469',
    created: 1234764383000,
    type: 'post',
    text: 'That is, aside from the fact that e-mail is completely broken and rules our lives. But that\'s a discussion to have over beers.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1214488323',
    created: 1234764341000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> I\'m super glad I\'m on Google Apps. There\'s no better option, IMO. I just don\'t get why Google doesn\'t make it easier to switch.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1214486495" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1214486495</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1214484538',
    created: 1234764207000,
    type: 'post',
    text: 'My wife is still waiting on Gmail themes in her account though. Not available in Google Apps for some reason. Okay, I\'m done ranting.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1214483584',
    created: 1234764172000,
    type: 'post',
    text: 'I migrated my wife\'s mail and decided to have my account forward to Gmail. In the end, it works great. Setting it up is a PITA.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1214480820',
    created: 1234764078000,
    type: 'post',
    text: 'It makes absolutely no sense to me that Google does not have a Gmail to Google Apps migration. Fortunately, I found a good IMAP ruby script.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1214478484',
    created: 1234764006000,
    type: 'post',
    text: 'Next step, mail migration. I had one Gmail account (mine) and one self-hosted account (my wife\'s). Migration options? 0. WTF?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1214476616',
    created: 1234763942000,
    type: 'post',
    text: 'I complained like a whining child to Google. An engineer reset my account the next morning. Look, I have no patience for arbitrary timeouts.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1214475413',
    created: 1234763898000,
    type: 'post',
    text: 'I messed up the configuration at first. I canceled the account to start with a clean slate and it said I would have to wait 7 days! Great!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1214472965',
    created: 1234763808000,
    type: 'post',
    text: 'The switch was extremely painful. Right out of the gate I was tripping all over the place.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1214472000',
    created: 1234763775000,
    type: 'post',
    text: 'I decided to switch my mail over to Google Apps because I was just done dealing with spam, timeouts, certificates, and drive capacity.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1214469427',
    created: 1234763684000,
    type: 'post',
    text: 'I never got a chance to tweet about my experience switching to Google Apps, so I\'m going to throw down some entries now about it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1213235672',
    created: 1234731465000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguard" rel="noopener noreferrer" target="_blank">@lightguard</a> That would be everyone of my tweets. I noticed that I consistently revise until I have less then 140 characters, then post.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1206747544" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1206747544</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1205084843',
    created: 1234493552000,
    type: 'post',
    text: 'Here\'s an oldy but goody that predates my twitter days. Laugh till the colors bleed from your eyes. <a href="http://teamsugar.com/group/46813/blog/771943" rel="noopener noreferrer" target="_blank">teamsugar.com/group/46813/blog/771943</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1204992103',
    created: 1234491195000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/richsharples" rel="noopener noreferrer" target="_blank">@richsharples</a> It seems the problem asadmin has is that it\'s been forgotten. Now its enjoying a rebirth. asadmin shell in V3. Sweet.<br><br>In reply to: <a href="https://x.com/richsharples/status/1204092368" rel="noopener noreferrer" target="_blank">@richsharples</a> <span class="status">1204092368</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1204193595',
    created: 1234474342000,
    type: 'post',
    text: 'You know for certain you\'ve found the one when she gives you this gift for Valentine\'s Day: <a href="https://mojavelinux.com/personal/mirage/index.php?showimage=22" rel="noopener noreferrer" target="_blank">mojavelinux.com/personal/mirage/index.php?showimage=22</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1204037853',
    created: 1234471607000,
    type: 'post',
    text: 'What a great session! I\'m so excited about asadmin in GlassFish V3. There was even mention of using Bean Validation for asadmin commands.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1203718427',
    created: 1234465937000,
    type: 'post',
    text: 'I\'m in the asadmin talk on GlassFish.tv: <a href="http://wikis.sun.com/display/TheAquarium/ASAdmin" rel="noopener noreferrer" target="_blank">wikis.sun.com/display/TheAquarium/ASAdmin</a> I\'m going to be a panelist.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1200893462',
    created: 1234396529000,
    type: 'post',
    text: 'JBoss Virtual Experience is over. I took home the prize of most posts in the chat room. I had difficulty resisting the urge to be sarcastic.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1200347531',
    created: 1234386081000,
    type: 'post',
    text: 'What I really want to do is turn all of this talking and chatting into writing. Writing is more permanent.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1200344958',
    created: 1234386038000,
    type: 'post',
    text: 'People sometimes say I talk to much (that is, when I am talking and not hiding). Today I think they discovered that I chat too much too.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1200193157',
    created: 1234383248000,
    type: 'post',
    text: 'JBoss in Action just got the thumbs up form Dimitris Andreadis, (former) JBoss AS Project Lead. Dimitris has been promoted to JBoss AS god.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1199996891',
    created: 1234379719000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Lukefx" rel="noopener noreferrer" target="_blank">@Lukefx</a> There is a presentation about "JBoss Rails" at 3:00 PM EST. Discover for yourself.<br><br>In reply to: <a href="https://x.com/Lukefx/status/1199725148" rel="noopener noreferrer" target="_blank">@Lukefx</a> <span class="status">1199725148</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1199880125',
    created: 1234377589000,
    type: 'post',
    text: 'I really like the style of the keynote at JBVE by Sacha because he is talking directly to the camera. Same for followup speakers.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1199682353',
    created: 1234373914000,
    type: 'post',
    text: 'FYI, JBoss on Rails BoF at JBoss Virtual Experience is at 4:30pm EST. <a href="http://www-2.virtualevents365.com/jboss_experience/network.php" rel="noopener noreferrer" target="_blank">www-2.virtualevents365.com/jboss_experience/network.php</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1199508169',
    created: 1234370769000,
    type: 'post',
    text: 'Let me clarify last statement. e.g. JBoss has a project to run Rails on JBoss. <a href="http://java.dzone.com/articles/odd-theses-an-interview-with-b" rel="noopener noreferrer" target="_blank">java.dzone.com/articles/odd-theses-an-interview-with-b</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1199408955',
    created: 1234369006000,
    type: 'post',
    text: 'Craig Muzilla emphasizing that JBoss is not necessarily fixed on Java and Java EE proper. Open to any languages coming out of open source.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1199403894',
    created: 1234368913000,
    type: 'post',
    text: 'JBoss Virtual Experience: <a href="http://www-2.virtualevents365.com/jboss_experience/" rel="noopener noreferrer" target="_blank">www-2.virtualevents365.com/jboss_experience/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1199402126',
    created: 1234368881000,
    type: 'post',
    text: 'The presentations at JBoss Virtual Experience are coming through great. The chat leaves some to be desired, but it\'s workable.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1198383860',
    created: 1234337686000,
    type: 'post',
    text: 'Gotta get to sleep so I can be up for my session tomorrow at JBVE. I\'ve got a full day of chatting ahead of me.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1198159262',
    created: 1234329408000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Heck, I wake up late according to Hawaiian time ;)<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1198157126',
    created: 1234329343000,
    type: 'post',
    text: 'What twisted people return an Iterator in an API? Seriously. Give me a freakin\' collection. I\'ll make my own iterator, thank you very much!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1197893756',
    created: 1234322517000,
    type: 'post',
    text: '"Find Usages" dialog in NetBeans is retarded. You\'re forced to select between "Usages" and "Overriding methods". These are not parallel.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1196930667',
    created: 1234301721000,
    type: 'post',
    text: 'Bean Validation is being strongly considered for inclusion in Java EE 6. The main reason cited is Emmanuel\'s diplomacy and hard work.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1196583118',
    created: 1234295361000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jclingan" rel="noopener noreferrer" target="_blank">@jclingan</a> You still have no logo for Mojarra. Get moving on that!<br><br>In reply to: <a href="https://x.com/jclingan/status/1196563077" rel="noopener noreferrer" target="_blank">@jclingan</a> <span class="status">1196563077</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1196579807',
    created: 1234295303000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jclingan" rel="noopener noreferrer" target="_blank">@jclingan</a> I\'m glad Sun stuck with the name GlassFish instead of just "Enterprise Server". I hate it when products don\'t use the known name.<br><br>In reply to: <a href="https://x.com/jclingan/status/1196545636" rel="noopener noreferrer" target="_blank">@jclingan</a> <span class="status">1196545636</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1196567137',
    created: 1234295077000,
    type: 'post',
    text: 'Obama addresses the press with full awareness that everyone in the world *can* hear what he is saying. Finally, a president who gets that.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1196534008',
    created: 1234294461000,
    type: 'post',
    text: 'For those who requested it, Virginia McGowan (aka MeMaw) celebrating her 85 Birthday: <a href="https://youtu.be/Sk-21mxL33s" rel="noopener noreferrer" target="_blank">youtu.be/Sk-21mxL33s</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1196487407',
    created: 1234293603000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> To get the MinoHD, you have to get 5 registration referrals for JavaOne. No small task given the price, so just go buy one.<br><br>In reply to: <a href="https://x.com/tech4j/status/1196402269" rel="noopener noreferrer" target="_blank">@tech4j</a> <span class="status">1196402269</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1196323840',
    created: 1234290598000,
    type: 'post',
    text: 'Sun is giving away Flip MinoHD portable video cameras instead of iPods this year. I got one for my brother and they are a solid product.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1196178413',
    created: 1234287916000,
    type: 'post',
    text: 'My grandmother is on YouTube and has her own blog, doings of her family "staff". I\'ll spare you the link since it\'s just a family thing.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1196139478',
    created: 1234287189000,
    type: 'post',
    text: 'The results are in! JSR-299 (Web Beans) and JSR-303 (Bean Validation) both received 14 "Yeh" votes and 0 "Neh" votes! That == acceptance!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1196125050',
    created: 1234286934000,
    type: 'post',
    text: 'I do want a Kindle (or equivalent) eventually. I believe in ebooks as an alternative to, not replacement for, books. Open format please!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1194425935',
    created: 1234240000000,
    type: 'post',
    text: 'If Google is a fat pig, then I keep it fed with my spam. Either that, or it\'s a fat pig *because* I constantly feed it spam.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1194161480',
    created: 1234233625000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> The next patch is going to be even better because we are going to proposal &lt;f:fragment&gt; (or &lt;f:panel&gt;?) in the core library.<br><br>In reply to: <a href="https://x.com/jasondlee/status/1192386766" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">1192386766</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1192624564',
    created: 1234202874000,
    type: 'post',
    text: 'should really say that the original creator of JSF doesn\'t understand how to use Java. The rest of us just inherited the mistake.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1192617569',
    created: 1234202739000,
    type: 'post',
    text: 'I can\'t understand why the JSF API uses abstract classes instead of interfaces. Sun can\'t seem to use their own language correctly.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1192331013',
    created: 1234197353000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> Don\'t worry, no double blacks for me. I\'m a cruiser. I like the smooth blues and breezy greens.<br><br>In reply to: <a href="https://x.com/rruss/status/1192267723" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">1192267723</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1192325998',
    created: 1234197263000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> The UIPanel group is now inserted dynamically by the Facelets builder so the developer doesn\'t have to worry about it. See EG.<br><br>In reply to: <a href="https://x.com/jasondlee/status/1191798054" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">1191798054</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1191060800',
    created: 1234158678000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> example: if (component instanceof UIPanel) { component.methodOnUIPanel(); }<br><br>In reply to: <a href="https://x.com/maxandersen/status/1190037722" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1190037722</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1191058528',
    created: 1234158602000,
    type: 'post',
    text: 'I just worked out how to support multiple sibling components under &lt;f:facet&gt; in Facelets. Damn hard API to extend, but works great now!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1191056498',
    created: 1234158527000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rruss" rel="noopener noreferrer" target="_blank">@rruss</a> They sure do. Sure beats rentals. I forgot you snowboarded. I\'m hoping next season to come out your way! We should pair up.<br><br>In reply to: <a href="https://x.com/rruss/status/1191046608" rel="noopener noreferrer" target="_blank">@rruss</a> <span class="status">1191046608</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1189705354',
    created: 1234125918000,
    type: 'post',
    text: 'It would be nice if the Java compiler recognized instanceof check to avoid a cast the same way it recognizes UnsupportedOperationException.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1188142994',
    created: 1234068586000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> The way I see it, what you are thinking is what you are doing. What you are actually doing is boring. "Typing on keyboard" See.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1186788660',
    created: 1234029194000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> You\'ve got to add eApps to that list. They are so in tune w/ Java. And they update you even when you\'re not going to have downtime.<br><br>In reply to: <a href="https://x.com/mraible/status/1186749852" rel="noopener noreferrer" target="_blank">@mraible</a> <span class="status">1186749852</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1186782340',
    created: 1234029026000,
    type: 'post',
    text: 'Explaining to someone what to change in a text document is an art. Typically goes something like, type "this text" but without the quotes.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1185394331',
    created: 1233976593000,
    type: 'post',
    text: 'I don\'t know why people are shocked that Phelps smoked on the grass. He\'s a big teenager. Hence, he does shit w/o thinking. Surprise.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1185390459',
    created: 1233976475000,
    type: 'post',
    text: 'My wife\'s new 5150 (aka Ride) snowboard arrived yesterday. <a href="http://www.fogdog.com/product/index.jsp?productId=3098671" rel="noopener noreferrer" target="_blank">www.fogdog.com/product/index.jsp?productId=3098671</a> She is frightenly coordinated now. Snow bunny :)',
    likes: 0,
    retweets: 0
  },
  {
    id: '1181219252',
    created: 1233871615000,
    type: 'post',
    text: 'First part of my article on JSF performance published: <a href="http://www.jsfcentral.com/articles/speed_up_your_jsf_app_1.html" rel="noopener noreferrer" target="_blank">www.jsfcentral.com/articles/speed_up_your_jsf_app_1.html</a> Trying to post on in.relation.to but it\'s down ATM.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1177532794',
    created: 1233780688000,
    type: 'post',
    text: 'Looking for a new job? A full-time JBoss AS documentation position at Red Hat is available: <a href="https://redhat.ats.hrsmart.com/cgi-bin/a/highlightjob.cgi?jobid=4032" rel="noopener noreferrer" target="_blank">redhat.ats.hrsmart.com/cgi-bin/a/highlightjob.cgi?jobid=4032</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1177332044',
    created: 1233776670000,
    type: 'post',
    text: 'In Bean Validation, you can have a different validator for each type the contraint annotation is applied to. Nice use of Java type system.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1177052248',
    created: 1233771162000,
    type: 'post',
    text: 'Finally! Gmail now has a sane way of selecting a label to assign to a message. An interactive select menu. Would make a good JSF component.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1175557198',
    created: 1233729749000,
    type: 'post',
    text: 'I finished integrating Bean Validation with JSF 2 today - the rough draft. Bean Validation is going to get a lot of "reuse" thanks to JSF 2.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1173761962',
    created: 1233686548000,
    type: 'post',
    text: 'Whoohoo! Just found out that my patch was accepted. The &lt;ui:repeat&gt; tag will support varStatus in JSF 2! <a href="https://facelets.dev.java.net/issues/show_bug.cgi?id=328" rel="noopener noreferrer" target="_blank">facelets.dev.java.net/issues/show_bug.cgi?id=328</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1173539892',
    created: 1233682131000,
    type: 'post',
    text: 'Firefox 3 is still retarded with fixed background images. I have a pretty pimped out laptop and it still chokes scrolling such a page.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1173488868',
    created: 1233681142000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxkatz" rel="noopener noreferrer" target="_blank">@maxkatz</a> Who said I can\'t drink and party at Red Hat? That\'s what conferences are for. I have discovered Belgium beer! And I\'m nuts for it.<br><br>In reply to: <a href="https://x.com/maxkatz/status/1171528246" rel="noopener noreferrer" target="_blank">@maxkatz</a> <span class="status">1171528246</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1173482461',
    created: 1233681017000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jclingan" rel="noopener noreferrer" target="_blank">@jclingan</a> Yes, the new Firefox auto-search history is soooo much more logical and intelligent.<br><br>In reply to: <a href="https://x.com/jclingan/status/1172186761" rel="noopener noreferrer" target="_blank">@jclingan</a> <span class="status">1172186761</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1171513278',
    created: 1233621655000,
    type: 'post',
    text: 'Joining Red Hat is like switching from high school to college. Could slack off before and still get As. Now I actually have to think hard.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1170490393',
    created: 1233598001000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Yeah, yeah, I know. But I\'m impatient sometimes I don\'t want to wait for the IDE to start up. We all have room for improvement.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1170449032" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1170449032</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1170359212',
    created: 1233595204000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> If you can believe it, I do a surprising amount of coding in VIM. I pretty much compile Java in my head, then build with Ant.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1170090979" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1170090979</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1170357007',
    created: 1233595155000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Working with JBDS is very important to me, don\'t get me wrong. What I should say is that NetBeans is replacing VIM for me.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1170090979" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1170090979</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1170072241',
    created: 1233589254000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> I\'m guitly too of often leaving out mention of JBoss Tools. Don\'t take offense, these are senior moments. We need to remember.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1169267772" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1169267772</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1170067745',
    created: 1233589163000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tedneward" rel="noopener noreferrer" target="_blank">@tedneward</a> If you are listening, mplayer. Mplayer is amazing. If you have a media file, and it isn\'t corrupt, mplayer can play it. Period.<br><br>In reply to: <a href="https://x.com/tedneward/status/1169437961" rel="noopener noreferrer" target="_blank">@tedneward</a> <span class="status">1169437961</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1170062447',
    created: 1233589047000,
    type: 'post',
    text: 'I\'ve been using NetBeans for all my development on the JSF 2 RI (Mojarra). I can\'t help it, it\'s instinctal. Some how it just works for me.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1168820484',
    created: 1233546295000,
    type: 'post',
    text: 'The Steelers just earned a spot on my hate list, along with Dallas and UVA. Sorry <a class="mention" href="https://x.com/gscottshaw" rel="noopener noreferrer" target="_blank">@gscottshaw</a>. Damn it, how the hell do they manage to win?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1165355607',
    created: 1233437269000,
    type: 'post',
    text: '♺ <a class="mention" href="https://x.com/mjasay" rel="noopener noreferrer" target="_blank">@mjasay</a> Open source doesn\'t make development easier, just different. It leads to superior code, but don\'t expect the road to be smooth.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1163790261',
    created: 1233376099000,
    type: 'post',
    text: 'My wife\'s grandfather had surgery today. It was swift and successful. Grandparents teach you not to take life and your health for granted.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1163784216',
    created: 1233375873000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Thanks for the great work over the past year. Community members are the ones who keep it real.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1163776464',
    created: 1233375593000,
    type: 'post',
    text: 'My session overlapped with nighttime skiing. Going up the chairlift I saw Venus hanging below a crescent moon. <a href="http://www.skyandtelescope.com/observing/ataglance" rel="noopener noreferrer" target="_blank">www.skyandtelescope.com/observing/ataglance</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1163771712',
    created: 1233375431000,
    type: 'post',
    text: 'Had a fun day at Whitetail with my wife. Skiing in the east is hit or miss. This year, it is hit.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1160141208',
    created: 1233273213000,
    type: 'post',
    text: 'Plans to play hookey got delayed until tomorrow. I was on the hook for too many things today.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1160138369',
    created: 1233273140000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Nope, credit for &lt;rewrite&gt; goes to Norman. I can claim to have inspired him since he add the feature after reviewing chapter 3.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1160119661',
    created: 1233272694000,
    type: 'post',
    text: '♺ 50% off Manning books <a href="http://ria.dzone.com/announcements/limited-time-50-all-manning-bo" rel="noopener noreferrer" target="_blank">ria.dzone.com/announcements/limited-time-50-all-manning-bo</a> Lots of great options. Hibernate Search, Dependency Injection, JBoss and Seam in Action.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1157620203',
    created: 1233204704000,
    type: 'post',
    text: 'I\'m digging the new look \'n feel of buttons in Firefox 3 on Linux. Round corners/gradient. You don\'t feel you have to style them any more.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1157600923',
    created: 1233204103000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> Yes, Groovy solves the import java.util.List problem and many others. But much time still must be spent w/ Java.<br><br>In reply to: <a href="https://x.com/JohnAment/status/1157238552" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">1157238552</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1157597655',
    created: 1233204002000,
    type: 'post',
    text: 'I should have mentioned that the demo application for JSF 2 is going to be checked into the Mojarra trunk as soon as I get the patch ready.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1157595000',
    created: 1233203917000,
    type: 'post',
    text: 'If I get some sleep tonight I might be playing hookey tomorrow at WhiteTail. Gotta get my snowboarding legs in shape for Sunday River.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1157232926',
    created: 1233194214000,
    type: 'post',
    text: 'Having to import java.util.List et al is completely ludicrous. A list is not a language extension. Same for IO. Can this be fixed in Java 7?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1156878182',
    created: 1233185407000,
    type: 'post',
    text: 'WhiteHouse.gov is serving up a hearty feed. I was skeptical as to whether it would be updated regularly enough to be useful, but it is!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1156781234',
    created: 1233183187000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> Like I said. On some days I\'m optimistic. It was a subconscious decision at the time. We\'ll see how the cards play.<br><br>In reply to: <a href="https://x.com/dhanji/status/1156748662" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">1156748662</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1156774553',
    created: 1233183036000,
    type: 'post',
    text: 'Just finished a sweet demo app for JSF 2 that uses a lot of the new features: Facelets, annotations, bookmarkable links, view parameters.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1156732525',
    created: 1233182114000,
    type: 'post',
    text: 'Some days I\'m an optimist. When I created a folder in my documents for Seam in Action, I put it inside of a folder named "books".',
    likes: 0,
    retweets: 0
  },
  {
    id: '1150935278',
    created: 1233023057000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JohnAment" rel="noopener noreferrer" target="_blank">@JohnAment</a> Good luck with your Seam project! Perhaps you could spread copies of Seam in Action around the office to help the cause.<br><br>In reply to: <a href="https://x.com/JohnAment/status/1150760187" rel="noopener noreferrer" target="_blank">@JohnAment</a> <span class="status">1150760187</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1148274130',
    created: 1232944288000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Oh good, I\'m glad to know it\'s not just me. I often envy folks who can work consistently, without ups and downs.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1148267738',
    created: 1232944053000,
    type: 'post',
    text: 'I just got finished merging the e-mail I had abandoned when I switched to Gmail 2 years ago. Painful. E-mail is not kind to me.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1146138670',
    created: 1232860211000,
    type: 'post',
    text: 'I love that I can go straight to whitehouse.gov and find out what the administration is doing, not secondhand through CNN or otherwise.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1146101424',
    created: 1232858562000,
    type: 'post',
    text: 'I\'m confirmed as a speaker at TSS 2009 and travel has been approved. Topic: Seam security. Vegas, here I come! <a href="http://javasymposium.techtarget.com/html/speakers.html#DAllen" rel="noopener noreferrer" target="_blank">javasymposium.techtarget.com/html/speakers.html#DAllen</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1145630444',
    created: 1232841513000,
    type: 'post',
    text: 'Ski conditions in the east are just sick this year. Whitetail is 100% open and Sunday River is buried under 24" of snow in &lt; 1 week. Sweet.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1145628908',
    created: 1232841465000,
    type: 'post',
    text: 'Just made plane reservations to go skiing/snowboarding at Sunday River in Maine with extended family. I\'m one of the snowboarders ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '1145618391',
    created: 1232841124000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dhanji" rel="noopener noreferrer" target="_blank">@dhanji</a> JSF definitely has flaws. But it\'s critical to the Java EE platform and if I can make it better, I will. Besides, it\'s not hard.<br><br>In reply to: <a href="https://x.com/dhanji/status/1144084387" rel="noopener noreferrer" target="_blank">@dhanji</a> <span class="status">1144084387</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1144061365',
    created: 1232776920000,
    type: 'post',
    text: 'By prototyping, it reminded me of a number of small enhancements that I have long wanted to see in JSF. Now I\'m in the driver\'s seat.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1144060204',
    created: 1232776870000,
    type: 'post',
    text: 'I\'ve been working like mad prototyping GET support for JSF. I focused first on page parameters, which map request values to value bindings.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1141933982',
    created: 1232718919000,
    type: 'post',
    text: 'Web Beans is officially "the spec formally known as Web Beans" Mark my word, it\'s for the best. Read for yourself <a href="https://in.relation.to/2009/01/23/revised-public-draft-of-jsr-299-java-contexts-and-dependency-injection/" rel="noopener noreferrer" target="_blank">in.relation.to/2009/01/23/revised-public-draft-of-jsr-299-java-contexts-and-dependency-injection/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1139290606',
    created: 1232638177000,
    type: 'post',
    text: 'You have to appreciate how rare it was to see people walking over a frozen reflection pond on the National Mall. Ponds rarely freeze in MD.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1136895332',
    created: 1232562477000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pawelwrzeszcz" rel="noopener noreferrer" target="_blank">@pawelwrzeszcz</a> Yep, Jason Porter and I worked on getting support for IntelliJ in seam-gen. We actually have some patches still to come.<br><br>In reply to: <a href="https://x.com/pawelwrzeszcz/status/1134558072" rel="noopener noreferrer" target="_blank">@pawelwrzeszcz</a> <span class="status">1134558072</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1136893239',
    created: 1232562425000,
    type: 'post',
    text: 'NetworkManager is making me want to pull my hair out. I think I was victim of a bad update so I rolled back. Connecting now, but lost time.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1135032749',
    created: 1232500090000,
    type: 'post',
    text: 'RHEL 5.3 was released today! I\'m excited because RHEL 5.2 has been such a dependable distro, much to my surprise (being an Ubuntu guy).',
    likes: 0,
    retweets: 0
  },
  {
    id: '1134003380',
    created: 1232474391000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> That\'s correct. I had already consumed my 140 characters. I believe it started with "black doesn\'t have to go to the back"<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1133930135',
    created: 1232473318000,
    type: 'post',
    text: 'Man\'s most powerful sword is his word. I am humbled and hopeful that Obama has no fear of speaking out and calling on help.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1133921102',
    created: 1232473203000,
    type: 'post',
    text: 'Who knew a memorial quote would come from the benediction: "yellow can stay mellow, red can get ahead, and white can do what\'s right" Amen!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1132230618',
    created: 1232422012000,
    type: 'post',
    text: 'Tomorrow there is going to be a lot of chatter no doubt. I plan to just listen. That\'s what you\'re supposed to do when the president speaks.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1129829005',
    created: 1232344615000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> GIT might be in Seam\'s not to distant future. There is a lot of internal chatter about GIT at JBoss in general. We\'ll see.<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1129824272',
    created: 1232344430000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> TLC is offering just over 4K w/ a VSP discount. Not the type of thing you want to shop at Walmart for, if you get my drift.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1128101492" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1128101492</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1129817096',
    created: 1232344157000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> That is just trippy. You either have to trust your mind or not value your life to take that leap.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/1129792797" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">1129792797</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1129807373',
    created: 1232343808000,
    type: 'post',
    text: 'Laurel MD ain\'t the most glamorous place to live. But I don\'t know what I would do w/o my local wine and spirits warehouse. Beer heaven.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1129802844',
    created: 1232343625000,
    type: 'post',
    text: 'Visited "the rents" tonight to watch the Raven\'s game. We brought them two surprises. Belgian Beer and a new website. Too bad Ravens lost.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1127769286',
    created: 1232262115000,
    type: 'post',
    text: 'I\'m a candidate for Lasik. I haven\'t made a final decision yet, but I\'m thinking that no time is better than the present.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1127767883',
    created: 1232262040000,
    type: 'post',
    text: 'I built Mojarra for the first time today and got a demo running. Mojarra == JSF 2.0 RI. I need to create patches this week for proposals.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1127766343',
    created: 1232261970000,
    type: 'post',
    text: 'I\'m really impressed with SilverStripe. I wish it were Java, but nothing beats it. Here are 5 reasons why <a href="http://www.myinkblog.com/2009/01/16/5-reasons-to-use-silverstripe-for-your-next-cms/" rel="noopener noreferrer" target="_blank">www.myinkblog.com/2009/01/16/5-reasons-to-use-silverstripe-for-your-next-cms/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1127764723',
    created: 1232261871000,
    type: 'post',
    text: 'I worked most of day with my wife building a website for my wife\'s family farm. We are using SilverStripe and a CSS grid framework.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1124760203',
    created: 1232141242000,
    type: 'post',
    text: 'In Thunderbird, there is a Print button in the print preview window. When you click it, you get: "You cannot print while in print preview."',
    likes: 0,
    retweets: 0
  },
  {
    id: '1123882659',
    created: 1232119231000,
    type: 'post',
    text: 'I decided to make the jump to Google Apps. I\'m sick of dealing w/ bounced mail and spam. I want it to be Google\'s problem, not mine.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1121892185',
    created: 1232051155000,
    type: 'post',
    text: 'Red Hat produces an internal videocast bimonthly. CEO informally addresses company, highlights events, interviews global offices. So cool.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1119434720',
    created: 1231970925000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> Sorry man, I had to do it :( I had a weak moment.<br><br>In reply to: <a href="https://x.com/tech4j/status/1119206275" rel="noopener noreferrer" target="_blank">@tech4j</a> <span class="status">1119206275</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1118700282',
    created: 1231952470000,
    type: 'post',
    text: 'To work like hell, you must learn to rest like the dead. If you don\'t rest, you drag ass. If you try to beat the system, it doesn\'t work ;)',
    likes: 0,
    retweets: 0
  },
  {
    id: '1118531013',
    created: 1231948381000,
    type: 'post',
    text: 'Just tried out devilspie. It rocks. Wanted gnome-terminal to start maximized. (if(is(window_class = "Gnome-terminal")(begin(maximize)))',
    likes: 0,
    retweets: 0
  },
  {
    id: '1118238311',
    created: 1231940486000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jclingan" rel="noopener noreferrer" target="_blank">@jclingan</a> My wife and I enjoy watching Fringe too. It brings out the mad scientist in us...rahahaha<br><br>In reply to: <a href="https://x.com/jclingan/status/1117655061" rel="noopener noreferrer" target="_blank">@jclingan</a> <span class="status">1117655061</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1117388812',
    created: 1231902905000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SKohler" rel="noopener noreferrer" target="_blank">@SKohler</a> Awesome to hear. Yeah, I\'m starving for some reviews. You\'re on the hook now! Hahaha, just kidding.<br><br>In reply to: @skohler <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1117383521',
    created: 1231902744000,
    type: 'post',
    text: 'Woke up early for a webcast only to discover people can\'t do timezone math; rescheduled for tomorrow. Same bat time, same bat channel.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1117377403',
    created: 1231902560000,
    type: 'post',
    text: 'I cringe to have to shine a light on the character of Hitler, but that clip had me rolling on the floor.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1117375982',
    created: 1231902517000,
    type: 'post',
    text: 'If you haven\'t seen it yet, no one expresses frustration over seeing the fail whale better then Hitler: <a href="https://youtu.be/wd4WZ3LqCKw" rel="noopener noreferrer" target="_blank">youtu.be/wd4WZ3LqCKw</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1114649619',
    created: 1231812728000,
    type: 'post',
    text: 'The agents that brag about the homes they sold in the neighborhood don\'t even bother putting the sale price on the postcard anymore.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1114123333',
    created: 1231797177000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/krharrison" rel="noopener noreferrer" target="_blank">@krharrison</a> I also got my XMP3 hooked up to Windows XP through VMWare. I did that mostly for comparison with libmtp functionality.<br><br>In reply to: <a href="https://x.com/krharrison/status/1114025623" rel="noopener noreferrer" target="_blank">@krharrison</a> <span class="status">1114025623</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1114085890',
    created: 1231796180000,
    type: 'post',
    text: 'Proposals hit point 3 on Gavin\'s must-have list for JSF 2: <a href="http://wiki.java.net/bin/view/Projects/Jsf2EGMap" rel="noopener noreferrer" target="_blank">wiki.java.net/bin/view/Projects/Jsf2EGMap</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1114080207',
    created: 1231796030000,
    type: 'post',
    text: 'On Friday I finished 2 more proposals for JSF 2. Page parameters (a la Seam) and bookmarkable links via preemptive navigation.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1114018822',
    created: 1231794383000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/krharrison" rel="noopener noreferrer" target="_blank">@krharrison</a> libmtp. You just compile/install it and amaroK will detect the device. Hopefully we can get an updated package for Ubuntu.<br><br>In reply to: <a href="https://x.com/krharrison/status/1113988332" rel="noopener noreferrer" target="_blank">@krharrison</a> <span class="status">1113988332</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1114016611',
    created: 1231794323000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tech4j" rel="noopener noreferrer" target="_blank">@tech4j</a> I should say I struggle defining Seam in &lt; 500 pages. There are so many ways to answer the question. Maybe I should start a poll.<br><br>In reply to: <a href="https://x.com/tech4j/status/1114008077" rel="noopener noreferrer" target="_blank">@tech4j</a> <span class="status">1114008077</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1114012231',
    created: 1231794206000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jbossnews" rel="noopener noreferrer" target="_blank">@jbossnews</a> I would love to hear from a user/company who is using a wide range of Java EE APIs. Not sure who yet.<br><br>In reply to: <a href="https://x.com/RHMiddleware/status/1113788685" rel="noopener noreferrer" target="_blank">@RHMiddleware</a> <span class="status">1113788685</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1114006197',
    created: 1231794049000,
    type: 'post',
    text: 'Just finished first cut of slides on Seam and Web Beans for the JBoss Virtual Experience (JBVE) conference. I always struggle defining Seam.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1113645624',
    created: 1231784193000,
    type: 'post',
    text: 'NFL\'s theme for this season: "Expect the unexpected" I expect Pittsburgh to beat Baltimore. Hopefully, the theme proves true.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1113634644',
    created: 1231783899000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/krharrison" rel="noopener noreferrer" target="_blank">@krharrison</a> I love my XMP3. It has some limitations but I have it working in Linux and loving it! That reminds me, I need to turn it on.<br><br>In reply to: <a href="https://x.com/krharrison/status/1108333982" rel="noopener noreferrer" target="_blank">@krharrison</a> <span class="status">1108333982</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1113613045',
    created: 1231783333000,
    type: 'post',
    text: 'I love the radical "punch" lines the NYTimes throws at Linux <a href="http://www.nytimes.com/2009/01/11/business/11ubuntu.html?pagewanted=1&amp;_r=1" rel="noopener noreferrer" target="_blank">www.nytimes.com/2009/01/11/business/11ubuntu.html?pagewanted=1&amp;_r=1</a> Fear the upgrade! Never used Ubuntu obviously.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1105578003',
    created: 1231459298000,
    type: 'post',
    text: 'Attending the NovaJUG tonight. Reza Rahman, coauthor of EJB 3 in Action, is giving an update on Java EE 6.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1105051908',
    created: 1231444106000,
    type: 'post',
    text: 'I\'m now on the JSF 2 EG, proxied through Pete Muir. I just submitted a proposal to add &lt;if&gt; to &lt;navigation-case&gt; for stateful navigation.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1105042996',
    created: 1231443873000,
    type: 'post',
    text: 'Gavin King sets the record straight about Web Beans: <a href="http://www.infoq.com/news/2009/01/webbeansqa" rel="noopener noreferrer" target="_blank">www.infoq.com/news/2009/01/webbeansqa</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1103688000',
    created: 1231390003000,
    type: 'post',
    text: 'I landed #4 on Manning\'s 2008 best seller list. Let\'s see what 2009 brings. The content is not falling out of date anytime soon.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1103655196',
    created: 1231388829000,
    type: 'post',
    text: 'My official announcement about working for Red Hat: <a href="https://mojavelinux.com/blog/archives/2009/01/im_a_red_hatter/" rel="noopener noreferrer" target="_blank">mojavelinux.com/blog/archives/2009/01/im_a_red_hatter/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1103553772',
    created: 1231385515000,
    type: 'post',
    text: 'The Asus EEE PC Linux just dropped to $199 at my local Best Buy. I already have a portable Linux computer, so I can\'t justify buying it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1103549774',
    created: 1231385395000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SUBZERO66" rel="noopener noreferrer" target="_blank">@SUBZERO66</a> Yeah, you have to master the flipping of the card. Our table swore it had RFID in it. When you flipped it to green, instant meat!<br><br>In reply to: <a href="https://x.com/bmuschko/status/1100911360" rel="noopener noreferrer" target="_blank">@bmuschko</a> <span class="status">1100911360</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1103547749',
    created: 1231385328000,
    type: 'reply',
    text: '@patrickrmcgowan Yeah, 8.10 is super solid. It continues to impress me by the day. That\'s the kind of Linux I have been missing for a while.<br><br>In reply to: <a href="https://x.com/prm8686/status/1101088047" rel="noopener noreferrer" target="_blank">@prm8686</a> <span class="status">1101088047</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1103543993',
    created: 1231385209000,
    type: 'post',
    text: 'This may be a bad economy, but I certainly haven\'t spent any less than in a good economy. In fact, I have probably spent more.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1102705590',
    created: 1231360025000,
    type: 'post',
    text: 'All my home computers are officially backed up. I am using Areca backup. Simple config and easy recovery. Check it out.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1100816555',
    created: 1231291595000,
    type: 'post',
    text: 'I\'m so psyched, I compiled cvs version of libmtp and got XMP3 device working in amaroK. Fun playing w/ devices on Linux again.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1100426126',
    created: 1231279250000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a>: Yeah man, back problems are crippling. I had trouble towards the end of the book. I\'ve got a ergo chair high on my wishlist.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1099900772" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1099900772</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1100345711',
    created: 1231276720000,
    type: 'post',
    text: 'Feasted at Fogo de Chao last night while seeing bro off to Italy. The meat literals falls off the stick.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1097860892',
    created: 1231186897000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Awesome job Emmanuel! I\'m heading to the book store tonight and I will take my picture with it ;)<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/1097479937" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">1097479937</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1097858234',
    created: 1231186817000,
    type: 'post',
    text: 'I just love the new look of JavaWorld (<a href="http://javaworld.com" rel="noopener noreferrer" target="_blank">javaworld.com</a>). It makes me want to read all the articles just to keep looking at it!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1092441886',
    created: 1230938287000,
    type: 'post',
    text: 'Upgraded both of my wife\'s computers to Ubuntu 8.10 (Intrepid). I have never been so impressed w/ Linux. Finally, another solid Ubuntu dist!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1092439133',
    created: 1230938172000,
    type: 'post',
    text: 'Got my version of the iPod for Xmas. Pioneer XMP3. MP3+XM radio. Finally, the combination I have been longing for. Can even record XM radio.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1092438630',
    created: 1230938154000,
    type: 'post',
    text: 'Amazing how small the world is sometimes. Met someone at New Year\'s who works w/ former coworker of mine. Found some fellow Linux folk.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1092435959',
    created: 1230938046000,
    type: 'post',
    text: 'Spent the New Year\'s in Fredneck (that\'s Fredrick, MD to the rest of you). Kick ass party. Saw friends from graduate school. Drank Mad Elf.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1092425593',
    created: 1230937642000,
    type: 'post',
    text: 'I spend a looooooooooooong time getting my wife\'s new monitor setup on Linux w/ an nvidia card. The nvidia driver is half brain dead.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1092424436',
    created: 1230937594000,
    type: 'post',
    text: 'I took a big break off from twitter and work over the holiday to give my mind some rest. I\'m breaking away and starting the New Year fresh.',
    likes: 0,
    retweets: 0
  }
])
