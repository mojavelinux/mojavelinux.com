'use strict'

inflateTweets([
  {
    id: '1344853785462542337',
    created: 1609473103000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> Ain\'t that the truth!<br><br>In reply to: <a href="https://x.com/brunoborges/status/1344839585059065856" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1344839585059065856</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1344838951920570368',
    created: 1609469566000,
    type: 'post',
    text: 'I think 2021 needs to have a probation period before we welcome it.',
    likes: 14,
    retweets: 5
  },
  {
    id: '1344798193876230144',
    created: 1609459849000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AmelieLens" rel="noopener noreferrer" target="_blank">@AmelieLens</a> I dig the full height sparklers! 🎇<br><br>In reply to: <a href="https://x.com/AmelieLens/status/1344759294055604228" rel="noopener noreferrer" target="_blank">@AmelieLens</a> <span class="status">1344759294055604228</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1344721720188391424',
    created: 1609441616000,
    type: 'quote',
    text: 'As we reflect on 2020, another extremely compelling reason to become an engaged climate activist. Wishful thinking isn\'t enough.<br><br>Quoting: <a href="https://x.com/swetac/status/1344412823624028160" rel="noopener noreferrer" target="_blank">@swetac</a> <span class="status">1344412823624028160</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1344600792041340928',
    created: 1609412784000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jponge" rel="noopener noreferrer" target="_blank">@jponge</a> <a class="mention" href="https://x.com/tomorrowland" rel="noopener noreferrer" target="_blank">@tomorrowland</a> <a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <a class="mention" href="https://x.com/AmelieLens" rel="noopener noreferrer" target="_blank">@AmelieLens</a> Unfortunately, not at this event. She may be doing an independent stream last I checked. <a class="mention" href="https://x.com/CharlottedWitte" rel="noopener noreferrer" target="_blank">@CharlottedWitte</a> is closing the event, though. Here\'s the lineup: <a href="https://www.tomorrowland.com/en/tomorrowland-31-12-2020/line-up/thursday-31-december-2020" rel="noopener noreferrer" target="_blank">www.tomorrowland.com/en/tomorrowland-31-12-2020/line-up/thursday-31-december-2020</a><br><br>In reply to: <a href="https://x.com/jponge/status/1344584520515903488" rel="noopener noreferrer" target="_blank">@jponge</a> <span class="status">1344584520515903488</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1344579748643520512',
    created: 1609407767000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tomorrowland" rel="noopener noreferrer" target="_blank">@tomorrowland</a> <a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <a class="mention" href="https://x.com/jponge" rel="noopener noreferrer" target="_blank">@jponge</a> ☝️ are you going to tune in?<br><br>In reply to: <a href="https://x.com/tomorrowland/status/1344568871659991040" rel="noopener noreferrer" target="_blank">@tomorrowland</a> <span class="status">1344568871659991040</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1343443987672121345',
    created: 1609136981000,
    type: 'quote',
    text: 'I think my jaw hit the floor on this one. Wow.<br><br>Quoting: <a href="https://x.com/theawesomer/status/1336806470344536064" rel="noopener noreferrer" target="_blank">@theawesomer</a> <span class="status">1336806470344536064</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1342647973059022848',
    created: 1608947196000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/vinny2020" rel="noopener noreferrer" target="_blank">@vinny2020</a> I figured, it was time. If not now, when? Amirite?<br><br>In reply to: <a href="https://x.com/vinny2020/status/1342606470307840002" rel="noopener noreferrer" target="_blank">@vinny2020</a> <span class="status">1342606470307840002</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1342373675291402241',
    created: 1608881798000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/hofmannedv" rel="noopener noreferrer" target="_blank">@hofmannedv</a> Just the original. At the advice of my spouse, we can skip the next two. The fourth is in the queue.<br><br>In reply to: <a href="https://x.com/hofmannedv/status/1342370167133773825" rel="noopener noreferrer" target="_blank">@hofmannedv</a> <span class="status">1342370167133773825</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1342369777600217089',
    created: 1608880869000,
    type: 'post',
    text: 'I can now say I\'ve watched Die Hard. As an unbiased observer, I can also support the assertion that it is, in fact, a Christmas movie. 🎄💣',
    likes: 27,
    retweets: 1
  },
  {
    id: '1342182607069409280',
    created: 1608836244000,
    type: 'post',
    text: 'Happy holidays to all my comrades! Know that the sacrifices you have made for your fellow citizens and the planet is the greatest gift of all. May your days be merry and bright! ⭐',
    likes: 7,
    retweets: 0
  },
  {
    id: '1342169988090658817',
    created: 1608833236000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> <a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> Wow! Nice work!<br><br>In reply to: <a href="https://x.com/ggrossetie/status/1341762934356238345" rel="noopener noreferrer" target="_blank">@ggrossetie</a> <span class="status">1341762934356238345</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1341965612394803202',
    created: 1608784509000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> I\'ll do you one better. While eating it, I was barking, "No kitty! It\'s my pumpkin pie!"<br><br>In reply to: <a href="https://x.com/marcsavy/status/1341363515681062913" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1341363515681062913</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1341449382504239104',
    created: 1608661430000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/benignbala" rel="noopener noreferrer" target="_blank">@benignbala</a> The best is yet to come on both fronts ;)<br><br>In reply to: <a href="https://x.com/benignbala/status/1341375316715978759" rel="noopener noreferrer" target="_blank">@benignbala</a> <span class="status">1341375316715978759</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1341363214706069504',
    created: 1608640886000,
    type: 'post',
    text: 'I bought a fancy new hand mixer to make (vegan) whipped cream. I was so excited for the upgrade &amp; modern tech. It turned out to be absolutely worthless. Big bottle of nope sauce. I went back to my 20 year old, grocery store bought, 3 speed mixer. It still whips the llama\'s ass.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1341361949624336384',
    created: 1608640584000,
    type: 'post',
    text: 'It\'s more like "sealed for your frustration" amirite?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1341361592290615299',
    created: 1608640499000,
    type: 'post',
    text: 'I\'ve had a lot of pumpkin pies in my life. I\'m telling you there\'s no reason for it to be anything other than vegan. Coconut milk FTW. Here\'s the recipe I use. <a href="https://veganheaven.org/wprm_print/12540" rel="noopener noreferrer" target="_blank">veganheaven.org/wprm_print/12540</a>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1341361077951426560',
    created: 1608640376000,
    type: 'post',
    text: 'Exhibit A: Vegan pumpkin pie.',
    photos: ['<div class="item"><img class="photo" src="media/1341361077951426560.jpg"></div>'],
    likes: 4,
    retweets: 0
  },
  {
    id: '1341169635534589952',
    created: 1608594733000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bmastenbrook" rel="noopener noreferrer" target="_blank">@bmastenbrook</a> <a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> I should mention that you can produce DocBook (or even DITA) from AsciiDoc, which allows you to tie into toolchains that can support additional capabilities. That\'s a key value proposition of AsciiDoc. Just not something everyone will leverage.<br><br>In reply to: @bmastenbrook <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1341169266742071296',
    created: 1608594645000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bmastenbrook" rel="noopener noreferrer" target="_blank">@bmastenbrook</a> <a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> Fair enough.<br><br>In reply to: @bmastenbrook <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1341169195774484480',
    created: 1608594628000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bmastenbrook" rel="noopener noreferrer" target="_blank">@bmastenbrook</a> <a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> (Granted, the diffing tool in GitHub is not open source, but it\'s freely available...so it still fits within our sphere).<br><br>In reply to: <a href="#1341169033580711936">1341169033580711936</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1341169033580711936',
    created: 1608594589000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bmastenbrook" rel="noopener noreferrer" target="_blank">@bmastenbrook</a> <a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> Well, Asciidoctor is and always has been focused on open source solutions. So if that\'s not sufficient, you\'ll need to turn to alternate approaches. I\'m sure there are PDF diffing tools somewhere out there. But it\'s not our focus.<br><br>In reply to: @bmastenbrook <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1341168104076845056',
    created: 1608594368000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <a class="mention" href="https://x.com/bmastenbrook" rel="noopener noreferrer" target="_blank">@bmastenbrook</a> IMHO, GitHub provides those diff bars. And trying to recreate what GitHub is doing to display a visual diff is a specialty best left to the specialists. See <a href="https://github.com/asciidoctor/asciidoc-docs/pull/27/files?short_path=23f118e#diff-23f118ea0ff6e7011a4d055db155a5cf068000b35884fe110ca8e6f97bca373f" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoc-docs/pull/27/files?short_path=23f118e#diff-23f118ea0ff6e7011a4d055db155a5cf068000b35884fe110ca8e6f97bca373f</a><br><br>In reply to: <a href="https://x.com/odrotbohm/status/1341157583344857090" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <span class="status">1341157583344857090</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1340933273153798144',
    created: 1608538380000,
    type: 'post',
    text: 'I intend to make the most of my newly acquired vegan culinary skills this holiday season. It\'s a gift to my partner and myself.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1340837379641249798',
    created: 1608515517000,
    type: 'quote',
    text: 'New Zealand gets all of my respect. True global citizens.<br><br>Quoting: <a href="https://x.com/RexChapman/status/1340373616064655366" rel="noopener noreferrer" target="_blank">@RexChapman</a> <span class="status">1340373616064655366</span>',
    likes: 6,
    retweets: 0
  },
  {
    id: '1340069551916998656',
    created: 1608332453000,
    type: 'post',
    text: 'Congress seems to think we like hearing about them failing at their jobs. If they did that where we work, most of them would be fired by now.',
    likes: 2,
    retweets: 1
  },
  {
    id: '1339681279852179458',
    created: 1608239881000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/quaid" rel="noopener noreferrer" target="_blank">@quaid</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> Fantastic to see! You\'re doing such influential work and I\'m so glad Asciidoctor gets to be a part of that story.<br><br>In reply to: <a href="https://x.com/quaid/status/1339594466974334981" rel="noopener noreferrer" target="_blank">@quaid</a> <span class="status">1339594466974334981</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1339514130667982848',
    created: 1608200030000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sfermigier" rel="noopener noreferrer" target="_blank">@sfermigier</a> <a class="mention" href="https://x.com/keithalexander" rel="noopener noreferrer" target="_blank">@keithalexander</a> <a class="mention" href="https://x.com/robsmallshire" rel="noopener noreferrer" target="_blank">@robsmallshire</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Thanks! We expect 2021 to be *the* year for AsciiDoc we\'ve all been waiting for. We have the plan and budget necessary to make it happen, so I\'m confident we\'ll get it done. And progress will continue in the years that follow.<br><br>In reply to: <a href="https://x.com/sfermigier/status/1339513468467228673" rel="noopener noreferrer" target="_blank">@sfermigier</a> <span class="status">1339513468467228673</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1339513000793804801',
    created: 1608199760000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sfermigier" rel="noopener noreferrer" target="_blank">@sfermigier</a> <a class="mention" href="https://x.com/keithalexander" rel="noopener noreferrer" target="_blank">@keithalexander</a> <a class="mention" href="https://x.com/robsmallshire" rel="noopener noreferrer" target="_blank">@robsmallshire</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> AsciiDoc Python is still making maintenance releases because it has existing users who rely on it. But it\'s not being evolved anymore.<br><br>In reply to: <a href="#1339512755934568451">1339512755934568451</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1339512755934568451',
    created: 1608199702000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sfermigier" rel="noopener noreferrer" target="_blank">@sfermigier</a> <a class="mention" href="https://x.com/keithalexander" rel="noopener noreferrer" target="_blank">@keithalexander</a> <a class="mention" href="https://x.com/robsmallshire" rel="noopener noreferrer" target="_blank">@robsmallshire</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> You\'re catching progress midstream. We\'re hard at work on making this transition and reclaiming <a href="http://asciidoc.org" rel="noopener noreferrer" target="_blank">asciidoc.org</a> for the language. A key goal of the WG in 2021 is to improve the messaging around the AsciiDoc language by giving it a new home. So yeah, progress is messy.<br><br>In reply to: <a href="https://x.com/sfermigier/status/1339510722078920710" rel="noopener noreferrer" target="_blank">@sfermigier</a> <span class="status">1339510722078920710</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1339506313613590531',
    created: 1608198166000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sfermigier" rel="noopener noreferrer" target="_blank">@sfermigier</a> <a class="mention" href="https://x.com/keithalexander" rel="noopener noreferrer" target="_blank">@keithalexander</a> <a class="mention" href="https://x.com/robsmallshire" rel="noopener noreferrer" target="_blank">@robsmallshire</a> The new documentation site, currently in development, will be a much better reference for this extension model. The docs for it is definitely an area that needs improvement.<br><br>In reply to: <a href="#1339504898035990529">1339504898035990529</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1339505900562706433',
    created: 1608198068000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sfermigier" rel="noopener noreferrer" target="_blank">@sfermigier</a> <a class="mention" href="https://x.com/keithalexander" rel="noopener noreferrer" target="_blank">@keithalexander</a> <a class="mention" href="https://x.com/robsmallshire" rel="noopener noreferrer" target="_blank">@robsmallshire</a> So in truth, right now, there\'s only one implementation. And that\'s Asciidoctor. Everyone in the community acknowledges this.<br><br>In reply to: <a href="#1339505667451699200">1339505667451699200</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1339505667451699200',
    created: 1608198012000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sfermigier" rel="noopener noreferrer" target="_blank">@sfermigier</a> <a class="mention" href="https://x.com/keithalexander" rel="noopener noreferrer" target="_blank">@keithalexander</a> <a class="mention" href="https://x.com/robsmallshire" rel="noopener noreferrer" target="_blank">@robsmallshire</a> AsciiDoc Python is deprecated and is no longer what AsciiDoc is about. We\'ll likely see a new (much more modern) implementation in Python that is compliant.<br><br>In reply to: <a href="#1339505442230128645">1339505442230128645</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1339505442230128645',
    created: 1608197958000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sfermigier" rel="noopener noreferrer" target="_blank">@sfermigier</a> <a class="mention" href="https://x.com/keithalexander" rel="noopener noreferrer" target="_blank">@keithalexander</a> <a class="mention" href="https://x.com/robsmallshire" rel="noopener noreferrer" target="_blank">@robsmallshire</a> That\'s the *entire* point of the AsciiDoc WG and the specification it\'s currently working towards. Asciidoctor has contributed the initial definition of the language, which will evolve into a formal definition that all will have to implement to be called an AsciiDoc processor.<br><br>In reply to: <a href="https://x.com/sfermigier/status/1339483989015408642" rel="noopener noreferrer" target="_blank">@sfermigier</a> <span class="status">1339483989015408642</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1339504898035990529',
    created: 1608197829000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sfermigier" rel="noopener noreferrer" target="_blank">@sfermigier</a> <a class="mention" href="https://x.com/keithalexander" rel="noopener noreferrer" target="_blank">@keithalexander</a> <a class="mention" href="https://x.com/robsmallshire" rel="noopener noreferrer" target="_blank">@robsmallshire</a> Indeed, that is the goal. Btw, AsciiDoc is highly extensible. Though you\'ve pointed to the incorrect reference. The extension model is documented here: <a href="https://www.rubydoc.info/gems/asciidoctor/Asciidoctor/Extensions" rel="noopener noreferrer" target="_blank">www.rubydoc.info/gems/asciidoctor/Asciidoctor/Extensions</a><br><br>In reply to: <a href="https://x.com/sfermigier/status/1339483552421974017" rel="noopener noreferrer" target="_blank">@sfermigier</a> <span class="status">1339483552421974017</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1339477611370536960',
    created: 1608191323000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/quaid" rel="noopener noreferrer" target="_blank">@quaid</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> Yes, Antora is all about multi-page HTML. It\'s far easier to construct than to deconstruct. But if what you really need is chunked HTML, then there\'s an extended converter for that. See <a href="https://github.com/owenh000/asciidoctor-multipage" rel="noopener noreferrer" target="_blank">github.com/owenh000/asciidoctor-multipage</a><br><br>In reply to: <a href="https://x.com/quaid/status/1339402854625280001" rel="noopener noreferrer" target="_blank">@quaid</a> <span class="status">1339402854625280001</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1339449506073440258',
    created: 1608184622000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/vogella" rel="noopener noreferrer" target="_blank">@vogella</a> I find my own rewards. The happiness of my community is a big part of that. It\'s just that I won\'t be bullied. There\'s no place for that. You\'re absolutely right that certain people (often the exception) feel entitled and try to ruin it.<br><br>In reply to: <a href="https://x.com/vogella/status/1339191386751569920" rel="noopener noreferrer" target="_blank">@vogella</a> <span class="status">1339191386751569920</span>',
    likes: 5,
    retweets: 0
  },
  {
    id: '1339382751607177216',
    created: 1608168707000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sfermigier" rel="noopener noreferrer" target="_blank">@sfermigier</a> <a class="mention" href="https://x.com/keithalexander" rel="noopener noreferrer" target="_blank">@keithalexander</a> <a class="mention" href="https://x.com/robsmallshire" rel="noopener noreferrer" target="_blank">@robsmallshire</a> It\'s not accurate to say that AsciiDoc is mostly a frontend for DocBook. It was developed initially with the semantics of DocBook in mind because DocBook has good semantics. But AsciiDoc is for technical writing, large and small.<br><br>In reply to: <a href="https://x.com/sfermigier/status/1339118616088895496" rel="noopener noreferrer" target="_blank">@sfermigier</a> <span class="status">1339118616088895496</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1339346312169320448',
    created: 1608160019000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> <a class="mention" href="https://x.com/domdorn" rel="noopener noreferrer" target="_blank">@domdorn</a> This is my latest find. <a href="https://youtu.be/1TLJiuHp88s" rel="noopener noreferrer" target="_blank">youtu.be/1TLJiuHp88s</a><br><br>In reply to: <a href="https://x.com/majson/status/1339207394002427904" rel="noopener noreferrer" target="_blank">@majson</a> <span class="status">1339207394002427904</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1339344934449545219',
    created: 1608159690000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/leftyaaron" rel="noopener noreferrer" target="_blank">@leftyaaron</a> <a class="mention" href="https://x.com/ASterling" rel="noopener noreferrer" target="_blank">@ASterling</a> ...we should really say *because* their prime minister is a socialist (and frankly, just a decent person).<br><br>In reply to: <a href="https://x.com/leftyaaron/status/1339343363707793409" rel="noopener noreferrer" target="_blank">@leftyaaron</a> <span class="status">1339343363707793409</span>',
    likes: 16,
    retweets: 0
  },
  {
    id: '1339344616353529856',
    created: 1608159615000,
    type: 'reply',
    text: '@zregvart I gave it attempt, but it seems like an extremely obscure way to set a color. I\'m just not understanding why this can\'t be simple. It seems like the CSS working group is really overlooking this essential use case.<br><br>In reply to: @zregvart <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1339344311595364358',
    created: 1608159542000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> <a class="mention" href="https://x.com/domdorn" rel="noopener noreferrer" target="_blank">@domdorn</a> You guys are sharing all the favs from my playlist. Let\'s keep the sharing going!<br><br>In reply to: <a href="https://x.com/majson/status/1339207394002427904" rel="noopener noreferrer" target="_blank">@majson</a> <span class="status">1339207394002427904</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1339113942224814082',
    created: 1608104618000,
    type: 'post',
    text: 'On the other hand, if you say, "this feature is important to me and will help me get my work done" then I keep that in mind when I\'m deciding what to work on. I value and respond to feedback, just not extortion.',
    likes: 26,
    retweets: 0
  },
  {
    id: '1339112543424430081',
    created: 1608104284000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cramTeXeD" rel="noopener noreferrer" target="_blank">@cramTeXeD</a> Your gratitude is much appreciated and a key reason why I show up every day to work on OSS!<br><br>In reply to: <a href="https://x.com/cramTeXeD/status/1339108561851473920" rel="noopener noreferrer" target="_blank">@cramTeXeD</a> <span class="status">1339108561851473920</span>',
    likes: 5,
    retweets: 0
  },
  {
    id: '1339105811990102017',
    created: 1608102679000,
    type: 'post',
    text: 'To cite an example of what I\'m talking about, it\'s often something like, "I\'ll switch to competing software if you don\'t implement this feature for me now." It\'s just childish and I won\'t stand for it.',
    likes: 29,
    retweets: 1
  },
  {
    id: '1339099222621097984',
    created: 1608101108000,
    type: 'post',
    text: 'I love working on open source and I will go out of my way to work on features for my community. But I will NOT be pushed around.',
    likes: 42,
    retweets: 0
  },
  {
    id: '1338642193343340544',
    created: 1607992144000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AmelieLens" rel="noopener noreferrer" target="_blank">@AmelieLens</a> Oh, and because my spouse and I adore your cats. 😻<br><br>In reply to: <a href="#1338407764381462528">1338407764381462528</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1338407764381462528',
    created: 1607936252000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AmelieLens" rel="noopener noreferrer" target="_blank">@AmelieLens</a> Yes! We want to rave into a better year listening to our favorite techno DJ and supporting women in music at the same time. No better way than an Amelie Lens set. Keep us on our toes! 🎉🎛️🥂<br><br>In reply to: <a href="https://x.com/AmelieLens/status/1338403462908944387" rel="noopener noreferrer" target="_blank">@AmelieLens</a> <span class="status">1338403462908944387</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1338405667741155329',
    created: 1607935752000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tomorrowland" rel="noopener noreferrer" target="_blank">@tomorrowland</a> I\'m excited for the show, but I\'m very troubled by the lack of women being showcased in this lineup. By my count, there is one. I expect better from a world of tomorrow.<br><br>In reply to: <a href="https://x.com/tomorrowland/status/1336677071809097730" rel="noopener noreferrer" target="_blank">@tomorrowland</a> <span class="status">1336677071809097730</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1337900802615078912',
    created: 1607815382000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/quaid" rel="noopener noreferrer" target="_blank">@quaid</a> I was chatting with Sarah about this last night. We need to know what\'s true, regardless of whether it\'s good or bad. Because that\'s how we can make informed decisions about what course to take. Journalism is not about the decision, it\'s about est the conditions to make one.<br><br>In reply to: <a href="https://x.com/quaid/status/1337899552368513024" rel="noopener noreferrer" target="_blank">@quaid</a> <span class="status">1337899552368513024</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1337596421248172032',
    created: 1607742812000,
    type: 'post',
    text: 'It\'s really too bad you cannot change the fill color of an SVG when used as a background image. Seems like an oversight. I\'ve tried absolutely everything (use, mask, etc) and the only way I can figure out how to do it is to set the color in the SVG data.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1337532471177404417',
    created: 1607727565000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/justinhopper" rel="noopener noreferrer" target="_blank">@justinhopper</a> <a class="mention" href="https://x.com/_Mark_Atwood" rel="noopener noreferrer" target="_blank">@_Mark_Atwood</a> Indeed. And we will continue to strive to do even better.<br><br>In reply to: <a href="https://x.com/justinhopper/status/1337530390962028545" rel="noopener noreferrer" target="_blank">@justinhopper</a> <span class="status">1337530390962028545</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1336474840950161408',
    created: 1607475407000,
    type: 'reply',
    text: '@wmhilton <a class="mention" href="https://x.com/ZoltanKochan" rel="noopener noreferrer" target="_blank">@ZoltanKochan</a> It will never end. (And I\'ve made peace with that by using a milestone named Support).<br><br>In reply to: @wmhilton <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1336442844874096640',
    created: 1607467778000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/OurRevolution" rel="noopener noreferrer" target="_blank">@OurRevolution</a> Do you even have to ask? Of course!!!<br><br>In reply to: <a href="https://x.com/OurRevolution/status/1336430353091727361" rel="noopener noreferrer" target="_blank">@OurRevolution</a> <span class="status">1336430353091727361</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1336380826334810113',
    created: 1607452992000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> All fonts on Fedora look amazing. It\'s not the font, it\'s the font rendering. (Granted, some fonts are weird looking, but that\'s the font itself). Some of the best fonts to me are the Noto, Roboto, and M+ families.<br><br>In reply to: <a href="https://x.com/settermjd/status/1336379554798182405" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1336379554798182405</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1336380210439024641',
    created: 1607452845000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> That could be it. I don\'t consider Ubuntu to be a good choice for fonts. I think Fedora is much better suited for the task. I have used Fedora now for well over a decade.<br><br>In reply to: <a href="https://x.com/settermjd/status/1336379156280578049" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1336379156280578049</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1336377708901670912',
    created: 1607452249000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> Which OS are you using? And do you have a HiDPI screen?<br><br>In reply to: <a href="https://x.com/settermjd/status/1336372935087579139" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1336372935087579139</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1336377524440338433',
    created: 1607452205000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> I may take some pics with my phone. A screenshot is going to lie because it, itself, is only an approximation. I can tell you that what I see (latest Fedora) is as sharp as I have ever seen fonts anywhere I know.<br><br>In reply to: <a href="https://x.com/settermjd/status/1336372571860856834" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1336372571860856834</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1336376875640229889',
    created: 1607452050000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rawkode" rel="noopener noreferrer" target="_blank">@rawkode</a> <a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> That\'s fair. We\'re all going to have a different idea of what looks best. So we can both be right depending on what we\'re looking for. I\'ll concede that both look excellent and that the margin between them is very small.<br><br>In reply to: <a href="https://x.com/rawkode/status/1336297801320439810" rel="noopener noreferrer" target="_blank">@rawkode</a> <span class="status">1336297801320439810</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1336295938415972352',
    created: 1607432753000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rawkode" rel="noopener noreferrer" target="_blank">@rawkode</a> <a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> Then I find your comment even more unfounded. How can you look at those fonts and say that they are terrible? It\'s simply not true. It\'s the most accurate rendering I\'ve ever seen, and certainly in Linux\'s history. You\'re putting down incredible work and progress.<br><br>In reply to: <a href="https://x.com/rawkode/status/1336292176028979200" rel="noopener noreferrer" target="_blank">@rawkode</a> <span class="status">1336292176028979200</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1336289806150561794',
    created: 1607431291000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rawkode" rel="noopener noreferrer" target="_blank">@rawkode</a> <a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> Your information is outdated. Linux is winning this battle right now. Try the latest Gnome on a quality monitor and you will see for yourself.<br><br>In reply to: <a href="https://x.com/rawkode/status/1336270957309276160" rel="noopener noreferrer" target="_blank">@rawkode</a> <span class="status">1336270957309276160</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1336289577758146560',
    created: 1607431237000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> And I am extremely critical about font rendering.<br><br>In reply to: <a href="#1336289324493500416">1336289324493500416</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1336289324493500416',
    created: 1607431176000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> As of right now, I\'m of the firm belief (based on observation) that Linux has the superior font rendering engine. I have the latest OS of both on the latest hardware and Linux wins easily.<br><br>In reply to: <a href="https://x.com/settermjd/status/1336269249476759557" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1336269249476759557</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1336079101052866560',
    created: 1607381055000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/8thHalfHour" rel="noopener noreferrer" target="_blank">@8thHalfHour</a> <a class="mention" href="https://x.com/mrhaki" rel="noopener noreferrer" target="_blank">@mrhaki</a> Hubert\'s series has done wonders for Asciidoctor education. I do hope that the new docs site we\'re making can prove to be just as useful. And it will be easier than ever to accept contributions.<br><br>In reply to: <a href="https://x.com/8thHalfHour/status/1335901069575028741" rel="noopener noreferrer" target="_blank">@8thHalfHour</a> <span class="status">1335901069575028741</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1335907409504522241',
    created: 1607340121000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rdeltour" rel="noopener noreferrer" target="_blank">@rdeltour</a> That is *really* nice. I only have one issue. Let\'s assume one of the version numbers is longer than the others. Is there a way for it to auto-size based on the content? (instead of 2em?)<br><br>In reply to: <a href="https://x.com/rdeltour/status/1335898122967519232" rel="noopener noreferrer" target="_blank">@rdeltour</a> <span class="status">1335898122967519232</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1335871500352585728',
    created: 1607331559000,
    type: 'post',
    text: 'Here\'s what I\'m going for: <a href="https://gist.github.com/mojavelinux/10078846ecc2772112420c7293c3d0ca" rel="noopener noreferrer" target="_blank">gist.github.com/mojavelinux/10078846ecc2772112420c7293c3d0ca</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1335809004375666698',
    created: 1607316659000,
    type: 'quote',
    text: 'Always. Motion smoothing is an abomination.<br><br>Quoting: <a href="https://x.com/wirecutter/status/1335744134208950274" rel="noopener noreferrer" target="_blank">@wirecutter</a> <span class="status">1335744134208950274</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1335808679338110976',
    created: 1607316581000,
    type: 'post',
    text: 'I wonder if this is something CSS grid can do.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1335808537960673280',
    created: 1607316548000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lukedary" rel="noopener noreferrer" target="_blank">@lukedary</a> Not quite. That causes a partially filled line to be spaced out. I\'m looking for left aligned tiles with the block aligned to the right.<br><br>In reply to: <a href="https://x.com/lukedary/status/1335806740906950656" rel="noopener noreferrer" target="_blank">@lukedary</a> <span class="status">1335806740906950656</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1335752073606926336',
    created: 1607303086000,
    type: 'post',
    text: 'If I use justify-content: flex-end on the container and margin-left: auto on the last item, then the wrapped row can end up being to the left of the first item. I want it to be aligned with the first item. Doesn\'t seem to be possible.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1335747256134168577',
    created: 1607301937000,
    type: 'post',
    text: 'In CSS (thinking about flexbox), is it possible to align a column of boxes to the right, but have them wrap from left to right? The idea is that the first wrapped item will left-align with the first item. But the right-most box will touch the right margin.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1335680304384634881',
    created: 1607285974000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PDXyogini" rel="noopener noreferrer" target="_blank">@PDXyogini</a> Have you seen <a href="https://youtu.be/Ver1OZdK2bA" rel="noopener noreferrer" target="_blank">youtu.be/Ver1OZdK2bA</a>?<br><br>In reply to: <a href="https://x.com/PDXyogini/status/1335675062448398336" rel="noopener noreferrer" target="_blank">@PDXyogini</a> <span class="status">1335675062448398336</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1335566455035727875',
    created: 1607258831000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> All very strong points.<br><br>In reply to: <a href="https://x.com/garrett/status/1335564558883315713" rel="noopener noreferrer" target="_blank">@garrett</a> <span class="status">1335564558883315713</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1335566072733286401',
    created: 1607258740000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> Perhaps it is. I kind of wish MDN would stop listing it because all it does is put a strain on all the recent progress in web technology.<br><br>In reply to: <a href="https://x.com/garrett/status/1335563440220418049" rel="noopener noreferrer" target="_blank">@garrett</a> <span class="status">1335563440220418049</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1335546685477380098',
    created: 1607254117000,
    type: 'post',
    text: 'highlight.js is forcing everyone to upgrade to v10, which doesn\'t support IE 11. This either spells the end of IE 11 or the end of highlight.js. (In reality, it will be somewhere in between).',
    likes: 1,
    retweets: 0
  },
  {
    id: '1335500964929814528',
    created: 1607243217000,
    type: 'post',
    text: 'I love CSS and the power it gives me. But I will always try both justify-content and align-items, then keep whichever one makes the text move in the direction I want it to move.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1335347578259435520',
    created: 1607206646000,
    type: 'reply',
    text: '@JosephLouthan <a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> Skim off the frontmatter, do the conversion, then restore the frontmatter. The frontmatter is not part of either AsciiDoc or Markdown, so processors are not going to know what to do with it. The frontmatter is a construct understood only by the site generator.<br><br>In reply to: @JosephLouthan <span class="status">&lt;deleted&gt;</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1334982077696016384',
    created: 1607119504000,
    type: 'post',
    text: 'PSA: The 12 days of Christmas (at least how the period is used by retail to drive sales) is entirely made up.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1334674758898569222',
    created: 1607046234000,
    type: 'reply',
    text: '@jbryant787 🤣<br><br>In reply to: @jbryant787 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1334666092061491200',
    created: 1607044167000,
    type: 'post',
    text: 'You\'d think something like a quick reference would be easy to create.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1334447526498340865',
    created: 1606992057000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/adsamalik" rel="noopener noreferrer" target="_blank">@adsamalik</a> OMG, I thought that was just me ;) I feel validated.<br><br>In reply to: <a href="https://x.com/adsamalik/status/1334442651249037314" rel="noopener noreferrer" target="_blank">@adsamalik</a> <span class="status">1334442651249037314</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1334243692735946752',
    created: 1606943460000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> I would love to know, because the current situation is unacceptable for me.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1334237531248975876" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1334237531248975876</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1333907265171066880',
    created: 1606863249000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/TheTomFlaherty" rel="noopener noreferrer" target="_blank">@TheTomFlaherty</a> 💯<br><br>In reply to: <a href="https://x.com/TheTomFlaherty/status/1333905539797970945" rel="noopener noreferrer" target="_blank">@TheTomFlaherty</a> <span class="status">1333905539797970945</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1333704852455653380',
    created: 1606814990000,
    type: 'quote',
    text: 'Feedback welcome before this gets rolled into the new docs site for Asciidoctor, which is on the way!<br><br>Quoting: <a href="https://x.com/abelsromero/status/1333525161048420356" rel="noopener noreferrer" target="_blank">@abelsromero</a> <span class="status">1333525161048420356</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1333533853957332992',
    created: 1606774221000,
    type: 'post',
    text: '"If the data you\'re interested in only comes in the PDF format, then it’s important to be aware that this is a deceptively simple-looking problem and that a 100% accurate solution may very well be impossible."',
    likes: 1,
    retweets: 0
  },
  {
    id: '1333533526193442817',
    created: 1606774143000,
    type: 'post',
    text: 'Thanks to <a class="mention" href="https://x.com/bentolor" rel="noopener noreferrer" target="_blank">@bentolor</a> for finding this article that goes into depth about the point I\'m trying to convey here. By no means am I saying that PDF has no use. Just know what you\'re getting yourself into. <a href="https://filingdb.com/b/pdf-text-extraction" rel="noopener noreferrer" target="_blank">filingdb.com/b/pdf-text-extraction</a><br><br>Quoting: <a href="#1333320045846351873">1333320045846351873</a>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1333532842081480705',
    created: 1606773980000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bentolor" rel="noopener noreferrer" target="_blank">@bentolor</a> @jbryant787 That is *exactly* the article I was looking for. That really captures everything I was trying to convey and more. I\'m bookmarking that one!<br><br>In reply to: <a href="https://x.com/bentolor/status/1333472976122109964" rel="noopener noreferrer" target="_blank">@bentolor</a> <span class="status">1333472976122109964</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1333532474899451904',
    created: 1606773892000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pvaneeckhoudt" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> Exactly.<br><br>In reply to: <a href="https://x.com/pvaneeckhoudt/status/1333481089600544769" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> <span class="status">1333481089600544769</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1333532408885350400',
    created: 1606773876000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pvaneeckhoudt" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> That last sentence is a very useful description.<br><br>In reply to: <a href="https://x.com/pvaneeckhoudt/status/1333474861423976449" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> <span class="status">1333474861423976449</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1333532105385537537',
    created: 1606773804000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_gettalong" rel="noopener noreferrer" target="_blank">@_gettalong</a> You have a solid point about tagged PDFs. In my experience, though, they are extremely difficult to understand and create, so it only complicates the issue of archiving further. It\'s a lot of hurdles for what plain text or XML already gives you.<br><br>In reply to: <a href="https://x.com/_gettalong/status/1333468522610089987" rel="noopener noreferrer" target="_blank">@_gettalong</a> <span class="status">1333468522610089987</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1333378629879808000',
    created: 1606737212000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/rmannibucau" rel="noopener noreferrer" target="_blank">@rmannibucau</a> Yes, I am talking about people who claim that PDF is for long term storage to preserve content (think like a library). You don\'t get back out what you put in. It\'s not semantic. So it\'s a bad assumption leading to a poor choice.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1333376797279477760" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1333376797279477760</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1333378275033321473',
    created: 1606737128000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/rmannibucau" rel="noopener noreferrer" target="_blank">@rmannibucau</a> I don\'t know why it has to be a single file. If I were archiving for long term, I would probably choose DocBook or DITA export, then put it in a folder. My second choice stepping down from that would be AsciiDoc.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1333377017383956480" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1333377017383956480</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1333377938041970693',
    created: 1606737048000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rmannibucau" rel="noopener noreferrer" target="_blank">@rmannibucau</a> 100% of cases. Yeah, that really is an absurd claim. To say that is denying the existence and success of web technologies. Be real.<br><br>In reply to: <a href="https://x.com/rmannibucau/status/1333359698029006848" rel="noopener noreferrer" target="_blank">@rmannibucau</a> <span class="status">1333359698029006848</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1333373757881077762',
    created: 1606736051000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BigBlueBeez" rel="noopener noreferrer" target="_blank">@BigBlueBeez</a> There are a plethora of open source libraries and desktop programs that can write PDF. Just because Adobe won\'t distribute one doesn\'t mean there aren\'t others available.<br><br>In reply to: <a href="https://x.com/BigBlueBeez/status/1333367315480649730" rel="noopener noreferrer" target="_blank">@BigBlueBeez</a> <span class="status">1333367315480649730</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1333373472848842753',
    created: 1606735983000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/rmannibucau" rel="noopener noreferrer" target="_blank">@rmannibucau</a> What you\'re talking about is print ready. That is not the same thing as archive. Archive is about long term storage for the purpose of preserving information. And for that, PDF is not a good fit.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1333363320645169153" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1333363320645169153</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1333356369664036864',
    created: 1606731905000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rmannibucau" rel="noopener noreferrer" target="_blank">@rmannibucau</a> You are mischaracterizing HTML. It may be your opinion, but it\'s not an accurate portrayal of HTML. HTML absolutely works offline. And it has proven far more effective and accessible than PDF ever dreams of being.<br><br>In reply to: <a href="https://x.com/rmannibucau/status/1333355620599291912" rel="noopener noreferrer" target="_blank">@rmannibucau</a> <span class="status">1333355620599291912</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1333344055111995392',
    created: 1606728969000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rmannibucau" rel="noopener noreferrer" target="_blank">@rmannibucau</a> And if you want something that looks the same, I would argue that an HTML page works just as well if not better than PDF, but has the benefit of not mangling the content to do so.<br><br>In reply to: <a href="#1333343794473750530">1333343794473750530</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1333343794473750530',
    created: 1606728907000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rmannibucau" rel="noopener noreferrer" target="_blank">@rmannibucau</a> I find a file folder works just as well (when we\'re talking about archiving). Wanting a file to look the same in a few years (assuming PDF viewers don\'t change) is a fine goal. But you\'re not describing an archive format.<br><br>In reply to: <a href="https://x.com/rmannibucau/status/1333340554168569858" rel="noopener noreferrer" target="_blank">@rmannibucau</a> <span class="status">1333340554168569858</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1333343455750221824',
    created: 1606728826000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fxthoorens" rel="noopener noreferrer" target="_blank">@fxthoorens</a> I agree there is confusion, which is why I want to draw attention to it. Ready to print and archive are different things and it\'s important that we communicate that.<br><br>In reply to: <a href="https://x.com/fxthoorens/status/1333342414384336899" rel="noopener noreferrer" target="_blank">@fxthoorens</a> <span class="status">1333342414384336899</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1333338668526800899',
    created: 1606727685000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/shahryareiv" rel="noopener noreferrer" target="_blank">@shahryareiv</a> That would be very valuable input for a specification for a plain text markup language. I totally agree. It should be done. I wish research institutions would step up to this challenge.<br><br>In reply to: <a href="https://x.com/shahryareiv/status/1333338118540365825" rel="noopener noreferrer" target="_blank">@shahryareiv</a> <span class="status">1333338118540365825</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1333338336396656641',
    created: 1606727606000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/shahryareiv" rel="noopener noreferrer" target="_blank">@shahryareiv</a> My point is that we\'re so far from PDF in this line of discussion, that it\'s really become a different topic altogether. PDF is not going to help any more than any other format if we look that far out. We could ask then if we even have computers to read the file systems.<br><br>In reply to: <a href="https://x.com/shahryareiv/status/1333336575606976513" rel="noopener noreferrer" target="_blank">@shahryareiv</a> <span class="status">1333336575606976513</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1333335147186319360',
    created: 1606726845000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/shahryareiv" rel="noopener noreferrer" target="_blank">@shahryareiv</a> I agree human cognition is the limit. But then, that is a far, far, far cry from "PDF is an archive format" which is absolutely is not. So whether text itself is an archive format is a separate discussion (for which all archive formats would have to consider).<br><br>In reply to: <a href="https://x.com/shahryareiv/status/1333333383334797313" rel="noopener noreferrer" target="_blank">@shahryareiv</a> <span class="status">1333333383334797313</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1333334760681132034',
    created: 1606726753000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/shahryareiv" rel="noopener noreferrer" target="_blank">@shahryareiv</a> But what\'s so important about the plain text format is that the text, the language, is as bare as it can be. (Even more so if you use minimal formatting marks). And that means the intention (such as where paragraphs break) has been preserved.<br><br>In reply to: <a href="#1333334554522710016">1333334554522710016</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1333334554522710016',
    created: 1606726704000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/shahryareiv" rel="noopener noreferrer" target="_blank">@shahryareiv</a> You don\'t need to rely on Asciidoctor, Ruby, or even modern operating systems. You have your eyes and brain and can take that text and make it what you want it to be. (Yes, we rely on a common understanding of language, but we can read hieroglyphs, so that\'s not a limitation).<br><br>In reply to: <a href="#1333334100812333057">1333334100812333057</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1333334100812333057',
    created: 1606726596000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/shahryareiv" rel="noopener noreferrer" target="_blank">@shahryareiv</a> Absolutely none of that matters because the information is in plain text, readily parseable. You don\'t need any existing software or permission. You can take the information (even if by hand) and transform it into something that can display it the way you want it displayed.<br><br>In reply to: <a href="https://x.com/shahryareiv/status/1333332853120249857" rel="noopener noreferrer" target="_blank">@shahryareiv</a> <span class="status">1333332853120249857</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1333333566218928128',
    created: 1606726468000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rmannibucau" rel="noopener noreferrer" target="_blank">@rmannibucau</a> I\'m not saying it isn\'t convenient. But archive has a very specific definition, which means to preserve. And if the goal is to preserve information, it\'s not the best format. If you just want something pretty to download, then sure.<br><br>In reply to: <a href="https://x.com/rmannibucau/status/1333331855861575680" rel="noopener noreferrer" target="_blank">@rmannibucau</a> <span class="status">1333331855861575680</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1333329903568130052',
    created: 1606725595000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rmannibucau" rel="noopener noreferrer" target="_blank">@rmannibucau</a> You are correct when you say "well configured". I would say "rasterized". But that is not how most people are using it.<br><br>In reply to: <a href="https://x.com/rmannibucau/status/1333325492167045120" rel="noopener noreferrer" target="_blank">@rmannibucau</a> <span class="status">1333325492167045120</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1333329709883527176',
    created: 1606725549000,
    type: 'post',
    text: 'PDF is a mutable format. It\'s only the viewer application that\'s giving you the impression that the content is immutable. The one exception is if the PDF has been rasterized. Then it\'s really just an image container.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1333329147498618881',
    created: 1606725415000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fxthoorens" rel="noopener noreferrer" target="_blank">@fxthoorens</a> That understanding is incorrect. It\'s not hard to edit. But it is difficult to retrieve the information out of it in a way that reproduces what went into it (violating the very purpose of an archive format).<br><br>In reply to: <a href="https://x.com/fxthoorens/status/1333322827869675520" rel="noopener noreferrer" target="_blank">@fxthoorens</a> <span class="status">1333322827869675520</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1333328776969687042',
    created: 1606725327000,
    type: 'post',
    text: 'The source is just the text chopped up into hierarchical tree of references. There\'s very little in the way of consistency for how the text is stored in that tree. It can be divided up in a myriad of ways, making it both easy to hack and hard piece back together. It\'s lossy.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1333327752477360130',
    created: 1606725082000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rmannibucau" rel="noopener noreferrer" target="_blank">@rmannibucau</a> The assumption that it\'s immutable is not correct. It\'s structured text (unless it\'s been rasterized). What would be immutable (as it could be) is either a raster image or a printed work.<br><br>In reply to: <a href="https://x.com/rmannibucau/status/1333325492167045120" rel="noopener noreferrer" target="_blank">@rmannibucau</a> <span class="status">1333325492167045120</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1333327275899584512',
    created: 1606724969000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/shahryareiv" rel="noopener noreferrer" target="_blank">@shahryareiv</a> In terms of being able to print the same result years later, perhaps. But that assumes we can trust printers to not change. If you asked me 50 years from now to produce the same output from Markdown or AsciiDoc, I could do it. Absolutely.<br><br>In reply to: <a href="https://x.com/shahryareiv/status/1333321921526706182" rel="noopener noreferrer" target="_blank">@shahryareiv</a> <span class="status">1333321921526706182</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1333326826530172929',
    created: 1606724862000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BigBlueBeez" rel="noopener noreferrer" target="_blank">@BigBlueBeez</a> A PDF is just as easy to edit (in terms of feasibility) as any other format (given the right tools or knowledge of programming). If you think it is locking people out of editing, that would be incorrect. It\'s just structured text.<br><br>In reply to: <a href="https://x.com/BigBlueBeez/status/1333321889796792321" rel="noopener noreferrer" target="_blank">@BigBlueBeez</a> <span class="status">1333321889796792321</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1333320658982293505',
    created: 1606723391000,
    type: 'post',
    text: 'My guess is that anyone who thinks PDF is an archive format has never actually studied the source of a PDF.',
    likes: 6,
    retweets: 1
  },
  {
    id: '1333320045846351873',
    created: 1606723245000,
    type: 'post',
    text: 'I find it strange that people think of PDF as an archive format. It severely chops up the text in the process of laying out, formatting, and paging it. It\'s absolutely not an archive format. A true archive format is plain text every single day of the week.',
    likes: 24,
    retweets: 2
  },
  {
    id: '1333319298069061635',
    created: 1606723067000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/djwfyi" rel="noopener noreferrer" target="_blank">@djwfyi</a> <a class="mention" href="https://x.com/intellijidea" rel="noopener noreferrer" target="_blank">@intellijidea</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> My hope is that the ideas being explored in the IntelliJ plugin can be taken back and adopted to other IDEs, like VS Code. I advocate for it because I want users and developers to see the innovation going on there.<br><br>In reply to: <a href="https://x.com/djwfyi/status/1332895775219716096" rel="noopener noreferrer" target="_blank">@djwfyi</a> <span class="status">1332895775219716096</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1332480709521334272',
    created: 1606523132000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> I do agree with that. They went too far with dependencies in the ecosystem in my opinion. I\'m continue to work to cut out dependencies so that Antora is as self-contained as it can be. Fortunately, Node is providing a better stdlib with each new version.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1332472915997159424" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1332472915997159424</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1332472116298465280',
    created: 1606521083000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> The package repository seems to be responding much slower than normal right now. Installation is not normally this slow. It\'s the HTTP calls that are slowing it down, not the installation itself.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1332456103674785792" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1332456103674785792</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1332291473979572226',
    created: 1606478014000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> Sadly true.<br><br>In reply to: <a href="https://x.com/garrett/status/1332290783236526083" rel="noopener noreferrer" target="_blank">@garrett</a> <span class="status">1332290783236526083</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1332276723761647616',
    created: 1606474498000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rahulsom" rel="noopener noreferrer" target="_blank">@rahulsom</a> There are certain things we just cannot rationalize.<br><br>In reply to: <a href="#1332276535257047045">1332276535257047045</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1332276535257047045',
    created: 1606474453000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rahulsom" rel="noopener noreferrer" target="_blank">@rahulsom</a> Or...if we look at animal rights, the absolute terror animals are put through in factory farms (especially in the US). It\'s not enough to say we can reduce the abuse and slaughter by half. It needs to stop completely.<br><br>In reply to: <a href="#1332275790453432321">1332275790453432321</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1332275790453432321',
    created: 1606474275000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rahulsom" rel="noopener noreferrer" target="_blank">@rahulsom</a> I hear your points, and they are rational. But we are past the point of reasonable asks. The pain of having to switch to a vegan/vegetarian diet is nothing compared to the pain we\'re inflicting as a society on our children and their children. Their suffering will be much worse.<br><br>In reply to: <a href="https://x.com/rahulsom/status/1332179361328369664" rel="noopener noreferrer" target="_blank">@rahulsom</a> <span class="status">1332179361328369664</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1332275201959104512',
    created: 1606474135000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/EDewit" rel="noopener noreferrer" target="_blank">@EDewit</a> That\'s a good question. Why does it take courage? At this point, it should only require having a conscience.<br><br>In reply to: @edewit <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1332058921461137408',
    created: 1606422569000,
    type: 'post',
    text: 'I really wish more Democrat politicians would commit to being vegan (or say so if they already are), especially progressives. Show some courage. Show us that you\'re serious about the causes you claim to stand for. Lead by example.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1332012851825786880',
    created: 1606411586000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/j2r2b" rel="noopener noreferrer" target="_blank">@j2r2b</a> That makes sense as the scope of this particular contract is very narrow. Either AsciidoctorJ should provide an API or core would need to be more forgiving.<br><br>In reply to: <a href="https://x.com/j2r2b/status/1331938713543331840" rel="noopener noreferrer" target="_blank">@j2r2b</a> <span class="status">1331938713543331840</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1331935753593491456',
    created: 1606393204000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/j2r2b" rel="noopener noreferrer" target="_blank">@j2r2b</a> Here\'s the test case for reference: <a href="https://github.com/asciidoctor/asciidoctor/blob/master/test/extensions_test.rb#L1415-L1431" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor/blob/master/test/extensions_test.rb#L1415-L1431</a><br><br>In reply to: <a href="#1331935435270983681">1331935435270983681</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1331935435270983681',
    created: 1606393128000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/j2r2b" rel="noopener noreferrer" target="_blank">@j2r2b</a> It may be the case that PhraseNode in the AsciidoctorJ API needs a setSubstitutions method for this purpose. I don\'t think this capability has ever been explored across this boundary.<br><br>In reply to: <a href="https://x.com/j2r2b/status/1331918841098616832" rel="noopener noreferrer" target="_blank">@j2r2b</a> <span class="status">1331918841098616832</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1331909305734373376',
    created: 1606386898000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/j2r2b" rel="noopener noreferrer" target="_blank">@j2r2b</a> Btw, it\'s much better to ask questions at <a href="https://discuss.asciidoctor.org" rel="noopener noreferrer" target="_blank">discuss.asciidoctor.org</a> or <a href="https://gitter.im/asciidoctor/asciidoctor" rel="noopener noreferrer" target="_blank">gitter.im/asciidoctor/asciidoctor</a> than on SO. The Asciidoctor project leads don\'t really hang out on SO.<br><br>In reply to: <a href="#1331908948438315010">1331908948438315010</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1331908948438315010',
    created: 1606386813000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/j2r2b" rel="noopener noreferrer" target="_blank">@j2r2b</a> I don\'t know exactly how this translates to the Java API. It\'s not something I have a lot of experience with. I only write extensions in Ruby.<br><br>In reply to: <a href="#1331908790946467842">1331908790946467842</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1331908790946467842',
    created: 1606386776000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/j2r2b" rel="noopener noreferrer" target="_blank">@j2r2b</a> You can return an Inline node with the subs attribute set on it. Asciidoctor then applies those subs to the value of the text property. Then it runs convert on the node. You can see what\'s happening here: <a href="https://github.com/asciidoctor/asciidoctor/blob/master/lib/asciidoctor/substitutors.rb#L333-L337" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor/blob/master/lib/asciidoctor/substitutors.rb#L333-L337</a><br><br>In reply to: <a href="https://x.com/j2r2b/status/1331906105488838659" rel="noopener noreferrer" target="_blank">@j2r2b</a> <span class="status">1331906105488838659</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1331891562087739393',
    created: 1606382668000,
    type: 'reply',
    text: '@jhkrug "guesting" &lt;= hmm, is my mind trying to introduce a new word here? "What were they doing before the pandemic?" "Oh, I heard they were guesting." 🤣<br><br>In reply to: <a href="#1331736571804938241">1331736571804938241</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1331891025518886912',
    created: 1606382540000,
    type: 'reply',
    text: 'I\'m once again going to prepare Chickpeas &amp; Dumplings by <a class="mention" href="https://x.com/badmannersfood" rel="noopener noreferrer" target="_blank">@badmannersfood</a> (similar to <a href="https://www.badmanners.com/recipes/chickpea-dumpling-soup" rel="noopener noreferrer" target="_blank">www.badmanners.com/recipes/chickpea-dumpling-soup</a>). I\'ll also be making a pumpkin pie from scratch for the first time. And, of course, cranberries. We\'ll see where it goes from there.<br><br>In reply to: <a href="#1331890362789421056">1331890362789421056</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1331890362789421056',
    created: 1606382382000,
    type: 'post',
    text: 'Getting prepared for my second vegan National Day of Mourning tomorrow. It\'s both a protest against broken traditions and practices that I was raised to follow and a day to think about what I can do for the next seven generations. 🥕🥔🧅🧄🌿🥧🙏',
    likes: 1,
    retweets: 0
  },
  {
    id: '1331873208354578432',
    created: 1606378292000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/j2r2b" rel="noopener noreferrer" target="_blank">@j2r2b</a> It\'s because the macro substitution is at the end of the substitution chain. So if you want further substitutions to be applied, you need to apply them in the processor. The return value is not reprocessed. (This is a point definitely worth covering in the docs).<br><br>In reply to: <a href="https://x.com/j2r2b/status/1331864898746396675" rel="noopener noreferrer" target="_blank">@j2r2b</a> <span class="status">1331864898746396675</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1331871789064933378',
    created: 1606377954000,
    type: 'quote',
    text: 'Um, <a class="mention" href="https://x.com/MayorHancock" rel="noopener noreferrer" target="_blank">@MayorHancock</a>, how about not visiting your family for the holidays like the REST OF US. Get a clue.<br><br>Quoting: <a href="https://x.com/KyleClark/status/1331738005862830085" rel="noopener noreferrer" target="_blank">@KyleClark</a> <span class="status">1331738005862830085</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1331736571804938241',
    created: 1606345715000,
    type: 'reply',
    text: '@jhkrug The idea of guesting coming from different sides of the mountain is what does it for me. Maybe that\'s just my quarantined mind speaking ;)<br><br>In reply to: @jhkrug <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1331725537908199425',
    created: 1606343085000,
    type: 'reply',
    text: '@jhkrug Super cool. Looks like such a charming and remote place.<br><br>In reply to: @jhkrug <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1331722760377143296',
    created: 1606342422000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> 🤣<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/1331713367548055553" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">1331713367548055553</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1331708042832842752',
    created: 1606338914000,
    type: 'quote',
    text: 'A circular reference in real life.<br><br>Quoting: <a href="https://x.com/scattermoon/status/1331682072025583618" rel="noopener noreferrer" target="_blank">@scattermoon</a> <span class="status">1331682072025583618</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1331705986873139201',
    created: 1606338423000,
    type: 'quote',
    text: 'I don\'t even think we can begin to fathom the scale of the mental health epidemic we\'re currently facing and will continue to face. Some days, it feels like breathing is an accomplishment (and for many, a matter of life or death because of COVID-19).<br><br>Quoting: <a href="https://x.com/BernieSanders/status/1331704434481651713" rel="noopener noreferrer" target="_blank">@BernieSanders</a> <span class="status">1331704434481651713</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1331538154252189697',
    created: 1606298409000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/algolia" rel="noopener noreferrer" target="_blank">@algolia</a> Thanks for the great support! I just received a notification from support and will follow-up to that thread.<br><br>In reply to: <a href="https://x.com/algolia/status/1331526650803183616" rel="noopener noreferrer" target="_blank">@algolia</a> <span class="status">1331526650803183616</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1331518993673306112',
    created: 1606293841000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/algolia" rel="noopener noreferrer" target="_blank">@algolia</a> I submitted a request for the app for the Asciidoctor project to be enlisted in the OSS plan. Did you receive it? If so, can you approve it? Or do I need to resubmit?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1331505314886160386',
    created: 1606290579000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jaredmorgs" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <a class="mention" href="https://x.com/evanchooly" rel="noopener noreferrer" target="_blank">@evanchooly</a> When using Antora\'s default UI, the breadcrumbs are not displayed on mobile. There isn\'t sufficient space for them the way the design is implemented.<br><br>In reply to: <a href="https://x.com/jaredmorgs/status/1331499944302112769" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <span class="status">1331499944302112769</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1331347286937329664',
    created: 1606252903000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> My father used to always say, "I have said it once. I\'m not going to say it again." It\'s actually a pretty effective way of being heard.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/1331339098599133185" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">1331339098599133185</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1331118848314019841',
    created: 1606198439000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cryptodavidw" rel="noopener noreferrer" target="_blank">@cryptodavidw</a> stem is a shorthand for either asciimath or latexmath, depending on the value of the stem attribute.<br><br>In reply to: <a href="https://x.com/cryptodavidw/status/1331018200545329153" rel="noopener noreferrer" target="_blank">@cryptodavidw</a> <span class="status">1331018200545329153</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1331118698120105984',
    created: 1606198403000,
    type: 'post',
    text: 'Another Antora site comes online. This time, it\'s Morphia by <a class="mention" href="https://x.com/evanchooly" rel="noopener noreferrer" target="_blank">@evanchooly</a>. <a href="https://morphia.dev" rel="noopener noreferrer" target="_blank">morphia.dev</a> A huge thanks to David Jencks from the Antora community for the assist.',
    likes: 3,
    retweets: 2
  },
  {
    id: '1331002732811935745',
    created: 1606170754000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cryptodavidw" rel="noopener noreferrer" target="_blank">@cryptodavidw</a> stem:[a^b(^c)]<br><br>AsciiDoc is not a math language. The stem macro denotes text that is.<br><br>In reply to: <a href="https://x.com/cryptodavidw/status/1330965299110133760" rel="noopener noreferrer" target="_blank">@cryptodavidw</a> <span class="status">1330965299110133760</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1330999414668726272',
    created: 1606169963000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CrainsChicago" rel="noopener noreferrer" target="_blank">@CrainsChicago</a> No.<br><br>In reply to: <a href="https://x.com/CrainsChicago/status/1330952480402890752" rel="noopener noreferrer" target="_blank">@CrainsChicago</a> <span class="status">1330952480402890752</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1330998342327758848',
    created: 1606169708000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lanceball" rel="noopener noreferrer" target="_blank">@lanceball</a> Just edged out by "covidiot"?<br><br>In reply to: <a href="https://x.com/lanceball/status/1330997241302093830" rel="noopener noreferrer" target="_blank">@lanceball</a> <span class="status">1330997241302093830</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1330801210153000960',
    created: 1606122708000,
    type: 'post',
    text: 'OH: "Put your emojicons in the chat!" 🤣',
    likes: 0,
    retweets: 0
  },
  {
    id: '1330331027248648200',
    created: 1606010607000,
    type: 'quote',
    text: 'Before you utter the words, "If I get COVID-19, I\'ll be fine. I\'ll get through it", perhaps stop and think first about what it will do the people around you. That\'s what this is really all about. Think about your loved ones. Think about your society. It\'s not just about you.<br><br>Quoting: <a href="https://x.com/karlitaliliana/status/1329897916387901440" rel="noopener noreferrer" target="_blank">@karlitaliliana</a> <span class="status">1329897916387901440</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1330284977087524867',
    created: 1605999628000,
    type: 'post',
    text: 'Personally, I\'m taking it as seriously as ever.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1330280735836692491',
    created: 1605998617000,
    type: 'quote',
    text: 'Accurate. (for the US, at least)<br><br>Quoting: <a href="https://x.com/PaymanBenz/status/1329556264255332352" rel="noopener noreferrer" target="_blank">@PaymanBenz</a> <span class="status">1329556264255332352</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1329946525384855552',
    created: 1605918935000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jaredmorgs" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> I should add that the new docs feature full text search, so getting around is going to be a whole lot easier and efficient. And having separate pages leaves much more room for tutorials and task-focused content.<br><br>In reply to: <a href="https://x.com/jaredmorgs/status/1329930131289227264" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <span class="status">1329930131289227264</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1329925462357786626',
    created: 1605913913000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> If only people showed that much enthusiasm for animal rights.<br><br>In reply to: <a href="https://x.com/starbuxman/status/1329924313261043713" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">1329924313261043713</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1329891669949313024',
    created: 1605905856000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sunrisemvmt" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a> <a class="mention" href="https://x.com/CoriBush" rel="noopener noreferrer" target="_blank">@CoriBush</a> ✅ Hard agree.<br><br>In reply to: <a href="https://x.com/sunrisemvmt/status/1329866244917485570" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a> <span class="status">1329866244917485570</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1329890576720748545',
    created: 1605905596000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jaredmorgs" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> It is indeed great raw material, but was never organized very well. But that\'s only because we didn\'t have Antora.<br><br>In reply to: <a href="https://x.com/jaredmorgs/status/1329749367536840704" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <span class="status">1329749367536840704</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1329889084030873601',
    created: 1605905240000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/waynebeaton" rel="noopener noreferrer" target="_blank">@waynebeaton</a> I\'m beginning to think that this is, in fact, the keystone of my sanity right now.<br><br>In reply to: <a href="https://x.com/waynebeaton/status/1329882202864234497" rel="noopener noreferrer" target="_blank">@waynebeaton</a> <span class="status">1329882202864234497</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1329722136995368960',
    created: 1605865437000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/zeldapistola" rel="noopener noreferrer" target="_blank">@zeldapistola</a> Ma pensée exactement!<br><br>In reply to: <a href="https://x.com/zeldapistola/status/1329714512832946176" rel="noopener noreferrer" target="_blank">@zeldapistola</a> <span class="status">1329714512832946176</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1329602761428635648',
    created: 1605836975000,
    type: 'post',
    text: 'After months and months of work, we\'re getting very close to being able to roll out the new docs site for Asciidoctor, which uses Antora to pull together versioned content from multiple repositories in the ecosystem. This is going to be a massive improvement for the docs.',
    likes: 64,
    retweets: 7
  },
  {
    id: '1329312351921659904',
    created: 1605767736000,
    type: 'post',
    text: 'work =&gt; word',
    likes: 0,
    retweets: 0
  },
  {
    id: '1329309617982115840',
    created: 1605767084000,
    type: 'post',
    text: 'The US is not mature enough to use the work lockdown anymore. Now we have to say we\'re taking a pause or entering Level Red. Sure. Whatever. Just stay home if you have a choice.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1329192285406257152',
    created: 1605739110000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> <a class="mention" href="https://x.com/buildpacks_io" rel="noopener noreferrer" target="_blank">@buildpacks_io</a> Now THAT is love.<br><br>In reply to: <a href="https://x.com/mraible/status/1329186459505614849" rel="noopener noreferrer" target="_blank">@mraible</a> <span class="status">1329186459505614849</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1329027001307394048',
    created: 1605699703000,
    type: 'post',
    text: 'Ever since the pandemic started, I have cleaned up the kitchen every day (since we only eat at home). It was a task I previously dreaded, but now I look forward to it. It not only helps me wind down before going to sleep, it allows me to end my day with a sense of accomplishment.',
    likes: 10,
    retweets: 0
  },
  {
    id: '1328647766193577985',
    created: 1605609287000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> I\'m glad to know you and very much look forward to seeing you in person again. Let\'s keep hope alive.<br><br>In reply to: <a href="https://x.com/headius/status/1328644364554350592" rel="noopener noreferrer" target="_blank">@headius</a> <span class="status">1328644364554350592</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1328252588366544896',
    created: 1605515069000,
    type: 'post',
    text: 'This is a pretty neat website to figure out which package provides the command you\'re looking for across platforms. <a href="https://command-not-found.com/asciidoctor" rel="noopener noreferrer" target="_blank">command-not-found.com/asciidoctor</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1327781831559712769',
    created: 1605402832000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ntschutta" rel="noopener noreferrer" target="_blank">@ntschutta</a> <a class="mention" href="https://x.com/venkat_s" rel="noopener noreferrer" target="_blank">@venkat_s</a> What\'s so ironic about that phrase is that it got me to notice so many more stop signs when I was a teenager. Not that I thought they were different, but because the phrase just got stuck in my head and made me more aware.<br><br>In reply to: <a href="https://x.com/ntschutta/status/1327779470913245188" rel="noopener noreferrer" target="_blank">@ntschutta</a> <span class="status">1327779470913245188</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1327712727591051264',
    created: 1605386356000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_fletchr" rel="noopener noreferrer" target="_blank">@_fletchr</a> I like those branches!<br><br>In reply to: <a href="https://x.com/_fletchr/status/1327710218449342464" rel="noopener noreferrer" target="_blank">@_fletchr</a> <span class="status">1327710218449342464</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1327712424288346112',
    created: 1605386284000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/wimdeblauwe" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> <a class="mention" href="https://x.com/gegastaldi" rel="noopener noreferrer" target="_blank">@gegastaldi</a> <a class="mention" href="https://x.com/netskymusic" rel="noopener noreferrer" target="_blank">@netskymusic</a> The photo makes it look larger than it is. It\'s 52". I bought it almost 10 years ago when I was still working at Red Hat. It has really held up well.<br><br>In reply to: <a href="https://x.com/wimdeblauwe/status/1327612395259367425" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> <span class="status">1327612395259367425</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1327582693048008704',
    created: 1605355353000,
    type: 'post',
    text: 'Nothing puts me in a weekend state of mind quite like a new <a class="mention" href="https://x.com/netskymusic" rel="noopener noreferrer" target="_blank">@netskymusic</a> set (from my beloved Antwerp no less!)',
    photos: ['<div class="item"><img class="photo" src="media/1327582693048008704.jpg"></div>', '<div class="item"><img class="photo" src="media/1327582693048008704-2.jpg"></div>', '<div class="item"><img class="photo" src="media/1327582693048008704-3.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1327582016213110789',
    created: 1605355192000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cramTeXeD" rel="noopener noreferrer" target="_blank">@cramTeXeD</a> That\'s a really good way to frame it.<br><br>In reply to: <a href="https://x.com/cramTeXeD/status/1327522188484808705" rel="noopener noreferrer" target="_blank">@cramTeXeD</a> <span class="status">1327522188484808705</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1327419035256963072',
    created: 1605316334000,
    type: 'quote',
    text: 'For those outside the US, this is how seriously broken and biased the electoral college system is, and why we complain about it so much as not being democratic.<br><br>Quoting: <a href="https://x.com/sam__l__/status/1327359394326130688" rel="noopener noreferrer" target="_blank">@sam__l__</a> <span class="status">1327359394326130688</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1327325182017503233',
    created: 1605293958000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_codewriter" rel="noopener noreferrer" target="_blank">@_codewriter</a> <a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> I have a way of creating one (as well as a PDF). I could consider enabling that option once we get it deployed.<br><br>That said, I assure you that you\'ll find the search a lot easier to use to get around.<br><br>In reply to: <a href="https://x.com/_codewriter/status/1327285941027954688" rel="noopener noreferrer" target="_blank">@_codewriter</a> <span class="status">1327285941027954688</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1326859938518376448',
    created: 1605183035000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Stephan007" rel="noopener noreferrer" target="_blank">@Stephan007</a> I\'ve been thinking about you all week. While I miss seeing you, I\'m super glad you\'re enjoying a relaxed state of mind. Rest up. Next year Devoxx will be back in force! 🧠<br><br>In reply to: <a href="https://x.com/Stephan007/status/1326537477520125952" rel="noopener noreferrer" target="_blank">@Stephan007</a> <span class="status">1326537477520125952</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1326829729744236544',
    created: 1605175833000,
    type: 'post',
    text: 'My secret to curing writer\'s block is Adriatique. 🎧',
    likes: 0,
    retweets: 0
  },
  {
    id: '1326800219263148032',
    created: 1605168797000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> Either. So yes, fork.<br><br>In reply to: <a href="https://x.com/majson/status/1326799762289537030" rel="noopener noreferrer" target="_blank">@majson</a> <span class="status">1326799762289537030</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1326796701869486082',
    created: 1605167959000,
    type: 'post',
    text: 'One nice feature GitHub Actions has over competitors is that it will run the CI workflow on your branch *before* you submit a PR. This helps you to avoid submitting a PR that\'s failing right out of the gate.',
    likes: 9,
    retweets: 0
  },
  {
    id: '1326794970506948611',
    created: 1605167546000,
    type: 'post',
    text: 'While I never want any user of software I write to struggle or waste time debugging, it\'s also important to understand that maintainers often spend hours, sometimes even days or weeks, debugging problems that users report. Suffice to say, we bleed for you too.',
    likes: 5,
    retweets: 0
  },
  {
    id: '1326786467553828864',
    created: 1605165519000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <a class="mention" href="https://x.com/rk3rn3r" rel="noopener noreferrer" target="_blank">@rk3rn3r</a> <a class="mention" href="https://x.com/debezium" rel="noopener noreferrer" target="_blank">@debezium</a> <a class="mention" href="https://x.com/dkhaywood" rel="noopener noreferrer" target="_blank">@dkhaywood</a> Indeed, they make a good combination.<br><br>In reply to: <a href="https://x.com/gunnarmorling/status/1326784936007839745" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <span class="status">1326784936007839745</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1326662379694776320',
    created: 1605135934000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rk3rn3r" rel="noopener noreferrer" target="_blank">@rk3rn3r</a> <a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <a class="mention" href="https://x.com/debezium" rel="noopener noreferrer" target="_blank">@debezium</a> You should talk to <a class="mention" href="https://x.com/dkhaywood" rel="noopener noreferrer" target="_blank">@dkhaywood</a>, who did exactly that.<br><br>In reply to: <a href="https://x.com/rk3rn3r/status/1326661602624593923" rel="noopener noreferrer" target="_blank">@rk3rn3r</a> <span class="status">1326661602624593923</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1326658477456908288',
    created: 1605135003000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rk3rn3r" rel="noopener noreferrer" target="_blank">@rk3rn3r</a> <a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <a class="mention" href="https://x.com/debezium" rel="noopener noreferrer" target="_blank">@debezium</a> is ahead of the curve. Already switched to Antora early on.<br><br>In reply to: <a href="https://x.com/rk3rn3r/status/1326655324309348354" rel="noopener noreferrer" target="_blank">@rk3rn3r</a> <span class="status">1326655324309348354</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1326635828869689344',
    created: 1605129603000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jnorthr" rel="noopener noreferrer" target="_blank">@jnorthr</a> It shouldn\'t be, but it won\'t be zero effort. The main difference is the folder structure and the referencing system. Though, you can start by sticking with a single document and keep all references local. That should reduce the effort.<br><br>In reply to: <a href="https://x.com/jnorthr/status/1326635286332313600" rel="noopener noreferrer" target="_blank">@jnorthr</a> <span class="status">1326635286332313600</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1326634806344478722',
    created: 1605129360000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jnorthr" rel="noopener noreferrer" target="_blank">@jnorthr</a> You need to do what\'s best for you and your readers. That said, Antora is the future of documentation sites written in AsciiDoc.<br><br>In reply to: <a href="https://x.com/jnorthr/status/1326633926748737542" rel="noopener noreferrer" target="_blank">@jnorthr</a> <span class="status">1326633926748737542</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1326620286309998593',
    created: 1605125898000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jnorthr" rel="noopener noreferrer" target="_blank">@jnorthr</a> I think you\'ll find this showcase helpful: <a href="https://gitlab.com/antora/antora.org/-/issues/20" rel="noopener noreferrer" target="_blank">gitlab.com/antora/antora.org/-/issues/20</a> The prime example being Antora\'s own docs.<br><br>In reply to: <a href="https://x.com/jnorthr/status/1326614100760289282" rel="noopener noreferrer" target="_blank">@jnorthr</a> <span class="status">1326614100760289282</span>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1326620089831968768',
    created: 1605125851000,
    type: 'post',
    text: 'Starting with Asciidoctor 2.0.12, we\'re officially testing on macOS (thanks to GitHub Actions). Although we didn\'t expect any surprises, it\'s good we won\'t be surprised in the future.',
    likes: 14,
    retweets: 2
  },
  {
    id: '1326593171141218305',
    created: 1605119433000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jnorthr" rel="noopener noreferrer" target="_blank">@jnorthr</a> Do you mean Antora in general, or the preview of this migration?<br><br>In reply to: <a href="https://x.com/jnorthr/status/1326581702861467649" rel="noopener noreferrer" target="_blank">@jnorthr</a> <span class="status">1326581702861467649</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1326432805757202433',
    created: 1605081199000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> The docs migration that\'s currently underway is going to rectify this situation. The whole Asciidoctor ecosystem will be served as an Antora site, pulling in updates as they happen (nightly). We are very close to getting that wrapped up.<br><br>In reply to: <a href="https://x.com/odrotbohm/status/1326170239835533313" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <span class="status">1326170239835533313</span>',
    likes: 6,
    retweets: 0
  },
  {
    id: '1326273157544570882',
    created: 1605043136000,
    type: 'reply',
    text: '@jbryant787 There\'s also a certain enthusiasm it brings to civic engagement / duty. I\'ve voted in every election since moving to Colorado. I\'d have to work to ignore it.<br><br>In reply to: <a href="#1326272654425272320">1326272654425272320</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1326272654425272320',
    created: 1605043016000,
    type: 'reply',
    text: '@jbryant787 Indeed. I\'m really hoping this election catapulted progress by providing a massive real world test. Seeing is believing.<br><br>In reply to: @jbryant787 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1326234221086609408',
    created: 1605033853000,
    type: 'post',
    text: 'And if you use AsciidoctorJ, thanks to <a class="mention" href="https://x.com/bobbytank42" rel="noopener noreferrer" target="_blank">@bobbytank42</a>, you can get Asciidoctor 2.0.12 by upgrading to AsciidoctorJ 2.4.2. <a href="https://github.com/asciidoctor/asciidoctorj/releases/tag/v2.4.2" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctorj/releases/tag/v2.4.2</a> 🎉<br><br>Quoting: <a href="#1326069971886092288">1326069971886092288</a>',
    likes: 8,
    retweets: 0
  },
  {
    id: '1326232732335185921',
    created: 1605033498000,
    type: 'post',
    text: 'Let\'s not forget that the US postal service (USPS), one of America\'s best ideas, helped saved its own democracy (not to mention the health of its citizens). That\'s worth celebrating. (So let\'s fight for a universal mail ballot system in the next administration, please).',
    likes: 4,
    retweets: 1
  },
  {
    id: '1326069971886092288',
    created: 1604994693000,
    type: 'post',
    text: 'Since the release cycle for Asciidoctor 2.0.11 was so long, it introduced some surprises. Asciidoctor 2.0.12 is out to smooth out those edges. <a href="https://github.com/asciidoctor/asciidoctor/releases/tag/v2.0.12" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor/releases/tag/v2.0.12</a>',
    likes: 34,
    retweets: 10
  },
  {
    id: '1326055305805926403',
    created: 1604991196000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> To elaborate, it does run the workflow on the fork, so you can still observe it run. It just doesn\'t synchronize with upstream in this case. But as soon as I restore the CI config and force push, then it synchronizes.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1326053882829697024" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1326053882829697024</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1326053291860480003',
    created: 1604990716000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Okay, so I was mistaken. It\'s a special case. If you force push, but modify the CI config (for example to trim down the number of jobs for debugging), it won\'t rerun. I guess that\'s some sort of protection.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1326052978432892929" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1326052978432892929</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1326048144421838848',
    created: 1604989489000,
    type: 'post',
    text: 'GitHub Actions doesn\'t seem to run if you force push to a pull request branch. Does anyone know what the config is to make it run in this scenario?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1325690475773222914',
    created: 1604904214000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aheritier" rel="noopener noreferrer" target="_blank">@aheritier</a> That\'s the sanitized explanation. The real reason was (and still is) to keep rich white land owners in power.<br><br>In reply to: <a href="https://x.com/aheritier/status/1325689788373065728" rel="noopener noreferrer" target="_blank">@aheritier</a> <span class="status">1325689788373065728</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1325604778357874689',
    created: 1604883782000,
    type: 'quote',
    text: 'For the animals!<br><br>Quoting: <a href="https://x.com/OurRevolution/status/1325604018505199616" rel="noopener noreferrer" target="_blank">@OurRevolution</a> <span class="status">1325604018505199616</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1325600655088656384',
    created: 1604882799000,
    type: 'post',
    text: 'To their credit, they issued me a refund. Good on them. My advice still stands, though.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1325286340854833152',
    created: 1604807860000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/hsablonniere" rel="noopener noreferrer" target="_blank">@hsablonniere</a> A fresh start.<br><br>In reply to: <a href="https://x.com/hsablonniere/status/1323742044431568897" rel="noopener noreferrer" target="_blank">@hsablonniere</a> <span class="status">1323742044431568897</span>',
    photos: ['<div class="item"><img class="photo" src="media/1325286340854833152.jpg"></div>'],
    likes: 1,
    retweets: 0
  },
  {
    id: '1325283691065610240',
    created: 1604807229000,
    type: 'post',
    text: 'not reaching =&gt; not be reaching',
    likes: 0,
    retweets: 0
  },
  {
    id: '1325283054642851840',
    created: 1604807077000,
    type: 'post',
    text: 'Those on the right will benefit from these too, so that\'s all the reaching they\'re going to get. I would never support a program or policy that hurts people or makes life harder (except for the rich, because they owe us the fair share they haven\'t been paying).',
    likes: 1,
    retweets: 0
  },
  {
    id: '1325282562617430017',
    created: 1604806960000,
    type: 'post',
    text: 'I will not reaching across the aisle or uniting. What I will be doing is continuing to advocate for programs &amp; policies that help us all, including free healthcare, a rapid &amp; just green energy transition, expanding women\'s &amp; LGBTQA+ rights, voting rights, free education, etc.',
    likes: 7,
    retweets: 0
  },
  {
    id: '1325192418296123393',
    created: 1604785468000,
    type: 'post',
    text: 'In a bit of tech news, I think I\'m getting the hang of GitHub Actions. I see a lot of potential here. It\'s strongest feature is the deep integration with GitHub events (down to the level of a comment on an issue).',
    likes: 4,
    retweets: 0
  },
  {
    id: '1325191188224245768',
    created: 1604785174000,
    type: 'post',
    text: 'everyday =&gt; every day<br><br>...and if every day is what it takes, we\'ll do it every day.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1325189291807420416',
    created: 1604784722000,
    type: 'post',
    text: 'Cake for breakfast. It\'s not everyday that you defeat fascism. Let\'s go!',
    photos: ['<div class="item"><img class="photo" src="media/1325189291807420416.jpg"></div>'],
    likes: 15,
    retweets: 0
  },
  {
    id: '1325188578352062466',
    created: 1604784552000,
    type: 'post',
    text: 'We don\'t want back to normal. We want on to better. Don\'t settle just because you\'ve been abused. Ask for what you deserve. And we all deserve better.',
    likes: 7,
    retweets: 2
  },
  {
    id: '1325188082736361473',
    created: 1604784434000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> Hard agree.<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1325180578560307201" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1325180578560307201</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1325187050228703233',
    created: 1604784188000,
    type: 'reply',
    text: '@jbryant787 I was thinking a compass would probably be a really good thing to have right now too.<br><br>In reply to: @jbryant787 <span class="status">&lt;deleted&gt;</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1325162122993528833',
    created: 1604778245000,
    type: 'post',
    text: 'My anxiety level just dropped in half. We aren\'t out of the woods yet (or will be anytime soon), but we\'ve picked up a flashlight.',
    likes: 18,
    retweets: 0
  },
  {
    id: '1324846243063894016',
    created: 1604702933000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> This is so far from a difference of opinion. A difference of opinion is what the tax rate should be. We are talking about the very pillars of democracy not only being threated, but completely eroded.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1324740309264158722" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1324740309264158722</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1324845653051154432',
    created: 1604702792000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> What\'s juvenile is to support a fascist.<br><br>I realized that by continuing to have relationships with people who don\'t respect democracy, I\'m enabling them. And I won\'t stand for it. For someone who believes in democracy, this is not a controversial position.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1324817366518124544" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1324817366518124544</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1324786724661161991',
    created: 1604688743000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Trump is a fascist and his actions show that irrefutably. Among other reasons, this is the one in particular that makes my position non-negotiable. It\'s not juvenile to stand up against fascism.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1324740309264158722" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1324740309264158722</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1324667227220553731',
    created: 1604660252000,
    type: 'quote',
    text: 'This is the real story. The only way out of this mess is for the people to seize back control, and the ballot box is a very effective way to do it (when it starts with organizing on the ground).<br><br>Quoting: <a href="https://x.com/TheWomensOrg/status/1324653254450569218" rel="noopener noreferrer" target="_blank">@TheWomensOrg</a> <span class="status">1324653254450569218</span>',
    likes: 8,
    retweets: 1
  },
  {
    id: '1324666313067749377',
    created: 1604660034000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> Thanks!<br><br>In reply to: <a href="https://x.com/garrett/status/1324658287376257024" rel="noopener noreferrer" target="_blank">@garrett</a> <span class="status">1324658287376257024</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1324652698524815360',
    created: 1604656788000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/benignbala" rel="noopener noreferrer" target="_blank">@benignbala</a> <a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> In the end, it\'s about a contract you establish with users. All terms have context. If you establish the contract with users, and the term can be understood and not otherwise cause cognitive dissonance, then I think that\'s what matters.<br><br>In reply to: <a href="https://x.com/benignbala/status/1324647102136283137" rel="noopener noreferrer" target="_blank">@benignbala</a> <span class="status">1324647102136283137</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1324640502399062016',
    created: 1604653881000,
    type: 'post',
    text: 'There are solid arguments for "current", and it does seem to be the emerging choice, but "latest" is the clear front runner.<br><br>Quoting: <a href="#1323543716796096514">1323543716796096514</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1324153683995627520',
    created: 1604537814000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/edburns" rel="noopener noreferrer" target="_blank">@edburns</a> <a class="mention" href="https://x.com/jasonintrator" rel="noopener noreferrer" target="_blank">@jasonintrator</a> Very interesting to note. Thank you for drawing my attention to it.<br><br>In reply to: <a href="https://x.com/edburns/status/1324149754104786945" rel="noopener noreferrer" target="_blank">@edburns</a> <span class="status">1324149754104786945</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1324100146297069571',
    created: 1604525050000,
    type: 'quote',
    text: '"Hold me accountable. Push me and my colleagues."<br><br>Now that\'s a representative who understands the job description. What a contrast to the status quo.<br><br>Quoting: <a href="https://x.com/JamaalBowmanNY/status/1323835231418359813" rel="noopener noreferrer" target="_blank">@JamaalBowmanNY</a> <span class="status">1323835231418359813</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1324097110791184384',
    created: 1604524326000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> Well said. And that goes hand-in-hand with big money in politics, which is also a cancer.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1324096020444258310" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1324096020444258310</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1324096786252754944',
    created: 1604524249000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> It\'s necessary.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1324026775135903744" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1324026775135903744</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1324090674912788480',
    created: 1604522791000,
    type: 'post',
    text: 'It\'s important for Americans to recognize that we wouldn\'t be in this situation if we had a national popular vote, ranked choice voting, &amp; a universal mail ballot system. The fact that we keep going down this road is the product of self harm inflicted by an unhealthy nostalgia.',
    likes: 10,
    retweets: 0
  },
  {
    id: '1324088391240085504',
    created: 1604522247000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/waynebeaton" rel="noopener noreferrer" target="_blank">@waynebeaton</a> Damn, damn hard.<br><br>In reply to: <a href="https://x.com/waynebeaton/status/1324081369073504257" rel="noopener noreferrer" target="_blank">@waynebeaton</a> <span class="status">1324081369073504257</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1324088291814092800',
    created: 1604522223000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lanceball" rel="noopener noreferrer" target="_blank">@lanceball</a> You captured my thoughts very accurately. Except for the last one. I\'m not willing to give up this country or the vision we have for it. We\'re going to drive those who cannot accept others back under the rock from which they came.<br><br>In reply to: <a href="https://x.com/lanceball/status/1323991192162521088" rel="noopener noreferrer" target="_blank">@lanceball</a> <span class="status">1323991192162521088</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1323790145896751105',
    created: 1604451140000,
    type: 'post',
    text: 'If your response to, "I don\'t want to be friends with someone who supports a racist" is, "Then I guess we can\'t be friends." 😱 I just have no words.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1323740346161922048',
    created: 1604439267000,
    type: 'post',
    text: 'As has become a tradition for me on stressful days like today, I\'m spending the day planting new life.🌱',
    likes: 15,
    retweets: 0
  },
  {
    id: '1323585784683798528',
    created: 1604402416000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rmannibucau" rel="noopener noreferrer" target="_blank">@rmannibucau</a> @sormuras This is already a configurable feature of Antora (starting in Antora 3). I\'m just trying to decide how to apply it.<br><br>In reply to: <a href="https://x.com/rmannibucau/status/1323574920140476417" rel="noopener noreferrer" target="_blank">@rmannibucau</a> <span class="status">1323574920140476417</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1323552050219679745',
    created: 1604394373000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> I have preferred "latest" in the past, but I\'m starting to warm up to "current". Naming in technology is a minefield.<br><br>In reply to: <a href="https://x.com/garrett/status/1323544310697123841" rel="noopener noreferrer" target="_blank">@garrett</a> <span class="status">1323544310697123841</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1323551789300379648',
    created: 1604394311000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> In this context, latest is short for "latest stable release", so it wouldn\'t include prereleases. And it definitely doesn\'t account for multiple release lines. But someone else could interpret it differently. That\'s the challenge of using a single word to represent a concept.<br><br>In reply to: <a href="https://x.com/joschi83/status/1323545234651947008" rel="noopener noreferrer" target="_blank">@joschi83</a> <span class="status">1323545234651947008</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1323543716796096514',
    created: 1604392386000,
    type: 'post',
    text: 'If you use a symbolic name to represent the latest / current / stable version in the URL of your docs, which name do you prefer?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1323508628582617088',
    created: 1604384021000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ramiromorales" rel="noopener noreferrer" target="_blank">@ramiromorales</a> Thanks! And thanks for pointing that out. I guess the release didn\'t want anything to do with 2020 ;)<br><br>In reply to: <a href="https://x.com/ramiromorales/status/1323485412057403392" rel="noopener noreferrer" target="_blank">@ramiromorales</a> <span class="status">1323485412057403392</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1323474677658910721',
    created: 1604375926000,
    type: 'post',
    text: 'To give you some positive news to celebrate, I present you with the 2.0.11 release of Asciidoctor. <a href="https://github.com/asciidoctor/asciidoctor/releases/tag/v2.0.11" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor/releases/tag/v2.0.11</a> 🎉',
    likes: 55,
    retweets: 12
  },
  {
    id: '1323465795070955520',
    created: 1604373809000,
    type: 'post',
    text: 'Exactly what I need right now, a diversion.',
    photos: ['<div class="item"><img class="photo" src="media/1323465795070955520.jpg"></div>'],
    likes: 1,
    retweets: 0
  },
  {
    id: '1323370680251940864',
    created: 1604351131000,
    type: 'post',
    text: 'If you support Trump, I don\'t want to know you. You\'re no longer part of my life. Trump has terrorized this country &amp; the world with his narcissism, reckless conduct, racism, sexism, &amp; attacks on democracy. If you condone that, you\'ll have to live your life without me in it.',
    likes: 21,
    retweets: 0
  },
  {
    id: '1323160150501281792',
    created: 1604300937000,
    type: 'reply',
    text: '@jbryant787 I believe in the youth. If we do manage to squash fascism, it will largely thanks to their efforts (and those who help lift up their voices).<br><br>In reply to: @jbryant787 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1323118043598184450',
    created: 1604290898000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/karenmcgrane" rel="noopener noreferrer" target="_blank">@karenmcgrane</a> 💯<br><br>In reply to: <a href="https://x.com/karenmcgrane/status/1323097355751727111" rel="noopener noreferrer" target="_blank">@karenmcgrane</a> <span class="status">1323097355751727111</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1323117625640038400',
    created: 1604290798000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> <a class="mention" href="https://x.com/ballottrax" rel="noopener noreferrer" target="_blank">@ballottrax</a> The pandemic might actually be doing something positive for voting. Here\'s hoping.<br><br>In reply to: <a href="https://x.com/arungupta/status/1323106798769893378" rel="noopener noreferrer" target="_blank">@arungupta</a> <span class="status">1323106798769893378</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1323115584641355776',
    created: 1604290312000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> I\'ve got a stash of Four Roses Single Barrel. We\'ll see if stretches.<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1323099133125685249" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1323099133125685249</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1323115389300084738',
    created: 1604290265000,
    type: 'post',
    text: 'I just made an old-fashioned for the first time. I call it getting ready. 🥃',
    likes: 4,
    retweets: 0
  },
  {
    id: '1323049204558618624',
    created: 1604274486000,
    type: 'post',
    text: '...and of toxic masculinity.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1323047702930747392',
    created: 1604274128000,
    type: 'post',
    text: 'If you\'re wondering if I\'m experiencing what you\'re reading about in the news, there\'s a chump driving around my building in a huge truck flying symbols of hate, revving his engine to get attention. Don\'t worry, it\'s just the death throes of the patriarchy.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1323016628599095296',
    created: 1604266719000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/justinhopper" rel="noopener noreferrer" target="_blank">@justinhopper</a> ...also aware enough to realize that I identify as the very thing I was taught in school to hate, a socialist.<br><br>In reply to: <a href="https://x.com/justinhopper/status/1322398082160422912" rel="noopener noreferrer" target="_blank">@justinhopper</a> <span class="status">1322398082160422912</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1322834576541061120',
    created: 1604223314000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mxmkm" rel="noopener noreferrer" target="_blank">@mxmkm</a> I would say so. As always, consult the CHANGELOG for all the gory details. <a href="https://github.com/asciidoctor/asciidoctor/blob/master/CHANGELOG.adoc#unreleased" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor/blob/master/CHANGELOG.adoc#unreleased</a><br><br>In reply to: <a href="https://x.com/mxmkm/status/1322830071627919364" rel="noopener noreferrer" target="_blank">@mxmkm</a> <span class="status">1322830071627919364</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1322824544109826050',
    created: 1604220922000,
    type: 'post',
    text: 'A new Asciidoctor patch release (2.0.11) is brewing. 🧙',
    likes: 16,
    retweets: 0
  },
  {
    id: '1322680325915303936',
    created: 1604186538000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/TheSteve0" rel="noopener noreferrer" target="_blank">@TheSteve0</a> Here here.<br><br>In reply to: <a href="https://x.com/TheSteve0/status/1322674487813263361" rel="noopener noreferrer" target="_blank">@TheSteve0</a> <span class="status">1322674487813263361</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1322669937190596608',
    created: 1604184061000,
    type: 'post',
    text: 'I\'ve already picked my drinks for Tuesday, Wednesday, and however many days it takes to get through this.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1322434691748622337',
    created: 1604127974000,
    type: 'post',
    text: 'If you\'re considering buying a subscription to HBO, do not do it through <a class="mention" href="https://x.com/Roku" rel="noopener noreferrer" target="_blank">@Roku</a>. It\'s a scam. You no longer get access to the HBO app itself (now called HBO Max). They only allow you to watch it through the Roku channel. Just buy the subscription from HBO directly.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1322323971941916672',
    created: 1604101577000,
    type: 'post',
    text: 'There\'s a very haunting kind of rage building up inside of me caused by people who I identified with / idolized / looked up to when I was growing up turning out to be complete trash humans. I\'m feeling a sort of disassociation with my own life. And it\'s disturbing.',
    likes: 5,
    retweets: 1
  },
  {
    id: '1322322134434078720',
    created: 1604101139000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BrettFavre" rel="noopener noreferrer" target="_blank">@BrettFavre</a> You used to my favorite quarterback when I was growing up. You are now dead to me. I will forget you ever existed. You have chosen the side of hate, fear, misogyny, fascism, &amp; racism, and it\'s unforgivable.<br><br>In reply to: <a href="https://x.com/BrettFavre/status/1322154221584732163" rel="noopener noreferrer" target="_blank">@BrettFavre</a> <span class="status">1322154221584732163</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1321933573499224064',
    created: 1604008498000,
    type: 'post',
    text: 'Of course, there will be people who won\'t follow the rule, or won\'t want to. But that\'s where a little peer pressure / encouragement can fill in. It works for other rules we\'ve agreed to follow (like forming lines or not talking during a movie), so why wouldn\'t it work here too?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1321931692983025664',
    created: 1604008050000,
    type: 'post',
    text: 'Elsewhere in the US, a lot of people aren\'t wearing masks because there\'s just no leadership. Instead, there\'s only confusion &amp; misinformation, which leads to apathy &amp; resentment. If the rule is clear and the reasoning well stated, there\'s evidence people would just do it.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1321930710664425474',
    created: 1604007816000,
    type: 'post',
    text: 'A bit of observation. As I was leaving Breckenridge two weeks ago, I drove down the main street. It was full of people out walking, shopping, &amp; hanging out. And yet, every last person was wearing a mask. How could that be? Turns out, it\'s mandated. Proof that fair rules do work.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1321929031252541441',
    created: 1604007415000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fwilhe" rel="noopener noreferrer" target="_blank">@fwilhe</a> Oh, there will be lasting effects. And there are many, many problems. But first, we need to stop the bleeding that is the current administration. Because we can\'t accomplish anything until that is solved. But solve we must.<br><br>In reply to: <a href="https://x.com/fwilhe/status/1321923693149376514" rel="noopener noreferrer" target="_blank">@fwilhe</a> <span class="status">1321923693149376514</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1321922988069904385',
    created: 1604005975000,
    type: 'post',
    text: '...and now confirmed as accepted! Voting is complete!',
    likes: 4,
    retweets: 0
  },
  {
    id: '1321917459419459584',
    created: 1604004657000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agentdero" rel="noopener noreferrer" target="_blank">@agentdero</a> I guess it\'s better than Travis CI jobs, which just get stuck for days. 🤷<br><br>In reply to: <a href="https://x.com/agentdero/status/1321914136377192448" rel="noopener noreferrer" target="_blank">@agentdero</a> <span class="status">1321914136377192448</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1321900777204404224',
    created: 1604000679000,
    type: 'quote',
    text: 'For context, 2.78 million voted in 2016.<br><br>Quoting: <a href="https://x.com/JenaGriswold/status/1321891299239157761" rel="noopener noreferrer" target="_blank">@JenaGriswold</a> <span class="status">1321891299239157761</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1321595106709835779',
    created: 1603927802000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> While I get this is a joke, defunding and eliminating are two very different things. Defunding is about reallocating funds to social services that will effectively deescalate and avoid needless and wrongful deaths caused by over-equipped police armies.<br><br>In reply to: <a href="https://x.com/brunoborges/status/1321571492786679809" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1321571492786679809</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1321569112636575744',
    created: 1603921604000,
    type: 'post',
    text: 'The #VOTE is in!<br><br>...and confirmed as received by the state of Colorado',
    likes: 5,
    retweets: 0,
    tags: ['vote']
  },
  {
    id: '1321558018316623878',
    created: 1603918959000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ChuckDurfee" rel="noopener noreferrer" target="_blank">@ChuckDurfee</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> I think you\'ve misunderstood how Asciidoctor PDF works. It does not use CSS. If you want to be able to use CSS, you\'d have to use Asciidoctor Web PDF.<br><br>In reply to: <a href="https://x.com/ChuckDurfee/status/1321549953982885889" rel="noopener noreferrer" target="_blank">@ChuckDurfee</a> <span class="status">1321549953982885889</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1321537660880478210',
    created: 1603914105000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ChuckDurfee" rel="noopener noreferrer" target="_blank">@ChuckDurfee</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> If you\'d like to see this happen, please file an issue. I\'ll follow up there.<br><br>In reply to: <a href="https://x.com/ChuckDurfee/status/1321517973627043841" rel="noopener noreferrer" target="_blank">@ChuckDurfee</a> <span class="status">1321517973627043841</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1321391986025271298',
    created: 1603879374000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ChuckDurfee" rel="noopener noreferrer" target="_blank">@ChuckDurfee</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Not currently. I would consider it, though. It even feels like a thing the theme should be able to control.<br><br>In reply to: <a href="https://x.com/ChuckDurfee/status/1321111864382689281" rel="noopener noreferrer" target="_blank">@ChuckDurfee</a> <span class="status">1321111864382689281</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1321372437099868161',
    created: 1603874713000,
    type: 'post',
    text: 'You know, when it happened, his mother tried to ask me what he did to me. I was too ashamed to tell her I almost died. I somehow felt like it was my fault, or that I was going to ruin his life. Instead, I said nothing. But I wasn\'t okay.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1321369972279402496',
    created: 1603874125000,
    type: 'post',
    text: 'In case you were wondering from this tweet, yes, I was bullied extremely severely by my cousin when I was young. I once thought he was going to kill me (because I almost suffocated to death).',
    likes: 0,
    retweets: 0
  },
  {
    id: '1321369170315931649',
    created: 1603873934000,
    type: 'post',
    text: 'In other words, cruelty is the point.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1321368867789131776',
    created: 1603873862000,
    type: 'post',
    text: 'I can always detect when someone is harassing me because that person shows no remorse. It\'s never "Oh, sorry to have offended you. Can we start over?" Instead, it\'s just a unrelenting stream of provocation. It\'s exactly what my cousin used to do to me when I was young.',
    likes: 1,
    retweets: 1
  },
  {
    id: '1321365732697935872',
    created: 1603873115000,
    type: 'post',
    text: 'The commission reports are better than nothing, but the sample sizes are small &amp; inconsistently sampled (sometimes it\'s mostly legal professionals, sometimes not). We essentially have to trust the commission, but that just feels like rubber stamping.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1321364989869895680',
    created: 1603872938000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/JenaGriswold" rel="noopener noreferrer" target="_blank">@JenaGriswold</a> I just voted &amp; wanted to give some feedback. I think having all those judges on the ballot to confirm with limited information about them is a form of voter disenfranchisement. No one could possibly claim to make an informed decision.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1321359986207850499',
    created: 1603871745000,
    type: 'post',
    text: 'Putting progressives aside, someone who harasses, taunts, or shames others isn\'t a decent person, period. Don\'t tolerate it.',
    likes: 0,
    retweets: 1
  },
  {
    id: '1321358862708953088',
    created: 1603871477000,
    type: 'post',
    text: 'Oh, and it\'s totally fine for someone to harass you on Twitter. According to Twitter, that\'s not a violation of their rules. So that\'s good to know.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1321358682114809857',
    created: 1603871434000,
    type: 'post',
    text: 'Folks, if someone is harassing you and claims to be progressive, don\'t blame all progressives. That person is just an asshole. And it\'s even possible they\'re doing it to get you to dislike progressives. In my opinion, you cannot harass people and be progressive.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1321352528903073792',
    created: 1603869967000,
    type: 'reply',
    text: '@Ihavenousefora1 I asked you to stop, you have refused to stop. I\'m reporting you for harassment. This is ridiculous. I don\'t have to take this.<br><br>In reply to: <a href="https://x.com/comaly93g42b84/status/1321351668882178049" rel="noopener noreferrer" target="_blank">@comaly93g42b84</a> <span class="status">1321351668882178049</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1321348999442395137',
    created: 1603869125000,
    type: 'reply',
    text: '@Ihavenousefora1 Um, you jumped into my mentions to pick a fight. Now get the hell out. I have no interest in having a conversation or a fight with you.<br><br>In reply to: <a href="https://x.com/comaly93g42b84/status/1321294702705074176" rel="noopener noreferrer" target="_blank">@comaly93g42b84</a> <span class="status">1321294702705074176</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1321347488012398594',
    created: 1603868765000,
    type: 'reply',
    text: '@Ihavenousefora1 You obviously didn\'t read my thread. Seriously, go away.<br><br>In reply to: <a href="https://x.com/comaly93g42b84/status/1321293894122901509" rel="noopener noreferrer" target="_blank">@comaly93g42b84</a> <span class="status">1321293894122901509</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1321266965302759426',
    created: 1603849567000,
    type: 'post',
    text: 'I really want this election to be over. It\'s destroying my mental state.',
    likes: 16,
    retweets: 0
  },
  {
    id: '1321255881346932736',
    created: 1603846924000,
    type: 'reply',
    text: '@Ihavenousefora1 Btw, I fault no one nor do I throw insults at someone who wants to vote third party. If that\'s what you want, do it. I might do so again in the future. But for reasons stated, I\'m not doing it this time.<br><br>In reply to: <a href="#1321253231721263104">1321253231721263104</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1321253231721263104',
    created: 1603846292000,
    type: 'reply',
    text: '@Ihavenousefora1 If we want to talk about what it means to not be an intelligent person, it is to deny reality. Denying reality gets us now where. We have to be mature and face it.<br><br>In reply to: <a href="#1321252231958544385">1321252231958544385</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1321252231958544385',
    created: 1603846054000,
    type: 'reply',
    text: '@Ihavenousefora1 I would love to be able to vote for Bernie. He\'s not on the ballot. As frustrated as I am, there\'s literally nothing I can do about that.<br><br>In reply to: <a href="#1321251947966390272">1321251947966390272</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1321251947966390272',
    created: 1603845986000,
    type: 'reply',
    text: '@Ihavenousefora1 You\'re offering delusions. I\'m not supporting Biden. I\'m voting for him because one of two people are going to win this election, and the alternative is worse for this environment and the movement to protect it. It\'s as simple as that. I was very clear the fight isn\'t over.<br><br>In reply to: <a href="https://x.com/comaly93g42b84/status/1321240072503676928" rel="noopener noreferrer" target="_blank">@comaly93g42b84</a> <span class="status">1321240072503676928</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1321239104370008067',
    created: 1603842924000,
    type: 'post',
    text: 'If there was ever a time to be motivated by fear and uncertainty. Now would be that time. Vote, especially for down-ballot candidates who are the future.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1321238825822089221',
    created: 1603842858000,
    type: 'quote',
    text: 'Mistakes happen. You can get yourself a new ballot if you need one.<br><br>Quoting: <a href="https://x.com/DenverElections/status/1318251730501394432" rel="noopener noreferrer" target="_blank">@DenverElections</a> <span class="status">1318251730501394432</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1321229774858063872',
    created: 1603840700000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> That\'s a very plausible assertion.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1321208726452359170" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1321208726452359170</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1321204173501652993',
    created: 1603834596000,
    type: 'post',
    text: 'Another example is "I\'m a technical writer, I don\'t know how to write JavaScript". You should be looking for a UI developer. What\'s stopping you? Do waiters assume they can cook the food too?',
    likes: 4,
    retweets: 1
  },
  {
    id: '1321203397228310529',
    created: 1603834411000,
    type: 'post',
    text: 'For example, one thing I hear a lot from developers is "I don\'t know how to design." Okay, and who said you could? You should be looking for a designer. What\'s stopping you? Do accountants assume they can perform heart surgery? What are we not getting about specialties?',
    likes: 4,
    retweets: 0
  },
  {
    id: '1321202521960894464',
    created: 1603834202000,
    type: 'post',
    text: 'The tech industry has gotten to a point where asking for professional help is forbidden. The mindset seems to be: "If I ask for help, I could get fired. So I\'m going to struggle through no matter the cost / assume I can be the expert at anything." It\'s really a sad state.',
    likes: 8,
    retweets: 0
  },
  {
    id: '1321146374998487041',
    created: 1603820816000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/badmannersfood" rel="noopener noreferrer" target="_blank">@badmannersfood</a> I\'ll have you know, my spouse ONLY has cake for breakfast ;)<br><br>In reply to: <a href="https://x.com/badmannersfood/status/1321141954042757120" rel="noopener noreferrer" target="_blank">@badmannersfood</a> <span class="status">1321141954042757120</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1321040608400568321',
    created: 1603795599000,
    type: 'reply',
    text: '@Darien4Commish Easy choice! You just picked up two votes from my spouse and I. Good luck! And...SCIENCE!<br><br>In reply to: @Darien4Commish <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1321006819976699911',
    created: 1603787543000,
    type: 'reply',
    text: '@Ihavenousefora1 Don\'t come into my mentions and launch insults at me. You don\'t know me. By saying what you said, it shows you\'re the one who lacks intelligence. If you don\'t have anything productive to say, get lost.<br><br>In reply to: <a href="https://x.com/comaly93g42b84/status/1320996243510874113" rel="noopener noreferrer" target="_blank">@comaly93g42b84</a> <span class="status">1320996243510874113</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1320964381568462849',
    created: 1603777425000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> My heart is with you and your family. May you fill the next few days (or however many) with only beautiful thoughts of precious memories. The world can wait.<br><br>In reply to: <a href="https://x.com/starbuxman/status/1320961210632736769" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">1320961210632736769</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1320927038128095232',
    created: 1603768522000,
    type: 'post',
    text: 'The Democrats are fighting fire with kindling.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1320852948314845184',
    created: 1603750857000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ssteingraber1" rel="noopener noreferrer" target="_blank">@ssteingraber1</a> I suppose I did learn that being an ally sometimes means doing something that I might not have otherwise done. I had to defer my own path / agenda to strengthen the voices of my allies.<br><br>In reply to: <a href="https://x.com/ssteingraber1/status/1320847178391322625" rel="noopener noreferrer" target="_blank">@ssteingraber1</a> <span class="status">1320847178391322625</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1320850717997842432',
    created: 1603750326000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ssteingraber1" rel="noopener noreferrer" target="_blank">@ssteingraber1</a> 👍<br><br>In reply to: <a href="https://x.com/ssteingraber1/status/1320847178391322625" rel="noopener noreferrer" target="_blank">@ssteingraber1</a> <span class="status">1320847178391322625</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1320846967476371456',
    created: 1603749431000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ssteingraber1" rel="noopener noreferrer" target="_blank">@ssteingraber1</a> It\'s not really about being coachable. It\'s about being an ally. I didn\'t learn anything. I\'m just staying true to my promise to support fellow climate activists. They have the strategy and plan and I\'m pitching in as they have asked.<br><br>In reply to: <a href="https://x.com/ssteingraber1/status/1320538949769699330" rel="noopener noreferrer" target="_blank">@ssteingraber1</a> <span class="status">1320538949769699330</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1320826578696204289',
    created: 1603744570000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bbatsov" rel="noopener noreferrer" target="_blank">@bbatsov</a> Congratulations on hitting this milestone! I use RuboCop every day on Asciidoctor PDF and it works great (and it\'s saved my tail more than once).<br><br>In reply to: <a href="https://x.com/bbatsov/status/1318874754187730946" rel="noopener noreferrer" target="_blank">@bbatsov</a> <span class="status">1318874754187730946</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1320811231339401216',
    created: 1603740911000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/vinton_IV" rel="noopener noreferrer" target="_blank">@vinton_IV</a> Don\'t clutter my timeline with this trash.<br><br>In reply to: <a href="https://x.com/vinton_IV/status/1320702674539552768" rel="noopener noreferrer" target="_blank">@vinton_IV</a> <span class="status">1320702674539552768</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1320790725559160835',
    created: 1603736022000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CedricChampeau" rel="noopener noreferrer" target="_blank">@CedricChampeau</a> I experience that as well, so it\'s not just you. Often times it happens when I\'m anxious or nervous. For me, the name is there, it\'s just blocked by other thoughts.<br><br>In reply to: <a href="https://x.com/CedricChampeau/status/1320786318264274947" rel="noopener noreferrer" target="_blank">@CedricChampeau</a> <span class="status">1320786318264274947</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1320671564459773954',
    created: 1603707612000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CrozierMark" rel="noopener noreferrer" target="_blank">@CrozierMark</a> It was absolutely not a fair contest, but it never was and we knew that going in. They stacked the deck against him as soon as he pulled ahead. This is not a secret.<br><br>In reply to: <a href="https://x.com/CrozierMark/status/1320637161511813121" rel="noopener noreferrer" target="_blank">@CrozierMark</a> <span class="status">1320637161511813121</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1320671152017043456',
    created: 1603707514000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/YassinHajaj" rel="noopener noreferrer" target="_blank">@YassinHajaj</a> Grow up.<br><br>In reply to: <a href="https://x.com/YassinHajaj/status/1320647463414468608" rel="noopener noreferrer" target="_blank">@YassinHajaj</a> <span class="status">1320647463414468608</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1320500402949746689',
    created: 1603666804000,
    type: 'post',
    text: 'And I\'m looking forward to making progress in down-ballot races.',
    likes: 18,
    retweets: 0
  },
  {
    id: '1320500399476862976',
    created: 1603666803000,
    type: 'post',
    text: 'But maybe we\'ll be able to focus on concrete policy fights instead of dealing with a narcissist trying to make a show out of our livelihood. The choices are crap. Showing support for my aforementioned allies is how I feel I can make the best of this situation.',
    likes: 26,
    retweets: 0
  },
  {
    id: '1320500396796702720',
    created: 1603666802000,
    type: 'post',
    text: 'Biden is a neo-liberal, corporate politician who robbed Bernie of the nomination for President. I\'m never going to forgive or excuse him for that. The sun may rise on Jan 20, but our problems will not have been cured. We\'ll still have to fight for what is right &amp; just.',
    likes: 20,
    retweets: 1
  },
  {
    id: '1320500395223834624',
    created: 1603666802000,
    type: 'post',
    text: 'They asked me because they believe it will give them a path (or at least breathing room) to succeed in their mission of protecting the environment on this planet that we all rely on to survive. My objective is to help them do so. I have been very clear about that.',
    likes: 28,
    retweets: 0
  },
  {
    id: '1320500393290264576',
    created: 1603666802000,
    type: 'post',
    text: 'I\'ve decided to vote for Biden. It\'s not because I like him as a candidate or think he\'ll be a great president. I have no misconceptions about who he is &amp; what he stands for. Rather, it\'s because <a class="mention" href="https://x.com/sunrisemvmt" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a>, <a class="mention" href="https://x.com/GretaThunberg" rel="noopener noreferrer" target="_blank">@GretaThunberg</a>, <a class="mention" href="https://x.com/Jamie_Margolin" rel="noopener noreferrer" target="_blank">@Jamie_Margolin</a>, &amp; <a class="mention" href="https://x.com/AlexandriaV2005" rel="noopener noreferrer" target="_blank">@AlexandriaV2005</a> have asked me to.',
    likes: 138,
    retweets: 21
  },
  {
    id: '1320110002082701312',
    created: 1603573725000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> <a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> Hugs from me too. We\'re here for you! Always will be.<br><br>In reply to: <a href="https://x.com/majson/status/1320084122702929923" rel="noopener noreferrer" target="_blank">@majson</a> <span class="status">1320084122702929923</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1319952228933718016',
    created: 1603536109000,
    type: 'reply',
    text: '@claire_SOLA <a class="mention" href="https://x.com/badmannersfood" rel="noopener noreferrer" target="_blank">@badmannersfood</a> The funny part is that I thought I had ruined the crust because it wouldn\'t stay together when I rolled it out. So I decide to press it into the dish instead. And it blind baked to absolute perfection. I attribute at least 50% of that outcome to luck ;)<br><br>In reply to: <a href="https://x.com/Chynes31/status/1319950332722642944" rel="noopener noreferrer" target="_blank">@Chynes31</a> <span class="status">1319950332722642944</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1319949480884686848',
    created: 1603535454000,
    type: 'post',
    text: 'I made the most perfect dessert I\'ve ever tasted, topped with mint I grew from cuttings. There\'s something insanely satisfying about that. It\'s a vegan chocolate coconut cream pie, recipe by <a class="mention" href="https://x.com/badmannersfood" rel="noopener noreferrer" target="_blank">@badmannersfood</a>.',
    photos: ['<div class="item"><img class="photo" src="media/1319949480884686848.jpg"></div>'],
    likes: 9,
    retweets: 0
  },
  {
    id: '1319927297999130624',
    created: 1603530165000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> Speaking of tops, did you see the news about OSIRIS-REx touching down on the asteroid Bennu? Lots of good documentary material about it on YouTube.<br><br>In reply to: <a href="https://x.com/alexsotob/status/1319908525653561344" rel="noopener noreferrer" target="_blank">@alexsotob</a> <span class="status">1319908525653561344</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1319894150079082496',
    created: 1603522262000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agentdero" rel="noopener noreferrer" target="_blank">@agentdero</a> Accurate.<br><br>In reply to: <a href="https://x.com/agentdero/status/1319850279441821698" rel="noopener noreferrer" target="_blank">@agentdero</a> <span class="status">1319850279441821698</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1319720978927161345',
    created: 1603480975000,
    type: 'quote',
    text: 'Yet another huge benefit of a mail / ballot box balloting system. If we combined this with ranked choice voting, then our democracy would be much more resilient.<br><br>Quoting: <a href="https://x.com/DenverElections/status/1319715483814260736" rel="noopener noreferrer" target="_blank">@DenverElections</a> <span class="status">1319715483814260736</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1319720335525048322',
    created: 1603480821000,
    type: 'quote',
    text: 'I\'m looking forward to adding to that total this weekend.<br><br>Quoting: <a href="https://x.com/JenaGriswold/status/1319720041135308801" rel="noopener noreferrer" target="_blank">@JenaGriswold</a> <span class="status">1319720041135308801</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1319695973933744128',
    created: 1603475013000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> <a class="mention" href="https://x.com/hsablonniere" rel="noopener noreferrer" target="_blank">@hsablonniere</a> The only Markdown is custom Markdown.<br><br>In reply to: <a href="https://x.com/ggrossetie/status/1319694993020362752" rel="noopener noreferrer" target="_blank">@ggrossetie</a> <span class="status">1319694993020362752</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1319601188958957568',
    created: 1603452415000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> <a class="mention" href="https://x.com/slonopotamus" rel="noopener noreferrer" target="_blank">@slonopotamus</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Most definitely.<br><br>In reply to: <a href="https://x.com/agoncal/status/1319590702905425921" rel="noopener noreferrer" target="_blank">@agoncal</a> <span class="status">1319590702905425921</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1319588073324281858',
    created: 1603449288000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> <a class="mention" href="https://x.com/slonopotamus" rel="noopener noreferrer" target="_blank">@slonopotamus</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> And thank you for your participation. You\'re a major reason why it\'s moving forward so rapidly!<br><br>In reply to: <a href="https://x.com/agoncal/status/1319551092762546177" rel="noopener noreferrer" target="_blank">@agoncal</a> <span class="status">1319551092762546177</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1319587620868001792',
    created: 1603449180000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> <a class="mention" href="https://x.com/slonopotamus" rel="noopener noreferrer" target="_blank">@slonopotamus</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Our friend <a class="mention" href="https://x.com/slonopotamus" rel="noopener noreferrer" target="_blank">@slonopotamus</a> is killing it. 🚀<br><br>In reply to: <a href="https://x.com/agoncal/status/1319551092762546177" rel="noopener noreferrer" target="_blank">@agoncal</a> <span class="status">1319551092762546177</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1319428741404053504',
    created: 1603411300000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <a class="mention" href="https://x.com/mellemcwhirter" rel="noopener noreferrer" target="_blank">@mellemcwhirter</a> I concur.<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/1319427647160655872" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">1319427647160655872</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1319161728823750656',
    created: 1603347639000,
    type: 'quote',
    text: 'I can attest it really did look like that. I could see it from over 70 miles away. The plume dwarfed the height of the Rocky Mountain front range. It resembled a huge explosion.<br><br>Quoting: <a href="https://x.com/GregoryMKuzma/status/1319112478958211077" rel="noopener noreferrer" target="_blank">@GregoryMKuzma</a> <span class="status">1319112478958211077</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1319160953716367360',
    created: 1603347454000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/odeke_et" rel="noopener noreferrer" target="_blank">@odeke_et</a> Amazing! Congrats to all!<br><br>In reply to: <a href="https://x.com/odeke_et/status/1319135684322492418" rel="noopener noreferrer" target="_blank">@odeke_et</a> <span class="status">1319135684322492418</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1319023687463440384',
    created: 1603314728000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dailyburn" rel="noopener noreferrer" target="_blank">@dailyburn</a> Please do! The community is a big part of why we press play every day. We\'re in this together!<br><br>In reply to: <a href="https://x.com/dailyburn/status/1319020711755722753" rel="noopener noreferrer" target="_blank">@dailyburn</a> <span class="status">1319020711755722753</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1318468663834337281',
    created: 1603182400000,
    type: 'post',
    text: 'I should also mention that I can do all routines within a space of about 40-50 square feet (basically, the space between my couch and my TV).',
    likes: 1,
    retweets: 0
  },
  {
    id: '1318467536199938048',
    created: 1603182131000,
    type: 'post',
    text: 'Something I\'ve been doing to stay sane &amp; healthy during the pandemic is exercising with <a class="mention" href="https://x.com/dailyburn" rel="noopener noreferrer" target="_blank">@dailyburn</a>. I was a collegiate athlete, yet I don\'t think I\'ve ever been in better shape. I can\'t say enough good things about the 365 program. It\'s approachable, diversified, &amp; effective.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1318458453505888256',
    created: 1603179965000,
    type: 'post',
    text: 'If you\'re using Travis conditions, I have a little tip. You cannot add modifier flags to the regex, but you can encode them in the expression itself. See <a href="https://github.com/asciidoctor/asciidoctor/blob/master/.travis.yml#L31" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor/blob/master/.travis.yml#L31</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1317973153092689921',
    created: 1603064261000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jessicaewest" rel="noopener noreferrer" target="_blank">@jessicaewest</a> Oh, you\'re going to have so much fun! I think of my trip from last year almost every day. It was such a unique adventure. You really come to appreciate the scale of this country.<br><br>In reply to: <a href="https://x.com/jessicaewest/status/1317766556605063171" rel="noopener noreferrer" target="_blank">@jessicaewest</a> <span class="status">1317766556605063171</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1317000726292369408',
    created: 1602832416000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gegastaldi" rel="noopener noreferrer" target="_blank">@gegastaldi</a> It\'s the one from this cookbook: <a href="https://www.goodreads.com/book/show/20862694-but-i-could-never-go-vegan" rel="noopener noreferrer" target="_blank">www.goodreads.com/book/show/20862694-but-i-could-never-go-vegan</a><br><br>In reply to: <a href="https://x.com/gegastaldi/status/1316914165261500418" rel="noopener noreferrer" target="_blank">@gegastaldi</a> <span class="status">1316914165261500418</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1317000696185704448',
    created: 1602832409000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CGuntur" rel="noopener noreferrer" target="_blank">@CGuntur</a> It\'s the one from this cookbook: <a href="https://www.goodreads.com/book/show/20862694-but-i-could-never-go-vegan" rel="noopener noreferrer" target="_blank">www.goodreads.com/book/show/20862694-but-i-could-never-go-vegan</a><br><br>In reply to: <a href="https://x.com/CGuntur/status/1316904274694230016" rel="noopener noreferrer" target="_blank">@CGuntur</a> <span class="status">1316904274694230016</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1316903314815131648',
    created: 1602809191000,
    type: 'post',
    text: 'Cookin\' up some crabless cakes at 10,000 ft. Yes, you can go vegan and still enjoy the classics.<br><br>Bonus: no shells to choke on.',
    photos: ['<div class="item"><img class="photo" src="media/1316903314815131648.jpg"></div>'],
    likes: 10,
    retweets: 0
  },
  {
    id: '1316818660674031616',
    created: 1602789008000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> I feel you man.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1316817764326146048" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1316817764326146048</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1316673905633837061',
    created: 1602754496000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/hsablonniere" rel="noopener noreferrer" target="_blank">@hsablonniere</a> <a class="mention" href="https://x.com/styx_hcr" rel="noopener noreferrer" target="_blank">@styx_hcr</a> @rgransberger So cool!<br><br>In reply to: <a href="https://x.com/hsablonniere/status/1315909867912212480" rel="noopener noreferrer" target="_blank">@hsablonniere</a> <span class="status">1315909867912212480</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1316673803548721154',
    created: 1602754472000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/styx_hcr" rel="noopener noreferrer" target="_blank">@styx_hcr</a> @rgransberger That\'s exactly what we plan to do for the next round, except we\'ll be using the vegan, soy-based formula. See <a href="https://www.etsy.com/shop/NoTraceShop?section_id=23568436" rel="noopener noreferrer" target="_blank">www.etsy.com/shop/NoTraceShop?section_id=23568436</a><br><br>In reply to: <a href="https://x.com/styx_hcr/status/1315898802776268800" rel="noopener noreferrer" target="_blank">@styx_hcr</a> <span class="status">1315898802776268800</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1316673477231796224',
    created: 1602754394000,
    type: 'reply',
    text: '@rgransberger Absolutely. We use the vegan wraps (soy-based) from NoTraceShop (on Etsy). See <a href="https://www.etsy.com/shop/NoTraceShop?section_id=23568436" rel="noopener noreferrer" target="_blank">www.etsy.com/shop/NoTraceShop?section_id=23568436</a> They also provide a kit to rewax them.<br><br>In reply to: @rgransberger <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1316659338077847554',
    created: 1602751023000,
    type: 'reply',
    text: '@jbryant787 It will interfere, but that\'s just a fact of life atm. Nevertheless, I\'m enjoying the time to the fullest, distractions be damned ;)<br><br>In reply to: @jbryant787 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1316300525604761601',
    created: 1602665475000,
    type: 'post',
    text: 'When I got to the vacation rental, I immediately went outside in the yard without having to wear a mask and took in the deepest breath. Ahhhh. I almost forgot what the outside smells like. It\'s been far too long.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1316138849932697600',
    created: 1602626929000,
    type: 'post',
    text: 'If you think apples are irresistible with peanut butter, I raise you apples with tahini sauce.',
    photos: ['<div class="item"><img class="photo" src="media/1316138849932697600.jpg"></div>'],
    likes: 2,
    retweets: 1
  },
  {
    id: '1315887825095716864',
    created: 1602567080000,
    type: 'post',
    text: 'I\'ve become so eco-conscious that I take my reusable wraps with me on vacation.',
    photos: ['<div class="item"><img class="photo" src="media/1315887825095716864.jpg"></div>'],
    likes: 7,
    retweets: 0
  },
  {
    id: '1315735914107158528',
    created: 1602530861000,
    type: 'post',
    text: 'In the past 24 hours, I\'ve learned that one friend of mine got COVID and another is getting divorced. Monday, please be kind.',
    likes: 5,
    retweets: 0
  },
  {
    id: '1315189897553010688',
    created: 1602400681000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tlberglund" rel="noopener noreferrer" target="_blank">@tlberglund</a> So sorry to hear that, Tim. Tap into that ability of yours to be calm under pressure and you\'ll be on the other side in no time. Your experience is yet another lesson for me to continue taking this pandemic as seriously as possible.<br><br>In reply to: <a href="https://x.com/tlberglund/status/1315075666803474433" rel="noopener noreferrer" target="_blank">@tlberglund</a> <span class="status">1315075666803474433</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1314860154274287616',
    created: 1602322064000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bentolor" rel="noopener noreferrer" target="_blank">@bentolor</a> Since you mentioned Sherlock, have you seen Enola Holmes yet? I really enjoyed it.<br><br>In reply to: <a href="https://x.com/bentolor/status/1314854297381855232" rel="noopener noreferrer" target="_blank">@bentolor</a> <span class="status">1314854297381855232</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1314847822152257537',
    created: 1602319124000,
    type: 'post',
    text: 'I\'m going to be taking a rare and very socially distanced vacation soon. Do you have any movie recommendations (to stream / rent) to help my mind escape?',
    likes: 4,
    retweets: 0
  },
  {
    id: '1314289566426165250',
    created: 1602186025000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jtheWhiteShark" rel="noopener noreferrer" target="_blank">@jtheWhiteShark</a> Exactly. And I\'ll tell you, the notice made your sister extremely stressed not only about herself, but about you and your children...and for the whole family.<br><br>In reply to: <a href="https://x.com/jtheWhiteShark/status/1314284475422044161" rel="noopener noreferrer" target="_blank">@jtheWhiteShark</a> <span class="status">1314284475422044161</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1313922038474522624',
    created: 1602098400000,
    type: 'quote',
    text: 'In the past, I\'ve been able to defeat that immune system, but it requires being stubborn as hell and, more important, planting lots and lots of seeds (read as: grassroots effort).<br><br>Quoting: <a href="https://x.com/jasongorman/status/1313733072068673536" rel="noopener noreferrer" target="_blank">@jasongorman</a> <span class="status">1313733072068673536</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1313893369337335810',
    created: 1602091564000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tmorello" rel="noopener noreferrer" target="_blank">@tmorello</a> ...and Chris Cornell.<br><br>In reply to: <a href="https://x.com/tmorello/status/1313892515863355392" rel="noopener noreferrer" target="_blank">@tmorello</a> <span class="status">1313892515863355392</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1313710554008236032',
    created: 1602047978000,
    type: 'post',
    text: 'Just received notice that my health insurance plan is dropping my healthcare provider. That means I have to find a new plan or healthcare provider...during a pandemic. This is America.',
    likes: 4,
    retweets: 3
  },
  {
    id: '1313345723442753536',
    created: 1601960995000,
    type: 'quote',
    text: 'Exactly.<br><br>Quoting: <a href="https://x.com/JakeAnbinder/status/1313246101525483523" rel="noopener noreferrer" target="_blank">@JakeAnbinder</a> <span class="status">1313246101525483523</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1313268041552072705',
    created: 1601942475000,
    type: 'post',
    text: 'I think having vote-by-mail / ballot dropbox (like Colorado) does more for voter engagement then GOTV efforts. We should still encourage / remind people to vote. But the constant badgering probably has diminishing returns. Nothing beats a solid process. cc: <a class="mention" href="https://x.com/JenaGriswold" rel="noopener noreferrer" target="_blank">@JenaGriswold</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1313042533094547456',
    created: 1601888709000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CatMcGeeCode" rel="noopener noreferrer" target="_blank">@CatMcGeeCode</a> I always do a quick prototype just to see whether what\'s in my head is feasible. I try not to spend to long on it or get attached to how I did it. I just want to see something real. Then I stop and discuss what I learned / discovered. That almost always leads to the next steps.<br><br>In reply to: <a href="https://x.com/CatMcGeeCode/status/1312761169879465984" rel="noopener noreferrer" target="_blank">@CatMcGeeCode</a> <span class="status">1312761169879465984</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1312652959646322688',
    created: 1601795828000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/sierrahull" rel="noopener noreferrer" target="_blank">@sierrahull</a> Congratulate Justin for me for winning a player of the year award. Personally, I think you both should have won for your respective instruments. You\'re a superstar couple! 🎶',
    likes: 0,
    retweets: 0
  },
  {
    id: '1312113446213750786',
    created: 1601667198000,
    type: 'reply',
    text: '@PatTheBerner <a class="mention" href="https://x.com/paulajean2020" rel="noopener noreferrer" target="_blank">@paulajean2020</a> That\'s a great rule.<br><br>In reply to: <a href="https://x.com/PatTheSocialist/status/1312111969252466688" rel="noopener noreferrer" target="_blank">@PatTheSocialist</a> <span class="status">1312111969252466688</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1311850595863523330',
    created: 1601604529000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cortizq" rel="noopener noreferrer" target="_blank">@cortizq</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> More than just a saying, that\'s honestly how we discover ways to make it better. The more I use it, and heavily, the clearer it becomes what changes are needed.<br><br>In reply to: <a href="https://x.com/cortizq/status/1311842920346128384" rel="noopener noreferrer" target="_blank">@cortizq</a> <span class="status">1311842920346128384</span>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1311782516261519366',
    created: 1601588298000,
    type: 'post',
    text: 'OSS is not about scoring points or winning free stuff. It\'s about collaborating on common solutions to shared problems. Be part of the solution (or enjoy your time doing something else that pleases you).',
    likes: 73,
    retweets: 15
  },
  {
    id: '1311764373979885568',
    created: 1601583972000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cortizq" rel="noopener noreferrer" target="_blank">@cortizq</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> And docs for new features are in the works.<br><br>In reply to: <a href="#1311543187266297856">1311543187266297856</a>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1311589889113124867',
    created: 1601542372000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <a class="mention" href="https://x.com/ilopmar" rel="noopener noreferrer" target="_blank">@ilopmar</a> I meant to say satisfaction, but the word sanctification is apropos ;)<br><br>In reply to: <a href="#1311589540163776512">1311589540163776512</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1311589540163776512',
    created: 1601542289000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <a class="mention" href="https://x.com/ilopmar" rel="noopener noreferrer" target="_blank">@ilopmar</a> I\'m definitely not excusing it, or the premise of it. But we should be clear this is very different from someone trying to participate and making missteps. The key is to not give these trolls the sanctification of calling them contributors. They know what they are doing.<br><br>In reply to: <a href="https://x.com/gunnarmorling/status/1311586035994972160" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <span class="status">1311586035994972160</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1311583322783379456',
    created: 1601540806000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ilopmar" rel="noopener noreferrer" target="_blank">@ilopmar</a> I would definitely mark those as spam. Those aren\'t sincere contributions in any way, shape, or form. Let\'s call it what it is.<br><br>In reply to: <a href="https://x.com/ilopmar/status/1311582865545625604" rel="noopener noreferrer" target="_blank">@ilopmar</a> <span class="status">1311582865545625604</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1311583007497576448',
    created: 1601540731000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ilopmar" rel="noopener noreferrer" target="_blank">@ilopmar</a> Wow, that\'s really bad. I wouldn\'t group those people with misguided OSS contributors. Those are straight up trolls. We shouldn\'t mince words. Any human should know better.<br><br>In reply to: <a href="https://x.com/ilopmar/status/1311580925252317184" rel="noopener noreferrer" target="_blank">@ilopmar</a> <span class="status">1311580925252317184</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1311581766075838464',
    created: 1601540435000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mxmkm" rel="noopener noreferrer" target="_blank">@mxmkm</a> <a class="mention" href="https://x.com/vogella" rel="noopener noreferrer" target="_blank">@vogella</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> It does if you use raw HTML. (The AsciiDoc feature isn\'t relevant since GitHub comments don\'t allow AsciiDoc).<br><br>It does work in AsciiDoc files that are stored in a repository on GitHub (like README.adoc).<br><br>In reply to: <a href="https://x.com/mxmkm/status/1311554450583162882" rel="noopener noreferrer" target="_blank">@mxmkm</a> <span class="status">1311554450583162882</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1311543187266297856',
    created: 1601531237000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cortizq" rel="noopener noreferrer" target="_blank">@cortizq</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> The What\'s New page, of course ;) <a href="https://docs.antora.org/antora/3.0/whats-new/" rel="noopener noreferrer" target="_blank">docs.antora.org/antora/3.0/whats-new/</a><br><br>In reply to: <a href="https://x.com/cortizq/status/1311484534630895619" rel="noopener noreferrer" target="_blank">@cortizq</a> <span class="status">1311484534630895619</span>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1311420319895232512',
    created: 1601501943000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/downey" rel="noopener noreferrer" target="_blank">@downey</a> That statement is not consistent with the one made in the Gitter app: "your personal data, and all associated information assets, has been transferred over to the new Controller of your data." That implies it already happened.<br><br>In reply to: @downey <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1311398196220837888',
    created: 1601496669000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> See my last reply.<br><br>In reply to: <a href="https://x.com/agoncal/status/1311398091606679560" rel="noopener noreferrer" target="_blank">@agoncal</a> <span class="status">1311398091606679560</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1311398097226883072',
    created: 1601496645000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> You can find code examples here: <a href="https://github.com/asciidoctor/asciidoctor/blob/master/features/xref.feature" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor/blob/master/features/xref.feature</a><br><br>In reply to: <a href="#1311397524368891904">1311397524368891904</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1311397869677502465',
    created: 1601496591000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/downey" rel="noopener noreferrer" target="_blank">@downey</a> They were already owned by an EU company (GitLab). The issue is that they transferred the data, then told users. That gave users no opportunity to remove their data prior to it being transferred. "Assurances" violates the spirit of GDPR as I understand it (apart from how I feel).<br><br>In reply to: @downey <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1311397524368891904',
    created: 1601496509000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> xref:my.adoc#myref[xrefstyle=basic]<br><br>(to use attributes, you must use the macro variant of the syntax, which you should be using anyway for interdocument xrefs)<br><br>In reply to: <a href="https://x.com/agoncal/status/1311392394605195265" rel="noopener noreferrer" target="_blank">@agoncal</a> <span class="status">1311392394605195265</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1311383798433460224',
    created: 1601493236000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> You can set xrefstyle per xref if you want.<br><br>In reply to: <a href="https://x.com/agoncal/status/1311307452336533504" rel="noopener noreferrer" target="_blank">@agoncal</a> <span class="status">1311307452336533504</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1311383653096632321',
    created: 1601493201000,
    type: 'quote',
    text: 'Well this is a surprise. My European colleagues especially are not happy about this lack of transparency regarding personal data transfer, I can tell you that much.<br><br>Quoting: <a href="https://x.com/gitchat/status/1311292352867299328" rel="noopener noreferrer" target="_blank">@gitchat</a> <span class="status">1311292352867299328</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1311059071391297536',
    created: 1601415815000,
    type: 'reply',
    text: '@jbryant787 Glad to hear it! Those are the best weeks, bar none.<br><br>In reply to: @jbryant787 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1311045760960483328',
    created: 1601412642000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> I\'d say so!<br><br>In reply to: <a href="https://x.com/settermjd/status/1311042155960578051" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1311042155960578051</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1311039116058918912',
    created: 1601411057000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/5docorg" rel="noopener noreferrer" target="_blank">@5docorg</a> It was Windows that wasted my time, not you ;)<br><br>In reply to: <a href="https://x.com/5docorg/status/1311037546709868546" rel="noopener noreferrer" target="_blank">@5docorg</a> <span class="status">1311037546709868546</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1311030251028316160',
    created: 1601408944000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/5docorg" rel="noopener noreferrer" target="_blank">@5docorg</a> Trust me, I hear you. I only wrestle with Windows (in CI) for my community. Otherwise, I wouldn\'t give it the time of day.<br><br>In reply to: <a href="https://x.com/5docorg/status/1311029873297821697" rel="noopener noreferrer" target="_blank">@5docorg</a> <span class="status">1311029873297821697</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1311029082851663872',
    created: 1601408665000,
    type: 'post',
    text: 'Just fought with Windows for the past 24 hours to remove a non-empty directory in process without resorting to sorcery. How did the start of your week go?',
    likes: 3,
    retweets: 0
  },
  {
    id: '1310338994081726465',
    created: 1601244135000,
    type: 'post',
    text: 'On the other hand, Object.assign (which can also be used, albeit more verbose) has only gotten marginally slower.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1310334134766592001',
    created: 1601242977000,
    type: 'post',
    text: 'In case you\'re wondering, Node 14 has the same issues.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1310331856575852544',
    created: 1601242434000,
    type: 'post',
    text: 'It mostly seems to be two things. The object spread operator and array manipulation. But these are so fundamental that they end up impacting a lot of code.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1310328620850671621',
    created: 1601241662000,
    type: 'post',
    text: 'Overall, I\'m not impressed by the changes to Node 12 (mostly coming from V8). Some operations got faster, but a whole lot got measurably slower. Code that I\'ve tested is consistently slower when running on Node 12.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1310327793318653952',
    created: 1601241465000,
    type: 'post',
    text: 'The object spread operator in JavaScript, which is used to copy an object and optionally append properties to it, got an order of magnitude slower in Node 12 (compared to Node 10).',
    likes: 2,
    retweets: 0
  },
  {
    id: '1310153946288193536',
    created: 1601200016000,
    type: 'quote',
    text: 'Same for me, except my turning point was getting fired. I like to think of it as being liberated ;)<br><br>Quoting: <a href="https://x.com/vlad_mihalcea/status/1301366708486189056" rel="noopener noreferrer" target="_blank">@vlad_mihalcea</a> <span class="status">1301366708486189056</span>',
    likes: 6,
    retweets: 0
  },
  {
    id: '1310145473622061056',
    created: 1601197996000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/myfear" rel="noopener noreferrer" target="_blank">@myfear</a> Happy Birthday. Thank you for always being a champion, of people, platforms, and causes. The intersectionality of your work means so much. 🎉',
    likes: 1,
    retweets: 0
  },
  {
    id: '1309584207824302080',
    created: 1601064180000,
    type: 'reply',
    text: '@bthompsonNA This is the modern rendition of that guide: <a href="https://asciidoctor.org/docs/user-manual/" rel="noopener noreferrer" target="_blank">asciidoctor.org/docs/user-manual/</a><br><br>In reply to: <a href="https://x.com/qBTpz/status/1309508708779487232" rel="noopener noreferrer" target="_blank">@qBTpz</a> <span class="status">1309508708779487232</span>',
    likes: 3,
    retweets: 1
  },
  {
    id: '1309387061548388352',
    created: 1601017177000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/gegastaldi" rel="noopener noreferrer" target="_blank">@gegastaldi</a> <a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> I agree that we need to direct the frustration at the system, not at each other. I think the question elicits a sharp response because we know we\'re in the middle of fighting an incredibly strong misinformation campaign, and because people are still getting killed.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1309374931319349248" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1309374931319349248</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1309322795046723584',
    created: 1601001855000,
    type: 'post',
    text: 'Black is beautiful. Black also makes beautiful beer. #BlackLivesMatter <br><br>Thank you Woods Boss Brewing for supporting the cause!',
    photos: ['<div class="item"><img class="photo" src="media/1309322795046723584.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['blacklivesmatter']
  },
  {
    id: '1309315400128016384',
    created: 1601000091000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gegastaldi" rel="noopener noreferrer" target="_blank">@gegastaldi</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> It\'s extremely important to understand the context that the alternative has been tried for decades (&amp; tried heavily in the most recent one) to no avail. If alternate language worked, we\'d be using it. It doesn\'t work. We have to push harder. We have to be louder.<br><br>In reply to: <a href="https://x.com/gegastaldi/status/1309089480054374401" rel="noopener noreferrer" target="_blank">@gegastaldi</a> <span class="status">1309089480054374401</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1309066421842960385',
    created: 1600940730000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/igels" rel="noopener noreferrer" target="_blank">@igels</a> <a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> <a class="mention" href="https://x.com/gegastaldi" rel="noopener noreferrer" target="_blank">@gegastaldi</a> You are defending the actions of a murderer. I can\'t even. Go away.<br><br>In reply to: <a href="https://x.com/igels/status/1308934322376912898" rel="noopener noreferrer" target="_blank">@igels</a> <span class="status">1308934322376912898</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1308923650905526272',
    created: 1600906691000,
    type: 'post',
    text: 'Do you realize how callous you sound when you say "maybe it wasn\'t murder." Hey, when someone is killed and they didn\'t kill themselves, it\'s murder. Stop looking for a deeper explanation.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1308922442052702208',
    created: 1600906403000,
    type: 'post',
    text: 'It\'s so strange to me that when someone in power or privileged commits a crime against humanity, people use whatever twist of logic they can to make it not true. How about asking yourself why you\'re working so hard to defend them? Why must the burden on the victims?',
    likes: 4,
    retweets: 0
  },
  {
    id: '1308917698726973443',
    created: 1600905272000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/igels" rel="noopener noreferrer" target="_blank">@igels</a> <a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> <a class="mention" href="https://x.com/gegastaldi" rel="noopener noreferrer" target="_blank">@gegastaldi</a> And then killed someone who was sleeping. Try again.<br><br>In reply to: <a href="https://x.com/igels/status/1308906641346224128" rel="noopener noreferrer" target="_blank">@igels</a> <span class="status">1308906641346224128</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1308892479647776775',
    created: 1600899259000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> <a class="mention" href="https://x.com/gegastaldi" rel="noopener noreferrer" target="_blank">@gegastaldi</a> Accurate. 🎯<br><br>In reply to: <a href="https://x.com/maxandersen/status/1308883180531965952" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1308883180531965952</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1308880270599356416',
    created: 1600896348000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> Exactly! That\'s the unbundling of services idea, which I really like (and which they are starting to test here in Denver). <a href="https://en.wikipedia.org/wiki/Defund_the_police#Unbundling_of_services" rel="noopener noreferrer" target="_blank">en.wikipedia.org/wiki/Defund_the_police#Unbundling_of_services</a><br><br>In reply to: <a href="https://x.com/marcsavy/status/1308879058709688320" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1308879058709688320</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1308879970048110592',
    created: 1600896277000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> <a class="mention" href="https://x.com/gegastaldi" rel="noopener noreferrer" target="_blank">@gegastaldi</a> I will have a listen. Though I firmly believe it\'s both. The justice system doesn\'t put tanks in the streets, stop and frisk citizens based on skin color, or seize money from citizens and use it to reinforce munitions. The policing system is deeply, deeply corrupt.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1308876400728956931" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1308876400728956931</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1308875344112480256',
    created: 1600895174000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gegastaldi" rel="noopener noreferrer" target="_blank">@gegastaldi</a> <a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> Indeed it is.<br><br>In reply to: <a href="https://x.com/gegastaldi/status/1308875089530757122" rel="noopener noreferrer" target="_blank">@gegastaldi</a> <span class="status">1308875089530757122</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1308866730576445442',
    created: 1600893120000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> ...but that officer cannot shoot a wall.<br><br>In reply to: <a href="#1308866558891102208">1308866558891102208</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1308866558891102208',
    created: 1600893079000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> Today the justice department said (via a verdict) that a police officer can enter a home unannounced and kill a citizen without facing criminal charges. It\'s even worse that the citizen was Black given the history of racial bias in our policing system.<br><br>In reply to: <a href="#1308865804608385028">1308865804608385028</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1308865804608385028',
    created: 1600892900000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> Understood. The situation here is quite extreme &amp; the intent is to deal with it poignantly. There\'s also <a href="https://8cantwait.org/" rel="noopener noreferrer" target="_blank">8cantwait.org/</a> &amp; <a href="https://www.8toabolition.com/" rel="noopener noreferrer" target="_blank">www.8toabolition.com/</a> which outline different approaches. You never get everything you want, so it\'s best to start with the strongest position.<br><br>In reply to: <a href="https://x.com/majson/status/1308864232415465475" rel="noopener noreferrer" target="_blank">@majson</a> <span class="status">1308864232415465475</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1308864149775147019',
    created: 1600892505000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> The police in the US are nothing like police in many other parts of the world. The only way to view police in the US are as an invading army. (Maybe not true in every town, but in enough towns that it doesn\'t matter that there are exceptions).<br><br>In reply to: <a href="#1308863683628539904">1308863683628539904</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1308863683628539904',
    created: 1600892394000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> If we\'re proposing to abolish anything, it\'s the exploding budgets to militarize the police. I happen to believe we can go all the way to replacing the system, but practically I recognize that the only way to get there in the short term is by curtailing budgets.<br><br>In reply to: <a href="#1308863163157409792">1308863163157409792</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1308863163157409792',
    created: 1600892270000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> And did I use the word abolish? It\'s a common distraction technique to replace words in statements, so I would be careful not to fall into it.<br><br>In reply to: <a href="https://x.com/majson/status/1308861986864848898" rel="noopener noreferrer" target="_blank">@majson</a> <span class="status">1308861986864848898</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1308862907980156930',
    created: 1600892209000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> The alternatives are well documented and have already been put into practice successfully in many districts. The most common forms are community-based policing and/or community review boards. You can find a wealth of information here: <a href="https://defundthepolice.org/alternatives-to-police-services/" rel="noopener noreferrer" target="_blank">defundthepolice.org/alternatives-to-police-services/</a><br><br>In reply to: <a href="https://x.com/majson/status/1308861986864848898" rel="noopener noreferrer" target="_blank">@majson</a> <span class="status">1308861986864848898</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1308861810330697729',
    created: 1600891947000,
    type: 'post',
    text: 'And justice implies "for all". There is no other definition of justice.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1308860720126328834',
    created: 1600891687000,
    type: 'post',
    text: 'Defund the police. It\'s a deeply corrupt and racist system that\'s incapable of reform. We must envision a new future that\'s based on justice. #BreonnaTaylor',
    likes: 2,
    retweets: 0,
    tags: ['breonnataylor']
  },
  {
    id: '1308709450623016960',
    created: 1600855622000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Java_Champions" rel="noopener noreferrer" target="_blank">@Java_Champions</a> <a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> Congrats Danno! You have mentored many an aspiring Java developer and the community is larger because of it. Way to be.<br><br>In reply to: <a href="https://x.com/Java_Champions/status/1308525703265234946" rel="noopener noreferrer" target="_blank">@Java_Champions</a> <span class="status">1308525703265234946</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1308661651286011905',
    created: 1600844226000,
    type: 'post',
    text: 'Regardless of version, we\'ve discovered that Asciidoctor.js runs significantly slower beginning with Node 12. This appears to be due to an "optimization" added in V8. It likely impacts all applications transpiled using Opal. We\'re in the process of researching a solution.',
    likes: 6,
    retweets: 3
  },
  {
    id: '1308652625076912129',
    created: 1600842074000,
    type: 'quote',
    text: 'We\'re reminded every four years of just how undemocratic our system is. And the total lack of will to do anything about it.<br><br>Quoting: <a href="https://x.com/WSJ/status/1305565413347123203" rel="noopener noreferrer" target="_blank">@WSJ</a> <span class="status">1305565413347123203</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1308651786954321920',
    created: 1600841874000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/netskymusic" rel="noopener noreferrer" target="_blank">@netskymusic</a> Nice! I love that song. It really sums up what this year has been like. There\'s the pandemic, but also new discoveries, like for me your music.<br><br>In reply to: <a href="https://x.com/netskymusic/status/1308406973134778369" rel="noopener noreferrer" target="_blank">@netskymusic</a> <span class="status">1308406973134778369</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1308599070403878915',
    created: 1600829305000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/karltuxstadt" rel="noopener noreferrer" target="_blank">@karltuxstadt</a> I think I see what you\'re saying. Yes, the first positional attribute is either the service or a local image. The thumbnail image is provided by the service, making it possible to reuse the first position for different things. To me, that makes it intuitive.<br><br>In reply to: <a href="https://x.com/karltuxstadt/status/1308595613433380864" rel="noopener noreferrer" target="_blank">@karltuxstadt</a> <span class="status">1308595613433380864</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1308594882684964865',
    created: 1600828307000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/karltuxstadt" rel="noopener noreferrer" target="_blank">@karltuxstadt</a> If you are talking about Asciidoctor PDF, then it may be related to this fix: <a href="https://github.com/asciidoctor/asciidoctor-pdf/commit/07b581aefbd71904fdad93d676fa1fc7ce3277e5" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-pdf/commit/07b581aefbd71904fdad93d676fa1fc7ce3277e5</a><br><br>In reply to: <a href="https://x.com/karltuxstadt/status/1308189101489758209" rel="noopener noreferrer" target="_blank">@karltuxstadt</a> <span class="status">1308189101489758209</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1308594547358724096',
    created: 1600828227000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Anradez" rel="noopener noreferrer" target="_blank">@Anradez</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> We\'re in the process of preparing the initial contribution. I\'ll be sending an update to the list early next week. In the meantime, you can follow the meeting minutes: <a href="https://www.eclipse.org/lists/asciidoc-wg/msg00199.html" rel="noopener noreferrer" target="_blank">www.eclipse.org/lists/asciidoc-wg/msg00199.html</a><br><br>In reply to: <a href="https://x.com/Anradez/status/1308425200355151873" rel="noopener noreferrer" target="_blank">@Anradez</a> <span class="status">1308425200355151873</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1308593921145012226',
    created: 1600828077000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gegastaldi" rel="noopener noreferrer" target="_blank">@gegastaldi</a> Sweet!<br><br>In reply to: <a href="https://x.com/gegastaldi/status/1308579039465091072" rel="noopener noreferrer" target="_blank">@gegastaldi</a> <span class="status">1308579039465091072</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1308572950212165633',
    created: 1600823078000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gegastaldi" rel="noopener noreferrer" target="_blank">@gegastaldi</a> ...not including friends and family, of course.<br><br>In reply to: <a href="#1308572739779743745">1308572739779743745</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1308572739779743745',
    created: 1600823027000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gegastaldi" rel="noopener noreferrer" target="_blank">@gegastaldi</a> Thanks. Though the problem in the US is that you can no longer tell calls you want (like a local business you contacted) from those you don\'t since almost all call systems use rotating numbers from all over the country. When the phone rings, it could be anyone.<br><br>In reply to: <a href="https://x.com/gegastaldi/status/1308550007772057601" rel="noopener noreferrer" target="_blank">@gegastaldi</a> <span class="status">1308550007772057601</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1308524255357759488',
    created: 1600811468000,
    type: 'post',
    text: 'I said I was blocking calls because I\'m getting spammed with political phone calls, and someone asked "so you don\'t like politics?" That\'s the problem. I find politics extremely important. I just don\'t want to be harassed. And I have no way of controlling who can contact me.',
    likes: 6,
    retweets: 0
  },
  {
    id: '1308503207216214016',
    created: 1600806450000,
    type: 'reply',
    text: '@the_Nizo <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Congrats!<br><br>In reply to: @the_Nizo <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1308205680495656960',
    created: 1600735514000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rustyrazorblade" rel="noopener noreferrer" target="_blank">@rustyrazorblade</a> I\'ll add an example.<br><br>In reply to: @rustyrazorblade <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1308198424672690177',
    created: 1600733784000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rustyrazorblade" rel="noopener noreferrer" target="_blank">@rustyrazorblade</a> How you do it is that you set the url of the content source to "." (dot), which means "here".<br><br>In reply to: <a href="#1308197504790429696">1308197504790429696</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1308197627968851968',
    created: 1600733594000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rustyrazorblade" rel="noopener noreferrer" target="_blank">@rustyrazorblade</a> Btw, this has always been the case, even since Antora 1.0.<br><br>In reply to: <a href="#1308197504790429696">1308197504790429696</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1308197504790429696',
    created: 1600733564000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rustyrazorblade" rel="noopener noreferrer" target="_blank">@rustyrazorblade</a> Yes, it can all be in one repo (playbook, content, and even UI). Several users do it this way. Antora will pull content from wherever you tell it to pull content, whether that is local or remote, one repository or many.<br><br>In reply to: @rustyrazorblade <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1308189510858141696',
    created: 1600731659000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/karltuxstadt" rel="noopener noreferrer" target="_blank">@karltuxstadt</a> Please ask questions in the chat channel at <a href="https://gitter.im/asciidoctor/asciidoctor" rel="noopener noreferrer" target="_blank">gitter.im/asciidoctor/asciidoctor</a>. Thanks!<br><br>In reply to: <a href="https://x.com/karltuxstadt/status/1308189101489758209" rel="noopener noreferrer" target="_blank">@karltuxstadt</a> <span class="status">1308189101489758209</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1308185470350680066',
    created: 1600730695000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rustyrazorblade" rel="noopener noreferrer" target="_blank">@rustyrazorblade</a> <a href="https://docs.antora.org" rel="noopener noreferrer" target="_blank">docs.antora.org</a><br><br>In reply to: @rustyrazorblade <span class="status">&lt;deleted&gt;</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1308151326287699968',
    created: 1600722555000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jponge" rel="noopener noreferrer" target="_blank">@jponge</a> <a class="mention" href="https://x.com/snicoll" rel="noopener noreferrer" target="_blank">@snicoll</a> I would add Reinier Zonneveld to that list. Between the two of them, something incredible is happening.<br><br>In reply to: <a href="https://x.com/jponge/status/1308128093467488257" rel="noopener noreferrer" target="_blank">@jponge</a> <span class="status">1308128093467488257</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1308127799341969412',
    created: 1600716945000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jponge" rel="noopener noreferrer" target="_blank">@jponge</a> <a class="mention" href="https://x.com/snicoll" rel="noopener noreferrer" target="_blank">@snicoll</a> I listened to it as well. Solid stuff.<br><br>In reply to: <a href="https://x.com/jponge/status/1308125592320180230" rel="noopener noreferrer" target="_blank">@jponge</a> <span class="status">1308125592320180230</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1308124776083476480',
    created: 1600716225000,
    type: 'reply',
    text: '@the_Nizo <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Add an AsciiDoc role to the block, then add CSS to a class selector with the same name either in your own stylesheet or in CSS added via docinfo. You should not be using AsciiDoc syntax to force layouts as that\'s a presentation concern.<br><br>In reply to: @the_Nizo <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1307819947607822336',
    created: 1600643548000,
    type: 'reply',
    text: '@patrickbkoetter @drblablablabla <a class="mention" href="https://x.com/JWildeboer" rel="noopener noreferrer" target="_blank">@JWildeboer</a> AsciiDoc was not created by O\'Reilly (they were just an early adopter). It was created by Stuart Rackham. The Asciidoctor community took over stewardship at about the halfway point in its history. And now we\'re working to make it an formal specification.<br><br>In reply to: @patrickbkoetter <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1307818517534707712',
    created: 1600643207000,
    type: 'reply',
    text: '@the_Nizo <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Or you could use CSS ;)<br><br>In reply to: @the_Nizo <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1307408084780953607',
    created: 1600545352000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CoriBush" rel="noopener noreferrer" target="_blank">@CoriBush</a> 💯 I so wish my school had this when I attended. It would have brought me joy, given me an appreciation for where our food comes from (and the planning that goes into it), and taught me important lessons to become a lifelong gardener.<br><br>In reply to: <a href="https://x.com/CoriBush/status/1305643741324161024" rel="noopener noreferrer" target="_blank">@CoriBush</a> <span class="status">1305643741324161024</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1306702780866809857',
    created: 1600377194000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DuncanLock" rel="noopener noreferrer" target="_blank">@DuncanLock</a> <a class="mention" href="https://x.com/opencollect" rel="noopener noreferrer" target="_blank">@opencollect</a> Thanks Duncan!<br><br>In reply to: <a href="https://x.com/DuncanLock/status/1306461134938013696" rel="noopener noreferrer" target="_blank">@DuncanLock</a> <span class="status">1306461134938013696</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1306510881086296070',
    created: 1600331442000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobbytank42" rel="noopener noreferrer" target="_blank">@bobbytank42</a> <a class="mention" href="https://x.com/ktosopl" rel="noopener noreferrer" target="_blank">@ktosopl</a> That\'s cool! One thing that seems to help is to get to know business owners. They\'re much more willing to honor requests, such as filling or exchanging containers. This is important because it sends a message. It encourages them to change their business to work that way.<br><br>In reply to: <a href="https://x.com/bobbytank42/status/1306509253528748032" rel="noopener noreferrer" target="_blank">@bobbytank42</a> <span class="status">1306509253528748032</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1306503631034753027',
    created: 1600329713000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ktosopl" rel="noopener noreferrer" target="_blank">@ktosopl</a> I agree it\'s tough right now. The pandemic set me back in some of my practices (like buying bulk foods from bins). On the other hand, I\'ve discovered a lot of new brands that are focused on going plastic free. And I\'m cooking at home exclusively, so no takeaway containers.<br><br>In reply to: <a href="https://x.com/ktosopl/status/1306445071319924736" rel="noopener noreferrer" target="_blank">@ktosopl</a> <span class="status">1306445071319924736</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1306502571285032960',
    created: 1600329461000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DuncanLock" rel="noopener noreferrer" target="_blank">@DuncanLock</a> It is moving forward. The best way to follow is to join the asciidoc-wg list, where we\'re in the process of preparing the initial contribution. <a href="https://accounts.eclipse.org/mailing-list/asciidoc-wg" rel="noopener noreferrer" target="_blank">accounts.eclipse.org/mailing-list/asciidoc-wg</a><br><br>In reply to: <a href="https://x.com/DuncanLock/status/1306463815500836864" rel="noopener noreferrer" target="_blank">@DuncanLock</a> <span class="status">1306463815500836864</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1306401514794098688',
    created: 1600305367000,
    type: 'post',
    text: 'I find the easiest way to do this, personally, is to use reusable bags and containers. You know they\'re going to try to push single-use plastic on you. So be prepared. Tell them to fill rather than pack.',
    likes: 6,
    retweets: 0
  },
  {
    id: '1306395211690438656',
    created: 1600303864000,
    type: 'post',
    text: 'If you care about your environment, you should be drastically reducing your own plastic consumption (esp single-use plastic) and pushing businesses &amp; gov\'t to eliminate it wholesale (likely more effective). Let\'s work together on this to make this planet healthier.',
    likes: 17,
    retweets: 3
  },
  {
    id: '1305824010245087232',
    created: 1600167679000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <a class="mention" href="https://x.com/vogella" rel="noopener noreferrer" target="_blank">@vogella</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Confirming. <a href="https://asciidoctor.org/docs/user-manual/#_footnotedef_1" rel="noopener noreferrer" target="_blank">asciidoctor.org/docs/user-manual/#_footnotedef_1</a><br><br>In reply to: <a href="https://x.com/odrotbohm/status/1305822773487558656" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <span class="status">1305822773487558656</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1305571551836663808',
    created: 1600107488000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sebi2706" rel="noopener noreferrer" target="_blank">@sebi2706</a> 💯<br><br>In reply to: <a href="https://x.com/sebi2706/status/1305570571174981633" rel="noopener noreferrer" target="_blank">@sebi2706</a> <span class="status">1305570571174981633</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1305571259762139136',
    created: 1600107419000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gregturn" rel="noopener noreferrer" target="_blank">@gregturn</a> <a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> I do have good news to share with you on this front. Marat is working on a replacement library for kindlegen that won\'t require any external tools. It\'s still rough, but early results are promising.<br><br>In reply to: <a href="https://x.com/gregturn/status/1305485037555638275" rel="noopener noreferrer" target="_blank">@gregturn</a> <span class="status">1305485037555638275</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1305456446398705670',
    created: 1600080045000,
    type: 'post',
    text: 'This pandemic has been very humbling for our society. However great we think we are, and however far we think we\'ve come, we can\'t beat a pandemic. (Nor a climate in crisis for that matter).',
    likes: 7,
    retweets: 1
  },
  {
    id: '1304886476551761921',
    created: 1599944154000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/schneidro" rel="noopener noreferrer" target="_blank">@schneidro</a> +1 And it\'s even more upsetting that it CAN be solved.<br><br>In reply to: <a href="https://x.com/schneidro/status/1304797547840503808" rel="noopener noreferrer" target="_blank">@schneidro</a> <span class="status">1304797547840503808</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1304676113440858112',
    created: 1599893999000,
    type: 'post',
    text: 'It really is absolutely stunning how much this society has betrayed us. It\'s unforgivable and unforgettable.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1304675859584802817',
    created: 1599893939000,
    type: 'quote',
    text: 'A key reason why I am not only vegan, but trying hard to eliminate plastic from my purchasing. I work with the mental model that everything that gets taken away is headed to a landfill.<br><br>Quoting: <a href="https://x.com/cra/status/1304569731651903496" rel="noopener noreferrer" target="_blank">@cra</a> <span class="status">1304569731651903496</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1304566891357368321',
    created: 1599867959000,
    type: 'post',
    text: '"Chance to win" is a con (except in games). If you did work hoping to win a payoff (which you almost never will), you were conned into doing work for free.',
    likes: 2,
    retweets: 1
  },
  {
    id: '1304184366587719680',
    created: 1599776758000,
    type: 'post',
    text: 'Since starting to listen to techno (and other deep styles), I find it harder to enjoy pop tracks. It alters your mind somehow. Reminds me a lot of the effect functional programming has on the mind as it relates to object-oriented programming.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1304181602738565120',
    created: 1599776099000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kennybastani" rel="noopener noreferrer" target="_blank">@kennybastani</a> And I do hate when they immediately blame the other party. I\'m behind you on that. It\'s trite, useless, and divisive (even if I myself dislike one or both parties).<br><br>In reply to: <a href="#1304180736962953216">1304180736962953216</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1304181177079664640',
    created: 1599775997000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kennybastani" rel="noopener noreferrer" target="_blank">@kennybastani</a> I firmly believe at this point that the corporate politicians are looking around thinking to themselves, "mission accomplished." They are destroying this world purposefully. They are already working together.<br><br>In reply to: <a href="#1304180736962953216">1304180736962953216</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1304180736962953216',
    created: 1599775892000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kennybastani" rel="noopener noreferrer" target="_blank">@kennybastani</a> No doubt. I wish politicians would stand up for core (so called "American") values, regardless of approach. The problem is, that\'s wishful thinking. All we have are (largely) corporate politicians who have no values. No amount of shaming them will fix it. They\'ve proven that.<br><br>In reply to: <a href="https://x.com/kennybastani/status/1304179619264974850" rel="noopener noreferrer" target="_blank">@kennybastani</a> <span class="status">1304179619264974850</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1304177942176477188',
    created: 1599775226000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kennybastani" rel="noopener noreferrer" target="_blank">@kennybastani</a> The problem is, that common ground is large reason why a lot of this is happening right now. (Not that I disagree with your point, but that there\'s a deeper point there too).<br><br>In reply to: <a href="https://x.com/kennybastani/status/1304177227999911937" rel="noopener noreferrer" target="_blank">@kennybastani</a> <span class="status">1304177227999911937</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1304177244558876672',
    created: 1599775060000,
    type: 'quote',
    text: 'I grew up right around there and I can tell you, I never remember that road flooding.<br><br>Quoting: <a href="https://x.com/DildineWTOP/status/1304153036164608000" rel="noopener noreferrer" target="_blank">@DildineWTOP</a> <span class="status">1304153036164608000</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1304146427833016321',
    created: 1599767712000,
    type: 'quote',
    text: 'The kids are all right.<br><br>Quoting: <a href="https://x.com/sunrisemvmt/status/1304144076602773510" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a> <span class="status">1304144076602773510</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1304131715300864000',
    created: 1599764205000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/Blessed_Madonna" rel="noopener noreferrer" target="_blank">@Blessed_Madonna</a> You just got yourself a new fan. I love your music and your activism. Your set at United We Stream was on point. 👏',
    likes: 0,
    retweets: 0
  },
  {
    id: '1303954253921222659',
    created: 1599721895000,
    type: 'post',
    text: '“The world has enough for everyone\'s need, but not enough for everyone\'s greed.” — Mahatma Gandhi',
    likes: 13,
    retweets: 4
  },
  {
    id: '1303771835092279307',
    created: 1599678403000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/vilojona" rel="noopener noreferrer" target="_blank">@vilojona</a> <a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> I second that! 🏆<br><br>In reply to: <a href="https://x.com/vilojona/status/1303386568212852737" rel="noopener noreferrer" target="_blank">@vilojona</a> <span class="status">1303386568212852737</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1303555726602768384',
    created: 1599626878000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bentolor" rel="noopener noreferrer" target="_blank">@bentolor</a> That explains everything ;) (I rarely watch US shows and often ones in a language I don\'t speak). I\'ll take this as a great sign then 👍<br><br>In reply to: <a href="https://x.com/bentolor/status/1303516889780490247" rel="noopener noreferrer" target="_blank">@bentolor</a> <span class="status">1303516889780490247</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1303509715675037696',
    created: 1599615908000,
    type: 'post',
    text: 'Netflix: Only 180,000 ₫ to keep the whole family entertained.<br>me: 😕 Wrong country. Check your table.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1303492085614440449',
    created: 1599611705000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/hsablonniere" rel="noopener noreferrer" target="_blank">@hsablonniere</a><br><br>In reply to: <a href="#1303491822937763840">1303491822937763840</a>',
    photos: ['<div class="item"><img class="photo" src="media/1303492085614440449.jpg"></div>'],
    likes: 1,
    retweets: 0
  },
  {
    id: '1303491822937763840',
    created: 1599611642000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/hsablonniere" rel="noopener noreferrer" target="_blank">@hsablonniere</a><br><br>In reply to: <a href="https://x.com/hsablonniere/status/1302918782831206401" rel="noopener noreferrer" target="_blank">@hsablonniere</a> <span class="status">1302918782831206401</span>',
    photos: ['<div class="item"><img class="photo" src="media/1303491822937763840.jpg"></div>'],
    likes: 3,
    retweets: 0
  },
  {
    id: '1303487325695913984',
    created: 1599610570000,
    type: 'post',
    text: 'Some days I don\'t think we\'re getting anywhere with making the tech industry more inclusive. It\'s just the same old bullshit. The same groups are mistreated or excluded. The typical people are excused.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1303165656108421121',
    created: 1599533878000,
    type: 'post',
    text: 'Spent Labor Day trying to save my outdoor plants from the impending doom of the incoming artic blast. 🌬️🤞',
    likes: 1,
    retweets: 0
  },
  {
    id: '1302861966801682433',
    created: 1599461473000,
    type: 'post',
    text: 'Our apartment garden just yielded 3 cups of basil leaves (without losing any plants). We\'re finally getting the hang of growing plants instead of gently killing them. 🌿',
    likes: 6,
    retweets: 0
  },
  {
    id: '1302767150512263169',
    created: 1599438867000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/jbangdev" rel="noopener noreferrer" target="_blank">@jbangdev</a> <a class="mention" href="https://x.com/5esse" rel="noopener noreferrer" target="_blank">@5esse</a> I\'ve always thought it was a poor choice to only install a JRE. Java is not just the JRE and to try to pretend so is naive.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1302699343699468289" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1302699343699468289</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1302761293393215490',
    created: 1599437471000,
    type: 'post',
    text: 'Time means nothing when I\'m trying to solve a hard problem.',
    likes: 7,
    retweets: 0
  },
  {
    id: '1302495547241066503',
    created: 1599374112000,
    type: 'post',
    text: 'My advice: avoid highly-engineered meat substitutes marketed as vegan. You don\'t need them. Keep it simple and make it yourself. Onions, potatoes, carrots, beans, rice, jackfruit, etc provide all the foundation you need. Then explore the millions of ways to spice it up.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1302494559218225152',
    created: 1599373876000,
    type: 'post',
    text: 'Eating vegan is not the converse of eating delicious food. In fact, it\'s quite the opposite. Vegan food is the best food I\'ve ever tasted, time and again. There\'s something about the simple, fresh ingredients that grab your taste buds and hold on.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1302492250727829504',
    created: 1599373326000,
    type: 'reply',
    text: '@jbryant787 What\'s worse is that even if I didn\'t feel well, my parents didn\'t believe me anyway. So basically, I never stayed home no matter what.<br><br>In reply to: @jbryant787 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1302491869708865536',
    created: 1599373235000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/connolly_s" rel="noopener noreferrer" target="_blank">@connolly_s</a> Every single day I imagine how nice it would be to know exactly that. Until then, I just stay home and focus on things I\'ve needed to get to for a long time anyway but never had the chance.<br><br>In reply to: <a href="https://x.com/connolly_s/status/1302397564390604801" rel="noopener noreferrer" target="_blank">@connolly_s</a> <span class="status">1302397564390604801</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1302396625084579840',
    created: 1599350527000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/connolly_s" rel="noopener noreferrer" target="_blank">@connolly_s</a> I certainly get that, though that\'s very much next level. First you have to think about anyone else / society before you actually think about them properly ;) What we were taught was not caring about spreading.<br><br>In reply to: <a href="https://x.com/connolly_s/status/1302395389610528768" rel="noopener noreferrer" target="_blank">@connolly_s</a> <span class="status">1302395389610528768</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1302393938037227520',
    created: 1599349886000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DHinojosa" rel="noopener noreferrer" target="_blank">@DHinojosa</a> <a class="mention" href="https://x.com/venkat_s" rel="noopener noreferrer" target="_blank">@venkat_s</a> <a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> I\'m so glad I\'m not being blamed here (even though I\'m the one from the tumultuous East Coast).<br><br>In reply to: @dhinojosa <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1302393509526159361',
    created: 1599349784000,
    type: 'post',
    text: 'The way I was taught about being sick (which I\'m not, currently) was all wrong. It was about whether I felt well enough to go to school / work. Not about whether I would make other people sick. That\'s how it should have been.',
    likes: 18,
    retweets: 2
  },
  {
    id: '1302387268011741185',
    created: 1599348296000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mesirii" rel="noopener noreferrer" target="_blank">@mesirii</a> <a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> OMG. This.<br><br>In reply to: <a href="https://x.com/mesirii/status/1302379794496139265" rel="noopener noreferrer" target="_blank">@mesirii</a> <span class="status">1302379794496139265</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1302379802242904064',
    created: 1599346516000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> Boot, of course ;)<br><br>In reply to: <a href="https://x.com/starbuxman/status/1302378725112123392" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">1302378725112123392</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1302157461999116288',
    created: 1599293506000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fanf42" rel="noopener noreferrer" target="_blank">@fanf42</a> Yeah, not looking forward to it. Just as my tomato plant gets busy fruiting.<br><br>In reply to: <a href="https://x.com/fanf42/status/1302149184913637376" rel="noopener noreferrer" target="_blank">@fanf42</a> <span class="status">1302149184913637376</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1302128580424417280',
    created: 1599286620000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/vlad_mihalcea" rel="noopener noreferrer" target="_blank">@vlad_mihalcea</a> I try really hard to do this in my own kitchen as well.<br><br>In reply to: <a href="https://x.com/vlad_mihalcea/status/1302106419177586689" rel="noopener noreferrer" target="_blank">@vlad_mihalcea</a> <span class="status">1302106419177586689</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1302062371624476672',
    created: 1599270835000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sierrahull" rel="noopener noreferrer" target="_blank">@sierrahull</a> Was just thinking that as I sit here typing in the dark.<br><br>In reply to: <a href="https://x.com/sierrahull/status/1302057094963769344" rel="noopener noreferrer" target="_blank">@sierrahull</a> <span class="status">1302057094963769344</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1302061566309744641',
    created: 1599270643000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/topicmeister" rel="noopener noreferrer" target="_blank">@topicmeister</a> Always giving it our all for the docs community!<br><br>In reply to: <a href="https://x.com/topicmeister/status/1302059067444166657" rel="noopener noreferrer" target="_blank">@topicmeister</a> <span class="status">1302059067444166657</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1302046400696311808',
    created: 1599267027000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/topicmeister" rel="noopener noreferrer" target="_blank">@topicmeister</a> To back that up, every client who we\'ve worked with or helped in some way has a search completely unique to them.<br><br>In reply to: <a href="#1302046149730131968">1302046149730131968</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1302046149730131968',
    created: 1599266967000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/topicmeister" rel="noopener noreferrer" target="_blank">@topicmeister</a> There will likely be an official extension to add an integrated search like lunr. But search will always be highly personal and thus something that\'s a starting point, but likely to be tailored per user. That\'s just the nature of search.<br><br>In reply to: <a href="https://x.com/topicmeister/status/1302042797818761216" rel="noopener noreferrer" target="_blank">@topicmeister</a> <span class="status">1302042797818761216</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1302045523604508674',
    created: 1599266818000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/connolly_s" rel="noopener noreferrer" target="_blank">@connolly_s</a> <a class="mention" href="https://x.com/TheASF" rel="noopener noreferrer" target="_blank">@TheASF</a> One of many such instances in recent months.<br><br>In reply to: <a href="https://x.com/connolly_s/status/1302020508565876737" rel="noopener noreferrer" target="_blank">@connolly_s</a> <span class="status">1302020508565876737</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1301792621241614336',
    created: 1599206521000,
    type: 'post',
    text: 'GitHub really needs to provide an option to prevent any user from mentioning (and thus notifying) a group that includes thousands of people. It\'s a bad situation.',
    likes: 12,
    retweets: 3
  },
  {
    id: '1301780452911374336',
    created: 1599203620000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cortizq" rel="noopener noreferrer" target="_blank">@cortizq</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> Trust me, you will know as soon as there is code. Right now it\'s just scratches in my code editor. But I can see the promise in it.<br><br>In reply to: <a href="https://x.com/cortizq/status/1301734053226393605" rel="noopener noreferrer" target="_blank">@cortizq</a> <span class="status">1301734053226393605</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1301679295174070273',
    created: 1599179502000,
    type: 'post',
    text: 'I\'m beginning to explore generating PDFs from an Antora-based site driven by the site navigation. It\'s a lot more complicated than you might think, but doable. Initial (but super rough) prototypes are showing promise.',
    likes: 21,
    retweets: 2
  },
  {
    id: '1301302740967026688',
    created: 1599089725000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/wimdeblauwe" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> <a class="mention" href="https://x.com/geoaxis" rel="noopener noreferrer" target="_blank">@geoaxis</a> <a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> Technically distributing kindlegen that way violates the EULA. (This is a prime example of why proprietary software should be avoided.)<br><br>In reply to: <a href="https://x.com/wimdeblauwe/status/1301257512155578368" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> <span class="status">1301257512155578368</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1301087084262748162',
    created: 1599038308000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Exactly.<br><br>In reply to: <a href="https://x.com/settermjd/status/1301085274437550080" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1301085274437550080</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1301080440799805440',
    created: 1599036724000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Solid strategy! (That\'s one of the key reasons we continue investing in that DocBook converter).<br><br>In reply to: <a href="https://x.com/settermjd/status/1301079871708422145" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1301079871708422145</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1301079644381167616',
    created: 1599036534000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/phoebe_bridgers" rel="noopener noreferrer" target="_blank">@phoebe_bridgers</a> Thank you for that moving performance tonight at #redrocks. It may have looked empty to you, but you were reaching our souls. For my partner and I, that performance was our introduction to you and your music and we thoroughly enjoyed it. Instant fans.',
    likes: 0,
    retweets: 0,
    tags: ['redrocks']
  },
  {
    id: '1301069029063323649',
    created: 1599034003000,
    type: 'quote',
    text: 'The Green New Deal Maker. ✌️<br><br>Quoting: <a href="https://x.com/NPR/status/1300987801052368897" rel="noopener noreferrer" target="_blank">@NPR</a> <span class="status">1300987801052368897</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1300672909715750912',
    created: 1598939561000,
    type: 'quote',
    text: '#JusticeForBreonnaTaylor<br><br>Quoting: <a href="https://x.com/espn/status/1300612317101649921" rel="noopener noreferrer" target="_blank">@espn</a> <span class="status">1300612317101649921</span>',
    likes: 0,
    retweets: 0,
    tags: ['justiceforbreonnataylor']
  },
  {
    id: '1300628247755894785',
    created: 1598928913000,
    type: 'post',
    text: 'OH: "We all have COVID-19 mentally."',
    likes: 1,
    retweets: 1
  },
  {
    id: '1300593156128272384',
    created: 1598920546000,
    type: 'post',
    text: 'By removing KindleGen, Amazon has royally pissed off independent authors and publishers.',
    likes: 4,
    retweets: 2
  },
  {
    id: '1300556822051389441',
    created: 1598911884000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agentdero" rel="noopener noreferrer" target="_blank">@agentdero</a> <a class="mention" href="https://x.com/TheASF" rel="noopener noreferrer" target="_blank">@TheASF</a> My position is that there is no defense. They need to change the name or admit that they simply don\'t respect the tribe or their wishes. The use of the feather, in particular, is egregious.<br><br>In reply to: <a href="https://x.com/agentdero/status/1300548285141139456" rel="noopener noreferrer" target="_blank">@agentdero</a> <span class="status">1300548285141139456</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1300555988894756864',
    created: 1598911685000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> I\'d add to that "...behind closed doors."<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1300555762565931008" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1300555762565931008</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1300524556520624128',
    created: 1598904191000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rjurney" rel="noopener noreferrer" target="_blank">@rjurney</a> It\'s better if you use the de-facto standard extension .adoc. But otherwise, yes indeed!<br><br>In reply to: <a href="https://x.com/rjurney/status/1300192548409888769" rel="noopener noreferrer" target="_blank">@rjurney</a> <span class="status">1300192548409888769</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1300135007369850880',
    created: 1598811315000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> 🤣<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/1300060618981085185" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">1300060618981085185</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1300034639667499016',
    created: 1598787386000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/chakathegod" rel="noopener noreferrer" target="_blank">@chakathegod</a> <a class="mention" href="https://x.com/AmelieLens" rel="noopener noreferrer" target="_blank">@AmelieLens</a> <a class="mention" href="https://x.com/CharlottedWitte" rel="noopener noreferrer" target="_blank">@CharlottedWitte</a> <a class="mention" href="https://x.com/esangiuliano" rel="noopener noreferrer" target="_blank">@esangiuliano</a> <a class="mention" href="https://x.com/realAdamBeyer" rel="noopener noreferrer" target="_blank">@realAdamBeyer</a> <a class="mention" href="https://x.com/BB_BORISBREJCHA" rel="noopener noreferrer" target="_blank">@BB_BORISBREJCHA</a> <a class="mention" href="https://x.com/djannamiranda" rel="noopener noreferrer" target="_blank">@djannamiranda</a> <a class="mention" href="https://x.com/TaleOfUs" rel="noopener noreferrer" target="_blank">@TaleOfUs</a> I\'ve checked out all of these, then gone even further. In fact, it\'s a deep rabbit hole and I\'m expanding my mind exploring all of the tunnels.<br><br>In reply to: <a href="https://x.com/chakathegod/status/1288610772793270272" rel="noopener noreferrer" target="_blank">@chakathegod</a> <span class="status">1288610772793270272</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1300003546058747904',
    created: 1598779972000,
    type: 'post',
    text: 'CI is like that really tough teacher who keeps making you do it over until you get it right.',
    likes: 8,
    retweets: 0
  },
  {
    id: '1299635747746504704',
    created: 1598692282000,
    type: 'post',
    text: 'Passed by the capitol in Denver today while running errands. Looks like war-torn area. A totally different place than when I was there for the climate strikes a year ago. Just reminds me of how serious this has all become. It\'s really sad the state has destroyed the peace.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1299264644733784064',
    created: 1598603805000,
    type: 'post',
    text: 'To be clear, this is the America I expect.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1299224617047875584',
    created: 1598594261000,
    type: 'quote',
    text: 'This is a glimpse into the America I thought I knew, one where the game wouldn\'t go on if someone was harmed. This is how you live up to the name "America\'s game." Solidarity. #BlackLivesMatter<br><br>Quoting: <a href="https://x.com/ComplexSports/status/1299124311878438912" rel="noopener noreferrer" target="_blank">@ComplexSports</a> <span class="status">1299124311878438912</span>',
    likes: 4,
    retweets: 0,
    tags: ['blacklivesmatter']
  },
  {
    id: '1299219104952270848',
    created: 1598592947000,
    type: 'post',
    text: 'Every time I update Twitter, the font size gets smaller (with no way to change it). I swear it\'s trying to make me think I\'m going blind.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1299152234844880897',
    created: 1598577004000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexbdebrie" rel="noopener noreferrer" target="_blank">@alexbdebrie</a> Best to ask in the chat channel at <a href="https://gitter.im/asciidoctor/asciidoctor" rel="noopener noreferrer" target="_blank">gitter.im/asciidoctor/asciidoctor</a>.<br><br>In reply to: <a href="https://x.com/alexbdebrie/status/1299027323862253570" rel="noopener noreferrer" target="_blank">@alexbdebrie</a> <span class="status">1299027323862253570</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1299151965226545152',
    created: 1598576940000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ufomelkor" rel="noopener noreferrer" target="_blank">@ufomelkor</a> <a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> Asciidoctor.js is generated from Asciidoctor Ruby, and nearly all incompatibilities between the two (due to language differences) have been closed.<br><br>In reply to: <a href="https://x.com/ufomelkor/status/1299079325610844174" rel="noopener noreferrer" target="_blank">@ufomelkor</a> <span class="status">1299079325610844174</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1299143199290617856',
    created: 1598574850000,
    type: 'post',
    text: 'Just submitted a colossal pull request. With that, I\'m going to call today a win. Good things coming in <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a>. <br><br>See <a href="https://gitlab.com/antora/antora/-/merge_requests/552" rel="noopener noreferrer" target="_blank">gitlab.com/antora/antora/-/merge_requests/552</a>.',
    likes: 13,
    retweets: 0
  },
  {
    id: '1299050465460592640',
    created: 1598552740000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> <a class="mention" href="https://x.com/jaredmorgs" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <a class="mention" href="https://x.com/intellijidea" rel="noopener noreferrer" target="_blank">@intellijidea</a> <a class="mention" href="https://x.com/code" rel="noopener noreferrer" target="_blank">@code</a> <a class="mention" href="https://x.com/ahus1de" rel="noopener noreferrer" target="_blank">@ahus1de</a> Yes indeed!<br><br>In reply to: <a href="https://x.com/settermjd/status/1298963669527736321" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1298963669527736321</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1299050306748051458',
    created: 1598552702000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dejcup" rel="noopener noreferrer" target="_blank">@dejcup</a> Absolutely. Couldn\'t agree more.<br><br>In reply to: <a href="https://x.com/dejcup/status/1298955156059557892" rel="noopener noreferrer" target="_blank">@dejcup</a> <span class="status">1298955156059557892</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1298900703528665089',
    created: 1598517034000,
    type: 'post',
    text: 'They also challenge the teacher, which makes it so much fun.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1298900441313361922',
    created: 1598516972000,
    type: 'quote',
    text: 'These are my favorite students. What matters is that they want to know why, not that they don\'t know. If they knew, they wouldn\'t need to be students. I love starting them down the paths that lead them to the why. The most skeptical students often turn out to be the best ones.<br><br>Quoting: <a href="https://x.com/aIeturner/status/1298372968838508546" rel="noopener noreferrer" target="_blank">@aIeturner</a> <span class="status">1298372968838508546</span>',
    likes: 11,
    retweets: 1
  },
  {
    id: '1298898480958603264',
    created: 1598516504000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> <a class="mention" href="https://x.com/jaredmorgs" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <a class="mention" href="https://x.com/intellijidea" rel="noopener noreferrer" target="_blank">@intellijidea</a> <a class="mention" href="https://x.com/code" rel="noopener noreferrer" target="_blank">@code</a> I have no doubt you\'ll discover this distinction for yourself. It takes a lot of effort to do what <a class="mention" href="https://x.com/ahus1de" rel="noopener noreferrer" target="_blank">@ahus1de</a> and team have done and I really appreciate their hard work and dedication. Hopefully, it\'s infectious (in the good way).<br><br>In reply to: <a href="#1298898113592090625">1298898113592090625</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1298898113592090625',
    created: 1598516417000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> <a class="mention" href="https://x.com/jaredmorgs" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <a class="mention" href="https://x.com/intellijidea" rel="noopener noreferrer" target="_blank">@intellijidea</a> <a class="mention" href="https://x.com/code" rel="noopener noreferrer" target="_blank">@code</a> The plugin for Code is a very important plugin to the ecosystem, but it would be unfair to say it comes anywhere close to the IntelliJ plugin. The reason the IntelliJ plugin is so strong is because it provides a dedicated parser for AsciiDoc, which opens a lot of doors.<br><br>In reply to: <a href="https://x.com/settermjd/status/1298895708288946176" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1298895708288946176</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1298751494984855552',
    created: 1598481460000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ameliaeiras" rel="noopener noreferrer" target="_blank">@ameliaeiras</a> <a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> I reposted. But it\'s as good as any opportunity just to say thanks!<br><br>In reply to: <a href="https://x.com/ameliaeiras/status/1298742534697492480" rel="noopener noreferrer" target="_blank">@ameliaeiras</a> <span class="status">1298742534697492480</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1298726352665735169',
    created: 1598475466000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> <a class="mention" href="https://x.com/ameliaeiras" rel="noopener noreferrer" target="_blank">@ameliaeiras</a> Autocomplete fail. Though really not a fail, because <a class="mention" href="https://x.com/ameliaeiras" rel="noopener noreferrer" target="_blank">@ameliaeiras</a> always deserves more praise ;)<br><br>In reply to: <a href="https://x.com/majson/status/1298725880345137152" rel="noopener noreferrer" target="_blank">@majson</a> <span class="status">1298725880345137152</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1298726243324399616',
    created: 1598475440000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/AmelieLens" rel="noopener noreferrer" target="_blank">@AmelieLens</a> Congratulations on the Higher EP release and breaking into the top 10. Also, a huge thank you for that set from Antwerp. My spouse and I both thoroughly enjoyed it. You absolutely made our night. 🎧',
    likes: 1,
    retweets: 0
  },
  {
    id: '1298722067374682115',
    created: 1598474444000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jponge" rel="noopener noreferrer" target="_blank">@jponge</a> <a class="mention" href="https://x.com/AmelieLens" rel="noopener noreferrer" target="_blank">@AmelieLens</a> <a class="mention" href="https://x.com/beatport" rel="noopener noreferrer" target="_blank">@beatport</a> <a class="mention" href="https://x.com/YouTube" rel="noopener noreferrer" target="_blank">@YouTube</a> Totally agree. Just listened to it last night. Pure energy.<br><br>In reply to: <a href="https://x.com/jponge/status/1298641967375671296" rel="noopener noreferrer" target="_blank">@jponge</a> <span class="status">1298641967375671296</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1298700960613593088',
    created: 1598469412000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pvaneeckhoudt" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> I\'m not aware of one, though we\'ve floated the idea around for years. I\'d love to see what you come up with.<br><br>In reply to: <a href="https://x.com/pvaneeckhoudt/status/1298679563497680896" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> <span class="status">1298679563497680896</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1298528633393278976',
    created: 1598428326000,
    type: 'quote',
    text: '"You don\'t need to be Black to be outraged. You need to be American and outraged." I certainly am.<br><br>Quoting: <a href="https://x.com/Rachel__Nichols/status/1298478959206662145" rel="noopener noreferrer" target="_blank">@Rachel__Nichols</a> <span class="status">1298478959206662145</span>',
    likes: 5,
    retweets: 0
  },
  {
    id: '1298144180695216129',
    created: 1598336665000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jroper" rel="noopener noreferrer" target="_blank">@jroper</a> That\'s actually how I pronounce it. Though I know others say sitch-a-way-tion.<br><br>In reply to: <a href="https://x.com/jroper/status/1298142328905846784" rel="noopener noreferrer" target="_blank">@jroper</a> <span class="status">1298142328905846784</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1297950087792926720',
    created: 1598290390000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Halo_Coffee" rel="noopener noreferrer" target="_blank">@Halo_Coffee</a> Your coffee is excellent and I so look forward to becoming a subscriber for life.<br><br>In reply to: <a href="#1297949365231747072">1297949365231747072</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1297949365231747072',
    created: 1598290218000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Halo_Coffee" rel="noopener noreferrer" target="_blank">@Halo_Coffee</a> Sweet! I\'ll watch closely for the announcement. Good luck!!<br><br>In reply to: <a href="https://x.com/Halo_Coffee/status/1297888984643833857" rel="noopener noreferrer" target="_blank">@Halo_Coffee</a> <span class="status">1297888984643833857</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1297768216106688512',
    created: 1598247028000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/Halo_Coffee" rel="noopener noreferrer" target="_blank">@Halo_Coffee</a> Do you have plans to ship from the US? RoyalMail transport for individual orders doesn\'t seem practical or particularly environmental.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1297079260356595712',
    created: 1598082768000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RoganJoshua" rel="noopener noreferrer" target="_blank">@RoganJoshua</a> Spotify requires that I enable DRM in my browser, so I pass.<br><br>In reply to: @roganjoshua <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1297078987387109376',
    created: 1598082703000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> Especially bourbon maple syrup from Vermont. 🥃😋<br><br>In reply to: <a href="https://x.com/ggrossetie/status/1297075798646624256" rel="noopener noreferrer" target="_blank">@ggrossetie</a> <span class="status">1297075798646624256</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1297010439495118849',
    created: 1598066360000,
    type: 'post',
    text: 'Two things I rarely used to have, but now have a ton since becoming vegan: carrots and maple syrup.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1296386667347992576',
    created: 1597917641000,
    type: 'post',
    text: 'Just discovered Koven. Wow, this is some spectacular d&amp;b. Such talent! <a href="https://youtu.be/nlCJac8OTvA" rel="noopener noreferrer" target="_blank">youtu.be/nlCJac8OTvA</a>',
    likes: 3,
    retweets: 2
  },
  {
    id: '1295853430011867137',
    created: 1597790508000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aheritier" rel="noopener noreferrer" target="_blank">@aheritier</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/github" rel="noopener noreferrer" target="_blank">@github</a> Well said.<br><br>In reply to: <a href="https://x.com/aheritier/status/1295777635738570754" rel="noopener noreferrer" target="_blank">@aheritier</a> <span class="status">1295777635738570754</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1295837097144672256',
    created: 1597786614000,
    type: 'reply',
    text: '@leif81 Are there any features you miss? Did you lose any playlists?<br><br>In reply to: <a href="https://x.com/leifgruenwoldt/status/1295833907531833345" rel="noopener noreferrer" target="_blank">@leifgruenwoldt</a> <span class="status">1295833907531833345</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1295827805641400320',
    created: 1597784398000,
    type: 'post',
    text: 'Update: Our hummingbird now visits us every day (or maybe more than one?). It loves the lantana (see <a href="https://en.wikipedia.org/wiki/Lantana" rel="noopener noreferrer" target="_blank">en.wikipedia.org/wiki/Lantana</a>). One day, we\'ll managed to snap a 📸.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1295776566526398467',
    created: 1597772182000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aheritier" rel="noopener noreferrer" target="_blank">@aheritier</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/github" rel="noopener noreferrer" target="_blank">@github</a> There\'s hope that the AsciiDoc Working Group will be able to assert some influence and guidance here as it begins to fulfill its mission to advocate for the language. On the other hand, I\'m glad to see GitLab embracing AsciiDoc so strongly.<br><br>In reply to: <a href="https://x.com/aheritier/status/1295727993294393357" rel="noopener noreferrer" target="_blank">@aheritier</a> <span class="status">1295727993294393357</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1295603501398028289',
    created: 1597730920000,
    type: 'post',
    text: '"Google Play music is going away soon" Dammit Google, as soon as I get comfortable using a service, you remove it.',
    likes: 7,
    retweets: 1
  },
  {
    id: '1295457682481012736',
    created: 1597696154000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rjurney" rel="noopener noreferrer" target="_blank">@rjurney</a> The best experience, in my opinion, is the AsciiDoc plugin for IntelliJ (and other JetBrains IDE products). <a href="https://plugins.jetbrains.com/plugin/7391-asciidoc" rel="noopener noreferrer" target="_blank">plugins.jetbrains.com/plugin/7391-asciidoc</a><br><br>In reply to: <a href="https://x.com/rjurney/status/1295164042479153152" rel="noopener noreferrer" target="_blank">@rjurney</a> <span class="status">1295164042479153152</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1295108372899880960',
    created: 1597612872000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> It also frames it in a very negative way. Frankly, stalebot is a jerk. And maintainers are inviting that jerk into their projects.<br><br>In reply to: <a href="#1295107826793107456">1295107826793107456</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1295107826793107456',
    created: 1597612742000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> So it closes it. That\'s all that matters to me.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1295106529021112320" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1295106529021112320</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1295086589802975232',
    created: 1597607679000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> stalebot is not actually doing what it suggests (marking an issue as stale). What it\'s really acting as is closebot. The former is pointless anyway since being stale is an implicit state.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1294949018880155648" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1294949018880155648</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1294947926184046592',
    created: 1597574619000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> From my experience (and I could be overlooking a feature), slatebot is braindead. It looks at its watch and says, "it\'s about that time, let\'s close this." So utterly pointless at best, more often destructive.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1294945464337551360" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1294945464337551360</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1294947088313094145',
    created: 1597574419000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/docToolchain" rel="noopener noreferrer" target="_blank">@docToolchain</a> I don\'t buy that argument.<br><br>In reply to: <a href="https://x.com/RalfDMueller/status/1294921697674645504" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <span class="status">1294921697674645504</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1294947007400939521',
    created: 1597574400000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> For example, if a maintainer asks for clarification (and marks it with a corresponding label) and that information never comes, then okay, close it as invalid. There are numerous scenarios like that which actually make sense to handle with a bot.<br><br>In reply to: <a href="#1294946396341022720">1294946396341022720</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1294946396341022720',
    created: 1597574254000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> Yes, there can be other more measured approaches. This is not it.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1294945464337551360" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1294945464337551360</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1294919117141491712',
    created: 1597567750000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/docToolchain" rel="noopener noreferrer" target="_blank">@docToolchain</a> I read the message as "we don\'t care about this issue or PR anymore". It resolves nothing. All it does is push people away. It\'s the most detrimental tool I\'ve ever encountered in OSS.<br><br>In reply to: <a href="https://x.com/RalfDMueller/status/1294916049939333121" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <span class="status">1294916049939333121</span>',
    likes: 9,
    retweets: 1
  },
  {
    id: '1294914099650805760',
    created: 1597566554000,
    type: 'post',
    text: '[stalebot] causes me to lose interest in a project.',
    likes: 12,
    retweets: 3
  },
  {
    id: '1294913491564769280',
    created: 1597566409000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/j2r2b" rel="noopener noreferrer" target="_blank">@j2r2b</a> <a class="mention" href="https://x.com/mmilinkov" rel="noopener noreferrer" target="_blank">@mmilinkov</a> <a class="mention" href="https://x.com/EclipseFdn" rel="noopener noreferrer" target="_blank">@EclipseFdn</a> <a class="mention" href="https://x.com/EclipseJavaIDE" rel="noopener noreferrer" target="_blank">@EclipseJavaIDE</a> AsciiDoc will be one such example.<br><br>In reply to: <a href="https://x.com/j2r2b/status/1294909369130323968" rel="noopener noreferrer" target="_blank">@j2r2b</a> <span class="status">1294909369130323968</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1294524096206614529',
    created: 1597473570000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/manoelcampos" rel="noopener noreferrer" target="_blank">@manoelcampos</a> <a class="mention" href="https://x.com/github" rel="noopener noreferrer" target="_blank">@github</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> I think GitHub Actions kind of makes the request obsolete as now you can have control over the build. Thanks for this action!<br><br>In reply to: <a href="https://x.com/manoelcampos/status/1294464458094522369" rel="noopener noreferrer" target="_blank">@manoelcampos</a> <span class="status">1294464458094522369</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1294409014743187457',
    created: 1597446132000,
    type: 'post',
    text: 'I put Thai Basil in my pesto. Wow, is that spicy! 😮',
    likes: 2,
    retweets: 0
  },
  {
    id: '1294408094722609152',
    created: 1597445913000,
    type: 'reply',
    text: '@jbryant787 💯 Sarah said exactly the same thing after donating to Bernie. It really turned her off the whole process.<br><br>In reply to: @jbryant787 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1294402177364979712',
    created: 1597444502000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tmorello" rel="noopener noreferrer" target="_blank">@tmorello</a> I think about this performance often.<br><br>In reply to: <a href="https://x.com/tmorello/status/1294395254599122944" rel="noopener noreferrer" target="_blank">@tmorello</a> <span class="status">1294395254599122944</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1294368590041251840',
    created: 1597436494000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/connolly_s" rel="noopener noreferrer" target="_blank">@connolly_s</a> Sadly, we have no such law here (but we still get the cookie dialogs, which is a just a double drag).<br><br>In reply to: <a href="https://x.com/connolly_s/status/1294367999512784896" rel="noopener noreferrer" target="_blank">@connolly_s</a> <span class="status">1294367999512784896</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1294365208958218241',
    created: 1597435688000,
    type: 'post',
    text: 'I hate that buying from an online store means you have to then unsubscribe from the marketing emails. There should be a law against this practice.',
    likes: 7,
    retweets: 0
  },
  {
    id: '1293988462840836096',
    created: 1597345865000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> <a class="mention" href="https://x.com/intellijidea" rel="noopener noreferrer" target="_blank">@intellijidea</a> This is a feature I dreamed of for so long. And I\'m still dreaming about where it can go from here.<br><br>In reply to: <a href="https://x.com/settermjd/status/1293986822763274245" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1293986822763274245</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1293595486650155009',
    created: 1597252172000,
    type: 'post',
    text: 'Oh, and of course Martin Garrix. For me, that\'s just a given. ➕➖',
    likes: 0,
    retweets: 0
  },
  {
    id: '1293470430888488960',
    created: 1597222356000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/vogella" rel="noopener noreferrer" target="_blank">@vogella</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/gradle" rel="noopener noreferrer" target="_blank">@gradle</a> <a class="mention" href="https://x.com/ysb33r" rel="noopener noreferrer" target="_blank">@ysb33r</a> and team have done a fantastic job rewriting that plugin. It was no small task and they crushed it.<br><br>In reply to: <a href="https://x.com/vogella/status/1293460826242920449" rel="noopener noreferrer" target="_blank">@vogella</a> <span class="status">1293460826242920449</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1293445985746608131',
    created: 1597216528000,
    type: 'reply',
    text: 'There were so many amazing performances, it\'s hard to pick my favorite. But if I had to choose, it would be Vini Vici. It just had it all, from tunes, to energy, to lights, to choreography. My next top six were Eric Prydz, Dixon, <a class="mention" href="https://x.com/Netsky" rel="noopener noreferrer" target="_blank">@Netsky</a>, Klingande, <a class="mention" href="https://x.com/AmelieLens" rel="noopener noreferrer" target="_blank">@AmelieLens</a>, &amp; Kölsch.<br><br>In reply to: <a href="#1293444040780681217">1293444040780681217</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1293444040780681217',
    created: 1597216065000,
    type: 'post',
    text: '🥲 #tomorrowland2020 ends in just a few hours (after 2 weeks of relive). It was a much need feast for the eyes, ears, and soul. I\'ll remember it fondly.',
    likes: 3,
    retweets: 0,
    tags: ['tomorrowland2020']
  },
  {
    id: '1292410977170812928',
    created: 1596969763000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aruizca" rel="noopener noreferrer" target="_blank">@aruizca</a> <a class="mention" href="https://x.com/Tesla" rel="noopener noreferrer" target="_blank">@Tesla</a> I had the same thought when I took my cross-country journey in a Model 3 last fall.<br><br>In reply to: <a href="https://x.com/aruizca/status/1292397232046645248" rel="noopener noreferrer" target="_blank">@aruizca</a> <span class="status">1292397232046645248</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1292354330897469441',
    created: 1596956257000,
    type: 'post',
    text: 'I saw a hummingbird perch for a brief moment today on the bamboo cage of my tomato plant. I can honestly say it was one of my life\'s best firsts. So exquisite. So magical. (The encounter was too fleeting to photograph, so I just enjoyed the spectacle).',
    likes: 6,
    retweets: 0
  },
  {
    id: '1291810042463698945',
    created: 1596826489000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/xahteiwi" rel="noopener noreferrer" target="_blank">@xahteiwi</a> <a class="mention" href="https://x.com/spamaps" rel="noopener noreferrer" target="_blank">@spamaps</a> It\'s a falsity that AsciiDoc has a smaller user community. The user community is huge. The asciidoctor gem has ~8M downloads, almost every major tech company is using AsciiDoc in some capacity, and the project has at least 500 contributors.<br><br>In reply to: <a href="https://x.com/xahteiwi/status/1291626635385540609" rel="noopener noreferrer" target="_blank">@xahteiwi</a> <span class="status">1291626635385540609</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1291450512043159552',
    created: 1596740770000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> As of yet, no one has stepped forward.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1291450100800159744" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1291450100800159744</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1291446978908311552',
    created: 1596739928000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> There are only so many hours in the day and I\'m already overstretched as it is. I\'m doing the best I can.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1291446081759465482" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1291446081759465482</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1291445709187629056',
    created: 1596739625000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> It\'s not dead, but it is just a lab. It\'s serving it\'s purpose as a whiteboard. It should not be treated as a production thing. Just a place to bounce around ideas.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1291338144370106376" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1291338144370106376</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1291159405187272705',
    created: 1596671365000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/shobull" rel="noopener noreferrer" target="_blank">@shobull</a> True. ;)<br><br>In reply to: <a href="https://x.com/shobull/status/1291156757000138752" rel="noopener noreferrer" target="_blank">@shobull</a> <span class="status">1291156757000138752</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1291096180974215168',
    created: 1596656291000,
    type: 'post',
    text: 'Really Twitter? That\'s what\'s trending? You don\'t say. 🤯 Top work.',
    photos: ['<div class="item"><img class="photo" src="media/1291096180974215168.png"></div>'],
    likes: 1,
    retweets: 0
  },
  {
    id: '1291095047337684992',
    created: 1596656021000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rmoff" rel="noopener noreferrer" target="_blank">@rmoff</a> <a href="https://plugins.jetbrains.com/plugin/7391-asciidoc" rel="noopener noreferrer" target="_blank">plugins.jetbrains.com/plugin/7391-asciidoc</a><br><br>In reply to: <a href="https://x.com/rmoff/status/1290948770138660864" rel="noopener noreferrer" target="_blank">@rmoff</a> <span class="status">1290948770138660864</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1291094225279283200',
    created: 1596655825000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/hsablonniere" rel="noopener noreferrer" target="_blank">@hsablonniere</a> <a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> <a class="mention" href="https://x.com/eleven_ty" rel="noopener noreferrer" target="_blank">@eleven_ty</a> I can confirm Opal no longer does that.<br><br>As for site generators, I\'m only focused on one ;)<br><br>In reply to: <a href="https://x.com/hsablonniere/status/1291088990590980096" rel="noopener noreferrer" target="_blank">@hsablonniere</a> <span class="status">1291088990590980096</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1291087637353074688',
    created: 1596654254000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/hsablonniere" rel="noopener noreferrer" target="_blank">@hsablonniere</a> <a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> <a class="mention" href="https://x.com/eleven_ty" rel="noopener noreferrer" target="_blank">@eleven_ty</a> I\'m not aware of such a plugin. I also wasn\'t aware of <a class="mention" href="https://x.com/eleven_ty" rel="noopener noreferrer" target="_blank">@eleven_ty</a> either. I\'d say go for it!<br><br>In reply to: <a href="https://x.com/hsablonniere/status/1291077332703285248" rel="noopener noreferrer" target="_blank">@hsablonniere</a> <span class="status">1291077332703285248</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1290944942479511553',
    created: 1596620233000,
    type: 'post',
    text: 'I told you to watch this space. One month later, another shock wave sweeps the nation. #MedicareForAll #GreenNewDeal #DefundThePolice #HousingForAll #BlackLivesMatter <a class="mention" href="https://x.com/CoriBush" rel="noopener noreferrer" target="_blank">@CoriBush</a> is taking one hell of a progressive agenda to Washington.<br><br>Quoting: <a href="#1280674767637340161">1280674767637340161</a>',
    likes: 1,
    retweets: 0,
    tags: ['medicareforall', 'greennewdeal', 'defundthepolice', 'housingforall', 'blacklivesmatter']
  },
  {
    id: '1290942904400146432',
    created: 1596619747000,
    type: 'quote',
    text: 'I never imagined someone who grew up in the UK would understand and accurately describe the experience of Americans so well. If you want to know why, as a society, our view of our own history is so skewed, this sums it up perfectly. What we were taught was propaganda.<br><br>Quoting: <a href="https://x.com/LastWeekTonight/status/1290287518852751361" rel="noopener noreferrer" target="_blank">@LastWeekTonight</a> <span class="status">1290287518852751361</span>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1290897245928214529',
    created: 1596608861000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/paulajean2020" rel="noopener noreferrer" target="_blank">@paulajean2020</a> <a class="mention" href="https://x.com/CoriBush" rel="noopener noreferrer" target="_blank">@CoriBush</a> You and Cori give me an incredible amount of hope. You are true representatives, true believers in progress. And that makes me a believer.<br><br>In reply to: <a href="https://x.com/paulajean2020/status/1290868553088999424" rel="noopener noreferrer" target="_blank">@paulajean2020</a> <span class="status">1290868553088999424</span>',
    likes: 14,
    retweets: 0
  },
  {
    id: '1290755086239412224',
    created: 1596574968000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/justinhopper" rel="noopener noreferrer" target="_blank">@justinhopper</a> <a class="mention" href="https://x.com/ZumFitness" rel="noopener noreferrer" target="_blank">@ZumFitness</a> We\'re stuck at home, so might as well get in shape, amirite? I\'ll check out Zum Fitness. Thus far, we\'ve been using Daily Burn for cardio / strength and Gaiam for yoga.<br><br>In reply to: <a href="https://x.com/justinhopper/status/1290740546101633024" rel="noopener noreferrer" target="_blank">@justinhopper</a> <span class="status">1290740546101633024</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1290603517963415552',
    created: 1596538831000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cramTeXeD" rel="noopener noreferrer" target="_blank">@cramTeXeD</a> Thanks for watching out.<br><br>In reply to: <a href="https://x.com/cramTeXeD/status/1290601707349643271" rel="noopener noreferrer" target="_blank">@cramTeXeD</a> <span class="status">1290601707349643271</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1290575783434452995',
    created: 1596532219000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/crascit" rel="noopener noreferrer" target="_blank">@crascit</a> <a class="mention" href="https://x.com/wittyelk" rel="noopener noreferrer" target="_blank">@wittyelk</a> Asciidoctor can also convert AsciiDoc to Leanpub Markdown, so you can get all of what Asciidoctor offers as well as all of what Leanpub offers. <a href="https://github.com/asciidoctor/asciidoctor-leanpub-converter" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-leanpub-converter</a><br><br>In reply to: <a href="#1290575432853594113">1290575432853594113</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1290575432853594113',
    created: 1596532135000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/crascit" rel="noopener noreferrer" target="_blank">@crascit</a> <a class="mention" href="https://x.com/wittyelk" rel="noopener noreferrer" target="_blank">@wittyelk</a> Marat has been doing an incredible job of responding to the needs of authors. And EPUB3 is not an easy platform to handle because of the wide variance in the quality and compatibility of reader apps. The converter is in good hands.<br><br>In reply to: <a href="https://x.com/crascit/status/1289882690502582274" rel="noopener noreferrer" target="_blank">@crascit</a> <span class="status">1289882690502582274</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1290574740109680640',
    created: 1596531970000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/wittyelk" rel="noopener noreferrer" target="_blank">@wittyelk</a> <a class="mention" href="https://x.com/crascit" rel="noopener noreferrer" target="_blank">@crascit</a> Asciidoctor has had support for epub3/mobi since 2014.<br><br>In reply to: <a href="https://x.com/wittyelk/status/1289948137218437120" rel="noopener noreferrer" target="_blank">@wittyelk</a> <span class="status">1289948137218437120</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1290574476145381377',
    created: 1596531907000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/crascit" rel="noopener noreferrer" target="_blank">@crascit</a> <a class="mention" href="https://x.com/wittyelk" rel="noopener noreferrer" target="_blank">@wittyelk</a> I think graduating it out of alpha is warranted. We were waiting until a test suite was added to drop the label, and that has since happened. Plus, it\'s very well tested by the user community.<br><br>In reply to: <a href="https://x.com/crascit/status/1290036996530814976" rel="noopener noreferrer" target="_blank">@crascit</a> <span class="status">1290036996530814976</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1290563604278702081',
    created: 1596529315000,
    type: 'post',
    text: 'I\'ve certainly had stints of exercising over the years, either haphazard or with a short-term goal in mind, but nothing as regular and general as what I\'m doing now. And I plan to stick with it.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1290560358936014848',
    created: 1596528541000,
    type: 'post',
    text: 'I\'ve started working out regularly (at home) for the first time since...well, college really. It\'s one of those things you don\'t think you want or need to do, but then are so happy with yourself for starting. Now, more than ever. 🏋️',
    likes: 10,
    retweets: 0
  },
  {
    id: '1290558790916423680',
    created: 1596528167000,
    type: 'post',
    text: 'A staff member at my residential complex has tested positive for COVID-19. So it\'s very close to home, literally. Makes me resolute in my decision to always wear a mask, even to pick up the mail, and to stay at home as much as possible.',
    likes: 10,
    retweets: 0
  },
  {
    id: '1289821700658356224',
    created: 1596352431000,
    type: 'post',
    text: 'Once this pandemic is over (which feels more like a dream than a reality atm), I\'m really questioning whether I\'ll patron a restaurant that offers non-vegan food (or at least non-veg). For various reasons, it\'s not enough to settle for the choose your own (V) adventure.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1289819469238300672',
    created: 1596351899000,
    type: 'quote',
    text: 'In the wake of overwhelming support for a policy relevant to this national &amp; global crisis, even for a Party that follows instead of leads, it\'s pathetic that Democrats still prefer to lick the boots of their corporate donors. They confuse boldness with tepidness. Such cowards.<br><br>Quoting: <a href="https://x.com/thenation/status/1289535103803125762" rel="noopener noreferrer" target="_blank">@thenation</a> <span class="status">1289535103803125762</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1289473750547566594',
    created: 1596269474000,
    type: 'quote',
    text: 'While there were so many incredible performances at #Tomorrowland2020, there\'s no doubt <a class="mention" href="https://x.com/vinivicimusic" rel="noopener noreferrer" target="_blank">@vinivicimusic</a> absolutely killed it. His light show at the Freedom stage simply blew the virtual roof off. A new experience has been defined.<br><br>Quoting: <a href="https://x.com/crew_fest/status/1287468169997815810" rel="noopener noreferrer" target="_blank">@crew_fest</a> <span class="status">1287468169997815810</span>',
    likes: 2,
    retweets: 0,
    tags: ['tomorrowland2020']
  },
  {
    id: '1289470956444897281',
    created: 1596268807000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/djannamiranda" rel="noopener noreferrer" target="_blank">@djannamiranda</a> <a class="mention" href="https://x.com/tomorrowland" rel="noopener noreferrer" target="_blank">@tomorrowland</a> Loved your set!<br><br>In reply to: <a href="https://x.com/djannamiranda/status/1287096334386900997" rel="noopener noreferrer" target="_blank">@djannamiranda</a> <span class="status">1287096334386900997</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1289351399461941250',
    created: 1596240303000,
    type: 'post',
    text: 'I\'ll keep saying it. I really wish Bash cp would ask you if you want to create a directory that doesn\'t exist instead of just saying "cp: cannot create regular file: ... Not a directory"<br><br>I don\'t care what other shells do. I care what Bash does.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1289341623977926656',
    created: 1596237972000,
    type: 'quote',
    text: '👏 Always be upgrading.<br><br>Quoting: <a href="https://x.com/eileencodes/status/1288871787288305664" rel="noopener noreferrer" target="_blank">@eileencodes</a> <span class="status">1288871787288305664</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1289308131441295360',
    created: 1596229987000,
    type: 'post',
    text: 'I think this should be the official theme song of the #Mars2020 #Perseverance rover. <a href="https://youtu.be/a5b-R3enH6g" rel="noopener noreferrer" target="_blank">youtu.be/a5b-R3enH6g</a>',
    likes: 0,
    retweets: 0,
    tags: ['mars2020', 'perseverance']
  },
  {
    id: '1289303516574388225',
    created: 1596228887000,
    type: 'post',
    text: 'I helped fix the archive pages for all mailing lists at Eclipse. <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=559990" rel="noopener noreferrer" target="_blank">bugs.eclipse.org/bugs/show_bug.cgi?id=559990</a> I\'m going to call that a win.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1289300221596323841',
    created: 1596228101000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pvaneeckhoudt" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> <a class="mention" href="https://x.com/ahus1de" rel="noopener noreferrer" target="_blank">@ahus1de</a> That\'s got to be quite a feeling for a tool to prompt you to use the thing that you made. 😍<br><br>In reply to: <a href="https://x.com/pvaneeckhoudt/status/1289205985467219968" rel="noopener noreferrer" target="_blank">@pvaneeckhoudt</a> <span class="status">1289205985467219968</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1289107243745460224',
    created: 1596182092000,
    type: 'post',
    text: 'I should clarify that it\'s mandatory to wear a mask in public in general. Stores are enforcing the rule themselves...so you absolutely cannot enter a store without one.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1289081525326102528',
    created: 1596175960000,
    type: 'post',
    text: 'It\'s amazing the difference a governor\'s order makes (<a class="mention" href="https://x.com/GovofCO" rel="noopener noreferrer" target="_blank">@GovofCO</a>). I was out running errands today up &amp; down the I-25 corridor in Denver and most people I saw were wearing a mask, even those out walking. And it\'s mandatory for stores.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1288610336996618240',
    created: 1596063620000,
    type: 'post',
    text: 'Suffice to say, I\'m hooked.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1288610077566410752',
    created: 1596063558000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/chakathegod" rel="noopener noreferrer" target="_blank">@chakathegod</a> <a class="mention" href="https://x.com/AmelieLens" rel="noopener noreferrer" target="_blank">@AmelieLens</a> <a class="mention" href="https://x.com/CharlottedWitte" rel="noopener noreferrer" target="_blank">@CharlottedWitte</a> <a class="mention" href="https://x.com/esangiuliano" rel="noopener noreferrer" target="_blank">@esangiuliano</a> <a class="mention" href="https://x.com/realAdamBeyer" rel="noopener noreferrer" target="_blank">@realAdamBeyer</a> <a class="mention" href="https://x.com/BB_BORISBREJCHA" rel="noopener noreferrer" target="_blank">@BB_BORISBREJCHA</a> <a class="mention" href="https://x.com/CharlottedWitte" rel="noopener noreferrer" target="_blank">@CharlottedWitte</a> is another one of my favs, as is <a class="mention" href="https://x.com/djannamiranda" rel="noopener noreferrer" target="_blank">@djannamiranda</a>. I\'ll check out the others you recommended as well.<br><br>In reply to: <a href="https://x.com/chakathegod/status/1288607344843042816" rel="noopener noreferrer" target="_blank">@chakathegod</a> <span class="status">1288607344843042816</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1288596016745218048',
    created: 1596060206000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gitchat" rel="noopener noreferrer" target="_blank">@gitchat</a> As I stated, it\'s probably specific to Gnome, so you aren\'t going to see it on macOS and Windows.<br><br>In reply to: <a href="https://x.com/gitchat/status/1288589431910404101" rel="noopener noreferrer" target="_blank">@gitchat</a> <span class="status">1288589431910404101</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1288595414648623104',
    created: 1596060062000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gitchat" rel="noopener noreferrer" target="_blank">@gitchat</a> I\'m pretty sure this is a browser issue, not necessarily with the Gitter app. There\'s a related problem that the context menu doesn\'t work on the last line of compose textarea when the browser is maximized. So there\'s some sort of bad interaction going on here.<br><br>In reply to: <a href="https://x.com/gitchat/status/1288589431910404101" rel="noopener noreferrer" target="_blank">@gitchat</a> <span class="status">1288589431910404101</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1288575922921070592',
    created: 1596055415000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gitchat" rel="noopener noreferrer" target="_blank">@gitchat</a> No, I mean just maximized. On two different computers on Fedora 31, the drag and drop fails to upload the image if the browser is maximized. The progress bar never reaches completion.<br><br>In reply to: <a href="https://x.com/gitchat/status/1288574700315201542" rel="noopener noreferrer" target="_blank">@gitchat</a> <span class="status">1288574700315201542</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1288566535368265728',
    created: 1596053177000,
    type: 'post',
    text: 'This may be related to Gnome. I have no idea. All I know is that it makes no sense. Both Firefox and Chrome behave the same way.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1288557869407727616',
    created: 1596051110000,
    type: 'post',
    text: 'Posting an image to a Gitter channel (<a class="mention" href="https://x.com/gitchat" rel="noopener noreferrer" target="_blank">@gitchat</a>) doesn\'t work if the browser is maximized. I can\'t even...',
    likes: 2,
    retweets: 0
  },
  {
    id: '1288437968630190080',
    created: 1596022524000,
    type: 'post',
    text: 'I didn\'t think I liked techno until I listened to the musical compositions of <a class="mention" href="https://x.com/AmelieLens" rel="noopener noreferrer" target="_blank">@AmelieLens</a>. Now I can\'t get enough of it.',
    photos: ['<div class="item"><img class="photo" src="media/1288437968630190080.jpg"></div>'],
    likes: 103,
    retweets: 5
  },
  {
    id: '1288344301756248064',
    created: 1596000192000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/downey" rel="noopener noreferrer" target="_blank">@downey</a> I\'m done with them.<br><br>In reply to: @downey <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1288033219581861889',
    created: 1595926024000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> What I was taught was primarily propaganda...ironically while learning about propaganda. It took me a long time to undo that. But I always sensed something was very rotten. And now I know my gut was right and I\'ve learned to trust it.<br><br>In reply to: <a href="https://x.com/settermjd/status/1288031642595057665" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1288031642595057665</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1288029719808471041',
    created: 1595925190000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/tomorrowland" rel="noopener noreferrer" target="_blank">@tomorrowland</a> You just showed the world how to pull off an event of the future in the present. Other festivals have been put on notice. Thank you for your amazing work and to all the artists who made history there. I was blown away.',
    photos: ['<div class="item"><img class="photo" src="media/1288029719808471041.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1288013296902103041',
    created: 1595921274000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> And we\'re watching that lack of limit play out in real time. We always thought (or were taught) that human life would be an unacceptable cost, a line, but that\'s proving untrue.<br><br>In reply to: <a href="https://x.com/settermjd/status/1287996496911175682" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1287996496911175682</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1287906473666740225',
    created: 1595895806000,
    type: 'quote',
    text: 'Um...they were already socialists. Duh. It just took you until now to finally recognize it. And it\'s already the better option over capitalism, which feds a corruption that seems to have no bottom. If we\'ve learned anything as a society, it\'s that capitalism is a parasite.<br><br>Quoting: <a href="https://x.com/opinion/status/1287860634424619010" rel="noopener noreferrer" target="_blank">@opinion</a> <span class="status">1287860634424619010</span>',
    likes: 3,
    retweets: 1
  },
  {
    id: '1287895867094003712',
    created: 1595893277000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/connolly_s" rel="noopener noreferrer" target="_blank">@connolly_s</a> <a class="mention" href="https://x.com/github" rel="noopener noreferrer" target="_blank">@github</a> <a class="mention" href="https://x.com/ThePracticalDev" rel="noopener noreferrer" target="_blank">@ThePracticalDev</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Insidious!<br><br>In reply to: <a href="https://x.com/connolly_s/status/1286743228985806850" rel="noopener noreferrer" target="_blank">@connolly_s</a> <span class="status">1286743228985806850</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1287872242764849152',
    created: 1595887644000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/netskymusic" rel="noopener noreferrer" target="_blank">@netskymusic</a> You nailed it! Thanks for bringing that drum &amp; base to #Tomorrowland2020.',
    photos: ['<div class="item"><img class="photo" src="media/1287872242764849152.jpg"></div>', '<div class="item"><img class="photo" src="media/1287872242764849152-2.jpg"></div>', '<div class="item"><img class="photo" src="media/1287872242764849152-3.jpg"></div>', '<div class="item"><img class="photo" src="media/1287872242764849152-4.jpg"></div>'],
    likes: 1,
    retweets: 0,
    tags: ['tomorrowland2020']
  },
  {
    id: '1287655110265298944',
    created: 1595835876000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/AmelieLens" rel="noopener noreferrer" target="_blank">@AmelieLens</a> Your set at #Tomorrowland2020 was INSANE! Thanks for the trip!',
    likes: 2,
    retweets: 0,
    tags: ['tomorrowland2020']
  },
  {
    id: '1287644095955968000',
    created: 1595833250000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/justinhopper" rel="noopener noreferrer" target="_blank">@justinhopper</a> <a class="mention" href="https://x.com/tomorrowland" rel="noopener noreferrer" target="_blank">@tomorrowland</a> I\'d love to see some behind the scenes of how they did it. I know many organizers who would be infinitely curious. This has application well outside of a music festival.<br><br>In reply to: <a href="#1287643023325634561">1287643023325634561</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1287643560003608583',
    created: 1595833122000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fasterthanlime" rel="noopener noreferrer" target="_blank">@fasterthanlime</a> <a class="mention" href="https://x.com/connolly_s" rel="noopener noreferrer" target="_blank">@connolly_s</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> If you catch word of anyone in that community considering so, please pass on the message.<br><br>In reply to: <a href="https://x.com/fasterthanlime/status/1287540933848965120" rel="noopener noreferrer" target="_blank">@fasterthanlime</a> <span class="status">1287540933848965120</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1287643023325634561',
    created: 1595832994000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/justinhopper" rel="noopener noreferrer" target="_blank">@justinhopper</a> <a class="mention" href="https://x.com/tomorrowland" rel="noopener noreferrer" target="_blank">@tomorrowland</a> It has to be huge. Because of that, I think it\'s going to change festivals forever. They\'ll keep tapping into that channel even when they\'re live again. And watching from home with a live audience in the frame (not a simulated one) will take engagement to a whole other level.<br><br>In reply to: <a href="https://x.com/justinhopper/status/1287588245606289410" rel="noopener noreferrer" target="_blank">@justinhopper</a> <span class="status">1287588245606289410</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1287537297685061632',
    created: 1595807787000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/connolly_s" rel="noopener noreferrer" target="_blank">@connolly_s</a> <a class="mention" href="https://x.com/fasterthanlime" rel="noopener noreferrer" target="_blank">@fasterthanlime</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> We welcome a Rust implemention. Please join us so we can work together to bring AsciiDoc to as many language platforms as possible.<br><br>In reply to: <a href="https://x.com/connolly_s/status/1287500758003396608" rel="noopener noreferrer" target="_blank">@connolly_s</a> <span class="status">1287500758003396608</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1287536954427416576',
    created: 1595807705000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kenglxn" rel="noopener noreferrer" target="_blank">@kenglxn</a> Have not heard yet. I\'ll definitely check him out. Thanks!<br><br>In reply to: <a href="https://x.com/kenglxn/status/1287510275889995777" rel="noopener noreferrer" target="_blank">@kenglxn</a> <span class="status">1287510275889995777</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1287512255882383360',
    created: 1595801817000,
    type: 'post',
    text: '➕✖️<br>MAR+IN GARRIX!<br>#Tomorrowland2020',
    photos: ['<div class="item"><img class="photo" src="media/1287512255882383360.jpg"></div>', '<div class="item"><img class="photo" src="media/1287512255882383360-2.jpg"></div>', '<div class="item"><img class="photo" src="media/1287512255882383360-3.jpg"></div>'],
    likes: 3,
    retweets: 0,
    tags: ['tomorrowland2020']
  },
  {
    id: '1287503602274070528',
    created: 1595799754000,
    type: 'post',
    text: 'The Cave provided the perfect ambience for this set.',
    photos: ['<div class="item"><img class="photo" src="media/1287503602274070528.jpg"></div>', '<div class="item"><img class="photo" src="media/1287503602274070528-2.jpg"></div>'],
    likes: 1,
    retweets: 0
  },
  {
    id: '1287497482201526272',
    created: 1595798294000,
    type: 'post',
    text: 'Netsky! #Tomorrowland2020',
    photos: ['<div class="item"><img class="photo" src="media/1287497482201526272.jpg"></div>', '<div class="item"><img class="photo" src="media/1287497482201526272-2.jpg"></div>', '<div class="item"><img class="photo" src="media/1287497482201526272-3.jpg"></div>'],
    likes: 4,
    retweets: 0,
    tags: ['tomorrowland2020']
  },
  {
    id: '1287342520918138880',
    created: 1595761349000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ericprydz" rel="noopener noreferrer" target="_blank">@ericprydz</a> <a class="mention" href="https://x.com/tomorrowland" rel="noopener noreferrer" target="_blank">@tomorrowland</a> Damn. Now that\'s what I call a warning.<br><br>In reply to: <a href="https://x.com/ericprydz/status/1285965799711178754" rel="noopener noreferrer" target="_blank">@ericprydz</a> <span class="status">1285965799711178754</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1287339929140264963',
    created: 1595760731000,
    type: 'post',
    text: 'The nice thing about a festival in virtual reality is that you don\'t have to worry about burning anybody, not even the DJ. #Tomorrowland2020',
    photos: ['<div class="item"><img class="photo" src="media/1287339929140264963.jpg"></div>'],
    likes: 1,
    retweets: 0,
    tags: ['tomorrowland2020']
  },
  {
    id: '1287339423072284672',
    created: 1595760610000,
    type: 'post',
    text: 'Thoroughly enjoyed the sights &amp; sounds from day one of the Tomorrowland virtual festival. A nice blend of music styles and the visuals + light shows have been incredible. Can\'t wait for day two! #Tomorrowland2020',
    photos: ['<div class="item"><img class="photo" src="media/1287339423072284672.jpg"></div>', '<div class="item"><img class="photo" src="media/1287339423072284672-2.jpg"></div>', '<div class="item"><img class="photo" src="media/1287339423072284672-3.jpg"></div>', '<div class="item"><img class="photo" src="media/1287339423072284672-4.jpg"></div>'],
    likes: 2,
    retweets: 1,
    tags: ['tomorrowland2020']
  },
  {
    id: '1287268650815168513',
    created: 1595743737000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> I think this song is just the medicine you need. <a href="https://youtu.be/X4hxzxakM9w" rel="noopener noreferrer" target="_blank">youtu.be/X4hxzxakM9w</a><br><br>In reply to: <a href="https://x.com/starbuxman/status/1287258176975286273" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">1287258176975286273</span>',
    likes: 4,
    retweets: 1
  },
  {
    id: '1287258671517437957',
    created: 1595741358000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/netskymusic" rel="noopener noreferrer" target="_blank">@netskymusic</a> Have fun with you\'re set today and good luck! I\'ll be out there in the virtual crowd.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1286804997842153472',
    created: 1595633193000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ashleymcnamara" rel="noopener noreferrer" target="_blank">@ashleymcnamara</a> Here\'s the grow light system we got: <a href="https://www.gardeners.com/buy/low-bamboo-frame-led-grow-lights/8597720.html" rel="noopener noreferrer" target="_blank">www.gardeners.com/buy/low-bamboo-frame-led-grow-lights/8597720.html</a> The herbs and lettuce LOVE it.<br><br>In reply to: <a href="#1286782922096164864">1286782922096164864</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1286784871377985536',
    created: 1595628395000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rkennke" rel="noopener noreferrer" target="_blank">@rkennke</a> I\'m not a fan of Swiss cheese, so that\'s not something I\'ve tried to substitute for.<br><br>In reply to: <a href="#1286784751206936576">1286784751206936576</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1286784751206936576',
    created: 1595628366000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rkennke" rel="noopener noreferrer" target="_blank">@rkennke</a> I have a trick for parmesan. It\'s lemon, garlic, oil, and breadcrumbs. I\'ve also blended in herbs like coriander. It really seems to trick the brain.<br><br>In reply to: <a href="https://x.com/rkennke/status/1286781723397820416" rel="noopener noreferrer" target="_blank">@rkennke</a> <span class="status">1286781723397820416</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1286782922096164864',
    created: 1595627930000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ashleymcnamara" rel="noopener noreferrer" target="_blank">@ashleymcnamara</a> Sarah and I really struggled for the first year. Then we discovered the self-watering systems like EarthBox and Veradek. The plants in those have taken off. We also purchased a grow light system, which keeps the light exposure and intensity consistent. Great for seedlings.<br><br>In reply to: <a href="https://x.com/ashleymcnamara/status/1286744489214660610" rel="noopener noreferrer" target="_blank">@ashleymcnamara</a> <span class="status">1286744489214660610</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1286781644444061696',
    created: 1595627625000,
    type: 'reply',
    text: '@vbjelak I\'ve been compiling a list (primarily from the same series of books). I\'ll have to find a place to post it and link to it.<br><br>In reply to: <a href="https://x.com/digitronex/status/1286765275711516672" rel="noopener noreferrer" target="_blank">@digitronex</a> <span class="status">1286765275711516672</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1286748065970319360',
    created: 1595619620000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BenNetworks" rel="noopener noreferrer" target="_blank">@BenNetworks</a> Totally with you about eliminating meat sanitation stress. No doubt access to fresh produce varies by region, but for me it\'s turned out to be substantially cheaper. It could be due to my proximity to California. I can also make multiple meals from a single bunch. Reuse is key.<br><br>In reply to: <a href="https://x.com/BenNetworks/status/1286631848567812097" rel="noopener noreferrer" target="_blank">@BenNetworks</a> <span class="status">1286631848567812097</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1286747361587290112',
    created: 1595619452000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rkennke" rel="noopener noreferrer" target="_blank">@rkennke</a> We have a vegan cheese brand available in the US named Miyoko that\'s absolutely amazing. The cream cheese goes splendid with tomatoes &amp; basil. We\'ve also made homemade cheese using root veggies, almond milk &amp; nooch, which is frankly better than what\'s in dairy-based mac &amp; cheese.<br><br>In reply to: <a href="https://x.com/rkennke/status/1286745573111078912" rel="noopener noreferrer" target="_blank">@rkennke</a> <span class="status">1286745573111078912</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1286605401132134402',
    created: 1595585606000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/zepag" rel="noopener noreferrer" target="_blank">@zepag</a> No, I do not. Technically speaking, my diet would be described as plant-based.<br><br>In reply to: <a href="https://x.com/zepag/status/1286594911895199744" rel="noopener noreferrer" target="_blank">@zepag</a> <span class="status">1286594911895199744</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1286585544147791873',
    created: 1595580871000,
    type: 'post',
    text: 'It\'s been one year since I switched to a vegan diet (&amp; being vegan in general). I\'ve not only had some of the best food of my life, but also the cheapest. It\'s surprisingly easy to make from scratch, store for leftovers, &amp; clean up. There\'s no way I\'d switch back now. 🍅🥕🌿<br><br>Quoting: <a href="#1157810980216139776">1157810980216139776</a>',
    likes: 16,
    retweets: 0
  },
  {
    id: '1286424242393817088',
    created: 1595542414000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kennybastani" rel="noopener noreferrer" target="_blank">@kennybastani</a> <a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> And more explanation is always needed. It just always is. You really can\'t go wrong with more guidance (assuming it\'s well structured).<br><br>In reply to: <a href="https://x.com/kennybastani/status/1286416107738484736" rel="noopener noreferrer" target="_blank">@kennybastani</a> <span class="status">1286416107738484736</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1286409492557516801',
    created: 1595538898000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kennybastani" rel="noopener noreferrer" target="_blank">@kennybastani</a> That\'s why I so appreciate the work of <a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a>. He has gratuitously served this role in the context of the Antora and Asciidoctor communities. (And there are others who have as well).<br><br>In reply to: <a href="https://x.com/kennybastani/status/1286407602071633920" rel="noopener noreferrer" target="_blank">@kennybastani</a> <span class="status">1286407602071633920</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1286074384554135552',
    created: 1595459002000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> Well said.<br><br>In reply to: <a href="https://x.com/brunoborges/status/1286072669096427521" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1286072669096427521</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1286069093842464769',
    created: 1595457740000,
    type: 'post',
    text: 'It\'s so sad that the US cannot issue a national facemask order. Instead, we\'re going to continue to play whack-a-mole and keep losing to this virus.',
    likes: 5,
    retweets: 0
  },
  {
    id: '1286026016826130432',
    created: 1595447470000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/waynebeaton" rel="noopener noreferrer" target="_blank">@waynebeaton</a> <a class="mention" href="https://x.com/EclipseFdn" rel="noopener noreferrer" target="_blank">@EclipseFdn</a> And this is a key reason why I dig the <a class="mention" href="https://x.com/EclipseFdn" rel="noopener noreferrer" target="_blank">@EclipseFdn</a>.<br><br>In reply to: <a href="https://x.com/waynebeaton/status/1286024057096482824" rel="noopener noreferrer" target="_blank">@waynebeaton</a> <span class="status">1286024057096482824</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1286025803487092736',
    created: 1595447419000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/waynebeaton" rel="noopener noreferrer" target="_blank">@waynebeaton</a> I\'m willing to use Google Docs in a pinch to reduce friction for a process, esp on short notice or for something short lived. But I\'m always thinking how we can evolve away from it.<br><br>In reply to: <a href="https://x.com/waynebeaton/status/1286024561658671106" rel="noopener noreferrer" target="_blank">@waynebeaton</a> <span class="status">1286024561658671106</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1286025233892143104',
    created: 1595447283000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/waynebeaton" rel="noopener noreferrer" target="_blank">@waynebeaton</a> 💯<br><br>In reply to: <a href="https://x.com/waynebeaton/status/1286021989690474497" rel="noopener noreferrer" target="_blank">@waynebeaton</a> <span class="status">1286021989690474497</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1285286744599007233',
    created: 1595271214000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/TayAndersonCO" rel="noopener noreferrer" target="_blank">@TayAndersonCO</a> You\'re one of the reasons I can feel positive about the state of the world right now. It feels incredible to know someone in office is actually fighting for the people.<br><br>In reply to: <a href="https://x.com/AuontaiAnderson/status/1285270396955230210" rel="noopener noreferrer" target="_blank">@AuontaiAnderson</a> <span class="status">1285270396955230210</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1285102910112559104',
    created: 1595227384000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/lanceball" rel="noopener noreferrer" target="_blank">@lanceball</a> I think it needs to both. I, too, didn\'t always feel this way. But that\'s because I was misled by my formal education and it took a lot of relearning to get there. With what I know now, there\'s just no other conclusion I can draw.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1285101590261706752" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1285101590261706752</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1284773149184348160',
    created: 1595148763000,
    type: 'quote',
    text: 'No! This is faulty reasoning. Adopt the secure voting system Colorado has. Take it from someone who lives here, it\'s so easy. The paper ballot arrives in the mail, I fill it out, and either mail it back or (as I prefer) walk it across the street to the ballot drop box. 📮<br><br>Quoting: <a href="https://x.com/AndrewYang/status/1284532973262471171" rel="noopener noreferrer" target="_blank">@AndrewYang</a> <span class="status">1284532973262471171</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1284770842510012417',
    created: 1595148213000,
    type: 'post',
    text: 'Watch this an tell me you\'re not impressed. (Sub Zero Project @ Defqon.1). It\'s amazing to witness human creativity find ways to make new experiences while the familiar ones are on hold. <a href="https://youtu.be/WLXqJMfodSY" rel="noopener noreferrer" target="_blank">youtu.be/WLXqJMfodSY</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1284418119226626049',
    created: 1595064117000,
    type: 'post',
    text: 'Is it possible to turn off notifications from GitHub Actions for certain repositories? I still want to watch the repository, I just don\'t need notifications for failed builds because I don\'t fix those builds.',
    likes: 5,
    retweets: 2
  },
  {
    id: '1284378196004282369',
    created: 1595054599000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> <a class="mention" href="https://x.com/GovofCO" rel="noopener noreferrer" target="_blank">@GovofCO</a> Thanks buddy! I think about you and your safety everyday too. By working together, we\'ll get though this and be stronger!<br><br>In reply to: <a href="https://x.com/starbuxman/status/1284369909057589248" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">1284369909057589248</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1284213459681501189',
    created: 1595015322000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/billmckibben" rel="noopener noreferrer" target="_blank">@billmckibben</a> Oh no! 😱 So sorry to hear that, Bill. Wishing you a speedy recovery. May it be fueled by the many recent victories. You can count on us to 😷 up and take action for the environment while you\'re out. ♥️<br><br>In reply to: <a href="https://x.com/billmckibben/status/1284184526575501312" rel="noopener noreferrer" target="_blank">@billmckibben</a> <span class="status">1284184526575501312</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1284146853668425730',
    created: 1594999442000,
    type: 'post',
    text: 'A statewide mask order is now in effect in Colorado. 😷<br><br>Thank you <a class="mention" href="https://x.com/GovofCO" rel="noopener noreferrer" target="_blank">@GovofCO</a> for listening to the science &amp; demonstrating common-sense leadership to help keep us safe.',
    likes: 6,
    retweets: 0
  },
  {
    id: '1283859770684784643',
    created: 1594930996000,
    type: 'post',
    text: 'My approach helped me commit the information to memory and avoid falling asleep.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1283858635743883265',
    created: 1594930726000,
    type: 'quote',
    text: 'I attended Cornell for 4 years, never even heard of Cornell Notes. I just wrote down whatever the professor said as fast as I could.<br><br>Quoting: <a href="https://x.com/beautifulstephh/status/1283722376925130752" rel="noopener noreferrer" target="_blank">@beautifulstephh</a> <span class="status">1283722376925130752</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1283819372751065096',
    created: 1594921365000,
    type: 'post',
    text: 'I\'ll note (as the letter suggests) that global crises trace back to democracy being under threat. The chaotic pandemic and the climate breakdown are prime examples.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1283816090133565440',
    created: 1594920582000,
    type: 'post',
    text: '"We don’t have to choose, and divide ourselves, over which crisis or issue we should prioritize, because it\'s all interconnected."<br><br><a href="https://climateemergencyeu.org/" rel="noopener noreferrer" target="_blank">climateemergencyeu.org/</a>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1283317411500789760',
    created: 1594801688000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tlberglund" rel="noopener noreferrer" target="_blank">@tlberglund</a> <a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> <a class="mention" href="https://x.com/ale_amurray" rel="noopener noreferrer" target="_blank">@ale_amurray</a> I\'m still in my first year of knowing. And now I\'m growing both (one we named coriander and the other cilantro).<br><br>In reply to: <a href="https://x.com/tlberglund/status/1283094380903936000" rel="noopener noreferrer" target="_blank">@tlberglund</a> <span class="status">1283094380903936000</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1283219580651855873',
    created: 1594778363000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jm" rel="noopener noreferrer" target="_blank">@jm</a> Yep. I like to say that I\'m intolerant of intolerance. Solidarity or the highway.<br><br>In reply to: <a href="https://x.com/jm/status/1283202570828341248" rel="noopener noreferrer" target="_blank">@jm</a> <span class="status">1283202570828341248</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1283180389293527041',
    created: 1594769019000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/phillip_webb" rel="noopener noreferrer" target="_blank">@phillip_webb</a> <a class="mention" href="https://x.com/MarcoBehler" rel="noopener noreferrer" target="_blank">@MarcoBehler</a> <a class="mention" href="https://x.com/wimdeblauwe" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> Macros are so powerful. Little functions you can put inside your document to do wonderfully cool things.<br><br>In reply to: <a href="https://x.com/phillip_webb/status/1283136028681711616" rel="noopener noreferrer" target="_blank">@phillip_webb</a> <span class="status">1283136028681711616</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1282968616799887360',
    created: 1594718529000,
    type: 'post',
    text: 'The new RuboCop docs (<a href="https://docs.rubocop.org" rel="noopener noreferrer" target="_blank">docs.rubocop.org</a>) make it very easy to find and configure rules. I really appreciate that.',
    likes: 1,
    retweets: 2
  },
  {
    id: '1282769227745652736',
    created: 1594670991000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/omniprof" rel="noopener noreferrer" target="_blank">@omniprof</a> I don\'t get your "antique tooling" remark. That would imply writing code is antique. Is it? Is a VCS like git antique? If anything, what this industry has proved is that the collaborative process works best when the exchange format is plain text.<br><br>In reply to: <a href="https://x.com/omniprof/status/1282680290574569472" rel="noopener noreferrer" target="_blank">@omniprof</a> <span class="status">1282680290574569472</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1282768444207710208',
    created: 1594670804000,
    type: 'reply',
    text: '@ntgussoni Linting is certainly achievable, just takes someone doing the work. Since linting requires comprehending the language, a spec is def going to help. I\'d look for this as an artifact of the spec process. We\'re seeing an early version of a linter emerging in the IntelliJ plugin.<br><br>In reply to: <a href="https://x.com/ntorresdev/status/1282694857614655493" rel="noopener noreferrer" target="_blank">@ntorresdev</a> <span class="status">1282694857614655493</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1282767676356476928',
    created: 1594670621000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/omniprof" rel="noopener noreferrer" target="_blank">@omniprof</a> <a class="mention" href="https://x.com/ahus1de" rel="noopener noreferrer" target="_blank">@ahus1de</a> Once there\'s a model, more will follow. The split view is the right mode for me, where you see the code and preview at the same time. But as with HTML editing, different modes are right for different people. Some will want direct editing in the preview (aka WYSIWYG).<br><br>In reply to: <a href="#1282767133198311424">1282767133198311424</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1282767133198311424',
    created: 1594670491000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/omniprof" rel="noopener noreferrer" target="_blank">@omniprof</a> <a class="mention" href="https://x.com/ahus1de" rel="noopener noreferrer" target="_blank">@ahus1de</a> It first required getting the IDE to understand the language. Then hooking that into the existing framework for visual preview and eventually visual tooling. The visual parts won\'t be right for everyone, but for those looking will hit a sweet spot.<br><br>In reply to: <a href="#1282766877605785605">1282766877605785605</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1282766877605785605',
    created: 1594670430000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/omniprof" rel="noopener noreferrer" target="_blank">@omniprof</a> The simple answer is because writing software is hard &amp; requires dedication. In short, someone has to do the work. Fortunately, someone is. I think the IntelliJ plugin is on a trajectory to fill that void, and other tools will follow. <a class="mention" href="https://x.com/ahus1de" rel="noopener noreferrer" target="_blank">@ahus1de</a> and team is doing fantastic work.<br><br>In reply to: <a href="https://x.com/omniprof/status/1282680290574569472" rel="noopener noreferrer" target="_blank">@omniprof</a> <span class="status">1282680290574569472</span>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1282760122607923200',
    created: 1594668820000,
    type: 'quote',
    text: 'It\'s about damn time. Good riddance!<br><br>Quoting: <a href="https://x.com/Commanders/status/1282661063943651328" rel="noopener noreferrer" target="_blank">@Commanders</a> <span class="status">1282661063943651328</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1282748333774143488',
    created: 1594666009000,
    type: 'reply',
    text: '😱 And how could I have left out the Eclipse Foundation? They\'re not only hosting the AsciiDoc Working Group, but have adopted AsciiDoc themselves to manage the process documents for the Foundation (thanks in a large part to <a class="mention" href="https://x.com/waynebeaton" rel="noopener noreferrer" target="_blank">@waynebeaton</a>).<br><br>In reply to: <a href="#1282742443465555969">1282742443465555969</a>',
    likes: 5,
    retweets: 2
  },
  {
    id: '1282743475373371392',
    created: 1594664851000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/phillip_webb" rel="noopener noreferrer" target="_blank">@phillip_webb</a> <a class="mention" href="https://x.com/omniprof" rel="noopener noreferrer" target="_blank">@omniprof</a> To that amazing list I\'ll add:<br><br>- the community is 🔥<br><br>Always helping each other and sharing such great ideas. With them, the best is always yet to come.<br><br>In reply to: <a href="https://x.com/phillip_webb/status/1282642218369470464" rel="noopener noreferrer" target="_blank">@phillip_webb</a> <span class="status">1282642218369470464</span>',
    likes: 5,
    retweets: 0
  },
  {
    id: '1282742443465555969',
    created: 1594664605000,
    type: 'post',
    text: 'Any list will be incomplete, but I\'d be remiss not to mention Red Hat &amp; CloudBees, who will also be on the initial Steering Committee for the Working Group. By mentioning Red Hat, Fedora comes to mind as well, which has put the full wind of this writing platform in its sails.<br><br>Quoting: <a href="#1282600638736527360">1282600638736527360</a>',
    likes: 5,
    retweets: 2
  },
  {
    id: '1282604647534915584',
    created: 1594631752000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> <a class="mention" href="https://x.com/phpstorm" rel="noopener noreferrer" target="_blank">@phpstorm</a> I cannot wait to hear your thoughts. I\'m so excited about where this plugin is headed, but we certainly need user testing to validate those accolades.<br><br>More than anything, this plugin is positioning itself to explore many more possibilities (xref validation comes to mind).<br><br>In reply to: <a href="https://x.com/settermjd/status/1282603073094012928" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1282603073094012928</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1282603446944718849',
    created: 1594631466000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/omniprof" rel="noopener noreferrer" target="_blank">@omniprof</a> Naturally, we can have code assistance. And the IntelliJ plugin for AsciiDoc is demonstrating how that can be done. For me, this is the perfect balance between working with the raw source and getting that 21st century help from the computer.<br><br>In reply to: <a href="#1282602961621966849">1282602961621966849</a>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1282602961621966849',
    created: 1594631350000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/omniprof" rel="noopener noreferrer" target="_blank">@omniprof</a> There\'s another way to look at this, taking a programmer\'s perspective. There\'s a reason why attempts to make visual tools for programming have flopped. We need to see the code. Plain text is honest, transparent, VCS friendly, and diffable. That translates to writing too.<br><br>In reply to: <a href="#1282601037497380865">1282601037497380865</a>',
    likes: 12,
    retweets: 2
  },
  {
    id: '1282601157609656320',
    created: 1594630920000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/omniprof" rel="noopener noreferrer" target="_blank">@omniprof</a> s/there own/their own/<br><br>In reply to: <a href="#1282600638736527360">1282600638736527360</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1282601037497380865',
    created: 1594630891000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/omniprof" rel="noopener noreferrer" target="_blank">@omniprof</a> I\'m definitely not going to say a word processor has no value. But it has value in a specific context. And that context is not in writing documentation with large-scale collaboration like we see in OSS.<br><br>In reply to: <a href="#1282600638736527360">1282600638736527360</a>',
    likes: 9,
    retweets: 0
  },
  {
    id: '1282600638736527360',
    created: 1594630796000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/omniprof" rel="noopener noreferrer" target="_blank">@omniprof</a> To each there own (esp for individual writing). But for teams, we simply cannot ignore the substantial gain in productivity &amp; collaboration gained by writing in AsciiDoc. Just ask MuleSoft, Couchbase, Neo4j, Fauna, Pivotal, Gradle, &amp; the list goes on. The story is consistent.<br><br>In reply to: <a href="https://x.com/omniprof/status/1282307738522071043" rel="noopener noreferrer" target="_blank">@omniprof</a> <span class="status">1282307738522071043</span>',
    likes: 21,
    retweets: 1
  },
  {
    id: '1282529786640777217',
    created: 1594613904000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RhapsoDani" rel="noopener noreferrer" target="_blank">@RhapsoDani</a> 🙋 Never stopped.<br><br>In reply to: <a href="https://x.com/RhapsoDani/status/1282374689868972036" rel="noopener noreferrer" target="_blank">@RhapsoDani</a> <span class="status">1282374689868972036</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1282206045205434369',
    created: 1594536718000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Lspacewalker" rel="noopener noreferrer" target="_blank">@Lspacewalker</a> Still use it every day.<br><br>In reply to: <a href="https://x.com/Lspacewalker/status/664493486436126721" rel="noopener noreferrer" target="_blank">@Lspacewalker</a> <span class="status">664493486436126721</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1282205910908063746',
    created: 1594536686000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MaxGrobe" rel="noopener noreferrer" target="_blank">@MaxGrobe</a> Yep, all those things too. It\'s undemocratic no matter how you look at it.<br><br>In reply to: <a href="https://x.com/MaxGrobe/status/1282120271932030977" rel="noopener noreferrer" target="_blank">@MaxGrobe</a> <span class="status">1282120271932030977</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1282118879158796289',
    created: 1594515936000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MaxGrobe" rel="noopener noreferrer" target="_blank">@MaxGrobe</a> Read about its history. (And I don\'t accept that something that started out as racist has reformed itself..esp given it hasn\'t changed in any fundamental way, other than reallocating the distribution of electors).<br><br>In reply to: <a href="https://x.com/MaxGrobe/status/1282111250294542337" rel="noopener noreferrer" target="_blank">@MaxGrobe</a> <span class="status">1282111250294542337</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1282087799353536512',
    created: 1594508526000,
    type: 'post',
    text: 'The electoral college is racist. If we\'re serious about dismantling systemic racism, that institution must be at the top of the list of what to bring down.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1282022273667354626',
    created: 1594492903000,
    type: 'post',
    text: 'I love how we\'re all like, "the kids are going to save the world!" And then when the kids speak up to save the world, we\'re like, "Oh, don\'t listen to them, they\'re just kids." 🤦',
    likes: 7,
    retweets: 2
  },
  {
    id: '1281784961968648193',
    created: 1594436324000,
    type: 'post',
    text: 'The DNC just solved the toilet paper shortage problem. We can just use the paper the platform is printed on.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1281647421097824257',
    created: 1594403531000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/davefarley77" rel="noopener noreferrer" target="_blank">@davefarley77</a> SEO<br><br>In reply to: <a href="https://x.com/davefarley77/status/1281257303149182977" rel="noopener noreferrer" target="_blank">@davefarley77</a> <span class="status">1281257303149182977</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1281327022304706560',
    created: 1594327142000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rgielen" rel="noopener noreferrer" target="_blank">@rgielen</a> <a class="mention" href="https://x.com/rmoff" rel="noopener noreferrer" target="_blank">@rmoff</a> Here\'s more info about incremental substitutions. <a href="https://asciidoctor.org/docs/user-manual/#incremental-substitutions" rel="noopener noreferrer" target="_blank">asciidoctor.org/docs/user-manual/#incremental-substitutions</a><br><br>In reply to: <a href="#1281326766024388609">1281326766024388609</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1281326766024388609',
    created: 1594327081000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rgielen" rel="noopener noreferrer" target="_blank">@rgielen</a> <a class="mention" href="https://x.com/rmoff" rel="noopener noreferrer" target="_blank">@rmoff</a> Cha-ching!<br><br>In reply to: <a href="https://x.com/rgielen/status/1281325709441617920" rel="noopener noreferrer" target="_blank">@rgielen</a> <span class="status">1281325709441617920</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1281274664451358720',
    created: 1594314659000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rgielen" rel="noopener noreferrer" target="_blank">@rgielen</a> <a class="mention" href="https://x.com/rmoff" rel="noopener noreferrer" target="_blank">@rmoff</a> You can add subs=+quotes to the block attribute list to force formatting to be applied.<br><br>In reply to: <a href="https://x.com/rgielen/status/1281199429345779712" rel="noopener noreferrer" target="_blank">@rgielen</a> <span class="status">1281199429345779712</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1280950148810457088',
    created: 1594237289000,
    type: 'reply',
    text: '@ntgussoni Yes!<br><br>In reply to: <a href="https://x.com/ntorresdev/status/1280855597953490944" rel="noopener noreferrer" target="_blank">@ntorresdev</a> <span class="status">1280855597953490944</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1280949961736085504',
    created: 1594237244000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rmoff" rel="noopener noreferrer" target="_blank">@rmoff</a> Best to talk to <a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> (and <a class="mention" href="https://x.com/oktadev" rel="noopener noreferrer" target="_blank">@oktadev</a>) about this topic as he has loads of experience with it.<br><br>In reply to: <a href="https://x.com/rmoff/status/1280904066139222017" rel="noopener noreferrer" target="_blank">@rmoff</a> <span class="status">1280904066139222017</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1280814976790179840',
    created: 1594205061000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Czajkowski" rel="noopener noreferrer" target="_blank">@Czajkowski</a> Whoa! I keep telling my plants, there\'s nothing I can do about the weather. This is just how it is :)<br><br>In reply to: <a href="https://x.com/Czajkowski/status/1280798688613470208" rel="noopener noreferrer" target="_blank">@Czajkowski</a> <span class="status">1280798688613470208</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1280785808048324609',
    created: 1594198107000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Czajkowski" rel="noopener noreferrer" target="_blank">@Czajkowski</a> As someone who\'s still struggling to keep outdoor plants alive for a single season (Colorado is a tough climate), I\'m impressed!! #amatuergardening right along with you! 🌼<br><br>In reply to: <a href="https://x.com/Czajkowski/status/1280783285895991296" rel="noopener noreferrer" target="_blank">@Czajkowski</a> <span class="status">1280783285895991296</span>',
    likes: 0,
    retweets: 0,
    tags: ['amatuergardening']
  },
  {
    id: '1280737288427696128',
    created: 1594186539000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> Regardless of how or where this virus originated, there\'s no denying it\'s our virus now. How we confront it from here on out will say a lot about who we are as a nation.<br><br>In reply to: <a href="https://x.com/starbuxman/status/1280730011540938752" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">1280730011540938752</span>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1280736059605708800',
    created: 1594186246000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lucperkins" rel="noopener noreferrer" target="_blank">@lucperkins</a> Just finished watching this episode on PBS by Lucy Worsley, in which she dispels many myths about the French Revolution. It seems we must chose sources carefully. <a href="https://www.pbs.org/video/marie-antoinette-the-doomed-queen-6mpxwq/" rel="noopener noreferrer" target="_blank">www.pbs.org/video/marie-antoinette-the-doomed-queen-6mpxwq/</a><br><br>In reply to: <a href="https://x.com/lucperkins/status/1280563675422576640" rel="noopener noreferrer" target="_blank">@lucperkins</a> <span class="status">1280563675422576640</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1280674767637340161',
    created: 1594171633000,
    type: 'quote',
    text: 'Watch this space 👇<br><br>Quoting: <a href="https://x.com/CoriBush/status/1280210335589453825" rel="noopener noreferrer" target="_blank">@CoriBush</a> <span class="status">1280210335589453825</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1280660173996974080',
    created: 1594168153000,
    type: 'post',
    text: '"We need leaders not in love with money but in love with justice." - MLK<br><br>Now more than ever.',
    likes: 6,
    retweets: 0
  },
  {
    id: '1280652858128347136',
    created: 1594166409000,
    type: 'reply',
    text: '@CRosserAuthor I couldn\'t have said it better. One thing that particularly annoys me is offsetting the keyboard and the touchpad. I don\'t get that design decision. It immediately knocks out half the options on the market.<br><br>In reply to: <a href="https://x.com/RosserWrites/status/1280637899910729728" rel="noopener noreferrer" target="_blank">@RosserWrites</a> <span class="status">1280637899910729728</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1280637260614914050',
    created: 1594162690000,
    type: 'reply',
    text: '@CRosserAuthor Agreed. I was about to followup that it\'s sad there really aren\'t any laptops worth buying.<br><br>In reply to: <a href="https://x.com/RosserWrites/status/1280636694237114369" rel="noopener noreferrer" target="_blank">@RosserWrites</a> <span class="status">1280636694237114369</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1280635792436498432',
    created: 1594162340000,
    type: 'post',
    text: 'I\'ve been extremely dissatisfied with the keyboard on the Dell XPS 13 9360. The keys have popped off one after the other. I\'m so tired of having to stop what I\'m doing to replace them. As soon as I fix one, another breaks.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1280215710308765696',
    created: 1594062185000,
    type: 'post',
    text: '#NoDAPL',
    likes: 0,
    retweets: 0,
    tags: ['nodapl']
  },
  {
    id: '1280026515673985024',
    created: 1594017077000,
    type: 'quote',
    text: 'I just learned this the other way around last year.<br><br>Quoting: <a href="https://x.com/copyconstruct/status/1279852725052661760" rel="noopener noreferrer" target="_blank">@copyconstruct</a> <span class="status">1279852725052661760</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1280015226637672450',
    created: 1594014386000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lanceball" rel="noopener noreferrer" target="_blank">@lanceball</a> You put into words what I\'ve been feeling all week watching us plummet back into this pandemic.<br><br>In reply to: <a href="https://x.com/lanceball/status/1279985441354194945" rel="noopener noreferrer" target="_blank">@lanceball</a> <span class="status">1279985441354194945</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1279692659037138945',
    created: 1593937480000,
    type: 'post',
    text: 'Reference: <a href="https://github.com/asciidoctor/asciidoctor-intellij-plugin/issues/103#issuecomment-653619359" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-intellij-plugin/issues/103#issuecomment-653619359</a>',
    likes: 8,
    retweets: 0
  },
  {
    id: '1279676390590214148',
    created: 1593933601000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/officialYULA" rel="noopener noreferrer" target="_blank">@officialYULA</a> That was incredible. Thank you from Denver!<br><br>In reply to: <a href="https://x.com/officialYULA/status/1279551045132103680" rel="noopener noreferrer" target="_blank">@officialYULA</a> <span class="status">1279551045132103680</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1279671630814314496',
    created: 1593932466000,
    type: 'post',
    text: 'Made chickpea omelets using kale microgreens from our home (inside) garden and honestly it seems unfair that vegan food is this good. WOW',
    likes: 0,
    retweets: 0
  },
  {
    id: '1279552353448849408',
    created: 1593904028000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rustywonder" rel="noopener noreferrer" target="_blank">@rustywonder</a> There\'s no question in my mind it\'s because it disrupts that pristine, insect desert, carpet-like monoculture Americans like to call a lawn. Let it grow. HOAs be dammed!<br><br>In reply to: <a href="https://x.com/rustywonder/status/1279400450048036864" rel="noopener noreferrer" target="_blank">@rustywonder</a> <span class="status">1279400450048036864</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1279153672258072576',
    created: 1593808975000,
    type: 'post',
    text: 'I just received word that preview for AsciiDoc in Javadoc (Asciidoclet) is coming to the Asciidoctor plugin for IntelliJ.',
    likes: 40,
    retweets: 16
  },
  {
    id: '1278802577061101569',
    created: 1593725268000,
    type: 'post',
    text: 'Just swung through downtown Denver to pick up beers from @NovelStrand &amp; <a class="mention" href="https://x.com/hogshead54" rel="noopener noreferrer" target="_blank">@hogshead54</a> in support of Black-owned businesses, saw a ton of Black Lives Matter yard signs. ✊',
    likes: 0,
    retweets: 0
  },
  {
    id: '1278749043653369856',
    created: 1593712504000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/chuckfrain" rel="noopener noreferrer" target="_blank">@chuckfrain</a> In Colorado, it\'s best to be unaffiliated because you can still request the ballot for the party you want to vote with that election cycle.<br><br>In reply to: <a href="#1278744911097917440">1278744911097917440</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1278744911097917440',
    created: 1593711519000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/chuckfrain" rel="noopener noreferrer" target="_blank">@chuckfrain</a> Yes, they are. They include name, city, and party affiliation.<br><br>In reply to: <a href="https://x.com/chuckfrain/status/1278703593932062720" rel="noopener noreferrer" target="_blank">@chuckfrain</a> <span class="status">1278703593932062720</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1278605073942118400',
    created: 1593678179000,
    type: 'quote',
    text: 'Headed there tomorrow!<br><br>Quoting: <a href="https://x.com/KyleClark/status/1268755591046053889" rel="noopener noreferrer" target="_blank">@KyleClark</a> <span class="status">1268755591046053889</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1278600552130084865',
    created: 1593677101000,
    type: 'post',
    text: 'As of yesterday, automatic voter registration went into effect in Colorado (tied to getting a driver\'s license or identification card...basically declaring yourself as a resident). <a href="https://leg.colorado.gov/bills/sb19-235" rel="noopener noreferrer" target="_blank">leg.colorado.gov/bills/sb19-235</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1278447358485938178',
    created: 1593640577000,
    type: 'reply',
    text: '@CRosserAuthor 😢<br><br>In reply to: <a href="https://x.com/RosserWrites/status/1278446011929767936" rel="noopener noreferrer" target="_blank">@RosserWrites</a> <span class="status">1278446011929767936</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1278445450836787202',
    created: 1593640122000,
    type: 'reply',
    text: '@CRosserAuthor Yes, that exactly. And this is not a radical assessment of the situation. That conclusion is evidence-based and widely reported.<br><br>In reply to: <a href="https://x.com/RosserWrites/status/1278443874382462978" rel="noopener noreferrer" target="_blank">@RosserWrites</a> <span class="status">1278443874382462978</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1278442705438281734',
    created: 1593639467000,
    type: 'reply',
    text: '@CRosserAuthor The consequence is that the GOP is going to maintain control of the Senate and their chokehold on progress. The suffering is going to continue for a long time, but it\'s largely thanks to the Democratic Party. They are setting up elections to lose.<br><br>In reply to: <a href="https://x.com/RosserWrites/status/1278442116855869440" rel="noopener noreferrer" target="_blank">@RosserWrites</a> <span class="status">1278442116855869440</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1278441592702013441',
    created: 1593639202000,
    type: 'reply',
    text: '@CRosserAuthor As for the presidential primary, Bernie was the excellent candidate.<br><br>In reply to: <a href="#1278441395531984896">1278441395531984896</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1278441395531984896',
    created: 1593639155000,
    type: 'reply',
    text: '@CRosserAuthor It\'s every election up and down the ballot. Romanoff vs Hickenlooper in Colorado, Booker vs McGrath in Kentucky, and the list goes on.<br><br>In reply to: <a href="https://x.com/RosserWrites/status/1278440884204437513" rel="noopener noreferrer" target="_blank">@RosserWrites</a> <span class="status">1278440884204437513</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1278440469068906498',
    created: 1593638934000,
    type: 'post',
    text: 'Instead of putting forth two solid candidates we\'d be comfortable voting for, what the Democratic Party is doing is putting forth one excellent candidate and one candidate we\'d never vote for. This is a recipe for disaster. This is the epicenter of the polarization.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1278262471841574914',
    created: 1593596496000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bk2204" rel="noopener noreferrer" target="_blank">@bk2204</a> 💯<br><br>In reply to: <a href="https://x.com/bk2204/status/1277752241919754243" rel="noopener noreferrer" target="_blank">@bk2204</a> <span class="status">1277752241919754243</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1278260813032112128',
    created: 1593596101000,
    type: 'post',
    text: 'Cornell does acknowledge in the report that the modeling has limits and they must be prepared to adjust their actions based on observations, such as switching back to online study. That\'s important. Staying the course when evidence shows it was the wrong choice is disastrous.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1278260075635720193',
    created: 1593595925000,
    type: 'post',
    text: 'I think cities and states that have applied these measures strongly have also proved this to be true. When there\'s a clear system in place that people must follow, the virus can be effectively controlled. When it\'s no-holds-barred, it runs rampant through the population.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1278259289757347840',
    created: 1593595738000,
    type: 'post',
    text: 'With online study, students are free to do whatever they want wherever they want, many in the vicinity of the campus. In-person (aka residential) instruction means the school can enforce mandatory screening tests &amp; behavioral requirements (social distancing, mask wearing, etc.).',
    likes: 0,
    retweets: 0
  },
  {
    id: '1278258690034790401',
    created: 1593595595000,
    type: 'post',
    text: 'Interesting that Cornell has concluded that having in-person study is better for protecting health than a purely online semester. The result, found through epidemiological modeling conducted by the OR dept, is based on the fact that it\'s a more effective way to control behavior.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1278091525029687296',
    created: 1593555740000,
    type: 'quote',
    text: 'It cannot be overstated how important this is for people who have to vote in person for any reason. Make space.<br><br>Quoting: <a href="https://x.com/georgestern/status/1278088442870726656" rel="noopener noreferrer" target="_blank">@georgestern</a> <span class="status">1278088442870726656</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1278079426123579392',
    created: 1593552855000,
    type: 'post',
    text: 'Don\'t use the words of the oppressor.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1278076103333036034',
    created: 1593552063000,
    type: 'post',
    text: 'Stop saying Amy McGrath won. She bought an election and very likely rigged the results in ways we many never know (or benefits from voter suppression). When you say she won, we lose all the power. cc: <a class="mention" href="https://x.com/sunrisemvmt" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1278075808209268736',
    created: 1593551992000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AP_Politics" rel="noopener noreferrer" target="_blank">@AP_Politics</a> Bullshit.<br><br>In reply to: <a href="https://x.com/AP_Politics/status/1278001285866950656" rel="noopener noreferrer" target="_blank">@AP_Politics</a> <span class="status">1278001285866950656</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1278075680094171137',
    created: 1593551962000,
    type: 'post',
    text: 'Another stolen election. How many times are we going to stand by while the elite class gets away with tyranny? Words matter. We need to call it what it is.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1278074745460670465',
    created: 1593551739000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bentolor" rel="noopener noreferrer" target="_blank">@bentolor</a> <a class="mention" href="https://x.com/MaxGrobe" rel="noopener noreferrer" target="_blank">@MaxGrobe</a> Exactly.<br><br>In reply to: <a href="https://x.com/bentolor/status/1277962282123706370" rel="noopener noreferrer" target="_blank">@bentolor</a> <span class="status">1277962282123706370</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1278074679853346816',
    created: 1593551723000,
    type: 'reply',
    text: '@jbryant787 In that case, I think there should be a way to disable certain warnings. The end goal should still be no warnings in the output. No noise to ignore.<br><br>In reply to: @jbryant787 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1277919803169103874',
    created: 1593514798000,
    type: 'post',
    text: 'I used to think warnings should not cause a built to fail. I\'ve changed my position on this. Experience has shown that 1 warning leads to 1000. It\'s better to be strict from the start.',
    likes: 22,
    retweets: 4
  },
  {
    id: '1277918994473693185',
    created: 1593514605000,
    type: 'post',
    text: '#IVoted. Once again, I\'d like to reiterate what I pleasure it is to vote in Colorado. The ballot comes to me. I walk it back (to a ballot drop box). And this has been an exciting voting year for me, first getting to vote for <a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a>, now <a class="mention" href="https://x.com/romanoff2020" rel="noopener noreferrer" target="_blank">@romanoff2020</a>.',
    likes: 1,
    retweets: 0,
    tags: ['ivoted']
  },
  {
    id: '1277909882797281281',
    created: 1593512433000,
    type: 'post',
    text: 'I cannot stop thinking about the fact that we built a society (most notably in the US) that was absolutely not equipped to handle a pandemic. And that\'s only a preview of the impending climate collapse.',
    likes: 5,
    retweets: 0
  },
  {
    id: '1277887675769520128',
    created: 1593507138000,
    type: 'post',
    text: 'Netlify uploads from the build server to the web server are painfully slow. They at least need to add some sort of indicator so you get some sense of how far along the transfer is.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1277881404970250241',
    created: 1593505643000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/oluwasayo_" rel="noopener noreferrer" target="_blank">@oluwasayo_</a> I agree about reduce. It makes it easy to forget the initial value / accumulator and makes formatting the whole thing awkward.<br><br>In reply to: @oluwasayo_ <span class="status">&lt;deleted&gt;</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1277872505647058944',
    created: 1593503521000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> <a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> <a class="mention" href="https://x.com/amazon" rel="noopener noreferrer" target="_blank">@amazon</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> You could stick it somewhere like an S3 bucket (until Amazon slaps you, but then you actually got someone\'s attention).<br><br>In reply to: <a href="https://x.com/mraible/status/1277870786468933633" rel="noopener noreferrer" target="_blank">@mraible</a> <span class="status">1277870786468933633</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1277763179720785920',
    created: 1593477456000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/romanoff2020" rel="noopener noreferrer" target="_blank">@romanoff2020</a> We love you for the effort you put in. The fact that you\'re tired shows just how much. That\'s what counts.<br><br>In reply to: <a href="https://x.com/AndrewRomanoff/status/1277762413312577536" rel="noopener noreferrer" target="_blank">@AndrewRomanoff</a> <span class="status">1277762413312577536</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1277761576649371650',
    created: 1593477074000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gedamore" rel="noopener noreferrer" target="_blank">@gedamore</a> In the issue, the Eclipse Foundation stated it would be approved and created on July 1. It may be slightly delayed, but I\'m pushing hard to get it moving. There\'s no doubt that COVID-19 has impacted the scheduling.<br><br>In reply to: <a href="https://x.com/gedamore/status/1277760817392320512" rel="noopener noreferrer" target="_blank">@gedamore</a> <span class="status">1277760817392320512</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1277760531927973888',
    created: 1593476825000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gedamore" rel="noopener noreferrer" target="_blank">@gedamore</a> That\'s absolutely not what I\'m saying. What I\'m saying is wait until the technical list is online so your input goes to the right space...where the spec is being worked on.<br><br>In reply to: <a href="https://x.com/gedamore/status/1277759395519016960" rel="noopener noreferrer" target="_blank">@gedamore</a> <span class="status">1277759395519016960</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1277760117471993858',
    created: 1593476726000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gedamore" rel="noopener noreferrer" target="_blank">@gedamore</a> Saying I need you to repost on another list is not shutting you down. I don\'t understand why you\'re being so combative. Do you actually want to help, or just bash the effort?<br><br>In reply to: <a href="https://x.com/gedamore/status/1277758810434633728" rel="noopener noreferrer" target="_blank">@gedamore</a> <span class="status">1277758810434633728</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1277759298093764608',
    created: 1593476530000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gedamore" rel="noopener noreferrer" target="_blank">@gedamore</a> That was not shutting you down. It was saying that list is not the technical list. I absolutely welcome your participation, on the spec list. And we are weeks away (if not shorter) from that starting. You\'re being completely unfair if you don\'t give that the space to happen.<br><br>In reply to: <a href="https://x.com/gedamore/status/1277758544444440576" rel="noopener noreferrer" target="_blank">@gedamore</a> <span class="status">1277758544444440576</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1277758571455758336',
    created: 1593476357000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gedamore" rel="noopener noreferrer" target="_blank">@gedamore</a> How exactly were you shut down? Given you are talking to the person who leads the AsciiDoc WG, I should know. And I call BS.<br><br>In reply to: <a href="https://x.com/gedamore/status/1277758160594284546" rel="noopener noreferrer" target="_blank">@gedamore</a> <span class="status">1277758160594284546</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1277758363690864640',
    created: 1593476308000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gedamore" rel="noopener noreferrer" target="_blank">@gedamore</a> The response writes itself. The community needs patience right now. It does no one any good to intentionally fragment because after a decade, you cannot wait a few more months for things to ramp up.<br><br>In reply to: <a href="https://x.com/gedamore/status/1277758003404406786" rel="noopener noreferrer" target="_blank">@gedamore</a> <span class="status">1277758003404406786</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1277757912580907009',
    created: 1593476200000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gedamore" rel="noopener noreferrer" target="_blank">@gedamore</a> Or feel free to spread FUD. But know that by doing so, you are not helping. It would be far better to be an advocate. If you want things to move faster, I encourage you to use your voice on the list(s).<br><br>In reply to: <a href="#1277757196227379200">1277757196227379200</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1277757510930202625',
    created: 1593476104000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gedamore" rel="noopener noreferrer" target="_blank">@gedamore</a> A draft spec would just be a waste of time and effort (and I don\'t have spare resources for that waste). I know how AsciiDoc is parsed. I want to begin the transition in a way that makes the effort count.<br><br>In reply to: <a href="https://x.com/gedamore/status/1277756424261533696" rel="noopener noreferrer" target="_blank">@gedamore</a> <span class="status">1277756424261533696</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1277757196227379200',
    created: 1593476029000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gedamore" rel="noopener noreferrer" target="_blank">@gedamore</a> There is no design by committee here. This is a community process that has the legal protections required. Please don\'t spread FUD.<br><br>In reply to: <a href="https://x.com/gedamore/status/1277756870082433024" rel="noopener noreferrer" target="_blank">@gedamore</a> <span class="status">1277756870082433024</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1277756757738024961',
    created: 1593475925000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gedamore" rel="noopener noreferrer" target="_blank">@gedamore</a> I expect real work in the spec to start this year.<br><br>In reply to: <a href="https://x.com/gedamore/status/1277756424261533696" rel="noopener noreferrer" target="_blank">@gedamore</a> <span class="status">1277756424261533696</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1277756041589977089',
    created: 1593475754000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gedamore" rel="noopener noreferrer" target="_blank">@gedamore</a> &gt; That\'s not how specs work<br><br>...specifically ones at the Eclipse Foundation. They do not accept "throw it over the wall" specs. And I happen to agree with them.<br><br>In reply to: <a href="#1277755501737127936">1277755501737127936</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1277755768859529218',
    created: 1593475689000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gedamore" rel="noopener noreferrer" target="_blank">@gedamore</a> And AsciiDoc was already in the high risk category for splintering, so it\'s especially pertinent in this case. Trust me, I thought about this. For A LONG TIME.<br><br>In reply to: <a href="#1277755501737127936">1277755501737127936</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1277755501737127936',
    created: 1593475625000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gedamore" rel="noopener noreferrer" target="_blank">@gedamore</a> That\'s not how specs work. All you\'d manage to do is create yet another definition of AsciiDoc. We need buy-in on the process for it to be trusted and accepted. And we cannot afford to splinter this community.<br><br>In reply to: <a href="https://x.com/gedamore/status/1277754784188002304" rel="noopener noreferrer" target="_blank">@gedamore</a> <span class="status">1277754784188002304</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1277755224644481024',
    created: 1593475559000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gedamore" rel="noopener noreferrer" target="_blank">@gedamore</a> I wouldn\'t have been working for the past year to establish a process, proposal, and a consortium of interested parties if I thought otherwise. This is literally what we are doing.<br><br>In reply to: <a href="https://x.com/gedamore/status/1277754365223161856" rel="noopener noreferrer" target="_blank">@gedamore</a> <span class="status">1277754365223161856</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1277754554432417792',
    created: 1593475399000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gedamore" rel="noopener noreferrer" target="_blank">@gedamore</a> And the design of AsciiDoc predates everyone who currently works on it and with it. So the only sensible thing to do is to work together to design what it will look like in the future.<br><br>In reply to: <a href="#1277754060712448001">1277754060712448001</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1277754060712448001',
    created: 1593475282000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gedamore" rel="noopener noreferrer" target="_blank">@gedamore</a> And I saw no other path forward for addressing this issue (as well as others) short of forming a working group and collaborating on a versioned spec. We simply cannot break documents without a common vision and a means to avoid doing so in the future.<br><br>In reply to: <a href="#1277753619354275840">1277753619354275840</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1277753619354275840',
    created: 1593475177000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gedamore" rel="noopener noreferrer" target="_blank">@gedamore</a> It\'s abundantly clear the main shortcoming that needs addressing is the use of regexp for the inline parser. This is well known &amp; well stated. But AsciiDoc had very humble beginnings. No one could have imagined it would receive the adoption it has. An understandable growing pain.<br><br>In reply to: <a href="https://x.com/gedamore/status/1276975505455976448" rel="noopener noreferrer" target="_blank">@gedamore</a> <span class="status">1276975505455976448</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1277752443304947712',
    created: 1593474896000,
    type: 'reply',
    text: '@TristanKildaire It\'s one of a handful of typographic text replacements that an AsciiDoc processor performs. See <a href="https://asciidoctor.org/docs/asciidoc-syntax-quick-reference/#text-replacement" rel="noopener noreferrer" target="_blank">asciidoctor.org/docs/asciidoc-syntax-quick-reference/#text-replacement</a><br><br>In reply to: @TristanKildaire <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1277751972947308545',
    created: 1593474784000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Tuttieeeee" rel="noopener noreferrer" target="_blank">@Tuttieeeee</a> That\'s precisely why the AsciiDoc WG has been formed.<br><br>In reply to: <a href="https://x.com/whitphx_ja/status/1277182266918047745" rel="noopener noreferrer" target="_blank">@whitphx_ja</a> <span class="status">1277182266918047745</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1277700729344364544',
    created: 1593462567000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jennifercord" rel="noopener noreferrer" target="_blank">@jennifercord</a> <a class="mention" href="https://x.com/BMcG1230" rel="noopener noreferrer" target="_blank">@BMcG1230</a> I\'m extremely encouraged to hear you\'re using the masks diligently. I want you &amp; your family to be safe. That\'s my #1 concern. For the distance I must travel, the only way I can guarantee that for you is if I\'m not there. This sucks, but we\'ll get through it.<br><br>In reply to: <a href="https://x.com/jennifercord/status/1277666766303494144" rel="noopener noreferrer" target="_blank">@jennifercord</a> <span class="status">1277666766303494144</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1277636502063689728',
    created: 1593447254000,
    type: 'reply',
    text: '@mistyhacks Exactly. I do not believe the policing system in it\'s current form can be fixed or reformed. To me, that means doing a hard reset and creating something with an entirely new mission to actually serve rather than to oppress.<br><br>In reply to: @mistyhacks <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1277635374278340608',
    created: 1593446985000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BMcG1230" rel="noopener noreferrer" target="_blank">@BMcG1230</a> Wise. Just tell them it\'s an act of love. And be sure to 😷.<br><br>In reply to: <a href="https://x.com/BMcG1230/status/1277610144159891456" rel="noopener noreferrer" target="_blank">@BMcG1230</a> <span class="status">1277610144159891456</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1277510233615552512',
    created: 1593417149000,
    type: 'post',
    text: 'My extended family on one side (spread all across the US) is getting together for a reunion in less than a month. I\'m staying at home to protect myself and others. The rest of my family is living in a different reality where there\'s no pandemic. Must be nice there.',
    likes: 20,
    retweets: 0
  },
  {
    id: '1277506551561940993',
    created: 1593416271000,
    type: 'quote',
    text: 'If I had a dollar &amp; square foot for every time I heard this. The exact opposite happened when I got older. I\'m now an unapologetic progressive &amp; socialist (because I actually care about others). I\'m mad as hell my educators conned me into believing this was somehow wrong.<br><br>Quoting: <a href="https://x.com/jessfromonline/status/1276884178076938241" rel="noopener noreferrer" target="_blank">@jessfromonline</a> <span class="status">1276884178076938241</span>',
    likes: 11,
    retweets: 2
  },
  {
    id: '1277500854195507200',
    created: 1593414913000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dcwoodruff" rel="noopener noreferrer" target="_blank">@dcwoodruff</a> <a class="mention" href="https://x.com/NewslineCO" rel="noopener noreferrer" target="_blank">@NewslineCO</a> Nice! Reminds me of Status Coup with a local focus. Just what I needed.<br><br>In reply to: <a href="https://x.com/dcwoodruff/status/1275844193039106048" rel="noopener noreferrer" target="_blank">@dcwoodruff</a> <span class="status">1275844193039106048</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1277500571679776770',
    created: 1593414845000,
    type: 'post',
    text: 'Thanks to <a class="mention" href="https://x.com/blueoxmusicfest" rel="noopener noreferrer" target="_blank">@blueoxmusicfest</a>, I was properly introduced to <a class="mention" href="https://x.com/mollytuttle" rel="noopener noreferrer" target="_blank">@mollytuttle</a>. She\'s a phenomenal singer / songwriter, a guitar virtuoso, and, best of all, a wonderful human. Just loving her music hard r/n.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1276642733034094593',
    created: 1593210321000,
    type: 'post',
    text: 'I endorse <a class="mention" href="https://x.com/romanoff2020" rel="noopener noreferrer" target="_blank">@romanoff2020</a> for US Senate representing Colorado (to unseat THE worse Senator in the nation). He\'s endorsed by <a class="mention" href="https://x.com/sunrisemvmt" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a>, which speaks volumes. But there\'s more. He shows up &amp; listens to constituents. I\'ve met him. He\'s the real deal. #Medicare4All #GreenNewDeal',
    likes: 0,
    retweets: 0,
    tags: ['medicare4all', 'greennewdeal']
  },
  {
    id: '1276638695177646081',
    created: 1593209358000,
    type: 'quote',
    text: 'Finally, real steps to resolve taxation w/o representation! There\'s nothing quite like taking on all the big and lingering problems at once. But while we\'re challenging the establishment, might as well. It\'s almost like the gov\'t could have been operating like this for years.<br><br>Quoting: <a href="https://x.com/Public_Citizen/status/1276590538054041600" rel="noopener noreferrer" target="_blank">@Public_Citizen</a> <span class="status">1276590538054041600</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1276417122877358080',
    created: 1593156531000,
    type: 'post',
    text: 'Same goes for Jefferson Davis, who was indicted for treason by the US federal courts. But it\'s not just about the treason charge itself, but rather the crime against humanity he defended in earning it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1276414265490989056',
    created: 1593155850000,
    type: 'post',
    text: 'No school in the US named Robert E. Lee should be allowed to reopen until it\'s renamed &amp; an apology is posted on the school site for glorifying a defender of slavery, racist, &amp; traitor to this country. He\'s a loser who deserves absolutely no respect from the country he betrayed.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1276410503393275904',
    created: 1593154953000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iphigenie" rel="noopener noreferrer" target="_blank">@iphigenie</a> Refined GitHub has proven to me to be extremely trustworthy. But I absolutely understand the importance of vetting the extension yourself and not taking my word for it. 👍<br><br>In reply to: <a href="https://x.com/iphigenie/status/1276409185744420865" rel="noopener noreferrer" target="_blank">@iphigenie</a> <span class="status">1276409185744420865</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1276407779935154176',
    created: 1593154303000,
    type: 'post',
    text: 'They don\'t even hide it! Right on the school site they embrace their link to the Confederacy &amp; plantations. But note when the school got its name. 1964. The same year the Civil Rights Act passed. This is 100% racism + anti-US sentiment through &amp; through. <a href="https://leehs.fcps.edu/about/school-history" rel="noopener noreferrer" target="_blank">leehs.fcps.edu/about/school-history</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1276096713938268160',
    created: 1593080139000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/djjazzyjeff215" rel="noopener noreferrer" target="_blank">@djjazzyjeff215</a> You\'re a good man. I grew up listening to your spins and signature scratches, and now I\'m two steppin\' to your magnificent house party in my kitchen each night to help get me through this pandemic. Thanks for keeping us sane &amp; taking time to pause for the cause!',
    likes: 0,
    retweets: 0
  },
  {
    id: '1276095475167322112',
    created: 1593079844000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> Neither did I, and I even have family from there who I visited many times. Sure enough, it\'s right there in the first sentence of the Wikipedia page. This change is a no-brainer.<br><br>In reply to: <a href="https://x.com/starbuxman/status/1276079349590966273" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">1276079349590966273</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1276048351100694528',
    created: 1593068609000,
    type: 'post',
    text: 'It turns out, this only works if you have the Refined GitHub extension installed. But then, why would you not?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1276046593024647168',
    created: 1593068190000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joaompinto4" rel="noopener noreferrer" target="_blank">@joaompinto4</a> Thanks João!<br><br>In reply to: <a href="https://x.com/joaompinto4/status/1275898997719347204" rel="noopener noreferrer" target="_blank">@joaompinto4</a> <span class="status">1275898997719347204</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1276029540070944769',
    created: 1593064124000,
    type: 'post',
    text: 'Doesn\'t seem to work on mobile.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1275908503727534080',
    created: 1593035267000,
    type: 'quote',
    text: 'Don\'t limit your outrage to the murders of George Floyd and Breonna Taylor. They are not alone. #ElijahMcClain<br><br>Quoting: <a href="https://x.com/StringerJourno/status/1275897221397929984" rel="noopener noreferrer" target="_blank">@StringerJourno</a> <span class="status">1275897221397929984</span>',
    likes: 0,
    retweets: 0,
    tags: ['elijahmcclain']
  },
  {
    id: '1275903659189002241',
    created: 1593034112000,
    type: 'post',
    text: 'If you append #L0 to the URL of an AsciiDoc file on GitHub (or any line number range), the site will display the AsciiDoc source by default instead of the rendered HTML. For example: <a href="https://github.com/asciidoctor/asciidoctor/blob/master/README.adoc#L0" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor/blob/master/README.adoc#L0</a><br><br>(There\'s also a toggle button in the toolbar.)',
    likes: 4,
    retweets: 1,
    tags: ['l0']
  },
  {
    id: '1275878567792963584',
    created: 1593028129000,
    type: 'quote',
    text: 'The Eclipse Foundation now runs a GitLab instance. Exciting stuff going on there!<br><br>Quoting: <a href="https://x.com/mmilinkov/status/1275876175630516225" rel="noopener noreferrer" target="_blank">@mmilinkov</a> <span class="status">1275876175630516225</span>',
    likes: 7,
    retweets: 1
  },
  {
    id: '1275878144751267840',
    created: 1593028029000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mmilinkov" rel="noopener noreferrer" target="_blank">@mmilinkov</a> <a class="mention" href="https://x.com/EclipseFdn" rel="noopener noreferrer" target="_blank">@EclipseFdn</a> <a class="mention" href="https://x.com/gitlab" rel="noopener noreferrer" target="_blank">@gitlab</a> YAS!!<br><br>In reply to: <a href="https://x.com/mmilinkov/status/1275876175630516225" rel="noopener noreferrer" target="_blank">@mmilinkov</a> <span class="status">1275876175630516225</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1275676875612999682',
    created: 1592980042000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BenJealous" rel="noopener noreferrer" target="_blank">@BenJealous</a> <a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a> I would expect nothing less. Not only the right side, but the side of wisdom too.<br><br>In reply to: <a href="https://x.com/BenJealous/status/1275665121243774979" rel="noopener noreferrer" target="_blank">@BenJealous</a> <span class="status">1275665121243774979</span>',
    likes: 10,
    retweets: 0
  },
  {
    id: '1275672991851065345',
    created: 1592979116000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lanceball" rel="noopener noreferrer" target="_blank">@lanceball</a> Bowman and AOC crushed their opponents, so I\'m going to let that lift my spirits.<br><br>In reply to: <a href="https://x.com/lanceball/status/1275638747829329920" rel="noopener noreferrer" target="_blank">@lanceball</a> <span class="status">1275638747829329920</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1275520816109813761',
    created: 1592942835000,
    type: 'reply',
    text: '@janakaSteph I don\'t want to totally rain on the change since we have been calling for a responsive design for a long time. But we\'re all very stressed right now and this came without warning. It\'s just not the right time.<br><br>In reply to: <a href="https://x.com/janaka___/status/1275520065996238849" rel="noopener noreferrer" target="_blank">@janaka___</a> <span class="status">1275520065996238849</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1275504666139394052',
    created: 1592938984000,
    type: 'quote',
    text: 'Let\'s rock this country. Vote <a class="mention" href="https://x.com/JamaalBowmanNY" rel="noopener noreferrer" target="_blank">@JamaalBowmanNY</a> and <a class="mention" href="https://x.com/Booker4KY" rel="noopener noreferrer" target="_blank">@Booker4KY</a>.<br><br>Quoting: <a href="https://x.com/ACLU/status/1275432883830824965" rel="noopener noreferrer" target="_blank">@ACLU</a> <span class="status">1275432883830824965</span>',
    likes: 0,
    retweets: 1
  },
  {
    id: '1275486273352990721',
    created: 1592934599000,
    type: 'post',
    text: 'Great, on top of everything that\'s going on, I also have to get used to a new GitHub interface.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1275140377641578497',
    created: 1592852131000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/XDetant" rel="noopener noreferrer" target="_blank">@XDetant</a> <a class="mention" href="https://x.com/c089" rel="noopener noreferrer" target="_blank">@c089</a> At first I replied to the wrong post. Although I never mentioned unit testing, I agree the responses are revealing a lot of misconceptions about them and tests in general.<br><br>In reply to: <a href="https://x.com/XDetant/status/1275030919309598723" rel="noopener noreferrer" target="_blank">@XDetant</a> <span class="status">1275030919309598723</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1275139163650527232',
    created: 1592851842000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/akorver" rel="noopener noreferrer" target="_blank">@akorver</a> I never said this was about unit testing. I said testing, period. And the percentage does matter because parts that are missed are not tested, and that\'s where I found errors.<br><br>In reply to: <a href="https://x.com/akorver/status/1275065871635869696" rel="noopener noreferrer" target="_blank">@akorver</a> <span class="status">1275065871635869696</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1275022720460152833',
    created: 1592824079000,
    type: 'post',
    text: 'The bucket list is quickly being supplanted by the 2021 list.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1274988585251246080',
    created: 1592815941000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/akorver" rel="noopener noreferrer" target="_blank">@akorver</a> a) I found real problems, so it was not a fruitless exercise and b) I acknowledged allowing sensible exemptions, such as accessors (which are excluded in my coverage report anyway)<br><br>In reply to: <a href="#1274899766405562368">1274899766405562368</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1274987439925284864',
    created: 1592815668000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/philip368320" rel="noopener noreferrer" target="_blank">@philip368320</a> But the tests are not the problem because they are telling you whether the code works. They\'re part of the application. This needs to be taken into account when making estimates. It\'s part of software development. Blaming the tests is the wrong way of looking at it.<br><br>In reply to: <a href="https://x.com/philip368320/status/1274983206798807040" rel="noopener noreferrer" target="_blank">@philip368320</a> <span class="status">1274983206798807040</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1274973811490742278',
    created: 1592812419000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/philip368320" rel="noopener noreferrer" target="_blank">@philip368320</a> The problematic assumption here is that tests cannot change. That\'s bizarre. Of course tests have to change if the behavior of the application changes. You replace them with new assertions. When it\'s all said and done, you need tests to exercise the code.<br><br>In reply to: <a href="https://x.com/philip368320/status/1274720453894959105" rel="noopener noreferrer" target="_blank">@philip368320</a> <span class="status">1274720453894959105</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1274972901683294214',
    created: 1592812202000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RaviBitragunta" rel="noopener noreferrer" target="_blank">@RaviBitragunta</a> This has nothing to do with unit or integration tests. The question is whether the code ever gets executed in a test environment. If not, there\'s no guarantee it works.<br><br>In reply to: <a href="https://x.com/RaviBitragunta/status/1274971347093876736" rel="noopener noreferrer" target="_blank">@RaviBitragunta</a> <span class="status">1274971347093876736</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1274899766405562368',
    created: 1592794765000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/akorver" rel="noopener noreferrer" target="_blank">@akorver</a> I think you misread my post.<br><br>In reply to: <a href="https://x.com/akorver/status/1274898594030653440" rel="noopener noreferrer" target="_blank">@akorver</a> <span class="status">1274898594030653440</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1274635004912394241',
    created: 1592731641000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/philip368320" rel="noopener noreferrer" target="_blank">@philip368320</a> I maintain enormous test suites and have done very large rewrites. I have never found tests to be difficult to rework. If anything, they guide me, show me what to look out for, and help me understand when I\'m done.<br><br>In reply to: <a href="https://x.com/philip368320/status/1274634346486362112" rel="noopener noreferrer" target="_blank">@philip368320</a> <span class="status">1274634346486362112</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1274634632693084161',
    created: 1592731552000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/philip368320" rel="noopener noreferrer" target="_blank">@philip368320</a> I find that the tests are the only reason I can refactor.<br><br>In reply to: <a href="https://x.com/philip368320/status/1274634346486362112" rel="noopener noreferrer" target="_blank">@philip368320</a> <span class="status">1274634346486362112</span>',
    likes: 5,
    retweets: 0
  },
  {
    id: '1274633087402381312',
    created: 1592731184000,
    type: 'post',
    text: 'Asciidoctor PDF has now reached 95% test coverage. On almost every line that was not previously covered, I discovered some sort of problem with the logic. I\'m now a firm and steadfast believer in complete test coverage (with sensible exemptions).',
    likes: 65,
    retweets: 12
  },
  {
    id: '1274401449712037888',
    created: 1592675957000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/connolly_s" rel="noopener noreferrer" target="_blank">@connolly_s</a> <a class="mention" href="https://x.com/rustlang" rel="noopener noreferrer" target="_blank">@rustlang</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/EclipseFdn" rel="noopener noreferrer" target="_blank">@EclipseFdn</a> That\'s a key indicator of success, in fact. Doing so before there\'s a spec would almost certainly guarantee divergence. But we don\'t have to fret about that any longer.<br><br>In reply to: <a href="https://x.com/connolly_s/status/1274266567266754565" rel="noopener noreferrer" target="_blank">@connolly_s</a> <span class="status">1274266567266754565</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1274400996748259328',
    created: 1592675849000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/davinkevin" rel="noopener noreferrer" target="_blank">@davinkevin</a> <a class="mention" href="https://x.com/bmathus" rel="noopener noreferrer" target="_blank">@bmathus</a> <a class="mention" href="https://x.com/gitlab" rel="noopener noreferrer" target="_blank">@gitlab</a> Support for AsciiDoc on GitLab is a) community-driven and b) supports includes and diagrams. So not sadly at all.<br><br>In reply to: <a href="https://x.com/davinkevin/status/1274345268775989253" rel="noopener noreferrer" target="_blank">@davinkevin</a> <span class="status">1274345268775989253</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1274251584428371968',
    created: 1592640226000,
    type: 'post',
    text: 'Lift ev\'ry voice and sing<br>...<br>Sing a song full of the faith that the dark past has taught us,<br>Sing a song full of the hope that the present has brought us;<br>Facing the rising sun of our new day begun,<br>Let us march on till victory is won.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1274250200257720322',
    created: 1592639896000,
    type: 'post',
    text: 'I was actually thinking of "Lift Every Voice and Sing", the Black national anthem. Though I heard and became conscious of both songs today.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1274247158221492224',
    created: 1592639171000,
    type: 'post',
    text: 'I\'m ashamed to say I only became conscious of the spiritual hymn “Oh Freedom” and its significance today, and I heard it sung multiple times. It\'s impossible to say I\'ve never heard it, but now I can identify it. I still have a lot of growing to do.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1274243436556595200',
    created: 1592638284000,
    type: 'post',
    text: 'Justice delayed is justice denied. It\'s not enough for justice to eventually be served.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1274238444944818176',
    created: 1592637094000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ghillert" rel="noopener noreferrer" target="_blank">@ghillert</a> <a class="mention" href="https://x.com/GoHugoIO" rel="noopener noreferrer" target="_blank">@GoHugoIO</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> While I agree it\'s a nice tool, I\'d argue <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> is the best option for generating site content from AsciiDoc using Asciidoctor. (It\'s more than just my bias. It was designed specifically for AsciiDoc-based content).<br><br>In reply to: <a href="https://x.com/ghillert/status/1273747442861658112" rel="noopener noreferrer" target="_blank">@ghillert</a> <span class="status">1273747442861658112</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1274165993795809280',
    created: 1592619820000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RBReich" rel="noopener noreferrer" target="_blank">@RBReich</a> Though I would argue we should also have an Indigenous Peoples\' Day. So let\'s restore one part of history while erasing another.<br><br>In reply to: <a href="https://x.com/RBReich/status/1274047182421794816" rel="noopener noreferrer" target="_blank">@RBReich</a> <span class="status">1274047182421794816</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1274165178007908354',
    created: 1592619625000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/netskymusic" rel="noopener noreferrer" target="_blank">@netskymusic</a> I have it on good account that you\'re a national treasure in Belgium.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1274143448170328064',
    created: 1592614445000,
    type: 'post',
    text: 'Listening to &amp; learning from this tremendously moving performance by <a class="mention" href="https://x.com/SHoney73" rel="noopener noreferrer" target="_blank">@SHoney73</a> on &amp; about this #JUNETEENTH2020. <a href="https://sweethoneyintherock.org/juneteenth" rel="noopener noreferrer" target="_blank">sweethoneyintherock.org/juneteenth</a>',
    likes: 0,
    retweets: 0,
    tags: ['juneteenth2020']
  },
  {
    id: '1274113515461070848',
    created: 1592607308000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/johnmark" rel="noopener noreferrer" target="_blank">@johnmark</a> No doubt. The reason I would make a case for Juneteenth is because that\'s the ask being made by the Black community and we should honor respect its significance to them. After all, it\'s about them first and foremost.<br><br>In reply to: <a href="https://x.com/johnmark/status/1274096228784111620" rel="noopener noreferrer" target="_blank">@johnmark</a> <span class="status">1274096228784111620</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1274109309580742657',
    created: 1592606305000,
    type: 'post',
    text: 'The US government needs to get off the couch and make #juneteenth a national holiday, then make all forms of voter suppression illegal, and hold any Secretary of State that allows it to happen fully accountable.',
    likes: 2,
    retweets: 1,
    tags: ['juneteenth']
  },
  {
    id: '1274089657987264512',
    created: 1592601620000,
    type: 'post',
    text: 'I became aware of the day during the Bernie 2016 campaign, but this is the first year I\'ve really paused to honor and reflect on it. And I will continue to do so from here on out.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1274087521496883201',
    created: 1592601111000,
    type: 'quote',
    text: 'This is an example of being antiracist. You refuse to let institutions continue to be comfortable with it. (For context, the state flag of Mississippi bears the Confederate army\'s battle flag, a symbol of slavery).<br><br>Quoting: <a href="https://x.com/SEC/status/1273759965329731585" rel="noopener noreferrer" target="_blank">@SEC</a> <span class="status">1273759965329731585</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1274085485476868096',
    created: 1592600625000,
    type: 'post',
    text: '“People like the term \'not racist\' because it means they don\'t have to do anything.” — <a class="mention" href="https://x.com/Dribram" rel="noopener noreferrer" target="_blank">@Dribram</a> in drawing the strong distinction between not racist and antiracist.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1274085039433646080',
    created: 1592600519000,
    type: 'quote',
    text: 'Huge shut out to <a class="mention" href="https://x.com/salesforce" rel="noopener noreferrer" target="_blank">@salesforce</a> &amp; <a class="mention" href="https://x.com/BoldForce" rel="noopener noreferrer" target="_blank">@BoldForce</a> for hosting <a class="mention" href="https://x.com/Dribram" rel="noopener noreferrer" target="_blank">@Dribram</a> on #JUNETEENTH2020 to share with employees and contractors about how to be an antiracist. Diversity statements are not enough. Companies need to be antiracist, and Salesforce is one walking the walk.<br><br>Quoting: <a href="https://x.com/BoldForce/status/1274012493266989056" rel="noopener noreferrer" target="_blank">@BoldForce</a> <span class="status">1274012493266989056</span>',
    likes: 0,
    retweets: 0,
    tags: ['juneteenth2020']
  },
  {
    id: '1274083864504184832',
    created: 1592600239000,
    type: 'post',
    text: 'I\'m spending #JUNETEENTH2020 educating myself more about racism &amp; its impact, reflecting on how I\'ve been racist (even if I didn\'t mean to, doesn\'t matter), &amp; learning how to be a stronger ally to eradicate racism. I recognize this as national holiday, even if my gov\'t doesn\'t.',
    likes: 11,
    retweets: 0,
    tags: ['juneteenth2020']
  },
  {
    id: '1274082087138521088',
    created: 1592599815000,
    type: 'post',
    text: 'The day slavery was ended in the US is not a recognized Federal holiday, yet Columbus Day is. There\'s something severely wrong with that. What\'s more, I was never taught about this day in grade school, or in any other context for that matter. #JUNETEENTH2020 #JuneteenthDay',
    likes: 15,
    retweets: 5,
    tags: ['juneteenth2020', 'juneteenthday']
  },
  {
    id: '1273872661915959297',
    created: 1592549884000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/KillerMike" rel="noopener noreferrer" target="_blank">@KillerMike</a> Do it. We\'re all in full sponge mode r/n, so there\'s never been a better opportunity to drop some truth and reconciliation.<br><br>In reply to: <a href="https://x.com/KillerMike/status/1273708949188096021" rel="noopener noreferrer" target="_blank">@KillerMike</a> <span class="status">1273708949188096021</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1273692801344012288',
    created: 1592507002000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/eckes" rel="noopener noreferrer" target="_blank">@eckes</a> <a class="mention" href="https://x.com/carbonfray" rel="noopener noreferrer" target="_blank">@carbonfray</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> It\'s been a long road to get to this point and we\'re only about to get underway.<br><br>In reply to: <a href="https://x.com/eckes/status/1273676831250620425" rel="noopener noreferrer" target="_blank">@eckes</a> <span class="status">1273676831250620425</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1273553292828655620',
    created: 1592473741000,
    type: 'post',
    text: 'Here\'s another book generated with Asciidoctor PDF. I love that Asciidoctor PDF empowers authors to self-publish. That\'s what we\'re all about. <a href="https://crascit.com/professional-cmake/" rel="noopener noreferrer" target="_blank">crascit.com/professional-cmake/</a>',
    likes: 12,
    retweets: 1
  },
  {
    id: '1273374628908380161',
    created: 1592431144000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> 🎉<br><br>In reply to: <a href="https://x.com/abelsromero/status/1273374335013654534" rel="noopener noreferrer" target="_blank">@abelsromero</a> <span class="status">1273374335013654534</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1272847507379220481',
    created: 1592305468000,
    type: 'quote',
    text: 'Allies in Kentucky, put your vote where your tweet is. #BlackLivesMatter<br><br>Quoting: <a href="https://x.com/sunrisemvmt/status/1272844416659722241" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a> <span class="status">1272844416659722241</span>',
    likes: 0,
    retweets: 0,
    tags: ['blacklivesmatter']
  },
  {
    id: '1272622083583827968',
    created: 1592251723000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JoeCyberGuru" rel="noopener noreferrer" target="_blank">@JoeCyberGuru</a> Dan\'s words, not mine ;) Though he does go on to say that the application should ideally be rewritten not to require any sort of privilege escalation. Baby steps I suppose.<br><br>In reply to: <a href="https://x.com/JoeCyberGuru/status/1272619040427479040" rel="noopener noreferrer" target="_blank">@JoeCyberGuru</a> <span class="status">1272619040427479040</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1272616007173980161',
    created: 1592250274000,
    type: 'quote',
    text: '"As a security engineer, I do not like users running with the --privileged mode. I wish they would figure out what privileges their container requires and run with as much security as possible."<br><br>Quoting: <a href="https://x.com/bobbyjohnstx/status/1272212206260752384" rel="noopener noreferrer" target="_blank">@bobbyjohnstx</a> <span class="status">1272212206260752384</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1272574893968642051',
    created: 1592240472000,
    type: 'quote',
    text: 'Superb! ⚖️<br><br>Quoting: <a href="https://x.com/BernieSanders/status/1272542452298272774" rel="noopener noreferrer" target="_blank">@BernieSanders</a> <span class="status">1272542452298272774</span>',
    likes: 5,
    retweets: 1
  },
  {
    id: '1272573514755657728',
    created: 1592240143000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/netskymusic" rel="noopener noreferrer" target="_blank">@netskymusic</a> <a class="mention" href="https://x.com/tomorrowland" rel="noopener noreferrer" target="_blank">@tomorrowland</a> And good luck!<br><br>In reply to: <a href="#1272572061798395904">1272572061798395904</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1272572061798395904',
    created: 1592239797000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/netskymusic" rel="noopener noreferrer" target="_blank">@netskymusic</a> <a class="mention" href="https://x.com/tomorrowland" rel="noopener noreferrer" target="_blank">@tomorrowland</a> Oh hell yeah! Can\'t happen soon enough. 😄<br><br>In reply to: <a href="https://x.com/netskymusic/status/1272529778499891207" rel="noopener noreferrer" target="_blank">@netskymusic</a> <span class="status">1272529778499891207</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1272411167911100422',
    created: 1592201437000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/IsomorphicGit" rel="noopener noreferrer" target="_blank">@IsomorphicGit</a> There\'s also a good argument I\'ve seen for main being sympathetic to autocomplete habits because it also starts with "ma".<br><br>In reply to: <a href="https://x.com/IsomorphicGit/status/1272410265942974465" rel="noopener noreferrer" target="_blank">@IsomorphicGit</a> <span class="status">1272410265942974465</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1272406128870035456',
    created: 1592200235000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/IsomorphicGit" rel="noopener noreferrer" target="_blank">@IsomorphicGit</a> I missed that this poll was specifically for <a class="mention" href="https://x.com/IsomorphicGit" rel="noopener noreferrer" target="_blank">@IsomorphicGit</a>. (I was thinking of the name in a broader context, where master isn\'t always stable). In this case, the term \'stable\' makes much more sense. Though, as you said, there\'s no other branch.<br><br>In reply to: <a href="https://x.com/IsomorphicGit/status/1272404643306487808" rel="noopener noreferrer" target="_blank">@IsomorphicGit</a> <span class="status">1272404643306487808</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1272401493576966144',
    created: 1592199130000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cenkuygur" rel="noopener noreferrer" target="_blank">@cenkuygur</a> <a class="mention" href="https://x.com/staceyabrams" rel="noopener noreferrer" target="_blank">@staceyabrams</a> Nah. #DefundThePolice<br><br>In reply to: <a href="https://x.com/cenkuygur/status/1272253260510818304" rel="noopener noreferrer" target="_blank">@cenkuygur</a> <span class="status">1272253260510818304</span>',
    likes: 0,
    retweets: 0,
    tags: ['defundthepolice']
  },
  {
    id: '1272395567310630912',
    created: 1592197717000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/IsomorphicGit" rel="noopener noreferrer" target="_blank">@IsomorphicGit</a> I don\'t get why people vote for \'stable\'. The master branch is rarely stable (in a production sense). How can something be stable when it has the freshest code that\'s not yet released?<br><br>In reply to: <a href="https://x.com/IsomorphicGit/status/1272356692202786821" rel="noopener noreferrer" target="_blank">@IsomorphicGit</a> <span class="status">1272356692202786821</span>',
    likes: 6,
    retweets: 0
  },
  {
    id: '1272091050668994562',
    created: 1592125115000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/thewarrenhaynes" rel="noopener noreferrer" target="_blank">@thewarrenhaynes</a> <a class="mention" href="https://x.com/govtmuleband" rel="noopener noreferrer" target="_blank">@govtmuleband</a> <a class="mention" href="https://x.com/blueoxmusicfest" rel="noopener noreferrer" target="_blank">@blueoxmusicfest</a> You reached my soul. 🎸🎙<br><br>In reply to: <a href="https://x.com/thewarrenhaynes/status/1270770675213643776" rel="noopener noreferrer" target="_blank">@thewarrenhaynes</a> <span class="status">1270770675213643776</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1272088408114950144',
    created: 1592124485000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nicolas_frankel" rel="noopener noreferrer" target="_blank">@nicolas_frankel</a> <a class="mention" href="https://x.com/g_scheibel" rel="noopener noreferrer" target="_blank">@g_scheibel</a> I\'m not sure what you mean by "write HTML". Can you elaborate?<br><br>In reply to: <a href="https://x.com/nicolas_frankel/status/1271901019350450176" rel="noopener noreferrer" target="_blank">@nicolas_frankel</a> <span class="status">1271901019350450176</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1272088190376005632',
    created: 1592124433000,
    type: 'post',
    text: 'Thanks to the <a class="mention" href="https://x.com/blueoxmusicfest" rel="noopener noreferrer" target="_blank">@blueoxmusicfest</a>, I discovered Warren Haynes and the songs of Gov\'t Mule. Killer performance. How did this music never hit my radar? We\'ll, I\'m just glad I can enjoy it now.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1272024959901003777',
    created: 1592109358000,
    type: 'post',
    text: 'Thank you, thank you, thank you <a class="mention" href="https://x.com/blueoxmusicfest" rel="noopener noreferrer" target="_blank">@blueoxmusicfest</a> for making our weekend!',
    likes: 2,
    retweets: 0
  },
  {
    id: '1272024710855835648',
    created: 1592109298000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> Trying our best to hold on. Growing plants, making food, and doing yoga. Nothing brightens my day like a new 🌱 emerging.<br><br>In reply to: <a href="https://x.com/starbuxman/status/1272020066104561665" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">1272020066104561665</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1271898964971753473',
    created: 1592079318000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nicolas_frankel" rel="noopener noreferrer" target="_blank">@nicolas_frankel</a> <a class="mention" href="https://x.com/g_scheibel" rel="noopener noreferrer" target="_blank">@g_scheibel</a> Enclose the value in single quotes. That enables substitutions (aka interpolation).<br><br>In reply to: <a href="https://x.com/nicolas_frankel/status/1271897808627544065" rel="noopener noreferrer" target="_blank">@nicolas_frankel</a> <span class="status">1271897808627544065</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1271877440244076544',
    created: 1592074186000,
    type: 'reply',
    text: '@lenazun It took me 10 years to realize this. Now I take my daily stretching / yoga very seriously and I feel SO much better. Things are working again as they should, like leaning down.<br><br>In reply to: @lenazun <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1271741487986864128',
    created: 1592041773000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iphigenie" rel="noopener noreferrer" target="_blank">@iphigenie</a> I think the redirect went down because they\'re putting up a full website. This might be the transition period.<br><br>In reply to: <a href="#1271740848342892545">1271740848342892545</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1271740848342892545',
    created: 1592041620000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iphigenie" rel="noopener noreferrer" target="_blank">@iphigenie</a> The redirect seems to have gone down. Here\'s the direct link: <a href="https://docs.google.com/document/d/1Z9fwkhrFiDgNCEwz8Lw5WvxCZBKBc6D7UBHyxaqdtms" rel="noopener noreferrer" target="_blank">docs.google.com/document/d/1Z9fwkhrFiDgNCEwz8Lw5WvxCZBKBc6D7UBHyxaqdtms</a><br><br>In reply to: <a href="https://x.com/iphigenie/status/1271739573882806272" rel="noopener noreferrer" target="_blank">@iphigenie</a> <span class="status">1271739573882806272</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1271739949423800320',
    created: 1592041406000,
    type: 'post',
    text: 'I\'ve never heard a clearer definition of neoliberalism (and it\'s racist underpinnings and implications) than the one presented by Angela Davis in this <a class="mention" href="https://x.com/democracynow" rel="noopener noreferrer" target="_blank">@democracynow</a> interview. <a href="https://youtu.be/8ebWFnGWOaA?t=262" rel="noopener noreferrer" target="_blank">youtu.be/8ebWFnGWOaA?t=262</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1271581445132185600',
    created: 1592003616000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/waynebeaton" rel="noopener noreferrer" target="_blank">@waynebeaton</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/revealjs" rel="noopener noreferrer" target="_blank">@revealjs</a> I\'m getting ahead of myself, but I\'m so looking forward to propose a subcommittee that focuses on AsciiDoc for presentations. I think there is so much still to explore.<br><br>In reply to: <a href="https://x.com/waynebeaton/status/1271547559408996354" rel="noopener noreferrer" target="_blank">@waynebeaton</a> <span class="status">1271547559408996354</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1271380647085367296',
    created: 1591955742000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/recena" rel="noopener noreferrer" target="_blank">@recena</a> <a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> I\'m using the term workflow here to mean how you work with git.<br><br>In reply to: <a href="https://x.com/recena/status/1271378429917777920" rel="noopener noreferrer" target="_blank">@recena</a> <span class="status">1271378429917777920</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1271377477915426816',
    created: 1591954986000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/benignbala" rel="noopener noreferrer" target="_blank">@benignbala</a> <a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> base is definitely worth considering. 👍<br><br>In reply to: <a href="https://x.com/benignbala/status/1271377215226122243" rel="noopener noreferrer" target="_blank">@benignbala</a> <span class="status">1271377215226122243</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1271377226030694400',
    created: 1591954926000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jholusa" rel="noopener noreferrer" target="_blank">@jholusa</a> <a class="mention" href="https://x.com/docusaurus" rel="noopener noreferrer" target="_blank">@docusaurus</a> Then you\'ll really like Antora. <a href="https://docs.antora.org" rel="noopener noreferrer" target="_blank">docs.antora.org</a><br><br>In reply to: <a href="https://x.com/jholusa/status/1271370323112460291" rel="noopener noreferrer" target="_blank">@jholusa</a> <span class="status">1271370323112460291</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1271374445026791424',
    created: 1591954263000,
    type: 'quote',
    text: 'This is a clear-cut example of why this is not just a US issue.<br><br>Quoting: <a href="https://x.com/vanessa_vash/status/1268430637800792066" rel="noopener noreferrer" target="_blank">@vanessa_vash</a> <span class="status">1268430637800792066</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1271372878760382466',
    created: 1591953889000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/recena" rel="noopener noreferrer" target="_blank">@recena</a> <a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> And you\'re absolutely free to do so...the great thing about branches. But if we call the default/main/trunk branch "stable", then that imposes a meaning which simply may not be true. So this branch needs to have a name that\'s fairly workflow agnostic.<br><br>In reply to: <a href="https://x.com/recena/status/1271372082207690756" rel="noopener noreferrer" target="_blank">@recena</a> <span class="status">1271372082207690756</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1271371928876400641',
    created: 1591953663000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> <a class="mention" href="https://x.com/thokuest" rel="noopener noreferrer" target="_blank">@thokuest</a> The problem with roots and trees is that really describes the contents (blobs and folder hierarchies). So we\'d really be mixing metaphors.<br><br>In reply to: <a href="https://x.com/brunoborges/status/1271305299748687872" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1271305299748687872</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1271371531998736384',
    created: 1591953568000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/recena" rel="noopener noreferrer" target="_blank">@recena</a> <a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> I think that assumes to much about workflow. In many projects, the main/trunk branch is actually unstable (perhaps even experimental) because that\'s where all the new stuff is landing. The stable code may live in branches, like v1.x, v2.x, etc.<br><br>In reply to: <a href="https://x.com/recena/status/1271304999511916544" rel="noopener noreferrer" target="_blank">@recena</a> <span class="status">1271304999511916544</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1271370968045252609',
    created: 1591953434000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> I\'m voting main, but I totally git why trunk makes sense and wouldn\'t object to it.<br><br>In reply to: <a href="https://x.com/brunoborges/status/1271297813884751873" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1271297813884751873</span>',
    likes: 0,
    retweets: 1
  },
  {
    id: '1271207652811436034',
    created: 1591914496000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Una" rel="noopener noreferrer" target="_blank">@Una</a> <a class="mention" href="https://x.com/github" rel="noopener noreferrer" target="_blank">@github</a> I\'m on board.<br><br>In reply to: <a href="https://x.com/Una/status/1271180494944829441" rel="noopener noreferrer" target="_blank">@Una</a> <span class="status">1271180494944829441</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1271206653635948544',
    created: 1591914258000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ryanbigg" rel="noopener noreferrer" target="_blank">@ryanbigg</a> I kind of feel like it needs a resolution clause (to remind us not to panic). Naming Things is Hard, But Breathe. ;)<br><br>In reply to: <a href="https://x.com/ryanbigg/status/1271204847539286018" rel="noopener noreferrer" target="_blank">@ryanbigg</a> <span class="status">1271204847539286018</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1271193496423022595',
    created: 1591911121000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> <a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> (I\'m referring specifically to user-facing configuration. Machine-to-machine communication is something entirely different, where JSON fits very well).<br><br>In reply to: <a href="#1271192876412633088">1271192876412633088</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1271192876412633088',
    created: 1591910974000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> <a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> Interesting. I do think TOML has a chance as a substitute for YAML. There\'s just no way users are going to go for JSON, so we really need something in between (or entirely new, I really don\'t care).<br><br>In reply to: <a href="https://x.com/aalmiray/status/1271191032076648448" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">1271191032076648448</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1271190202858344448',
    created: 1591910336000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> <a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> And does it?<br><br>In reply to: <a href="https://x.com/brunoborges/status/1271189279880945665" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1271189279880945665</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1270990077087281152',
    created: 1591862622000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/0gust1" rel="noopener noreferrer" target="_blank">@0gust1</a> I support works of art ;)<br><br>In reply to: <a href="https://x.com/0gust1/status/1270989828759437312" rel="noopener noreferrer" target="_blank">@0gust1</a> <span class="status">1270989828759437312</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1270989678313799681',
    created: 1591862527000,
    type: 'reply',
    text: '@janakaSteph Outstanding point. We need to keep the rivers clean of all forms of pollution.<br><br>In reply to: <a href="https://x.com/janaka___/status/1270989014162714624" rel="noopener noreferrer" target="_blank">@janaka___</a> <span class="status">1270989014162714624</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1270988719932108801',
    created: 1591862299000,
    type: 'post',
    text: 'I remember being on a field trip (likely in MD or VA) as a young child where we encountered one of these. After reading the inscription, I thought to myself, "Why do we make statues for terrible people? That\'s messed up."',
    likes: 1,
    retweets: 0
  },
  {
    id: '1270986407629090816',
    created: 1591861748000,
    type: 'post',
    text: 'I fully support the removal of memorials to racist traitors from public grounds. There was ample opportunity to relocate these to museums, but apologists didn\'t feel they were worthy (I have to agree). It\'s time to take out the trash.',
    likes: 5,
    retweets: 0
  },
  {
    id: '1270983477421211649',
    created: 1591861049000,
    type: 'post',
    text: 'Before you object to the call to defund the police (or the words chosen), ask yourself why you\'re willing to stand with the force that was historically tasked to keep this society unequal (&amp; racially profiled). Inform yourself about the history of policing in this country.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1270655924575731714',
    created: 1591782954000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dberkholz" rel="noopener noreferrer" target="_blank">@dberkholz</a> The first half of this video by <a class="mention" href="https://x.com/krystalball" rel="noopener noreferrer" target="_blank">@krystalball</a> was recorded to address exactly your concern / confusion about #defundthepolice. I recommend hearing her out. <a href="https://youtu.be/xF-ODBmp0aQ" rel="noopener noreferrer" target="_blank">youtu.be/xF-ODBmp0aQ</a><br><br>In reply to: <a href="https://x.com/dberkholz/status/1270583761902735361" rel="noopener noreferrer" target="_blank">@dberkholz</a> <span class="status">1270583761902735361</span>',
    likes: 0,
    retweets: 0,
    tags: ['defundthepolice']
  },
  {
    id: '1270583890688630784',
    created: 1591765780000,
    type: 'quote',
    text: 'YES!!!!! This is fantastic news for the progressive movement and even better news for the people of WV, who have been depleted for decades by self-serving politicians. But she\'s not having any of it. Good luck in the general, Paula Jean!<br><br>Quoting: <a href="https://x.com/paulajean2020/status/1270557816055529472" rel="noopener noreferrer" target="_blank">@paulajean2020</a> <span class="status">1270557816055529472</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1270303676855271426',
    created: 1591698972000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Stephan007" rel="noopener noreferrer" target="_blank">@Stephan007</a> And I forgot to say, I\'ll miss seeing you!<br><br>In reply to: <a href="https://x.com/Stephan007/status/1270262031141216256" rel="noopener noreferrer" target="_blank">@Stephan007</a> <span class="status">1270262031141216256</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1270267631698718720',
    created: 1591690378000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Stephan007" rel="noopener noreferrer" target="_blank">@Stephan007</a> Given this is my favorite event of the year, with some of my favorite people in attendance, on stage, and, most importantly, behind the scenes, this stings. But it stings in the way disinfectant stings on a wound. It\'s a sting that begins the healing process. 🦀🥖🍻🤗<br><br>In reply to: <a href="https://x.com/Stephan007/status/1270262031141216256" rel="noopener noreferrer" target="_blank">@Stephan007</a> <span class="status">1270262031141216256</span>',
    likes: 11,
    retweets: 0
  },
  {
    id: '1270153346762981376',
    created: 1591663130000,
    type: 'quote',
    text: 'We\'ve defunded education for decades and still have teachers. Only seems fair to let the police play the underpaid role for a while.<br><br>Quoting: <a href="https://x.com/akilahgreen/status/1269738949263552512" rel="noopener noreferrer" target="_blank">@akilahgreen</a> <span class="status">1269738949263552512</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1270098055677612033',
    created: 1591649948000,
    type: 'post',
    text: 'Nothing allays the pain and frustration of a nation like bureaucrats bureaucrating. 🤦',
    likes: 2,
    retweets: 0
  },
  {
    id: '1269914161435140096',
    created: 1591606104000,
    type: 'post',
    text: 'I\'m unsure at this point whether I\'m learning to grow plants, or just to not kill them.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1269747011042959360',
    created: 1591566252000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/EricaJoy" rel="noopener noreferrer" target="_blank">@EricaJoy</a> I remember that too, vividly! It was my first memory of police and it has never sat well with me. Only as an adult do I truly understand why.<br><br>In reply to: <a href="https://x.com/EricaJoy/status/1269706464752316416" rel="noopener noreferrer" target="_blank">@EricaJoy</a> <span class="status">1269706464752316416</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1269554116717539332',
    created: 1591520263000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ysb33r" rel="noopener noreferrer" target="_blank">@ysb33r</a> That IS badass. I dig it.<br><br>In reply to: <a href="https://x.com/ysb33r/status/1268576288496922626" rel="noopener noreferrer" target="_blank">@ysb33r</a> <span class="status">1268576288496922626</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1269527316582903808',
    created: 1591513873000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iphigenie" rel="noopener noreferrer" target="_blank">@iphigenie</a> Indeed. I have a lot of reading to do. My first assignment from Cornell in two decades. Time to head to the (virtual) library!<br><br>In reply to: <a href="https://x.com/iphigenie/status/1269526783017324551" rel="noopener noreferrer" target="_blank">@iphigenie</a> <span class="status">1269526783017324551</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1269524423725350913',
    created: 1591513183000,
    type: 'reply',
    text: 'I am extremely proud of <a class="mention" href="https://x.com/Cornell" rel="noopener noreferrer" target="_blank">@Cornell</a> for opening space during the reunion for Black, Native, &amp; Asian alumni to share testimony about the crisis of racism, lynching, &amp; genocide that stretches all the way back to the founding of this country &amp; later the founding of the institution.<br><br>In reply to: <a href="#1269520659236786177">1269520659236786177</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1269522283078377477',
    created: 1591512673000,
    type: 'post',
    text: 'The last presenter shared a reading list to help us navigate the conversation with friends and family about race, social injustice, and the uprisings, and also ways we can help: <a href="https://notariot.com" rel="noopener noreferrer" target="_blank">notariot.com</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1269520659236786177',
    created: 1591512286000,
    type: 'post',
    text: 'For my 20th reunion from <a class="mention" href="https://x.com/Cornell" rel="noopener noreferrer" target="_blank">@Cornell</a>, I chose to attend the event “Mosaic: A Racial Justice Teach-In.” <a href="https://live.alumni.cornell.edu/reunion?v=5edc3c35e144f89f56370569" rel="noopener noreferrer" target="_blank">live.alumni.cornell.edu/reunion?v=5edc3c35e144f89f56370569</a><br><br>Cornellians I know will not be silent during this historical moment. They\'ll use their maverick voices to echo the call for justice &amp; to defund the police.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1269115352664834048',
    created: 1591415653000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/untappd" rel="noopener noreferrer" target="_blank">@untappd</a> Can you make a list of Black-own breweries in the app?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1269027458004611072',
    created: 1591394698000,
    type: 'post',
    text: 'At this point, I think the phrase should be "let the door hit you in the ass on the way out." The door knows what it\'s doing.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1269023354385608704',
    created: 1591393719000,
    type: 'quote',
    text: 'Protests in small towns have a huge impact. That\'s because they show just how universal support is for this movement, and how appalled Americans are about police brutality and militarization.<br><br>Quoting: <a href="https://x.com/annehelen/status/1268236391395127296" rel="noopener noreferrer" target="_blank">@annehelen</a> <span class="status">1268236391395127296</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1268825128382554112',
    created: 1591346458000,
    type: 'quote',
    text: 'Never forget that the freedoms you enjoy as citizen of the US you inherited because someone, somewhere protested to fight for them. In fact, our Constitution is the result of a protest for freedom. Think about that before you complain about protests being inconvenient to you.<br><br>Quoting: <a href="https://x.com/estellevw/status/1268796794856587264" rel="noopener noreferrer" target="_blank">@estellevw</a> <span class="status">1268796794856587264</span>',
    likes: 4,
    retweets: 1
  },
  {
    id: '1268656772073451520',
    created: 1591306319000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> Happy Birthday! I hope you have a good one. Ask you family to give you hugs from me. 🤗',
    likes: 1,
    retweets: 0
  },
  {
    id: '1268624759744413697',
    created: 1591298687000,
    type: 'post',
    text: 'Our very own community member <a class="mention" href="https://x.com/brunchboy" rel="noopener noreferrer" target="_blank">@brunchboy</a> wrote a column for the book 97 Things Every Java Programmer Should Know (1 of 2) about how to augment Javadoc with AsciiDoc. <a href="https://www.oreilly.com/library/view/97-things-every/9781491952689/ch03.html#augment_javadoc_with_asciidoc" rel="noopener noreferrer" target="_blank">www.oreilly.com/library/view/97-things-every/9781491952689/ch03.html#augment_javadoc_with_asciidoc</a>',
    likes: 5,
    retweets: 0
  },
  {
    id: '1268463878259630081',
    created: 1591260330000,
    type: 'post',
    text: 'I\'m about to be the owner of a food processor for the first time in my life. I think that means I\'m leveling up as a (vegan) chef in progress. I have a feeling I\'m going to enjoy slicing, dicing, and grating too much. 🔪',
    likes: 2,
    retweets: 0
  },
  {
    id: '1268462615275036672',
    created: 1591260029000,
    type: 'post',
    text: 'I\'m really relating to this song right now. In fact, I\'m enjoying the resurgence of hardstyle, esp. D-Block and S-te-Fan. Our nightly dance parties in the kitchen while making dinner is giving me much needed relief from all the heavy stuff. <a href="https://youtu.be/9qQ055zzXjk" rel="noopener noreferrer" target="_blank">youtu.be/9qQ055zzXjk</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1268460506576154625',
    created: 1591259526000,
    type: 'post',
    text: 'The US does not have a national healthcare system. Instead, we have the largest military budget in the world &amp; some of the most well-funded &amp; militarized police depts (which receive equipment from the US military). The protests aren\'t going away until this situation is reversed.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1268310219504758785',
    created: 1591223695000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MsPackyetti" rel="noopener noreferrer" target="_blank">@MsPackyetti</a> ✅ Action demanded<br><br>In reply to: <a href="https://x.com/MsPackyetti/status/1268264058836398081" rel="noopener noreferrer" target="_blank">@MsPackyetti</a> <span class="status">1268264058836398081</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1268262624711438337',
    created: 1591212347000,
    type: 'quote',
    text: 'Thank you Europe. We need your voices right now.<br><br>Quoting: <a href="https://x.com/travisakers/status/1267643014966583298" rel="noopener noreferrer" target="_blank">@travisakers</a> <span class="status">1267643014966583298</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1268071710344884224',
    created: 1591166830000,
    type: 'reply',
    text: '@techpractical <a class="mention" href="https://x.com/github" rel="noopener noreferrer" target="_blank">@github</a> Indeed, we must continue to be vocal about what they\'re doing right and what they\'re doing wrong. Saying what they\'re doing right doesn\'t mean what they\'re doing wrong is okay.<br><br>In reply to: @techpractical <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1268065670870126593',
    created: 1591165390000,
    type: 'post',
    text: 'Thank you <a class="mention" href="https://x.com/github" rel="noopener noreferrer" target="_blank">@github</a> for using your platform to speak up for black lives and against police brutality, and for doing so by linking to concrete solutions. <a href="https://www.joincampaignzero.org/" rel="noopener noreferrer" target="_blank">www.joincampaignzero.org/</a>',
    photos: ['<div class="item"><img class="photo" src="media/1268065670870126593.jpg"></div>'],
    likes: 2,
    retweets: 0
  },
  {
    id: '1267969016674410497',
    created: 1591142345000,
    type: 'post',
    text: 'The problem is that the United States always believed it was better than the rest of the world. We were taught this as kids &amp; heard it parroted constantly. This made us blind to our faults from the start &amp; allowed them to root deeply into society. We must dig deep to remove them.',
    likes: 10,
    retweets: 0
  },
  {
    id: '1267623538048954368',
    created: 1591059977000,
    type: 'post',
    text: 'No doubt <a class="mention" href="https://x.com/IronMuleBrewery" rel="noopener noreferrer" target="_blank">@IronMuleBrewery</a> has their to-go game down. Check \'em out if you\'re in Colorado and need some brews at home. <a href="https://shop.arryved.com/preOrder?locationId=BPqZJ1vH" rel="noopener noreferrer" target="_blank">shop.arryved.com/preOrder?locationId=BPqZJ1vH</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1267608557031059456',
    created: 1591056405000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a><br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/1267583917567197186" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">1267583917567197186</span>',
    photos: ['<div class="item"><img class="photo" src="media/1267608557031059456.jpg"></div>'],
    likes: 1,
    retweets: 0
  },
  {
    id: '1267564404360372224',
    created: 1591045878000,
    type: 'quote',
    text: 'FFS.<br><br>Quoting: <a href="https://x.com/KenidraRWoods_/status/1267508669652127745" rel="noopener noreferrer" target="_blank">@KenidraRWoods_</a> <span class="status">1267508669652127745</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1267546773276352512',
    created: 1591041675000,
    type: 'quote',
    text: 'More cops is not the solution.<br><br>Also notice how cops are referred to as "the force". There\'s a reason for that. Using force is all they know.<br><br>Quoting: <a href="https://x.com/danarubinstein/status/1267540162638118913" rel="noopener noreferrer" target="_blank">@danarubinstein</a> <span class="status">1267540162638118913</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1267546264004001792',
    created: 1591041553000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/chuckfrain" rel="noopener noreferrer" target="_blank">@chuckfrain</a> I\'m also not going to violate the space of a personal conversation. My point is to highlight that the mindset is there even in good cops (as I\'m sure he is). It\'s a toxic culture. He likely has good judgement not to harm a person in custody, but his colleague may not.<br><br>In reply to: <a href="https://x.com/chuckfrain/status/1267511275598340096" rel="noopener noreferrer" target="_blank">@chuckfrain</a> <span class="status">1267511275598340096</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1267501941795373059',
    created: 1591030986000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/chuckfrain" rel="noopener noreferrer" target="_blank">@chuckfrain</a> This kind of racism is so widespread, it\'s frankly just engrained in our society. No one would think anything of it, and that\'s the problem. It cannot be fixed by a one complaint or a thousand.<br><br>In reply to: <a href="https://x.com/chuckfrain/status/1267462634619707394" rel="noopener noreferrer" target="_blank">@chuckfrain</a> <span class="status">1267462634619707394</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1267415050970468352',
    created: 1591010270000,
    type: 'reply',
    text: '@ndw I\'m 100% in favor of disconnecting donations from a mailinglist subscription. It should be required by law to make it an option. (Even better if we just had public funding of elections).<br><br>In reply to: @ndw <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1267406090464931840',
    created: 1591008133000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> That\'s exactly what led me to the book. I found good stuff online, but the treasure trove is in this series, and funny as hell.<br><br>In reply to: <a href="https://x.com/headius/status/1267394014904487939" rel="noopener noreferrer" target="_blank">@headius</a> <span class="status">1267394014904487939</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1267392601172271104',
    created: 1591004917000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> If you\'re looking for a killer recipe book series, check out <a class="mention" href="https://x.com/thugkitchen" rel="noopener noreferrer" target="_blank">@thugkitchen</a>. Those books just don\'t know how to disappoint. I used to poke fun at people who oh and ah about food. Now I\'m the one making all the noises. Game changer.<br><br>In reply to: <a href="https://x.com/headius/status/1267390616876519426" rel="noopener noreferrer" target="_blank">@headius</a> <span class="status">1267390616876519426</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1267391960110645248',
    created: 1591004764000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> Congrats! Same here. TBH, I never thought I\'d like or even be able to cook. It has now become one of the central activities keeping me sane. There\'s something that makes food that you make for yourself taste better. I think it\'s the self care ;)<br><br>In reply to: <a href="https://x.com/headius/status/1267390616876519426" rel="noopener noreferrer" target="_blank">@headius</a> <span class="status">1267390616876519426</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1267337425652551681',
    created: 1590991762000,
    type: 'post',
    text: 'I have a cousin who\'s a cop. I\'ve heard him use racist and derogatory language when describing the people he\'s suppose to protect. He\'s been recognized as one of the good cops. Defund the police. Start over.',
    likes: 6,
    retweets: 1
  },
  {
    id: '1267183049902444544',
    created: 1590954956000,
    type: 'quote',
    text: 'Thank you Belgium.<br><br>Quoting: <a href="https://x.com/nowthisimpact/status/1266694092139921409" rel="noopener noreferrer" target="_blank">@nowthisimpact</a> <span class="status">1266694092139921409</span>',
    likes: 7,
    retweets: 3
  },
  {
    id: '1267180939567722496',
    created: 1590954453000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/WaywardWinifred" rel="noopener noreferrer" target="_blank">@WaywardWinifred</a> If there were ever a time for him to go all on, now is that time.<br><br>In reply to: <a href="https://x.com/WaywardWinifred/status/1267156213374095366" rel="noopener noreferrer" target="_blank">@WaywardWinifred</a> <span class="status">1267156213374095366</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1267041793465413632',
    created: 1590921278000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ghillert" rel="noopener noreferrer" target="_blank">@ghillert</a> I can only imagine the kind of PTSD it must cause, not to mention having an understanding where this leads.<br><br>The only silver lining is that maybe citizens will finally start to understand what terror we inflict on other countries.<br><br>In reply to: <a href="https://x.com/ghillert/status/1267040636487647232" rel="noopener noreferrer" target="_blank">@ghillert</a> <span class="status">1267040636487647232</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1267040777516933121',
    created: 1590921036000,
    type: 'quote',
    text: 'Just bad apples. That\'s what they\'ll tell you. But when the departments refuse to prosecute the criminals in the force, it makes the whole bunch rotten. And that\'s the result you\'re seeing here.<br><br>Quoting: <a href="https://x.com/AnnaKendrick47/status/1266963846649933824" rel="noopener noreferrer" target="_blank">@AnnaKendrick47</a> <span class="status">1266963846649933824</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1267036227586289664',
    created: 1590919951000,
    type: 'quote',
    text: 'Who exactly are they protecting and serving? Certainly not the residents in this community. The only thing the military is doing here is picking a fight. Kind of puts the wars the US wages in perspective.<br><br>Quoting: <a href="https://x.com/tkerssen/status/1266921821653385225" rel="noopener noreferrer" target="_blank">@tkerssen</a> <span class="status">1266921821653385225</span>',
    likes: 5,
    retweets: 1
  },
  {
    id: '1267024027689775104',
    created: 1590917043000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> The sad truth is that the system is working exactly as it was designed. That\'s why we need to rebuild it from the ground up using citizen review boards who hire directly from the community. Anything else is just kicking the can down the road until the next lynching.<br><br>In reply to: <a href="https://x.com/metacosm/status/1267011454890381312" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">1267011454890381312</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1266984754903052290',
    created: 1590907679000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/shobull" rel="noopener noreferrer" target="_blank">@shobull</a> No doubt. These are unprecedented times and the agitation and threats are coming from the highest level, fanning the flames of injustice. Civility has little to cling on to, and that\'s a real problem.<br><br>In reply to: <a href="https://x.com/shobull/status/1266820760162594818" rel="noopener noreferrer" target="_blank">@shobull</a> <span class="status">1266820760162594818</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1266983438608855040',
    created: 1590907365000,
    type: 'quote',
    text: 'Really powerful statement by <a class="mention" href="https://x.com/IlhanMN" rel="noopener noreferrer" target="_blank">@IlhanMN</a>. You aren\'t interested in getting justice for a community if you\'re putting the lives in that community at risk.<br><br>Quoting: <a href="https://x.com/IlhanMN/status/1266941736959557634" rel="noopener noreferrer" target="_blank">@IlhanMN</a> <span class="status">1266941736959557634</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1266533653686784000',
    created: 1590800128000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/shobull" rel="noopener noreferrer" target="_blank">@shobull</a> That only touches on the terror this brings to society. Their actions, &amp; the commands issued from the police chiefs, governors, &amp; others in power, are rapidly feeding a fascist state. They\'re the ones inciting this violence. Standing up a military on home soil is not an accident.<br><br>In reply to: <a href="#1266532395978600448">1266532395978600448</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1266532395978600448',
    created: 1590799828000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/shobull" rel="noopener noreferrer" target="_blank">@shobull</a> Do you see the military-sized presence in the backdrop? This was the same tactic used at Standing Rock &amp; Ferguson. The police force is acting as an agent of the state to suppress protests &amp; intimate Black, Brown, and Native communities. It\'s the very definition of a police state.<br><br>In reply to: <a href="https://x.com/shobull/status/1266512045744115712" rel="noopener noreferrer" target="_blank">@shobull</a> <span class="status">1266512045744115712</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1266484152586493952',
    created: 1590788326000,
    type: 'post',
    text: 'I\'m absolutely outraged by the murder of black citizens by police (which is lynching), the disgusting racism (which is all racism), &amp; the complete lack of a mature response by officials to do anything substantial to address these problems. I\'m also feeling pretty damn hopeless.',
    likes: 7,
    retweets: 0
  },
  {
    id: '1266330523237928961',
    created: 1590751698000,
    type: 'quote',
    text: 'The US is now a police state. There\'s just no denying it anymore. This is outrageous.<br><br>Quoting: <a href="https://x.com/CNNThisMorning/status/1266315061209030658" rel="noopener noreferrer" target="_blank">@CNNThisMorning</a> <span class="status">1266315061209030658</span>',
    likes: 5,
    retweets: 1
  },
  {
    id: '1266124798087684097',
    created: 1590702649000,
    type: 'quote',
    text: 'Absolutely. You may feel like a stickler at first. But trust the advice, it\'s better for all parties in the end.<br><br>Quoting: <a href="https://x.com/gchorba/status/1266116111939956736" rel="noopener noreferrer" target="_blank">@gchorba</a> <span class="status">1266116111939956736</span>',
    likes: 8,
    retweets: 0
  },
  {
    id: '1266124345526464513',
    created: 1590702542000,
    type: 'post',
    text: 'I had forgotten how to use prepend to override a class method (aka static method) in Ruby. I found this post that\'s made-to-order to answer that question. <a href="https://sikac.hu/how-to-use-module-prepend-to-override-class-method-in-ruby-4197c6159cd3" rel="noopener noreferrer" target="_blank">sikac.hu/how-to-use-module-prepend-to-override-class-method-in-ruby-4197c6159cd3</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1266115586251042816',
    created: 1590700453000,
    type: 'quote',
    text: 'Seems legit to me.<br><br>Quoting: <a href="https://x.com/BristolSheriff/status/1266030663263813636" rel="noopener noreferrer" target="_blank">@BristolSheriff</a> <span class="status">1266030663263813636</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1265776448377241600',
    created: 1590619596000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bbatsov" rel="noopener noreferrer" target="_blank">@bbatsov</a> See <a href="https://docs.antora.org/antora/2.3/whats-new/#smarter-pagination" rel="noopener noreferrer" target="_blank">docs.antora.org/antora/2.3/whats-new/#smarter-pagination</a><br><br>In reply to: <a href="#1265751428473032704">1265751428473032704</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1265751428473032704',
    created: 1590613631000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bbatsov" rel="noopener noreferrer" target="_blank">@bbatsov</a> Tip: If you declare the page-pagination attribute in the playbook (under asciidoc.attributes) you will get next / prev links at the bottom of each page like on <a href="https://docs.antora.org" rel="noopener noreferrer" target="_blank">docs.antora.org</a>.<br><br>In reply to: <a href="https://x.com/bbatsov/status/1265667136883765249" rel="noopener noreferrer" target="_blank">@bbatsov</a> <span class="status">1265667136883765249</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1265751266979807232',
    created: 1590613593000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bbatsov" rel="noopener noreferrer" target="_blank">@bbatsov</a> Love, love, love! (Then again, I\'m biased).<br><br>In reply to: <a href="https://x.com/bbatsov/status/1265667136883765249" rel="noopener noreferrer" target="_blank">@bbatsov</a> <span class="status">1265667136883765249</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1265521534996746243',
    created: 1590558820000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/hsablonniere" rel="noopener noreferrer" target="_blank">@hsablonniere</a> <a class="mention" href="https://x.com/ClementD" rel="noopener noreferrer" target="_blank">@ClementD</a> I\'ve been thinking this was just me for over a month. Wow.<br><br>In reply to: <a href="https://x.com/hsablonniere/status/1265328880648364032" rel="noopener noreferrer" target="_blank">@hsablonniere</a> <span class="status">1265328880648364032</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1265381126572437504',
    created: 1590525344000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/catallman" rel="noopener noreferrer" target="_blank">@catallman</a> Ironically, that\'s what an elevator pitch is supposed to be. Just get to the point. Instead, it seems to have become exactly the opposite.<br><br>In reply to: <a href="https://x.com/catallman/status/1265380692466135040" rel="noopener noreferrer" target="_blank">@catallman</a> <span class="status">1265380692466135040</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1265178070748590081',
    created: 1590476932000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/wintermeyer" rel="noopener noreferrer" target="_blank">@wintermeyer</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> <a class="mention" href="https://x.com/jekyllrb" rel="noopener noreferrer" target="_blank">@jekyllrb</a> Thanks for clarifying. I wasn\'t seeking to judge. Using Antora output with a Jekyll site is perfectly reasonable, just not a scenario I had considered. To handle that case, we either need the configuration option or change the default. If you file an issue, we can address it.<br><br>In reply to: <a href="https://x.com/wintermeyer/status/1265177448502759424" rel="noopener noreferrer" target="_blank">@wintermeyer</a> <span class="status">1265177448502759424</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1265177033165795328',
    created: 1590476685000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/wintermeyer" rel="noopener noreferrer" target="_blank">@wintermeyer</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> <a class="mention" href="https://x.com/jekyllrb" rel="noopener noreferrer" target="_blank">@jekyllrb</a> I\'m curious, are you actually using Jekyll?<br><br>In reply to: <a href="https://x.com/wintermeyer/status/1265176826453819393" rel="noopener noreferrer" target="_blank">@wintermeyer</a> <span class="status">1265176826453819393</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1265176603727781888',
    created: 1590476582000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gegastaldi" rel="noopener noreferrer" target="_blank">@gegastaldi</a> To be honest, no native speaker understands why it\'s spelled that way. We just say it the way it should be spelled.<br><br>In reply to: <a href="https://x.com/gegastaldi/status/1265148360681545730" rel="noopener noreferrer" target="_blank">@gegastaldi</a> <span class="status">1265148360681545730</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1265175183255498754',
    created: 1590476244000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/wintermeyer" rel="noopener noreferrer" target="_blank">@wintermeyer</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> <a class="mention" href="https://x.com/jekyllrb" rel="noopener noreferrer" target="_blank">@jekyllrb</a> I\'m open to adding a configuration option to the playbook that controls this prefix character.<br><br>In reply to: <a href="https://x.com/wintermeyer/status/1265172612570918913" rel="noopener noreferrer" target="_blank">@wintermeyer</a> <span class="status">1265172612570918913</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1265175088938143744',
    created: 1590476221000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/wintermeyer" rel="noopener noreferrer" target="_blank">@wintermeyer</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> <a class="mention" href="https://x.com/jekyllrb" rel="noopener noreferrer" target="_blank">@jekyllrb</a> Not currently. (There\'s a way to change the UI directory, but not the directories for content assets). There\'s a note how to handle this for a GitHub Pages site, but that assumes you aren\'t using Jekyll. See <a href="https://docs.antora.org/antora/2.3/publish-to-github-pages/#jekyll-and-underscore-files" rel="noopener noreferrer" target="_blank">docs.antora.org/antora/2.3/publish-to-github-pages/#jekyll-and-underscore-files</a><br><br>In reply to: <a href="https://x.com/wintermeyer/status/1265172612570918913" rel="noopener noreferrer" target="_blank">@wintermeyer</a> <span class="status">1265172612570918913</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1265133340887146499',
    created: 1590466268000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bryanwklein" rel="noopener noreferrer" target="_blank">@bryanwklein</a> <a class="mention" href="https://x.com/stefanct" rel="noopener noreferrer" target="_blank">@stefanct</a> <a class="mention" href="https://x.com/Vjeux" rel="noopener noreferrer" target="_blank">@Vjeux</a> <a class="mention" href="https://x.com/rauchg" rel="noopener noreferrer" target="_blank">@rauchg</a> AsciiDoc has all that, or has the ability to create all that (via extensions or custom templates).<br><br>In reply to: <a href="https://x.com/bryanwklein/status/1265094479641341952" rel="noopener noreferrer" target="_blank">@bryanwklein</a> <span class="status">1265094479641341952</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1265036292171034624',
    created: 1590443129000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kamer_ee" rel="noopener noreferrer" target="_blank">@kamer_ee</a> <a class="mention" href="https://x.com/MarcoBehler" rel="noopener noreferrer" target="_blank">@MarcoBehler</a> <a class="mention" href="https://x.com/middlemanapp" rel="noopener noreferrer" target="_blank">@middlemanapp</a> So it turns out to be a good thing. Hugo is an example of what happens when they do. They don\'t enable the full capabilities of AsciiDoc and therefore people end up running into a lot of problems. The Jekyll and Middleman integrations have the freedom to go further.<br><br>In reply to: <a href="#1265036043398438912">1265036043398438912</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1265036043398438912',
    created: 1590443070000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kamer_ee" rel="noopener noreferrer" target="_blank">@kamer_ee</a> <a class="mention" href="https://x.com/MarcoBehler" rel="noopener noreferrer" target="_blank">@MarcoBehler</a> <a class="mention" href="https://x.com/middlemanapp" rel="noopener noreferrer" target="_blank">@middlemanapp</a> Most static site generators integrate Markdown as a built-in option, but refuse to bake in additional markup languages. That\'s understandable because a) it requires more effort &amp; b) to integrate AsciiDoc properly requires expertise in AsciiDoc which the maintainers don\'t have.<br><br>In reply to: <a href="https://x.com/kamer_ee/status/1265033749714141185" rel="noopener noreferrer" target="_blank">@kamer_ee</a> <span class="status">1265033749714141185</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1265031132556390400',
    created: 1590441899000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kamer_ee" rel="noopener noreferrer" target="_blank">@kamer_ee</a> <a class="mention" href="https://x.com/MarcoBehler" rel="noopener noreferrer" target="_blank">@MarcoBehler</a> <a class="mention" href="https://x.com/middlemanapp" rel="noopener noreferrer" target="_blank">@middlemanapp</a> Almost every major static site generator offers AsciiDoc support. The most popular is probably Jekyll, though you can find support in Middleman, Hugo, JBake, nanoc, and many others.<br><br>In reply to: <a href="https://x.com/kamer_ee/status/1264896706778365952" rel="noopener noreferrer" target="_blank">@kamer_ee</a> <span class="status">1264896706778365952</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1264455625234247682',
    created: 1590304688000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/javinpaul" rel="noopener noreferrer" target="_blank">@javinpaul</a> <a class="mention" href="https://x.com/_tamanm" rel="noopener noreferrer" target="_blank">@_tamanm</a> I think rsync is pretty essential, even for local operations.<br><br>In reply to: <a href="https://x.com/javinpaul/status/1264436774945710080" rel="noopener noreferrer" target="_blank">@javinpaul</a> <span class="status">1264436774945710080</span>',
    likes: 5,
    retweets: 0
  },
  {
    id: '1264455230063669248',
    created: 1590304593000,
    type: 'post',
    text: 'I remember sitting in the audience at an early JavaOne yearning for an announcement Java would be open. Although it didn\'t happen then, it\'s been surreal to see it make that transition. OpenJDK (yes, open) + Java EE hosted at the Eclipse Foundation would have felt like a dream.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1264452960613855234',
    created: 1590304052000,
    type: 'post',
    text: 'I was in the 1st class at Cornell to use Java for the programming intro course. Years later I rediscovered it via Struts &amp; AppFuse, then went on to write a book about Seam. The Java network was like nothing I\'d experienced...and I leaned on it hard to make Java open. #MovedbyJava',
    likes: 11,
    retweets: 0,
    tags: ['movedbyjava']
  },
  {
    id: '1263948273200451584',
    created: 1590183726000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lfryc" rel="noopener noreferrer" target="_blank">@lfryc</a> <a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Sarah too.<br><br>In reply to: <a href="https://x.com/lfryc/status/1263886638331113477" rel="noopener noreferrer" target="_blank">@lfryc</a> <span class="status">1263886638331113477</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1263791712045895682',
    created: 1590146398000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aslakknutsen" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <a class="mention" href="https://x.com/samaaron" rel="noopener noreferrer" target="_blank">@samaaron</a> Same. My only regret is not waking up sooner. I could make excuses, but I know that I always had a choice and didn\'t make it soon enough.<br><br>In reply to: <a href="https://x.com/aslakknutsen/status/1263784895635128321" rel="noopener noreferrer" target="_blank">@aslakknutsen</a> <span class="status">1263784895635128321</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1263738725755834368',
    created: 1590133766000,
    type: 'post',
    text: 'I like the new navigate to your fork button on GitHub (the arrow on the right) provided by Refined GitHub. Makes navigation a lot simpler. <a href="https://github.com/sindresorhus/refined-github#repositories" rel="noopener noreferrer" target="_blank">github.com/sindresorhus/refined-github#repositories</a>',
    photos: ['<div class="item"><img class="photo" src="media/1263738725755834368.png"></div>'],
    likes: 3,
    retweets: 0
  },
  {
    id: '1263601869613486080',
    created: 1590101136000,
    type: 'post',
    text: 'Believe it or not, the arugula (which I forgot to mention) just geminated this morning! They are really loving this LED light.<br><br>Quoting: <a href="#1263060104015785984">1263060104015785984</a>',
    photos: ['<div class="item"><img class="photo" src="media/1263601869613486080.jpg"></div>'],
    likes: 3,
    retweets: 0
  },
  {
    id: '1263601231592697856',
    created: 1590100984000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/chuckfrain" rel="noopener noreferrer" target="_blank">@chuckfrain</a> <a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> That\'s OSS at work right there!<br><br>In reply to: <a href="https://x.com/chuckfrain/status/1263600670915125248" rel="noopener noreferrer" target="_blank">@chuckfrain</a> <span class="status">1263600670915125248</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1263601047030755328',
    created: 1590100940000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RJDickenson" rel="noopener noreferrer" target="_blank">@RJDickenson</a> I\'m counting the days! The first seeds just germinated this morning in my little garden.<br><br>In reply to: <a href="https://x.com/RJDickenson/status/1263591336776577026" rel="noopener noreferrer" target="_blank">@RJDickenson</a> <span class="status">1263591336776577026</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1263424384452063232',
    created: 1590058821000,
    type: 'post',
    text: 'I\'m beginning to wonder if there will ever be a time in my life when the message in the song Changes by 2Pac is no longer contemporary. It\'s tragic that the song never ages. <a href="https://genius.com/2pac-changes-lyrics" rel="noopener noreferrer" target="_blank">genius.com/2pac-changes-lyrics</a> #BlackLivesMatter',
    likes: 4,
    retweets: 0,
    tags: ['blacklivesmatter']
  },
  {
    id: '1263404012692312064',
    created: 1590053964000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/netzwerg999" rel="noopener noreferrer" target="_blank">@netzwerg999</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> rouge seems to do a decent job. Have you tried it?<br><br>-a source-highlighter=rouge<br><br>In reply to: @netzwerg999 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1263258221944266752',
    created: 1590019204000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bengl" rel="noopener noreferrer" target="_blank">@bengl</a> 🤣<br><br>In reply to: <a href="https://x.com/bengl/status/1263254396420476933" rel="noopener noreferrer" target="_blank">@bengl</a> <span class="status">1263254396420476933</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1263251198972268544',
    created: 1590017530000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bengl" rel="noopener noreferrer" target="_blank">@bengl</a> There\'s certainly no right or wrong here. What matters is to be consistent. I consistently don\'t use semicolons because they just don\'t have utility. But if someone believes they provide important aesthetics, go with it. I\'ll always honor the style the project requests.<br><br>In reply to: <a href="https://x.com/bengl/status/1263230252274388992" rel="noopener noreferrer" target="_blank">@bengl</a> <span class="status">1263230252274388992</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1263222704905138176',
    created: 1590010737000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bengl" rel="noopener noreferrer" target="_blank">@bengl</a> No, I refuse to use them because they serve no purpose (since line endings already do that). Punctuation in prose has a very well-defined and important purpose.<br><br>In reply to: <a href="https://x.com/bengl/status/1262856441813241858" rel="noopener noreferrer" target="_blank">@bengl</a> <span class="status">1262856441813241858</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1263221970700693504',
    created: 1590010562000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/zacbowden" rel="noopener noreferrer" target="_blank">@zacbowden</a> Community and control. It\'s not always easy, but paving your own path never is. But the reward is far greater.<br><br>In reply to: <a href="https://x.com/zacbowden/status/1263128436815560704" rel="noopener noreferrer" target="_blank">@zacbowden</a> <span class="status">1263128436815560704</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1263198461815480320',
    created: 1590004957000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gregturn" rel="noopener noreferrer" target="_blank">@gregturn</a> s/there/their/<br><br>In reply to: <a href="#1263190165087911936">1263190165087911936</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1263190165087911936',
    created: 1590002978000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gregturn" rel="noopener noreferrer" target="_blank">@gregturn</a> Agreed.<br><br>Though I will admit I absolutely did not expect that printers would actually print text in grayscale. That\'s kind of one of those "isn\'t that there job to get right?" kind of things.<br><br>In reply to: <a href="https://x.com/gregturn/status/1263165795867246593" rel="noopener noreferrer" target="_blank">@gregturn</a> <span class="status">1263165795867246593</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1263164298940911616',
    created: 1589996811000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gregturn" rel="noopener noreferrer" target="_blank">@gregturn</a> Let\'s at least add a note about it to the publishing mode section.<br><br>In reply to: <a href="https://x.com/gregturn/status/1263147867612557317" rel="noopener noreferrer" target="_blank">@gregturn</a> <span class="status">1263147867612557317</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1263163850020413442',
    created: 1589996704000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gregturn" rel="noopener noreferrer" target="_blank">@gregturn</a> It\'s a recommendation to use off-black for screen (like with the web). But for print, it should almost always be black. We could make it a feature of media=prepress to override. Though generally you\'ll want to customize the theme for other reasons.<br><br>In reply to: <a href="https://x.com/gregturn/status/1263147867612557317" rel="noopener noreferrer" target="_blank">@gregturn</a> <span class="status">1263147867612557317</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1263162990011932673',
    created: 1589996499000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/atelierbram" rel="noopener noreferrer" target="_blank">@atelierbram</a> I should have mentioned that we got a grow light for the indoor plants. And they responded to it right away. I\'ll share pics soon.<br><br>In reply to: <a href="https://x.com/atelierbram/status/1263072904889204736" rel="noopener noreferrer" target="_blank">@atelierbram</a> <span class="status">1263072904889204736</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1263070335752040448',
    created: 1589974409000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/IzisFilipaldi" rel="noopener noreferrer" target="_blank">@IzisFilipaldi</a> Good luck!<br><br>In reply to: <a href="https://x.com/IzisFilipaldi/status/1263063254915600384" rel="noopener noreferrer" target="_blank">@IzisFilipaldi</a> <span class="status">1263063254915600384</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1263070303955021828',
    created: 1589974401000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/IzisFilipaldi" rel="noopener noreferrer" target="_blank">@IzisFilipaldi</a> So cool! The love is real. Now when I wake up, my first thought is how my plants are doing. And it\'s not just about the buds. I get excited when I see more roots.<br><br>In reply to: <a href="https://x.com/IzisFilipaldi/status/1263063254915600384" rel="noopener noreferrer" target="_blank">@IzisFilipaldi</a> <span class="status">1263063254915600384</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1263069185006948352',
    created: 1589974135000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/atelierbram" rel="noopener noreferrer" target="_blank">@atelierbram</a> Both indoor and outdoor. I\'m basically throwing every approach at the wall to see what technique works best in the Colorado climate.<br><br>In reply to: <a href="https://x.com/atelierbram/status/1263062082641108998" rel="noopener noreferrer" target="_blank">@atelierbram</a> <span class="status">1263062082641108998</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1263060104015785984',
    created: 1589971969000,
    type: 'post',
    text: 'This week, I sowed seeds for the first time (not counting elementary school). Carrots, cilantro, lettuce, and spinach. Now, we wait... 🌱',
    likes: 11,
    retweets: 0
  },
  {
    id: '1263025735461072896',
    created: 1589963775000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> <a class="mention" href="https://x.com/connolly_s" rel="noopener noreferrer" target="_blank">@connolly_s</a> <a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> It took me a second, but his nose gives it away.<br><br>In reply to: <a href="https://x.com/metacosm/status/1263025506045304832" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">1263025506045304832</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1263024770334908416',
    created: 1589963545000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> <a class="mention" href="https://x.com/connolly_s" rel="noopener noreferrer" target="_blank">@connolly_s</a> <a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> He\'s at 2:29. It is just a flash, but that\'s him.<br><br>In reply to: <a href="https://x.com/metacosm/status/1263020174980112385" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">1263020174980112385</span>',
    photos: ['<div class="item"><img class="photo" src="media/1263024770334908416.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1262990782018908162',
    created: 1589955442000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/havenruthie" rel="noopener noreferrer" target="_blank">@havenruthie</a> <a class="mention" href="https://x.com/TheWebbyAwards" rel="noopener noreferrer" target="_blank">@TheWebbyAwards</a> <a class="mention" href="https://x.com/AviSchiffmann" rel="noopener noreferrer" target="_blank">@AviSchiffmann</a> I use that site every single day. It\'s very well done. And I love that he\'s learned so much while creating it. That\'s what programming is all about. Finding a passion. Learning a skill. Making information easy to access. Being proud of what you made.<br><br>In reply to: <a href="https://x.com/havenruthie/status/1262968314927566850" rel="noopener noreferrer" target="_blank">@havenruthie</a> <span class="status">1262968314927566850</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1262898258973618177',
    created: 1589933383000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> Now don\'t go insulting us ;) We\'re not that thick. 😝<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/1262877642707144707" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">1262877642707144707</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1262897902340497408',
    created: 1589933298000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/connolly_s" rel="noopener noreferrer" target="_blank">@connolly_s</a> <a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> <a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> Just watched the video and recognized him immediately. It\'s amazing what the brain will ignore when it doesn\'t expect to see something.<br><br>In reply to: <a href="https://x.com/connolly_s/status/1262873202268745729" rel="noopener noreferrer" target="_blank">@connolly_s</a> <span class="status">1262873202268745729</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1262871046169915393',
    created: 1589926895000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> I don\'t think we needed COVID to know that. It\'s just that COVID makes it much harder to use ignorance as an excuse. As a society, we know we\'re destroying the planet and seem to be just fine with doing so. And the future generations will despise us for it.<br><br>In reply to: <a href="https://x.com/starbuxman/status/1262869583242727426" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">1262869583242727426</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1262869986827091968',
    created: 1589926642000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joshsimmons" rel="noopener noreferrer" target="_blank">@joshsimmons</a> <a class="mention" href="https://x.com/chrisjrn" rel="noopener noreferrer" target="_blank">@chrisjrn</a> <a class="mention" href="https://x.com/VillanoM" rel="noopener noreferrer" target="_blank">@VillanoM</a> I\'m so, so happy for you both. 🌈<br><br>In reply to: <a href="https://x.com/joshsimmons/status/1262868352256446465" rel="noopener noreferrer" target="_blank">@joshsimmons</a> <span class="status">1262868352256446465</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1262865843626496001',
    created: 1589925654000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobmcwhirter" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> To be honest, I wouldn\'t have known to look for it. Though I always suspected that the line "I want my MTV" was another voice. So there you go.<br><br>In reply to: <a href="https://x.com/bobmcwhirter/status/1262865029889757184" rel="noopener noreferrer" target="_blank">@bobmcwhirter</a> <span class="status">1262865029889757184</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1262864055062024192',
    created: 1589925228000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/metacosm" rel="noopener noreferrer" target="_blank">@metacosm</a> News to me.<br><br>In reply to: <a href="https://x.com/metacosm/status/1262863947235053575" rel="noopener noreferrer" target="_blank">@metacosm</a> <span class="status">1262863947235053575</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1262826427788869632',
    created: 1589916257000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/romanoff2020" rel="noopener noreferrer" target="_blank">@romanoff2020</a> Big changes, not baby steps. Perfectly stated.<br><br>In reply to: <a href="https://x.com/AndrewRomanoff/status/1262816677839978498" rel="noopener noreferrer" target="_blank">@AndrewRomanoff</a> <span class="status">1262816677839978498</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1262806501686562816',
    created: 1589911506000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/wintermeyer" rel="noopener noreferrer" target="_blank">@wintermeyer</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> Highlighting is done by the highlight.js. The UI controls how highlight.js is loaded. The default UI selectively enables certain languages. Since highlight.js supports elixir, we can add it. Please file issue at <a href="https://gitlab.com/antora/antora-ui-default" rel="noopener noreferrer" target="_blank">gitlab.com/antora/antora-ui-default</a><br><br>In reply to: <a href="https://x.com/wintermeyer/status/1262721478669275136" rel="noopener noreferrer" target="_blank">@wintermeyer</a> <span class="status">1262721478669275136</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1262643457836765184',
    created: 1589872633000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/connolly_s" rel="noopener noreferrer" target="_blank">@connolly_s</a> <a class="mention" href="https://x.com/zepag" rel="noopener noreferrer" target="_blank">@zepag</a> You can find a great example of Lunr integration in the <a class="mention" href="https://x.com/fauna" rel="noopener noreferrer" target="_blank">@fauna</a> docs. <a href="https://docs.fauna.com" rel="noopener noreferrer" target="_blank">docs.fauna.com</a><br><br>In reply to: <a href="https://x.com/connolly_s/status/1262642897830150144" rel="noopener noreferrer" target="_blank">@connolly_s</a> <span class="status">1262642897830150144</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1262643232808112128',
    created: 1589872580000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/zepag" rel="noopener noreferrer" target="_blank">@zepag</a> Search is more about the HTML. It doesn\'t matter how it\'s made. We use Algolia docsearch for <a href="http://docs.antora.org" rel="noopener noreferrer" target="_blank">docs.antora.org</a>. Another option is Lunr, which is supported via an extension. See <a href="https://gitlab.com/antora/antora-ui-default/-/issues/68" rel="noopener noreferrer" target="_blank">gitlab.com/antora/antora-ui-default/-/issues/68</a> for ideas.<br><br>In reply to: <a href="https://x.com/zepag/status/1262634170590863368" rel="noopener noreferrer" target="_blank">@zepag</a> <span class="status">1262634170590863368</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1262347725963538433',
    created: 1589802125000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/wimdeblauwe" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> <a class="mention" href="https://x.com/wintermeyer" rel="noopener noreferrer" target="_blank">@wintermeyer</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> The main difference is that Antora is a site generator, Asciidoctor is not. Asciidoctor was always meant to be integrated into a site generator for making a docs or book site. The main advantage is references and navigation.<br><br>In reply to: <a href="https://x.com/wimdeblauwe/status/1262344628088569856" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> <span class="status">1262344628088569856</span>',
    likes: 0,
    retweets: 1
  },
  {
    id: '1262319116578242560',
    created: 1589795304000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MarcoBehler" rel="noopener noreferrer" target="_blank">@MarcoBehler</a> Yep, that\'s the drawback.<br><br>In reply to: <a href="https://x.com/MarcoBehler/status/1262317873810362369" rel="noopener noreferrer" target="_blank">@MarcoBehler</a> <span class="status">1262317873810362369</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1262316583759695872',
    created: 1589794700000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MarcoBehler" rel="noopener noreferrer" target="_blank">@MarcoBehler</a> PDF is not a friendly environment for code samples.<br><br>Have you tried the %autofit option? It has drawbacks too, but a useful tool.<br><br>In reply to: <a href="https://x.com/MarcoBehler/status/1262289033352425473" rel="noopener noreferrer" target="_blank">@MarcoBehler</a> <span class="status">1262289033352425473</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1261960199377543170',
    created: 1589709732000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/eregontp" rel="noopener noreferrer" target="_blank">@eregontp</a> <a class="mention" href="https://x.com/ChrisGSeaton" rel="noopener noreferrer" target="_blank">@ChrisGSeaton</a> Oh, I see what you\'re saying. Some could still be valid even when the search is a string.<br><br>In reply to: <a href="https://x.com/eregontp/status/1261957523055280128" rel="noopener noreferrer" target="_blank">@eregontp</a> <span class="status">1261957523055280128</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1261956736329478146',
    created: 1589708906000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ChKal" rel="noopener noreferrer" target="_blank">@ChKal</a> I started to venture down that hole, but stopped myself. Incredibly frustrating.<br><br>In reply to: @chkal <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1261956606125699072',
    created: 1589708875000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/eregontp" rel="noopener noreferrer" target="_blank">@eregontp</a> <a class="mention" href="https://x.com/ChrisGSeaton" rel="noopener noreferrer" target="_blank">@ChrisGSeaton</a> \\1 through \\9 too.<br><br>In reply to: <a href="https://x.com/eregontp/status/1261956154831384577" rel="noopener noreferrer" target="_blank">@eregontp</a> <span class="status">1261956154831384577</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1261949258275500033',
    created: 1589707123000,
    type: 'post',
    text: 'Screw Google for adding a Meet sidebar to Gmail that cannot be removed. I despise forced integrations.',
    likes: 8,
    retweets: 0
  },
  {
    id: '1261942956115673090',
    created: 1589705621000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rob_winch" rel="noopener noreferrer" target="_blank">@rob_winch</a> <a class="mention" href="https://x.com/ysb33r" rel="noopener noreferrer" target="_blank">@ysb33r</a> <a class="mention" href="https://x.com/styx_hcr" rel="noopener noreferrer" target="_blank">@styx_hcr</a> Open an issue in <a href="https://gitlab.com/antora/antora" rel="noopener noreferrer" target="_blank">gitlab.com/antora/antora</a>. That project dual serves as the main architecture forum for Antora and the ecosystem.<br><br>In reply to: <a href="https://x.com/rob_winch/status/1261814036020129792" rel="noopener noreferrer" target="_blank">@rob_winch</a> <span class="status">1261814036020129792</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1261808185142173696',
    created: 1589673489000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ChrisGSeaton" rel="noopener noreferrer" target="_blank">@ChrisGSeaton</a> I didn\'t realize back references were used if "a" is a string. Learned something new today.<br><br>In reply to: <a href="https://x.com/ChrisGSeaton/status/1261807667112263680" rel="noopener noreferrer" target="_blank">@ChrisGSeaton</a> <span class="status">1261807667112263680</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1261619929096376321',
    created: 1589628605000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/wintermeyer" rel="noopener noreferrer" target="_blank">@wintermeyer</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> PDF export is in prototype.<br><br>In reply to: <a href="https://x.com/wintermeyer/status/1261618753558757376" rel="noopener noreferrer" target="_blank">@wintermeyer</a> <span class="status">1261618753558757376</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1261419399191293954',
    created: 1589580795000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rob_winch" rel="noopener noreferrer" target="_blank">@rob_winch</a> <a class="mention" href="https://x.com/ysb33r" rel="noopener noreferrer" target="_blank">@ysb33r</a> <a class="mention" href="https://x.com/styx_hcr" rel="noopener noreferrer" target="_blank">@styx_hcr</a> I think we\'re at the point now with Antora that we could entertain the proposal to bring this into the Antora org. A lot has changed since we first discussed it.<br><br>In reply to: <a href="https://x.com/rob_winch/status/1261281445022220288" rel="noopener noreferrer" target="_blank">@rob_winch</a> <span class="status">1261281445022220288</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1261400988776198144',
    created: 1589576406000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/connolly_s" rel="noopener noreferrer" target="_blank">@connolly_s</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> I\'m not saying it isn\'t a problem, but the assumption changed. (And now we need to adapt).<br><br>In reply to: <a href="#1261400540249915393">1261400540249915393</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1261400540249915393',
    created: 1589576299000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/connolly_s" rel="noopener noreferrer" target="_blank">@connolly_s</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Since Asciidoctor only processes a single file, the assumption is that it\'s the file you are processing. It\'s up to tools like Antora, which process a collection of files, to enhance the message. And that\'s planned for Antora 3.<br><br>In reply to: <a href="https://x.com/connolly_s/status/1261245566123757570" rel="noopener noreferrer" target="_blank">@connolly_s</a> <span class="status">1261245566123757570</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1261383699922251776',
    created: 1589572284000,
    type: 'post',
    text: 'When people ask for support, I push them to share their source code in a public repository (even if its just a stripped down version or prototype). I\'m an open source advocate and I\'m not going to stop advocating for that model, even for support. Those are my terms. OSS works.',
    likes: 36,
    retweets: 8
  },
  {
    id: '1261382806711627777',
    created: 1589572071000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/wintermeyer" rel="noopener noreferrer" target="_blank">@wintermeyer</a> Having said that, the maintainer of asciidoctor-epub3 has implemented chunking at the section level. That work could be generalized into a multi-page HTML converter (currently here: <a href="https://github.com/asciidoctor/asciidoctor-extensions-lab/blob/master/lib/multipage-html5-converter.rb" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-extensions-lab/blob/master/lib/multipage-html5-converter.rb</a>). Perhaps Marat is interested in driving that. Ask in gitter channel.<br><br>In reply to: <a href="#1261382423125741568">1261382423125741568</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1261382423125741568',
    created: 1589571979000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/wintermeyer" rel="noopener noreferrer" target="_blank">@wintermeyer</a> I\'m not going to say there isn\'t still value in a multi-page converter, but we\'ve been pursuing an alternate direction in the form of Antora in which pages are defined separately and leverages its mature page referencing system. In other words, pre-chunked.<br><br>In reply to: <a href="https://x.com/wintermeyer/status/1261172643123859458" rel="noopener noreferrer" target="_blank">@wintermeyer</a> <span class="status">1261172643123859458</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1261198183176171521',
    created: 1589528053000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> It\'s about exerting constant pressure for a more just world.<br><br>In reply to: <a href="#1261198033020088322">1261198033020088322</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1261198033020088322',
    created: 1589528017000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> I agree it\'s important to remember this isn\'t new. That doesn\'t mean we shouldn\'t be angry, but helps us avoid the trap that everything was fine before. The way I look at it, we need to be that rude press, or at least support it. The good side doesn\'t win because it\'s good.<br><br>In reply to: <a href="https://x.com/majson/status/1261189713471340545" rel="noopener noreferrer" target="_blank">@majson</a> <span class="status">1261189713471340545</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1261188887025602561',
    created: 1589525837000,
    type: 'quote',
    text: '"The world we all live in is broken and powerful people broke it" and yet "we face an enormous amount of cultural pressure not to be angry."<br><br>Quoting: <a href="https://x.com/dcwoodruff/status/1260934252423610376" rel="noopener noreferrer" target="_blank">@dcwoodruff</a> <span class="status">1260934252423610376</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1260524222746517506',
    created: 1589367368000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/romanoff2020" rel="noopener noreferrer" target="_blank">@romanoff2020</a> Congratulations on the <a class="mention" href="https://x.com/OurRevolution" rel="noopener noreferrer" target="_blank">@OurRevolution</a> endorsement! I just donated (my first) $27 to help you carry on the revolution that <a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a> started (and to not be afraid to go even further).',
    likes: 4,
    retweets: 0
  },
  {
    id: '1260522119500140544',
    created: 1589366867000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sierrahull" rel="noopener noreferrer" target="_blank">@sierrahull</a> <a class="mention" href="https://x.com/AMERICANAFEST" rel="noopener noreferrer" target="_blank">@AMERICANAFEST</a> <a class="mention" href="https://x.com/Shawn_Colvin" rel="noopener noreferrer" target="_blank">@Shawn_Colvin</a> <a class="mention" href="https://x.com/JohnPrineMusic" rel="noopener noreferrer" target="_blank">@JohnPrineMusic</a> Thank you for bringing even more ☀️ to a sunny Mother\'s Day. Your story about John Prine playing through the rain is just what we needed to break through the clouds over all our lives. I donated to <a class="mention" href="https://x.com/MusiCares" rel="noopener noreferrer" target="_blank">@MusiCares</a> and <a class="mention" href="https://x.com/hungerfreeco" rel="noopener noreferrer" target="_blank">@hungerfreeco</a> in your honor to help those who are struggling. 🎶<br><br>In reply to: <a href="https://x.com/sierrahull/status/1259504825168994304" rel="noopener noreferrer" target="_blank">@sierrahull</a> <span class="status">1259504825168994304</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1260520458723581952',
    created: 1589366471000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RecordingAcad" rel="noopener noreferrer" target="_blank">@RecordingAcad</a> <a class="mention" href="https://x.com/pickathon" rel="noopener noreferrer" target="_blank">@pickathon</a> <a class="mention" href="https://x.com/sierrahull" rel="noopener noreferrer" target="_blank">@sierrahull</a> <a class="mention" href="https://x.com/MusiCares" rel="noopener noreferrer" target="_blank">@MusiCares</a> It\'s that much fabled stage in the middle of woods! So cool to see it for real. I love concerts in the woods.<br><br>In reply to: <a href="https://x.com/RecordingAcad/status/1259936381830336521" rel="noopener noreferrer" target="_blank">@RecordingAcad</a> <span class="status">1259936381830336521</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1260514340010029056',
    created: 1589365012000,
    type: 'post',
    text: 'I know there are lots of great yoga videos out there, but the AM Yoga for your Week series by Rodney Yee has excellent pacing and has always served me well. It\'s a great way to get going again if you\'ve stopped, as I have many times. <a href="https://www.gaia.com/series/am-yoga-your-week" rel="noopener noreferrer" target="_blank">www.gaia.com/series/am-yoga-your-week</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1260499750635405313',
    created: 1589361534000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/odrotbohm" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <a class="mention" href="https://x.com/leanpub" rel="noopener noreferrer" target="_blank">@leanpub</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> The thanks really goes to <a class="mention" href="https://x.com/ysb33r" rel="noopener noreferrer" target="_blank">@ysb33r</a>.<br><br>In reply to: <a href="https://x.com/odrotbohm/status/1260475240255193088" rel="noopener noreferrer" target="_blank">@odrotbohm</a> <span class="status">1260475240255193088</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1260139615186374656',
    created: 1589275671000,
    type: 'post',
    text: 'I find that I\'m checking in with myself a lot more often during this time.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1259967920374820864',
    created: 1589234735000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/vilojona" rel="noopener noreferrer" target="_blank">@vilojona</a> <a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/MaxGrobe" rel="noopener noreferrer" target="_blank">@MaxGrobe</a> Another very excellent point. Though for me it\'s even simpler. Every life matters.<br><br>In reply to: <a href="https://x.com/vilojona/status/1259937191532380160" rel="noopener noreferrer" target="_blank">@vilojona</a> <span class="status">1259937191532380160</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1259934972829249536',
    created: 1589226880000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/MaxGrobe" rel="noopener noreferrer" target="_blank">@MaxGrobe</a> Fantastic counter point. What seems strong now potentially becomes a huge weakness later.<br><br>In reply to: <a href="https://x.com/alexsotob/status/1259932974310711296" rel="noopener noreferrer" target="_blank">@alexsotob</a> <span class="status">1259932974310711296</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1259915089001345025',
    created: 1589222139000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/MaxGrobe" rel="noopener noreferrer" target="_blank">@MaxGrobe</a> It\'s the tone and giddiness in the voice. It\'s where it\'s headed. Given it\'s a person I know too well, I can read into it.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1259845132066738177" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1259845132066738177</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1259791900149641216',
    created: 1589192769000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MaxGrobe" rel="noopener noreferrer" target="_blank">@MaxGrobe</a> And this is not the first circumstance to which they have applied this reasoning.<br><br>In reply to: <a href="#1259791017294716932">1259791017294716932</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1259791017294716932',
    created: 1589192558000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MaxGrobe" rel="noopener noreferrer" target="_blank">@MaxGrobe</a> These individuals believe that the ones who will survive this pandemic are the ones who are "strong enough" and that it\'s not worth trying to save people who aren\'t.<br><br>In reply to: <a href="https://x.com/MaxGrobe/status/1259781447835140096" rel="noopener noreferrer" target="_blank">@MaxGrobe</a> <span class="status">1259781447835140096</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1259777185629388806',
    created: 1589189261000,
    type: 'reply',
    text: 'And I really have to thank <a class="mention" href="https://x.com/thugkitchen" rel="noopener noreferrer" target="_blank">@thugkitchen</a>. They made my transition a healthy one and showed me I do not need to rely on plant-based meats.<br><br>In reply to: <a href="#1259775571405320198">1259775571405320198</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1259776193538060289',
    created: 1589189024000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/robilad" rel="noopener noreferrer" target="_blank">@robilad</a> 💯<br><br>In reply to: <a href="https://x.com/robilad/status/1259776083928514560" rel="noopener noreferrer" target="_blank">@robilad</a> <span class="status">1259776083928514560</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1259775571405320198',
    created: 1589188876000,
    type: 'post',
    text: 'So after one year, would I stop eating vegan and go back to meat? Not a chance. I\'m vegan for so many reasons (health, environment, animal rights), but even without any of that, it\'s simply a better life for me.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1259774845207732224',
    created: 1589188703000,
    type: 'post',
    text: 'And nearly all my intolerances to food (either physical or mental) have disappeared. I don\'t pick out onions or jalapenos, discovered I love leeks and bok choy, dive into avocados, totally dig carrots, and cannot resist nuts and seeds. And there\'s still so much more out there.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1259773563004809217',
    created: 1589188397000,
    type: 'post',
    text: 'I kind of thought I had tasted everything there is to taste. Again, wrong. I\'ve discovered or rediscovered so many new flavors and combinations, it was like I started eating all over again. And yet, many things I loved before I love even more in a new way.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1259772872848883713',
    created: 1589188232000,
    type: 'post',
    text: 'I couldn\'t have been more wrong. It has brought us closer together, mostly because I\'m now 100% involved in food making. We replaced going out to eat with cooking together (which turned out to be essential during quarantine). And it gives us a sense of pride when we enjoy it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1259770950796455936',
    created: 1589187774000,
    type: 'post',
    text: 'At first, I was a little scared. I didn\'t tell my spouse for a few weeks because I was worried it would make her life more difficult, and that I was being selfish.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1259770456971722752',
    created: 1589187657000,
    type: 'post',
    text: 'Today marks 1 year since I stopped eating meat (and later became a vegan). I didn\'t know what to expect when I started, other than to feel I was making a difference; taking a stand. I\'ve never been happier about what I eat. I love how it changed my life.<br><br>Thanks <a class="mention" href="https://x.com/GretaThunberg" rel="noopener noreferrer" target="_blank">@GretaThunberg</a>!<br><br>Quoting: <a href="#1129605075687677954">1129605075687677954</a>',
    likes: 14,
    retweets: 0
  },
  {
    id: '1259767800194715648',
    created: 1589187023000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BMcG1230" rel="noopener noreferrer" target="_blank">@BMcG1230</a> The world needs an abundance of Tooties right now. ♥️<br><br>In reply to: <a href="https://x.com/BMcG1230/status/1259672517142331393" rel="noopener noreferrer" target="_blank">@BMcG1230</a> <span class="status">1259672517142331393</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1259766516251103235',
    created: 1589186717000,
    type: 'post',
    text: 'It pretty depressing, maddening, and confusing to find out that members of my own family believe in eugenics. I can\'t even...',
    likes: 1,
    retweets: 0
  },
  {
    id: '1259668948187885573',
    created: 1589163455000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BMcG1230" rel="noopener noreferrer" target="_blank">@BMcG1230</a> I believe it. She connected people with facts. A direct line.<br><br>In reply to: <a href="https://x.com/BMcG1230/status/1259657465668476929" rel="noopener noreferrer" target="_blank">@BMcG1230</a> <span class="status">1259657465668476929</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1259653549937459201',
    created: 1589159784000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BMcG1230" rel="noopener noreferrer" target="_blank">@BMcG1230</a> I miss Tootie.<br><br>In reply to: <a href="https://x.com/BMcG1230/status/1259638313247543296" rel="noopener noreferrer" target="_blank">@BMcG1230</a> <span class="status">1259638313247543296</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1259605666638884864',
    created: 1589148367000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gregturn" rel="noopener noreferrer" target="_blank">@gregturn</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Those are the docs for the latest release (1.5.3). What you see on GitHub is the README for the unreleased version. Though you can switch to the 1.5.x branch for something closer: <a href="https://github.com/asciidoctor/asciidoctor-pdf/tree/v1.5.x" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-pdf/tree/v1.5.x</a><br><br>In reply to: <a href="https://x.com/gregturn/status/1259538108359548932" rel="noopener noreferrer" target="_blank">@gregturn</a> <span class="status">1259538108359548932</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1259437426499448834',
    created: 1589108256000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gregturn" rel="noopener noreferrer" target="_blank">@gregturn</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Thanks for your contribution! Future writers looking to publish to KDP will thank you too for the time you probably saved them. We writers gotta look out for one another.<br><br>In reply to: <a href="https://x.com/gregturn/status/1259101289151168512" rel="noopener noreferrer" target="_blank">@gregturn</a> <span class="status">1259101289151168512</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1258870647381307392',
    created: 1588973125000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> <a class="mention" href="https://x.com/netskymusic" rel="noopener noreferrer" target="_blank">@netskymusic</a> I often used to say that I got into programming just so I had an excuse to listen to music. I love so many genres of music, but EDM / d\'n\'b works best for programming by far.<br><br>In reply to: <a href="https://x.com/majson/status/1258864051775471617" rel="noopener noreferrer" target="_blank">@majson</a> <span class="status">1258864051775471617</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1258856701442224129',
    created: 1588969800000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/netskymusic" rel="noopener noreferrer" target="_blank">@netskymusic</a> Your garden is as epic as your drum and bass. 🙌<br><br>In reply to: <a href="https://x.com/netskymusic/status/1258855089596502020" rel="noopener noreferrer" target="_blank">@netskymusic</a> <span class="status">1258855089596502020</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1258132639422148608',
    created: 1588797170000,
    type: 'quote',
    text: 'Swoon!<br><br>Quoting: <a href="https://x.com/JoshWComeau/status/1258014903253622784" rel="noopener noreferrer" target="_blank">@JoshWComeau</a> <span class="status">1258014903253622784</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1258109929316945920',
    created: 1588791756000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/chuckfrain" rel="noopener noreferrer" target="_blank">@chuckfrain</a> You might want to check with <a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> since he\'s actually had patches accepted.<br><br>In reply to: <a href="https://x.com/chuckfrain/status/1258106276032241669" rel="noopener noreferrer" target="_blank">@chuckfrain</a> <span class="status">1258106276032241669</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1257937244842684416',
    created: 1588750585000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Java_Champions" rel="noopener noreferrer" target="_blank">@Java_Champions</a> <a class="mention" href="https://x.com/Audrey_Neveu" rel="noopener noreferrer" target="_blank">@Audrey_Neveu</a> Congratulations! I\'m really excited to have you as part of this group.<br><br>In reply to: <a href="https://x.com/Java_Champions/status/1257707314259222534" rel="noopener noreferrer" target="_blank">@Java_Champions</a> <span class="status">1257707314259222534</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1257847937612144640',
    created: 1588729292000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mairin" rel="noopener noreferrer" target="_blank">@mairin</a> A very odd omission. I\'ve added my vote to the issue: <a href="https://github.com/eclipse/che/issues/15606" rel="noopener noreferrer" target="_blank">github.com/eclipse/che/issues/15606</a><br><br>In reply to: <a href="https://x.com/mairin/status/1257530027412987904" rel="noopener noreferrer" target="_blank">@mairin</a> <span class="status">1257530027412987904</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1257733642278719488',
    created: 1588702042000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/wintermeyer" rel="noopener noreferrer" target="_blank">@wintermeyer</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> It\'s not my extension personally, but it does live in the Asciidoctor ecosystem, so I have to make sure it represents the ecosystem well.<br><br>In reply to: <a href="https://x.com/wintermeyer/status/1257732371379630080" rel="noopener noreferrer" target="_blank">@wintermeyer</a> <span class="status">1257732371379630080</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1257723971362906116',
    created: 1588699736000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/wintermeyer" rel="noopener noreferrer" target="_blank">@wintermeyer</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> I can\'t speak for other extensions, but we take pride in ours. When there\'s a problem, I want to see it resolved. But to do that, we need to communicate via the issue tracker as that\'s how we keep it healthy.<br><br>In reply to: <a href="https://x.com/wintermeyer/status/1257719988347113482" rel="noopener noreferrer" target="_blank">@wintermeyer</a> <span class="status">1257719988347113482</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1257723597881135104',
    created: 1588699647000,
    type: 'post',
    text: 'Looks like <a class="mention" href="https://x.com/MartinGarrix" rel="noopener noreferrer" target="_blank">@MartinGarrix</a> is back at it again, this time doing a live set on Dutch waters. <a href="https://youtu.be/Pnp_7IaqW74" rel="noopener noreferrer" target="_blank">youtu.be/Pnp_7IaqW74</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1257716241277399042',
    created: 1588697893000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/wintermeyer" rel="noopener noreferrer" target="_blank">@wintermeyer</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Have you checked the issue tracker? It appears that other users are reporting problems as well. Perhaps it\'s not working with the latest update of VSCode? (I\'m not a user of it myself, so I\'m not familiar with the situation).<br><br>In reply to: <a href="https://x.com/wintermeyer/status/1257582751693316096" rel="noopener noreferrer" target="_blank">@wintermeyer</a> <span class="status">1257582751693316096</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1257513301342162944',
    created: 1588649509000,
    type: 'post',
    text: 'I don\'t know who needs to hear this, but we don\'t have to go to another planet to create a better society.',
    likes: 4,
    retweets: 1
  },
  {
    id: '1257471518566580228',
    created: 1588639547000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/snicoll" rel="noopener noreferrer" target="_blank">@snicoll</a> Totally.<br><br>In reply to: <a href="https://x.com/snicoll/status/1257304873764188161" rel="noopener noreferrer" target="_blank">@snicoll</a> <span class="status">1257304873764188161</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1257218102485377024',
    created: 1588579128000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ALRubinger" rel="noopener noreferrer" target="_blank">@ALRubinger</a> Whoa! And damn, we had times. Munich and our Euro tour always comes to mind first. And there will be more. As I like to say, your contract with OSS never runs out...and that means neither does our time together. Onwards! Oh, and welcome to the band of traitors. ▶️<br><br>In reply to: <a href="https://x.com/ALRubinger/status/1257214184669868032" rel="noopener noreferrer" target="_blank">@ALRubinger</a> <span class="status">1257214184669868032</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1256884594193793025',
    created: 1588499613000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/netskymusic" rel="noopener noreferrer" target="_blank">@netskymusic</a> Thank you for your streams (and for helping people #StayAtHome). Antwerp is my favorite place in the world to visit and listening to your sets from your apartment took me on a trip there. Thanks for that gift! I look forward to visiting you again virtually. 🍻',
    likes: 1,
    retweets: 0,
    tags: ['stayathome']
  },
  {
    id: '1256883674525478912',
    created: 1588499394000,
    type: 'post',
    text: 'Stories of open source connecting us still amaze me, even to this day. Though an Antora community member, <a class="mention" href="https://x.com/brunchboy" rel="noopener noreferrer" target="_blank">@brunchboy</a>, I discovered <a class="mention" href="https://x.com/netskymusic" rel="noopener noreferrer" target="_blank">@netskymusic</a>, who uses software developed by <a class="mention" href="https://x.com/brunchboy" rel="noopener noreferrer" target="_blank">@brunchboy</a> that itself uses Antora for docs. Now I\'m coding to the drum &amp; bass by <a class="mention" href="https://x.com/netskymusic" rel="noopener noreferrer" target="_blank">@netskymusic</a>. 🌍',
    likes: 8,
    retweets: 0
  },
  {
    id: '1256667216889536512',
    created: 1588447786000,
    type: 'post',
    text: 'This UI is nicely showing off many of the features of an Antora-based docs site. <a href="https://boozallen.github.io/sdp-docs/jte/1.7.1/pipeline-templating/what_is_a_pipeline_template.html" rel="noopener noreferrer" target="_blank">boozallen.github.io/sdp-docs/jte/1.7.1/pipeline-templating/what_is_a_pipeline_template.html</a> Well done.',
    likes: 11,
    retweets: 0
  },
  {
    id: '1256518808174358528',
    created: 1588412403000,
    type: 'post',
    text: 'And even if there are payment details on file, I should be allowed to set my own limits (controlling how much overage is allowed before builds are disabled).',
    likes: 1,
    retweets: 0
  },
  {
    id: '1256518165237870593',
    created: 1588412250000,
    type: 'post',
    text: 'I\'m all for paying for computing resources. But if I never give my payment details, then there was never any understanding of a transaction, and I don\'t expect to get billed. They should disable builds if the account exceeds the limit and there are no payment details on file.',
    likes: 4,
    retweets: 1
  },
  {
    id: '1256517407654330373',
    created: 1588412069000,
    type: 'post',
    text: 'I\'m not pleased about the fact that Netlify lets you rack up build minutes over the limit on the free plan, then asks for your credit card so you can pay for that overage. That\'s a sketchy business practice to me.',
    likes: 7,
    retweets: 2
  },
  {
    id: '1256122989927489536',
    created: 1588318033000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> Just tried it. GitLab provides exactly what I need. And it\'s push button. So simple. cc: <a class="mention" href="https://x.com/ysb33r" rel="noopener noreferrer" target="_blank">@ysb33r</a><br><br>In reply to: <a href="#1256118804301545472">1256118804301545472</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1256118804301545472',
    created: 1588317035000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> Yes, I\'m looking for password protection for the static HTML files on the server (as clients often request this for testing and early access sites). I was not aware that\'s something GitLab Pages offers. Good to know! <a href="https://docs.gitlab.com/ce/user/project/pages/pages_access_control.html" rel="noopener noreferrer" target="_blank">docs.gitlab.com/ce/user/project/pages/pages_access_control.html</a><br><br>In reply to: <a href="https://x.com/RalfDMueller/status/1256115772658724865" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <span class="status">1256115772658724865</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1256005650083016705',
    created: 1588290057000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> Is it possible to use Okta to add password protection to static sites? If so, do you have any material available I could read on the subject?',
    likes: 1,
    retweets: 1
  },
  {
    id: '1256005210658320384',
    created: 1588289952000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/chuckfrain" rel="noopener noreferrer" target="_blank">@chuckfrain</a> Basically, straightforward password protection for static sites is completely inaccessible right now (except for Netlify\'s prohibitively expensive plans). This should be maybe $10 / month at most.<br><br>In reply to: <a href="#1256004810593071104">1256004810593071104</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1256004810593071104',
    created: 1588289856000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/chuckfrain" rel="noopener noreferrer" target="_blank">@chuckfrain</a> I\'m looking for something a non-technical person could use that\'s provided by a web host that doesn\'t require access to the web server configuration...and which ideally would support oauth or some sort of user management.<br><br>In reply to: <a href="https://x.com/chuckfrain/status/1256000959907540992" rel="noopener noreferrer" target="_blank">@chuckfrain</a> <span class="status">1256000959907540992</span>',
    likes: 0,
    retweets: 1
  },
  {
    id: '1255995909080412160',
    created: 1588287734000,
    type: 'post',
    text: 'Straightforward password protection for static sites. What options are out there? (aside from Netlify\'s Pro or Enterprise plan)',
    likes: 0,
    retweets: 0
  },
  {
    id: '1255762485832867840',
    created: 1588232082000,
    type: 'post',
    text: 'I was out getting groceries tonight, and unfortunately the attitude in Colorado seems to be, "look, we tried, but we\'re not taking this seriously anymore." It\'s really discouraging. (Especially the jerk totally ignoring social distancing going around hitting on all the women).',
    likes: 1,
    retweets: 0
  },
  {
    id: '1255599727539716096',
    created: 1588193277000,
    type: 'post',
    text: 'I really wish that browsers would scroll an anchor to the top of the visible region (aware of elements that would cover it, like a navbar) instead of the top of the viewport. There are workarounds, but why? Can\'t browsers just do the right thing?',
    likes: 7,
    retweets: 0
  },
  {
    id: '1255004442367340547',
    created: 1588051350000,
    type: 'post',
    text: 'If I manage to make it through this (mentally), <a class="mention" href="https://x.com/MartinGarrix" rel="noopener noreferrer" target="_blank">@MartinGarrix</a> will have played a key role. It feels good to dance around each night to feel alive. <a href="https://youtu.be/1jQYrvFf5NI" rel="noopener noreferrer" target="_blank">youtu.be/1jQYrvFf5NI</a>. Music is a great healer. 🕺🎶',
    likes: 0,
    retweets: 0
  },
  {
    id: '1254730724218789889',
    created: 1587986091000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> <a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> See <a href="https://gitlab.com/antora/antora/-/issues/368" rel="noopener noreferrer" target="_blank">gitlab.com/antora/antora/-/issues/368</a><br><br>In reply to: <a href="https://x.com/alexsotob/status/1254729934972518400" rel="noopener noreferrer" target="_blank">@alexsotob</a> <span class="status">1254729934972518400</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1254018214930952193',
    created: 1587816215000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GretaThunberg" rel="noopener noreferrer" target="_blank">@GretaThunberg</a> Brilliant.<br><br>In reply to: <a href="https://x.com/GretaThunberg/status/1253669277724553218" rel="noopener noreferrer" target="_blank">@GretaThunberg</a> <span class="status">1253669277724553218</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1253756345632739328',
    created: 1587753781000,
    type: 'reply',
    text: '@TedAtCIS <a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> That would be best. Overloading .txt doesn\'t help tools understand what it contains (aside from a very OS-specific behavior of launching a basic text editor). If we all use .adoc, then tools can be updated to do the right thing (like Antora, for example).<br><br>In reply to: @TedAtCIS <span class="status">&lt;deleted&gt;</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1253460000103518209',
    created: 1587683126000,
    type: 'post',
    text: 'I have seen several reports now where the Ruby that Homebrew installs performs terribly. This is why I always recommend installing Ruby for running programs installed in your home directory using <a href="http://rvm.io" rel="noopener noreferrer" target="_blank">rvm.io</a>.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1253281632561229825',
    created: 1587640600000,
    type: 'post',
    text: 'Every day is #EarthDay',
    likes: 1,
    retweets: 0,
    tags: ['earthday']
  },
  {
    id: '1253248339560157184',
    created: 1587632663000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> @TedAtCIS I recommend skimming the feature list. <a href="https://github.com/asciidoctor/asciidoctor-intellij-plugin/blob/master/FEATURES.adoc" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-intellij-plugin/blob/master/FEATURES.adoc</a><br><br>In reply to: <a href="#1253247984285872130">1253247984285872130</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1253247984285872130',
    created: 1587632578000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> @TedAtCIS It has deep awareness of the structure of the document and of references. And it has first-class support, intellisense, code completion, and refactoring for Antora.<br><br>In reply to: <a href="https://x.com/settermjd/status/1253247276115546113" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1253247276115546113</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1253244813517836288',
    created: 1587631822000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/OJourdan" rel="noopener noreferrer" target="_blank">@OJourdan</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> And thank you for bringing Asciidoctor to labs around the world!<br><br>In reply to: <a href="https://x.com/OJourdan/status/1253237019595202560" rel="noopener noreferrer" target="_blank">@OJourdan</a> <span class="status">1253237019595202560</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1253231214648934400',
    created: 1587628580000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> @TedAtCIS My hope is that the IntelliJ plugin is sketching the blueprint that plugins for other IDEs / editors will follow.<br><br>In reply to: <a href="#1253230660828803072">1253230660828803072</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1253230660828803072',
    created: 1587628448000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> @TedAtCIS While I use VIM, Sarah uses Atom to do all her AsciiDoc editing and enjoys it. However, the best experience with AsciiDoc editing right now, hands down, is IntelliJ. It\'s on a whole other level.<br><br>In reply to: <a href="https://x.com/settermjd/status/1253230027824644096" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1253230027824644096</span>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1253227951832760320',
    created: 1587627802000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/OJourdan" rel="noopener noreferrer" target="_blank">@OJourdan</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Your project has a very special meaning to me because it was through LabVIEW that I discovered my passion for programming and logic. It\'s surreal seeing the Asciidoctor function in LabVIEW. Little did my past self know I\'d one day see a program I wrote in the palette.<br><br>In reply to: <a href="https://x.com/OJourdan/status/1253216896322805760" rel="noopener noreferrer" target="_blank">@OJourdan</a> <span class="status">1253216896322805760</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1252905948470849537',
    created: 1587551030000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> <a class="mention" href="https://x.com/amisspeltyouth" rel="noopener noreferrer" target="_blank">@amisspeltyouth</a> That topic needs a little <a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> treatment ;)<br><br>In reply to: <a href="https://x.com/settermjd/status/1252864207126540288" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1252864207126540288</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1252845021792886792',
    created: 1587536504000,
    type: 'post',
    text: 'If you\'re looking for some sentimental EDM to help get you through, check out this live set <a class="mention" href="https://x.com/MartinGarrix" rel="noopener noreferrer" target="_blank">@MartinGarrix</a> played from his rooftop last week. <a href="https://youtu.be/tspNk3SwZ9s" rel="noopener noreferrer" target="_blank">youtu.be/tspNk3SwZ9s</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1252840878017662976',
    created: 1587535516000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> 👆 That community member is <a class="mention" href="https://x.com/stoeps" rel="noopener noreferrer" target="_blank">@stoeps</a>.<br><br>In reply to: <a href="https://x.com/antoraproject/status/1252355912498470914" rel="noopener noreferrer" target="_blank">@antoraproject</a> <span class="status">1252355912498470914</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1252524666817830912',
    created: 1587460126000,
    type: 'quote',
    text: 'We have to rethink the priorities of our society. It\'s time to restore dignity to where it was lost (or perhaps was never there to begin with). And that starts with our food and the people who harvest it. I fight for these workers.<br><br>Quoting: <a href="https://x.com/ajplus/status/1252522462652358657" rel="noopener noreferrer" target="_blank">@ajplus</a> <span class="status">1252522462652358657</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1252318501156433920',
    created: 1587410972000,
    type: 'quote',
    text: 'Asciidoctor output inside of Asciidoctor output. So meta.<br><br>Quoting: <a href="https://x.com/vilimpoc/status/1252243357113495559" rel="noopener noreferrer" target="_blank">@vilimpoc</a> <span class="status">1252243357113495559</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1252050304813555712',
    created: 1587347029000,
    type: 'quote',
    text: 'This is America.<br><br>Quoting: <a href="https://x.com/AndrewRomanoff/status/1251968947492184072" rel="noopener noreferrer" target="_blank">@AndrewRomanoff</a> <span class="status">1251968947492184072</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1252049601894346753',
    created: 1587346861000,
    type: 'reply',
    text: 'Beer acquired. Thank you <a class="mention" href="https://x.com/IronMuleBrewery" rel="noopener noreferrer" target="_blank">@IronMuleBrewery</a> and Wild Blue Yonder Brewing! Now I just need to decide what I want to do at home while drinking your concoctions.<br><br>In reply to: <a href="#1251718413967392770">1251718413967392770</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1251776556449492992',
    created: 1587281762000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rmoff" rel="noopener noreferrer" target="_blank">@rmoff</a> I do, and I use podman instead of Docker. So I think it\'s either one (Docker w/o SELinux) or the other (podman w/ SELinux).<br><br>In reply to: <a href="https://x.com/rmoff/status/1251505541861564416" rel="noopener noreferrer" target="_blank">@rmoff</a> <span class="status">1251505541861564416</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1251758627188174849',
    created: 1587277487000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ryguyrg" rel="noopener noreferrer" target="_blank">@ryguyrg</a> I\'m glad to hear that. Thanks for sharing.<br><br>In reply to: <a href="https://x.com/ryguyrg/status/1251757310046687233" rel="noopener noreferrer" target="_blank">@ryguyrg</a> <span class="status">1251757310046687233</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1251756742511169537',
    created: 1587277038000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ryguyrg" rel="noopener noreferrer" target="_blank">@ryguyrg</a> It\'s not just e-mail. I\'m seeing it in almost all social media platforms as well as phone calls and text messages, and even warnings from financial institutions notifying me of blocking stuff. It\'s like an onslaught of scams.<br><br>In reply to: <a href="https://x.com/ryguyrg/status/1251755800210493442" rel="noopener noreferrer" target="_blank">@ryguyrg</a> <span class="status">1251755800210493442</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1251747627743367171',
    created: 1587274865000,
    type: 'post',
    text: 'Is it just me, or has spam increased dramatically during this pandemic? I\'m used to dealing with a lot of crap, but this is just getting out of hand.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1251718413967392770',
    created: 1587267900000,
    type: 'post',
    text: 'Getting ready for the all-important beer run tomorrow to refresh the spirits and support local business (those that are offering curb-side pickup).',
    likes: 1,
    retweets: 0
  },
  {
    id: '1251718025163812865',
    created: 1587267807000,
    type: 'post',
    text: 'This sounds kind of funny, but the focus on being vegan has actually provided much needed structure in these trying told. The time spent with my spouse researching and cooking new recipes will be something I\'ll carry with me forever.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1251716448915001346',
    created: 1587267431000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bk2204" rel="noopener noreferrer" target="_blank">@bk2204</a> Just made my first one last week (out of the Thug Kitchen cookbook). Won\'t be the last. Easy and breakfast for dinner. What\'s not to love?<br><br>In reply to: <a href="https://x.com/bk2204/status/1251690810925596673" rel="noopener noreferrer" target="_blank">@bk2204</a> <span class="status">1251690810925596673</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1251715052966744065',
    created: 1587267099000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/TheTomFlaherty" rel="noopener noreferrer" target="_blank">@TheTomFlaherty</a> <a class="mention" href="https://x.com/Noahpinion" rel="noopener noreferrer" target="_blank">@Noahpinion</a> <a class="mention" href="https://x.com/patrickc" rel="noopener noreferrer" target="_blank">@patrickc</a> I love your point about respect for this public sector. That\'s so true. The public sector values us first, so it would serve us well to respect it. Yeah, it\'s messy, but so is everything. It\'s about priorities.<br><br>In reply to: <a href="https://x.com/TheTomFlaherty/status/1251688434889920512" rel="noopener noreferrer" target="_blank">@TheTomFlaherty</a> <span class="status">1251688434889920512</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1251562055578066945',
    created: 1587230621000,
    type: 'reply',
    text: '@rschuetzler Exactly.<br><br>In reply to: @rschuetzler <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1251480088182321152',
    created: 1587211079000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sebi2706" rel="noopener noreferrer" target="_blank">@sebi2706</a> <a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> @mwessendorf I\'m my mind, the best mask is just to stay at home (for those who can). If I\'m out, it\'s just a dash.<br><br>In reply to: <a href="https://x.com/sebi2706/status/1251462061273554946" rel="noopener noreferrer" target="_blank">@sebi2706</a> <span class="status">1251462061273554946</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1251460012016234496',
    created: 1587206292000,
    type: 'reply',
    text: '@mwessendorf I have that feeling too. I know it\'s what we have to do. I know it\'s how we get through this with the fewest lives lost. But it still leaves me feeling sad.<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1251456877315014656',
    created: 1587205545000,
    type: 'post',
    text: 'Funny thing about software. People always tell you, don\'t overthink it. Just keep it simple. And yet, people always want it to do way more than what you were overthinking. So the truth is, people don\'t want simple. And that\'s the force you need to keep pushing back against.',
    likes: 11,
    retweets: 2
  },
  {
    id: '1251415680886267904',
    created: 1587195723000,
    type: 'post',
    text: 'Tonight my freezer fed me. Thank you past self.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1250625174832951297',
    created: 1587007251000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mairin" rel="noopener noreferrer" target="_blank">@mairin</a> You will never be erased to me. My respect and appreciation for your work, dedication, and legacy is timeless.<br><br>In reply to: <a href="https://x.com/mairin/status/1250513919011164165" rel="noopener noreferrer" target="_blank">@mairin</a> <span class="status">1250513919011164165</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1250586082808381440',
    created: 1586997931000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jflory7" rel="noopener noreferrer" target="_blank">@jflory7</a> <a class="mention" href="https://x.com/fedora" rel="noopener noreferrer" target="_blank">@fedora</a> <a class="mention" href="https://x.com/fedoracommunity" rel="noopener noreferrer" target="_blank">@fedoracommunity</a> Workin\' on it ;)<br><br>In reply to: <a href="https://x.com/jflory7/status/1250482364272250880" rel="noopener noreferrer" target="_blank">@jflory7</a> <span class="status">1250482364272250880</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1250559575939444736',
    created: 1586991611000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> <a class="mention" href="https://x.com/tlberglund" rel="noopener noreferrer" target="_blank">@tlberglund</a> Personally, I find someone saying something "wrong" interesting. It\'s like spice. I\'ll often laugh about it because I like it.<br><br>In reply to: <a href="#1250559027496382465">1250559027496382465</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1250559027496382465',
    created: 1586991481000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> <a class="mention" href="https://x.com/tlberglund" rel="noopener noreferrer" target="_blank">@tlberglund</a> For native speakers in any language, it largely depends on personal experience. Native speakers don\'t know grammar as much they do patterns. A phrase sounds wrong because it\'s not familiar, not because you can explain the rules. Sometimes saying it wrong is actually right.<br><br>In reply to: <a href="https://x.com/brunoborges/status/1250555013866254336" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1250555013866254336</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1250490351233585153',
    created: 1586975107000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/melissajmckay" rel="noopener noreferrer" target="_blank">@melissajmckay</a> <a class="mention" href="https://x.com/ShlomiBenHaim" rel="noopener noreferrer" target="_blank">@ShlomiBenHaim</a> That is incredibly cool.<br><br>In reply to: <a href="https://x.com/melissajmckay/status/1250486234591358976" rel="noopener noreferrer" target="_blank">@melissajmckay</a> <span class="status">1250486234591358976</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1250466355444244481',
    created: 1586969386000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/petitlaurent" rel="noopener noreferrer" target="_blank">@petitlaurent</a> <a class="mention" href="https://x.com/evanchooly" rel="noopener noreferrer" target="_blank">@evanchooly</a> Oooooh, that\'s nice! Thanks for sharing.<br><br>In reply to: <a href="https://x.com/petitlaurent/status/1250464067006980102" rel="noopener noreferrer" target="_blank">@petitlaurent</a> <span class="status">1250464067006980102</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1250249982965198848',
    created: 1586917799000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/evanchooly" rel="noopener noreferrer" target="_blank">@evanchooly</a> That\'s basically what I settled on. It works, just not as elegant. Though I\'m curious if there\'s any other language that does allow it.<br><br>In reply to: <a href="https://x.com/evanchooly/status/1250239302258298880" rel="noopener noreferrer" target="_blank">@evanchooly</a> <span class="status">1250239302258298880</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1250238129488855040',
    created: 1586914973000,
    type: 'post',
    text: 'You know a musician is the realest of the real when that person has to take a peek at the track list on the record to figure out which song to play live next. 🎸',
    likes: 1,
    retweets: 0
  },
  {
    id: '1250237637832478721',
    created: 1586914855000,
    type: 'post',
    text: 'I wish I could destructure an object in JavaScript and still be able to access the object being destructured. There are plenty of times when I\'ve needed to do that.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1249945379329568768',
    created: 1586845175000,
    type: 'quote',
    text: '<a class="mention" href="https://x.com/fanf42" rel="noopener noreferrer" target="_blank">@fanf42</a> Thought you might appreciate this thread.<br><br>Quoting: <a href="https://x.com/karlitaliliana/status/1249890783798251521" rel="noopener noreferrer" target="_blank">@karlitaliliana</a> <span class="status">1249890783798251521</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1249937567853641729',
    created: 1586843313000,
    type: 'quote',
    text: 'If you\'re looking for some great live content to keep you connected during this #StayHome, check out this concert by <a class="mention" href="https://x.com/sierrahull" rel="noopener noreferrer" target="_blank">@sierrahull</a>. Her musical excellence and heartwarming personality will be sure to lift your spirits.<br><br>Quoting: <a href="https://x.com/sierrahull/status/1249737423808081923" rel="noopener noreferrer" target="_blank">@sierrahull</a> <span class="status">1249737423808081923</span>',
    likes: 0,
    retweets: 0,
    tags: ['stayhome']
  },
  {
    id: '1249906466405748737',
    created: 1586835898000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/downey" rel="noopener noreferrer" target="_blank">@downey</a> Yep.<br><br>In reply to: @downey <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1249839357319467008',
    created: 1586819898000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/chuckfrain" rel="noopener noreferrer" target="_blank">@chuckfrain</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> You can find the spec here: <a href="https://gitlab.com/antora/antora/-/blob/master/packages/playbook-builder/lib/config/schema.js" rel="noopener noreferrer" target="_blank">gitlab.com/antora/antora/-/blob/master/packages/playbook-builder/lib/config/schema.js</a><br><br>In reply to: <a href="https://x.com/chuckfrain/status/1249439279564099584" rel="noopener noreferrer" target="_blank">@chuckfrain</a> <span class="status">1249439279564099584</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1249831752375226368',
    created: 1586818085000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dejcup" rel="noopener noreferrer" target="_blank">@dejcup</a> <a class="mention" href="https://x.com/ysb33r" rel="noopener noreferrer" target="_blank">@ysb33r</a> <a class="mention" href="https://x.com/bobbytank42" rel="noopener noreferrer" target="_blank">@bobbytank42</a> <a class="mention" href="https://x.com/ahus1de" rel="noopener noreferrer" target="_blank">@ahus1de</a> <a class="mention" href="https://x.com/intellijidea" rel="noopener noreferrer" target="_blank">@intellijidea</a> This would be an excellent example of something the tooling group under the AsciiDoc WG would discuss and agree on.<br><br>In reply to: <a href="https://x.com/dejcup/status/1249350955847614465" rel="noopener noreferrer" target="_blank">@dejcup</a> <span class="status">1249350955847614465</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1249831507205611520',
    created: 1586818026000,
    type: 'reply',
    text: '@alexkluew_dev There\'s a more current cheatsheet available here: <a href="https://asciidoctor.org/docs/asciidoc-syntax-quick-reference/" rel="noopener noreferrer" target="_blank">asciidoctor.org/docs/asciidoc-syntax-quick-reference/</a><br><br>In reply to: <a href="https://x.com/alexeimakes/status/1249747985325600768" rel="noopener noreferrer" target="_blank">@alexeimakes</a> <span class="status">1249747985325600768</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1249625375434403840',
    created: 1586768881000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DevoxxMA" rel="noopener noreferrer" target="_blank">@DevoxxMA</a> I\'m sorry you have to face this difficult situation and I thank you for your leadership. Stay safe. Stay strong. 🇲🇦<br><br>In reply to: <a href="https://x.com/DevoxxMA/status/1249623360029560832" rel="noopener noreferrer" target="_blank">@DevoxxMA</a> <span class="status">1249623360029560832</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1249595678851887104',
    created: 1586761800000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/zepag" rel="noopener noreferrer" target="_blank">@zepag</a> Right! Exactly. One of my common sayings now is "that\'s just what it is." And that sort of releases me to focus on what\'s next instead of what\'s past.<br><br>In reply to: <a href="https://x.com/zepag/status/1249595125380120577" rel="noopener noreferrer" target="_blank">@zepag</a> <span class="status">1249595125380120577</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1249595112771866624',
    created: 1586761665000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/zepag" rel="noopener noreferrer" target="_blank">@zepag</a> That\'s very similar to the progression I went through over the years. I gradually stopped being so hard on myself and just learned to accept life as the messy thing it is. (And, much to my dismay, life started to go smoother too).<br><br>In reply to: <a href="https://x.com/zepag/status/1249593317718974464" rel="noopener noreferrer" target="_blank">@zepag</a> <span class="status">1249593317718974464</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1249591479909019651',
    created: 1586760799000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/zepag" rel="noopener noreferrer" target="_blank">@zepag</a> What I know about myself is that it\'s complicated. I like to think of it that I don\'t enjoy pointless / trivial interactions. Rather, I guard my time so I\'m able to put my whole self into interactions (e.g., a conference). The energy that takes requires frequent recharge.<br><br>In reply to: <a href="#1249590147802251265">1249590147802251265</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1249590147802251265',
    created: 1586760482000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/zepag" rel="noopener noreferrer" target="_blank">@zepag</a> ...and so I wonder if this is sort of self-therapy for overcoming that assumption / mindset.<br><br>In reply to: <a href="#1249589945666158593">1249589945666158593</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1249589945666158593',
    created: 1586760433000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/zepag" rel="noopener noreferrer" target="_blank">@zepag</a> I\'m not thinking about it as being that deep. And, in fact, what you\'re saying is kind of my point. Someone who has been identifying as being an introvert, then realizing that\'s not it at all. In other words, getting in touch with person in the mirror.<br><br>In reply to: <a href="https://x.com/zepag/status/1249587228151615490" rel="noopener noreferrer" target="_blank">@zepag</a> <span class="status">1249587228151615490</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1249424658656419840',
    created: 1586721026000,
    type: 'post',
    text: 'I wonder how many people have discovered they\'re actually an introvert. (Or vice versa).',
    likes: 4,
    retweets: 0
  },
  {
    id: '1249216048479367168',
    created: 1586671289000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ambernoelle" rel="noopener noreferrer" target="_blank">@ambernoelle</a> The Giving Tree. No question. I can imagine my father reading it to my brother and I like it was yesterday.<br><br>In reply to: <a href="https://x.com/ambernoelle/status/1249081416442425346" rel="noopener noreferrer" target="_blank">@ambernoelle</a> <span class="status">1249081416442425346</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1249176242152247305',
    created: 1586661799000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/sierrahull" rel="noopener noreferrer" target="_blank">@sierrahull</a> After today\'s jam session, we got Justin\'s latest album to listen to while a-cookin\'. Wow! 🎶😍🎶',
    likes: 1,
    retweets: 0
  },
  {
    id: '1249124296041148417',
    created: 1586649414000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/sierrahull" rel="noopener noreferrer" target="_blank">@sierrahull</a> You and Justin\'s #StayAtHome concerts are bringing so much joy into our home. You\'re such sweethearts. For today\'s session, both my spouse and I were listening in. Thank you! From our family to yours, we\'ll be raising our 🍺🍷 to your health tonight.',
    likes: 1,
    retweets: 0,
    tags: ['stayathome']
  },
  {
    id: '1248534624403197953',
    created: 1586508825000,
    type: 'post',
    text: 'Sweet semver calculator. Super useful when defining your dependencies.<br><br><a href="https://semver.npmjs.com/" rel="noopener noreferrer" target="_blank">semver.npmjs.com/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1248393435347734539',
    created: 1586475163000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JordanChariton" rel="noopener noreferrer" target="_blank">@JordanChariton</a> <a class="mention" href="https://x.com/jennelizabethj" rel="noopener noreferrer" target="_blank">@jennelizabethj</a> <a class="mention" href="https://x.com/StatusCoup" rel="noopener noreferrer" target="_blank">@StatusCoup</a> 😥<br><br>In reply to: <a href="https://x.com/JordanChariton/status/1247233241813319680" rel="noopener noreferrer" target="_blank">@JordanChariton</a> <span class="status">1247233241813319680</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1248040586071781376',
    created: 1586391037000,
    type: 'post',
    text: 'I\'m incensed. There\'s no other way to put it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1247972208586059776',
    created: 1586374735000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fanf42" rel="noopener noreferrer" target="_blank">@fanf42</a> Same.<br><br>In reply to: <a href="https://x.com/fanf42/status/1247969283331817474" rel="noopener noreferrer" target="_blank">@fanf42</a> <span class="status">1247969283331817474</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1247963450417938433',
    created: 1586372647000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sunrisemvmt" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a> Your resounding endorsement of <a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a> is something I\'ll never forget. Thank you for showing us the side of history we should all be on.<br><br>In reply to: <a href="https://x.com/sunrisemvmt/status/1247918264539320320" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a> <span class="status">1247918264539320320</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1247963071600930816',
    created: 1586372556000,
    type: 'quote',
    text: 'I will continue fighting for those whom I don\'t know. It might be in my own way, but that just makes it all the more sincere.<br><br>Quoting: <a href="https://x.com/sunrisemvmt/status/1247937463617757186" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a> <span class="status">1247937463617757186</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1247961347767144448',
    created: 1586372145000,
    type: 'post',
    text: 'The next 4 years are going to be brutal.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1247960794286841856',
    created: 1586372014000,
    type: 'quote',
    text: 'My emotions want to get the better of me, but I won\'t let them fill today with negativity. Instead, I\'ll share that Bernie is the first person I\'ve ever called my hero. He totally changed the way I look at the world. He\'ll always be my President, no matter what position he holds.<br><br>Quoting: <a href="https://x.com/OurRevolution/status/1247932217449472006" rel="noopener noreferrer" target="_blank">@OurRevolution</a> <span class="status">1247932217449472006</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1247830198403207168',
    created: 1586340877000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> Currently, it can only be a directory (string value) or an array of files. It can\'t be mixed.<br><br>In reply to: <a href="https://x.com/alexsotob/status/1247799810188443651" rel="noopener noreferrer" target="_blank">@alexsotob</a> <span class="status">1247799810188443651</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1247748918512734208',
    created: 1586321498000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lillydizzle" rel="noopener noreferrer" target="_blank">@lillydizzle</a> 💔 he\'s a\'sittin\' on a 🌈 now, and forever.<br><br>In reply to: <a href="https://x.com/lillydizzle/status/1247742832812503040" rel="noopener noreferrer" target="_blank">@lillydizzle</a> <span class="status">1247742832812503040</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1247662414704676865',
    created: 1586300874000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cmaki" rel="noopener noreferrer" target="_blank">@cmaki</a> I\'d love to have chat with you about this and catch up! And certainly looking forward to the blog post. All articles out there seem to be focused on React-based admin apps instead of basic password protection of the site itself. It\'s like a whole use case is being missed.<br><br>In reply to: <a href="https://x.com/cmaki/status/1247363431918784512" rel="noopener noreferrer" target="_blank">@cmaki</a> <span class="status">1247363431918784512</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1247657054644232192',
    created: 1586299596000,
    type: 'post',
    text: 'The sensitive variable policy on Netlify is the right spirit, but the wrong scope. It\'s not just the environment variables you want to protect. It\'s the WHOLE build configuration. <a href="https://docs.netlify.com/configure-builds/environment-variables/#sensitive-variable-policy" rel="noopener noreferrer" target="_blank">docs.netlify.com/configure-builds/environment-variables/#sensitive-variable-policy</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1247656558529404928',
    created: 1586299478000,
    type: 'post',
    text: 'On Netlify, the deploy preview branch should use the command (and perhaps the whole netlify.toml) from the target branch, not the PR branch. (The exception would be for site members). Otherwise, any user submitting a PR can modify the command the Netlify build runs. That\'s bad.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1247651275874787329',
    created: 1586298219000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> <a class="mention" href="https://x.com/zepag" rel="noopener noreferrer" target="_blank">@zepag</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> It\'s even simpler than that.<br><br>[source]<br>----<br>ifdef::bool1[include::Foo.java[]]<br>----<br><br>ifdef, ifndef, ifeval, and include are preprocessor directives, so they don\'t "see" the document structure. That\'s why they work everywhere.<br><br>In reply to: <a href="https://x.com/agoncal/status/1247514098725728261" rel="noopener noreferrer" target="_blank">@agoncal</a> <span class="status">1247514098725728261</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1247613007955255301',
    created: 1586289095000,
    type: 'post',
    text: 'Are there any specs for a static website manifest that goes beyond what a sitemap can hold (and even beyond a web app manifest)? The idea is to feed information into a tool that manages and validates cross-site references.',
    likes: 0,
    retweets: 1
  },
  {
    id: '1247448804623712256',
    created: 1586249946000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rmannibucau" rel="noopener noreferrer" target="_blank">@rmannibucau</a> So more accurate to say that syntax highlighters are often not thread safe.<br><br>In reply to: <a href="https://x.com/rmannibucau/status/1247440368041418752" rel="noopener noreferrer" target="_blank">@rmannibucau</a> <span class="status">1247440368041418752</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1247448635693862912',
    created: 1586249905000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rmannibucau" rel="noopener noreferrer" target="_blank">@rmannibucau</a> It looks like Rouge is not thread safe with regard to how it registers themes. If you require \'rouge\' into the Ruby runtime eagerly, it should solve it. I recommend filing an issue with rouge. I can\'t control the behavior of dependencies.<br><br>In reply to: <a href="https://x.com/rmannibucau/status/1247446509790081026" rel="noopener noreferrer" target="_blank">@rmannibucau</a> <span class="status">1247446509790081026</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1247444799633780736',
    created: 1586248991000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rmannibucau" rel="noopener noreferrer" target="_blank">@rmannibucau</a> Try without the syntax highlighter. If that still gives you an empty HTML, please submit a sample project so that I can study it. We are very careful to ensure thread safety in Asciidoctor core, and if that safety is being violated, I want to know about it.<br><br>In reply to: <a href="https://x.com/rmannibucau/status/1247443646485532672" rel="noopener noreferrer" target="_blank">@rmannibucau</a> <span class="status">1247443646485532672</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1247444231360090113',
    created: 1586248855000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rmannibucau" rel="noopener noreferrer" target="_blank">@rmannibucau</a> The problem seems to be that *CodeRay* is not thread safe. I recommend using Rouge instead.<br><br>In reply to: <a href="#1247444032365539329">1247444032365539329</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1247444032365539329',
    created: 1586248808000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rmannibucau" rel="noopener noreferrer" target="_blank">@rmannibucau</a> The require statement should already be thread safe. That\'s something JRuby handles. If not, report to JRuby.<br><br>In reply to: <a href="https://x.com/rmannibucau/status/1247443646485532672" rel="noopener noreferrer" target="_blank">@rmannibucau</a> <span class="status">1247443646485532672</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1247440993894436864',
    created: 1586248083000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rmannibucau" rel="noopener noreferrer" target="_blank">@rmannibucau</a> It should be. These converters don\'t rely on global state. If you can point to where there is a problem, I\'d be happy to look into it.<br><br>In reply to: <a href="https://x.com/rmannibucau/status/1247440368041418752" rel="noopener noreferrer" target="_blank">@rmannibucau</a> <span class="status">1247440368041418752</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1247344955665731585',
    created: 1586225186000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jm" rel="noopener noreferrer" target="_blank">@jm</a> What\'s bizarre is that I can get the authentication part working with the provided widget on the free plan. I just can\'t use it to enforce authorization. So I fail to see the point of having it.<br><br>In reply to: <a href="https://x.com/jm/status/1247343484916416517" rel="noopener noreferrer" target="_blank">@jm</a> <span class="status">1247343484916416517</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1247342756176859137',
    created: 1586224662000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jm" rel="noopener noreferrer" target="_blank">@jm</a> The role-based access redirect rules requires the Business plan, which is 1K per month.<br><br>In reply to: <a href="https://x.com/jm/status/1247340429093240832" rel="noopener noreferrer" target="_blank">@jm</a> <span class="status">1247340429093240832</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1247339919296458752',
    created: 1586223985000,
    type: 'post',
    text: 'Has anyone figured out how to add gated access to a site hosted on Netlify that doesn\'t cost 1K/month? That\'s pretty excessive for just protecting a static web site.',
    likes: 2,
    retweets: 1
  },
  {
    id: '1247285833985445888',
    created: 1586211090000,
    type: 'post',
    text: 'Question the means, not the madness.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1247285760937439235',
    created: 1586211073000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/zepag" rel="noopener noreferrer" target="_blank">@zepag</a> <a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> The way I like to explain it, the include directive gets replaced with the lines from the include file, then parsing continues. So the parser never "sees" the include directive.<br><br>In reply to: <a href="https://x.com/zepag/status/1247280089932410881" rel="noopener noreferrer" target="_blank">@zepag</a> <span class="status">1247280089932410881</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1247270716715577345',
    created: 1586207486000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Not in that way. You can assign a document attribute above the include and it will be available to the contents of the include. Remember, and include just expands lines into place. It doesn\'t provide structure or context itself.<br><br>In reply to: <a href="https://x.com/agoncal/status/1247156296987492352" rel="noopener noreferrer" target="_blank">@agoncal</a> <span class="status">1247156296987492352</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1247262300009984000',
    created: 1586205479000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> Actually, I really do. I\'m trying to use this opportunity to learn new things...perhaps things I wouldn\'t have normally pushed myself to try. It gives me purpose at a time when purpose is in short supply.<br><br>In reply to: <a href="https://x.com/settermjd/status/1247251153735356417" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1247251153735356417</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1247249304256778240',
    created: 1586202381000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a> <a class="mention" href="https://x.com/sproutsfm" rel="noopener noreferrer" target="_blank">@sproutsfm</a> This is my local grocery store and I\'m outraged. It\'s bad for the workers health and it\'s bad for the health of my community.<br><br>In reply to: <a href="https://x.com/BernieSanders/status/1246457634099789826" rel="noopener noreferrer" target="_blank">@BernieSanders</a> <span class="status">1246457634099789826</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1247247252357132289',
    created: 1586201892000,
    type: 'quote',
    text: '<a class="mention" href="https://x.com/GovEvers" rel="noopener noreferrer" target="_blank">@GovEvers</a> Thank you for doing the right thing to protect our country and our fellow citizens. Thank you for having the guts and moral clarity that <a class="mention" href="https://x.com/JoeBiden" rel="noopener noreferrer" target="_blank">@JoeBiden</a> doesn\'t have. I can breathe easier now.<br><br>Quoting: <a href="https://x.com/MollyBeck/status/1247219479894994945" rel="noopener noreferrer" target="_blank">@MollyBeck</a> <span class="status">1247219479894994945</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1246669156805529600',
    created: 1586064063000,
    type: 'quote',
    text: 'Colorado is raising its measures to flatten the curve. The governor is asking anyone who goes out in public—even for a trip to the store—to wear a mask. I guess it\'s time to dig out that sewing machine. 😷<br><br>Quoting: <a href="https://x.com/GovofCO/status/1246597168254910464" rel="noopener noreferrer" target="_blank">@GovofCO</a> <span class="status">1246597168254910464</span>',
    likes: 6,
    retweets: 2
  },
  {
    id: '1246587648430542848',
    created: 1586044630000,
    type: 'post',
    text: 'Have you ever thought that what we had before was not normal?',
    likes: 6,
    retweets: 1
  },
  {
    id: '1246587021763792897',
    created: 1586044481000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fanf42" rel="noopener noreferrer" target="_blank">@fanf42</a> The Democratic elites don\'t believe in healthcare for all. Only a majority of the voters do. The voters are running against the corporate party (yes, singular) at this point.<br><br>In reply to: <a href="https://x.com/fanf42/status/1246577006785019904" rel="noopener noreferrer" target="_blank">@fanf42</a> <span class="status">1246577006785019904</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1245885275269554177',
    created: 1585877171000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Seafoam6" rel="noopener noreferrer" target="_blank">@Seafoam6</a> A colleague of mine is dead. I\'m not screwing around.<br><br>In reply to: <a href="#1245884677212102659">1245884677212102659</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1245884677212102659',
    created: 1585877029000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Seafoam6" rel="noopener noreferrer" target="_blank">@Seafoam6</a> That\'s what I\'m doing. They\'re both culpable.<br><br>In reply to: @seafoam6 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1245878821191970816',
    created: 1585875632000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/crichardson" rel="noopener noreferrer" target="_blank">@crichardson</a> 💯 There\'s absolutely no excuse anymore to not have mail-in ballots.<br><br>In reply to: <a href="https://x.com/crichardson/status/1245878388922839040" rel="noopener noreferrer" target="_blank">@crichardson</a> <span class="status">1245878388922839040</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1245878487497359360',
    created: 1585875553000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/sierrahull" rel="noopener noreferrer" target="_blank">@sierrahull</a> You don\'t know how much your surprise live concert helped heal my 💔 today.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1245878167752994817',
    created: 1585875477000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Seafoam6" rel="noopener noreferrer" target="_blank">@Seafoam6</a> They are both full of moronic dipshits, if that\'s what you\'re asking.<br><br>In reply to: @seafoam6 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1245868737426681856',
    created: 1585873228000,
    type: 'quote',
    text: 'Before you point fingers at the GOP, here\'s a Democratic nominee denying the science and promoting a day-long event that would knowingly and acutely spread this virus (not to mention creating a completely untenable and chaotic situation). #COVIDー19<br><br>Quoting: <a href="https://x.com/nowandben/status/1245831379318910976" rel="noopener noreferrer" target="_blank">@nowandben</a> <span class="status">1245831379318910976</span>',
    likes: 2,
    retweets: 0,
    tags: ['covid19']
  },
  {
    id: '1245630962865590273',
    created: 1585816538000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/javaposse" rel="noopener noreferrer" target="_blank">@javaposse</a> Rest in peace and wisdom. The Java community won\'t be the same without you. We\'ll never forget you and all the wonderful moments and laughter you shared with us. 🕊️<br><br>In reply to: <a href="https://x.com/javaposse/status/1245583036588019715" rel="noopener noreferrer" target="_blank">@javaposse</a> <span class="status">1245583036588019715</span>',
    likes: 9,
    retweets: 0
  },
  {
    id: '1245608229800837122',
    created: 1585811118000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JonathanGiles" rel="noopener noreferrer" target="_blank">@JonathanGiles</a> <a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> <a class="mention" href="https://x.com/alaurei" rel="noopener noreferrer" target="_blank">@alaurei</a> I only had the opportunity to share one sit-down dinner with Carl. I don\'t remember what was said then, but I do remember it being special. I\'ve cherished that moment ever since, especially today.<br><br>In reply to: <a href="https://x.com/JonathanGiles/status/1245596503898329088" rel="noopener noreferrer" target="_blank">@JonathanGiles</a> <span class="status">1245596503898329088</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1245598815312175104',
    created: 1585808874000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_JamesWard" rel="noopener noreferrer" target="_blank">@_JamesWard</a> To finish that thought: ...to the entire Java community, who he touched so dearly with his wisdom, tender heart, and witty humor. I\'m truly sorry.<br><br>In reply to: <a href="#1245589822841843714">1245589822841843714</a>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1245589822841843714',
    created: 1585806730000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_JamesWard" rel="noopener noreferrer" target="_blank">@_JamesWard</a> I\'m gutted. I don\'t even know what to say, other than the world truly will never be as it was. My heart breaks for Carl, his family and close friends, and the entire Java community. 💔<br><br>In reply to: <a href="https://x.com/_JamesWard/status/1245587471892484096" rel="noopener noreferrer" target="_blank">@_JamesWard</a> <span class="status">1245587471892484096</span>',
    likes: 6,
    retweets: 0
  },
  {
    id: '1245459401097359360',
    created: 1585775635000,
    type: 'post',
    text: 'Well I\'ll be damned. <a class="mention" href="https://x.com/gitchat" rel="noopener noreferrer" target="_blank">@gitchat</a> finally has threaded discussions! You have to enable the feature in the settings per room. <a href="https://gitlab.com/gitlab-org/gitter/webapp/-/blob/develop/docs/rooms.md#enable-threaded-conversations-beta" rel="noopener noreferrer" target="_blank">gitlab.com/gitlab-org/gitter/webapp/-/blob/develop/docs/rooms.md#enable-threaded-conversations-beta</a>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1245395046549028875',
    created: 1585760292000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/vogella" rel="noopener noreferrer" target="_blank">@vogella</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> It sure can.<br><br>In reply to: <a href="https://x.com/vogella/status/1245343990855147520" rel="noopener noreferrer" target="_blank">@vogella</a> <span class="status">1245343990855147520</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1245257800764153856',
    created: 1585727570000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> <a class="mention" href="https://x.com/fully_us" rel="noopener noreferrer" target="_blank">@fully_us</a> <a class="mention" href="https://x.com/oktadev" rel="noopener noreferrer" target="_blank">@oktadev</a> I just got one of those last summer (mine by <a class="mention" href="https://x.com/UPLIFTDesk" rel="noopener noreferrer" target="_blank">@UPLIFTDesk</a>, but essentially the same design). Changed my work life. Truly amazing.<br><br>In reply to: <a href="https://x.com/mraible/status/962454871738810369" rel="noopener noreferrer" target="_blank">@mraible</a> <span class="status">962454871738810369</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1244918763666866176',
    created: 1585646737000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lfryc" rel="noopener noreferrer" target="_blank">@lfryc</a> Full-text search is the easy part since there are many services that will do it. Here are three examples:<br><br><a href="https://docs.couchbase.com" rel="noopener noreferrer" target="_blank">docs.couchbase.com</a> (Algolia)<br><a href="https://docs.mulesoft.com" rel="noopener noreferrer" target="_blank">docs.mulesoft.com</a> (Coveo)<br><a href="https://docs.fauna.com/fauna/current/index.html" rel="noopener noreferrer" target="_blank">docs.fauna.com/fauna/current/index.html</a> (Lunr)<br><br>In reply to: <a href="https://x.com/lfryc/status/1244917705351794695" rel="noopener noreferrer" target="_blank">@lfryc</a> <span class="status">1244917705351794695</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1244845932899786754',
    created: 1585629373000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CoriBush" rel="noopener noreferrer" target="_blank">@CoriBush</a> ❤️ The world is about to witness how strong the fight is in you. May you get well soon!<br><br>In reply to: <a href="https://x.com/CoriBush/status/1244711137473634304" rel="noopener noreferrer" target="_blank">@CoriBush</a> <span class="status">1244711137473634304</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1244823224002764801',
    created: 1585623958000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/dblevins" rel="noopener noreferrer" target="_blank">@dblevins</a> Happy Birthday! Looking forward to hanging out at the next round of #vdrinks.',
    likes: 1,
    retweets: 0,
    tags: ['vdrinks']
  },
  {
    id: '1244811015469985796',
    created: 1585621048000,
    type: 'post',
    text: 'After a very long hiatus, I\'ve started doing yoga again. It\'s really helping both mind and body, even after only a few short sessions. #QuaratineLife',
    likes: 3,
    retweets: 0,
    tags: ['quaratinelife']
  },
  {
    id: '1244809927886962692',
    created: 1585620788000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lfryc" rel="noopener noreferrer" target="_blank">@lfryc</a> Antora? (It\'s not all those things yet, but it\'s aimed at that space).<br><br>In reply to: <a href="https://x.com/lfryc/status/1244598325342621698" rel="noopener noreferrer" target="_blank">@lfryc</a> <span class="status">1244598325342621698</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1244809627528617984',
    created: 1585620717000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garbage" rel="noopener noreferrer" target="_blank">@garbage</a> I was just thinking that when I got up this morning. Since I work from home, I thought it would be the same. But it\'s really not the same. We just have to reach deep inside ourselves and find the strength to prevail.<br><br>In reply to: <a href="https://x.com/garbage/status/1244802233369128961" rel="noopener noreferrer" target="_blank">@garbage</a> <span class="status">1244802233369128961</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1244405961210519552',
    created: 1585524475000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> <a class="mention" href="https://x.com/crichardson" rel="noopener noreferrer" target="_blank">@crichardson</a> Perhaps this is what you were looking for? ☝️ It goes beyond just cases.<br><br>In reply to: <a href="https://x.com/starbuxman/status/1244401524282818560" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">1244401524282818560</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1244384444435185664',
    created: 1585519345000,
    type: 'post',
    text: 'My ice plant seedling has bloomed and I can only see this as a glimpse of hope. If not that, at least it\'s something to look at.',
    photos: ['<div class="item"><img class="photo" src="media/1244384444435185664.jpg"></div>'],
    likes: 2,
    retweets: 0
  },
  {
    id: '1244220082844094464',
    created: 1585480158000,
    type: 'quote',
    text: 'Both hilarious and insightful.<br><br>Quoting: <a href="https://x.com/ajplus/status/1243964813102985216" rel="noopener noreferrer" target="_blank">@ajplus</a> <span class="status">1243964813102985216</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1244219575899545600',
    created: 1585480038000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> You might find this report helpful in finding answers to how we got to where we are. <a href="https://youtu.be/_32dRbA38Z8" rel="noopener noreferrer" target="_blank">youtu.be/_32dRbA38Z8</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1243706101885001728',
    created: 1585357616000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SanneGrinovero" rel="noopener noreferrer" target="_blank">@SanneGrinovero</a> <a class="mention" href="https://x.com/fedora" rel="noopener noreferrer" target="_blank">@fedora</a> I\'ve been a happy user of Fedora for many years now, and this news makes me even happier. 🐧<br><br>In reply to: <a href="https://x.com/SanneGrinovero/status/1243662628590891008" rel="noopener noreferrer" target="_blank">@SanneGrinovero</a> <span class="status">1243662628590891008</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1243611627179696133',
    created: 1585335091000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/petitlaurent" rel="noopener noreferrer" target="_blank">@petitlaurent</a> I agree they should be seen as part of the production code. Good way of putting it. As to whether it makes you code faster, I can only speak from my experience and what I\'ve observed. It always does, substantially.<br><br>In reply to: <a href="https://x.com/petitlaurent/status/1243603175350898691" rel="noopener noreferrer" target="_blank">@petitlaurent</a> <span class="status">1243603175350898691</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1243594838693400576',
    created: 1585331089000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/petitlaurent" rel="noopener noreferrer" target="_blank">@petitlaurent</a> And if you\'re hands are on the test suite daily, you will naturally make improvements to it. Organic. That\'s why I say it doesn\'t matter what it\'s composed of. Because you will develop it in tandem with the software as needs arise.<br><br>In reply to: <a href="#1243594635592609792">1243594635592609792</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1243594635592609792',
    created: 1585331040000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/petitlaurent" rel="noopener noreferrer" target="_blank">@petitlaurent</a> Here\'s were I get a bit controversial. I really don\'t care what the test suite is composed of. You can call the tests whatever you want, unit, integration, whatever. As long as you\'re comfortable using it, that\'s all that matters. (Of course, there\'s always room for improvement).<br><br>In reply to: <a href="https://x.com/petitlaurent/status/1243587309053915148" rel="noopener noreferrer" target="_blank">@petitlaurent</a> <span class="status">1243587309053915148</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1243579424613621761',
    created: 1585327414000,
    type: 'reply',
    text: '@bexelbie Colorado also demanded its marijuana shops be reopened and the exemption was granted.<br><br>In reply to: @bexelbie <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1243578262833352704',
    created: 1585327137000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/petitlaurent" rel="noopener noreferrer" target="_blank">@petitlaurent</a> By "good" I mean to things:<br><br>1. Easy to run<br>2. Easy to add a new test<br><br>The key is that the barrier is low. If it feels hard, you won\'t do it, then all the benefit evaporates. Simpler is better.<br><br>In reply to: <a href="https://x.com/petitlaurent/status/1243576586990911494" rel="noopener noreferrer" target="_blank">@petitlaurent</a> <span class="status">1243576586990911494</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1243300492727345152',
    created: 1585260911000,
    type: 'post',
    text: 'Once you get a good test suite set up, you can really start coding rapidly. Take it from experience.',
    likes: 27,
    retweets: 7
  },
  {
    id: '1243120341003591681',
    created: 1585217960000,
    type: 'quote',
    text: 'The Amendment King doing the amendment thing.<br><br>Quoting: <a href="https://x.com/JordanUhl/status/1243045317051809794" rel="noopener noreferrer" target="_blank">@JordanUhl</a> <span class="status">1243045317051809794</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1243119435369443328',
    created: 1585217744000,
    type: 'post',
    text: 'We talk a lot about how critical medical personnel are in a pandemic, and they most certainly are. No question. We should also give a shout out to musicians whose music helps keep us sane and our spirits high during trying times.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1243117537786646530',
    created: 1585217291000,
    type: 'post',
    text: 'This <a class="mention" href="https://x.com/MartinGarrix" rel="noopener noreferrer" target="_blank">@MartinGarrix</a> set from Virtual Ultra 2020 takes me places. <a href="https://youtu.be/HDMPQWEuruE" rel="noopener noreferrer" target="_blank">youtu.be/HDMPQWEuruE</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1243075068642512899',
    created: 1585207166000,
    type: 'post',
    text: 'I should add this is not just about defending the environment, though that\'s the crux of the case. It\'s also about defending the Tribe\'s ancestral and sacred homeland, which includes respecting treaties the US made with them but never honored.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1243073790294147072',
    created: 1585206861000,
    type: 'quote',
    text: 'And now for some good news for the environment and the indigenous peoples leading the fight to protect it. #NoDAPL scored a major victory. This just goes to show that when we unite in solidarity, we can win. ✊<br><br>Quoting: <a href="https://x.com/Earthjustice/status/1242862832934322176" rel="noopener noreferrer" target="_blank">@Earthjustice</a> <span class="status">1242862832934322176</span>',
    likes: 1,
    retweets: 0,
    tags: ['nodapl']
  },
  {
    id: '1243044521346093056',
    created: 1585199883000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/cmaki" rel="noopener noreferrer" target="_blank">@cmaki</a> We just crossed 1,000 cases here, so it was now or serious trouble...if even soon enough. Now we wait.<br><br>In reply to: <a href="https://x.com/cmaki/status/1243042401519685632" rel="noopener noreferrer" target="_blank">@cmaki</a> <span class="status">1243042401519685632</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1243040423657922560',
    created: 1585198906000,
    type: 'post',
    text: 'Walking and hiking is still permitted, which I think is important for getting fresh air and sunlight to stay healthy. In this state, you can easily stay dozens of feet away from others, so a very low risk activity. Granted, walking becomes a game of dodge.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1243038825472864257',
    created: 1585198525000,
    type: 'post',
    text: 'It took Colorado too long to come around, but I\'m glad they have. "This tells the public that this is truly serious, and we truly mean it." (from a public health official) That\'s the direct language we\'ve been looking for.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1243036449248702466',
    created: 1585197958000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rotnroll666" rel="noopener noreferrer" target="_blank">@rotnroll666</a> I\'m going the caveman route so far.<br><br>In reply to: <a href="https://x.com/rotnroll666/status/1242717054005886976" rel="noopener noreferrer" target="_blank">@rotnroll666</a> <span class="status">1242717054005886976</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1243006340319993857',
    created: 1585190780000,
    type: 'post',
    text: 'Just received the emergency alert on my phone for the stay at home order for Colorado, effective Thursday morning. There are still too many exceptions as far as I\'m concerned, but at least a step towards taking the crisis seriously. If we stay at home, we can beat it quickly.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1242895301926481920',
    created: 1585164306000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/crichardson" rel="noopener noreferrer" target="_blank">@crichardson</a> Truth.<br><br>In reply to: <a href="https://x.com/crichardson/status/1242894882919727105" rel="noopener noreferrer" target="_blank">@crichardson</a> <span class="status">1242894882919727105</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1242890613374070784',
    created: 1585163188000,
    type: 'quote',
    text: '$1,200 is not enough.<br><br>It\'s 65% of rent for my 1-bedroom apartment in Denver...which, trust me, is not fancy.<br><br>Quoting: <a href="https://x.com/ninaturner/status/1242886572279771142" rel="noopener noreferrer" target="_blank">@ninaturner</a> <span class="status">1242886572279771142</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1242890206467854338',
    created: 1585163091000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sunrisemvmt" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a> $1,858 for a 1-bedroom apartment here in south Denver, and that\'s just the base amount (no utilities).<br><br>In reply to: <a href="https://x.com/sunrisemvmt/status/1242887432367943680" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a> <span class="status">1242887432367943680</span>',
    likes: 6,
    retweets: 0
  },
  {
    id: '1242889379317600257',
    created: 1585162894000,
    type: 'post',
    text: 'As if the stress of the pandemic wasn\'t enough, it\'s maddening to witness the government squander taxpayer money by siphoning it to giant corporations instead of giving relief to workers and funding universal programs (heath care in particular). 😡 #stimulusbill #COVID19',
    likes: 5,
    retweets: 1,
    tags: ['stimulusbill', 'covid19']
  },
  {
    id: '1242881898717696000',
    created: 1585161110000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CityofLoneTree" rel="noopener noreferrer" target="_blank">@CityofLoneTree</a> This is better than the statement issued Monday. We HAVE to take this extremely seriously. Every city / country that didn\'t do so early on has paid the price. While the loss of revenue is unfortunate, we can make it up. We can\'t bring lives back. The message must be, #StayAtHome!<br><br>In reply to: <a href="https://x.com/CityofLoneTree/status/1242874210495725571" rel="noopener noreferrer" target="_blank">@CityofLoneTree</a> <span class="status">1242874210495725571</span>',
    likes: 0,
    retweets: 0,
    tags: ['stayathome']
  },
  {
    id: '1242761804788281345',
    created: 1585132478000,
    type: 'post',
    text: 'After attending several, my advice for online conferences is to stretch it out. You\'re automatically immersed when there in person. When it\'s online, you have to juggle other activities. Time moves quicker. Therefore, it takes longer to soak it up. Give that a chance to happen.',
    likes: 3,
    retweets: 1
  },
  {
    id: '1242555388370677767',
    created: 1585083264000,
    type: 'post',
    text: 'covidiot: oh, you\'re one of those distancing people. 🙄 relax. i\'m healthy.<br>covid-19: yaaas, more hosts-esses! come closer.🦹<br>covidiot: I\'m invincible!<br>covid-19: wow, sweeeet place!<br>covidiot: oh shit! just kidding. here, I\'ll keep my distance. 😷<br>covid-19: too late suckerz!!!! 🦠',
    likes: 1,
    retweets: 0
  },
  {
    id: '1242401058111938560',
    created: 1585046469000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lisacrispin" rel="noopener noreferrer" target="_blank">@lisacrispin</a> The only way out of this is if we get periodic testing for every resident until it\'s defeated. We have no idea where the virus clusters are (only where it\'s been), so we\'re effectively chasing a ghost. We need to push for testing (a concept which for you needs no introduction).<br><br>In reply to: <a href="https://x.com/lisacrispin/status/1242395478983139330" rel="noopener noreferrer" target="_blank">@lisacrispin</a> <span class="status">1242395478983139330</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1242357362855272449',
    created: 1585036051000,
    type: 'post',
    text: '"Remember when life is so unfair, hold on, hold on, we\'re almost there."',
    likes: 0,
    retweets: 0
  },
  {
    id: '1242356715820990465',
    created: 1585035897000,
    type: 'reply',
    text: 'Now I\'m listening to the set by <a class="mention" href="https://x.com/MartinGarrix" rel="noopener noreferrer" target="_blank">@MartinGarrix</a>. <a href="https://youtu.be/-z66_M8rwcM" rel="noopener noreferrer" target="_blank">youtu.be/-z66_M8rwcM</a><br><br>In reply to: <a href="#1242225152676335616">1242225152676335616</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1242340287298408448',
    created: 1585031980000,
    type: 'post',
    text: 'The virus is telling you, "FIND ME SOME NEW HOSTS!" Don\'t listen to it. Stay home.',
    photos: ['<div class="item"><img class="photo" src="media/1242340287298408448.gif"></div>'],
    likes: 0,
    retweets: 2
  },
  {
    id: '1242335172223107073',
    created: 1585030761000,
    type: 'post',
    text: 'Maybe we need to take a different approach explaining this situation to some people. If you go out, you\'re doing what the virus wants. Don\'t let it control you. Show it who\'s boss!',
    likes: 4,
    retweets: 1
  },
  {
    id: '1242295529004040195',
    created: 1585021309000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lorrdenh" rel="noopener noreferrer" target="_blank">@lorrdenh</a> I swear they\'re playing the "let\'s just see what happens, then maybe we\'ll do more" game. There\'s also a lot of shifting of responsibility to citizens to "do the right thing" so the politicians can blame us when it goes badly, as it will.<br><br>In reply to: <a href="https://x.com/lorrdenh/status/1242291545262481420" rel="noopener noreferrer" target="_blank">@lorrdenh</a> <span class="status">1242291545262481420</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1242294106359021568',
    created: 1585020970000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gegastaldi" rel="noopener noreferrer" target="_blank">@gegastaldi</a> True. I think the message should be to make your own food as much as possible and only resort to delivery in a pinch. Each outing increases the risk of spread substantially. And yet our mayor is like "go get that take-out as much as possible to support tax revenue!"<br><br>In reply to: <a href="https://x.com/gegastaldi/status/1242273755260485635" rel="noopener noreferrer" target="_blank">@gegastaldi</a> <span class="status">1242273755260485635</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1242293066125492224',
    created: 1585020722000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/TheSteve0" rel="noopener noreferrer" target="_blank">@TheSteve0</a> I know. It\'s outrageous. As if what\'s happening in Italy just doesn\'t apply to us. The message is "wave and say hi, just don\'t touch." As if this is just a common cold.<br><br>In reply to: <a href="https://x.com/TheSteve0/status/1242274786731950082" rel="noopener noreferrer" target="_blank">@TheSteve0</a> <span class="status">1242274786731950082</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1242269375282810882',
    created: 1585015073000,
    type: 'post',
    text: 'Or how about "Will the mall remain open?" Response: "Yes, for now it\'s remaining open."<br><br>HELL NO. A mall? Are you serious? How can you stay 6 feet away from people in a mall?<br><br>This is not a serious response.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1242268771537936384',
    created: 1585014930000,
    type: 'post',
    text: 'But the real confusing part is "practice good social distancing" while also "please support local restaurants by getting take-out" Like, what? How can you possibly do both? You\'re telling people to stay home and go out at the same time.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1242268114412134402',
    created: 1585014773000,
    type: 'post',
    text: 'As an example, "I see teenagers playing pickup basketball. Why\'s that allowed?" The response: "We ask you to practice good social distancing, which means not playing basketball." Sorry, they\'re going to ignore you unless you say, "It\'s forbidden. You\'ll receive a citation."',
    likes: 0,
    retweets: 0
  },
  {
    id: '1242266664596426752',
    created: 1585014427000,
    type: 'post',
    text: 'I\'m listening to the town hall for my city and I\'m telling you, the language is not direct enough. It\'s all "personal choice" / "personal practices". Pleading. Some of it is that the city hasn\'t been given the resources by the county or state. But the directive is just too weak.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1242255932874035201',
    created: 1585011869000,
    type: 'post',
    text: 'After eating vegan for the better part of a year, you don\'t even think of it as eating vegan anymore. It\'s just eating.',
    likes: 9,
    retweets: 1
  },
  {
    id: '1242245539187458048',
    created: 1585009391000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sierrahull" rel="noopener noreferrer" target="_blank">@sierrahull</a> I\'ve been looking forward to the release of this song ever since I heard you perform it in Denver a year ago (back when live events were still a thing). It instantly grabbed my fancy. I\'m glad I get to revisit that journey to the middle of the woods over and over. Thanks!<br><br>In reply to: <a href="https://x.com/sierrahull/status/1242169948501413888" rel="noopener noreferrer" target="_blank">@sierrahull</a> <span class="status">1242169948501413888</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1242225152676335616',
    created: 1585004530000,
    type: 'post',
    text: 'One thing that\'s not so bad about #QuarantineLife is the sheer amount of music streams coming online to keep us all sane. I\'m currently listening to (and grateful for) the set by <a class="mention" href="https://x.com/arminvanbuuren" rel="noopener noreferrer" target="_blank">@arminvanbuuren</a> as part of Virtual Ultra Music Festival 2020. <a href="https://youtu.be/XQGjXoSAZEE" rel="noopener noreferrer" target="_blank">youtu.be/XQGjXoSAZEE</a>',
    likes: 3,
    retweets: 0,
    tags: ['quarantinelife']
  },
  {
    id: '1242216878426161155',
    created: 1585002557000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ObsoleteDogma" rel="noopener noreferrer" target="_blank">@ObsoleteDogma</a> The only way out I can see is weekly tests administered from mobile units until we\'ve got it beat. Otherwise, we don\'t have the necessary data to control it. But I don\'t hear anyone talking about this. Why not?<br><br>In reply to: <a href="https://x.com/ObsoleteDogma/status/1242154791733727232" rel="noopener noreferrer" target="_blank">@ObsoleteDogma</a> <span class="status">1242154791733727232</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1242147128341712897',
    created: 1584985928000,
    type: 'post',
    text: 'As we learn that many people who have #COVIDー19 are asymptomatic (aka only carriers), know that in the US, you have to demonstrate symptoms or have been in contact with someone who does to get tested. So this is going well :(<br><br>Best just to act like you have it.',
    likes: 0,
    retweets: 0,
    tags: ['covid19']
  },
  {
    id: '1242031674587639808',
    created: 1584958401000,
    type: 'post',
    text: 'The next time you waive off paid sick leave as unnecessary or a handout, I hope you remember this crisis. No one should be forced to go to work for fear of lost wages because that person is sick. It\'s not just wrong, it\'s dangerous.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1241879880750391297',
    created: 1584922211000,
    type: 'reply',
    text: 'I cannot overstate how brilliant <a class="mention" href="https://x.com/AOC" rel="noopener noreferrer" target="_blank">@AOC</a>, <a class="mention" href="https://x.com/IlhanMN" rel="noopener noreferrer" target="_blank">@IlhanMN</a>, and <a class="mention" href="https://x.com/RashidaTlaib" rel="noopener noreferrer" target="_blank">@RashidaTlaib</a> are, truly savants, and yet at the same time so real. We\'re lucky to have them representing their constituents and all of us in Congress.<br><br>In reply to: <a href="#1241868395668111361">1241868395668111361</a>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1241868395668111361',
    created: 1584919472000,
    type: 'quote',
    text: 'Bernie Sanders is literally serving the role of a President during this crisis. He\'s showing up and walking the walk.<br><br>Quoting: <a href="https://x.com/BernieSanders/status/1241861299354525697" rel="noopener noreferrer" target="_blank">@BernieSanders</a> <span class="status">1241861299354525697</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1241480240909520896',
    created: 1584826929000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/crichardson" rel="noopener noreferrer" target="_blank">@crichardson</a> Check out <a href="https://www.tableau.com/covid-19-coronavirus-data-resources" rel="noopener noreferrer" target="_blank">www.tableau.com/covid-19-coronavirus-data-resources</a><br><br>In reply to: <a href="https://x.com/crichardson/status/1241476591646961664" rel="noopener noreferrer" target="_blank">@crichardson</a> <span class="status">1241476591646961664</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1241474975350927361',
    created: 1584825674000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/myfear" rel="noopener noreferrer" target="_blank">@myfear</a> <a class="mention" href="https://x.com/carbonfray" rel="noopener noreferrer" target="_blank">@carbonfray</a> We will! Healthy and sane so far. It\'s weird, but also comforting that we have such a strong support network. We\'ll all need each other in the weeks and months to come.<br><br>In reply to: <a href="https://x.com/myfear/status/1241283668368392192" rel="noopener noreferrer" target="_blank">@myfear</a> <span class="status">1241283668368392192</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1241474074892591104',
    created: 1584825459000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DK_1977" rel="noopener noreferrer" target="_blank">@DK_1977</a> Thanks Daniel! Knowing I\'m part of such a great community not only makes my birthday happy, it keeps me busy and motivated during these trying times. May you stay healthy! 🍻<br><br>In reply to: @dk_1977 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1241473171275972613',
    created: 1584825244000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kenfinnigan" rel="noopener noreferrer" target="_blank">@kenfinnigan</a> We\'re fortunate to have gotten our business in order before this all hit. Fingers crossed our preparation will get us though this and that we can help out others. Healthy so far, but counting our blessings every single day.<br><br>In reply to: <a href="https://x.com/kenfinnigan/status/1241319377204641792" rel="noopener noreferrer" target="_blank">@kenfinnigan</a> <span class="status">1241319377204641792</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1241472522006102016',
    created: 1584825089000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/benignbala" rel="noopener noreferrer" target="_blank">@benignbala</a> Thanks Bala! I\'ve celebrated many birthdays with this amazing community and look forward to celebrating many more. 🎉<br><br>In reply to: <a href="https://x.com/benignbala/status/1241345797725147136" rel="noopener noreferrer" target="_blank">@benignbala</a> <span class="status">1241345797725147136</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1241472017318047744',
    created: 1584824969000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gregturn" rel="noopener noreferrer" target="_blank">@gregturn</a> I believe you can if you provide a custom CSS file. I\'m not working on the EPUB3 converter anymore (thanks to Marat), so best to inquire using an issue or the gitter channel.<br><br>In reply to: <a href="https://x.com/gregturn/status/1241390366785503238" rel="noopener noreferrer" target="_blank">@gregturn</a> <span class="status">1241390366785503238</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1241447019593363456',
    created: 1584819009000,
    type: 'post',
    text: 'During #QuarantineLife, there\'s a strong temptation to turn to corporate media for answers. I urge you to resist. Instead, use the opportunity to discover independent news &amp; journalism. If you\'re looking for something in the same format, try Rising (<a class="mention" href="https://x.com/hilltvlive" rel="noopener noreferrer" target="_blank">@hilltvlive</a>) or <a class="mention" href="https://x.com/democracynow" rel="noopener noreferrer" target="_blank">@democracynow</a>.',
    likes: 1,
    retweets: 0,
    tags: ['quarantinelife']
  },
  {
    id: '1241327067397894145',
    created: 1584790410000,
    type: 'reply',
    text: '@jbryant787 Thanks Jay! It\'s amazing how quickly things have changed after our talk only a few weeks ago. I sit back and raise my glass today, then keep fighting the good fight tomorrow.<br><br>In reply to: @jbryant787 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1241325451819110401',
    created: 1584790025000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/thjanssen123" rel="noopener noreferrer" target="_blank">@thjanssen123</a> Thanks Thorben! I\'m enjoying my #coronabirthday with a few beers, but definitely not a Corona! 🤣<br><br>In reply to: <a href="https://x.com/thjanssen123/status/1241221557151830016" rel="noopener noreferrer" target="_blank">@thjanssen123</a> <span class="status">1241221557151830016</span>',
    likes: 1,
    retweets: 0,
    tags: ['coronabirthday']
  },
  {
    id: '1241324578380505093',
    created: 1584789816000,
    type: 'reply',
    text: '@AndyGeeDe So true.<br><br>In reply to: @AndyGeeDe <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1241324388735012864',
    created: 1584789771000,
    type: 'post',
    text: 'This pandemic is revealing which politicians are leaders, and which ones have just been cosplaying.',
    likes: 6,
    retweets: 2
  },
  {
    id: '1241302138992488448',
    created: 1584784466000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> Thanks Alex! I can always count on warm wishes from you to brighten my special day. 🍻<br><br>In reply to: <a href="https://x.com/alexsotob/status/1241042386102484994" rel="noopener noreferrer" target="_blank">@alexsotob</a> <span class="status">1241042386102484994</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1241291646525788160',
    created: 1584781965000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobbytank42" rel="noopener noreferrer" target="_blank">@bobbytank42</a> Thanks Robert! Your friendship means so much to me. So even though this is a #coronabirthday, I can enjoy it all the same knowing we\'re in touch virtually. 🍻<br><br>In reply to: <a href="https://x.com/bobbytank42/status/1241042024750616577" rel="noopener noreferrer" target="_blank">@bobbytank42</a> <span class="status">1241042024750616577</span>',
    likes: 3,
    retweets: 0,
    tags: ['coronabirthday']
  },
  {
    id: '1241288225836032000',
    created: 1584781149000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/arungupta" rel="noopener noreferrer" target="_blank">@arungupta</a> Thanks Arun! Although I\'m stuck at home on my #coronabirthday, I\'m hacking on open source and enjoying what I can do. Your advocacy work reminds me that having the right attitude and a smile makes the day bright, no matter what. ☀️<br><br>In reply to: <a href="https://x.com/arungupta/status/1240993421176627201" rel="noopener noreferrer" target="_blank">@arungupta</a> <span class="status">1240993421176627201</span>',
    likes: 1,
    retweets: 0,
    tags: ['coronabirthday']
  },
  {
    id: '1241286160460087296',
    created: 1584780657000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/badrelhouari" rel="noopener noreferrer" target="_blank">@badrelhouari</a> <a class="mention" href="https://x.com/matkar" rel="noopener noreferrer" target="_blank">@matkar</a> <a class="mention" href="https://x.com/ameliaeiras" rel="noopener noreferrer" target="_blank">@ameliaeiras</a> Yes! The vbar is open and all the best folks are on the way. I love it! Reconnecting is the best thing we can do to get though these crazy times.<br><br>In reply to: <a href="https://x.com/badrelhouari/status/1240992844053168133" rel="noopener noreferrer" target="_blank">@badrelhouari</a> <span class="status">1240992844053168133</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1241285793991127048',
    created: 1584780569000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BMcG1230" rel="noopener noreferrer" target="_blank">@BMcG1230</a> Cheers cuz! With the world so crazy, it means a lot to know you\'re out there thinking of me. And I\'m thinking of you too! Hopefully we\'ll be seeing each other soon on the other side of this.<br><br>In reply to: <a href="https://x.com/BMcG1230/status/1240987827946639362" rel="noopener noreferrer" target="_blank">@BMcG1230</a> <span class="status">1240987827946639362</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1241285357833867264',
    created: 1584780465000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matkar" rel="noopener noreferrer" target="_blank">@matkar</a> <a class="mention" href="https://x.com/ameliaeiras" rel="noopener noreferrer" target="_blank">@ameliaeiras</a> I\'m sure there will be plenty more in our future. And I\'d love to hang with you. This was just the trial run. And the survey says, more vdrinks!<br><br>In reply to: <a href="https://x.com/matkar/status/1240981588785102851" rel="noopener noreferrer" target="_blank">@matkar</a> <span class="status">1240981588785102851</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1241284961996394497',
    created: 1584780371000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kenfinnigan" rel="noopener noreferrer" target="_blank">@kenfinnigan</a> Thanks Ken! I hope you\'re doing well and keeping healthy. Your wishes have extra meaning on a #coronabirthday. 🍻<br><br>In reply to: <a href="https://x.com/kenfinnigan/status/1240957941483798528" rel="noopener noreferrer" target="_blank">@kenfinnigan</a> <span class="status">1240957941483798528</span>',
    likes: 0,
    retweets: 0,
    tags: ['coronabirthday']
  },
  {
    id: '1241284228572008448',
    created: 1584780196000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lhawthorn" rel="noopener noreferrer" target="_blank">@lhawthorn</a> Thanks my friend and mentor! Although it\'s been strange having a #coronabirthday, I\'m making the best of it 2 meters away. I\'m drawing on the memory of hugs 🤗 to stay strong. I\'ll cherish them all the more when we get to again. 🍻 to your health!<br><br>In reply to: <a href="https://x.com/lhawthorn/status/1240943575774478336" rel="noopener noreferrer" target="_blank">@lhawthorn</a> <span class="status">1240943575774478336</span>',
    likes: 1,
    retweets: 0,
    tags: ['coronabirthday']
  },
  {
    id: '1241281939383791617',
    created: 1584779650000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/wimdeblauwe" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> Thanks! And my pleasure! I couldn\'t do it without such a great community, which I\'m reminded of today.<br><br>In reply to: <a href="https://x.com/wimdeblauwe/status/1240917922966634496" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> <span class="status">1240917922966634496</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1241281727030386689',
    created: 1584779600000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/vlad_mihalcea" rel="noopener noreferrer" target="_blank">@vlad_mihalcea</a> Thanks Vlad. As you might have guessed, I\'ve been working on releasing some open source software. That always makes for a good day, even a #coronabirthday. 🍻<br><br>In reply to: <a href="https://x.com/vlad_mihalcea/status/1240916397796048896" rel="noopener noreferrer" target="_blank">@vlad_mihalcea</a> <span class="status">1240916397796048896</span>',
    likes: 1,
    retweets: 0,
    tags: ['coronabirthday']
  },
  {
    id: '1241281012497182722',
    created: 1584779429000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/myfear" rel="noopener noreferrer" target="_blank">@myfear</a> Thanks Markus! Yours was the first and it keep me in good spirits throughout this #coronabirthday, from ☕ to 🍺.<br><br>In reply to: <a href="https://x.com/myfear/status/1240913049709621249" rel="noopener noreferrer" target="_blank">@myfear</a> <span class="status">1240913049709621249</span>',
    likes: 2,
    retweets: 0,
    tags: ['coronabirthday']
  },
  {
    id: '1241241497346781184',
    created: 1584770008000,
    type: 'quote',
    text: 'The virus has appeared in my little city. I\'m sheltering at home as much as possible to avoid catching or spreading it. Thankfully, I made and froze a bunch of soup last week, so I shouldn\'t need to go out for a while.<br><br>Quoting: <a href="https://x.com/CityofLoneTree/status/1241171562394419201" rel="noopener noreferrer" target="_blank">@CityofLoneTree</a> <span class="status">1241171562394419201</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1241106828093583365',
    created: 1584737901000,
    type: 'reply',
    text: '@AndyGeeDe Thanks Andy! And trust me, the beer will be very real. The toast will just be virtual ;) I\'ll be raising my glass to your health.<br><br>In reply to: @AndyGeeDe <span class="status">&lt;deleted&gt;</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1240768754591633420',
    created: 1584657298000,
    type: 'reply',
    text: '...and <a class="mention" href="https://x.com/nicolas_frankel" rel="noopener noreferrer" target="_blank">@nicolas_frankel</a>.<br><br>In reply to: <a href="#1240756243280486400">1240756243280486400</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1240763292215205888',
    created: 1584655995000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ivan_stefanov" rel="noopener noreferrer" target="_blank">@ivan_stefanov</a> 💯*💯<br><br>In reply to: <a href="https://x.com/ivan_stefanov/status/1240760264418607110" rel="noopener noreferrer" target="_blank">@ivan_stefanov</a> <span class="status">1240760264418607110</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1240759817506975744',
    created: 1584655167000,
    type: 'post',
    text: 'It\'s also a good opportunity to feel your way around your video conferencing tool (which you\'re going to need a lot) without the pressure of being in a formal meeting. For example, I finally figured out where the button for gallery view is located. 🧐',
    likes: 4,
    retweets: 0
  },
  {
    id: '1240756243280486400',
    created: 1584654315000,
    type: 'reply',
    text: 'Got to hang with <a class="mention" href="https://x.com/ivan_stefanov" rel="noopener noreferrer" target="_blank">@ivan_stefanov</a>, <a class="mention" href="https://x.com/navssurtani" rel="noopener noreferrer" target="_blank">@navssurtani</a>, <a class="mention" href="https://x.com/heathervc" rel="noopener noreferrer" target="_blank">@heathervc</a>, <a class="mention" href="https://x.com/omniprof" rel="noopener noreferrer" target="_blank">@omniprof</a>, <a class="mention" href="https://x.com/stuartmarks" rel="noopener noreferrer" target="_blank">@stuartmarks</a>, <a class="mention" href="https://x.com/dblevins" rel="noopener noreferrer" target="_blank">@dblevins</a>, <a class="mention" href="https://x.com/ameliaeiras" rel="noopener noreferrer" target="_blank">@ameliaeiras</a>, <a class="mention" href="https://x.com/bbohmann" rel="noopener noreferrer" target="_blank">@bbohmann</a>, and more. Let\'s do it again soon!<br><br>In reply to: <a href="#1240754370355933184">1240754370355933184</a>',
    likes: 10,
    retweets: 0
  },
  {
    id: '1240754370355933184',
    created: 1584653868000,
    type: 'post',
    text: 'Just joined my first vdrinks / vbar today organized by <a class="mention" href="https://x.com/ameliaeiras" rel="noopener noreferrer" target="_blank">@ameliaeiras</a> and got to see lots of familiar faces. Didn\'t know I needed, but it was great. I highly recommend doing it with your friends. Say hi. Laugh. Cry. It\'s important to enjoy life. We\'re apart, but not isolated. 🍻',
    likes: 14,
    retweets: 1
  },
  {
    id: '1240681459645476865',
    created: 1584636485000,
    type: 'post',
    text: 'We\'re once again finding out which companies have (and have kept) our contact information.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1240427984848216065',
    created: 1584576052000,
    type: 'post',
    text: '#DearBernie,<br><br>I\'m still with you.',
    likes: 5,
    retweets: 1,
    tags: ['dearbernie']
  },
  {
    id: '1240352055249604608',
    created: 1584557949000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/chuckfrain" rel="noopener noreferrer" target="_blank">@chuckfrain</a> What I love most about the Gesture is that if you shift position, it flexes to support that position instead of fighting you.<br><br>In reply to: <a href="#1240351759081472000">1240351759081472000</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1240351759081472000',
    created: 1584557878000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/chuckfrain" rel="noopener noreferrer" target="_blank">@chuckfrain</a> I went through this research recently. I found three things:<br><br>* headrest (for when leaning back)<br>* responsive (adjusts to your position dynamically)<br>* adjustable lumbar<br><br>I choose the Gesture chair by Steelcase and I love it. The Think chair was my second choice.<br><br>In reply to: <a href="https://x.com/chuckfrain/status/1240325126786342914" rel="noopener noreferrer" target="_blank">@chuckfrain</a> <span class="status">1240325126786342914</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1240209159762690050',
    created: 1584523880000,
    type: 'post',
    text: 'This is a really, really good clip by <a class="mention" href="https://x.com/krystalball" rel="noopener noreferrer" target="_blank">@krystalball</a> about how to approach the current crisis and how to respond to it as a society; not the virus part, but the economic part. <a href="https://youtu.be/WGWlQIssXr8" rel="noopener noreferrer" target="_blank">youtu.be/WGWlQIssXr8</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1240190534305579008',
    created: 1584519439000,
    type: 'quote',
    text: 'Trust has always motivated me to work harder.<br><br>Quoting: <a href="https://x.com/jasonfried/status/1239669228615208960" rel="noopener noreferrer" target="_blank">@jasonfried</a> <span class="status">1239669228615208960</span>',
    likes: 5,
    retweets: 0
  },
  {
    id: '1240158646199300096',
    created: 1584511836000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> <a class="mention" href="https://x.com/fguime" rel="noopener noreferrer" target="_blank">@fguime</a> Even better, carrot dogs. I\'m testing my will power trying not to have them for every meal.<br><br>In reply to: <a href="https://x.com/headius/status/1240157922505572353" rel="noopener noreferrer" target="_blank">@headius</a> <span class="status">1240157922505572353</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1240051574006677504',
    created: 1584486308000,
    type: 'post',
    text: 'Given that Antora now has almost 1,200 tests, I cannot even imagine a site generator that has no tests.',
    likes: 13,
    retweets: 6
  },
  {
    id: '1239989059985895425',
    created: 1584471404000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mairin" rel="noopener noreferrer" target="_blank">@mairin</a> 5 is the one I\'ve been yearning for.<br><br>In reply to: <a href="https://x.com/mairin/status/1239926399416315909" rel="noopener noreferrer" target="_blank">@mairin</a> <span class="status">1239926399416315909</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1239945515074080769',
    created: 1584461022000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/schneidro" rel="noopener noreferrer" target="_blank">@schneidro</a> I\'m not going to say I\'m not conflicted about it. Believe me, I am. But given this is a party election, we *should* be able to sort this out without compromising the outcome.<br><br>In reply to: <a href="https://x.com/schneidro/status/1239933645114105856" rel="noopener noreferrer" target="_blank">@schneidro</a> <span class="status">1239933645114105856</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1239869625933811712',
    created: 1584442929000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/TheDemocrats" rel="noopener noreferrer" target="_blank">@TheDemocrats</a> You\'re embarrassing yourself and endangering lives. Do the right thing. Show us you\'re better than the current administration.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1239869172626018306',
    created: 1584442821000,
    type: 'post',
    text: 'CDC: Please stay home! There\'s a pandemic that\'s still evolving. We need to flatten the curve so we don\'t overwhelm the medical system.<br><br>DNC: There\'s an important election today. Go out and vote! Stand in line for hours. Don\'t sit this one out.<br><br>🤦 #COVIDー19 #DemocraticPrimary',
    likes: 1,
    retweets: 0,
    tags: ['covid19', 'democraticprimary']
  },
  {
    id: '1239866460417085440',
    created: 1584442174000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BMcG1230" rel="noopener noreferrer" target="_blank">@BMcG1230</a> Damn right.<br><br>In reply to: <a href="https://x.com/BMcG1230/status/1239865763990814720" rel="noopener noreferrer" target="_blank">@BMcG1230</a> <span class="status">1239865763990814720</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1239863760883314688',
    created: 1584441530000,
    type: 'post',
    text: '<a href="https://youtu.be/uqLWULhi-dI" rel="noopener noreferrer" target="_blank">youtu.be/uqLWULhi-dI</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1239863539382116353',
    created: 1584441477000,
    type: 'post',
    text: 'It\'s beyond my belief how reckless &amp; irresponsible the Democrats are being by going ahead with onsite elections today (urging people to congregate). This is worse than the current adminstration. The election should be postponed &amp; switched to vote-by-mail (like Oregon &amp; Colorado).',
    likes: 3,
    retweets: 2
  },
  {
    id: '1239710511412867072',
    created: 1584404993000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mairin" rel="noopener noreferrer" target="_blank">@mairin</a> For the record, I\'m not opposed to # 2. It just takes a little getting used to is all, but maybe that\'s a good thing.<br><br>In reply to: <a href="#1239679482799075328">1239679482799075328</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1239679482799075328',
    created: 1584397595000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mairin" rel="noopener noreferrer" target="_blank">@mairin</a> I\'m really connecting with # 1. The style of the break in the stem on the "f" makes it immediately recognizable and balanced.<br><br>In reply to: <a href="https://x.com/mairin/status/1239644425581735943" rel="noopener noreferrer" target="_blank">@mairin</a> <span class="status">1239644425581735943</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1239607580558647296',
    created: 1584380452000,
    type: 'quote',
    text: 'If you\'re making the adjustment to working (or presenting) from home, here\'s some advice from someone who\'s been doing it for over two decades (and who also happens to be one of my favorite speakers).<br><br>Quoting: <a href="https://x.com/tlberglund/status/1239526562745049088" rel="noopener noreferrer" target="_blank">@tlberglund</a> <span class="status">1239526562745049088</span>',
    likes: 4,
    retweets: 1
  },
  {
    id: '1239505861686358017',
    created: 1584356200000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jaredmorgs" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> I haven\'t heard anything of that sort (beyond what some Americans already do). But they\'ve definitely wiped out the paper and medicine aisles. I\'d take a picture of it, except the aisle is already full of people taking a picture of it.<br><br>In reply to: <a href="https://x.com/jaredmorgs/status/1239505401462177793" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <span class="status">1239505401462177793</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1239500445409619968',
    created: 1584354909000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/notdred" rel="noopener noreferrer" target="_blank">@notdred</a> We said take the gloves off. We had no idea that was the power he had hiding inside them ;)<br><br>In reply to: <a href="https://x.com/notdred/status/1239356264343310338" rel="noopener noreferrer" target="_blank">@notdred</a> <span class="status">1239356264343310338</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1239499733955051520',
    created: 1584354740000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ChrisRaymondV" rel="noopener noreferrer" target="_blank">@ChrisRaymondV</a> Thank you for calling out the baiting. It\'s clearly a disingenuous question meant to distort his position. And thanks for your support too!<br><br>In reply to: <a href="https://x.com/ChrisRaymondV/status/1239365280247287808" rel="noopener noreferrer" target="_blank">@ChrisRaymondV</a> <span class="status">1239365280247287808</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1239498799094976512',
    created: 1584354517000,
    type: 'post',
    text: 'One more thing about that #DemDebate. Did anyone else find the "we\'re going to hold people harmless" legal speak from Biden to be extremely bizarre? First, don\'t hold anyone. Second, just say, "you won\'t be charged or lose income if you\'re affected by this virus."',
    likes: 0,
    retweets: 0,
    tags: ['demdebate']
  },
  {
    id: '1239483467764621312',
    created: 1584350861000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/hsablonniere" rel="noopener noreferrer" target="_blank">@hsablonniere</a> <a class="mention" href="https://x.com/opendevise" rel="noopener noreferrer" target="_blank">@opendevise</a> <a class="mention" href="https://x.com/clever_cloud" rel="noopener noreferrer" target="_blank">@clever_cloud</a> One thing you helped me understand is that being remote doesn\'t always mean being isolated.<br><br>In reply to: <a href="https://x.com/hsablonniere/status/1239480078951755777" rel="noopener noreferrer" target="_blank">@hsablonniere</a> <span class="status">1239480078951755777</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1239482770990063618',
    created: 1584350695000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bentolor" rel="noopener noreferrer" target="_blank">@bentolor</a> One thing I will not do is give up hope. I see positive change coming. I will do what I can to support the movements that are bringing it.<br><br>In reply to: <a href="https://x.com/bentolor/status/1239480258480529413" rel="noopener noreferrer" target="_blank">@bentolor</a> <span class="status">1239480258480529413</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1239464289452175361',
    created: 1584346289000,
    type: 'quote',
    text: 'Release notes via presentation. So meta! And so amazing. Great work <a class="mention" href="https://x.com/obilodeau" rel="noopener noreferrer" target="_blank">@obilodeau</a>, <a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a>, and team!<br><br>Quoting: <a href="https://x.com/obilodeau/status/1239367308050673666" rel="noopener noreferrer" target="_blank">@obilodeau</a> <span class="status">1239367308050673666</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1239463749989175296',
    created: 1584346160000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/leftyaaron" rel="noopener noreferrer" target="_blank">@leftyaaron</a> He also called us followers, which really irks me. He fundamentally doesn\'t understand this movement...that it\'s about policy, not a messiah.<br><br>In reply to: <a href="https://x.com/leftyaaron/status/1239396876417257472" rel="noopener noreferrer" target="_blank">@leftyaaron</a> <span class="status">1239396876417257472</span>',
    likes: 138,
    retweets: 14
  },
  {
    id: '1239462944766693376',
    created: 1584345968000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bentolor" rel="noopener noreferrer" target="_blank">@bentolor</a> Per the radical idea point, hardly any person I\'ve talked to believes these ideas are radical (point by point). We have a President in our history (FDR) who went even further in some cases and yet extremely popular. That\'s what makes this all so bizarre. It\'s media brainwashing.<br><br>In reply to: <a href="#1239461371760250881">1239461371760250881</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1239461920605798400',
    created: 1584345724000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bentolor" rel="noopener noreferrer" target="_blank">@bentolor</a> And this is going to continue repeating itself until that power, and the money that fuels it, is reined in.<br><br>In reply to: <a href="#1239461371760250881">1239461371760250881</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1239461371760250881',
    created: 1584345593000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bentolor" rel="noopener noreferrer" target="_blank">@bentolor</a> The scary part is that the American people are once again being deceived into believing Bernie\'s ideas are radical. It\'s also important to recognize the divide is manufactured by the political insiders. Bernie was winning and building a movement to beat Trump. They kneecapped it.<br><br>In reply to: <a href="https://x.com/bentolor/status/1239457249946488832" rel="noopener noreferrer" target="_blank">@bentolor</a> <span class="status">1239457249946488832</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1239459978194194433',
    created: 1584345261000,
    type: 'quote',
    text: 'Here\'s another thing Biden lied about tonight, and it\'s an important one. Sure, Biden changed his stance in 2012, but then this serves as an example of not leading when it\'s hard. (Bernie\'s support dates back much further and was endorsing state legislation around this time).<br><br>Quoting: <a href="https://x.com/nickusen/status/1239355308599906304" rel="noopener noreferrer" target="_blank">@nickusen</a> <span class="status">1239355308599906304</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1239388154945089537',
    created: 1584328137000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gregturn" rel="noopener noreferrer" target="_blank">@gregturn</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> I\'m extremely proud of how far it has come. Marat is doing a bang up job.<br><br>In reply to: <a href="https://x.com/gregturn/status/1239207903690702848" rel="noopener noreferrer" target="_blank">@gregturn</a> <span class="status">1239207903690702848</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1239386402858524672',
    created: 1584327719000,
    type: 'post',
    text: 'If the lying wasn\'t enough, crushing the energy Bernie has brought into the Democratic Party is a close second. It spits in the face of young people and new voters by saying "we don\'t need you to win". Unbelievable arrogance. #DemDebate<br><br> <a href="https://youtu.be/f-MTTcr5i-Q" rel="noopener noreferrer" target="_blank">youtu.be/f-MTTcr5i-Q</a>',
    likes: 3,
    retweets: 1,
    tags: ['demdebate']
  },
  {
    id: '1239381662112210944',
    created: 1584326589000,
    type: 'post',
    text: 'I had read reports, but I wasn\'t absolutely sure until tonight how willing Biden is to lie. Sorry, that\'s disqualifying for me. That\'s how I was raised. The Democrat party needs to go with Bernie if they\'re going to have any chance of winning this. #demdebate',
    likes: 9,
    retweets: 1,
    tags: ['demdebate']
  },
  {
    id: '1239372706107125761',
    created: 1584324454000,
    type: 'post',
    text: 'Let\'s not have a war on things. That never ends well. #DemDebate',
    likes: 0,
    retweets: 0,
    tags: ['demdebate']
  },
  {
    id: '1239369766986342402',
    created: 1584323753000,
    type: 'post',
    text: 'Biden saying he won despite having no organizing or money pretty much proves that the heart of his campaign in inside the beltway, miles from the people. The establishment can win without even doing any work. Is that the message? #DemDebate',
    likes: 2,
    retweets: 0,
    tags: ['demdebate']
  },
  {
    id: '1239364128176623617',
    created: 1584322409000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sunrisemvmt" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a> It\'s a step, but be careful. Based on what he suggested earlier in the debate, I think he only meant on federal lands. Watch him walk back to that position.<br><br>In reply to: <a href="https://x.com/sunrisemvmt/status/1239363585622642689" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a> <span class="status">1239363585622642689</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1239363693940371456',
    created: 1584322305000,
    type: 'post',
    text: 'Bernie: Ban fracking.<br>Biden: No new fracking (on federal lands).<br><br>On this topic, what else do you need to know? <a class="mention" href="https://x.com/sunrisemvmt" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a> #DemDebate',
    likes: 1,
    retweets: 0,
    tags: ['demdebate']
  },
  {
    id: '1239362415986237442',
    created: 1584322000000,
    type: 'post',
    text: 'We need to transform our energy system away from fossil fuels aggressively, but nevermind about that, let\'s talk about those beetles spreading disease and eating foliage in the NorthEast. Right? Those damn beetles. #DemDebate <a class="mention" href="https://x.com/sunrisemvmt" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a>',
    likes: 0,
    retweets: 0,
    tags: ['demdebate']
  },
  {
    id: '1239356910505086976',
    created: 1584320688000,
    type: 'quote',
    text: 'That just proves to me he still doesn\'t regret saying it. So how the hell does he have the support of older citizens? I just don\'t get it. #DemDebate<br><br>Quoting: <a href="https://x.com/JordanChariton/status/1239354153706586112" rel="noopener noreferrer" target="_blank">@JordanChariton</a> <span class="status">1239354153706586112</span>',
    likes: 2,
    retweets: 0,
    tags: ['demdebate']
  },
  {
    id: '1239354534582874113',
    created: 1584320121000,
    type: 'post',
    text: '"What leadership is about is going forward when it\'s not a popular idea." <a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a> his whole career. #DemDebate',
    likes: 0,
    retweets: 0,
    tags: ['demdebate']
  },
  {
    id: '1239353744602435585',
    created: 1584319933000,
    type: 'post',
    text: 'Joe Biden just flat out lied about what he said in the Senate. If we\'re going to say blatant lying is disqualifying, as we rightfully do regarding the current President, you can\'t give an exception because that person is anointed by the Democrats. #DemDebate',
    likes: 0,
    retweets: 0,
    tags: ['demdebate']
  },
  {
    id: '1239347865987751938',
    created: 1584318531000,
    type: 'post',
    text: 'Biden seems to only care about people\'s welfare in a crisis. But what after that? They\'re on their own? Rugged individualism? #DemDebate',
    likes: 2,
    retweets: 0,
    tags: ['demdebate']
  },
  {
    id: '1239346273012068354',
    created: 1584318152000,
    type: 'post',
    text: 'War. Forces. Deploy. Surging.<br><br>These are frightening words coming out of Joe Biden\'s mouth.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1239344889667768321',
    created: 1584317822000,
    type: 'post',
    text: '"I don\'t want to get into politics." - Biden<br><br>Um, dude, this is a political debate. Hello!',
    likes: 1,
    retweets: 0
  },
  {
    id: '1239343948734308352',
    created: 1584317597000,
    type: 'post',
    text: 'There it is right there. Biden attacked Medicare for All and took a shot at Italy at the same time. Folks, that\'s just more of the same. Bernie is the hope and change, in this crisis and beyond. #DemDebate',
    likes: 6,
    retweets: 0,
    tags: ['demdebate']
  },
  {
    id: '1239152452978393088',
    created: 1584271941000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DavidAgStone" rel="noopener noreferrer" target="_blank">@DavidAgStone</a> <a class="mention" href="https://x.com/diamondlucci" rel="noopener noreferrer" target="_blank">@diamondlucci</a> <a class="mention" href="https://x.com/Celentra" rel="noopener noreferrer" target="_blank">@Celentra</a> I think if he does several a week, they can be shorter. Shorter discussions are easier to share. The sweet spot is 30-45 minutes.<br><br>In reply to: <a href="https://x.com/DavidAgStone/status/1238987160700637184" rel="noopener noreferrer" target="_blank">@DavidAgStone</a> <span class="status">1238987160700637184</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1239152100220624897',
    created: 1584271857000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Celentra" rel="noopener noreferrer" target="_blank">@Celentra</a> Keep doing these. Do 1,000 if you must. I think they\'ll reach way more people than even the rallies do. Make the dialog as conversational as possible. Try different guests too.<br><br>In reply to: <a href="https://x.com/Celentra/status/1238982769893683200" rel="noopener noreferrer" target="_blank">@Celentra</a> <span class="status">1238982769893683200</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1239150784610091008',
    created: 1584271544000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/briebriejoy" rel="noopener noreferrer" target="_blank">@briebriejoy</a> and that next part..."more than enough people to tell us that we didn\'t get covered for what we thought we paid for."<br><br>In reply to: <a href="https://x.com/briebriejoy/status/1238974694419808256" rel="noopener noreferrer" target="_blank">@briebriejoy</a> <span class="status">1238974694419808256</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1239120925175345152',
    created: 1584264424000,
    type: 'quote',
    text: 'Hopefully this helps everyone realize that we\'re not trying to distance from each other forever, but rather to default a shared threat. In other words, we\'re all on the same team the virus is the opponent. Let\'s fight it together.<br><br>Quoting: <a href="https://x.com/ConallMcD/status/1238865202868883461" rel="noopener noreferrer" target="_blank">@ConallMcD</a> <span class="status">1238865202868883461</span>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1239003163539042310',
    created: 1584236348000,
    type: 'quote',
    text: 'You can\'t replace one liar with another and expect things to be different. Lying is disqualifying.<br><br>Quoting: <a href="https://x.com/DavidShuster/status/1238832450169643009" rel="noopener noreferrer" target="_blank">@DavidShuster</a> <span class="status">1238832450169643009</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1239001538132996096',
    created: 1584235960000,
    type: 'quote',
    text: 'Bernie hosted a #FiresideChat and, as you\'d expect, he actually lit the fire himself. Folks, it doesn\'t get much more genuine than that.<br><br>Quoting: <a href="https://x.com/fshakir/status/1202051015614308354" rel="noopener noreferrer" target="_blank">@fshakir</a> <span class="status">1202051015614308354</span>',
    likes: 5,
    retweets: 0,
    tags: ['firesidechat']
  },
  {
    id: '1238896180278358017',
    created: 1584210841000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/johncusack" rel="noopener noreferrer" target="_blank">@johncusack</a> <a class="mention" href="https://x.com/shaunking" rel="noopener noreferrer" target="_blank">@shaunking</a> <a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a> If he stays in the friend zone, the revolution waits another 4 years. It\'s as simple as that. I, for one, don\'t accept that.<br><br>In reply to: <a href="https://x.com/johncusack/status/1238687269420138497" rel="noopener noreferrer" target="_blank">@johncusack</a> <span class="status">1238687269420138497</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1238728278464053249',
    created: 1584170810000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> <a class="mention" href="https://x.com/theotown" rel="noopener noreferrer" target="_blank">@theotown</a> Exactly. It\'s about looking out for the citizens (&amp; the nation\'s health) ALL the way though the crisis. Not just testing them &amp; throwing them back out in the storm. Right now, it means protecting lost wages. Other measures down the road. To look at this as free shit is inhumane.<br><br>In reply to: <a href="https://x.com/brunoborges/status/1238721122155114498" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1238721122155114498</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1238688941395431425',
    created: 1584161432000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/genehack" rel="noopener noreferrer" target="_blank">@genehack</a> <a class="mention" href="https://x.com/joshsimmons" rel="noopener noreferrer" target="_blank">@joshsimmons</a> My spouse and I LOVE The Repair Shop! I\'m constantly amazed by what they\'re able to restore.<br><br>In reply to: <a href="https://x.com/genehack/status/1238314575776190464" rel="noopener noreferrer" target="_blank">@genehack</a> <span class="status">1238314575776190464</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1238639151043342337',
    created: 1584149561000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ipbabble" rel="noopener noreferrer" target="_blank">@ipbabble</a> The key is to look for the good and raise up those voices.<br><br>In reply to: @ipbabble <span class="status">&lt;deleted&gt;</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1238635722040467456',
    created: 1584148743000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tom_enebo" rel="noopener noreferrer" target="_blank">@tom_enebo</a> I also find hat beer in the chili, beer served with the chili, and beer when making the chili is what really pulls it together. (And even beer with no chili at all).<br><br>In reply to: <a href="https://x.com/tom_enebo/status/1238633198285971457" rel="noopener noreferrer" target="_blank">@tom_enebo</a> <span class="status">1238633198285971457</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1238631738504437760',
    created: 1584147793000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tom_enebo" rel="noopener noreferrer" target="_blank">@tom_enebo</a> Me too! I\'m making this kick ass one: <a href="https://www.thugkitchen.com/recipes/pumpkin-chili" rel="noopener noreferrer" target="_blank">www.thugkitchen.com/recipes/pumpkin-chili</a><br><br>In reply to: <a href="https://x.com/tom_enebo/status/1238622768113074176" rel="noopener noreferrer" target="_blank">@tom_enebo</a> <span class="status">1238622768113074176</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1238615181585977345',
    created: 1584143846000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tom_enebo" rel="noopener noreferrer" target="_blank">@tom_enebo</a> That\'s why I\'m throwing together two soups. It keeps me off the road and requires minimal ingredients. Seems like the best strategy right now.<br><br>In reply to: <a href="https://x.com/tom_enebo/status/1238607224467009547" rel="noopener noreferrer" target="_blank">@tom_enebo</a> <span class="status">1238607224467009547</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1238605790623105026',
    created: 1584141607000,
    type: 'quote',
    text: 'Citizens of the world. This is what a President sounds like.<br><br>Quoting: <a href="https://x.com/BernieSanders/status/1238536047455330306" rel="noopener noreferrer" target="_blank">@BernieSanders</a> <span class="status">1238536047455330306</span>',
    likes: 8,
    retweets: 2
  },
  {
    id: '1238603889756090368',
    created: 1584141154000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AlexandriaV2005" rel="noopener noreferrer" target="_blank">@AlexandriaV2005</a> <a class="mention" href="https://x.com/patagonia" rel="noopener noreferrer" target="_blank">@patagonia</a> <a class="mention" href="https://x.com/WholeFoods" rel="noopener noreferrer" target="_blank">@WholeFoods</a> <a class="mention" href="https://x.com/patagonia" rel="noopener noreferrer" target="_blank">@patagonia</a> is consistently on the right side of history.<br><br>In reply to: <a href="https://x.com/AlexandriaV2005/status/1238603473542737920" rel="noopener noreferrer" target="_blank">@AlexandriaV2005</a> <span class="status">1238603473542737920</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1238603671635517441',
    created: 1584141102000,
    type: 'reply',
    text: '@jbryant787 Totally. One thing that makes me feel better about social distancing is that it\'s a win-win. Both society and the environment benefits. So I can really get behind that.<br><br>In reply to: @jbryant787 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1238601666150031360',
    created: 1584140623000,
    type: 'reply',
    text: '@jbryant787 I\'m going to strongly consider switching to that strategy. Unfortunately, as soon as I\'ve gotten my zero waste routine down, it\'s being thrown into disarray.<br><br>In reply to: @jbryant787 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1238600980364550144',
    created: 1584140460000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/shaunking" rel="noopener noreferrer" target="_blank">@shaunking</a> <a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a> He HAS to go hard. He HAS to be relentless. If this is really about US, then he needs to get out of the friend zone.<br><br>In reply to: <a href="https://x.com/shaunking/status/1238528402920964097" rel="noopener noreferrer" target="_blank">@shaunking</a> <span class="status">1238528402920964097</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1238600569939353601',
    created: 1584140362000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a> Please make the case on Sunday that only you have the policies, preparation, temperament, and experience to be President. And please don\'t call Biden your friend. He may be your friend, but he\'s not our friend. And this is about #NotMeUs.',
    likes: 0,
    retweets: 0,
    tags: ['notmeus']
  },
  {
    id: '1238599795129737217',
    created: 1584140177000,
    type: 'post',
    text: 'I just went out to buy ingredients for soup. It\'s crazy out here. I think I need to stay in just for my own sanity, if not only for social distancing.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1238599318472278016',
    created: 1584140064000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bk2204" rel="noopener noreferrer" target="_blank">@bk2204</a> Seeing the same thing here.<br><br>In reply to: <a href="https://x.com/bk2204/status/1238590958930997248" rel="noopener noreferrer" target="_blank">@bk2204</a> <span class="status">1238590958930997248</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1238562875150565379',
    created: 1584131375000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/leftyaaron" rel="noopener noreferrer" target="_blank">@leftyaaron</a> <a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a> He needs to come out swinging at this point.<br><br>In reply to: <a href="https://x.com/leftyaaron/status/1238562036025454592" rel="noopener noreferrer" target="_blank">@leftyaaron</a> <span class="status">1238562036025454592</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1238398360781873152',
    created: 1584092152000,
    type: 'post',
    text: 'Climate striking while social distancing. It\'s the essential recipe for getting back to normal.<br><br>#ClimateStrikeOnline #DigitalStrikeOnline Solidarity with <a class="mention" href="https://x.com/GretaThunberg" rel="noopener noreferrer" target="_blank">@GretaThunberg</a> and #FridaysForFuture',
    photos: ['<div class="item"><img class="photo" src="media/1238398360781873152.jpg"></div>'],
    likes: 9,
    retweets: 0,
    tags: ['climatestrikeonline', 'digitalstrikeonline', 'fridaysforfuture']
  },
  {
    id: '1238376257487654914',
    created: 1584086882000,
    type: 'post',
    text: 'I\'ve been ramping up work on Antora these last two weeks. We\'re close to bringing you the 2.3 release with some exciting new features, such as multiple start paths per content source, automatic reference text for page references, and a helper to lookup other pages in the UI.',
    likes: 5,
    retweets: 0
  },
  {
    id: '1238373977447231489',
    created: 1584086338000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SimonNarode" rel="noopener noreferrer" target="_blank">@SimonNarode</a> <a class="mention" href="https://x.com/NomikiKonst" rel="noopener noreferrer" target="_blank">@NomikiKonst</a> <a class="mention" href="https://x.com/TheNomikiShow" rel="noopener noreferrer" target="_blank">@TheNomikiShow</a> "student load debt" is a typo, but also kind of descriptive.<br><br>In reply to: <a href="#1238314967012524034">1238314967012524034</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1238372009555312640',
    created: 1584085869000,
    type: 'post',
    text: '"It\'s going to be very difficult for a candidate who reiterates the status quo of endless wars to win against Trump."',
    likes: 0,
    retweets: 0
  },
  {
    id: '1238371873374658562',
    created: 1584085837000,
    type: 'post',
    text: 'Biden is the safe choice? He not only voted for the Iraq War, he helped orchestrate consent for it. He\'s a safe bet if what you\'re looking for is more regime change wars.<br><br><a href="https://youtu.be/rrLFQnlf6lQ" rel="noopener noreferrer" target="_blank">youtu.be/rrLFQnlf6lQ</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1238371200029437954',
    created: 1584085676000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SimonNarode" rel="noopener noreferrer" target="_blank">@SimonNarode</a> <a class="mention" href="https://x.com/NomikiKonst" rel="noopener noreferrer" target="_blank">@NomikiKonst</a> <a class="mention" href="https://x.com/TheNomikiShow" rel="noopener noreferrer" target="_blank">@TheNomikiShow</a> That should read: He literally trapped a generation in student loan debt they cannot escape from.<br><br>In reply to: <a href="#1238314967012524034">1238314967012524034</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1238314967012524034',
    created: 1584072269000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SimonNarode" rel="noopener noreferrer" target="_blank">@SimonNarode</a> <a class="mention" href="https://x.com/NomikiKonst" rel="noopener noreferrer" target="_blank">@NomikiKonst</a> <a class="mention" href="https://x.com/TheNomikiShow" rel="noopener noreferrer" target="_blank">@TheNomikiShow</a> For example, say that the reason we have to cancel student debt is because Biden wrote the bill that prevents student debt from being included in bankruptcy. He literally trapped a generation in student load debt that cannot escape from. SAY THAT!<br><br>In reply to: <a href="#1238313182130663427">1238313182130663427</a>',
    likes: 21,
    retweets: 5
  },
  {
    id: '1238313182130663427',
    created: 1584071844000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SimonNarode" rel="noopener noreferrer" target="_blank">@SimonNarode</a> <a class="mention" href="https://x.com/NomikiKonst" rel="noopener noreferrer" target="_blank">@NomikiKonst</a> <a class="mention" href="https://x.com/TheNomikiShow" rel="noopener noreferrer" target="_blank">@TheNomikiShow</a> I totally agree, he HAS to take the gloves off. If he truly believes this is a crisis, as those of us in the movement do, there is no more time for niceties. Politics isn\'t pretty. We don\'t use the word "fight" for nothing. We need to put up a fight this time. HIT BIDEN HARD.<br><br>In reply to: <a href="https://x.com/SimonNarode/status/1238276442028257280" rel="noopener noreferrer" target="_blank">@SimonNarode</a> <span class="status">1238276442028257280</span>',
    likes: 10,
    retweets: 1
  },
  {
    id: '1238287055605522433',
    created: 1584065614000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/chuckfrain" rel="noopener noreferrer" target="_blank">@chuckfrain</a> Because we wouldn\'t be experiencing a spread (and panic) like this if we had a system that actually focused on care and response instead of profits and giving tax breaks for the rich (including secondary/ related systems like CDC).<br><br>In reply to: <a href="https://x.com/chuckfrain/status/1238276469140455425" rel="noopener noreferrer" target="_blank">@chuckfrain</a> <span class="status">1238276469140455425</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1238269328530624513',
    created: 1584061388000,
    type: 'post',
    text: 'This is why you don\'t settle on a half-baked health care system. How are we going to pay #MedicareForAll? Well, I ask, how are we going to pay for society grinding to a halt? We screwed around and this is the consequence. The consequence of votes. #coronavirus',
    likes: 3,
    retweets: 0,
    tags: ['medicareforall', 'coronavirus']
  },
  {
    id: '1238176845998088192',
    created: 1584039338000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jdiggerj" rel="noopener noreferrer" target="_blank">@jdiggerj</a> Will do. And it goes without saying I\'ll be thinking of you too! Good luck!<br><br>In reply to: <a href="https://x.com/jdiggerj/status/1238068471331729409" rel="noopener noreferrer" target="_blank">@jdiggerj</a> <span class="status">1238068471331729409</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1237991909823369216',
    created: 1583995246000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MStine" rel="noopener noreferrer" target="_blank">@MStine</a> Clearly, they couldn\'t see your ❤️.<br><br>In reply to: @mstine <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1237814258324271104',
    created: 1583952891000,
    type: 'quote',
    text: 'Really, really encouraging for the progressive movement. I\'m excited to be rallying together behind a common agenda.<br><br>Quoting: <a href="https://x.com/shaunking/status/1237517521550360576" rel="noopener noreferrer" target="_blank">@shaunking</a> <span class="status">1237517521550360576</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1237677754755227648',
    created: 1583920346000,
    type: 'reply',
    text: '@SeaSparks21 <a class="mention" href="https://x.com/thugkitchen" rel="noopener noreferrer" target="_blank">@thugkitchen</a> I also love that this meal is dirt cheap. Carrots. Onions. Buns. BBQ sauce. Boom. Then look around for whatever toppings you\'ve got. Or go all out for the cilantro pesto.<br><br>In reply to: <a href="#1237677209466351616">1237677209466351616</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1237677209466351616',
    created: 1583920216000,
    type: 'reply',
    text: '@SeaSparks21 <a class="mention" href="https://x.com/thugkitchen" rel="noopener noreferrer" target="_blank">@thugkitchen</a> I was in no way prepared for what I was about to taste. Try all the different toppings. Each combo gives a different experience.<br><br>In reply to: @SeaSparks21 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1237648378449678336',
    created: 1583913342000,
    type: 'post',
    text: 'On a night that felt like the world\'s turning upside down, I discovered the glory of carrot dogs. What genius thought of this creation?!? Holy hell. YOU\'VE GOT TO TRY THIS! (Thank you for making my night right, <a class="mention" href="https://x.com/thugkitchen" rel="noopener noreferrer" target="_blank">@thugkitchen</a>).',
    photos: ['<div class="item"><img class="photo" src="media/1237648378449678336.jpg"></div>', '<div class="item"><img class="photo" src="media/1237648378449678336-2.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1237642986432778246',
    created: 1583912056000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/VanJones68" rel="noopener noreferrer" target="_blank">@VanJones68</a> Damn right. Only this time, the stakes are even higher. Like WAY higher.<br><br>In reply to: <a href="https://x.com/VanJones68/status/1237555567855570945" rel="noopener noreferrer" target="_blank">@VanJones68</a> <span class="status">1237555567855570945</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1237483236772597761',
    created: 1583873969000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/__Cloudia" rel="noopener noreferrer" target="_blank">@__Cloudia</a> You put my state of mind into words.<br><br>In reply to: <a href="https://x.com/__Cloudia/status/1237476064898711558" rel="noopener noreferrer" target="_blank">@__Cloudia</a> <span class="status">1237476064898711558</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1237432505256439808',
    created: 1583861874000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/NelStamp" rel="noopener noreferrer" target="_blank">@NelStamp</a> Also remind them of this incredibly strong proposal for women\'s reproductive rights that would have an immediate impact for women on day 1.<br><br><a href="https://berniesanders.com/issues/reproductive-justice-all/" rel="noopener noreferrer" target="_blank">berniesanders.com/issues/reproductive-justice-all/</a><br><br>In reply to: <a href="https://x.com/NelStamp/status/1237376902693883904" rel="noopener noreferrer" target="_blank">@NelStamp</a> <span class="status">1237376902693883904</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1237428113648136193',
    created: 1583860827000,
    type: 'post',
    text: 'As I watch this election process play out, these words by <a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a> come to mind.<br><br>"We cannot convincingly promote democracy abroad if we do not live it vigorously here at home."<br><br>In other words, we have some looking the the mirror to do.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1237137067236151296',
    created: 1583791436000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/connolly_s" rel="noopener noreferrer" target="_blank">@connolly_s</a> <a class="mention" href="https://x.com/devongovett" rel="noopener noreferrer" target="_blank">@devongovett</a> It really is a whole new ballgame. So many hacks just melt away.<br><br>In reply to: <a href="https://x.com/connolly_s/status/1237136198201729030" rel="noopener noreferrer" target="_blank">@connolly_s</a> <span class="status">1237136198201729030</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1237117902081277953',
    created: 1583786867000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> <a class="mention" href="https://x.com/nottycode" rel="noopener noreferrer" target="_blank">@nottycode</a> Salient point. It\'s the sentiment I was going for.<br><br>In reply to: <a href="https://x.com/garrett/status/1237117560769970177" rel="noopener noreferrer" target="_blank">@garrett</a> <span class="status">1237117560769970177</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1237107977724645376',
    created: 1583784500000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nottycode" rel="noopener noreferrer" target="_blank">@nottycode</a> That\'s a cultural reference lost in translation. As I meant it, it means as friends would walk. Perhaps a picture helps.<br><br>In reply to: <a href="https://x.com/nottycode/status/1237105987829579783" rel="noopener noreferrer" target="_blank">@nottycode</a> <span class="status">1237105987829579783</span>',
    photos: ['<div class="item"><img class="photo" src="media/1237107977724645376.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1237097018759118848',
    created: 1583781888000,
    type: 'post',
    text: 'If you\'re able to vote tomorrow in a US primary election tomorrow, and you care about the future of this country, your fellow citizens of all ages, and this planet, I beg of you, vote for Bernie. And put your arm around your neighbors and stroll with them to the polls.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1237085837709340672',
    created: 1583779222000,
    type: 'quote',
    text: 'This is prime example of how #COVIDー19 is a medical justice issue.<br><br>(And while we\'re on the subject, school lunches should be free).<br><br>Quoting: <a href="https://x.com/ClintSmithIII/status/1237004025331167233" rel="noopener noreferrer" target="_blank">@ClintSmithIII</a> <span class="status">1237004025331167233</span>',
    likes: 1,
    retweets: 1,
    tags: ['covid19']
  },
  {
    id: '1237078733535596552',
    created: 1583777528000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AnandWrites" rel="noopener noreferrer" target="_blank">@AnandWrites</a> I\'d also like to see some damn empathy. Instead of "these policies are so radical, so here\'s why you should fear them" how about "these policies save millions of lives, perhaps the planet, so show some courage and support them if you care about life as we know it"<br><br>In reply to: <a href="https://x.com/AnandWrites/status/1237045471509729283" rel="noopener noreferrer" target="_blank">@AnandWrites</a> <span class="status">1237045471509729283</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1237077332721991680',
    created: 1583777194000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AnandWrites" rel="noopener noreferrer" target="_blank">@AnandWrites</a> The truth.<br><br>In reply to: <a href="https://x.com/AnandWrites/status/1237045471509729283" rel="noopener noreferrer" target="_blank">@AnandWrites</a> <span class="status">1237045471509729283</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1237076279616102401',
    created: 1583776943000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AndrewYang" rel="noopener noreferrer" target="_blank">@AndrewYang</a> Or endorse a candidate that isn\'t swimming in Wall Street, big pharma, and corporate PAC money so maybe we have a chance at any sort of future.<br><br>In reply to: <a href="https://x.com/AndrewYang/status/1236993176046616578" rel="noopener noreferrer" target="_blank">@AndrewYang</a> <span class="status">1236993176046616578</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1237075018082701314',
    created: 1583776642000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/NelStamp" rel="noopener noreferrer" target="_blank">@NelStamp</a> <a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a> Thanks!<br><br>In reply to: <a href="https://x.com/NelStamp/status/1237027824273436673" rel="noopener noreferrer" target="_blank">@NelStamp</a> <span class="status">1237027824273436673</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1236912828193189888',
    created: 1583737973000,
    type: 'reply',
    text: 'I would be remiss not to mention <a class="mention" href="https://x.com/NaomiAKlein" rel="noopener noreferrer" target="_blank">@NaomiAKlein</a> (who inspired <a class="mention" href="https://x.com/Janefonda" rel="noopener noreferrer" target="_blank">@Janefonda</a> into climate action) and <a class="mention" href="https://x.com/SylviaEarle" rel="noopener noreferrer" target="_blank">@SylviaEarle</a> (because without blue, there is no green).<br><br>In reply to: <a href="#1236780769902604288">1236780769902604288</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1236812769434849280',
    created: 1583714117000,
    type: 'post',
    text: '"Don\'t engage in negative debates. Always avoid making negative remarks."',
    likes: 0,
    retweets: 0
  },
  {
    id: '1236810526572699648',
    created: 1583713583000,
    type: 'post',
    text: '"If we ever encounter disrespect or hostility from others, we walk away from the situation."',
    likes: 0,
    retweets: 0
  },
  {
    id: '1236810248658153472',
    created: 1583713516000,
    type: 'post',
    text: '"No matter what, we stay positive and focused on what matters. We don’t mention our opponents by name!"',
    likes: 0,
    retweets: 0
  },
  {
    id: '1236809837138202624',
    created: 1583713418000,
    type: 'post',
    text: '"We treat everyone we encounter with care and respect, whether or not they agree with us."',
    likes: 0,
    retweets: 0
  },
  {
    id: '1236809266360504321',
    created: 1583713282000,
    type: 'post',
    text: 'So this narrative that the campaign tolerates anything but the utmost professional behavior is a complete myth. Believe what you want, but these are the facts which you can verify yourself.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1236808501940215808',
    created: 1583713100000,
    type: 'post',
    text: 'In order to volunteer for Bernie, you must attend compliance training. You must also agree to a code of conduct that clearly and repeatedly states you\'re required to treat everyone with dignity and respect and never use negative language. See for yourself. <a href="https://berniesanders.com/text/learn/" rel="noopener noreferrer" target="_blank">berniesanders.com/text/learn/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1236780769902604288',
    created: 1583706488000,
    type: 'post',
    text: 'On this #IWD I want to lift up the voices of the many women who work day &amp; night to protect that which gives us all life, Earth, including <a class="mention" href="https://x.com/GretaThunberg" rel="noopener noreferrer" target="_blank">@GretaThunberg</a> <a class="mention" href="https://x.com/vanessa_vash" rel="noopener noreferrer" target="_blank">@vanessa_vash</a> <a class="mention" href="https://x.com/AlexandriaV2005" rel="noopener noreferrer" target="_blank">@AlexandriaV2005</a> <a class="mention" href="https://x.com/Janefonda" rel="noopener noreferrer" target="_blank">@Janefonda</a> <a class="mention" href="https://x.com/Jamie_Margolin" rel="noopener noreferrer" target="_blank">@Jamie_Margolin</a> <a class="mention" href="https://x.com/VarshPrakash" rel="noopener noreferrer" target="_blank">@VarshPrakash</a> <a class="mention" href="https://x.com/israhirsi" rel="noopener noreferrer" target="_blank">@israhirsi</a> <a class="mention" href="https://x.com/AOC" rel="noopener noreferrer" target="_blank">@AOC</a> and many more I haven\'t mentioned.',
    likes: 2,
    retweets: 0,
    tags: ['iwd']
  },
  {
    id: '1236609152568283142',
    created: 1583665571000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/WorkingFamilies" rel="noopener noreferrer" target="_blank">@WorkingFamilies</a> Would you please consider endorsing Bernie now? The progressive movement could really use your support.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1236542172884688896',
    created: 1583649602000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/romanoff2020" rel="noopener noreferrer" target="_blank">@romanoff2020</a> Wow! Way to go dude. Game on!<br><br>In reply to: <a href="https://x.com/AndrewRomanoff/status/1236480756559613952" rel="noopener noreferrer" target="_blank">@AndrewRomanoff</a> <span class="status">1236480756559613952</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1236436741902360576',
    created: 1583624465000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ilyseh" rel="noopener noreferrer" target="_blank">@ilyseh</a> <a class="mention" href="https://x.com/briebriejoy" rel="noopener noreferrer" target="_blank">@briebriejoy</a> <a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a> <a class="mention" href="https://x.com/JoeBiden" rel="noopener noreferrer" target="_blank">@JoeBiden</a> Awesome! We should also remind folks that Bernie organized 70 rallies in 2017 to save the ACA. One of the main reasons he did it was to protect funding for Planned Parenthood.<br><br>In reply to: <a href="https://x.com/ilyseh/status/1236398599086497796" rel="noopener noreferrer" target="_blank">@ilyseh</a> <span class="status">1236398599086497796</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1236405129693335552',
    created: 1583616928000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/venkat_s" rel="noopener noreferrer" target="_blank">@venkat_s</a> @devdotnext <a class="mention" href="https://x.com/Oracle" rel="noopener noreferrer" target="_blank">@Oracle</a> <a class="mention" href="https://x.com/JRebel_Java" rel="noopener noreferrer" target="_blank">@JRebel_Java</a> <a class="mention" href="https://x.com/AzulSystems" rel="noopener noreferrer" target="_blank">@AzulSystems</a> <a class="mention" href="https://x.com/pragprog" rel="noopener noreferrer" target="_blank">@pragprog</a> <a class="mention" href="https://x.com/InfoQ" rel="noopener noreferrer" target="_blank">@InfoQ</a> <a class="mention" href="https://x.com/UtahJava" rel="noopener noreferrer" target="_blank">@UtahJava</a> <a class="mention" href="https://x.com/JavaMUG" rel="noopener noreferrer" target="_blank">@JavaMUG</a> <a class="mention" href="https://x.com/denverjug" rel="noopener noreferrer" target="_blank">@denverjug</a> <a class="mention" href="https://x.com/dosug" rel="noopener noreferrer" target="_blank">@dosug</a> The timing wasn\'t good for me the first time around, so I\'d like to step up to participate on the reboot.<br><br>In reply to: <a href="https://x.com/venkat_s/status/1235988220460126208" rel="noopener noreferrer" target="_blank">@venkat_s</a> <span class="status">1235988220460126208</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1236159910145998849',
    created: 1583558463000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nearyd" rel="noopener noreferrer" target="_blank">@nearyd</a> <a class="mention" href="https://x.com/joshsimmons" rel="noopener noreferrer" target="_blank">@joshsimmons</a> In my experience so far working closely with the Eclipse Foundation, I second this statement.<br><br>In reply to: <a href="https://x.com/nearyd/status/1236109245831163904" rel="noopener noreferrer" target="_blank">@nearyd</a> <span class="status">1236109245831163904</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1236070181165600768',
    created: 1583537070000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ChrisGSeaton" rel="noopener noreferrer" target="_blank">@ChrisGSeaton</a> <a class="mention" href="https://x.com/gregory_boggs" rel="noopener noreferrer" target="_blank">@gregory_boggs</a> <a class="mention" href="https://x.com/lrz" rel="noopener noreferrer" target="_blank">@lrz</a> The problem with that argument in the US is that political parties use public funds and their elections are almost always conducted by the state government. So it doesn\'t make sense here. They aren\'t private clubs (at least the two main parties). They must answer to the people.<br><br>In reply to: <a href="https://x.com/ChrisGSeaton/status/1236067422794330114" rel="noopener noreferrer" target="_blank">@ChrisGSeaton</a> <span class="status">1236067422794330114</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1236068086626701313',
    created: 1583536571000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RepKenBuck" rel="noopener noreferrer" target="_blank">@RepKenBuck</a> This is absolutely disgusting, particularly in a state where people have been killed and terrorized by that weapon. As your constituent, I am ashamed of you!<br><br>In reply to: <a href="https://x.com/RepKenBuck/status/1235944686910660609" rel="noopener noreferrer" target="_blank">@RepKenBuck</a> <span class="status">1235944686910660609</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1236066462235672576',
    created: 1583536184000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ChrisGSeaton" rel="noopener noreferrer" target="_blank">@ChrisGSeaton</a> <a class="mention" href="https://x.com/gregory_boggs" rel="noopener noreferrer" target="_blank">@gregory_boggs</a> <a class="mention" href="https://x.com/lrz" rel="noopener noreferrer" target="_blank">@lrz</a> In other words, it\'s at the very heart of American values to be independent. And as a free person, you can lend your support where it\'s needed. This party association thing is more of a recent invention.<br><br>In reply to: <a href="#1236065986417065985">1236065986417065985</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1236065986417065985',
    created: 1583536070000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ChrisGSeaton" rel="noopener noreferrer" target="_blank">@ChrisGSeaton</a> <a class="mention" href="https://x.com/gregory_boggs" rel="noopener noreferrer" target="_blank">@gregory_boggs</a> <a class="mention" href="https://x.com/lrz" rel="noopener noreferrer" target="_blank">@lrz</a> I\'ve talked to a lot of fellow independents and I can tell you why. They don\'t want to be corralled into a party because they feel used by that party. But they very much care who runs because that person ends up affecting their lives substantially.<br><br>In reply to: <a href="https://x.com/ChrisGSeaton/status/1236063497886277636" rel="noopener noreferrer" target="_blank">@ChrisGSeaton</a> <span class="status">1236063497886277636</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1236053329739780096',
    created: 1583533053000,
    type: 'post',
    text: 'And A LOT of them there were mothers and fathers still raising their children (&amp; some who lost children). So when you fire up a tweet to vilify Bernie supporters &amp; volunteers, think about who you\'re directing it towards and ask yourself if they\'re really your intended target.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1236050920875503619',
    created: 1583532478000,
    type: 'post',
    text: 'And I received more hugs in those few days than I\'ve ever received at one time, even compared to events with family and friends.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1236050574023340032',
    created: 1583532396000,
    type: 'post',
    text: 'I went to the Democratic National Convention in 2016 (outside the fences). At the Bernie gatherings, I met some of the most tenderhearted people I\'ve ever encountered who genuinely care about the lives of fellow citizens. All they talked about were programs that help people.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1236045619162001408',
    created: 1583531214000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sikhprof" rel="noopener noreferrer" target="_blank">@sikhprof</a> 🤗<br><br>In reply to: <a href="https://x.com/simran/status/1235737098461892609" rel="noopener noreferrer" target="_blank">@simran</a> <span class="status">1235737098461892609</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1236020265793900544',
    created: 1583525170000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/COChicanoJoe" rel="noopener noreferrer" target="_blank">@COChicanoJoe</a> <a class="mention" href="https://x.com/Diana4Colorado" rel="noopener noreferrer" target="_blank">@Diana4Colorado</a> <a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a> <a class="mention" href="https://x.com/ewarren" rel="noopener noreferrer" target="_blank">@ewarren</a> So I stand with you in disavowing any such behavior, because it\'s wrong and doesn\'t follow Bernie\'s lead. We can be tough on issues, but we must be soft on people.<br><br>In reply to: <a href="#1236019130181926912">1236019130181926912</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1236019130181926912',
    created: 1583524899000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/COChicanoJoe" rel="noopener noreferrer" target="_blank">@COChicanoJoe</a> <a class="mention" href="https://x.com/Diana4Colorado" rel="noopener noreferrer" target="_blank">@Diana4Colorado</a> <a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a> <a class="mention" href="https://x.com/ewarren" rel="noopener noreferrer" target="_blank">@ewarren</a> The internet can be an extremely toxic forum. But that is NOT coming from Bernie. Those people are acting on their own dispositions and do not represent the movement. For all we know, they are sowing seeds of discord.<br><br>In reply to: <a href="#1236018200904486912">1236018200904486912</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1236018200904486912',
    created: 1583524677000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/COChicanoJoe" rel="noopener noreferrer" target="_blank">@COChicanoJoe</a> <a class="mention" href="https://x.com/Diana4Colorado" rel="noopener noreferrer" target="_blank">@Diana4Colorado</a> <a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a> <a class="mention" href="https://x.com/ewarren" rel="noopener noreferrer" target="_blank">@ewarren</a> I want to say something here that might help. In the 5+ years I\'ve closely followed Bernie, I have always understood from him that there\'s no place for negative behavior. I tell people all the time he\'s taught me more about civility &amp; solidarity than any person I\'ve ever known.<br><br>In reply to: <a href="https://x.com/COChicanoJoe/status/1235958768581533696" rel="noopener noreferrer" target="_blank">@COChicanoJoe</a> <span class="status">1235958768581533696</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1235898098284556288',
    created: 1583496043000,
    type: 'post',
    text: 'To think someone who\'s in a position to champion major reform would just sit on the sidelines during this election is infuriating. It\'s not like we\'re in the middle of a climate or health crisis or anything...oh wait.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1235725479518195712',
    created: 1583454887000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GeoffMiami" rel="noopener noreferrer" target="_blank">@GeoffMiami</a> And I\'m tremendously sorry you\'ve been put in this position. On the bright side, you have a personal story that A LOT of people are going to relate to. Tell it.<br><br>In reply to: <a href="#1235723683886321664">1235723683886321664</a>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1235723683886321664',
    created: 1583454459000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GeoffMiami" rel="noopener noreferrer" target="_blank">@GeoffMiami</a> This is exactly why, when we talk about money in politics, we must treat it like the plague!<br><br>In reply to: <a href="https://x.com/GeoffMiami/status/1235719559849693185" rel="noopener noreferrer" target="_blank">@GeoffMiami</a> <span class="status">1235719559849693185</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1235685806334832643',
    created: 1583445428000,
    type: 'post',
    text: 'Check out this feature <a class="mention" href="https://x.com/ahus1de" rel="noopener noreferrer" target="_blank">@ahus1de</a> is working on in the AsciiDoc plugin for IntelliJ. Game changer.',
    photos: ['<div class="item"><img class="photo" src="media/1235685806334832643.gif"></div>'],
    likes: 25,
    retweets: 7
  },
  {
    id: '1235673036608561152',
    created: 1583442384000,
    type: 'post',
    text: 'And here they are!',
    photos: ['<div class="item"><img class="photo" src="media/1235673036608561152.jpg"></div>', '<div class="item"><img class="photo" src="media/1235673036608561152-2.jpg"></div>'],
    likes: 16,
    retweets: 0
  },
  {
    id: '1235668893797998592',
    created: 1583441396000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MStine" rel="noopener noreferrer" target="_blank">@MStine</a> I also happen to know a guy at Razer (my bro). But I picked it purely on merit.<br><br>In reply to: <a href="#1235667746731339776">1235667746731339776</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1235668609080283137',
    created: 1583441328000,
    type: 'reply',
    text: '@brianleathem <a class="mention" href="https://x.com/shobull" rel="noopener noreferrer" target="_blank">@shobull</a> Thanks! I definitely picked up advocacy chops from him. He could sell a coat to a buffalo.<br><br>In reply to: @brianleathem <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1235667746731339776',
    created: 1583441123000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MStine" rel="noopener noreferrer" target="_blank">@MStine</a> The Razer Blade is more about power. It requires a manual Linux install and a few small to tweaks to get it humming. But the hardware is top of the line. The SSD reads at 3GB/s, the fans almost never have to run, and it powers a huge display flawlessly.<br><br>In reply to: <a href="#1235666873875697664">1235666873875697664</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1235666873875697664',
    created: 1583440914000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MStine" rel="noopener noreferrer" target="_blank">@MStine</a> For the Dell XPS, it\'s the full sized keyboard and the fact that everything is tested and works right out of the box. I\'ve never used a better laptop for Linux. It performs very well.<br><br>In reply to: @mstine <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1235666355770159104',
    created: 1583440791000,
    type: 'post',
    text: 'NASA just named the new Mars rover Perseverance. I couldn\'t think of a more fitting name for this moment in time.<br><br>"We, as humans, will not give up. The human race will always persevere into the future."',
    likes: 2,
    retweets: 1
  },
  {
    id: '1235665095746678784',
    created: 1583440491000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MStine" rel="noopener noreferrer" target="_blank">@MStine</a> I did a bunch of research on this and it came down to the Dell XPS 13 with Ubuntu and the Razer Blade Stealth. I have both and they both work remarkably well. Both have great communities behind them with lots of info.<br><br>In reply to: @mstine <span class="status">&lt;deleted&gt;</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1235647234819014657',
    created: 1583436232000,
    type: 'post',
    text: 'powerful interests = Wall Street, corporate-backed politicians, the status quo, greed, general corruption, etc. In other words, the boot in our face that pushes us down and holds back progress.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1235644398592864256',
    created: 1583435556000,
    type: 'post',
    text: 'I\'m experiencing PTSD watching the progressive movement <a class="mention" href="https://x.com/ewarren" rel="noopener noreferrer" target="_blank">@ewarren</a> &amp; <a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a> both tenaciously fight for slip through our fingers again. Let\'s not let that happen! Don\'t lose sight of why we\'re in this &amp; the powerful interests we\'re up against. Get up and fight for it!!',
    likes: 3,
    retweets: 0
  },
  {
    id: '1235639447850373122',
    created: 1583434376000,
    type: 'quote',
    text: 'Just to put this in context, this tops the 8 million donations from 2 million individuals in 2016.<br><br>Quoting: <a href="https://x.com/BernieSanders/status/1235256190906421249" rel="noopener noreferrer" target="_blank">@BernieSanders</a> <span class="status">1235256190906421249</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1235486306832023552',
    created: 1583397864000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/davidsirota" rel="noopener noreferrer" target="_blank">@davidsirota</a> Having to explain what the establishment is to the establishment is peak 2020.<br><br>In reply to: <a href="https://x.com/davidsirota/status/1235414080560582658" rel="noopener noreferrer" target="_blank">@davidsirota</a> <span class="status">1235414080560582658</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1235480362752438272',
    created: 1583396447000,
    type: 'post',
    text: 'Keep in mind when looking at the "delegate math" in the Democratic race that there are still 144 pledged delegates to be allocated in California. It has a different process than other states that takes longer to play out.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1235474503783739392',
    created: 1583395050000,
    type: 'post',
    text: 'Today I became an uncle again twice in two minutes. Congratulations to my brother <a class="mention" href="https://x.com/shobull" rel="noopener noreferrer" target="_blank">@shobull</a> and his spouse. I can\'t wait to meet the kiddos!',
    likes: 21,
    retweets: 0
  },
  {
    id: '1235402885447520257',
    created: 1583377975000,
    type: 'quote',
    text: 'No doubt it\'s the right thing to do safeguard human life and health. I\'m still really going to miss the opportunity to see everyone who was coming to town. 😪<br><br>Quoting: <a href="https://x.com/devdotnext/status/1235358946312978434" rel="noopener noreferrer" target="_blank">@devdotnext</a> <span class="status">1235358946312978434</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1235401833625063424',
    created: 1583377724000,
    type: 'post',
    text: 'The American people are far more united than the media would like us to believe. They count on us fighting with each other. Please don\'t forget that.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1235401264407760896',
    created: 1583377588000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/edburns" rel="noopener noreferrer" target="_blank">@edburns</a> <a class="mention" href="https://x.com/fairvote" rel="noopener noreferrer" target="_blank">@fairvote</a> Thanks for the reference. I\'m very encouraged that the Colorado SoS just tweeted this weekend about a commission to explore ranked choice voting in the state. This can happen!<br><br>In reply to: <a href="https://x.com/edburns/status/1235400421704859648" rel="noopener noreferrer" target="_blank">@edburns</a> <span class="status">1235400421704859648</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1235397355215568896',
    created: 1583376656000,
    type: 'post',
    text: 'I hope progressives, especially young progressives, realize we\'re being burned by lack of ranked choice voting.',
    likes: 5,
    retweets: 0
  },
  {
    id: '1235370835730300929',
    created: 1583370333000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GeoffMiami" rel="noopener noreferrer" target="_blank">@GeoffMiami</a> 😢 I\'m very sorry to hear about your loss. You aren\'t kidding about those eyes. They\'re a reflection of your beautiful soul.<br><br>In reply to: <a href="https://x.com/GeoffMiami/status/1235368082232705024" rel="noopener noreferrer" target="_blank">@GeoffMiami</a> <span class="status">1235368082232705024</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1235297180325498881',
    created: 1583352773000,
    type: 'reply',
    text: '@mwessendorf It\'s not about age. It\'s not about gender. It\'s about agenda. It\'s about how his policies will affect communities. He has stood for rights, justice, and equality his entire life. And he\'s going all out to make this country work for all people, and for future generations.<br><br>In reply to: <a href="#1235290874533289986">1235290874533289986</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1235293987323482121',
    created: 1583352011000,
    type: 'post',
    text: 'Here\'s where we are. The progressive candidates can either consolidate to have a shot at winning this primary, or hang up the agenda and tell the working class to wait another 4 years. I, for one, am tired of waiting.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1235292415105421317',
    created: 1583351637000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> All the freakin\' time.<br><br>In reply to: <a href="https://x.com/headius/status/1235284181942521858" rel="noopener noreferrer" target="_blank">@headius</a> <span class="status">1235284181942521858</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1235291690551914497',
    created: 1583351464000,
    type: 'post',
    text: 'Thanks to my friend <a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a>. He really made my day.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1235290874533289986',
    created: 1583351269000,
    type: 'reply',
    text: '@mwessendorf He has the overwhelming support of young people because he cares first and foremost about their future. To just see him as an old guy is to not understand him at all. He fights for future generations of all races and identies as hard as any person can.<br><br>In reply to: @mwessendorf <span class="status">&lt;deleted&gt;</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1235147673029623809',
    created: 1583317127000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/heapstack" rel="noopener noreferrer" target="_blank">@heapstack</a> <a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a> <a class="mention" href="https://x.com/biden" rel="noopener noreferrer" target="_blank">@biden</a> <a class="mention" href="https://x.com/ewarren" rel="noopener noreferrer" target="_blank">@ewarren</a> My thoughts exactly.<br><br>In reply to: <a href="https://x.com/heapstack/status/1235147072044773377" rel="noopener noreferrer" target="_blank">@heapstack</a> <span class="status">1235147072044773377</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1235136710264025089',
    created: 1583314514000,
    type: 'post',
    text: 'Bernie is committed to banning fracking, which is why I say in a Democratic primary that this is such an important win for the natural world.',
    likes: 6,
    retweets: 1
  },
  {
    id: '1235136261951606787',
    created: 1583314407000,
    type: 'post',
    text: 'I see you Utah. Thanks to fellow Americans for voting to put Bernie on top there too. Colorado, Utah, and California are home to some of the most treasured parks in this country, many of which are under threat. So these wins are wins for the natural world. #Bernie2020',
    likes: 7,
    retweets: 1,
    tags: ['bernie2020']
  },
  {
    id: '1235135211878445056',
    created: 1583314156000,
    type: 'post',
    text: 'And for all my fellow Americans in California who voted to put Bernie and the progressive movement on top, I thank you. I want more than a respectable country. I want a country we can be proud of because of how it treats its citizens, its allies, and the environment. #Bernie2020',
    likes: 1,
    retweets: 0,
    tags: ['bernie2020']
  },
  {
    id: '1235128930249531392',
    created: 1583312659000,
    type: 'quote',
    text: 'Couldn\'t have said it better myself. I\'ll add it\'s also because of the multi-racial, multi-generational, multi-gender, people-powered movement behind him.<br><br>Quoting: <a href="https://x.com/marclamonthill/status/1234841573277491203" rel="noopener noreferrer" target="_blank">@marclamonthill</a> <span class="status">1234841573277491203</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1235127661501939712',
    created: 1583312356000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> <a class="mention" href="https://x.com/antonarhipov" rel="noopener noreferrer" target="_blank">@antonarhipov</a> <a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> Troll level:<br><br>In reply to: <a href="https://x.com/majson/status/1235124737640194048" rel="noopener noreferrer" target="_blank">@majson</a> <span class="status">1235124737640194048</span>',
    photos: ['<div class="item"><img class="photo" src="media/1235127661501939712.gif"></div>'],
    likes: 3,
    retweets: 0
  },
  {
    id: '1235126837438599168',
    created: 1583312160000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> It means so much to me that you used the word "trust" because that\'s exactly what I hope to earn from everyone in this great community. Projects come and go, but the trust we build with each other is forever.<br><br>In reply to: <a href="https://x.com/starbuxman/status/1235103406802403329" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">1235103406802403329</span>',
    likes: 8,
    retweets: 0
  },
  {
    id: '1235112827699904513',
    created: 1583308820000,
    type: 'post',
    text: 'Thank you to all my fellow Americans in Colorado who voted to put Bernie on top. I knew together we\'d send a clear message to Washington that we want a progressive agenda now. #Bernie2020',
    likes: 2,
    retweets: 0,
    tags: ['bernie2020']
  },
  {
    id: '1234961929761722369',
    created: 1583272843000,
    type: 'post',
    text: 'Vote like someone\'s life depends on it, because it does. #Bernie2020 #NotMeUs',
    likes: 1,
    retweets: 0,
    tags: ['bernie2020', 'notmeus']
  },
  {
    id: '1234757290764320768',
    created: 1583224053000,
    type: 'post',
    text: 'I\'ve visited a lot of state election sites, and I must say the Utah site is among the best. It\'s elegant, intuitive, and accessible. 👏 <a href="https://vote.utah.gov/" rel="noopener noreferrer" target="_blank">vote.utah.gov/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1234659281326112770',
    created: 1583200686000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/johnmark" rel="noopener noreferrer" target="_blank">@johnmark</a> You and me both.<br><br>In reply to: <a href="https://x.com/johnmark/status/1234651794250379267" rel="noopener noreferrer" target="_blank">@johnmark</a> <span class="status">1234651794250379267</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1234619579386589184',
    created: 1583191220000,
    type: 'post',
    text: 'In every conversation I have about the 2020 election, I raise the urgency for bold action to curb climate change and protect the environment. Regardless of who gets elected, we\'ll still be faced with this crisis.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1234540233334697985',
    created: 1583172302000,
    type: 'reply',
    text: '@confusedophan Oh, I had no idea this was a way to show support. In that case, I\'ll do it right away. Thanks for the heads up!<br><br>In reply to: @confusedophan <span class="status">&lt;deleted&gt;</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1234284233289453569',
    created: 1583111267000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JenaGriswold" rel="noopener noreferrer" target="_blank">@JenaGriswold</a> <a class="mention" href="https://x.com/COSecofState" rel="noopener noreferrer" target="_blank">@COSecofState</a> Thank you! YES!!!<br><br>In reply to: <a href="https://x.com/JenaGriswold/status/1234281899901632513" rel="noopener noreferrer" target="_blank">@JenaGriswold</a> <span class="status">1234281899901632513</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1234063545311977473',
    created: 1583058651000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ChuckRocha" rel="noopener noreferrer" target="_blank">@ChuckRocha</a> You, sir, are a very special human. Thanks for pep talk and for all the hard work you do. Now, let\'s go get it!<br><br>In reply to: <a href="https://x.com/ChuckRocha/status/1233944145217179650" rel="noopener noreferrer" target="_blank">@ChuckRocha</a> <span class="status">1233944145217179650</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1233850509771055104',
    created: 1583007860000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/KonaTech140" rel="noopener noreferrer" target="_blank">@KonaTech140</a> ☝️ I think you might be able to guess who that is.<br><br>In reply to: <a href="#1233847537221668864">1233847537221668864</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1233847537221668864',
    created: 1583007151000,
    type: 'post',
    text: 'Best text of the day: "I\'m a millennial who grows cannabis. I\'m definitely voting for Bernie. lol"',
    likes: 2,
    retweets: 0
  },
  {
    id: '1233701193307656194',
    created: 1582972260000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jaketapper" rel="noopener noreferrer" target="_blank">@jaketapper</a> <a class="mention" href="https://x.com/tedneward" rel="noopener noreferrer" target="_blank">@tedneward</a> <a class="mention" href="https://x.com/SenSanders" rel="noopener noreferrer" target="_blank">@SenSanders</a> <a class="mention" href="https://x.com/HillaryClinton" rel="noopener noreferrer" target="_blank">@HillaryClinton</a> The only way to really settle this is to honor the vote outcome from the public elections. I don\'t even know why this is a debate in the US.<br><br>In reply to: <a href="#1233699625506201600">1233699625506201600</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1233699625506201600',
    created: 1582971886000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jaketapper" rel="noopener noreferrer" target="_blank">@jaketapper</a> <a class="mention" href="https://x.com/tedneward" rel="noopener noreferrer" target="_blank">@tedneward</a> <a class="mention" href="https://x.com/SenSanders" rel="noopener noreferrer" target="_blank">@SenSanders</a> <a class="mention" href="https://x.com/HillaryClinton" rel="noopener noreferrer" target="_blank">@HillaryClinton</a> BS. It\'s because the superdelegates had tainted the entire election process in 2016 by counting their own votes before the first public vote was cast, then declaring they wouldn\'t even support Sanders in states he won. It\'s deceitful to compare that situation to 2020.<br><br>In reply to: <a href="https://x.com/jaketapper/status/1233498782156951552" rel="noopener noreferrer" target="_blank">@jaketapper</a> <span class="status">1233498782156951552</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1233637553086976001',
    created: 1582957087000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/emily_isaac_" rel="noopener noreferrer" target="_blank">@emily_isaac_</a> I\'m loving the BERN app. After using it for awhile, I have a suggestion. It would be helpful if the person detail screen showed the registration deadline and primary date. And maybe even a link to the elections page on the SOS site for their state.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1233595663100727297',
    created: 1582947099000,
    type: 'post',
    text: 'This is the message that makes my day.<br><br>"I\'m voting for Bernie."<br><br>🔥😍🔥',
    likes: 1,
    retweets: 0
  },
  {
    id: '1233595015663734785',
    created: 1582946945000,
    type: 'post',
    text: 'I\'m not going to lie, it\'s disheartening to know that I have family members who are still Republican and Trump supporters. But there are more who are not, and that reinvigorates me again. I fight for justice for all of them.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1233594601878904832',
    created: 1582946846000,
    type: 'post',
    text: 'I\'m kind of liking that volunteering for Bernie using the BERN app is motivating me to reconnect with friends and family I haven\'t talked to in a while. But that\'s what this is all about, having real conversations about our collective future. People powered.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1233549570870636544',
    created: 1582936110000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ixchelruiz" rel="noopener noreferrer" target="_blank">@ixchelruiz</a> Whoa!<br><br>In reply to: <a href="https://x.com/ixchelruiz/status/1233535758335528961" rel="noopener noreferrer" target="_blank">@ixchelruiz</a> <span class="status">1233535758335528961</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1233533648567066624',
    created: 1582932314000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mairin" rel="noopener noreferrer" target="_blank">@mairin</a> <a class="mention" href="https://x.com/stickster" rel="noopener noreferrer" target="_blank">@stickster</a> I\'ll tell you it really is growing on me. These are the best iterations I\'ve seen so far. 👍<br><br>In reply to: <a href="#1233533116196651008">1233533116196651008</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1233533116196651008',
    created: 1582932187000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mairin" rel="noopener noreferrer" target="_blank">@mairin</a> <a class="mention" href="https://x.com/stickster" rel="noopener noreferrer" target="_blank">@stickster</a> Rather than condensed, I think I meant cramped. I\'m out of my element a bit here, but my gut tells me it\'s the proximity of the stems that\'s giving me that sense, not the scaling. As if the top of the f is overreaching.<br><br>In reply to: <a href="https://x.com/mairin/status/1233532241646673920" rel="noopener noreferrer" target="_blank">@mairin</a> <span class="status">1233532241646673920</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1233531515390197760',
    created: 1582931805000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/stickster" rel="noopener noreferrer" target="_blank">@stickster</a> <a class="mention" href="https://x.com/mairin" rel="noopener noreferrer" target="_blank">@mairin</a> It could be that the curve at the top of the f comes down slightly too far (making it almost touch the cross bar). Perhaps if it came down just a touch less, it would balance.<br><br>In reply to: <a href="#1233530777901469697">1233530777901469697</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1233530777901469697',
    created: 1582931630000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/stickster" rel="noopener noreferrer" target="_blank">@stickster</a> <a class="mention" href="https://x.com/mairin" rel="noopener noreferrer" target="_blank">@mairin</a> It\'s definitely growing on me, though I share Paul\'s thoughts. The right seems a bit...condensed.<br><br>In reply to: <a href="https://x.com/stickster/status/1232725275022155776" rel="noopener noreferrer" target="_blank">@stickster</a> <span class="status">1232725275022155776</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1233529838088015872',
    created: 1582931405000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/indianatwork" rel="noopener noreferrer" target="_blank">@indianatwork</a> Sure, but it\'s the probability that 2 people have the same birthday and the birthday = Feb 29 and you know the other person and have talked about the subject. So the odds are still pretty great. All that aside, it\'s more bizarre there\'s a day that only exists every 4 years.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1233524788691423232" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1233524788691423232</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1233517253020139520',
    created: 1582928405000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/indianatwork" rel="noopener noreferrer" target="_blank">@indianatwork</a> That\'s so crazy. What enormous odds ;)<br><br>In reply to: <a href="https://x.com/indianatwork/status/1233509089285672966" rel="noopener noreferrer" target="_blank">@indianatwork</a> <span class="status">1233509089285672966</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1233504683248865280',
    created: 1582925408000,
    type: 'post',
    text: 'One of my cousins was born on a date that only happens once every four years. Tomorrow is that day. 🎉🎂',
    likes: 3,
    retweets: 0
  },
  {
    id: '1233488269201092609',
    created: 1582921495000,
    type: 'post',
    text: 'Friends who vote for Bernie get extra hugs from me. And, if it\'s your thing, 🍻 too. That\'s a promise. A better world is possible.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1233453423149760512',
    created: 1582913187000,
    type: 'post',
    text: 'It feels really strange when I\'m trying to encourage family members to vote in order to make a better future for themselves and their children, and some just can\'t be bothered.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1233148898291531776',
    created: 1582840582000,
    type: 'quote',
    text: 'For my followers in and around Boston, one last chance to hear from Bernie and the movement leaders before you decide who you\'ll vote for on Super Tuesday.<br><br>Quoting: <a href="https://x.com/VarshPrakash/status/1233131877697032192" rel="noopener noreferrer" target="_blank">@VarshPrakash</a> <span class="status">1233131877697032192</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232958419771875328',
    created: 1582795169000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> @ToddMancini <a class="mention" href="https://x.com/code" rel="noopener noreferrer" target="_blank">@code</a> It\'s built in to core. You just set the stem attribute on the document and it loads MathJax.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1232951968823435264" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1232951968823435264</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232951364033994753',
    created: 1582793486000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> @ToddMancini <a class="mention" href="https://x.com/code" rel="noopener noreferrer" target="_blank">@code</a> asciidoctor-latex is something completely different. That\'s a converter that produces LaTeX. It also has some unsanctioned language enhancements which aren\'t supported anywhere else in the ecosystem.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1232944770424672258" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1232944770424672258</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232943802236596224',
    created: 1582791684000,
    type: 'reply',
    text: '@ToddMancini <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/code" rel="noopener noreferrer" target="_blank">@code</a> Yes, the preview pane should show the LaTeX (or AsciiMath) equations using MathJax. If something is missing, the maintainer could port the feature over from similar tools in the ecosystem (the browser live preview or Atom live preview).<br><br>In reply to: @ToddMancini <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232934102023598082',
    created: 1582789371000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ellispratt" rel="noopener noreferrer" target="_blank">@ellispratt</a> That\'s one of the cool things about Bernie\'s plan. It\'s actually one of the most comprehensive in all the world in that it includes everything, head to toe. (Granted, it\'s just a plan at this point, but I like the vision).<br><br>In reply to: <a href="https://x.com/ellispratt/status/1232930159440867329" rel="noopener noreferrer" target="_blank">@ellispratt</a> <span class="status">1232930159440867329</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232927190938861569',
    created: 1582787723000,
    type: 'post',
    text: 'OH: We\'re digital inches away.<br><br>🤔 That\'s a new one.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232922244927479809',
    created: 1582786544000,
    type: 'reply',
    text: '@SeaSparks21 I bet it will do wonders! As my physical therapist said, the body wants to move. So if you can give it more range of motion, everything starts to improve.<br><br>In reply to: @SeaSparks21 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232911861386842112',
    created: 1582784068000,
    type: 'reply',
    text: '@SeaSparks21 Good luck with the surgery! As my friend from Norway reminds me, it\'s in the best interest of the country to get you back on your feet. A healthy citizenry is cheaper and more productive. (On top of being morally right). So why would they oppose it?<br><br>In reply to: @SeaSparks21 <span class="status">&lt;deleted&gt;</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1232910768548401152',
    created: 1582783808000,
    type: 'reply',
    text: '@SeaSparks21 Yep. Did you see the John Oliver clip about Medicare for All? He brought up that point about bankruptcies. It\'s outrageous.<br><br>In reply to: @SeaSparks21 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232879409507360768',
    created: 1582776331000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/indianatwork" rel="noopener noreferrer" target="_blank">@indianatwork</a> I believe it.<br><br>In reply to: <a href="https://x.com/indianatwork/status/1232878780496084993" rel="noopener noreferrer" target="_blank">@indianatwork</a> <span class="status">1232878780496084993</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232876873199181824',
    created: 1582775726000,
    type: 'reply',
    text: '@SeaSparks21 Like in Europe, a tip should be, "that service was so good, I\'d like to give a bonus to your income, knowing you\'re already compensated fairly for the labor you did."<br><br>In reply to: <a href="#1232876371472306179">1232876371472306179</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232876371472306179',
    created: 1582775607000,
    type: 'reply',
    text: '@SeaSparks21 I\'ve also often thought it\'s strange that tips correlate to what I eat as a customer. The cost of the dish is not correlated to the quality of service. It\'s a personal want. So if I want a less expensive dish, the tip suffers. We need a more sensible and fair situation.<br><br>In reply to: <a href="#1232875774471892996">1232875774471892996</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232875774471892996',
    created: 1582775465000,
    type: 'reply',
    text: '@SeaSparks21 We also need to end the myth that tips are correlated with quality of service. The person tipping is often struggling too (perhaps even gets income the same way), so tips are closer associated with wealth (and often arbitrary even then).<br><br>In reply to: @SeaSparks21 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232875046101647360',
    created: 1582775291000,
    type: 'reply',
    text: '@SeaSparks21 My spouse had to get jaw surgery to fix a misalignment that was causing the jaw to dissolve. It was classified as dental. Cost us 20k just for the surgery and prep. Nearly made us bankrupt. We pulled through only thanks to a family member who helped us.<br><br>In reply to: @SeaSparks21 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232865645588443136',
    created: 1582773050000,
    type: 'reply',
    text: '@jbryant787 Haven\'t seen it, but now I\'ve got it in my queue. Thanks for the tip!<br><br>In reply to: @jbryant787 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232865165705564161',
    created: 1582772935000,
    type: 'post',
    text: 'I should add that I do not have dental insurance. I\'m fortunate enough to be able to pay for it out of pocket. For those who can\'t, their health can and will suffer, and that\'s unacceptable. The mouth is part of the body and thus part of heath care.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1232849466509516800',
    created: 1582769192000,
    type: 'post',
    text: 'Got my teeth cleaned this morning and talked to my dental hygienist about voting for Bernie so we all get dental care as part of comprehensive health care for all citizens. She said wasn\'t going to vote, but now she is. #NotMeUs',
    likes: 0,
    retweets: 0,
    tags: ['notmeus']
  },
  {
    id: '1232847209080557568',
    created: 1582768654000,
    type: 'post',
    text: 'The first woman appointed to the US cabinet and the longest serving Secretary of Labor.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1232846798588235777',
    created: 1582768556000,
    type: 'quote',
    text: 'Most people know about President FDR, but do you know the woman, Frances Perkins, who created his groundbreaking New Deal?<br><br>I sure wish this book had been around and part of my early education.<br><br>Quoting: <a href="https://x.com/jennifercord/status/1232836514381430786" rel="noopener noreferrer" target="_blank">@jennifercord</a> <span class="status">1232836514381430786</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1232766468137209856',
    created: 1582749404000,
    type: 'quote',
    text: 'We fought a lot over the single "blessed" reference implementation mandate in the JCP, so I\'m really glad to see that has been eliminated in the Eclipse Foundation spec process.<br><br>Quoting: <a href="https://x.com/waynebeaton/status/1232763670456930304" rel="noopener noreferrer" target="_blank">@waynebeaton</a> <span class="status">1232763670456930304</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1232753664667746304',
    created: 1582746351000,
    type: 'post',
    text: 'This is the biggest story being overlooked in this election. Diversity is woven into the senior level of the Bernie campaign and they\'re spending large amounts of TIME having real conversations with disenfranchised voters. <a href="https://youtu.be/lfcvHOtVQ1k" rel="noopener noreferrer" target="_blank">youtu.be/lfcvHOtVQ1k</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232625095656529920',
    created: 1582715698000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sten_aksel" rel="noopener noreferrer" target="_blank">@sten_aksel</a> Bespoke is exactly as it suggests, for hand crafted presentations (meaning it requires plenty of time &amp; effort). It you want something more out of the box, use the reveal.js converter.<br><br>In reply to: <a href="https://x.com/sten_aksel/status/1232619229393891328" rel="noopener noreferrer" target="_blank">@sten_aksel</a> <span class="status">1232619229393891328</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232454397998665729',
    created: 1582675001000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/johnmark" rel="noopener noreferrer" target="_blank">@johnmark</a> ...can\'t touch this!<br><br>In reply to: <a href="https://x.com/johnmark/status/1232450267750158336" rel="noopener noreferrer" target="_blank">@johnmark</a> <span class="status">1232450267750158336</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232442669214752768',
    created: 1582672204000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/johnmark" rel="noopener noreferrer" target="_blank">@johnmark</a> You nailed it!<br><br>In reply to: <a href="https://x.com/johnmark/status/1232438684802138112" rel="noopener noreferrer" target="_blank">@johnmark</a> <span class="status">1232438684802138112</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232222727538241536',
    created: 1582619766000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ninaturner" rel="noopener noreferrer" target="_blank">@ninaturner</a> <a class="mention" href="https://x.com/rosaclemente" rel="noopener noreferrer" target="_blank">@rosaclemente</a> <a class="mention" href="https://x.com/ava" rel="noopener noreferrer" target="_blank">@ava</a> <a class="mention" href="https://x.com/SusanSarandon" rel="noopener noreferrer" target="_blank">@SusanSarandon</a> <a class="mention" href="https://x.com/briebriejoy" rel="noopener noreferrer" target="_blank">@briebriejoy</a> The thing we must remember is that only love can defeat hate. What hurts any one of us hurts all of us. I think our strategy should be to show what that love looks like. As you say, tough on issues, soft on people. I subscribe to that. Rise above.<br><br>In reply to: <a href="https://x.com/ninaturner/status/1232167676778307585" rel="noopener noreferrer" target="_blank">@ninaturner</a> <span class="status">1232167676778307585</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232217496561733633',
    created: 1582618519000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ohlol" rel="noopener noreferrer" target="_blank">@ohlol</a> <a class="mention" href="https://x.com/jezhumble" rel="noopener noreferrer" target="_blank">@jezhumble</a> I don\'t envy anyone trying to run a campaign in this environment. What I can tell you is that solidarity is what brought me here and I believe deep down that\'s what this movement is about. I\'ll do my best and also encourage fellow supporters to do the same.<br><br>In reply to: @ohlol <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232216040123224064',
    created: 1582618172000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ohlol" rel="noopener noreferrer" target="_blank">@ohlol</a> <a class="mention" href="https://x.com/jezhumble" rel="noopener noreferrer" target="_blank">@jezhumble</a> I\'m 100% willing to call out those things when I see them. I don\'t want dismiss any real concerns, but there have been instances I\'ve seen when the comment was meant to be satire. That\'s where this platform breaks down. Even I have a hard time telling the difference sometimes.<br><br>In reply to: @ohlol <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232214278679089153',
    created: 1582617752000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ohlol" rel="noopener noreferrer" target="_blank">@ohlol</a> <a class="mention" href="https://x.com/jezhumble" rel="noopener noreferrer" target="_blank">@jezhumble</a> If the criticism is that Bernie needs to use clearer language, then I\'m happy to call on him to do that. Perhaps we can suggest language he should use if what he says isn\'t easily understood.<br><br>In reply to: <a href="#1232213978094268416">1232213978094268416</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232213978094268416',
    created: 1582617680000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ohlol" rel="noopener noreferrer" target="_blank">@ohlol</a> <a class="mention" href="https://x.com/jezhumble" rel="noopener noreferrer" target="_blank">@jezhumble</a> No, I don\'t think it\'s credible. Because the main intent itself to be a smear. We can start with Bernie and other surrogates stating that it\'s not acceptable. And then we can have a conversation about whether they can do or say more. To say they\'re fine with it is simple a lie.<br><br>In reply to: @ohlol <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232213148465168384',
    created: 1582617482000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ohlol" rel="noopener noreferrer" target="_blank">@ohlol</a> <a class="mention" href="https://x.com/jezhumble" rel="noopener noreferrer" target="_blank">@jezhumble</a> If it was "warren bro" I would condemn that too.<br><br>In reply to: <a href="#1232212993011617793">1232212993011617793</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232212993011617793',
    created: 1582617445000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ohlol" rel="noopener noreferrer" target="_blank">@ohlol</a> <a class="mention" href="https://x.com/jezhumble" rel="noopener noreferrer" target="_blank">@jezhumble</a> The central point of the article that Gray is being toxic by saying "bernie bro" is racial erasure is ridiculous. We need to recognize that slurs like that are hurtful and shouldn\'t be used, no matter which campaign it impacts. It\'s wrong.<br><br>In reply to: <a href="#1232212098043670531">1232212098043670531</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232212098043670531',
    created: 1582617232000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ohlol" rel="noopener noreferrer" target="_blank">@ohlol</a> <a class="mention" href="https://x.com/jezhumble" rel="noopener noreferrer" target="_blank">@jezhumble</a> Yes, the one from the independent (the daily beast is not credible). It\'s disappointingly one-sided and parroting corp media smears. Online communication is getting increasingly toxic, no doubt. But to say Bernie\'s campaign is encouraging or driving this behavior is ludicrous.<br><br>In reply to: @ohlol <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232207160727130112',
    created: 1582616055000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jezhumble" rel="noopener noreferrer" target="_blank">@jezhumble</a> For what it\'s worth, I agree we should be critical. When I listen to Bernie, that\'s what I hear him encouraging us to do. Challenge him. Push him. Build support from the bottom up. And make noise about it. He says it\'s not about him. And I believe him. So, to reiterate, I agree.<br><br>In reply to: <a href="https://x.com/jezhumble/status/1231615105307013121" rel="noopener noreferrer" target="_blank">@jezhumble</a> <span class="status">1231615105307013121</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232203847113723904',
    created: 1582615265000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jezhumble" rel="noopener noreferrer" target="_blank">@jezhumble</a> I\'ve gotten lambasted from someone siding with every campaign so far. (And I\'m not innocent as I\'ve made some bad calls). I\'m definitely not suggesting it doesn\'t exist. I\'m just not convinced it\'s coming from any one place in particular. And that should concern us even more.<br><br>In reply to: <a href="#1232202998731771905">1232202998731771905</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232202998731771905',
    created: 1582615062000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jezhumble" rel="noopener noreferrer" target="_blank">@jezhumble</a> Every Bernie supporter I\'ve met in person has the biggest heart as anyone I\'ve met. So if there\'s a problem, it\'s online (and maybe not real people). I wonder how much is the failing of social media / tech to leave room for empathy. I\'m observing tons of harsh language all over.<br><br>In reply to: <a href="#1232201766042656768">1232201766042656768</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1232201766042656768',
    created: 1582614768000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jezhumble" rel="noopener noreferrer" target="_blank">@jezhumble</a> I\'m a white male and Bernie supporter and I condemn any toxic behavior. This movement is about solidarity and that\'s the path I encourage everyone in it to follow. We\'re human and emotions happen. However, we should heed Nina\'s advice: be tough on issues, but soft on people.<br><br>In reply to: <a href="https://x.com/jezhumble/status/1232192712352268289" rel="noopener noreferrer" target="_blank">@jezhumble</a> <span class="status">1232192712352268289</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232195643130540033',
    created: 1582613309000,
    type: 'post',
    text: 'Tired: "Hello, World!"<br>Wired: "Hello somebody!"<br><br>wdyt <a class="mention" href="https://x.com/michaelsayman" rel="noopener noreferrer" target="_blank">@michaelsayman</a>?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232190589895995392',
    created: 1582612104000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nicokosi" rel="noopener noreferrer" target="_blank">@nicokosi</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> <a class="mention" href="https://x.com/jchristophegay" rel="noopener noreferrer" target="_blank">@jchristophegay</a> Don\'t forget to join us in the chat channel at <a href="http://gitter.im/antora/users" rel="noopener noreferrer" target="_blank">gitter.im/antora/users</a>. We have such a helpful and enthusiastic user community.<br><br>In reply to: <a href="https://x.com/nicokosi/status/1232178327118602241" rel="noopener noreferrer" target="_blank">@nicokosi</a> <span class="status">1232178327118602241</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1232190226522464257',
    created: 1582612017000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/melissajmckay" rel="noopener noreferrer" target="_blank">@melissajmckay</a> <a class="mention" href="https://x.com/jfrog" rel="noopener noreferrer" target="_blank">@jfrog</a> Cool! Congratulations! <a class="mention" href="https://x.com/jfrog" rel="noopener noreferrer" target="_blank">@jfrog</a> is lucky. I look forward to learning about all the great things they\'re doing from you.<br><br>In reply to: <a href="https://x.com/melissajmckay/status/1232185598502260736" rel="noopener noreferrer" target="_blank">@melissajmckay</a> <span class="status">1232185598502260736</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1232151538442760192',
    created: 1582602793000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/connolly_s" rel="noopener noreferrer" target="_blank">@connolly_s</a> The attributes are captured into the model. The default HTML converter doesn\'t emit them, though a custom / extended converter could.<br><br>In reply to: <a href="https://x.com/connolly_s/status/1232111261669826560" rel="noopener noreferrer" target="_blank">@connolly_s</a> <span class="status">1232111261669826560</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1232058223022108672',
    created: 1582580545000,
    type: 'post',
    text: 'With the advent of "then" (aka yield_self), I\'m finding Ruby to be quite conducive for functional programming (esp. when applied to scripts). <a href="https://zverok.github.io/blog/2018-03-23-yield_self2.html" rel="noopener noreferrer" target="_blank">zverok.github.io/blog/2018-03-23-yield_self2.html</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1231996396674277381',
    created: 1582565805000,
    type: 'post',
    text: 'I was so excited I botched that tweet. The person who replied to tell me I have the wrong number also supports Bernie.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1231982668826333187',
    created: 1582562532000,
    type: 'post',
    text: 'I sent a text message asking a friend if he supports Bernie. Only it\'s no longer my friend\'s number. But here\'s the kicker. The person who replied to tell me that supports Bernie. Hello somebody! #NotMeUs',
    likes: 4,
    retweets: 0,
    tags: ['notmeus']
  },
  {
    id: '1231695965142142976',
    created: 1582494176000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/johnmark" rel="noopener noreferrer" target="_blank">@johnmark</a> Peace, love, and respect to you, my friend.<br><br>In reply to: <a href="https://x.com/johnmark/status/1231692281410916354" rel="noopener noreferrer" target="_blank">@johnmark</a> <span class="status">1231692281410916354</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1231688619137499136',
    created: 1582492425000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/johnmark" rel="noopener noreferrer" target="_blank">@johnmark</a> And if anything I said feels like an attack, I am sorry for that too. I\'m only trying to clarify what we believe this is about. And it ain\'t just about Bernie.<br><br>In reply to: <a href="#1231687867866333184">1231687867866333184</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1231687867866333184',
    created: 1582492246000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/johnmark" rel="noopener noreferrer" target="_blank">@johnmark</a> I am sorry to hear that it triggers your PTSD. I wish that didn\'t have to happen.<br><br>In reply to: <a href="https://x.com/johnmark/status/1231685578523017216" rel="noopener noreferrer" target="_blank">@johnmark</a> <span class="status">1231685578523017216</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1231687440949112832',
    created: 1582492144000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/johnmark" rel="noopener noreferrer" target="_blank">@johnmark</a> I often say that if he wins, it\'s not over. It\'s just the start. We still have to protest. We still have to complain (even to and about Bernie). We still have to fight...until every last one of us is free and treated equally, etc<br><br>In reply to: <a href="#1231686904283746304">1231686904283746304</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1231686904283746304',
    created: 1582492016000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/johnmark" rel="noopener noreferrer" target="_blank">@johnmark</a> I absolutely hear what you\'re saying about the savior thing. I think about it a lot. But when you\'re at a rally, you realize quickly that the energy is not for a savior. It\'s for how the policies affect those communities. People are being seen &amp; heard. The power is us.<br><br>In reply to: <a href="https://x.com/johnmark/status/1231685578523017216" rel="noopener noreferrer" target="_blank">@johnmark</a> <span class="status">1231685578523017216</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1231685333525262336',
    created: 1582491641000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/johnmark" rel="noopener noreferrer" target="_blank">@johnmark</a> If you don\'t connect with that, fine. Just don\'t paint it as being something it isn\'t. That\'s all I\'m saying.<br><br>In reply to: <a href="#1231684632967409664">1231684632967409664</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1231684632967409664',
    created: 1582491474000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/johnmark" rel="noopener noreferrer" target="_blank">@johnmark</a> And I do take issue with trying to change it\'s meaning because it erases the tremendously hard work of the movement builders, often minorities, that underpin this campaign. They fight for people who, alone, cannot fight for themselves.<br><br>In reply to: <a href="#1231683309488394240">1231683309488394240</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1231683309488394240',
    created: 1582491159000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/johnmark" rel="noopener noreferrer" target="_blank">@johnmark</a> You\'re alluding to something that simply isn\'t factual. The slogan was not introduced by Bernie (so it\'s not about him). It came from a people\'s movement he inspired in 2016. It has nothing to do with criticism. It\'s about movements working together.<br><br>In reply to: <a href="https://x.com/johnmark/status/1231679531574026243" rel="noopener noreferrer" target="_blank">@johnmark</a> <span class="status">1231679531574026243</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1231670140569174016',
    created: 1582488019000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/johnmark" rel="noopener noreferrer" target="_blank">@johnmark</a> The idea of the slogan is that you and I are supposed to say it to ourselves. It\'s about fighting for someone else, perhaps even someone you don\'t know, instead of being selfish. When you hurt, I hurt. None of us are free until all of us are free. etc. Get it?<br><br>In reply to: <a href="#1231669306926088193">1231669306926088193</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1231669306926088193',
    created: 1582487820000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/johnmark" rel="noopener noreferrer" target="_blank">@johnmark</a> It\'s so selfish to put other people first, especially those that have less or who have been denied.<br><br>In reply to: <a href="https://x.com/johnmark/status/1231667625010323456" rel="noopener noreferrer" target="_blank">@johnmark</a> <span class="status">1231667625010323456</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1231519001852858370',
    created: 1582451985000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tedneward" rel="noopener noreferrer" target="_blank">@tedneward</a> One thing you said I\'ll agree with. He is the antithesis of Trump. And that\'s a good thing. We don\'t want to be like Trump at all. But the Democrats have all said as much.<br><br>In reply to: <a href="#1231517431857729536">1231517431857729536</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1231517431857729536',
    created: 1582451610000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tedneward" rel="noopener noreferrer" target="_blank">@tedneward</a> He\'s pulling people in from all walks of life. That\'s the opposite of alienating. And that\'s why he received nearly 50% of vote in Nevada. Maybe go to a rally? You\'ll find love and compassion abound. It\'s the media that\'s painting him in the image you describe.<br><br>In reply to: <a href="https://x.com/tedneward/status/1231514546675712000" rel="noopener noreferrer" target="_blank">@tedneward</a> <span class="status">1231514546675712000</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1231497975429353473',
    created: 1582446972000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tedneward" rel="noopener noreferrer" target="_blank">@tedneward</a> If you don\'t support Bernie, that does not, by itself, make you part of the Democratic establishment. There\'s still oceans of room for broad coalitions among the people.<br><br>In reply to: <a href="#1231496907232440321">1231496907232440321</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1231496907232440321',
    created: 1582446717000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tedneward" rel="noopener noreferrer" target="_blank">@tedneward</a> Democratic establishment = Big money, special interests<br><br>In other words, not 99% of the voters.<br><br>In reply to: <a href="#1231495405814157313">1231495405814157313</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1231495405814157313',
    created: 1582446359000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tedneward" rel="noopener noreferrer" target="_blank">@tedneward</a> It\'s about restructuring the Democratic Party to focus on the workers (and those who cannot work) of every race, gender, identity, religion, and ability instead of the interests of big money. He has chosen a side. Those who support him know what he means.<br><br>In reply to: <a href="https://x.com/tedneward/status/1231480699334483974" rel="noopener noreferrer" target="_blank">@tedneward</a> <span class="status">1231480699334483974</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1231460215473627136',
    created: 1582437969000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GeoffMiami" rel="noopener noreferrer" target="_blank">@GeoffMiami</a> <a class="mention" href="https://x.com/rebeinstein" rel="noopener noreferrer" target="_blank">@rebeinstein</a> <a class="mention" href="https://x.com/KillerMike" rel="noopener noreferrer" target="_blank">@KillerMike</a> This is the clip I most often share when talking to friends and family about Bernie. It represents a breakthrough in intersectional solidarity that reaches people in a deep way. Our society has been denied. Our democracy has been denied. And it\'s time to set it right.<br><br>In reply to: <a href="https://x.com/GeoffMiami/status/1231438254521626624" rel="noopener noreferrer" target="_blank">@GeoffMiami</a> <span class="status">1231438254521626624</span>',
    likes: 69,
    retweets: 6
  },
  {
    id: '1231420491891494914',
    created: 1582428498000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GeoffMiami" rel="noopener noreferrer" target="_blank">@GeoffMiami</a> <a class="mention" href="https://x.com/CornelWest" rel="noopener noreferrer" target="_blank">@CornelWest</a> I really can\'t say this enough. Thank you for pounding the pavement for us. And please thank the person next to you for me too.<br><br>In reply to: <a href="https://x.com/GeoffMiami/status/1231386335421849600" rel="noopener noreferrer" target="_blank">@GeoffMiami</a> <span class="status">1231386335421849600</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1231419704406732800',
    created: 1582428310000,
    type: 'post',
    text: 'Tonight, the movement won. Tonight, the people won. Justice for all. Let\'s keep fighting for it.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1231126248631357440',
    created: 1582358345000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ashleymcnamara" rel="noopener noreferrer" target="_blank">@ashleymcnamara</a> <a class="mention" href="https://x.com/girlsnamewillis" rel="noopener noreferrer" target="_blank">@girlsnamewillis</a> Congratulations! Wishing you two all the happiness in the world. If ever you have trouble finding that happiness in the world, look for it in each others\' eyes or in a hug. It always works for us.<br><br>In reply to: <a href="https://x.com/ashleymcnamara/status/1231054484366708736" rel="noopener noreferrer" target="_blank">@ashleymcnamara</a> <span class="status">1231054484366708736</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1230954674045386752',
    created: 1582317438000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/scottdavis99" rel="noopener noreferrer" target="_blank">@scottdavis99</a> <a class="mention" href="https://x.com/ixchelruiz" rel="noopener noreferrer" target="_blank">@ixchelruiz</a> <a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> <a class="mention" href="https://x.com/devnexus" rel="noopener noreferrer" target="_blank">@devnexus</a> That\'s how you know they ❤️ you.<br><br>In reply to: <a href="https://x.com/scottdavis99/status/1230954400056840193" rel="noopener noreferrer" target="_blank">@scottdavis99</a> <span class="status">1230954400056840193</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1230953237127131136',
    created: 1582317096000,
    type: 'post',
    text: 'As a registered independent in Colorado, you receive a Primary Election ballot just for...being a resident. If you don\'t specify which party\'s ballot to receive, you get both the Democratic and Republican ones (you can only return one).',
    likes: 0,
    retweets: 0
  },
  {
    id: '1230950159430762497',
    created: 1582316362000,
    type: 'quote',
    text: 'This person gets me. I feel like the more I scroll into a street, the more elusive the street name becomes.<br><br>Quoting: <a href="https://x.com/caseyjohnston/status/1230668928415825920" rel="noopener noreferrer" target="_blank">@caseyjohnston</a> <span class="status">1230668928415825920</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1230897640029577217',
    created: 1582303841000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> <a class="mention" href="https://x.com/ankinson" rel="noopener noreferrer" target="_blank">@ankinson</a> <a class="mention" href="https://x.com/petitlaurent" rel="noopener noreferrer" target="_blank">@petitlaurent</a> <a class="mention" href="https://x.com/springrestdocs" rel="noopener noreferrer" target="_blank">@springrestdocs</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> That one too. Again, the general design accounts for it. We just haven\'t implemented the parts that need to be implemented to realize it. The language is another facet of the virtual file, like model and family.<br><br>In reply to: <a href="https://x.com/alexsotob/status/1230894430913208323" rel="noopener noreferrer" target="_blank">@alexsotob</a> <span class="status">1230894430913208323</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1230893313881858048',
    created: 1582302809000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ankinson" rel="noopener noreferrer" target="_blank">@ankinson</a> <a class="mention" href="https://x.com/petitlaurent" rel="noopener noreferrer" target="_blank">@petitlaurent</a> <a class="mention" href="https://x.com/springrestdocs" rel="noopener noreferrer" target="_blank">@springrestdocs</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> API docs being a key use case, btw.<br><br>In reply to: <a href="#1230892853951229952">1230892853951229952</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1230893093395693570',
    created: 1582302757000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ankinson" rel="noopener noreferrer" target="_blank">@ankinson</a> <a class="mention" href="https://x.com/petitlaurent" rel="noopener noreferrer" target="_blank">@petitlaurent</a> <a class="mention" href="https://x.com/springrestdocs" rel="noopener noreferrer" target="_blank">@springrestdocs</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> Antora as a whole doesn\'t require a specific directory structure, just the scanner (which can be substituted). While we believe the standard structure is important, we also recognize there are other ways to get content into the catalog.<br><br>In reply to: <a href="#1230892853951229952">1230892853951229952</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1230892853951229952',
    created: 1582302699000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ankinson" rel="noopener noreferrer" target="_blank">@ankinson</a> <a class="mention" href="https://x.com/petitlaurent" rel="noopener noreferrer" target="_blank">@petitlaurent</a> <a class="mention" href="https://x.com/springrestdocs" rel="noopener noreferrer" target="_blank">@springrestdocs</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> In Antora 3, we\'ll be working on a simpler mechanism to integrate content from external or generated sources, either through configuration, extension, or both. The mechanism is already in place, but the wiring for it is a bit out of reach for most people (requiring custom code).<br><br>In reply to: <a href="https://x.com/ankinson/status/1230795416695951360" rel="noopener noreferrer" target="_blank">@ankinson</a> <span class="status">1230795416695951360</span>',
    likes: 5,
    retweets: 0
  },
  {
    id: '1230576864436998144',
    created: 1582227362000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ChristoferDutz" rel="noopener noreferrer" target="_blank">@ChristoferDutz</a> <a class="mention" href="https://x.com/obilodeau" rel="noopener noreferrer" target="_blank">@obilodeau</a> <a class="mention" href="https://x.com/revealjs" rel="noopener noreferrer" target="_blank">@revealjs</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/highlightjs" rel="noopener noreferrer" target="_blank">@highlightjs</a> <a class="mention" href="https://x.com/TheASF" rel="noopener noreferrer" target="_blank">@TheASF</a> That\'s pretty cool!<br><br>In reply to: <a href="https://x.com/ChristoferDutz/status/1230522703670587393" rel="noopener noreferrer" target="_blank">@ChristoferDutz</a> <span class="status">1230522703670587393</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1230070737668427783',
    created: 1582106692000,
    type: 'post',
    text: 'I just discovered that in Colorado, you can check the status of your ballot online. I just confirmed that my ballot was accepted for counting (after dropping it in a ballot drop-off location a day ago). That gives me additional confidence in the voting system. cc: <a class="mention" href="https://x.com/JenaGriswold" rel="noopener noreferrer" target="_blank">@JenaGriswold</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1230063329835503616',
    created: 1582104925000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/johniadarola" rel="noopener noreferrer" target="_blank">@johniadarola</a> Bernie Sanders #OurRevolution<br>Greta Thunberg #FridaysForFuture<br>Sunrise Movement<br><br>In reply to: <a href="https://x.com/johniadarola/status/1229891678095343616" rel="noopener noreferrer" target="_blank">@johniadarola</a> <span class="status">1229891678095343616</span>',
    likes: 3,
    retweets: 0,
    tags: ['ourrevolution', 'fridaysforfuture']
  },
  {
    id: '1229692658647363584',
    created: 1582016551000,
    type: 'post',
    text: 'My father (aka poppy), out there changing the world one seedling at a time. <a href="https://globalvolunteers.org/st-lucia-earthboxes-help-volunteers-work-toward-community-goals/" rel="noopener noreferrer" target="_blank">globalvolunteers.org/st-lucia-earthboxes-help-volunteers-work-toward-community-goals/</a> I might have to get myself one of those EarthBox planters!',
    likes: 1,
    retweets: 0
  },
  {
    id: '1229608157099331584',
    created: 1581996404000,
    type: 'post',
    text: 'On this Presidents\' Day, I\'m casting my first vote for <a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a> to become the next US President. (Colorado has early voting, so I don\'t have to wait until Super Tuesday). #Bernie2020 #NotMeUs',
    likes: 4,
    retweets: 2,
    tags: ['bernie2020', 'notmeus']
  },
  {
    id: '1229601245247098881',
    created: 1581994756000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rnelson0" rel="noopener noreferrer" target="_blank">@rnelson0</a> <a class="mention" href="https://x.com/quaid" rel="noopener noreferrer" target="_blank">@quaid</a> @soniagupta504 I\'ve already admitted that it was a misunderstanding on my part. I\'m not trying to sow any sort of animosity.<br><br>In reply to: <a href="https://x.com/rnelson0/status/1229594671099318273" rel="noopener noreferrer" target="_blank">@rnelson0</a> <span class="status">1229594671099318273</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1229561915459792897',
    created: 1581985379000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/quaid" rel="noopener noreferrer" target="_blank">@quaid</a> @soniagupta504 Oh, now I\'m blocked. Very strange. I did say I share the concern, but I guess that\'s not good enough.<br><br>In reply to: <a href="https://x.com/quaid/status/1229550046330253313" rel="noopener noreferrer" target="_blank">@quaid</a> <span class="status">1229550046330253313</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1229554317478125568',
    created: 1581983567000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/quaid" rel="noopener noreferrer" target="_blank">@quaid</a> @soniagupta504 So point taken.<br><br>In reply to: <a href="https://x.com/quaid/status/1229550046330253313" rel="noopener noreferrer" target="_blank">@quaid</a> <span class="status">1229550046330253313</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1229553177986359297',
    created: 1581983296000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/quaid" rel="noopener noreferrer" target="_blank">@quaid</a> @soniagupta504 It\'s the second statement that really bothers me, because it\'s dangerously close to propagating the myth that not voting for Warren is to accept racism and sexism. And I very much do not accept it. (I say this with all due respect, and I mean that).<br><br>In reply to: <a href="#1229551605785092096">1229551605785092096</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1229551605785092096',
    created: 1581982921000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/quaid" rel="noopener noreferrer" target="_blank">@quaid</a> @soniagupta504 *If* someone believes she\'s the best candidate, but is not voting for her because that person doesn\'t believe she can win, then I agree that\'s a big problem.<br><br>(I don\'t think the statement frames it this way, but I\'m willing to give the benefit of the doubt).<br><br>In reply to: <a href="https://x.com/quaid/status/1229550046330253313" rel="noopener noreferrer" target="_blank">@quaid</a> <span class="status">1229550046330253313</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1229548830250553345',
    created: 1581982259000,
    type: 'reply',
    text: '@soniagupta504 <a class="mention" href="https://x.com/quaid" rel="noopener noreferrer" target="_blank">@quaid</a> I happen to think Warren is a good person who has done amazing work. I can like someone and not think that person is the best candidate. You\'re statement leaves no room for that possibility, which makes me conclude it\'s divisive.<br><br>In reply to: <a href="#1229548266250887169">1229548266250887169</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1229548266250887169',
    created: 1581982125000,
    type: 'reply',
    text: '@soniagupta504 <a class="mention" href="https://x.com/quaid" rel="noopener noreferrer" target="_blank">@quaid</a> Don\'t speak for me. I don\'t think she doesn\'t have the ability to win. I think it\'s because Bernie is a better candidate with a better platform and a stronger movement. Everything Bernie is about is getting away from the injustices we know.<br><br>In reply to: @soniagupta504 <span class="status">&lt;deleted&gt;</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1229546123229925376',
    created: 1581981614000,
    type: 'post',
    text: 'People who defend billionaires over workers and economic justice absolutely disgust me. Like literally, I\'m sick to my stomach thinking about it. Billionaires become billionaires by inheriting wealth and/or exploiting workers.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1229545216949899264',
    created: 1581981398000,
    type: 'reply',
    text: '@avneet_paul You\'re citing pure fiction. Thankfully, the workers are now wise to this deception. I have no interest in discussing fiction with you. Goodbye.<br><br>In reply to: <a href="https://x.com/avneet842/status/1229542437468672005" rel="noopener noreferrer" target="_blank">@avneet842</a> <span class="status">1229542437468672005</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1229523713080451074',
    created: 1581976271000,
    type: 'reply',
    text: '@avneet_paul It\'s wrong for Peter not to pay at all, which is the current situation. Billionaires (the status) should not exist. Get out of my timeline if you think otherwise.<br><br>In reply to: <a href="https://x.com/avneet842/status/1229516196929773570" rel="noopener noreferrer" target="_blank">@avneet842</a> <span class="status">1229516196929773570</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1229499788648824832',
    created: 1581970567000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lifeofmoe93" rel="noopener noreferrer" target="_blank">@lifeofmoe93</a> @avneet_paul Bernie shows compassion for the disenfranchised. You can try to use that against him to make it sound like he supports the situation. He absolutely does not. He wants to solve the root cause that got them there. A nurse or doctor does not cause the sickness.<br><br>In reply to: <a href="https://x.com/lifeofmoe93/status/1229490720240738305" rel="noopener noreferrer" target="_blank">@lifeofmoe93</a> <span class="status">1229490720240738305</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1229496088073101312',
    created: 1581969684000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lifeofmoe93" rel="noopener noreferrer" target="_blank">@lifeofmoe93</a> @avneet_paul You talk about evading reality, yet have to rely on out of context clips to make a point. Good luck with that. Try reading his book or an actual law he\'s written. He is consistent in fighting for worker rights, paying fair share, and universal, non-discriminatory programs.<br><br>In reply to: <a href="https://x.com/lifeofmoe93/status/1229490720240738305" rel="noopener noreferrer" target="_blank">@lifeofmoe93</a> <span class="status">1229490720240738305</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1229486616370016256',
    created: 1581967426000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lifeofmoe93" rel="noopener noreferrer" target="_blank">@lifeofmoe93</a> @avneet_paul No, I\'m saying this is a classic example of spreading misinformation by taking old clips to make it look like someone is saying something they are not saying. It\'s childish and it\'s not going to work. To say Bernie supports bread lines or Castro is just ignorant.<br><br>In reply to: <a href="https://x.com/lifeofmoe93/status/1229408553275920384" rel="noopener noreferrer" target="_blank">@lifeofmoe93</a> <span class="status">1229408553275920384</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1229485494561456128',
    created: 1581967159000,
    type: 'reply',
    text: '@avneet_paul You have no idea what you\'re talking about and showing little understanding of how our gov\'t works. We\'ve had the programs Bernie is proposing for a large part of our country\'s history. It\'s when they were torn apart that income inequality skyrocketed. These aren\'t radical ideas.<br><br>In reply to: <a href="https://x.com/avneet842/status/1229406851927814145" rel="noopener noreferrer" target="_blank">@avneet842</a> <span class="status">1229406851927814145</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1229364786065887233',
    created: 1581938380000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MikePosner" rel="noopener noreferrer" target="_blank">@MikePosner</a> Thanks for sharing your story and your compassion. It was inspiring!<br><br>In reply to: <a href="https://x.com/MikePosner/status/1228832302295158785" rel="noopener noreferrer" target="_blank">@MikePosner</a> <span class="status">1228832302295158785</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1229319539382312961',
    created: 1581927592000,
    type: 'post',
    text: 'Ballot in hand. The envelope says it all. Vote early! And that\'s exactly what I\'ll be doing.',
    photos: ['<div class="item"><img class="photo" src="media/1229319539382312961.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1229309899852500992',
    created: 1581925294000,
    type: 'reply',
    text: '@avneet_paul No. That\'s completely misunderstanding his point. His point is about the danger of wealth inequality. He\'s saying a country should not allow its citizens to starve. The solution is to fix wealth inequality. You\'re using clips to make him say something else. It doesn\'t work.<br><br>In reply to: <a href="https://x.com/avneet842/status/1229262729539653633" rel="noopener noreferrer" target="_blank">@avneet842</a> <span class="status">1229262729539653633</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1229257844978962432',
    created: 1581912883000,
    type: 'post',
    text: 'Although just about every statement Bernie made was met with roaring applause, from where I was standing, the top issues were a woman\'s right to control her own body / making Roe law, equal pay for womxn, and public education over charter schools.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1229256307670044672',
    created: 1581912516000,
    type: 'post',
    text: 'On top of taking part in a #NotMeUs rally in Denver that was heard coast to coast, I also discovered a new band, <a class="mention" href="https://x.com/devotchkamusic" rel="noopener noreferrer" target="_blank">@devotchkamusic</a>. Now enjoying the tunes while sipping some locally-brewed beer from <a class="mention" href="https://x.com/ResoluteBrewCo" rel="noopener noreferrer" target="_blank">@ResoluteBrewCo</a>. I\'d call that a good night.',
    likes: 1,
    retweets: 0,
    tags: ['notmeus']
  },
  {
    id: '1229251097052749825',
    created: 1581911274000,
    type: 'post',
    text: '11,400 in attendance. 😲',
    likes: 0,
    retweets: 0
  },
  {
    id: '1229250663068073990',
    created: 1581911171000,
    type: 'quote',
    text: 'Holy moly!!!<br><br>Quoting: <a href="https://x.com/the_vello/status/1229214912775368704" rel="noopener noreferrer" target="_blank">@the_vello</a> <span class="status">1229214912775368704</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1229241990572904448',
    created: 1581909103000,
    type: 'post',
    text: 'That Bernie rally was so lit (practically deafening) that you\'d think it was the night of the general election. Wow.',
    photos: ['<div class="item"><img class="photo" src="media/1229241990572904448.jpg"></div>', '<div class="item"><img class="photo" src="media/1229241990572904448-2.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1229238180076978176',
    created: 1581908194000,
    type: 'post',
    text: 'The establishment Dems soooooo want this to be about them. It\'s not. #NotMeUs',
    photos: ['<div class="item"><img class="photo" src="media/1229238180076978176.jpg"></div>', '<div class="item"><img class="photo" src="media/1229238180076978176-2.jpg"></div>'],
    likes: 0,
    retweets: 0,
    tags: ['notmeus']
  },
  {
    id: '1229223108378271744',
    created: 1581904601000,
    type: 'post',
    text: '"Because we believe in education, we believe in EDUCATORS!" — <a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a> calling for a minimum salary of $60,000 for all public school teachers.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1229192255820517378',
    created: 1581897245000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/carolinegleich" rel="noopener noreferrer" target="_blank">@carolinegleich</a> They\'re so patient too. I took a 2 week road trip and my black rose (named beau) was totally fine when I got back. Just in time for watering day. I can\'t say the same for some of my other plants.<br><br>In reply to: <a href="https://x.com/carolinegleich/status/1229182041175969792" rel="noopener noreferrer" target="_blank">@carolinegleich</a> <span class="status">1229182041175969792</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1229179893394444289',
    created: 1581894298000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/carolinegleich" rel="noopener noreferrer" target="_blank">@carolinegleich</a> I love succulents! Here\'s mine.<br><br>In reply to: <a href="https://x.com/carolinegleich/status/1229168423227117568" rel="noopener noreferrer" target="_blank">@carolinegleich</a> <span class="status">1229168423227117568</span>',
    photos: ['<div class="item"><img class="photo" src="media/1229179893394444289.jpg"></div>'],
    likes: 1,
    retweets: 0
  },
  {
    id: '1229178648252436480',
    created: 1581894001000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <a class="mention" href="https://x.com/michaelneale" rel="noopener noreferrer" target="_blank">@michaelneale</a> See <a href="http://clagnut.com/blog/2380/" rel="noopener noreferrer" target="_blank">clagnut.com/blog/2380/</a><br><br>In reply to: <a href="#1229178475719749632">1229178475719749632</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1229178475719749632',
    created: 1581893960000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <a class="mention" href="https://x.com/michaelneale" rel="noopener noreferrer" target="_blank">@michaelneale</a> It\'s called a pangram.<br><br>In reply to: <a href="#1229178302025224192">1229178302025224192</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1229178302025224192',
    created: 1581893918000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <a class="mention" href="https://x.com/michaelneale" rel="noopener noreferrer" target="_blank">@michaelneale</a> There is a sentence in almost every known language (not all of which use every letter exactly once, which has as special name). It\'s essential for testing fonts.<br><br>In reply to: <a href="https://x.com/emmanuelbernard/status/1229158325557891072" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> <span class="status">1229158325557891072</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1229127660460113922',
    created: 1581881844000,
    type: 'post',
    text: 'If you\'re in Denver, don\'t miss the opportunity to see Bernie Sanders at the rally later today. Bring a friend. <a href="https://events.berniesanders.com/event/231792" rel="noopener noreferrer" target="_blank">events.berniesanders.com/event/231792</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1228813053208088576',
    created: 1581806836000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dcwoodruff" rel="noopener noreferrer" target="_blank">@dcwoodruff</a> I\'m becoming increasingly frustrated that I never hear about these events before they happen, despite being on dozens of lists. Is there a definite list of events anywhere?<br><br>In reply to: <a href="https://x.com/dcwoodruff/status/1228784096790405125" rel="noopener noreferrer" target="_blank">@dcwoodruff</a> <span class="status">1228784096790405125</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1228809912009940992',
    created: 1581806087000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AbdulElSayed" rel="noopener noreferrer" target="_blank">@AbdulElSayed</a> <a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a> It\'s voter suppression. And it\'s being done by Democrats.<br><br>In reply to: <a href="https://x.com/AbdulElSayed/status/1228798156583788544" rel="noopener noreferrer" target="_blank">@AbdulElSayed</a> <span class="status">1228798156583788544</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1228640599194165249',
    created: 1581765720000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Lucyrushi" rel="noopener noreferrer" target="_blank">@Lucyrushi</a> More than anything else, it\'s the people. Such an abundance of kindheartedness. The city is pretty special too.<br><br>In reply to: <a href="#1228639544855101444">1228639544855101444</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1228639544855101444',
    created: 1581765469000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Lucyrushi" rel="noopener noreferrer" target="_blank">@Lucyrushi</a> You will love it.<br><br>In reply to: <a href="https://x.com/Lucyrushi/status/1228637256254152704" rel="noopener noreferrer" target="_blank">@Lucyrushi</a> <span class="status">1228637256254152704</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1228548264825368576',
    created: 1581743706000,
    type: 'quote',
    text: 'O<br>M<br>G<br><br>You will not be able to resist these pearls. Grrrrr.<br><br>Quoting: <a href="https://x.com/IsomorphicGit/status/1228528468654272512" rel="noopener noreferrer" target="_blank">@IsomorphicGit</a> <span class="status">1228528468654272512</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1228386538557337600',
    created: 1581705147000,
    type: 'quote',
    text: 'Could just as easily be the American Government.<br><br>Quoting: <a href="https://x.com/thejuicemedia/status/1227347211941318657" rel="noopener noreferrer" target="_blank">@thejuicemedia</a> <span class="status">1227347211941318657</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1228377243870609409',
    created: 1581702931000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/phillip_webb" rel="noopener noreferrer" target="_blank">@phillip_webb</a> Awwww. Sounds like you\'re blessed with the best valentine. Sweet dreams.<br><br>In reply to: <a href="https://x.com/phillip_webb/status/1228357848993677317" rel="noopener noreferrer" target="_blank">@phillip_webb</a> <span class="status">1228357848993677317</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1228374196650041349',
    created: 1581702205000,
    type: 'post',
    text: 'On this #ValentinesDay, it\'s important to remember that love is love. 🏳️‍🌈',
    likes: 3,
    retweets: 1,
    tags: ['valentinesday']
  },
  {
    id: '1228246979194392576',
    created: 1581671874000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/wintermeyer" rel="noopener noreferrer" target="_blank">@wintermeyer</a> Thanks! I love the tough customers ;)<br><br>To be clear, you\'re referring to the Asciidoctor PDF 1.5.0 release.<br><br>In reply to: <a href="https://x.com/wintermeyer/status/1228234309556830209" rel="noopener noreferrer" target="_blank">@wintermeyer</a> <span class="status">1228234309556830209</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1228229948994772995',
    created: 1581667813000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/vogella" rel="noopener noreferrer" target="_blank">@vogella</a> <a class="mention" href="https://x.com/EclipseJavaIDE" rel="noopener noreferrer" target="_blank">@EclipseJavaIDE</a> That\'s dang impressive. Congrats!<br><br>In reply to: <a href="https://x.com/vogella/status/1228229540687695872" rel="noopener noreferrer" target="_blank">@vogella</a> <span class="status">1228229540687695872</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1228109155350663168',
    created: 1581639014000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sweblogtweets" rel="noopener noreferrer" target="_blank">@sweblogtweets</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/jqassistant" rel="noopener noreferrer" target="_blank">@jqassistant</a> I\'ve moved that issue up to 2.x. It seems reasonable to implement.<br><br>In reply to: <a href="https://x.com/sweblogtweets/status/1227362218775371780" rel="noopener noreferrer" target="_blank">@sweblogtweets</a> <span class="status">1227362218775371780</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1228058454037692416',
    created: 1581626926000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> <a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> Yes, I agree. I was just pointing out that work has been done on it.<br><br>In reply to: <a href="https://x.com/alexsotob/status/1227999510707744768" rel="noopener noreferrer" target="_blank">@alexsotob</a> <span class="status">1227999510707744768</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1227936842613325824',
    created: 1581597931000,
    type: 'post',
    text: 'I can\'t help but to conclude that the influence and message of <a class="mention" href="https://x.com/GretaThunberg" rel="noopener noreferrer" target="_blank">@GretaThunberg</a> had something to do with Parasite sweeping the Oscars. We have to start connecting with and caring about the consequences of climate change, especially how it disproportionally impacts the poor.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1227767283105140736',
    created: 1581557505000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ProudResister" rel="noopener noreferrer" target="_blank">@ProudResister</a> <a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a> Thanks Ryan. Welcome to #NotMeUs to fight together for a future that works for everyone. ♥️☮️🌈<br><br>In reply to: <a href="https://x.com/ProudSocialist/status/1227710443524849665" rel="noopener noreferrer" target="_blank">@ProudSocialist</a> <span class="status">1227710443524849665</span>',
    likes: 0,
    retweets: 0,
    tags: ['notmeus']
  },
  {
    id: '1227708077954101248',
    created: 1581543390000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/waynebeaton" rel="noopener noreferrer" target="_blank">@waynebeaton</a> Yep. I feel ya.<br><br>In reply to: <a href="https://x.com/waynebeaton/status/1227680436962430976" rel="noopener noreferrer" target="_blank">@waynebeaton</a> <span class="status">1227680436962430976</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1227679245935116288',
    created: 1581536516000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/codeslubber" rel="noopener noreferrer" target="_blank">@codeslubber</a> <a class="mention" href="https://x.com/briebriejoy" rel="noopener noreferrer" target="_blank">@briebriejoy</a> 💯 for reading...on the toilet if you must. It\'s good for the mind and the soul.<br><br>In reply to: <a href="https://x.com/codeslubber/status/1227677890340212737" rel="noopener noreferrer" target="_blank">@codeslubber</a> <span class="status">1227677890340212737</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1227678714042802176',
    created: 1581536389000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/leftyaaron" rel="noopener noreferrer" target="_blank">@leftyaaron</a> @bygomez02 <a class="mention" href="https://x.com/hilltvlive" rel="noopener noreferrer" target="_blank">@hilltvlive</a> <a class="mention" href="https://x.com/SocialistAlt" rel="noopener noreferrer" target="_blank">@SocialistAlt</a> <a class="mention" href="https://x.com/democracynow" rel="noopener noreferrer" target="_blank">@democracynow</a> <a class="mention" href="https://x.com/TYTPolitics" rel="noopener noreferrer" target="_blank">@TYTPolitics</a> <a class="mention" href="https://x.com/KyleKulinski" rel="noopener noreferrer" target="_blank">@KyleKulinski</a> <a class="mention" href="https://x.com/HumanistReport" rel="noopener noreferrer" target="_blank">@HumanistReport</a> <a class="mention" href="https://x.com/RealChangeNews" rel="noopener noreferrer" target="_blank">@RealChangeNews</a> I\'ll add <a class="mention" href="https://x.com/StatusCoup" rel="noopener noreferrer" target="_blank">@StatusCoup</a> and <a class="mention" href="https://x.com/TheRealNews" rel="noopener noreferrer" target="_blank">@TheRealNews</a> to that list.<br><br>In reply to: <a href="https://x.com/leftyaaron/status/1227674136262008832" rel="noopener noreferrer" target="_blank">@leftyaaron</a> <span class="status">1227674136262008832</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1227678341240520704',
    created: 1581536300000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/waynebeaton" rel="noopener noreferrer" target="_blank">@waynebeaton</a> I\'m firmly on the fence about this one. It does some things really well, but it also misbehaves quite profoundly.<br><br>In reply to: <a href="https://x.com/waynebeaton/status/1227672300113276928" rel="noopener noreferrer" target="_blank">@waynebeaton</a> <span class="status">1227672300113276928</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1227677322129494016',
    created: 1581536057000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/edburns" rel="noopener noreferrer" target="_blank">@edburns</a> 👏<br><br>In reply to: <a href="https://x.com/edburns/status/1227676363898982400" rel="noopener noreferrer" target="_blank">@edburns</a> <span class="status">1227676363898982400</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1227676531549331456',
    created: 1581535868000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/briebriejoy" rel="noopener noreferrer" target="_blank">@briebriejoy</a> #BathroomBreaks4All Get it trending. 🤣<br><br>In reply to: <a href="https://x.com/briebriejoy/status/1227667241921105920" rel="noopener noreferrer" target="_blank">@briebriejoy</a> <span class="status">1227667241921105920</span>',
    likes: 7,
    retweets: 0,
    tags: ['bathroombreaks4all']
  },
  {
    id: '1227533891742756864',
    created: 1581501860000,
    type: 'post',
    text: '...refuge and opportunity.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1227498021283221505',
    created: 1581493308000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/michaelsayman" rel="noopener noreferrer" target="_blank">@michaelsayman</a> Thank you and all the volunteers who traveled to NH to GOTV. You\'re heroes tonight. Please pass it on.<br><br>In reply to: <a href="https://x.com/michaelsayman/status/1227448133296193536" rel="noopener noreferrer" target="_blank">@michaelsayman</a> <span class="status">1227448133296193536</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1227496961244221440',
    created: 1581493056000,
    type: 'post',
    text: 'Bernie Sanders. It\'s happening. We\'re going to transform this country to one that\'s as good as its promise, for all citizens and for those who seek refuge here. #NotMeUs',
    likes: 2,
    retweets: 0,
    tags: ['notmeus']
  },
  {
    id: '1227495066647707648',
    created: 1581492604000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gregturn" rel="noopener noreferrer" target="_blank">@gregturn</a> That\'s not currently possible (without an extension). We hit the same problem in the Asciidoctor docs, so we\'d certainly consider it.<br><br>In reply to: <a href="https://x.com/gregturn/status/1227436769903292416" rel="noopener noreferrer" target="_blank">@gregturn</a> <span class="status">1227436769903292416</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1227299794474815488',
    created: 1581446047000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> <a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> Yes, David Jencks put one together. Ping him in the gitter channel.<br><br>In reply to: <a href="https://x.com/majson/status/1227242608092598272" rel="noopener noreferrer" target="_blank">@majson</a> <span class="status">1227242608092598272</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1227038268765564928',
    created: 1581383695000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/michaelsayman" rel="noopener noreferrer" target="_blank">@michaelsayman</a> Now that\'s putting points on the board!<br><br>In reply to: <a href="https://x.com/michaelsayman/status/1227036915297177600" rel="noopener noreferrer" target="_blank">@michaelsayman</a> <span class="status">1227036915297177600</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1227030286027739136',
    created: 1581381791000,
    type: 'quote',
    text: 'At the very least, it should be the other way around.<br><br>Quoting: <a href="https://x.com/davidbrunelle/status/1226928859481661440" rel="noopener noreferrer" target="_blank">@davidbrunelle</a> <span class="status">1226928859481661440</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1227030043345354752',
    created: 1581381734000,
    type: 'post',
    text: 'As part of my zero waste effort, I\'ve switched to using botanical soaps hand-crafted locally by <a class="mention" href="https://x.com/ApothecaryFairy" rel="noopener noreferrer" target="_blank">@ApothecaryFairy</a> and packaged in paper and cardboard). They even do custom requests.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1227001501270208513',
    created: 1581374929000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ezraklein" rel="noopener noreferrer" target="_blank">@ezraklein</a> Oh, stop lying. Or keep lying and destroy your credibility. Do what you want.<br><br>In reply to: <a href="https://x.com/ezraklein/status/1226861900832661504" rel="noopener noreferrer" target="_blank">@ezraklein</a> <span class="status">1226861900832661504</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1226993704428113920',
    created: 1581373070000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jonbullock" rel="noopener noreferrer" target="_blank">@jonbullock</a> I always just use "upstream" (or upstream issue).<br><br>In reply to: <a href="https://x.com/jonbullock/status/1226975159636103175" rel="noopener noreferrer" target="_blank">@jonbullock</a> <span class="status">1226975159636103175</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1226975231941783555',
    created: 1581368666000,
    type: 'post',
    text: '👽 journalist: What makes your country so great?<br>🇺🇸 citizen: Well, we\'re the wealthiest nation in the most advanced civilization in history.<br>👽 journalist: That must mean every citizen has amazing health care.<br>🇺🇸 citizen: 👀',
    likes: 6,
    retweets: 1
  },
  {
    id: '1226833652090228739',
    created: 1581334910000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mxmkm" rel="noopener noreferrer" target="_blank">@mxmkm</a> Oh, I will ;)<br><br>In reply to: <a href="https://x.com/mxmkm/status/1226827763157491713" rel="noopener noreferrer" target="_blank">@mxmkm</a> <span class="status">1226827763157491713</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1226823729142882306',
    created: 1581332544000,
    type: 'post',
    text: 'Lots of exciting things happening in Asciidoctor this week!',
    likes: 23,
    retweets: 1
  },
  {
    id: '1226806212018130944',
    created: 1581328368000,
    type: 'reply',
    text: '@brucefranksjr You truly are one of my heroes.<br><br>In reply to: @brucefranksjr <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1226803323891662849',
    created: 1581327679000,
    type: 'quote',
    text: 'Beautiful! ☮️❤️🌈🤝<br><br>Quoting: <a href="https://x.com/nowthisimpact/status/1226747685006434304" rel="noopener noreferrer" target="_blank">@nowthisimpact</a> <span class="status">1226747685006434304</span>',
    likes: 5,
    retweets: 3
  },
  {
    id: '1226781166293831680',
    created: 1581322397000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/corneil" rel="noopener noreferrer" target="_blank">@corneil</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Thank you. But you\'re very worthy. This project is by all of us and for all of us. It always has been.<br><br>In reply to: <a href="https://x.com/corneil/status/1226491527666401281" rel="noopener noreferrer" target="_blank">@corneil</a> <span class="status">1226491527666401281</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1226472292466610176',
    created: 1581248755000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/michaelsayman" rel="noopener noreferrer" target="_blank">@michaelsayman</a> You really should post the video clip of your speech from this event. It\'s an important story and needs to be heard.<br><br>In reply to: <a href="https://x.com/michaelsayman/status/1223088170088136706" rel="noopener noreferrer" target="_blank">@michaelsayman</a> <span class="status">1223088170088136706</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1226437718051147777',
    created: 1581240512000,
    type: 'quote',
    text: 'To make this work for two files that are in a git worktree, you need to add the --no-index flag:<br><br>$ git diff --no-index --color-words file-a.adoc file-b.adoc<br><br>Quoting: <a href="https://x.com/fifthposition/status/1225920844108640258" rel="noopener noreferrer" target="_blank">@fifthposition</a> <span class="status">1225920844108640258</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1226427638413021184',
    created: 1581238109000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Jamie_Margolin" rel="noopener noreferrer" target="_blank">@Jamie_Margolin</a> That is AWESOME! This will not only help US win, it\'s a win for all future generations. Thanks for all that you do.<br><br>In reply to: <a href="https://x.com/Jamie_Margolin/status/1226272308584562688" rel="noopener noreferrer" target="_blank">@Jamie_Margolin</a> <span class="status">1226272308584562688</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1226265262275452929',
    created: 1581199396000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/michaelsayman" rel="noopener noreferrer" target="_blank">@michaelsayman</a> Garbage.<br><br>In reply to: <a href="https://x.com/michaelsayman/status/1226250409850720256" rel="noopener noreferrer" target="_blank">@michaelsayman</a> <span class="status">1226250409850720256</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1226265083530993664',
    created: 1581199353000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/johnmark" rel="noopener noreferrer" target="_blank">@johnmark</a> I\'d be interested in joining / supporting (even though I don\'t work for a large company, I would stand with those who do).<br><br>In reply to: <a href="https://x.com/johnmark/status/1226215992302481410" rel="noopener noreferrer" target="_blank">@johnmark</a> <span class="status">1226215992302481410</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1226074739770191872',
    created: 1581153971000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> You might want to reach out to <a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> or <a href="https://github.com/aerostitch" rel="noopener noreferrer" target="_blank">github.com/aerostitch</a>. I didn\'t do the packaging for Debian, so that\'s not my area of knowledge.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1226073839949484033" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1226073839949484033</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1226073330895085570',
    created: 1581153636000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Having done that several times many years ago for the Asciidoctor gems, let me just say...I feel your pain. Although it was a ton of work, it really did pay off in the long run.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1226069880182579200" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1226069880182579200</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1226072746418827264',
    created: 1581153496000,
    type: 'post',
    text: 'I thoroughly enjoyed watching <a class="mention" href="https://x.com/DollyParton" rel="noopener noreferrer" target="_blank">@DollyParton</a>\'s Heartstrings (Netflix). It was deeply touching. Each episode features a story developed from the lyrics of one of her songs. Such a rewarding idea (since we often ponder the meaning of lyrics and the characters in them).',
    likes: 2,
    retweets: 0
  },
  {
    id: '1226053715154440192',
    created: 1581148959000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sunrisemvmt" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a> You are perfect the way you are. And when the sun rises, we know Bernie will be thinking of all of you.<br><br>In reply to: <a href="https://x.com/sunrisemvmt/status/1225981212554125312" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a> <span class="status">1225981212554125312</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1225972840769261568',
    created: 1581129677000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sarahhollowell" rel="noopener noreferrer" target="_blank">@sarahhollowell</a> Same!<br><br>In reply to: <a href="https://x.com/sarahhollowell/status/1222629834171604992" rel="noopener noreferrer" target="_blank">@sarahhollowell</a> <span class="status">1222629834171604992</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1225952452563763201',
    created: 1581124816000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> That was exactly the problem. I\'d say it was a rookie mistake, but Fedora is packaging multiple versions of Java now and I managed to confuse dnf when I was upgrading.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1225947861172527106" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1225947861172527106</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1225949229312425984',
    created: 1581124047000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Yep, it\'s all good now. It works right out of the box, assuming someone doesn\'t have mixed versions of OpenJDK installed like I did. (I upgraded recently and hadn\'t finish cleaning up my packages).<br><br>In reply to: <a href="#1225948340338282497">1225948340338282497</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1225948340338282497',
    created: 1581123836000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Oops, this is my mistake. javac on my machine is java 13. Let me clear that out.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1225948107248197633" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1225948107248197633</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1225945202067558400',
    created: 1581123087000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> I tried with Java 11 and that didn\'t work either. It said I had 55.0 instead of 57.0.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1225942402323689472" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1225942402323689472</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1225945104700936192',
    created: 1581123064000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> The installation works, but when I execute the script I get:<br><br>helloworld has been compiled by a more recent version of the Java Runtime (class file version 57.0), this version of the Java Runtime only recognizes class file versions up to 52.0<br><br>I have Java 8 installed.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1225942402323689472" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1225942402323689472</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1225922547515973633',
    created: 1581117686000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/garrett" rel="noopener noreferrer" target="_blank">@garrett</a> I\'m just using the default theme, though you gave me the idea to try to run without extensions and a fresh profile to establish a baseline. I\'ll see if that solves it. Thanks.<br><br>In reply to: <a href="https://x.com/garrett/status/1225917856430383112" rel="noopener noreferrer" target="_blank">@garrett</a> <span class="status">1225917856430383112</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1225908491782479872',
    created: 1581114335000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> And to think we still have some really exciting stuff in the pipeline. I\'m itching to get it integrated and released!<br><br>In reply to: <a href="https://x.com/settermjd/status/1225903594534207488" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1225903594534207488</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1225902123101118465',
    created: 1581112816000,
    type: 'post',
    text: 'When it does come up, it\'s often way off the right of where I clicked.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1225901815503286272',
    created: 1581112743000,
    type: 'post',
    text: 'There\'s a bug in Firefox (for Wayland) that is absolutely killing me. The context menu only comes up 50% of the time. (It works fine in all other apps). This only started happening in Firefox 72. I\'m not sure what\'s going on, but I sure hope it gets corrected soon for sanity.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1225861960639512576',
    created: 1581103241000,
    type: 'quote',
    text: 'Bernie gets it. He really gets it.<br><br>Quoting: <a href="https://x.com/BernieSanders/status/1225595022332002305" rel="noopener noreferrer" target="_blank">@BernieSanders</a> <span class="status">1225595022332002305</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1225849652517330944',
    created: 1581100307000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> There\'s almost always a hypocrite hiding behind someone who\'s throwing stones.<br><br>In reply to: <a href="https://x.com/headius/status/1225846859664691200" rel="noopener noreferrer" target="_blank">@headius</a> <span class="status">1225846859664691200</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1225849285306007552',
    created: 1581100219000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> I hear you. The social platforms are quickly becoming (or have already become) a trial by which to judge others. I wish they would embrace the OSS spirit of supporting each other (at least the OSS I remember).<br><br>In reply to: <a href="https://x.com/headius/status/1225846859664691200" rel="noopener noreferrer" target="_blank">@headius</a> <span class="status">1225846859664691200</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1225845646315253760',
    created: 1581099351000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> I, for one, would never do that to a person. I have no right to tell you what decisions to make and wouldn\'t be so arrogant to try. I would (and do) share what a great experience it has been for me in case it proves useful for others and because it\'s now a core part of my life.<br><br>In reply to: <a href="https://x.com/headius/status/1225767302538371072" rel="noopener noreferrer" target="_blank">@headius</a> <span class="status">1225767302538371072</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1225737255144185857',
    created: 1581073509000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/JenaGriswold" rel="noopener noreferrer" target="_blank">@JenaGriswold</a> I just learned that several states are using ranked choice voting this spring (such as Alaska, Hawaii, and our neighbor, Kansas). Can we do that in Colorado please?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1225714526890278912',
    created: 1581068090000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> <a class="mention" href="https://x.com/jellym4nn" rel="noopener noreferrer" target="_blank">@jellym4nn</a> Cool! That\'s a lot of 🐢🐢🐢<br><br>In reply to: <a href="https://x.com/headius/status/1225712931943407617" rel="noopener noreferrer" target="_blank">@headius</a> <span class="status">1225712931943407617</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1225581146076876800',
    created: 1581036290000,
    type: 'post',
    text: '(Despite the best efforts of the political establishment to stop working people’s voices from being heard).',
    likes: 0,
    retweets: 0
  },
  {
    id: '1225578309334863872',
    created: 1581035613000,
    type: 'quote',
    text: 'Bernie won Iowa. If you think otherwise, you\'re being played. Congratulations, Bernie! And congratulations to every volunteer who made this happen. This is good for all of us.* #NotMeUs<br><br>* Even billionaires, who will get to enjoy a livable planet that they\'ll help pay to protect.<br><br>Quoting: <a href="https://x.com/BernieSanders/status/1225524699217702918" rel="noopener noreferrer" target="_blank">@BernieSanders</a> <span class="status">1225524699217702918</span>',
    likes: 2,
    retweets: 0,
    tags: ['notmeus']
  },
  {
    id: '1225558180320751617',
    created: 1581030814000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/carolinegleich" rel="noopener noreferrer" target="_blank">@carolinegleich</a> <a class="mention" href="https://x.com/BLMNational" rel="noopener noreferrer" target="_blank">@BLMNational</a> <a class="mention" href="https://x.com/Interior" rel="noopener noreferrer" target="_blank">@Interior</a> 😢<br><br>In reply to: <a href="https://x.com/carolinegleich/status/1225554373884837888" rel="noopener noreferrer" target="_blank">@carolinegleich</a> <span class="status">1225554373884837888</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1225485484253679623',
    created: 1581013482000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/TomPerez" rel="noopener noreferrer" target="_blank">@TomPerez</a> No, it\'s time for you to resign!<br><br>In reply to: <a href="https://x.com/TomPerez/status/1225468833458245632" rel="noopener noreferrer" target="_blank">@TomPerez</a> <span class="status">1225468833458245632</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1225379384095174659',
    created: 1580988186000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/russel_winder" rel="noopener noreferrer" target="_blank">@russel_winder</a> Sorry to hear that. I\'ll be keeping you in my thoughts. If you need strength, just remember how many of us care about you and your well-being. 🎈<br><br>In reply to: <a href="https://x.com/russel_winder/status/1225364706812157954" rel="noopener noreferrer" target="_blank">@russel_winder</a> <span class="status">1225364706812157954</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1225336860341178369',
    created: 1580978047000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/myfear" rel="noopener noreferrer" target="_blank">@myfear</a> <a class="mention" href="https://x.com/ameliaeiras" rel="noopener noreferrer" target="_blank">@ameliaeiras</a> I endorse this message 💯. I\'ll also add that no one lights up my stream with emoji in quite the same way (though you certainly gave it a great shot). Her hugs are epic as well. A very happy birthday to you, <a class="mention" href="https://x.com/ameliaeiras" rel="noopener noreferrer" target="_blank">@ameliaeiras</a>! 🥳🍻🤗<br><br>In reply to: <a href="https://x.com/myfear/status/1225330679925428224" rel="noopener noreferrer" target="_blank">@myfear</a> <span class="status">1225330679925428224</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1225318522516959232',
    created: 1580973675000,
    type: 'post',
    text: 'Clarification: multiply by 1777 locations, making the compounding even worse.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1225317311520727041',
    created: 1580973387000,
    type: 'quote',
    text: 'Wow, the compounding impact of rounding from caucuses is startling. Just study how this worksheet normalizes votes at a single site. Now multiply that by hundreds of locations. You wonder why we\'re calling for a popular vote across the board.<br><br>Quoting: <a href="https://x.com/JennUWinn84/status/1225306453575131136" rel="noopener noreferrer" target="_blank">@JennUWinn84</a> <span class="status">1225306453575131136</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1225239493726240769',
    created: 1580954833000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ryangrim" rel="noopener noreferrer" target="_blank">@ryangrim</a> <a class="mention" href="https://x.com/michaelsayman" rel="noopener noreferrer" target="_blank">@michaelsayman</a> Btw, thank you for sharing that stream to give us a window into the caucus process. It helped a lot to put the stories and clips that followed into context.<br><br>In reply to: <a href="https://x.com/ryangrim/status/1225238081566990336" rel="noopener noreferrer" target="_blank">@ryangrim</a> <span class="status">1225238081566990336</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1225201817190952960',
    created: 1580945851000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gabriel_zucman" rel="noopener noreferrer" target="_blank">@gabriel_zucman</a> cc: <a class="mention" href="https://x.com/leftyaaron" rel="noopener noreferrer" target="_blank">@leftyaaron</a> ☝️<br><br>In reply to: <a href="https://x.com/gabriel_zucman/status/1224781044840222725" rel="noopener noreferrer" target="_blank">@gabriel_zucman</a> <span class="status">1224781044840222725</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1225201624211083264',
    created: 1580945805000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fanf42" rel="noopener noreferrer" target="_blank">@fanf42</a> <a class="mention" href="https://x.com/gabriel_zucman" rel="noopener noreferrer" target="_blank">@gabriel_zucman</a> Great article. This gives me the material arguments I was looking for. Thanks.<br><br>In reply to: <a href="https://x.com/fanf42/status/1225137039278051330" rel="noopener noreferrer" target="_blank">@fanf42</a> <span class="status">1225137039278051330</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1225201266504036354',
    created: 1580945719000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/leftyaaron" rel="noopener noreferrer" target="_blank">@leftyaaron</a> That\'s so cool! <a class="mention" href="https://x.com/JenaGriswold" rel="noopener noreferrer" target="_blank">@JenaGriswold</a> and her predecessors deserve a lot of credit for making voting in Colorado easy, secure, and fun. I\'ve asked her about the stubs. Let\'s see.<br><br>In reply to: <a href="https://x.com/leftyaaron/status/1225200438162509825" rel="noopener noreferrer" target="_blank">@leftyaaron</a> <span class="status">1225200438162509825</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1225196703726235648',
    created: 1580944631000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/JenaGriswold" rel="noopener noreferrer" target="_blank">@JenaGriswold</a> How do you feel about the idea of ballot stubs that voters can use to verify that the votes were counted after the results are posted? This seems like a pretty vital step to full transparency.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1225194611884220416',
    created: 1580944133000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/leftyaaron" rel="noopener noreferrer" target="_blank">@leftyaaron</a> I like the receipt / stub idea. I wish Colorado had that. (Otherwise, the system is great here).<br><br>In reply to: <a href="https://x.com/leftyaaron/status/1225178763152855040" rel="noopener noreferrer" target="_blank">@leftyaaron</a> <span class="status">1225178763152855040</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1225177540869812225',
    created: 1580940063000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fanf42" rel="noopener noreferrer" target="_blank">@fanf42</a> <a class="mention" href="https://x.com/gabriel_zucman" rel="noopener noreferrer" target="_blank">@gabriel_zucman</a> My reading of economists (albeit progressive ones like S. Kelton) have concluded that it ends up being so little money to them that they don\'t even change behavior. But they will change behavior if they are fear mongered into doing so (which turns out badly for them).<br><br>In reply to: <a href="https://x.com/fanf42/status/1225137039278051330" rel="noopener noreferrer" target="_blank">@fanf42</a> <span class="status">1225137039278051330</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1225095464413880320',
    created: 1580920494000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> That would at least be logical (even though there are still fairer ways to resolve a tie). Here, one candidate already had earned dozens more votes, but somehow that equates to an equal number of delegates.<br><br>In reply to: <a href="https://x.com/alexsotob/status/1225080000560422912" rel="noopener noreferrer" target="_blank">@alexsotob</a> <span class="status">1225080000560422912</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1225042279284408321',
    created: 1580907814000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> Perhaps my mind was trying to encourage me to do more planting ;)<br><br>In reply to: <a href="https://x.com/marcsavy/status/1225038908691505154" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1225038908691505154</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1225041956293623808',
    created: 1580907737000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> It\'s very real. And very undemocratic. But it\'s good that people are finally seeing that there\'s no justification for this process anymore.<br><br>In reply to: <a href="https://x.com/ggrossetie/status/1225039362204762112" rel="noopener noreferrer" target="_blank">@ggrossetie</a> <span class="status">1225039362204762112</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1225038739111497733',
    created: 1580906970000,
    type: 'post',
    text: 'Correction: plant-based',
    likes: 0,
    retweets: 0
  },
  {
    id: '1225038650859114499',
    created: 1580906949000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> OMG, I meant to write plant-based.<br><br>In reply to: <a href="#1225038498874281985">1225038498874281985</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1225038498874281985',
    created: 1580906912000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> No (at least, not yet). It means all the food that goes into the dish is some part of a plant. You can be vegan and eat synthetic food additives like MSG or high fructose corn syrup. (There\'s a whole range of these).<br><br>In reply to: <a href="https://x.com/marcsavy/status/1225036005314568192" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1225036005314568192</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1225035689856602112',
    created: 1580906243000,
    type: 'post',
    text: 'Over half a year (planted-based) vegan. I feel great and have never been happier about eating. That\'s also because I no longer experience shame while eating something delectable, conditioning I had picked up from my youth. I just think, my body needs this, and enjoy the moment.<br><br>Quoting: <a href="#1194931897249349634">1194931897249349634</a>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1225032478378692609',
    created: 1580905477000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/majson" rel="noopener noreferrer" target="_blank">@majson</a> <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Hahaha. Exactly what I was thinking ;)<br><br>In reply to: <a href="https://x.com/majson/status/1225029262853464064" rel="noopener noreferrer" target="_blank">@majson</a> <span class="status">1225029262853464064</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1225032324351328257',
    created: 1580905440000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> I agree. It\'s revolting.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1225031092283092993" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1225031092283092993</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1225030573900460033',
    created: 1580905023000,
    type: 'quote',
    text: 'Get this trending. It\'s like an ice bucket challenge for democracy.<br><br>Quoting: <a href="https://x.com/nainagradin/status/1224707020642770951" rel="noopener noreferrer" target="_blank">@nainagradin</a> <span class="status">1224707020642770951</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1225029455707439104',
    created: 1580904756000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> That\'s undeniably true, but we\'re also witnessing the Democratic Party doing everything they can do to ensure this nightmare continues another 4 years. They have no excuse if it comes to that. They can\'t even blame the other party this time.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1225023701676183552" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1225023701676183552</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1224878370178994176',
    created: 1580868735000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/KyleKulinski" rel="noopener noreferrer" target="_blank">@KyleKulinski</a> <a class="mention" href="https://x.com/michaelsayman" rel="noopener noreferrer" target="_blank">@michaelsayman</a> You\'re absolutely right. This is a distraction designed to slow down organizing. There will be a lot of this. Use organizing as the tool to fight back. Focus is the secret weapon.<br><br>In reply to: <a href="https://x.com/KyleKulinski/status/1224846482311872512" rel="noopener noreferrer" target="_blank">@KyleKulinski</a> <span class="status">1224846482311872512</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1224851285670649856',
    created: 1580862277000,
    type: 'quote',
    text: 'We intervene in the affairs of other countries (supposedly) because we don\'t think they\'re doing democracy right, and we\'re over here choosing a president using a coin toss by someone who\'s clearly never flipped a coin before.<br><br>Quoting: <a href="https://x.com/OldRowOfficial/status/1224732196897947649" rel="noopener noreferrer" target="_blank">@OldRowOfficial</a> <span class="status">1224732196897947649</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1224787145086234624',
    created: 1580846985000,
    type: 'reply',
    text: '@lizgeorge27 That\'s the attitude to have. Yes!<br><br>In reply to: @lizgeorge27 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1224778288016449536',
    created: 1580844873000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GeoffMiami" rel="noopener noreferrer" target="_blank">@GeoffMiami</a> I\'ll share a thought that keeps playing in my head. "Let\'s burn that anger and frustration to make votes." ;)<br><br>In reply to: <a href="https://x.com/GeoffMiami/status/1224760643695337472" rel="noopener noreferrer" target="_blank">@GeoffMiami</a> <span class="status">1224760643695337472</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1224758500678463488',
    created: 1580840156000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GeoffMiami" rel="noopener noreferrer" target="_blank">@GeoffMiami</a> As Nina said at the DNC in 2016, "Let\'s not give them the sa-tis-faction!"<br><br>In reply to: <a href="#1224757172959006721">1224757172959006721</a>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1224757172959006721',
    created: 1580839839000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GeoffMiami" rel="noopener noreferrer" target="_blank">@GeoffMiami</a> I\'m extremely grateful to you for making the trip to Iowa to knock on doors. I recognize this situation is far more frustrating for you given how much you\'ve done. I\'m just trying to be there for our family so your work pounding the pavement and knocking on doors can\'t be undone.<br><br>In reply to: <a href="#1224754795451281408">1224754795451281408</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1224754795451281408',
    created: 1580839272000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GeoffMiami" rel="noopener noreferrer" target="_blank">@GeoffMiami</a> I know you\'re frustrated. I am too. But I urge you not to promote that hashtag. It doesn\'t serve us well. We know we\'re taking on the entire establishment. They need us to be shaken so they can divide us. We must not fall for it. Let\'s stay focused on making us unstoppable.<br><br>In reply to: <a href="https://x.com/GeoffMiami/status/1224686720794398720" rel="noopener noreferrer" target="_blank">@GeoffMiami</a> <span class="status">1224686720794398720</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1224643444041113601',
    created: 1580812724000,
    type: 'post',
    text: 'I got tired if waiting around for the results of the election tonight, so I decided to be productive and wrapped up the last release candidate for Asciidoctor PDF 1.5.0. <a href="https://github.com/asciidoctor/asciidoctor-pdf/releases/tag/v1.5.0.rc.3" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-pdf/releases/tag/v1.5.0.rc.3</a>',
    likes: 29,
    retweets: 3
  },
  {
    id: '1224641434940133376',
    created: 1580812245000,
    type: 'reply',
    text: '@SeaSparks21 🤦<br><br>In reply to: @SeaSparks21 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1224641309874384896',
    created: 1580812215000,
    type: 'post',
    text: 'If you made the trip to #jfokus, and you\'re hanging around until Friday, it would be really cool if you stopped by the #FridaysForFuture climate strike at the Swedish Parliament House (where <a class="mention" href="https://x.com/GretaThunberg" rel="noopener noreferrer" target="_blank">@GretaThunberg</a> will be) to show your support. (any time from 08:00 to 15:00)',
    likes: 3,
    retweets: 1,
    tags: ['jfokus', 'fridaysforfuture']
  },
  {
    id: '1224635690274545664',
    created: 1580810875000,
    type: 'post',
    text: 'Can we have an US election without a vote counting controversy? Sigh. Of course not, because that\'s one of the problems we\'re trying to fix by voting. Frustrating.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1224466258948608000',
    created: 1580770480000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/acruiz" rel="noopener noreferrer" target="_blank">@acruiz</a> <a class="mention" href="https://x.com/jzb" rel="noopener noreferrer" target="_blank">@jzb</a> I support your right to be respectfully angry. (For the record, so am I).<br><br>In reply to: <a href="https://x.com/acruiz/status/1224465302811021312" rel="noopener noreferrer" target="_blank">@acruiz</a> <span class="status">1224465302811021312</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1224466139725484032',
    created: 1580770451000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/acruiz" rel="noopener noreferrer" target="_blank">@acruiz</a> <a class="mention" href="https://x.com/jzb" rel="noopener noreferrer" target="_blank">@jzb</a> Fair enough. I think we\'ve said what needs to be said.<br><br>In reply to: <a href="https://x.com/acruiz/status/1224464975722418176" rel="noopener noreferrer" target="_blank">@acruiz</a> <span class="status">1224464975722418176</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1224464707743932420',
    created: 1580770110000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/acruiz" rel="noopener noreferrer" target="_blank">@acruiz</a> <a class="mention" href="https://x.com/jzb" rel="noopener noreferrer" target="_blank">@jzb</a> Well, you are talking to them, because this is a public dialog. I\'m looking forwards, not backwards. I\'m not going to be pulled into looking backwards. We know the situation and we know that to win, we must organize. And that means listening and seeking to understand.<br><br>In reply to: <a href="https://x.com/acruiz/status/1224464289693609985" rel="noopener noreferrer" target="_blank">@acruiz</a> <span class="status">1224464289693609985</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1224463187795304448',
    created: 1580769748000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/acruiz" rel="noopener noreferrer" target="_blank">@acruiz</a> <a class="mention" href="https://x.com/jzb" rel="noopener noreferrer" target="_blank">@jzb</a> All I\'m saying is try the line "vote for my candidate you irresponsible, petulant person!" and tell me how many votes it gets. Families lost farms because of bad trade deals. Black parents &amp; children were murdered by police. We need to help them see why Dems solve these issues.<br><br>In reply to: <a href="https://x.com/acruiz/status/1224460653039181826" rel="noopener noreferrer" target="_blank">@acruiz</a> <span class="status">1224460653039181826</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1224459660960268288',
    created: 1580768907000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/acruiz" rel="noopener noreferrer" target="_blank">@acruiz</a> <a class="mention" href="https://x.com/jzb" rel="noopener noreferrer" target="_blank">@jzb</a> I don\'t think you hate them. I\'m telling you it fuels the hate calling people names. Try to understand them. That\'s how this political process works.<br><br>In reply to: <a href="https://x.com/acruiz/status/1224458995768926213" rel="noopener noreferrer" target="_blank">@acruiz</a> <span class="status">1224458995768926213</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1224457714639589377',
    created: 1580768443000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/acruiz" rel="noopener noreferrer" target="_blank">@acruiz</a> <a class="mention" href="https://x.com/jzb" rel="noopener noreferrer" target="_blank">@jzb</a> Calling voters irresponsible (and following on a thread that started out calling them petulant) is hateful and hurtful. It doesn\'t get us to where Trump is out of office, so cut it out. Choose love.<br><br>In reply to: <a href="https://x.com/acruiz/status/1224454688629608448" rel="noopener noreferrer" target="_blank">@acruiz</a> <span class="status">1224454688629608448</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1224452628404260864',
    created: 1580767230000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/acruiz" rel="noopener noreferrer" target="_blank">@acruiz</a> <a class="mention" href="https://x.com/jzb" rel="noopener noreferrer" target="_blank">@jzb</a> You\'re the one who is choosing words of hate and spite, so I agree that sort of unity means nothing. I\'m not interested in this conversation anymore because I\'m focused on reaching people &amp; encouraging them to join together to defeat Trump. It\'s a movement of love and compassion.<br><br>In reply to: <a href="https://x.com/acruiz/status/1224451350479568897" rel="noopener noreferrer" target="_blank">@acruiz</a> <span class="status">1224451350479568897</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1224450845518921728',
    created: 1580766805000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/acruiz" rel="noopener noreferrer" target="_blank">@acruiz</a> <a class="mention" href="https://x.com/jzb" rel="noopener noreferrer" target="_blank">@jzb</a> We can see who really cares about unity.<br><br>In reply to: <a href="https://x.com/acruiz/status/1224445030657449984" rel="noopener noreferrer" target="_blank">@acruiz</a> <span class="status">1224445030657449984</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1224437141385560064',
    created: 1580763538000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sunrisemvmt" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a> <a class="mention" href="https://x.com/DataProgress" rel="noopener noreferrer" target="_blank">@DataProgress</a> <a class="mention" href="https://x.com/SunriseIowa" rel="noopener noreferrer" target="_blank">@SunriseIowa</a> <a class="mention" href="https://x.com/CCIAction" rel="noopener noreferrer" target="_blank">@CCIAction</a> That\'s crazy good news!<br><br>In reply to: <a href="https://x.com/sunrisemvmt/status/1224435471171743745" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a> <span class="status">1224435471171743745</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1224433407054110721',
    created: 1580762647000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jzb" rel="noopener noreferrer" target="_blank">@jzb</a> People are hurting and our first priority should be to listen and find out why. Not attack them.<br><br>In reply to: <a href="#1224433022918770689">1224433022918770689</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1224433022918770689',
    created: 1580762556000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jzb" rel="noopener noreferrer" target="_blank">@jzb</a> Well, if you blame voters, you have made your choice to sow seeds to get Trump reelected. Because that isn\'t unity.<br><br>In reply to: <a href="https://x.com/jzb/status/1224431724920131586" rel="noopener noreferrer" target="_blank">@jzb</a> <span class="status">1224431724920131586</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1224432711722356737',
    created: 1580762481000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jzb" rel="noopener noreferrer" target="_blank">@jzb</a> For the record, I\'ll vote to get Trump out of office, so don\'t include me in that divisive poll you originally cited. But I have a right to be mad when the DNC changes it\'s rules for a billionaire. I can do both.<br><br>In reply to: <a href="#1224432339201089537">1224432339201089537</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1224432339201089537',
    created: 1580762393000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jzb" rel="noopener noreferrer" target="_blank">@jzb</a> And if we\'re going to cite polls, Bernie beats Trump in every national poll since 2016. So do you really want to beat Trump? If so, why ignore all those polls?<br><br>In reply to: <a href="#1224431870286295041">1224431870286295041</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1224431870286295041',
    created: 1580762281000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jzb" rel="noopener noreferrer" target="_blank">@jzb</a> The fellow Bernie supporters I met (the actual people, not the online accounts they want you to think represent us) are some of the warmest, most down to earth, committed voters I\'ve ever met. They\'re anything but petulant. They\'re teachers. Moms. Nurses. Union leaders. Students.<br><br>In reply to: <a href="#1224430185400766464">1224430185400766464</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1224430185400766464',
    created: 1580761879000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jzb" rel="noopener noreferrer" target="_blank">@jzb</a> You can\'t win on entitlement. It takes energy. It takes being on the ground. And yes, it takes unity. And unity is about finding common ground and fighting together. It\'s the media who tries to divide us.<br><br>In reply to: <a href="#1224429066721230849">1224429066721230849</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1224429066721230849',
    created: 1580761612000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jzb" rel="noopener noreferrer" target="_blank">@jzb</a> You\'re feeding controversy where there isn\'t any. Clinton didn\'t go to states that mattered to win the electoral college and voters didn\'t turn out for that reason too. Blaming voters gets you more of that, and it isn\'t petulance.<br><br>In reply to: <a href="#1224428546421968896">1224428546421968896</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1224428546421968896',
    created: 1580761488000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jzb" rel="noopener noreferrer" target="_blank">@jzb</a> Again, the media is feeding you stories. Let\'s go with facts. Clinton received more votes. It was the electoral college to blame (and not the first or even second time). It was also Republican-led voter suppression, which has been proven (far less people could vote).<br><br>In reply to: <a href="https://x.com/jzb/status/1224426219434860545" rel="noopener noreferrer" target="_blank">@jzb</a> <span class="status">1224426219434860545</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1224425500057063424',
    created: 1580760762000,
    type: 'reply',
    text: '@SeaSparks21 I\'m trying to remember to breathe.<br><br>In reply to: @SeaSparks21 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1224422628179378177',
    created: 1580760077000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jzb" rel="noopener noreferrer" target="_blank">@jzb</a> This is what the media wants you to believe. We\'re in a primary right now. The focus is on putting forward the best candidate to win in Nov. We can disagree on who that candidate is without saying "my candidate\'s supporters are more mature than yours." That\'s not a path to unity.<br><br>In reply to: <a href="https://x.com/jzb/status/1224394992623456259" rel="noopener noreferrer" target="_blank">@jzb</a> <span class="status">1224394992623456259</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1224417989740703751',
    created: 1580758971000,
    type: 'post',
    text: 'I hope that the #IowaCaucus goers see what I see in #Bernie2020 (and are immune to the misinformation). Either way, the work continues. Because this is about all of us.',
    likes: 0,
    retweets: 0,
    tags: ['iowacaucus', 'bernie2020']
  },
  {
    id: '1224302132519567360',
    created: 1580731349000,
    type: 'quote',
    text: '"You can tell how good I feel by how nervous the establishment is getting." 🤣<br><br>Quoting: <a href="https://x.com/BernieSanders/status/1223985287489040384" rel="noopener noreferrer" target="_blank">@BernieSanders</a> <span class="status">1223985287489040384</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1224296346963722241',
    created: 1580729970000,
    type: 'post',
    text: 'I\'m working on one more issue, then I\'ll make a final RC for Asciidoctor PDF 1.5.0. The final release will follow shortly thereafter.',
    likes: 16,
    retweets: 1
  },
  {
    id: '1224276846054801408',
    created: 1580725320000,
    type: 'post',
    text: 'Wake Me When It\'s Over by The Cranberries. Yes.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1224100260735152128',
    created: 1580683219000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lhawthorn" rel="noopener noreferrer" target="_blank">@lhawthorn</a> I had never done it before, but I can tell you once I started cooking regularly, it has helped tremendously with my anxiety. I also listen to music while I do it, which probably helps a lot.<br><br>In reply to: <a href="https://x.com/lhawthorn/status/1223998564608638978" rel="noopener noreferrer" target="_blank">@lhawthorn</a> <span class="status">1223998564608638978</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1223860345917149185',
    created: 1580626019000,
    type: 'quote',
    text: 'I\'m so glad to see Winterwatch placing the spotlight on the climate crisis and the devastating impact human activity is having on nature. (I\'m also glad I still have 4 more episodes of nature to observe!)<br><br>Quoting: <a href="https://x.com/BBCSpringwatch/status/1223362042423336961" rel="noopener noreferrer" target="_blank">@BBCSpringwatch</a> <span class="status">1223362042423336961</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1223828712807981056',
    created: 1580618477000,
    type: 'post',
    text: 'While at the taproom today, I discovered The Dead South. Just listened to their entire catalog. Amazing band. They burn the floor with every song they play. I need to see them now. Come to Denver!',
    likes: 2,
    retweets: 0
  },
  {
    id: '1223799445914914818',
    created: 1580611499000,
    type: 'quote',
    text: 'Wow. Now that\'s what you call grassroots.<br><br>Quoting: <a href="https://x.com/jackcalifano/status/1223774023907188737" rel="noopener noreferrer" target="_blank">@jackcalifano</a> <span class="status">1223774023907188737</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1223721379763277824',
    created: 1580592887000,
    type: 'post',
    text: 'Just had roasted fennel. I\'m sorry I\'ve ignored this plant my whole life. Seconds please!',
    likes: 3,
    retweets: 0
  },
  {
    id: '1223719802797207555',
    created: 1580592511000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/svenpet" rel="noopener noreferrer" target="_blank">@svenpet</a> <a class="mention" href="https://x.com/matkar" rel="noopener noreferrer" target="_blank">@matkar</a> Pro tip: In autopilot, hang your arm on the steering wheel using a sloth grip and it will just keep driving. (You\'ll still be able to feel the jerk if it needs you).<br><br>In reply to: <a href="#1223719063949643782">1223719063949643782</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1223719063949643782',
    created: 1580592335000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/svenpet" rel="noopener noreferrer" target="_blank">@svenpet</a> <a class="mention" href="https://x.com/matkar" rel="noopener noreferrer" target="_blank">@matkar</a> From the time you approach the car to the sing along while driving, they\'ve thought about the driver and the driving experience. And you get to avoid petrol stations. I logged 4,000 miles in one and loved it.<br><br>In reply to: <a href="https://x.com/svenpet/status/1223717425918685186" rel="noopener noreferrer" target="_blank">@svenpet</a> <span class="status">1223717425918685186</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1223718292877193216',
    created: 1580592151000,
    type: 'quote',
    text: 'You love to hear it. Divestment works. Keep pressing.<br><br>Quoting: <a href="https://x.com/NaomiAKlein/status/1223276415942037506" rel="noopener noreferrer" target="_blank">@NaomiAKlein</a> <span class="status">1223276415942037506</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1223716926599450624',
    created: 1580591825000,
    type: 'post',
    text: 'Lone Tree is on a blues kick right now and I\'m here for this.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1223713049267314688',
    created: 1580590901000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/matkar" rel="noopener noreferrer" target="_blank">@matkar</a> <a class="mention" href="https://x.com/svenpet" rel="noopener noreferrer" target="_blank">@svenpet</a> Like no car I\'ve ever driven. Really something special.<br><br>In reply to: <a href="https://x.com/matkar/status/1223709762740740096" rel="noopener noreferrer" target="_blank">@matkar</a> <span class="status">1223709762740740096</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1223699605507866625',
    created: 1580587695000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> <a class="mention" href="https://x.com/rotnroll666" rel="noopener noreferrer" target="_blank">@rotnroll666</a> @AndyGeeDe Hahahaha. No.<br><br>In reply to: <a href="https://x.com/brunoborges/status/1223638707925729281" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1223638707925729281</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1223697396942561280',
    created: 1580587169000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fanf42" rel="noopener noreferrer" target="_blank">@fanf42</a> <a class="mention" href="https://x.com/CageJulia" rel="noopener noreferrer" target="_blank">@CageJulia</a> I have not yet, though a student of those who likely have. Added to my reading list.<br><br>In reply to: <a href="https://x.com/fanf42/status/1223606900643827718" rel="noopener noreferrer" target="_blank">@fanf42</a> <span class="status">1223606900643827718</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1223566555918716928',
    created: 1580555974000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jaredmorgs" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <a class="mention" href="https://x.com/DaveDri" rel="noopener noreferrer" target="_blank">@DaveDri</a> 💯<br><br>In reply to: <a href="https://x.com/jaredmorgs/status/1223561344416276480" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <span class="status">1223561344416276480</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1223566490537873410',
    created: 1580555958000,
    type: 'reply',
    text: '@AndyGeeDe <a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> No, arguing with Bruno about YAML. That\'s where the time goes.<br><br>In reply to: @AndyGeeDe <span class="status">&lt;deleted&gt;</span>',
    likes: 6,
    retweets: 0
  },
  {
    id: '1223553904064921600',
    created: 1580552957000,
    type: 'quote',
    text: 'THE<br>TIME<br>IS<br>NOW<br>THERE<br>ARE<br>MORE<br>OF<br>US<br>WE<br>ARE<br>STRONGER<br>WE<br>WILL<br>WAIT<br>NO<br>LONGER<br><br>Quoting: <a href="https://x.com/GNproductions01/status/1223228891491459072" rel="noopener noreferrer" target="_blank">@GNproductions01</a> <span class="status">1223228891491459072</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1223536254240690181',
    created: 1580548749000,
    type: 'post',
    text: 'I\'m adding The Cranberries to my all-time favorite (modern rock) band list, probably at #3 behind Garbage and Soundgarden.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1223511334483972098',
    created: 1580542808000,
    type: 'reply',
    text: '@ChittoorMadhvi <a class="mention" href="https://x.com/Singer4BoCo" rel="noopener noreferrer" target="_blank">@Singer4BoCo</a> <a class="mention" href="https://x.com/carolinegleich" rel="noopener noreferrer" target="_blank">@carolinegleich</a> <a class="mention" href="https://x.com/katieboue" rel="noopener noreferrer" target="_blank">@katieboue</a> <a class="mention" href="https://x.com/350Colorado" rel="noopener noreferrer" target="_blank">@350Colorado</a> Your voice is so powerful and your message is so clear that it\'s igniting the flames of change. Thank you again and again.<br><br>In reply to: <a href="https://x.com/Madhvi4EE/status/1223440822084997120" rel="noopener noreferrer" target="_blank">@Madhvi4EE</a> <span class="status">1223440822084997120</span>',
    likes: 4,
    retweets: 1
  },
  {
    id: '1223334283072299010',
    created: 1580500596000,
    type: 'post',
    text: 'In case it\'s unclear, by "one vote per election", I\'m not referring to voter fraud, which is a myth. I\'m referring to big money being used to swing elections and the electoral college giving more weight to votes in certain states. Both devalue the vote of the individual.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1223324424289390594',
    created: 1580498245000,
    type: 'post',
    text: 'By "actual revolution", I\'m referring to a violent one. Think the French Revolution. I\'m not saying we don\'t want revolutionary changes. We absolutely do. My hope is that we can get there though nonviolent action and the ballot box.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1223223932448231424',
    created: 1580474286000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> And we\'re just getting warmed up. Still plenty of places to take it.<br><br>In reply to: <a href="https://x.com/settermjd/status/1223220417697067009" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1223220417697067009</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1223190340057714688',
    created: 1580466277000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> Nah. That\'s actually where I got my start using lightweight markup languages. It was when I started seeking out better alternatives that I discovered AsciiDoc. Textile is too HTML-centric, and not in a good way.<br><br>In reply to: <a href="https://x.com/settermjd/status/1223184146169835520" rel="noopener noreferrer" target="_blank">@settermjd</a> <span class="status">1223184146169835520</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1223144794207092736',
    created: 1580455418000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mihn" rel="noopener noreferrer" target="_blank">@mihn</a> OMG, I just realized I can add tahini drizzle on top. That\'s not in the book. A whole new world awaits!<br><br>In reply to: <a href="#1223144634819330055">1223144634819330055</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1223144634819330055',
    created: 1580455380000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mihn" rel="noopener noreferrer" target="_blank">@mihn</a> This one here: <a href="https://www.thugkitchen.com/recipes/lemony-red-lentil-soup-w-tahini-drizzle" rel="noopener noreferrer" target="_blank">www.thugkitchen.com/recipes/lemony-red-lentil-soup-w-tahini-drizzle</a><br><br>In reply to: <a href="https://x.com/mihn/status/1223143183208648707" rel="noopener noreferrer" target="_blank">@mihn</a> <span class="status">1223143183208648707</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1223144243562074112',
    created: 1580455287000,
    type: 'quote',
    text: 'I\'m now in love with Chunk and Nibbles. 🍅🥕<br><br>Quoting: <a href="https://x.com/dodo/status/1223093546887958528" rel="noopener noreferrer" target="_blank">@dodo</a> <span class="status">1223093546887958528</span>',
    likes: 13,
    retweets: 2
  },
  {
    id: '1223140007419842560',
    created: 1580454277000,
    type: 'post',
    text: 'If you had told me years ago that one of my favorite things to eat would be lentil soup, I would have 🤣. But now I\'m all 😍. Mmmmm mmmm good.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1223086358924652544',
    created: 1580441486000,
    type: 'post',
    text: 'I\'ve made my first trek into GitHub Actions. We\'re using it to run Asciidoctor PDF tests on Windows (CRuby 2.3 to 2.7 and JRuby). It has impressive speed.',
    likes: 5,
    retweets: 0
  },
  {
    id: '1223015211860295681',
    created: 1580424523000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_codewriter" rel="noopener noreferrer" target="_blank">@_codewriter</a> <a class="mention" href="https://x.com/briandominick" rel="noopener noreferrer" target="_blank">@briandominick</a> As much as I wish I didn\'t have to admit it, I agree the chances are slim. But I\'m willing to hold out hope that it\'s the tipping point we need it to be. At the very least, it\'s worth a shot (a shot we missed and/or was stolen in 2016).<br><br>In reply to: <a href="https://x.com/_codewriter/status/1223013976600125442" rel="noopener noreferrer" target="_blank">@_codewriter</a> <span class="status">1223013976600125442</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1223013454040031232',
    created: 1580424104000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/briandominick" rel="noopener noreferrer" target="_blank">@briandominick</a> Yep. That could be why I feel so conflicted. We are so far from an ideal place, it can be hard to even know where to begin.<br><br>In reply to: @BrianDominick <span class="status">&lt;deleted&gt;</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1223012932868378625',
    created: 1580423980000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/briandominick" rel="noopener noreferrer" target="_blank">@briandominick</a> Oh yeah!<br><br>In reply to: @BrianDominick <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1223012857886826496',
    created: 1580423962000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/briandominick" rel="noopener noreferrer" target="_blank">@briandominick</a> Thanks for being patient with me. Trust me, I did not intend to lecture you. If anything, you were playing the roll of teacher, challenging me to make my points for a debate.<br><br>In reply to: @BrianDominick <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1223011341289394176',
    created: 1580423600000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/briandominick" rel="noopener noreferrer" target="_blank">@briandominick</a> To be clear, I fear violence, not necessarily revolution. A revolution of some sort is definitely needed. I just 🙏 it\'s the peaceful kind (being mindful of that fact that the current system already subjects many people to violence).<br><br>In reply to: @BrianDominick <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1223010312258875392',
    created: 1580423355000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/briandominick" rel="noopener noreferrer" target="_blank">@briandominick</a> Cool. And I\'d enjoy having that conversation. As I said, I\'d like to have holes punched in my understanding. It wouldn\'t be the first time ;)<br><br>In reply to: @BrianDominick <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1223009818530574336',
    created: 1580423237000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/briandominick" rel="noopener noreferrer" target="_blank">@briandominick</a> And I totally recognize that Bernie is not as left as we probably both want him to be. But we do need a viable movement or we end up with nothing. That\'s not an argument for the center. It\'s saying we need to crack open the flood gates so we can get the water rushing.<br><br>In reply to: <a href="#1223009032660631552">1223009032660631552</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1223009032660631552',
    created: 1580423050000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/briandominick" rel="noopener noreferrer" target="_blank">@briandominick</a> I know you\'re strong left. Heck, you were one of my inspirations to go vegan (hence the question about it at dinner). It\'s more that I don\'t think of myself as right of left. So it\'s more about where I am. But maybe I\'m still figuring it all out.<br><br>In reply to: @BrianDominick <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1223008553985658880',
    created: 1580422936000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/briandominick" rel="noopener noreferrer" target="_blank">@briandominick</a> I don\'t think this is really about a competition of who has been been progressive longer (something I\'d clearly lose, at least in so far as positions I\'ve communicated publicly). I think it\'s about a sharing our understanding so we can be better allies for substantive change.<br><br>In reply to: @BrianDominick <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1223006978844160000',
    created: 1580422560000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/briandominick" rel="noopener noreferrer" target="_blank">@briandominick</a> Again, I didn\'t intend to lecture. Only as an opportunity to put my words where my retweets were, so to speak. Often times, communicating is how we solidify our understanding, and I saw this as a chance to do that. Because I do struggle to find the words for stuff like this.<br><br>In reply to: @BrianDominick <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1223006230764871681',
    created: 1580422382000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_codewriter" rel="noopener noreferrer" target="_blank">@_codewriter</a> <a class="mention" href="https://x.com/briandominick" rel="noopener noreferrer" target="_blank">@briandominick</a> Understood.<br><br>In reply to: <a href="https://x.com/_codewriter/status/1223005842255089667" rel="noopener noreferrer" target="_blank">@_codewriter</a> <span class="status">1223005842255089667</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1223006177803419648',
    created: 1580422369000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_codewriter" rel="noopener noreferrer" target="_blank">@_codewriter</a> I seek to learn, so I\'m happy to be corrected. (If you want to switch accounts now, please don\'t hesitate to do so).<br><br>In reply to: <a href="#1223005867324260352">1223005867324260352</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1223005867324260352',
    created: 1580422295000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_codewriter" rel="noopener noreferrer" target="_blank">@_codewriter</a> I\'m surprised you believe I\'m to your right. Maybe that\'s an indication that I\'m not communicating something clearly or correctly, because I don\'t think I am (at least not substantially).<br><br>In reply to: <a href="#1223005516378411009">1223005516378411009</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1223005516378411009',
    created: 1580422212000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_codewriter" rel="noopener noreferrer" target="_blank">@_codewriter</a> I *definitely* didn\'t intend for you to read it as patronizing. I saw it as an opportunity to be able to articulate what the political revolution really means (to me). It was a fun an interesting challenge, to be honest.<br><br>In reply to: <a href="https://x.com/_codewriter/status/1222997940442214400" rel="noopener noreferrer" target="_blank">@_codewriter</a> <span class="status">1222997940442214400</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1222987090339622912',
    created: 1580417818000,
    type: 'post',
    text: 'But none of this is going to happen short of a political revolution. The people must show their strength by getting involved, showing up, and letting our voices be heard. As Bernie says, "Not Me, Us."',
    likes: 0,
    retweets: 0
  },
  {
    id: '1222987089374920704',
    created: 1580417818000,
    type: 'post',
    text: 'Getting back to the election, we must restore voting rights. Every person in this country of voting age should be allowed to cast one vote per election, period. No other barriers. Doing so should be easy, accessible, and counted. Suppressing the vote should be a severe crime.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1222987088414441472',
    created: 1580417818000,
    type: 'post',
    text: 'At the end of the day, we can agree that all lives do, in fact, matter. The problem is, right now, some lives matter more than most, most notably while and male. Being white and male, I say without hesitation this situation needs to change.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1222987087474937859',
    created: 1580417818000,
    type: 'post',
    text: 'Another is ending institutionalized racism and other forms of bigotry. We\'ll live in a country where black lives matter, women lives matter, LGBTQA lives matter, and we should all have equal standing and opportunity under the law.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1222987086464045057',
    created: 1580417818000,
    type: 'post',
    text: 'Yet another is to cancel student debt and make education tuition free. While the latter is a policy change, the first is a structural one to free a generation from a life of debt. (And something we used to have, btw).',
    likes: 0,
    retweets: 0
  },
  {
    id: '1222987085495205889',
    created: 1580417817000,
    type: 'post',
    text: 'Another is to join the rest of the industrialized world by guaranteeing health care to every person in this country so it lives up to its promise of the right to (a healthy and respectable) life.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1222987084551442432',
    created: 1580417817000,
    type: 'post',
    text: 'Given war is about oil, another is to transform our energy system by keeping fossil fuels in the ground and switching entirely to green energy post haste. Fossil fuels not only destroy the environment we depend on, but the money controls our political system &amp; drives us to war.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1222987083523821568',
    created: 1580417817000,
    type: 'post',
    text: 'Bernie is going to end endless war and the stranglehold of the military industrial complex. That\'s a big structural change because we live in the structure of interventionist wars. Our defense budget shows that we prioritize violent solutions over peace, diplomacy, and democracy.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1222987082546597888',
    created: 1580417817000,
    type: 'post',
    text: 'Another is legalizing marijuana nationally and expunging the records of those who have been targeted by unjust laws. We need to stop destroying lives, esp. in a way that disproportionately affects minorities, crowds prisons, and fuels the private prison industry.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1222987081527353344',
    created: 1580417816000,
    type: 'post',
    text: 'A key part of accomplishing that is to stop big money from controlling politics, which means ending Citizens United and switching to publicly funded elections. We wonder how we\'re going to pay for things, yet throw big 💰 at campaigns, which effectively burn it on media buys.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1222987080554336256',
    created: 1580417816000,
    type: 'post',
    text: 'The structural changes start with fixing the massive and unprecedented wealth inequality in this country, in which 3 people own more wealth then the bottom half. That is not the condition for a sustainable and thriving society.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1222987079392681984',
    created: 1580417816000,
    type: 'post',
    text: 'The people need to send a very clear message that we won\'t stand by and watch our country be torn apart and gutted. We will take back power and hold on to it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1222987078260019201',
    created: 1580417816000,
    type: 'post',
    text: 'A political revolution is about millions of people waking up and engaging in the political process. If we want to defeat Trump, and handily, we need record-breaking voter turnout (even while the vote is being suppressed).',
    likes: 0,
    retweets: 0
  },
  {
    id: '1222987077152755712',
    created: 1580417815000,
    type: 'post',
    text: 'The whole point of a political, peaceful revolution is to prevent an actual revolution from happening. Right now, many signs are pointing to us heading in the direction of a revolution, and that\'s scary. Key structural changes are necessary to defuse it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1222987074254491648',
    created: 1580417815000,
    type: 'quote',
    text: 'I\'ll start by saying that Bernie has written two books and countless speeches on the subject, so I defer to his writing as the authority. That said, I\'ll give it my best shot. #Bernie2020<br><br>Quoting: <a href="https://x.com/_codewriter/status/1222871561637638144" rel="noopener noreferrer" target="_blank">@_codewriter</a> <span class="status">1222871561637638144</span>',
    likes: 1,
    retweets: 0,
    tags: ['bernie2020']
  },
  {
    id: '1222847737978163200',
    created: 1580384594000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CedricChampeau" rel="noopener noreferrer" target="_blank">@CedricChampeau</a> <a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> You my brother in arms ;)<br><br>In reply to: <a href="https://x.com/CedricChampeau/status/1222847196430839815" rel="noopener noreferrer" target="_blank">@CedricChampeau</a> <span class="status">1222847196430839815</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1222845728319426560',
    created: 1580384115000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CedricChampeau" rel="noopener noreferrer" target="_blank">@CedricChampeau</a> <a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> Same here. Do you use the left or right alt key? I would think the left alt key is already used for other things.<br><br>In reply to: <a href="https://x.com/CedricChampeau/status/1222839152661712896" rel="noopener noreferrer" target="_blank">@CedricChampeau</a> <span class="status">1222839152661712896</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1222843674129649664',
    created: 1580383625000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sebi2706" rel="noopener noreferrer" target="_blank">@sebi2706</a> <a class="mention" href="https://x.com/emmanuelbernard" rel="noopener noreferrer" target="_blank">@emmanuelbernard</a> I use the right Alt. So to enter é, I type RAlt+\'+e. Linux comes with most of the composes defined. You just need to define your compose (i.e., initiator) key. See /usr/share/X11/locale/en_US.UTF-8/Compose for a list of combos. You can define your own in ~/.XCompose.<br><br>In reply to: <a href="https://x.com/sebi2706/status/1222840153452044288" rel="noopener noreferrer" target="_blank">@sebi2706</a> <span class="status">1222840153452044288</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1222797628548608000',
    created: 1580372647000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ryanbigg" rel="noopener noreferrer" target="_blank">@ryanbigg</a> Is there an open proposal for adding that keyword?<br><br>In reply to: <a href="#1222780229963280385">1222780229963280385</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1222780229963280385',
    created: 1580368499000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ryanbigg" rel="noopener noreferrer" target="_blank">@ryanbigg</a> Oh, that\'s nice. I\'ve always wanted to compress that, but the module keyword interprets it differently. This finally gives me a way.<br><br>In reply to: <a href="https://x.com/ryanbigg/status/1222768422863294465" rel="noopener noreferrer" target="_blank">@ryanbigg</a> <span class="status">1222768422863294465</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1222659619522326533',
    created: 1580339743000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <a class="mention" href="https://x.com/AndrewYang" rel="noopener noreferrer" target="_blank">@AndrewYang</a> Bernie is always being attacked by the establishment. That\'s nothing new. They\'re also ineffective. (If Yang were in that position, he would be attacked by the establishment too, so the attacks don\'t give Yang an advantage here).<br><br>In reply to: <a href="https://x.com/adamcoomes/status/1222599079743643655" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <span class="status">1222599079743643655</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1222129091387650051',
    created: 1580213256000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/grdryn" rel="noopener noreferrer" target="_blank">@grdryn</a> I just don\'t understand why the key combo was removed since the toolbar still exists. (If the toolbar isn\'t important, the whole thing should just be removed). While I don\'t use it often, I use it when I\'m following scripts or for sites that are hard to find.<br><br>In reply to: <a href="https://x.com/grdryn/status/1222116488984846337" rel="noopener noreferrer" target="_blank">@grdryn</a> <span class="status">1222116488984846337</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1222111214903255040',
    created: 1580208994000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/StatusCoup" rel="noopener noreferrer" target="_blank">@StatusCoup</a> <a class="mention" href="https://x.com/ninaturner" rel="noopener noreferrer" target="_blank">@ninaturner</a> <a class="mention" href="https://x.com/DNC" rel="noopener noreferrer" target="_blank">@DNC</a> And by the way, even though I follow <a class="mention" href="https://x.com/StatusCoup" rel="noopener noreferrer" target="_blank">@StatusCoup</a>, look at what Twitter shows me when I visit the account. You aren\'t kidding. The suppression is real. This interview must be really making an impact.<br><br>In reply to: <a href="https://x.com/StatusCoup/status/1221857305047814146" rel="noopener noreferrer" target="_blank">@StatusCoup</a> <span class="status">1221857305047814146</span>',
    photos: ['<div class="item"><img class="photo" src="media/1222111214903255040.jpg"></div>'],
    likes: 2,
    retweets: 0
  },
  {
    id: '1222110822563864581',
    created: 1580208900000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JordanChariton" rel="noopener noreferrer" target="_blank">@JordanChariton</a> <a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a> <a class="mention" href="https://x.com/ninaturner" rel="noopener noreferrer" target="_blank">@ninaturner</a> Wow, that was a ringing endorsement from Nina. You should be VERY proud of the work you\'ve done, and the recognition you\'re receiving for that work. Great interview.<br><br>In reply to: <a href="https://x.com/JordanChariton/status/1221877063549964289" rel="noopener noreferrer" target="_blank">@JordanChariton</a> <span class="status">1221877063549964289</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1222092447993303041',
    created: 1580204519000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/benignbala" rel="noopener noreferrer" target="_blank">@benignbala</a> The irony is that the person\'s point is that no one will use Asciidoctor PDF because the theme is ugly. Yet, that person seems to be the only one who thinks that way.<br><br>In reply to: <a href="https://x.com/benignbala/status/1222084792411459584" rel="noopener noreferrer" target="_blank">@benignbala</a> <span class="status">1222084792411459584</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221927032067522560',
    created: 1580165081000,
    type: 'post',
    text: 'The closest suitable replacement I found is <a href="https://addons.mozilla.org/en-US/firefox/addon/popup-bookmarks" rel="noopener noreferrer" target="_blank">addons.mozilla.org/en-US/firefox/addon/popup-bookmarks</a>, but it\'s not really the same.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221926865499123712',
    created: 1580165041000,
    type: 'post',
    text: 'Is there really no key combo in Firefox to toggle the bookmarks toolbar? That\'s like a basic feature of any web browser that has been around since forever.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221620679587422209',
    created: 1580092041000,
    type: 'post',
    text: 'I had a bunch of stickers sitting around that I couldn\'t figure out what to do with. Then it dawn on me. Art project!',
    photos: ['<div class="item"><img class="photo" src="media/1221620679587422209.jpg"></div>'],
    likes: 1,
    retweets: 0
  },
  {
    id: '1221620265471209472',
    created: 1580091942000,
    type: 'post',
    text: 'Turning over the lead of Asciidoctor EPUB3 is the best thing I ever did for that project. It went from idle to on fire. Asciidoctor relies on its many leaders and the communities they build. It always has.',
    likes: 24,
    retweets: 3
  },
  {
    id: '1221618429091696643',
    created: 1580091504000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/michaelsayman" rel="noopener noreferrer" target="_blank">@michaelsayman</a> My recommendation for all supporters is to do what I would do when I was a competition swimmer. Go harder!<br><br>In reply to: <a href="https://x.com/michaelsayman/status/1221614189610844160" rel="noopener noreferrer" target="_blank">@michaelsayman</a> <span class="status">1221614189610844160</span>',
    likes: 5,
    retweets: 0
  },
  {
    id: '1221596163507355649',
    created: 1580086196000,
    type: 'post',
    text: 'On a day like today, in the words of Chris Stapleton, "I\'m just a traveler on this Earth. When I\'m gone, someone else will have to sing this song." #RIPKobeBryant',
    likes: 0,
    retweets: 0,
    tags: ['ripkobebryant']
  },
  {
    id: '1221581318179315715',
    created: 1580082656000,
    type: 'reply',
    text: '@brucefranksjr <a class="mention" href="https://x.com/MSNBC" rel="noopener noreferrer" target="_blank">@MSNBC</a> That is egregious!<br><br>In reply to: @brucefranksjr <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221580607492218880',
    created: 1580082487000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mxmkm" rel="noopener noreferrer" target="_blank">@mxmkm</a> Let it be at this point. If someone has no interest in participating, it\'s not worth engaging. I know I nudged you to file issues and test in the beginning and, in contrast, you rose to the challenge. 🙏🏆<br><br>In reply to: <a href="https://x.com/mxmkm/status/1221542462809890826" rel="noopener noreferrer" target="_blank">@mxmkm</a> <span class="status">1221542462809890826</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221579334093365249',
    created: 1580082183000,
    type: 'post',
    text: 'Chris Stapleton is a music genius. My latest favs by him are Them Stems and Midnight Train to Memphis. In the latter, his voice is reminiscent of Chris Cornell.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1221526354853629960',
    created: 1580069552000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> Fortunately GitHub Actions does!<br><br>In reply to: <a href="https://x.com/headius/status/1221525936610279424" rel="noopener noreferrer" target="_blank">@headius</a> <span class="status">1221525936610279424</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1221512510982877184',
    created: 1580066251000,
    type: 'post',
    text: 'Am I missing something, or does AppVeyor not offer JRuby as a Ruby runtime?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221412735079989250',
    created: 1580042463000,
    type: 'post',
    text: 'If you like Java and scripting, <a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> has just the thing for you. It\'s called j\'bang. <a href="https://github.com/maxandersen/jbang" rel="noopener noreferrer" target="_blank">github.com/maxandersen/jbang</a>',
    likes: 15,
    retweets: 9
  },
  {
    id: '1221410923778199552',
    created: 1580042031000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/indianatwork" rel="noopener noreferrer" target="_blank">@indianatwork</a> Certainly did.<br><br>In reply to: <a href="https://x.com/indianatwork/status/1221409292273836034" rel="noopener noreferrer" target="_blank">@indianatwork</a> <span class="status">1221409292273836034</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221410796208394240',
    created: 1580042001000,
    type: 'post',
    text: 'GitHub Actions should not be enabling core.autocrlf by default. No other CI server I know of does this. <a href="https://github.community/t5/GitHub-Actions/git-config-core-autocrlf-should-default-to-false/m-p/30445" rel="noopener noreferrer" target="_blank">github.community/t5/GitHub-Actions/git-config-core-autocrlf-should-default-to-false/m-p/30445</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1221406969216417793',
    created: 1580041088000,
    type: 'post',
    text: 'I received a letter from the insurance company encouraging me to sue someone for my fall. What kind of insane world do we live in?',
    likes: 4,
    retweets: 0
  },
  {
    id: '1221405886129639424',
    created: 1580040830000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> I just want to document it as a requirement that you must disable to develop on the project.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1221400435837284353" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1221400435837284353</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221393779908734976',
    created: 1580037944000,
    type: 'post',
    text: 'Am I wrong for refusing to put a .gitattributes file in my projects to undo the devilish core.autocrlf setting in Windows? It feels all kinds of wrong to me.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221390307117256705',
    created: 1580037116000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sativaacidni" rel="noopener noreferrer" target="_blank">@sativaacidni</a> <a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> @gigaman824 <a class="mention" href="https://x.com/AndrewYang" rel="noopener noreferrer" target="_blank">@AndrewYang</a> I also believe in Bernie because no matter what we aim to accomplish, it\'s going to take a political revolution to do it. And that\'s exactly what Bernie is successfully doing. He\'s successful because the ideas he\'s running on are so universally popular.<br><br>In reply to: <a href="#1221389233950052357">1221389233950052357</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221389233950052357',
    created: 1580036860000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sativaacidni" rel="noopener noreferrer" target="_blank">@sativaacidni</a> <a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> @gigaman824 <a class="mention" href="https://x.com/AndrewYang" rel="noopener noreferrer" target="_blank">@AndrewYang</a> Because it\'s already worked in this country\'s history, and it works in nearly every major country on Earth. UBI by itself borderlines on a libertarian idea. You can\'t just solve inequality and injustice with a check. (But a check / program can be a vital tool in the solution).<br><br>In reply to: <a href="https://x.com/sativaacidni/status/1221294080916959232" rel="noopener noreferrer" target="_blank">@sativaacidni</a> <span class="status">1221294080916959232</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221388337795059723',
    created: 1580036646000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sativaacidni" rel="noopener noreferrer" target="_blank">@sativaacidni</a> <a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> @gigaman824 <a class="mention" href="https://x.com/AndrewYang" rel="noopener noreferrer" target="_blank">@AndrewYang</a> We\'re not disagreeing on that. I have been studying UBI and I think it shows promise. My main concern is that it will come up short if we don\'t also have universal programs (e.g., Medicare For All). Otherwise, all the money flows right back to barely-regulated corporations.<br><br>In reply to: <a href="https://x.com/sativaacidni/status/1221293812720590849" rel="noopener noreferrer" target="_blank">@sativaacidni</a> <span class="status">1221293812720590849</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221290873578610689',
    created: 1580013409000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sativaacidni" rel="noopener noreferrer" target="_blank">@sativaacidni</a> <a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> @gigaman824 <a class="mention" href="https://x.com/AndrewYang" rel="noopener noreferrer" target="_blank">@AndrewYang</a> I\'m not discounting a VAT outright. It\'s one of the tools in the portfolio to consider. But it can\'t be the only one. It\'s not a panacea. We have to break up the big money stockpiles to get out of this extreme wealth inequality.<br><br>In reply to: <a href="https://x.com/sativaacidni/status/1221283720407265280" rel="noopener noreferrer" target="_blank">@sativaacidni</a> <span class="status">1221283720407265280</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221290419121557504',
    created: 1580013301000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sativaacidni" rel="noopener noreferrer" target="_blank">@sativaacidni</a> <a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> @gigaman824 <a class="mention" href="https://x.com/AndrewYang" rel="noopener noreferrer" target="_blank">@AndrewYang</a> Rich people spend very little money (in ways that we need). That\'s part of the problem. They concentrate wealth, then use it in eclectic ways that don\'t go back into the system. The middle class, in contrast, puts almost 100% of the money back into the pool.<br><br>In reply to: <a href="https://x.com/sativaacidni/status/1221283401518526466" rel="noopener noreferrer" target="_blank">@sativaacidni</a> <span class="status">1221283401518526466</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221289863120482304',
    created: 1580013168000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sativaacidni" rel="noopener noreferrer" target="_blank">@sativaacidni</a> <a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> @gigaman824 <a class="mention" href="https://x.com/AndrewYang" rel="noopener noreferrer" target="_blank">@AndrewYang</a> Please don\'t put words where there aren\'t any. Bernie does not hate this rich. Nothing in his speeches suggest that. He hates the system which allows extreme wealth concentration. He hates inequality. He hates that people are left behind. His message is one of love &amp; solidarity.<br><br>In reply to: <a href="https://x.com/sativaacidni/status/1221283074601865216" rel="noopener noreferrer" target="_blank">@sativaacidni</a> <span class="status">1221283074601865216</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221289332683657216',
    created: 1580013042000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <a class="mention" href="https://x.com/AndrewYang" rel="noopener noreferrer" target="_blank">@AndrewYang</a> And to say "if the rich pay for it" is rejecting the rule of law in this country. If it\'s a law, you pay it or you go to jail. That\'s how this works. (I know, we\'re so use to corporate crooks just walking around, but times are changing).<br><br>In reply to: <a href="#1221288960598523904">1221288960598523904</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221288960598523904',
    created: 1580012953000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <a class="mention" href="https://x.com/AndrewYang" rel="noopener noreferrer" target="_blank">@AndrewYang</a> I trust that the numbers Bernie identified as needed for each program are honest. (In case you hadn\'t noticed, he\'s damn good at raising money). That aside, he never said there\'s only one way to balance wealth. So why would you think he\'d reject other (progressive) pathways?<br><br>In reply to: <a href="https://x.com/adamcoomes/status/1221253942304223235" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <span class="status">1221253942304223235</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221250671418019841',
    created: 1580003824000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <a class="mention" href="https://x.com/AndrewYang" rel="noopener noreferrer" target="_blank">@AndrewYang</a> Also: <a href="https://berniesanders.com/issues/tax-extreme-wealth/" rel="noopener noreferrer" target="_blank">berniesanders.com/issues/tax-extreme-wealth/</a><br><br>In reply to: <a href="#1221250134371028992">1221250134371028992</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221250134371028992',
    created: 1580003696000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <a class="mention" href="https://x.com/AndrewYang" rel="noopener noreferrer" target="_blank">@AndrewYang</a> Citation: <a href="https://berniesanders.com/issues/income-inequality-tax-plan/" rel="noopener noreferrer" target="_blank">berniesanders.com/issues/income-inequality-tax-plan/</a><br><br>In reply to: <a href="https://x.com/adamcoomes/status/1221221989635018752" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <span class="status">1221221989635018752</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221244561072119808',
    created: 1580002367000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> @gigaman824 <a class="mention" href="https://x.com/AndrewYang" rel="noopener noreferrer" target="_blank">@AndrewYang</a> Political courage. That\'s what will stop it.<br><br>In reply to: <a href="https://x.com/adamcoomes/status/1221233690732265479" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <span class="status">1221233690732265479</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221236060186800128',
    created: 1580000340000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <a class="mention" href="https://x.com/AndrewYang" rel="noopener noreferrer" target="_blank">@AndrewYang</a> I\'m not sure how we\'re disagreeing, other than you\'re misrepresenting Bernie as impractical (something I\'m not doing of Yang). At its heart, a freedom dividend moves money from rich to poor. Yang also wants to source from wealth (talking about being shareholders in the country).<br><br>In reply to: <a href="#1221234846367748097">1221234846367748097</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221234846367748097',
    created: 1580000051000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <a class="mention" href="https://x.com/AndrewYang" rel="noopener noreferrer" target="_blank">@AndrewYang</a> With all due respect, this is a naive view of billionaires. They inherit most of what they have, and either contribute little to society or do it by exploiting workers. (Not all, of course, but enough of them that it\'s a problem).<br><br>In reply to: <a href="https://x.com/adamcoomes/status/1221233031702294528" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <span class="status">1221233031702294528</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221234441437057026',
    created: 1579999954000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> @gigaman824 <a class="mention" href="https://x.com/AndrewYang" rel="noopener noreferrer" target="_blank">@AndrewYang</a> I\'m not disagreeing that loopholes are the issue. They are. Look at all the top corporations like Amazon not paying a nickel in taxes. Clearly a problem. But there\'s wealth concentration elsewhere too. We need to be able to deal with both issues.<br><br>In reply to: <a href="https://x.com/adamcoomes/status/1221233690732265479" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <span class="status">1221233690732265479</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221232230049038338',
    created: 1579999427000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> @gigaman824 <a class="mention" href="https://x.com/AndrewYang" rel="noopener noreferrer" target="_blank">@AndrewYang</a> It\'s not the tax bracket. It\'s the wealth tax. Huge difference. It\'s taxing the wealth concentration at the very top, above millions and billions.<br><br>In reply to: <a href="https://x.com/adamcoomes/status/1221228297557893120" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <span class="status">1221228297557893120</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221231933008408578',
    created: 1579999356000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <a class="mention" href="https://x.com/AndrewYang" rel="noopener noreferrer" target="_blank">@AndrewYang</a> The people aren\'t the enemy. The system that allows the concentration of the wealth is the enemy. It shouldn\'t be possible that 3 people own more wealth than the bottom half of the country. That\'s literally diabolical, not to mention immoral.<br><br>In reply to: <a href="https://x.com/adamcoomes/status/1221230482165248001" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <span class="status">1221230482165248001</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1221227613714169857',
    created: 1579998327000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> @gigaman824 <a class="mention" href="https://x.com/AndrewYang" rel="noopener noreferrer" target="_blank">@AndrewYang</a> How can you run a progressive campaign and not deal with wealth inequality? You\'re not doing yourself and service here, and you\'re also not making any sense. Arguing for a freedom dividend, but saying the wealthy can have it all. It\'s inconsistent.<br><br>In reply to: <a href="https://x.com/adamcoomes/status/1221226286225510402" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <span class="status">1221226286225510402</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221225885027561472',
    created: 1579997914000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <a class="mention" href="https://x.com/AndrewYang" rel="noopener noreferrer" target="_blank">@AndrewYang</a> We literally benefit every day from the product of those wealth taxes. You can\'t rest in the comfort of modern society and pretend otherwise. That erases history.<br><br>In reply to: <a href="#1221225066773372928">1221225066773372928</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221225066773372928',
    created: 1579997719000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <a class="mention" href="https://x.com/AndrewYang" rel="noopener noreferrer" target="_blank">@AndrewYang</a> That\'s categorically false. And this country is where they have been used most effectively in the past. Come on, this is basic history.<br><br>In reply to: <a href="https://x.com/adamcoomes/status/1221223310672957445" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <span class="status">1221223310672957445</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221224862808563712',
    created: 1579997671000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <a class="mention" href="https://x.com/AndrewYang" rel="noopener noreferrer" target="_blank">@AndrewYang</a> He documents each source every time he talks about it. Wealth and speculation taxes, and large corporations paying their fair share, covers a vast majority, and ending endless wars is another. He\'s not giving away free stuff. The proposals have been vetted.<br><br>In reply to: <a href="https://x.com/adamcoomes/status/1221221989635018752" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <span class="status">1221221989635018752</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221222396218068992',
    created: 1579997083000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <a class="mention" href="https://x.com/AndrewYang" rel="noopener noreferrer" target="_blank">@AndrewYang</a> The reason I say I\'m disappointed is because it\'s a republican talking point. Let\'s not fear monger about social programs. History has proven they can be paid for by wealth and speculation taxes, to mention two key sources.<br><br>In reply to: <a href="#1221221507302449153">1221221507302449153</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221221507302449153',
    created: 1579996871000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <a class="mention" href="https://x.com/AndrewYang" rel="noopener noreferrer" target="_blank">@AndrewYang</a> And Bernie has one of the clearest plans for how to pay for it all, so I\'m not sure what you\'re getting at. It\'s disappointing to see you using the free pizza trope. Let\'s keep this debate about facts.<br><br>In reply to: <a href="https://x.com/adamcoomes/status/1221217396117114880" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <span class="status">1221217396117114880</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221217828944019456',
    created: 1579995994000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GeoffMiami" rel="noopener noreferrer" target="_blank">@GeoffMiami</a> Damn you\'re a hero. May the good hearts keep you warm.<br><br>In reply to: <a href="https://x.com/GeoffMiami/status/1221178352041693186" rel="noopener noreferrer" target="_blank">@GeoffMiami</a> <span class="status">1221178352041693186</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1221214669257396224',
    created: 1579995240000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <a class="mention" href="https://x.com/AndrewYang" rel="noopener noreferrer" target="_blank">@AndrewYang</a> I value Yang\'s contributions to the dialog. I believe he could be a great ally for Bernie and the political revolution, which is absolutely necessary to get anywhere. We need a broad coalition working on all fronts. Our enemy is the concentration of wealth and power.<br><br>In reply to: <a href="#1221213808699461632">1221213808699461632</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221213808699461632',
    created: 1579995035000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/adamcoomes" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <a class="mention" href="https://x.com/AndrewYang" rel="noopener noreferrer" target="_blank">@AndrewYang</a> Because he\'s been fighting his whole life for the most noble causes, he stands up for people who can\'t stand up for themselves, he\'s transformed the dialog of modern politics, he lifts others up, he inspires us to be better humans, he\'s fearless. And, oh yeah, he\'s going to win.<br><br>In reply to: <a href="https://x.com/adamcoomes/status/1221209208252772359" rel="noopener noreferrer" target="_blank">@adamcoomes</a> <span class="status">1221209208252772359</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221195653860163588',
    created: 1579990707000,
    type: 'post',
    text: 'I\'m #VotingForBernie because my father is a transgender women who will have a better, safer life of dignity under a diverse #PresidentSanders adminstration. This is the most important vote of my lifetime.',
    likes: 11,
    retweets: 4,
    tags: ['votingforbernie', 'presidentsanders']
  },
  {
    id: '1221194361649729538',
    created: 1579990399000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MoveOn" rel="noopener noreferrer" target="_blank">@MoveOn</a> Oh go to hell. He\'s a freakin\' comedian. His endorsement is a testament to what his true values are, not these lies. He\'s a peace advocate above all else. No one is perfect, but to say he\'s these things is to be even more imperfect. Do better.<br><br>In reply to: <a href="https://x.com/MoveOn/status/1221125549051011073" rel="noopener noreferrer" target="_blank">@MoveOn</a> <span class="status">1221125549051011073</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221192220566597634',
    created: 1579989888000,
    type: 'reply',
    text: '@SeaSparks21 I\'d say your captured the essence of it. Sustainably and justice go hand in hand. When resources are limited and monopolized, justice (and our planet) suffers.<br><br>In reply to: @SeaSparks21 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1221191336537284616',
    created: 1579989677000,
    type: 'post',
    text: 'I\'m #VotingForBernie because my brother and his spouse have a growing family and I want them to enjoy life rather than being saddled by medical debt.',
    likes: 2,
    retweets: 0,
    tags: ['votingforbernie']
  },
  {
    id: '1221190765411553285',
    created: 1579989541000,
    type: 'post',
    text: 'I\'m #VotingForBernie because, believe it or not, I have not yet had the opportunity to do so. (In 2016, Colorado only had a caucus and it wasn\'t open to independents). I will cast my ballot in behalf of someone I don\'t know, but for whom I fight for. Solidarity forever.',
    likes: 3,
    retweets: 0,
    tags: ['votingforbernie']
  },
  {
    id: '1221065375514648579',
    created: 1579959646000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joshfoxfilm" rel="noopener noreferrer" target="_blank">@joshfoxfilm</a> I can honestly say I\'ve never seen anything like this in politics. Something big is happening.<br><br>In reply to: <a href="https://x.com/joshfoxfilm/status/1220908234682355713" rel="noopener noreferrer" target="_blank">@joshfoxfilm</a> <span class="status">1220908234682355713</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1221060143699226625',
    created: 1579958399000,
    type: 'quote',
    text: 'I absolutely adore this practice.<br><br>Quoting: <a href="https://x.com/adsamalik/status/1221039394246873088" rel="noopener noreferrer" target="_blank">@adsamalik</a> <span class="status">1221039394246873088</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1220818457475735558',
    created: 1579900776000,
    type: 'post',
    text: '"Why does the default theme for Asciidoctor PDF have to be so ugly?"<br><br>Some people. I have words, but I won\'t say them.',
    likes: 12,
    retweets: 1
  },
  {
    id: '1220619499423944706',
    created: 1579853341000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/thunderheadeng" rel="noopener noreferrer" target="_blank">@thunderheadeng</a> I was wrong. All you need is for it to be installed. Asciidoctor PDF requires it automatically. What we are missing, though, is tests for this. So I\'ll add some.<br><br>In reply to: <a href="#1220109730644783106">1220109730644783106</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1220506622117609472',
    created: 1579826429000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jaredmorgs" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <a class="mention" href="https://x.com/bryanwklein" rel="noopener noreferrer" target="_blank">@bryanwklein</a> I agree with Jared, if you absolutely have to. But I think decades of open source have proved it\'s a myth (dare I even say a great lie) that being open gives your competitors a leg up on you. It\'s been debunked time and again.<br><br>In reply to: <a href="https://x.com/jaredmorgs/status/1220504215627284480" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <span class="status">1220504215627284480</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1220495347081826304',
    created: 1579823741000,
    type: 'quote',
    text: 'That\'s my cousin, yo. Congrats Jennifer! You\'re an inspiration!<br><br>Quoting: <a href="https://x.com/doingmycalling/status/1220368472636608513" rel="noopener noreferrer" target="_blank">@doingmycalling</a> <span class="status">1220368472636608513</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1220404892025540608',
    created: 1579802174000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jaredmorgs" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> I also like to point out to companies that if you don\'t tell your story, your competitor will tell it for you. Having your docs out there let\'s you control the facts and narrative.<br><br>In reply to: <a href="https://x.com/jaredmorgs/status/1220330230184697856" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <span class="status">1220330230184697856</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1220109730644783106',
    created: 1579731802000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/thunderheadeng" rel="noopener noreferrer" target="_blank">@thunderheadeng</a> Aha, you make a good point. -r is still required, though we could try to require it automatically. Feel free to file an issue with that request.<br><br>In reply to: <a href="https://x.com/thunderheadeng/status/1220105685054083072" rel="noopener noreferrer" target="_blank">@thunderheadeng</a> <span class="status">1220105685054083072</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1220100114624073728',
    created: 1579729510000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/algore" rel="noopener noreferrer" target="_blank">@algore</a> <a class="mention" href="https://x.com/GretaThunberg" rel="noopener noreferrer" target="_blank">@GretaThunberg</a> Anyone else notice the sobering truth emanating from that monitor? That monitor is woke.<br><br>In reply to: <a href="https://x.com/algore/status/1220045020805378053" rel="noopener noreferrer" target="_blank">@algore</a> <span class="status">1220045020805378053</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1219947308265066497',
    created: 1579693078000,
    type: 'quote',
    text: 'Greta\'s most poignant speech yet. It\'s really sad that such powerful and supposedly intelligent people need this explained to them so many times.<br><br>Quoting: <a href="https://x.com/democracynow/status/1219749514698993670" rel="noopener noreferrer" target="_blank">@democracynow</a> <span class="status">1219749514698993670</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1219774801604071424',
    created: 1579651949000,
    type: 'post',
    text: '#ILikeBernie No, I love Bernie...for his policies, for his integrity, for his persistence, for his bravery, for his leadership, for his humanity.',
    likes: 3,
    retweets: 0,
    tags: ['ilikebernie']
  },
  {
    id: '1219754565773352960',
    created: 1579647125000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PeterJaap" rel="noopener noreferrer" target="_blank">@PeterJaap</a> <a class="mention" href="https://x.com/andrewhowdencom" rel="noopener noreferrer" target="_blank">@andrewhowdencom</a> <a class="mention" href="https://x.com/keen_code" rel="noopener noreferrer" target="_blank">@keen_code</a> <a class="mention" href="https://x.com/petericebear" rel="noopener noreferrer" target="_blank">@petericebear</a> Don\'t miss kramdown-asciidoc to convert Markdown to AsciiDoc. The AsciiDoc it produces is cleaner and more toolchain friendly than what pandoc emits.<br><br>In reply to: <a href="https://x.com/PeterJaap/status/1218930146016710660" rel="noopener noreferrer" target="_blank">@PeterJaap</a> <span class="status">1218930146016710660</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1219565447705530368',
    created: 1579602035000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mxmkm" rel="noopener noreferrer" target="_blank">@mxmkm</a> As it turned out, the failures had only to do with the test harness and not the tests themselves. So your testing was excellent!<br><br>In reply to: <a href="https://x.com/mxmkm/status/1219564358138826752" rel="noopener noreferrer" target="_blank">@mxmkm</a> <span class="status">1219564358138826752</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1219565214800044032',
    created: 1579601980000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mxmkm" rel="noopener noreferrer" target="_blank">@mxmkm</a> Thanks! Always appreciated!<br><br>In reply to: <a href="https://x.com/mxmkm/status/1219564358138826752" rel="noopener noreferrer" target="_blank">@mxmkm</a> <span class="status">1219564358138826752</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1219562945891332102',
    created: 1579601439000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> AppVeyor for now.<br><br>In reply to: <a href="https://x.com/brunoborges/status/1219561502429995009" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1219561502429995009</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1219561011608293377',
    created: 1579600978000,
    type: 'post',
    text: 'Here I was thinking we were running Asciidoctor PDF tests on Windows, only to find out the build ran once, 8 months ago. Not to worry. They\'re running and and passing now!',
    likes: 3,
    retweets: 0
  },
  {
    id: '1219429469884014594',
    created: 1579569616000,
    type: 'post',
    text: 'His message was not just about achieving racial equality, it\'s about eradicating poverty and putting an end to the institutionalized racism that too often causes it. That means taxing the rich and atoning for sins of the past and present (via reparations).',
    likes: 0,
    retweets: 0
  },
  {
    id: '1219429468923551745',
    created: 1579569615000,
    type: 'post',
    text: 'For most of my life, I grew up without an identifiable hero. MLK is an obvious choice, but the high school taught version of him felt too convenient. Thanks to reeducation by <a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a>, I now realize that MLK truly is my hero (as well as the person who made me realize it).',
    likes: 0,
    retweets: 0
  },
  {
    id: '1219362825576972289',
    created: 1579553726000,
    type: 'post',
    text: 'Another version of Asciidoctor EPUB3 is out. As of this release, the project has a new maintainer! Please join me in welcoming Marat Radchenko to the project.<br><br><a href="https://github.com/asciidoctor/asciidoctor-epub3/releases/tag/v1.5.0.alpha.10" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-epub3/releases/tag/v1.5.0.alpha.10</a>',
    likes: 15,
    retweets: 5
  },
  {
    id: '1219234546173927424',
    created: 1579523142000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rikkiends" rel="noopener noreferrer" target="_blank">@rikkiends</a> <a class="mention" href="https://x.com/veganricha" rel="noopener noreferrer" target="_blank">@veganricha</a> Thanks for the tip! (Right now, I\'m super into Thug Kitchen, but variety is the spice of life).<br><br>In reply to: <a href="https://x.com/rikkiends/status/1219233383286484992" rel="noopener noreferrer" target="_blank">@rikkiends</a> <span class="status">1219233383286484992</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1219234310940610560',
    created: 1579523086000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Razer" rel="noopener noreferrer" target="_blank">@Razer</a> Correction: GeForce GTX 1650<br><br>In reply to: <a href="#1219222955105734656">1219222955105734656</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1219223405024595974',
    created: 1579520486000,
    type: 'post',
    text: 'The Crucial X8 is a screaming fast portable SSD drive. It\'s absolutely worth getting if you can. Runs backups in seconds.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1219222955105734656',
    created: 1579520379000,
    type: 'post',
    text: 'Linux (Fedora) works phenomenally well on the <a class="mention" href="https://x.com/Razer" rel="noopener noreferrer" target="_blank">@Razer</a> Blade Stealth 13" (Late 2019) with the GeForce Max 1650 (nouveau driver). Will post more about it when I get a chance.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1218825830354173952',
    created: 1579425697000,
    type: 'post',
    text: 'Did you test the NVIDIA card to verify it\'s actually faster than the integrated Intel card? From my tests, regardless of whether I make it the primary card or use primusrun, it\'s slower. (I\'m fine with the Intel card because it\'s so screaming fast, just curious).',
    likes: 0,
    retweets: 0
  },
  {
    id: '1218825066021941248',
    created: 1579425515000,
    type: 'post',
    text: 'I did discover that to get Thunderbolt working, I had to downgrade to the 5.4.5 kernel, though I suspect that will get ironed out in time.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1218824790896594944',
    created: 1579425449000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/nhripps" rel="noopener noreferrer" target="_blank">@nhripps</a> Thank you for your concise, yet indispensable guide on how to install Fedora on the Razer Blade Stealth. I referred back to it (and the NVIDIA Optimus Fedora quickdocs) many times to achieve a seamless setup.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1218820661499400192',
    created: 1579424464000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bryanwklein" rel="noopener noreferrer" target="_blank">@bryanwklein</a> My bro <a class="mention" href="https://x.com/shobull" rel="noopener noreferrer" target="_blank">@shobull</a> will be thrilled to hear that.<br><br>In reply to: <a href="https://x.com/bryanwklein/status/1218819871753945089" rel="noopener noreferrer" target="_blank">@bryanwklein</a> <span class="status">1218819871753945089</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1218716557552930816',
    created: 1579399644000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bk2204" rel="noopener noreferrer" target="_blank">@bk2204</a> I\'ve almost gotten a Carbon a few times, but I haven\'t liked ThinkPad touchpads in the past. Do you think it matches up to the one on the Mac?<br><br>In reply to: <a href="https://x.com/bk2204/status/1218715798929387520" rel="noopener noreferrer" target="_blank">@bk2204</a> <span class="status">1218715798929387520</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1218716094174597120',
    created: 1579399534000,
    type: 'reply',
    text: '@SAShrader First, that you were speaking from the heart. I could feel the pain in your voice. Second, that you\'re Americans being left behind, ignored, exploited for 💵. The best America is small towns and that showed me we\'re losing our soul. And the mountain tops turned into poison. 😭<br><br>In reply to: @SAShrader <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1218714819613753349',
    created: 1579399230000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bk2204" rel="noopener noreferrer" target="_blank">@bk2204</a> Hmm, when I used Linux on Mac many years ago, it was a good fit. Too bad to see it struggling now. I\'ve got 2 computers with Thunderbolt running great (Dell XPS and Razer Blade Stealth), so there\'s hope out there.<br><br>In reply to: <a href="https://x.com/bk2204/status/1218710761301848064" rel="noopener noreferrer" target="_blank">@bk2204</a> <span class="status">1218710761301848064</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1218714033722818561',
    created: 1579399042000,
    type: 'reply',
    text: '@SAShrader Together, united we will win back the pride and joy for the people of these towns and the numerous across the nation. Your words will stick with me forever.<br><br>In reply to: @SAShrader <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1218642327482191874',
    created: 1579381946000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/hmvictorh" rel="noopener noreferrer" target="_blank">@hmvictorh</a> Promises 👍<br><br>In reply to: <a href="https://x.com/hmvictorh/status/1218602581368418304" rel="noopener noreferrer" target="_blank">@hmvictorh</a> <span class="status">1218602581368418304</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1218603960115662848',
    created: 1579372799000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/hmvictorh" rel="noopener noreferrer" target="_blank">@hmvictorh</a> It\'s just very repetitive. But there are so many great ones. Salvation. When You\'re Gone. Zombie. Dreams. 😍<br><br>In reply to: <a href="https://x.com/hmvictorh/status/1218602581368418304" rel="noopener noreferrer" target="_blank">@hmvictorh</a> <span class="status">1218602581368418304</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1218600602218844163',
    created: 1579371998000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/hmvictorh" rel="noopener noreferrer" target="_blank">@hmvictorh</a> Linger is probably my least favorite song.<br><br>In reply to: <a href="https://x.com/hmvictorh/status/1218597550162415618" rel="noopener noreferrer" target="_blank">@hmvictorh</a> <span class="status">1218597550162415618</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1218490457329528832',
    created: 1579345738000,
    type: 'post',
    text: 'Animal Instinct by The Cranberries is an unrated song. I only recently heard it for the first time, but I instantly loved it.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1218393220385628160',
    created: 1579322555000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BriceDutheil" rel="noopener noreferrer" target="_blank">@BriceDutheil</a> Very cool. People are always asking about themes. We could highlight this on the themes page in the new docs. I\'ll pass it on to our editor-in-chief.<br><br>In reply to: <a href="https://x.com/BriceDutheil/status/1217116072308019205" rel="noopener noreferrer" target="_blank">@BriceDutheil</a> <span class="status">1217116072308019205</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1218274192828715008',
    created: 1579294176000,
    type: 'reply',
    text: '@SeaSparks21 @SAShrader Actually, I was thinking of an even earlier town hall. But that just goes to show how committed Bernie is to those who are often forgotten.<br><br>In reply to: <a href="#1218273857389264896">1218273857389264896</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1218273857389264896',
    created: 1579294096000,
    type: 'reply',
    text: '@SeaSparks21 I remember it fondly. I also had the privilege to connect with @SAShrader, who once again shared her stories at the DNC platform hearings. I\'ve never been so moved by a testimony.<br><br>In reply to: @SeaSparks21 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1218273041605517313',
    created: 1579293902000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fwilhe" rel="noopener noreferrer" target="_blank">@fwilhe</a> Amen.<br><br>In reply to: <a href="https://x.com/fwilhe/status/1218253403798806530" rel="noopener noreferrer" target="_blank">@fwilhe</a> <span class="status">1218253403798806530</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1218251621123026945',
    created: 1579288795000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fwilhe" rel="noopener noreferrer" target="_blank">@fwilhe</a> The primary is critical. If the Democrats nominate the wrong candidate, there\'s little to no chance of turning this thing around once November rolls around. That\'s what terrifies me the most. I don\'t want to think about what world we\'ll have if the autocrat isn\'t defeated.<br><br>In reply to: <a href="https://x.com/fwilhe/status/1218238811081269250" rel="noopener noreferrer" target="_blank">@fwilhe</a> <span class="status">1218238811081269250</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1218250262864785409',
    created: 1579288471000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fwilhe" rel="noopener noreferrer" target="_blank">@fwilhe</a> Here\'s the calendar: <a href="https://en.wikipedia.org/wiki/2020_Democratic_Party_presidential_primaries#Primary_and_caucus_calendar" rel="noopener noreferrer" target="_blank">en.wikipedia.org/wiki/2020_Democratic_Party_presidential_primaries#Primary_and_caucus_calendar</a><br><br>In reply to: <a href="#1218249563401670656">1218249563401670656</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1218249866658402304',
    created: 1579288376000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MaxGrobe" rel="noopener noreferrer" target="_blank">@MaxGrobe</a> My thought exactly.<br><br>In reply to: <a href="https://x.com/MaxGrobe/status/1218240796828274691" rel="noopener noreferrer" target="_blank">@MaxGrobe</a> <span class="status">1218240796828274691</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1218249563401670656',
    created: 1579288304000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fwilhe" rel="noopener noreferrer" target="_blank">@fwilhe</a> Yes. There\'s no exact date since the primaries happen on different days over the course of several months. But there\'s a point when it becomes clear who will likely take it. And that highly depends on how close the races are. At worse, the summer.<br><br>In reply to: <a href="https://x.com/fwilhe/status/1218238811081269250" rel="noopener noreferrer" target="_blank">@fwilhe</a> <span class="status">1218238811081269250</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1218248949145882624',
    created: 1579288158000,
    type: 'post',
    text: 'FYI, I\'ve observed that in Ruby 2.7, RubyGems sets the SOURCE_DATE_EPOCH environment variable internally. This can be observed by running a program with Bundler. In this scenario, there\'s no way to run a program without this variable being set. (I\'ve reported it).',
    likes: 0,
    retweets: 0
  },
  {
    id: '1218238053824598016',
    created: 1579285560000,
    type: 'quote',
    text: 'It\'s hard to believe voting for 2020 is already starting. Or maybe it\'s hard to believe it\'s finally starting. Either way, here we go!<br><br>Quoting: <a href="https://x.com/IlhanMN/status/1218210688767795200" rel="noopener noreferrer" target="_blank">@IlhanMN</a> <span class="status">1218210688767795200</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1218009463191371776',
    created: 1579231060000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a> I pretty much put my life on pause for 3 years.<br><br>In reply to: <a href="https://x.com/BernieSanders/status/1217966036659724294" rel="noopener noreferrer" target="_blank">@BernieSanders</a> <span class="status">1217966036659724294</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1218009046336397313',
    created: 1579230960000,
    type: 'post',
    text: 'From my vantage point, GitHub Actions is surging. This is both good and worrying. It gives test suites access to more platforms (good). It also introduces more proprietary lock-in (worrying). I think I\'m going to do dual CI between GitHub and GitLab if/when I make the switch.',
    likes: 3,
    retweets: 1
  },
  {
    id: '1218007608222789632',
    created: 1579230617000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/uncapybarable" rel="noopener noreferrer" target="_blank">@uncapybarable</a> 😍<br><br>In reply to: <a href="https://x.com/uncapybarable/status/1217894765779390464" rel="noopener noreferrer" target="_blank">@uncapybarable</a> <span class="status">1217894765779390464</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1218007189224411138',
    created: 1579230518000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/JordanChariton" rel="noopener noreferrer" target="_blank">@JordanChariton</a> What I find fascinating is that the mass movement is starting to be able to see through it all, and that makes me hopeful. We really need to be less gullible as a society. The young people and independent reporting are showing us how.<br><br>In reply to: <a href="https://x.com/JordanChariton/status/1218005143033131009" rel="noopener noreferrer" target="_blank">@JordanChariton</a> <span class="status">1218005143033131009</span>',
    likes: 6,
    retweets: 1
  },
  {
    id: '1218002915966771200',
    created: 1579229499000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> I\'m contemplating what this means for Antora.<br><br>In reply to: <a href="#1218002762505605120">1218002762505605120</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1218002762505605120',
    created: 1579229462000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> You might want to check out this article about gradual migration to TypeScript and/or TypeScript compatibility. Looks like you don\'t need to maintain those types by hand. <a href="https://www.twilio.com/blog/move-to-typescript" rel="noopener noreferrer" target="_blank">www.twilio.com/blog/move-to-typescript</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1217998747944333312',
    created: 1579228505000,
    type: 'post',
    text: 'Voting is a right. Full stop.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1217995964708900866',
    created: 1579227841000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/WalkerBragman" rel="noopener noreferrer" target="_blank">@WalkerBragman</a> This week we were reminded who the true progressive is.<br><br>In reply to: <a href="https://x.com/WalkerBragman/status/1217884472231841792" rel="noopener noreferrer" target="_blank">@WalkerBragman</a> <span class="status">1217884472231841792</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1217935224278880256',
    created: 1579213360000,
    type: 'quote',
    text: 'TikTok is introducing such a fascinating visual language. It may seem bizarre at first glace, but there\'s a genius to it. This is a great example.<br><br>Quoting: <a href="https://x.com/sunrisemvmt/status/1217930612809654272" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a> <span class="status">1217930612809654272</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1217592518017048576',
    created: 1579131652000,
    type: 'quote',
    text: '"Anything less than immediately ceasing these investments in the fossil fuel industry would be a betrayal of life itself. Today’s business as usual is turning into a crime against humanity."<br><br>Quoting: <a href="https://x.com/GretaThunberg/status/1215655255003860993" rel="noopener noreferrer" target="_blank">@GretaThunberg</a> <span class="status">1215655255003860993</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1217566868317163521',
    created: 1579125537000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/janeosanders" rel="noopener noreferrer" target="_blank">@janeosanders</a> <a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a> Well said!<br><br>In reply to: <a href="https://x.com/janeosanders/status/1217510424742154241" rel="noopener noreferrer" target="_blank">@janeosanders</a> <span class="status">1217510424742154241</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1217566187799736320',
    created: 1579125375000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rotnroll666" rel="noopener noreferrer" target="_blank">@rotnroll666</a> <a class="mention" href="https://x.com/github" rel="noopener noreferrer" target="_blank">@github</a> Seeing the same thing. I thought it was just me.<br><br>In reply to: <a href="https://x.com/rotnroll666/status/1217563310217990152" rel="noopener noreferrer" target="_blank">@rotnroll666</a> <span class="status">1217563310217990152</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1217564303575764992',
    created: 1579124925000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jaredmorgs" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <a class="mention" href="https://x.com/plaindocs" rel="noopener noreferrer" target="_blank">@plaindocs</a> <a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> <a class="mention" href="https://x.com/supersole" rel="noopener noreferrer" target="_blank">@supersole</a> And yes, there are some people who are confused. And we will deal with that. But let\'s not make it out to be more people than it is. The vast majority know where to find the Asciidoctor user manual and are successful using it. (And even that\'s about to get an overhaul).<br><br>In reply to: <a href="#1217562505389592577">1217562505389592577</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1217563912133988352',
    created: 1579124832000,
    type: 'reply',
    text: '@SeaSparks21 <a class="mention" href="https://x.com/bern_identity" rel="noopener noreferrer" target="_blank">@bern_identity</a> It\'s sad because I really had no beef with Warren, except for not speaking up in 2016 to advance the movement, which I was willing to look past. But she just gave me an inescapable reason to distrust her. She\'s getting some profoundly bad advice (or it\'s who she is).<br><br>In reply to: @SeaSparks21 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1217562505389592577',
    created: 1579124497000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jaredmorgs" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <a class="mention" href="https://x.com/plaindocs" rel="noopener noreferrer" target="_blank">@plaindocs</a> <a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> <a class="mention" href="https://x.com/supersole" rel="noopener noreferrer" target="_blank">@supersole</a> Them includes me ;) I\'m extremely well aware that the old AsciiDoc site needs to be dealt with. But it cannot be until the spec is underway. And that\'s what I\'m working on.<br><br>In reply to: <a href="https://x.com/jaredmorgs/status/1217561584748261376" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <span class="status">1217561584748261376</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1217538812051480576',
    created: 1579118848000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/plaindocs" rel="noopener noreferrer" target="_blank">@plaindocs</a> <a class="mention" href="https://x.com/jaredmorgs" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> <a class="mention" href="https://x.com/supersole" rel="noopener noreferrer" target="_blank">@supersole</a> Granted, the spec process will clear all this up so there can be absolutely no confusion. Which is a key reason why we\'re doing it.<br><br>In reply to: <a href="#1217538510179061760">1217538510179061760</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1217538510179061760',
    created: 1579118776000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/plaindocs" rel="noopener noreferrer" target="_blank">@plaindocs</a> <a class="mention" href="https://x.com/jaredmorgs" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> <a class="mention" href="https://x.com/supersole" rel="noopener noreferrer" target="_blank">@supersole</a> At this point, we should not even be talking about <a href="http://AsciiDoc.py" rel="noopener noreferrer" target="_blank">AsciiDoc.py</a>. It\'s in the past. There\'s only one AsciiDoc, and it\'s what Asciidoctor defines. All the lead devs have confirmed this. And that\'s the input for the spec process.<br><br>In reply to: <a href="https://x.com/plaindocs/status/1217444576379711488" rel="noopener noreferrer" target="_blank">@plaindocs</a> <span class="status">1217444576379711488</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1217358469692563456',
    created: 1579075851000,
    type: 'reply',
    text: '@belensisaw <a class="mention" href="https://x.com/michaelsayman" rel="noopener noreferrer" target="_blank">@michaelsayman</a> Indeed, Bernie\'s giving us a master class on integrity and we should be here for it.<br><br>In reply to: <a href="https://x.com/thebelensisa/status/1217337576769626112" rel="noopener noreferrer" target="_blank">@thebelensisa</a> <span class="status">1217337576769626112</span>',
    likes: 9,
    retweets: 0
  },
  {
    id: '1217354867628482560',
    created: 1579074992000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bern_identity" rel="noopener noreferrer" target="_blank">@bern_identity</a> I\'m speechless. The only reason you shouldn\'t shake Bernie\'s hand is if you don\'t have a hand you can shake. wtf<br><br>In reply to: <a href="https://x.com/bern_identity/status/1217301218470481922" rel="noopener noreferrer" target="_blank">@bern_identity</a> <span class="status">1217301218470481922</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1217284367875104773',
    created: 1579058184000,
    type: 'quote',
    text: 'Seems like the corporate media and Democratic establishment are doing a perfectly fine job of screwing it up all on their own. But keep blaming other countries for our problems. I know it\'s easier to point fingers than it is to reform.<br><br>Quoting: <a href="https://x.com/SpeakerPelosi/status/1217170364587028480" rel="noopener noreferrer" target="_blank">@SpeakerPelosi</a> <span class="status">1217170364587028480</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1217276992581586944',
    created: 1579056425000,
    type: 'post',
    text: 'Bernie is the anti-war candidate. Bernie is the human rights candidate. Bernie is the climate action candidate. Bernie is the Medicare-for-all candidate. Bernie is the important issues candidate.<br><br>I\'m voting for Bernie Sanders in the Democratic primary.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1217270936400846849',
    created: 1579054981000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jaredmorgs" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> It\'s very similar to employees with expense accounts. Anything under X amount is approved. We trust you. If you can\'t trust them, that\'s a different issue.<br><br>In reply to: <a href="#1217270385411907584">1217270385411907584</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1217270385411907584',
    created: 1579054850000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jaredmorgs" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> I disagree with them even when used for development safeguards. I trust in people, not security theater.<br><br>In reply to: <a href="https://x.com/jaredmorgs/status/1214530906280431616" rel="noopener noreferrer" target="_blank">@jaredmorgs</a> <span class="status">1214530906280431616</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1217200762427604992',
    created: 1579038250000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/tlberglund" rel="noopener noreferrer" target="_blank">@tlberglund</a> <a class="mention" href="https://x.com/SanneGrinovero" rel="noopener noreferrer" target="_blank">@SanneGrinovero</a> Most of the time, I have no idea what the track title is even supposed to mean. They seem arbitrary. Same when submitting proposals.<br><br>In reply to: <a href="https://x.com/tlberglund/status/1217141510145105920" rel="noopener noreferrer" target="_blank">@tlberglund</a> <span class="status">1217141510145105920</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1217175673237725185',
    created: 1579032269000,
    type: 'post',
    text: 'OH: Yep, it\'s screaming fast alright. When it\'s going fast, it\'s screaming.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1217175279661019136',
    created: 1579032175000,
    type: 'post',
    text: 'OH: After navigating though 5 minutes of wasting my time, I got to the menu I wanted.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1216866654266609665',
    created: 1578958593000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CoryBooker" rel="noopener noreferrer" target="_blank">@CoryBooker</a> Thank you for using your platform to talk about being vegan and raising awareness of the atrocities committed by factory farms. That really made a difference and increased my respect for you dramatically.<br><br>In reply to: <a href="https://x.com/CoryBooker/status/1216751780345864193" rel="noopener noreferrer" target="_blank">@CoryBooker</a> <span class="status">1216751780345864193</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1216841369630625792',
    created: 1578952564000,
    type: 'post',
    text: 'If you\'re using RubyGems via JRuby (such as with AsciidoctorJ), there\'s important information you need to know about the repository. See <a href="https://github.com/asciidoctor/asciidoctorj/issues/896" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctorj/issues/896</a>',
    likes: 1,
    retweets: 4
  },
  {
    id: '1216838964151062528',
    created: 1578951991000,
    type: 'post',
    text: 'If you\'re using RubyGems in Ruby 2.7, be careful. It now sets the SOURCE_DATE_EPOCH environment variable if not already set. And this can have side effects.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1216707431767478277',
    created: 1578920631000,
    type: 'post',
    text: 'The Intel graphics card on Ice Lake seems faster to me than the Nvidia card (GTX 1650 Max), at least on Linux.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1216456266773495808',
    created: 1578860749000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agentdero" rel="noopener noreferrer" target="_blank">@agentdero</a> That\'s exactly what it feels like. The fan is much more active than normal.<br><br>In reply to: <a href="https://x.com/agentdero/status/1216453489955495936" rel="noopener noreferrer" target="_blank">@agentdero</a> <span class="status">1216453489955495936</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1216452307421786113',
    created: 1578859805000,
    type: 'post',
    text: 'I\'ve tried this on 3 computers now and I\'m convinced the performance, mostly around smooth scrolling, has degraded in Firefox 72. I\'m switching to Firefox 68 ESR for now.<br><br>Quoting: <a href="#1216143279625924609">1216143279625924609</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1216451619400765440',
    created: 1578859641000,
    type: 'post',
    text: 'A second release candidate of Asciidoctor PDF 1.5.0 is available. The final release is happening very soon, so be sure to give this one a try so you can be sure the final release will be ready for primetime. <a href="https://github.com/asciidoctor/asciidoctor-pdf/releases/tag/v1.5.0.rc.2" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-pdf/releases/tag/v1.5.0.rc.2</a>',
    likes: 19,
    retweets: 7
  },
  {
    id: '1216181443875033088',
    created: 1578795226000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rmehmandarov" rel="noopener noreferrer" target="_blank">@rmehmandarov</a> That\'s exactly why I was upgrading in the first place.<br><br>In reply to: <a href="https://x.com/rmehmandarov/status/1216146885725097984" rel="noopener noreferrer" target="_blank">@rmehmandarov</a> <span class="status">1216146885725097984</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1216143279625924609',
    created: 1578786127000,
    type: 'post',
    text: 'It might just be me, but Firefox 72 feels really sluggish on Linux. I downgraded to confirm the update is the cause. Seems to be the new kinetic scrolling.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1215756365458595840',
    created: 1578693879000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ItGumby" rel="noopener noreferrer" target="_blank">@ItGumby</a> Thanks Brian. That really means a lot.<br><br>In reply to: <a href="https://x.com/ItGumby/status/1215732201255579651" rel="noopener noreferrer" target="_blank">@ItGumby</a> <span class="status">1215732201255579651</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1215486757799518208',
    created: 1578629600000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/evanlweber" rel="noopener noreferrer" target="_blank">@evanlweber</a> <a class="mention" href="https://x.com/NaomiAKlein" rel="noopener noreferrer" target="_blank">@NaomiAKlein</a> Your writing *is* genius. I keep commenting about it to people I talk to. I\'m a big believer in the power of the written word, so eloquently stated.<br><br>In reply to: <a href="https://x.com/evanlweber/status/1215462382996115456" rel="noopener noreferrer" target="_blank">@evanlweber</a> <span class="status">1215462382996115456</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1215365917497970688',
    created: 1578600789000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> @rgransberger And much respect for being allies in the fight for justice. Just so we\'re clear on that point ;)<br><br>In reply to: <a href="https://x.com/brunoborges/status/1215359549961469952" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1215359549961469952</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1215364421930733568',
    created: 1578600433000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> @rgransberger I don\'t think we\'re disagreeing...just working through nuance. I\'m def not questioning your understanding of the subject matter ;)<br><br>Kneeling is the most peaceful form of protest &amp; it\'s just outrageous to me that it is being called out. That\'s really where I have the problem.<br><br>In reply to: <a href="https://x.com/brunoborges/status/1215359549961469952" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1215359549961469952</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1215358291305365504',
    created: 1578598971000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> @rgransberger And that picture on the original post pretty much tells the story. We are talking about banning that. Do you realize how important and iconic that picture has been in the history of equality? How many movements it has inspired?<br><br>In reply to: <a href="#1215357832414945280">1215357832414945280</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1215357832414945280',
    created: 1578598862000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> @rgransberger Because this is still silencing of the truth. There\'s a difference between some moron spreading hate and someone drawing attention to a massive injustice. We can\'t be silent about injustice because we worry someone will be an asshole. We can see assholes.<br><br>In reply to: <a href="https://x.com/brunoborges/status/1215356147089080320" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1215356147089080320</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1215355103063883776',
    created: 1578598211000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/michaelsayman" rel="noopener noreferrer" target="_blank">@michaelsayman</a> I\'m confident we will. Right now, it\'s really about strengthening the platform we\'re going to use to win.<br><br>In reply to: <a href="https://x.com/michaelsayman/status/1215352505451843584" rel="noopener noreferrer" target="_blank">@michaelsayman</a> <span class="status">1215352505451843584</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1215353672583876608',
    created: 1578597870000,
    type: 'reply',
    text: '@rgransberger I point this out because this is such a grave problem. Police, especially in the US, are murdering citizens that don\'t look like them. And that needs to be seen so it can be stopped.<br><br>In reply to: <a href="#1215352285598908416">1215352285598908416</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1215352285598908416',
    created: 1578597539000,
    type: 'reply',
    text: '@rgransberger I don\'t agree because the statement has nothing to do with neutrality. It\'s extremely clear (at least to me) that it\'s directed at the movement that calls for racial equality. The statement, itself, is political. Kneeling is about stopping the killing of minorities by the state.<br><br>In reply to: @rgransberger <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1215351614862585856',
    created: 1578597379000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GovofCO" rel="noopener noreferrer" target="_blank">@GovofCO</a> <a class="mention" href="https://x.com/LtGovofCO" rel="noopener noreferrer" target="_blank">@LtGovofCO</a> <a class="mention" href="https://x.com/McCluskieforCO" rel="noopener noreferrer" target="_blank">@McCluskieforCO</a> @KerryDonovanSD5 Then show that courage and ban fracking.<br><br>In reply to: <a href="https://x.com/GovofCO/status/1215351508646027264" rel="noopener noreferrer" target="_blank">@GovofCO</a> <span class="status">1215351508646027264</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1215347552024842240',
    created: 1578596411000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GovofCO" rel="noopener noreferrer" target="_blank">@GovofCO</a> <a class="mention" href="https://x.com/LtGovofCO" rel="noopener noreferrer" target="_blank">@LtGovofCO</a> <a class="mention" href="https://x.com/McCluskieforCO" rel="noopener noreferrer" target="_blank">@McCluskieforCO</a> @KerryDonovanSD5 #MedicareForAll<br><br>In reply to: <a href="https://x.com/GovofCO/status/1215347353739116544" rel="noopener noreferrer" target="_blank">@GovofCO</a> <span class="status">1215347353739116544</span>',
    likes: 0,
    retweets: 0,
    tags: ['medicareforall']
  },
  {
    id: '1215347437138669569',
    created: 1578596383000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GovofCO" rel="noopener noreferrer" target="_blank">@GovofCO</a> <a class="mention" href="https://x.com/LtGovofCO" rel="noopener noreferrer" target="_blank">@LtGovofCO</a> <a class="mention" href="https://x.com/McCluskieforCO" rel="noopener noreferrer" target="_blank">@McCluskieforCO</a> @KerryDonovanSD5 It\'s a nice story, but the reality is--at least on the Front Range--that it\'s just that same enrichment system for insurance companies and hospital in a different color. It just shifts the costs from one place to another. We need #MedicareForAll. Period.<br><br>In reply to: <a href="https://x.com/GovofCO/status/1215345627946610689" rel="noopener noreferrer" target="_blank">@GovofCO</a> <span class="status">1215345627946610689</span>',
    likes: 0,
    retweets: 0,
    tags: ['medicareforall']
  },
  {
    id: '1215345747173883904',
    created: 1578595980000,
    type: 'quote',
    text: 'And I will once again be boycotting the Olympics.<br><br>Quoting: <a href="https://x.com/ajplus/status/1215337722505826304" rel="noopener noreferrer" target="_blank">@ajplus</a> <span class="status">1215337722505826304</span>',
    likes: 6,
    retweets: 2
  },
  {
    id: '1215341057417695233',
    created: 1578594862000,
    type: 'post',
    text: 'I\'m thrilled to learn <a class="mention" href="https://x.com/sunrisemvmt" rel="noopener noreferrer" target="_blank">@sunrisemvmt</a> has endorsed <a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a> for President. It\'s not just about 1 candidate. Like Bernie, Sunrise is building a movement. Like Bernie, Sunrise is not afraid to stand up to the fossil fuel industry. Like Bernie, Sunrise is about us, the people. 👏',
    likes: 4,
    retweets: 1
  },
  {
    id: '1215211480653561857',
    created: 1578563969000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> <a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> So something like:<br><br>include::short-definition-a.adoc[prefix="* "]<br><br>or<br><br>include::short-definition-a.adoc[prepend="* "]<br><br>In reply to: <a href="#1215211307571404800">1215211307571404800</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1215211307571404800',
    created: 1578563927000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/agoncal" rel="noopener noreferrer" target="_blank">@agoncal</a> <a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> This just gave me the idea that perhaps the include directive could support a prefix and suffix attribute that specifies text to prepend or append to the included contents. Could be useful for headings, lists, etc.<br><br>In reply to: <a href="https://x.com/agoncal/status/1215199661180452864" rel="noopener noreferrer" target="_blank">@agoncal</a> <span class="status">1215199661180452864</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1215063992932040705',
    created: 1578528805000,
    type: 'quote',
    text: 'Ruby has such fun and productive language features, esp for scripting.<br><br>Quoting: <a href="https://x.com/Todd_A_Jacobs/status/1215049762619240449" rel="noopener noreferrer" target="_blank">@Todd_A_Jacobs</a> <span class="status">1215049762619240449</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1215018717228105729',
    created: 1578518010000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CedricChampeau" rel="noopener noreferrer" target="_blank">@CedricChampeau</a> <a class="mention" href="https://x.com/Kiview" rel="noopener noreferrer" target="_blank">@Kiview</a> <a class="mention" href="https://x.com/overleaf" rel="noopener noreferrer" target="_blank">@overleaf</a> <a class="mention" href="https://x.com/gitlab" rel="noopener noreferrer" target="_blank">@gitlab</a> <a class="mention" href="https://x.com/github" rel="noopener noreferrer" target="_blank">@github</a> I could be wrong. Who knows.<br><br>In reply to: <a href="#1215017440448376832">1215017440448376832</a>',
    likes: 0,
    retweets: 1
  },
  {
    id: '1215017440448376832',
    created: 1578517706000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CedricChampeau" rel="noopener noreferrer" target="_blank">@CedricChampeau</a> <a class="mention" href="https://x.com/Kiview" rel="noopener noreferrer" target="_blank">@Kiview</a> <a class="mention" href="https://x.com/overleaf" rel="noopener noreferrer" target="_blank">@overleaf</a> <a class="mention" href="https://x.com/gitlab" rel="noopener noreferrer" target="_blank">@gitlab</a> <a class="mention" href="https://x.com/github" rel="noopener noreferrer" target="_blank">@github</a> Good luck. (I seriously doubt it would ever happen).<br><br>In reply to: <a href="https://x.com/CedricChampeau/status/1215016797398810625" rel="noopener noreferrer" target="_blank">@CedricChampeau</a> <span class="status">1215016797398810625</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1215016499296882688',
    created: 1578517482000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Kiview" rel="noopener noreferrer" target="_blank">@Kiview</a> <a class="mention" href="https://x.com/CedricChampeau" rel="noopener noreferrer" target="_blank">@CedricChampeau</a> <a class="mention" href="https://x.com/overleaf" rel="noopener noreferrer" target="_blank">@overleaf</a> <a class="mention" href="https://x.com/gitlab" rel="noopener noreferrer" target="_blank">@gitlab</a> I put my faith in <a class="mention" href="https://x.com/gitlab" rel="noopener noreferrer" target="_blank">@gitlab</a> for exploring in this space.<br><br>In reply to: <a href="https://x.com/Kiview/status/1214837066980311040" rel="noopener noreferrer" target="_blank">@Kiview</a> <span class="status">1214837066980311040</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1215016063777173505',
    created: 1578517378000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mxmkm" rel="noopener noreferrer" target="_blank">@mxmkm</a> I\'m super glad to hear that!<br><br>In reply to: <a href="https://x.com/mxmkm/status/1214854456900235265" rel="noopener noreferrer" target="_blank">@mxmkm</a> <span class="status">1214854456900235265</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1214855263481516033',
    created: 1578479040000,
    type: 'post',
    text: 'I love the idea of "democracy dollars", vouchers that individuals can use to fund elections (instead of all other mechanisms). It would transform politics.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1214850923211575299',
    created: 1578478005000,
    type: 'post',
    text: 'Ever since I added a test suite, Asciidoctor PDF got much better. Not perfect. But much, much better. Not to mention more stable and resilient.',
    likes: 15,
    retweets: 0
  },
  {
    id: '1214850138495053825',
    created: 1578477818000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mxmkm" rel="noopener noreferrer" target="_blank">@mxmkm</a> And technically, it doesn\'t even require RVM. It works as long as `ruby -v` works (and you have the bundler gem installed if it didn\'t come with the Ruby installation).<br><br>In reply to: <a href="#1214849760487604230">1214849760487604230</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1214849760487604230',
    created: 1578477728000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mxmkm" rel="noopener noreferrer" target="_blank">@mxmkm</a> If anything goes wrong, just remove the .bundle folder and start again.<br><br>In reply to: <a href="#1214849430056136706">1214849430056136706</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1214849430056136706',
    created: 1578477649000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mxmkm" rel="noopener noreferrer" target="_blank">@mxmkm</a> It also makes it super easy to install gems from GitHub, because then you just add:<br><br>gem \'asciidoctor-pdf\', github: \'asciidoctor/asciidoctor-pdf\', branch: \'master\'<br><br>bundle exec asciidoctor-pdf -v<br><br>In reply to: <a href="#1214848818782433280">1214848818782433280</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1214849166645420032',
    created: 1578477586000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mxmkm" rel="noopener noreferrer" target="_blank">@mxmkm</a> The key is to keep the gems as self-contained as possible. It\'s fine if you do this in a bunch of places under your user directory. Everything stays isolated. I\'ve been doing it for almost a decade and it never fails me.<br><br>In reply to: <a href="#1214848818782433280">1214848818782433280</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1214848818782433280',
    created: 1578477503000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mxmkm" rel="noopener noreferrer" target="_blank">@mxmkm</a> You must have a directory where your documents live. That\'s where you want to set up the Gemfile.<br><br>(It\'s also possible to do the same thing with a gemset in rvm so it works globally, but that\'s a bit more to maintain / advanced).<br><br>In reply to: <a href="https://x.com/mxmkm/status/1214848258385809409" rel="noopener noreferrer" target="_blank">@mxmkm</a> <span class="status">1214848258385809409</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1214848618009464834',
    created: 1578477455000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mxmkm" rel="noopener noreferrer" target="_blank">@mxmkm</a> Wherever you plan to run a Ruby program, you add a Gemfile, then run:<br><br>bundle --path=.bundle/gems<br>bundle exec &lt;name-of-program&gt;<br><br>(e.g., bundle exec asciidoctor-pdf)<br><br>In reply to: <a href="https://x.com/mxmkm/status/1214848258385809409" rel="noopener noreferrer" target="_blank">@mxmkm</a> <span class="status">1214848258385809409</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1214787353782317057',
    created: 1578462849000,
    type: 'post',
    text: '#NoWarWithIran',
    likes: 3,
    retweets: 0,
    tags: ['nowarwithiran']
  },
  {
    id: '1214678559354015744',
    created: 1578436910000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DelegateStewart" rel="noopener noreferrer" target="_blank">@DelegateStewart</a> <a class="mention" href="https://x.com/BernieSanders" rel="noopener noreferrer" target="_blank">@BernieSanders</a> My hometown represent! (I grew up just off of Muncaster Rd).<br><br>In reply to: <a href="https://x.com/DelegateStewart/status/1214637589166034944" rel="noopener noreferrer" target="_blank">@DelegateStewart</a> <span class="status">1214637589166034944</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1214667025567514625',
    created: 1578434160000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mxmkm" rel="noopener noreferrer" target="_blank">@mxmkm</a> Yes, it can be frustrating at times. That\'s the trade-off of having software that can be changed at any time. Bu there are strategies and best practices to make it manageable. And those need to be employed.<br><br>In reply to: <a href="#1214666701079318528">1214666701079318528</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1214666701079318528',
    created: 1578434083000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mxmkm" rel="noopener noreferrer" target="_blank">@mxmkm</a> This is why I recommend using Bundler with a Gemfile. You specify the version matrix for your project and have lots of control over what gets updated and when. And it\'s easy to rollback (since the Gemfile is committed). This challenge is not unique to this project.<br><br>In reply to: <a href="https://x.com/mxmkm/status/1214563455522361346" rel="noopener noreferrer" target="_blank">@mxmkm</a> <span class="status">1214563455522361346</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1214657007174602752',
    created: 1578431772000,
    type: 'reply',
    text: '@jbryant787 We\'ve found a few other small issues too. I\'ll do another release candidate mid-week. We\'re aiming for final this weekend.<br><br>In reply to: @jbryant787 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1214517850632679425',
    created: 1578398594000,
    type: 'post',
    text: 'Note that Asciidoctor PDF will crash when rendering header cells in the table body. There are also small layout inconsistencies when running on Ruby 2.7. Both of these issues will be fixed in the next release candidate.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1214356458332028928',
    created: 1578360115000,
    type: 'post',
    text: 'Branch restrictions are a productivity killer. I recommend against using them unless the repository branch is in a critical path to production. Better is to establish trust in your organization / team.',
    likes: 7,
    retweets: 0
  },
  {
    id: '1214354273405849600',
    created: 1578359595000,
    type: 'reply',
    text: '@brianwisti <a class="mention" href="https://x.com/obra" rel="noopener noreferrer" target="_blank">@obra</a> You seem to be missing Antora. <a href="https://antora.org" rel="noopener noreferrer" target="_blank">antora.org</a>.<br><br>In reply to: @brianwisti <span class="status">&lt;deleted&gt;</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1214296991326302208',
    created: 1578345937000,
    type: 'post',
    text: 'The first release candidate of Asciidoctor PDF 1.5.0 has arrived! Some highlights:<br><br>🧪 900+ tests<br>🔡 Fixed compatibility with ttfunk<br>🏃‍♀️ Can start running content / page numbering after start of body<br>🔆 Line highlighting for source blocks<br><br>See details here: <a href="https://github.com/asciidoctor/asciidoctor-pdf/releases/tag/v1.5.0.rc.1" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-pdf/releases/tag/v1.5.0.rc.1</a>',
    likes: 95,
    retweets: 29
  },
  {
    id: '1214288922701754368',
    created: 1578344014000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/vmassol" rel="noopener noreferrer" target="_blank">@vmassol</a> <a class="mention" href="https://x.com/bobbytank42" rel="noopener noreferrer" target="_blank">@bobbytank42</a> <a class="mention" href="https://x.com/glaforge" rel="noopener noreferrer" target="_blank">@glaforge</a> <a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/ApacheGroovy" rel="noopener noreferrer" target="_blank">@ApacheGroovy</a> <a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> Parsing inline elements into an AST will require coordination with the spec. It\'s simply not something that can be addressed in Asciidoctor alone. It\'s a key goal of the AsciiDoc spec process and reference implementation. We\'ll have news about it soon.<br><br>In reply to: <a href="https://x.com/vmassol/status/1214267595018428416" rel="noopener noreferrer" target="_blank">@vmassol</a> <span class="status">1214267595018428416</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1214256785437032448',
    created: 1578336352000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/benignbala" rel="noopener noreferrer" target="_blank">@benignbala</a> <a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> <a class="mention" href="https://x.com/vmassol" rel="noopener noreferrer" target="_blank">@vmassol</a> <a class="mention" href="https://x.com/glaforge" rel="noopener noreferrer" target="_blank">@glaforge</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/ApacheGroovy" rel="noopener noreferrer" target="_blank">@ApacheGroovy</a> <a class="mention" href="https://x.com/bobbytank42" rel="noopener noreferrer" target="_blank">@bobbytank42</a> <a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> <a class="mention" href="https://x.com/ysb33r" rel="noopener noreferrer" target="_blank">@ysb33r</a> Asciidoctor is a mixed AST and streaming parser. The streaming happens for inline elements. A key goal of the spec and reference impl will be a full AST parser. However, it requires language changes, hence the delay.<br><br>In reply to: <a href="https://x.com/benignbala/status/1214226083739914247" rel="noopener noreferrer" target="_blank">@benignbala</a> <span class="status">1214226083739914247</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1213937782537670656',
    created: 1578260295000,
    type: 'reply',
    text: '@SeaSparks21 I\'m pretty dreadful with hardware too. I have a tendency to make it worse when I touch it. I like the safe space of software where a reboot always gives you a clean slate.<br><br>In reply to: @SeaSparks21 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1213733481441742848',
    created: 1578211586000,
    type: 'post',
    text: 'Came across this handy article for when you can\'t remember the key combo to enter BIOS. <a href="https://www.groovypost.com/howto/bios-uefi-setup-guide-boot-from-cd-dvd-usb-drive-sd-card/" rel="noopener noreferrer" target="_blank">www.groovypost.com/howto/bios-uefi-setup-guide-boot-from-cd-dvd-usb-drive-sd-card/</a> (F2 for Dell XPS)',
    likes: 1,
    retweets: 1
  },
  {
    id: '1213733232224526341',
    created: 1578211527000,
    type: 'reply',
    text: '@SeaSparks21 It\'s a great way to debug hardware problems independent of OS. I\'ve used the live USB media many times in my life for that purpose. Make sure the BIOS is configured to boot to USB: <a href="https://www.groovypost.com/howto/bios-uefi-setup-guide-boot-from-cd-dvd-usb-drive-sd-card/" rel="noopener noreferrer" target="_blank">www.groovypost.com/howto/bios-uefi-setup-guide-boot-from-cd-dvd-usb-drive-sd-card/</a> If it won\'t run with the live USB media, then the computer is likely toast.<br><br>In reply to: <a href="#1213732523387187202">1213732523387187202</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1213732523387187202',
    created: 1578211358000,
    type: 'reply',
    text: '@SeaSparks21 That\'s a tough one. It could be a lot of things. That\'s a reason I like the live USB media (copy the installer to a USB drive &amp; boot with it plugged in). It then runs Linux off the USB instead of the harddisk, only requiring a working CPU + graphics card. <a href="https://fedoramagazine.org/make-fedora-usb-stick/" rel="noopener noreferrer" target="_blank">fedoramagazine.org/make-fedora-usb-stick/</a><br><br>In reply to: @SeaSparks21 <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1213692588974166016',
    created: 1578201837000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/phillip_webb" rel="noopener noreferrer" target="_blank">@phillip_webb</a> <a class="mention" href="https://x.com/_fletchr" rel="noopener noreferrer" target="_blank">@_fletchr</a> Exactly.<br><br>In reply to: <a href="https://x.com/phillip_webb/status/1213692391661527040" rel="noopener noreferrer" target="_blank">@phillip_webb</a> <span class="status">1213692391661527040</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1213692158407954432',
    created: 1578201734000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/phillip_webb" rel="noopener noreferrer" target="_blank">@phillip_webb</a> Now you have to interpret all the symbols. 🤦<br><br>In reply to: <a href="https://x.com/phillip_webb/status/1213688414429573120" rel="noopener noreferrer" target="_blank">@phillip_webb</a> <span class="status">1213688414429573120</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1213687579343589377',
    created: 1578200642000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/phillip_webb" rel="noopener noreferrer" target="_blank">@phillip_webb</a> I spent part of the day looking at cables trying to figure out what they go to and which way data flows. Cables have become a bird\'s nest.<br><br>In reply to: <a href="https://x.com/phillip_webb/status/1213686896200577024" rel="noopener noreferrer" target="_blank">@phillip_webb</a> <span class="status">1213686896200577024</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1213553964957884417',
    created: 1578168786000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/evanchooly" rel="noopener noreferrer" target="_blank">@evanchooly</a> Given that lives are at stake, it\'s a distinction worth making.<br><br>In reply to: <a href="https://x.com/evanchooly/status/1213538251006500864" rel="noopener noreferrer" target="_blank">@evanchooly</a> <span class="status">1213538251006500864</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1213533529729945600',
    created: 1578163914000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/evanchooly" rel="noopener noreferrer" target="_blank">@evanchooly</a> I stand by my statement. Defense is not war. War is a choice. Defense is not. In the US, the whole meaning of defense has been bastardized. We don\'t even attempt diplomacy anymore. We manufacture a conflict, then use it as an excuse to attack. No UN country should start a war.<br><br>In reply to: <a href="https://x.com/evanchooly/status/1213484228882714626" rel="noopener noreferrer" target="_blank">@evanchooly</a> <span class="status">1213484228882714626</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1213359427434082304',
    created: 1578122405000,
    type: 'reply',
    text: '@delfanbaum I 100% think this, and I cringe every time I have to type it. That\'s why we try to refer to man(ual) page wherever possible in the docs.<br><br>In reply to: @delfanbaum <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1213222531227717633',
    created: 1578089766000,
    type: 'post',
    text: 'In Firefox, is it possible to enable touch and hold to stop the kinetic (apz) scrolling? I can tap (down and up), but that\'s different to how it works on a touch device. I want touchstart to pause the scrolling.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1213205875898191872',
    created: 1578085795000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jbaruch" rel="noopener noreferrer" target="_blank">@jbaruch</a> Understood.<br><br>In reply to: <a href="https://x.com/jbaruch/status/1213200382027091969" rel="noopener noreferrer" target="_blank">@jbaruch</a> <span class="status">1213200382027091969</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1213202550821810177',
    created: 1578085002000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jbaruch" rel="noopener noreferrer" target="_blank">@jbaruch</a> We had no UN back then. The whole point of the UN is to end wars. War does not have to be the solution, not anymore.<br><br>In reply to: <a href="https://x.com/jbaruch/status/1213200382027091969" rel="noopener noreferrer" target="_blank">@jbaruch</a> <span class="status">1213200382027091969</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1213199148440272898',
    created: 1578084191000,
    type: 'post',
    text: 'I just learned that <a class="mention" href="https://x.com/janeosanders" rel="noopener noreferrer" target="_blank">@janeosanders</a> &amp; <a class="mention" href="https://x.com/GretaThunberg" rel="noopener noreferrer" target="_blank">@GretaThunberg</a> share the same birthday...and that day is today.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1213195294633914368',
    created: 1578083272000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jbaruch" rel="noopener noreferrer" target="_blank">@jbaruch</a> It\'s disingenuous to compare that to anything that\'s going on right now. These are interventionist wars with absolutely no moral stance. The US is the agitator here. The US is the one committing war crimes.<br><br>In reply to: <a href="https://x.com/jbaruch/status/1213181798596915201" rel="noopener noreferrer" target="_blank">@jbaruch</a> <span class="status">1213181798596915201</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1213164787766448128',
    created: 1578075999000,
    type: 'post',
    text: 'Any candidate who cannot condemn war outright needs to get the hell out of politics. War is never the solution, no matter how you try to spin it.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1213057490906046464',
    created: 1578050418000,
    type: 'post',
    text: 'Up and running on Fedora 31. It\'s such a fantastic desktop.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1212858050530304000',
    created: 1578002867000,
    type: 'post',
    text: 'I figured it out! I had disabled wayland in gdm a few versions ago, which was preventing it from running the desktop. With that gone, it just works.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1212551631956410368',
    created: 1577929811000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> 100% And even worse, it\'s a very narrow demographic.<br><br>In reply to: <a href="https://x.com/starbuxman/status/1212550412177305600" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">1212550412177305600</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1212487726143954944',
    created: 1577914575000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mmatloka" rel="noopener noreferrer" target="_blank">@mmatloka</a> @CRosserAuthor It now scales per display. The exception being my initial post, which I\'m still trying to figure out why it isn\'t linked to that setting.<br><br>In reply to: <a href="https://x.com/mmatloka/status/1212415818195251202" rel="noopener noreferrer" target="_blank">@mmatloka</a> <span class="status">1212415818195251202</span>',
    likes: 1,
    retweets: 0
  }
])
