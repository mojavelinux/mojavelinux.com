;(function () {
  var selectorNames = ['year', 'type', 'content']
  var years = Array.prototype.slice.call(document.querySelectorAll('.tweets .year')).map(function (el) {
    return el.dataset.year
  })
  var head = document.head
  var fullName = document.querySelector('.profile .full-name').textContent
  var screenName = document.querySelector('.profile .screen-name').textContent

  function getTweetElementById (id) {
    var el = document.getElementById(id)
    if (el && (el.classList.contains('tweet') || el.closest('.tweets'))) return el
  }

  function getTweets () {
    return document.querySelector('.tweets')
  }

  function loadTweets (year) {
    var src = 'tweets-' + year + '.js'
    if (head.querySelector('script[src="' + src + '"]')) return
    head.appendChild(Object.assign(document.createElement('script'), { src: src }))
  }

  function onFilterChange () {
    var tweets = getTweets()
    var name = this.className
    var resetScrollPosition = true
    if (this.value) {
      if (name === 'year') {
        var year = this.value
        if (year === '*') {
          resetScrollPosition = false
          years.forEach(loadTweets)
        } else {
          var styleEl = (
            document.querySelector('style.years') ||
            head.appendChild(Object.assign(document.createElement('style'), { className: 'years' }))
          )
          if (!styleEl.classList.contains('year-' + year)) {
            styleEl.classList.add('year-' + year)
            var styleSheet = styleEl.sheet
            var rule = '.tweets.filter-by-year[data-year="' + year + '"] .year:not([data-year="' + year + '"]) .tweet { display: none; }'
            styleSheet.insertRule(rule, styleSheet.cssRules.length)
          }
          loadTweets(year)
        }
      }
      tweets.classList.add('filter-by-' + name)
      tweets.dataset[name] = this.value
    } else {
      if (name === 'year' && years.length) loadTweets(years[0]) // in case default year wasn't loaded on page load
      tweets.classList.remove('filter-by-' + name)
      delete tweets.dataset[name]
    }
    if (resetScrollPosition) tweets.scrollTop = 0
  }

  function onHashChange (e) {
    var target
    var newHash = new URL(e.newURL).hash
    if (newHash) {
      if ((target = getTweetElementById(newHash.slice(1)))) {
        var yearSelector = document.querySelector('.search .year')
        yearSelector.value = target.parentNode.dataset.year
        onFilterChange.call(yearSelector)
      }
      return
    }
    var oldHash = new URL(e.oldURL).hash
    if (oldHash && (target = getTweetElementById(oldHash.slice(1)))) target.scrollIntoView()
  }

  function writeToClipboard (text) {
    window.navigator.clipboard.writeText(text).then(function () {}, function () {})
  }

  selectorNames.forEach(function (selectorName) {
    document.querySelector('.search .' + selectorName).addEventListener('change', onFilterChange)
  })

  document.querySelector('.search .reset').addEventListener('click', function () {
    var tweets = getTweets()
    selectorNames.forEach(function (selectorName) {
      var selector = document.querySelector('.search .' + selectorName)
      selector.selectedIndex = 0
    })
    selectorNames.forEach(function (selectorName) {
      var selector = document.querySelector('.search .' + selectorName)
      onFilterChange.call(selector)
    })
    return false
  })

  getTweets().addEventListener('click', function (e) {
    if (!(e.altKey && e.target.className === 'timestamp')) return true
    e.stopPropagation()
    e.preventDefault()
    window.navigator.clipboard.writeText(e.target.closest('.tweet').id)
    return false
  })

  Object.assign(document.querySelector('.search .tag'), { value: '' }).addEventListener('keydown', function (e) {
    if (e.key !== 'Enter') return
    var tweets = getTweets()
    var tag = (this.value = this.value.replace(/[^a-zA-Z0-9_]/g, '')).toLowerCase()
    if (tag && /^\d+$/.test(tag)) {
      var target = getTweetElementById(tag)
      if (target) {
        window.location.hash = tag
        this.value = tag = ''
      }
    }
    if (tag) {
      var styleEl = (
        document.querySelector('style.tags') ||
        head.appendChild(Object.assign(document.createElement('style'), { className: 'tags' }))
      )
      var tagClass = 'tag-' + tag
      if (!styleEl.classList.contains(tagClass)) {
        styleEl.classList.add(tagClass)
        var styleSheet = styleEl.sheet
        var rule = '.tweets.filter-by-tag[data-tag="' + tag + '"] .tweet:not(.' + tagClass + ') { display: none; }'
        styleSheet.insertRule(rule, styleSheet.cssRules.length)
      }
      tweets.classList.add('filter-by-tag')
      tweets.dataset.tag = tag
    } else {
      tweets.classList.remove('filter-by-tag')
      delete tweets.dataset.tag
    }
  })

  window.addEventListener('DOMContentLoaded', function () {
    var yearSelector = document.querySelector('.search .year')
    if (years.length) {
      head
        .appendChild(Object.assign(document.createElement('style'), { className: 'years' }))
        .sheet
        .insertRule('.tweets:not(.filter-by-year) .year:not([data-year="' + years[0] + '"]) .tweet { display: none; }', 0)
      years.forEach(function (year) {
        yearSelector.appendChild(Object.assign(document.createElement('option'), { value: year, textContent: year }))
      })
    }
    getTweets().scrollTop = 0
    var initialHash = window.location.hash
    var initialYear = years.length && years[0]
    if (initialHash) {
      var target = getTweetElementById(initialHash.slice(1))
      if (target) {
        initialYear = undefined
        yearSelector.value = target.parentNode.dataset.year
        onFilterChange.call(yearSelector)
      }
    }
    if (initialYear) loadTweets(initialYear)
    window.addEventListener('hashchange', onHashChange)
  })

  window.inflateTweets = function (data) {
    if (!data.length) return
    var year = new Date(data[0].created).getFullYear()
    var groupEl = document.querySelector('.tweets .year[data-year="' + year + '"]')
    // if hasTarget is true, inflate each existing entry to preserve target; otherwise, bulk inflate
    var hasTarget = !!groupEl.querySelector(':target')
    var result = data.reduce(function (accum, tweet) {
      var id = tweet.id
      var className = 'tweet ' + tweet.type
      var tags = tweet.tags || []
      for (var i = 0, len = tags.length; i < len; i++) className += ' tag-' + tags[i]
      var date = new Date(tweet.created)
      var htmlContent = '<div class="avatar"><svg><use xlink:href="#avatar"/></svg></div><div class="body"><div class="header"><span class="full-name">' + fullName + '</span> <span class="screen-name">' + screenName + '</span> <span class="time"> <a href="#' + id + '" class="timestamp" title="' + date.toISOString().split('.', 1)[0].replace('T', ' ') + '">' + date.toLocaleString('en-us', { day: 'numeric', month: 'short', year: 'numeric' }) + '</a></span></div><div class="text">' + tweet.text + '</div>' + (tweet.photos ? '<div class="photos">' + tweet.photos.join('') + '</div>' : '') + '<div class="footer"><div class="actions"><button aria-label="Retweet" class="retweet" disabled=""><span class="icon icon-retweet"></span> <span class="count">' + tweet.retweets + '</span></button> <button aria-label="Like" class="like" disabled=""><span class="icon icon-heart"></span> <span class="count">' + tweet.likes + '</span></button></div></div></div>'
      if (hasTarget) {
        var tweetEl = accum && accum.nextElementSibling
        if (!(tweetEl && tweetEl.id === id)) tweetEl = document.getElementById(id)
        tweetEl.className = className
        tweetEl.innerHTML = htmlContent
        return tweetEl
      }
      ;(accum || (accum = [])).push('<div id="' + id + '" class="' + className + '" data-year="' + year + '">' + htmlContent + '</div>')
      return accum
    }, undefined)
    if (!hasTarget) groupEl.innerHTML = result.join('\n')
  }
})()
