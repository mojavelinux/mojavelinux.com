'use strict'

inflateTweets([
  {
    id: '1739363612794495484',
    created: 1703531575000,
    type: 'post',
    text: 'I\'m (not) dreaming of a white Christmas. 🎄☃️',
    photos: ['<div class="item"><img class="photo" src="media/1739363612794495484.jpg"></div>'],
    likes: 5,
    retweets: 0
  },
  {
    id: '1737938272255357426',
    created: 1703191748000,
    type: 'post',
    text: '"I see no ability to embed reusable content with AsciiDoc."<br><br>Tell me you didn\'t read the documentation without telling me you didn\'t read the documentation ;)',
    likes: 7,
    retweets: 0
  },
  {
    id: '1737015087976591865',
    created: 1702971643000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/saronyitbarek" rel="noopener noreferrer" target="_blank">@saronyitbarek</a> Nikki is by far the most entertaining, CPK will push you to new heights, Ellen will always make you feel joyful, Phoenix will pump you up, and Robyn will help you connect with your body. And there are many others. They all make exercising fun, esp for those who hate exercising.<br><br>In reply to: <a href="#1737013296543244580">1737013296543244580</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1737013296543244580',
    created: 1702971216000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/saronyitbarek" rel="noopener noreferrer" target="_blank">@saronyitbarek</a> To me, Daily Burn proves that a little bit goes a long way and that working out can be something you look forward to. We laugh just as hard as we breathe. That makes it part of life rather than a timeout from it. JD (anchor + regular participant) gives great motivational talks.<br><br>In reply to: <a href="#1737012252593521023">1737012252593521023</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1737012252593521023',
    created: 1702970967000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/saronyitbarek" rel="noopener noreferrer" target="_blank">@saronyitbarek</a> I\'ll explain why Daily Burn worked for us, as we definitely came from a "we don\'t need no exercise" mentally. 1st, the workouts are bit-sized + achievable (some as short as 5min, most ~25. 2nd, we can do them right in front of the couch. 3rd, the trainers have huge personalities.<br><br>In reply to: <a href="#1736923375094608251">1736923375094608251</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1736923375094608251',
    created: 1702949777000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/saronyitbarek" rel="noopener noreferrer" target="_blank">@saronyitbarek</a> Daily Burn and doing it with my spouse.<br><br>When I don\'t feel like a guided workout, I go practice lacrosse by throwing the ball against a wall (though I realize that\'s very specific...but it did work for me).<br><br>In reply to: <a href="https://x.com/saronyitbarek/status/1736922449210728929" rel="noopener noreferrer" target="_blank">@saronyitbarek</a> <span class="status">1736922449210728929</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1736899626198880569',
    created: 1702944115000,
    type: 'quote',
    text: 'Maybe not the Yule Log we had in mind this year, but then again, it is 2023, so yeah.<br><br>Quoting: <a href="https://x.com/hafsteinn/status/1736882893723402559" rel="noopener noreferrer" target="_blank">@hafsteinn</a> <span class="status">1736882893723402559</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1733786410396668389',
    created: 1702201867000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ktosopl" rel="noopener noreferrer" target="_blank">@ktosopl</a> That definitely sounds like a bad lesson &amp; I agree you shouldn\'t let it discourage you. As someone who worked very hard at becoming proficient at an instrument, I can share that it can be useful to push yourself to sight read a tough piece, but not when you\'re just starting out.<br><br>In reply to: <a href="https://x.com/ktosopl/status/1733759003375939907" rel="noopener noreferrer" target="_blank">@ktosopl</a> <span class="status">1733759003375939907</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1731488256578183410',
    created: 1701653944000,
    type: 'post',
    text: 'Firefox is crashing on my Android phone the first time I try to load any web page. I guess I need to clear my data and reinstall, but geez that is disruptive.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1730718089774293151',
    created: 1701470322000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> That\'s the one I have my eye on too. Good choice!<br><br>In reply to: <a href="https://x.com/headius/status/1730716822658318664" rel="noopener noreferrer" target="_blank">@headius</a> <span class="status">1730716822658318664</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1730713548907499854',
    created: 1701469239000,
    type: 'quote',
    text: 'Game on!<br><br>Quoting: <a href="https://x.com/NLL/status/1730700850514088028" rel="noopener noreferrer" target="_blank">@NLL</a> <span class="status">1730700850514088028</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1730669157660684415',
    created: 1701458656000,
    type: 'post',
    text: 'I will never not want to use `bundle --path=.bundle/gems` (or whatever local path is appropriate). My brain simply cannot remember the extra step `bundle config set path .bundle/gems` without looking it up. Why they deprecated the flag is beyond me.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1730011071283356144',
    created: 1701301756000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> IMO Jeff Trainor has been one of the most exciting players to watch this past year. Dude has wheels.<br><br>In reply to: <a href="https://x.com/danarestia/status/1729875979445969102" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1729875979445969102</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1729827647344009624',
    created: 1701258024000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sdelamo" rel="noopener noreferrer" target="_blank">@sdelamo</a> <a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> <a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> <a class="mention" href="https://x.com/jetbrains" rel="noopener noreferrer" target="_blank">@jetbrains</a> <a class="mention" href="https://x.com/iAWriter" rel="noopener noreferrer" target="_blank">@iAWriter</a> <a class="mention" href="https://x.com/intellijidea" rel="noopener noreferrer" target="_blank">@intellijidea</a> <a class="mention" href="https://x.com/intellij" rel="noopener noreferrer" target="_blank">@intellij</a> I anticipate that the completion of the AsciiDoc spec will create a new market for editors (partly because of momentum, but mostly because they will become easier to create). Still, I think the "AsciiDoc doesn\'t have good editors" is just a parroted, baseless claim.<br><br>In reply to: <a href="https://x.com/sdelamo/status/1729825548493340837" rel="noopener noreferrer" target="_blank">@sdelamo</a> <span class="status">1729825548493340837</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1729824455449260116',
    created: 1701257263000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sdelamo" rel="noopener noreferrer" target="_blank">@sdelamo</a> <a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> <a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> <a class="mention" href="https://x.com/jetbrains" rel="noopener noreferrer" target="_blank">@jetbrains</a> <a class="mention" href="https://x.com/iAWriter" rel="noopener noreferrer" target="_blank">@iAWriter</a> <a class="mention" href="https://x.com/intellijidea" rel="noopener noreferrer" target="_blank">@intellijidea</a> My point is, people are in the habit of saying something they don\'t actually want. No one wants a half-baked tool. We want a tool that actually assists. And for what it offers, I think IntelliJ strikes a very good balance between speed and functionality. Most users I know agree.<br><br>In reply to: <a href="#1729823964803850401">1729823964803850401</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1729823964803850401',
    created: 1701257146000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sdelamo" rel="noopener noreferrer" target="_blank">@sdelamo</a> <a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> <a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> <a class="mention" href="https://x.com/jetbrains" rel="noopener noreferrer" target="_blank">@jetbrains</a> <a class="mention" href="https://x.com/iAWriter" rel="noopener noreferrer" target="_blank">@iAWriter</a> <a class="mention" href="https://x.com/intellijidea" rel="noopener noreferrer" target="_blank">@intellijidea</a> I don\'t really get this illusion about a "light" editor. You (as in people) want to do serious writing, but you also want the tool to have limited functionality? I don\'t get it. Writing is serious and you need serious tooling to do it. The IntelliJ plugin provides that.<br><br>In reply to: <a href="https://x.com/sdelamo/status/1729821203269857503" rel="noopener noreferrer" target="_blank">@sdelamo</a> <span class="status">1729821203269857503</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1729639176335692021',
    created: 1701213089000,
    type: 'quote',
    text: 'How it all started. #asciidoctor<br><br>Btw, asciidoc was not a gem at that time, but rather a Python package. We now refer to it as AsciiDoc py, the legacy AsciiDoc processor. AsciiDoc has moved on since then.<br><br>Quoting: <a href="https://x.com/runemadsen/status/1729463989728403909" rel="noopener noreferrer" target="_blank">@runemadsen</a> <span class="status">1729463989728403909</span>',
    likes: 8,
    retweets: 2,
    tags: ['asciidoctor']
  },
  {
    id: '1729636677256192147',
    created: 1701212493000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/runemadsen" rel="noopener noreferrer" target="_blank">@runemadsen</a> <a class="mention" href="https://x.com/OReillyMedia" rel="noopener noreferrer" target="_blank">@OReillyMedia</a> <a class="mention" href="https://x.com/odewahn" rel="noopener noreferrer" target="_blank">@odewahn</a> <a class="mention" href="https://x.com/GitHubEng" rel="noopener noreferrer" target="_blank">@GitHubEng</a> It\'s cool to have this context btw because I never knew the origin of that incident report.<br><br>In reply to: <a href="#1729636432744980965">1729636432744980965</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1729636432744980965',
    created: 1701212435000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/runemadsen" rel="noopener noreferrer" target="_blank">@runemadsen</a> <a class="mention" href="https://x.com/OReillyMedia" rel="noopener noreferrer" target="_blank">@OReillyMedia</a> <a class="mention" href="https://x.com/odewahn" rel="noopener noreferrer" target="_blank">@odewahn</a> <a class="mention" href="https://x.com/GitHubEng" rel="noopener noreferrer" target="_blank">@GitHubEng</a> I\'m also grateful that incident was reported because it forever changed my career (and truthfully, my life too).<br><br>In reply to: <a href="#1729635421267624194">1729635421267624194</a>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1729636116876100060',
    created: 1701212360000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sdelamo" rel="noopener noreferrer" target="_blank">@sdelamo</a> <a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> <a class="mention" href="https://x.com/jetbrains" rel="noopener noreferrer" target="_blank">@jetbrains</a> <a class="mention" href="https://x.com/iAWriter" rel="noopener noreferrer" target="_blank">@iAWriter</a> I happen to think the AsciiDoc editor in IntelliJ is pretty incredible. The claim that AsciiDoc doesn\'t have good tooling support is a myth. Though we\'re far from pencils down. Still plenty of work to do. That\'s what I hope the tooling group in the AsciiDoc WG will drive forward.<br><br>In reply to: <a href="https://x.com/sdelamo/status/1729545003548991802" rel="noopener noreferrer" target="_blank">@sdelamo</a> <span class="status">1729545003548991802</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1729635421267624194',
    created: 1701212194000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/runemadsen" rel="noopener noreferrer" target="_blank">@runemadsen</a> <a class="mention" href="https://x.com/OReillyMedia" rel="noopener noreferrer" target="_blank">@OReillyMedia</a> <a class="mention" href="https://x.com/odewahn" rel="noopener noreferrer" target="_blank">@odewahn</a> <a class="mention" href="https://x.com/GitHubEng" rel="noopener noreferrer" target="_blank">@GitHubEng</a> I point that out because the development happened outside of GitHub in the open and those security requirements and pluggability still influence the development of Asciidoctor today.<br><br>In reply to: <a href="#1729635014726267205">1729635014726267205</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1729635014726267205',
    created: 1701212097000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/runemadsen" rel="noopener noreferrer" target="_blank">@runemadsen</a> <a class="mention" href="https://x.com/OReillyMedia" rel="noopener noreferrer" target="_blank">@OReillyMedia</a> <a class="mention" href="https://x.com/odewahn" rel="noopener noreferrer" target="_blank">@odewahn</a> <a class="mention" href="https://x.com/GitHubEng" rel="noopener noreferrer" target="_blank">@GitHubEng</a> It was me who primarily worked on that replacement. GitHub started the project, but I pushed extremely hard to get it into a viable state so it could be swapped back. I did it because I believed in AsciiDoc (&amp; still do). That effort eventually led to me starting my own business.<br><br>In reply to: <a href="https://x.com/runemadsen/status/1729466335564878183" rel="noopener noreferrer" target="_blank">@runemadsen</a> <span class="status">1729466335564878183</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1729425006101438971',
    created: 1701162027000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/HFXThunderbirds" rel="noopener noreferrer" target="_blank">@HFXThunderbirds</a> That\'s a solid team right there. Best of luck (though it\'s really all skill)!<br><br>In reply to: <a href="https://x.com/HFXThunderbirds/status/1729293237628625311" rel="noopener noreferrer" target="_blank">@HFXThunderbirds</a> <span class="status">1729293237628625311</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1729424095614218311',
    created: 1701161810000,
    type: 'post',
    text: '#hugops to all my Spring friends. May the 🌥️ break through.',
    likes: 7,
    retweets: 0,
    tags: ['hugops']
  },
  {
    id: '1729423846887854374',
    created: 1701161750000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> #hugops<br><br>In reply to: <a href="https://x.com/abelsromero/status/1729420089215086673" rel="noopener noreferrer" target="_blank">@abelsromero</a> <span class="status">1729420089215086673</span>',
    likes: 1,
    retweets: 0,
    tags: ['hugops']
  },
  {
    id: '1727092771750633891',
    created: 1700605979000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/topicmeister" rel="noopener noreferrer" target="_blank">@topicmeister</a> Thank you. And I do enjoy putting in the work to create them. With that said, boundaries need to be clarified from time to time so I can continue to do so.<br><br>In reply to: <a href="https://x.com/topicmeister/status/1727089698555015215" rel="noopener noreferrer" target="_blank">@topicmeister</a> <span class="status">1727089698555015215</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1727071839946723801',
    created: 1700600988000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/b_vasilescu" rel="noopener noreferrer" target="_blank">@b_vasilescu</a> <a class="mention" href="https://x.com/aserebrenik" rel="noopener noreferrer" target="_blank">@aserebrenik</a> I see a ton of entitlement and quite a lot of unprofessional behavior. While I can\'t control how people behavior, I can control the standards we uphold as a group in the spaces I run.<br><br>In reply to: <a href="https://x.com/b_vasilescu/status/1727002008924917932" rel="noopener noreferrer" target="_blank">@b_vasilescu</a> <span class="status">1727002008924917932</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1727058817828147561',
    created: 1700597884000,
    type: 'post',
    text: 'And yet, I understand that no one owes me anything for doing so. I chose to do so. But I also don\'t owe anyone anything more, not even if they are throwing a temper tantrum for attention or trying to manipulate me by proclaiming my software will never reach critical mass.',
    likes: 6,
    retweets: 0
  },
  {
    id: '1727058250263347549',
    created: 1700597748000,
    type: 'post',
    text: 'I think people who come into an OSS project demanding attention, whether it be to implement their feature request or review their PR have no idea that open source maintainers spend an entire additional work day every day volunteering to maintain projects.',
    likes: 6,
    retweets: 0
  },
  {
    id: '1727057481220964353',
    created: 1700597565000,
    type: 'post',
    text: 'Drive-by help is not help. Committing to helping anywhere near the degree to which the maintainer is putting in the work is actual help. Try to live in their shoes for a week or even a day.',
    likes: 6,
    retweets: 0
  },
  {
    id: '1727056963517710835',
    created: 1700597441000,
    type: 'post',
    text: 'To be abundantly clear, open source maintainers own you absolutely nothing. They may choose to give more, and often will, but that doesn\'t change the fact that they\'re under no obligation to do so. The best way to keep a project moving forward is to offer to help &amp; keep doing so.',
    likes: 66,
    retweets: 15
  },
  {
    id: '1727056339606855944',
    created: 1700597293000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/siedlerchr" rel="noopener noreferrer" target="_blank">@siedlerchr</a> There just seems to be this misconception that open source maintainers owe anyone anything. They just don\'t. The software is provided as is. You can submit feedback and it may or may not get used. No one promised you anything. (If you hire them, different story entirely).<br><br>In reply to: <a href="#1727055884386541804">1727055884386541804</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1727055884386541804',
    created: 1700597184000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/siedlerchr" rel="noopener noreferrer" target="_blank">@siedlerchr</a> First, I didn\'t say there would be no activity. I said that PRs might be in a queue and you just have to wait your turn. That could take 2-3 years (in my own experience, even trying my hardest). But you\'re also not forced to use any project you don\'t want to use.<br><br>In reply to: <a href="https://x.com/siedlerchr/status/1727054153816625277" rel="noopener noreferrer" target="_blank">@siedlerchr</a> <span class="status">1727054153816625277</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1727053295611682998',
    created: 1700596567000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/siedlerchr" rel="noopener noreferrer" target="_blank">@siedlerchr</a> I don\'t agree with that assessment. It\'s not the maintainers responsibility to address PRs in a timely manner. Yes, they should not be ignored, but I think 2-3 years is more reasonable. If someone comes forward to help maintain the project, that should be addressed more quickly.<br><br>In reply to: <a href="https://x.com/siedlerchr/status/1727051595995828299" rel="noopener noreferrer" target="_blank">@siedlerchr</a> <span class="status">1727051595995828299</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1727049345038733821',
    created: 1700595625000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/siedlerchr" rel="noopener noreferrer" target="_blank">@siedlerchr</a> In open source, a year is practically a blink of an eye. And that often happens because the maintainer is completely overwhelmed and it takes a year just to get through the backlog. But sure, keep blaming open source maintainers for doing work for free.<br><br>In reply to: <a href="https://x.com/siedlerchr/status/1727048390243209528" rel="noopener noreferrer" target="_blank">@siedlerchr</a> <span class="status">1727048390243209528</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1727048814333477052',
    created: 1700595499000,
    type: 'post',
    text: 'When following Semver, major versions do provide an opportunity to break APIs, but not across the whole library. With npm packages, I find myself having to rewrite my code with every major package release, which is taking the break API idea entirely too far.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1727048179697479956',
    created: 1700595347000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SheltonLouisX" rel="noopener noreferrer" target="_blank">@SheltonLouisX</a> Even for projects that follow it, they exercise the break API too liberally so you get exhausted having to rewrite code. While Semver permits that, there\'s an understanding that breaking APIs should be used sparingly to retain the integrity of the library. npm packages abuse it.<br><br>In reply to: <a href="https://x.com/SheltonLouisX/status/1726984397558342006" rel="noopener noreferrer" target="_blank">@SheltonLouisX</a> <span class="status">1726984397558342006</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1726870185955701228',
    created: 1700552910000,
    type: 'post',
    text: 'In contrast, Node.js itself and the stdlib are incredibly stable, complete, and reliable, which makes the goal of zero dependencies very reasonable to achieve.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1726869913674117344',
    created: 1700552845000,
    type: 'post',
    text: 'The culture in the npm package ecosystem seems to be to break the API on every major (and often minor) release. It\'s so self-centered. This is a large part why I\'m gradually moving towards zero dependencies wherever possible. That way, stable stuff can just remain stable ffs.',
    likes: 12,
    retweets: 3
  },
  {
    id: '1726775515296526587',
    created: 1700530339000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> Even family members who have known Sarah for two decades still write her name as Sara. Every time I see, I just think "wow, that is just so lazy".<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1726736877779288091" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1726736877779288091</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1726734575420776651',
    created: 1700520578000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> You also have to understand that your want may be rejected as out of scope. It has to be good for the greater good/group, not a selfish thing.<br><br>In reply to: <a href="#1726734235216535570">1726734235216535570</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1726734235216535570',
    created: 1700520497000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> I agree we can (and in many cases should) push for a contract, but that doesn\'t immediately address the problem of people not getting the dynamics of how open source (or community) works. If you want something, you need to be the person to add it (or commission it).<br><br>In reply to: <a href="https://x.com/brunoborges/status/1726732003620647014" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1726732003620647014</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1726720731185381691',
    created: 1700517277000,
    type: 'post',
    text: 'I\'m really frustrated that open source is becoming "Please implement this. I don\'t have time, but I expect that you do." If you find yourself doing that, don\'t.',
    likes: 45,
    retweets: 6
  },
  {
    id: '1726713575614578921',
    created: 1700515571000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> True, but that change has to come from putting our foot down. We can\'t just expect people to change unprompted. But we can become less available to those who refuse to respect boundaries. I\'ve been working on doing that and it\'s starting to work (at least within my own sphere).<br><br>In reply to: <a href="https://x.com/marcsavy/status/1726566689020809628" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1726566689020809628</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1726564151101882827',
    created: 1700479946000,
    type: 'post',
    text: 'Open source is not "I want, I want, I want." It\'s "I can solve that, I can solve that, I can solve that." You get out what you put in.',
    likes: 5,
    retweets: 0
  },
  {
    id: '1726343029261525501',
    created: 1700427226000,
    type: 'post',
    text: 'I\'m just happy to be able to watch great PLL games, but if I had to decide on cities, I\'d have made these revisions:<br><br>Chaos -&gt; NY (Banditland)<br>Atlas -&gt; Denver (strong roots)<br>Chrome -&gt; Carolina (sounds cool)<br><br>What I\'m most excited about is that the #rollwoods roll in Cali. 😎🥍',
    likes: 0,
    retweets: 0,
    tags: ['rollwoods']
  },
  {
    id: '1725974570648166438',
    created: 1700339379000,
    type: 'post',
    text: 'PSA: Oat nog is pretty damn good.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1725401193453699546',
    created: 1700202675000,
    type: 'post',
    text: 'Every time I get a reCAPTCHA right, I feel the way I felt went I didn\'t fail a quiz in high school. 😓',
    likes: 0,
    retweets: 0
  },
  {
    id: '1724961891393421799',
    created: 1700097937000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/oleg_nenashev" rel="noopener noreferrer" target="_blank">@oleg_nenashev</a> 💪<br><br>In reply to: <a href="https://x.com/oleg_nenashev/status/1724860928284320125" rel="noopener noreferrer" target="_blank">@oleg_nenashev</a> <span class="status">1724860928284320125</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1724551984580350359',
    created: 1700000208000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> Denver!<br><br>In reply to: <a href="https://x.com/danarestia/status/1724548978098782500" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1724548978098782500</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1723903113067663758',
    created: 1699845505000,
    type: 'quote',
    text: 'cc: <a class="mention" href="https://x.com/settermjd" rel="noopener noreferrer" target="_blank">@settermjd</a> another book written using Asciidoctor.<br><br>Quoting: <a href="https://x.com/julian_rubisch/status/1723646692895969374" rel="noopener noreferrer" target="_blank">@julian_rubisch</a> <span class="status">1723646692895969374</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1723417735365816759',
    created: 1699729782000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ashleymcnamara" rel="noopener noreferrer" target="_blank">@ashleymcnamara</a> The genuine love. The only way to generate that is from within 😉<br><br>In reply to: <a href="https://x.com/ashleymcnamara/status/1723283292475228209" rel="noopener noreferrer" target="_blank">@ashleymcnamara</a> <span class="status">1723283292475228209</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1723301628416057691',
    created: 1699702100000,
    type: 'reply',
    text: '@AdamLeviNLL I\'m totally fine with keeping box and field. I think those are different sports (under the same umbrella), both of which are awesome and worth preserving. Sixes...still not sure. But it\'s here to stay (at least until LA 2028).<br><br>In reply to: <a href="#1723301026101334150">1723301026101334150</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1723301026101334150',
    created: 1699701956000,
    type: 'reply',
    text: '@AdamLeviNLL It also perpetuates the idea that girls/women have to play with a different set of rules. We need to at least get rid of that fact at the entry level, even if it will take much longer at the competition level.<br><br>In reply to: <a href="#1723300433832079695">1723300433832079695</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1723300433832079695',
    created: 1699701815000,
    type: 'reply',
    text: '@AdamLeviNLL Looking through that book, it clearly highlights the major problem with teaching lacrosse. There\'s no one definition of lacrosse. For beginners, there really needs to be a universal format to start with. It\'s already challenging enough to learn how to catch &amp; throw. My $0.02.<br><br>In reply to: <a href="https://x.com/AdamLeviLAX/status/1723086293196755091" rel="noopener noreferrer" target="_blank">@AdamLeviLAX</a> <span class="status">1723086293196755091</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1723197864430485826',
    created: 1699677361000,
    type: 'post',
    text: 'Under 500 and still going!',
    likes: 1,
    retweets: 0
  },
  {
    id: '1722703098055971094',
    created: 1699559399000,
    type: 'post',
    text: 'See the history of a method using git log -L<br><br><a href="https://calebhearth.com/git-method-history" rel="noopener noreferrer" target="_blank">calebhearth.com/git-method-history</a><br><br>Cool stuff!',
    likes: 5,
    retweets: 1
  },
  {
    id: '1721632623334707221',
    created: 1699304178000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GraemePerrow" rel="noopener noreferrer" target="_blank">@GraemePerrow</a> <a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> 💯<br><br>In reply to: <a href="https://x.com/GraemePerrow/status/1721589202716963122" rel="noopener noreferrer" target="_blank">@GraemePerrow</a> <span class="status">1721589202716963122</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1720899352141422848',
    created: 1699129352000,
    type: 'quote',
    text: 'This was covered in Artifishal almost 5 years ago. Stop buying salmon. Not only are these practices wrong, they\'re also producing very unhealthy fish for you and the environment. <a href="https://www.patagonia.com/stories/artifishal/video-79192.html" rel="noopener noreferrer" target="_blank">www.patagonia.com/stories/artifishal/video-79192.html</a><br><br>Quoting: <a href="https://x.com/humaneleagueuk/status/1720505624532852736" rel="noopener noreferrer" target="_blank">@humaneleagueuk</a> <span class="status">1720505624532852736</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1720745344307531861',
    created: 1699092634000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AndreyHihlovski" rel="noopener noreferrer" target="_blank">@AndreyHihlovski</a> Thanks!<br><br>In reply to: <a href="https://x.com/AndreyHihlovski/status/1720731019676991967" rel="noopener noreferrer" target="_blank">@AndreyHihlovski</a> <span class="status">1720731019676991967</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1720630398764683302',
    created: 1699065229000,
    type: 'post',
    text: 'My phone now dials the emergency service if I don\'t flip it around in my pocket. Technology is regressing. FFS.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1720629930638430286',
    created: 1699065117000,
    type: 'post',
    text: '"You used your phone 2 minutes more this week than last week."<br><br>I care more about the shit under the rim of my toilet than this information. Go to fucking hell with this drivel.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1720611489336213760',
    created: 1699060721000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a><br><br>In reply to: <a href="https://x.com/brunoborges/status/1720607107215736940" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1720607107215736940</span>',
    photos: ['<div class="item"><img class="photo" src="media/1720611489336213760.jpg"></div>'],
    likes: 0,
    retweets: 0
  },
  {
    id: '1720611119281197143',
    created: 1699060632000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> Will do. Cheers!<br><br>In reply to: <a href="https://x.com/brunoborges/status/1720607107215736940" rel="noopener noreferrer" target="_blank">@brunoborges</a> <span class="status">1720607107215736940</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1720535954945933429',
    created: 1699042712000,
    type: 'post',
    text: 'I still feel like there\'s space for a developer to create his or her own company. But having Open Collective in the mix makes that a lot less stressful and scary. Your livelihood doesn\'t depend on your ability to monetize the work you already did (and from which others benefit).',
    likes: 2,
    retweets: 1
  },
  {
    id: '1720535440845902284',
    created: 1699042589000,
    type: 'post',
    text: 'I\'m a bit slow to post this, but still very relevant. <a class="mention" href="https://x.com/opencollect" rel="noopener noreferrer" target="_blank">@opencollect</a> aims to make working full-time for an open source project an alternative to a career developing code for a for-profit company. I wish that existed when I started out because it\'s what I\'ve always wanted.',
    likes: 9,
    retweets: 1
  },
  {
    id: '1720504923912118418',
    created: 1699035313000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/AndreyHihlovski" rel="noopener noreferrer" target="_blank">@AndreyHihlovski</a> OMG, that\'s it!<br><br>In reply to: <a href="https://x.com/AndreyHihlovski/status/1720409342518730887" rel="noopener noreferrer" target="_blank">@AndreyHihlovski</a> <span class="status">1720409342518730887</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1720393040001396770',
    created: 1699008638000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DaschnerS" rel="noopener noreferrer" target="_blank">@DaschnerS</a> And thanks for your tips to reintroduce me to awesome features I overlooked. I\'m still trying to put your tips into practice, but you know how tough it can be to change that muscle memory ;)<br><br>In reply to: <a href="https://x.com/DaschnerS/status/1720347884200112417" rel="noopener noreferrer" target="_blank">@DaschnerS</a> <span class="status">1720347884200112417</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1720318047721021687',
    created: 1698990759000,
    type: 'quote',
    text: 'I share my experience, my very existence, with Vim every day. There\'s never I day I don\'t use it, and often times the files I have open have been open for months. Vim is my digital home.<br><br>Quoting: <a href="https://x.com/itsfoss2/status/1720002971704926225" rel="noopener noreferrer" target="_blank">@itsfoss2</a> <span class="status">1720002971704926225</span>',
    likes: 12,
    retweets: 1
  },
  {
    id: '1720241052899070110',
    created: 1698972402000,
    type: 'post',
    text: 'The struggle I have with saying no is that I want to be known as someone who gets things done — not as someone who says certain things aren’t possible.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1720207633506803793',
    created: 1698964434000,
    type: 'post',
    text: 'I have a <a class="mention" href="https://x.com/badmannersfood" rel="noopener noreferrer" target="_blank">@badmannersfood</a> reader tip. Adding half a peach to Jade &amp; Juice substantially enhances the flavor. It\'s become my daily fix.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1720174873161445724',
    created: 1698956623000,
    type: 'post',
    text: 'As a follow-up to this, I discovered the GitHub for Poets series on YouTube. Now THAT is what a gentle introduction to git looks like. I love it.<br><br>Quoting: <a href="#1719061712312103373">1719061712312103373</a>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1719997874484724148',
    created: 1698914423000,
    type: 'post',
    text: 'If I started a major open source project today, I might go through <a class="mention" href="https://x.com/opencollect" rel="noopener noreferrer" target="_blank">@opencollect</a> from day 1. It\'s about more than just funding. It provides structure to help manage everything that\'s going to happen as the project grows, something we\'ve looked to companies to provide in the past.',
    likes: 5,
    retweets: 0
  },
  {
    id: '1719996032048672997',
    created: 1698913984000,
    type: 'post',
    text: 'I love that <a class="mention" href="https://x.com/opencollect" rel="noopener noreferrer" target="_blank">@opencollect</a> does not require an account to contribute to a collective. For me, it\'s more than just lowering a barrier. It speaks to their core value of helping collectives get support rather than focusing on their own numbers for reports. Thanks <a class="mention" href="https://x.com/opencollect" rel="noopener noreferrer" target="_blank">@opencollect</a>!',
    likes: 4,
    retweets: 0
  },
  {
    id: '1719140431160848446',
    created: 1698709993000,
    type: 'post',
    text: 'I get that a group of people did unspeakable things. It\'s revolting to even think about and shreds my faith in humanity. I wish it never happened. But collectively punishing people with death, even those guilty of the crimes, only brings about more suffering. It\'s wrong too.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1719136675907936637',
    created: 1698709098000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/hsablonniere" rel="noopener noreferrer" target="_blank">@hsablonniere</a> Love them both. The 😸 looks like something you would go for.<br><br>In reply to: <a href="https://x.com/hsablonniere/status/1719109089546916226" rel="noopener noreferrer" target="_blank">@hsablonniere</a> <span class="status">1719109089546916226</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1719133691320975673',
    created: 1698708386000,
    type: 'post',
    text: 'I never thought I\'d get to the point when I\'d have to start filtering my follows using the criteria "turns a blind eye to genocide or encourages it" Wow. This is madness.',
    likes: 8,
    retweets: 0
  },
  {
    id: '1719132948136448501',
    created: 1698708209000,
    type: 'quote',
    text: 'You don\'t fight extremism by being an extremist, escalating tensions, or disregarding international law. Here\'s a refreshingly clear and level-headed statement that emphasizes what we should all be rallying behind for the safety of civilians on both sides, which is peace. 🇮🇱🕊️🇵🇸<br><br>Quoting: <a href="https://x.com/democracynow/status/1718973676593299940" rel="noopener noreferrer" target="_blank">@democracynow</a> <span class="status">1718973676593299940</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1719073999890554978',
    created: 1698694155000,
    type: 'reply',
    text: '@hamzadabaghi <a class="mention" href="https://x.com/moshhamedani" rel="noopener noreferrer" target="_blank">@moshhamedani</a> Oooh, that\'s good. Specifically <a href="https://codewithmosh.com/p/the-ultimate-git-course" rel="noopener noreferrer" target="_blank">codewithmosh.com/p/the-ultimate-git-course</a>. That\'s precisely the kind of resources I\'m looking for.<br><br>In reply to: @hamzadabaghi <span class="status">&lt;deleted&gt;</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1719061712312103373',
    created: 1698691225000,
    type: 'post',
    text: 'If I wanted to help a non-programmer learn how to use git for the first time, what courses would you recommend? I\'m open to both free and paid resources. I\'m not looking for blogs or tutorials that touch on a few aspects, but rather complete educational courses.',
    likes: 4,
    retweets: 1
  },
  {
    id: '1718011186644455831',
    created: 1698440760000,
    type: 'post',
    text: '"Every time you touch some code, clean something up."<br><br>I once worked in a group that forbid you to change the code unless required to complete a story. Needless to say, the code was a damn mess. Let programmers fix it as they see it. You ain\'t coming back.<br><a href="https://blog.testdouble.com/posts/2022-01-20-stop-paying-debts-start-maintaining-code/" rel="noopener noreferrer" target="_blank">blog.testdouble.com/posts/2022-01-20-stop-paying-debts-start-maintaining-code/</a>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1717847581831643247',
    created: 1698401754000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/oleg_nenashev" rel="noopener noreferrer" target="_blank">@oleg_nenashev</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> The benefit of doing it this way is that if the team decides to make the switch, they just need to import the files from the staging repository and they are now switched over to AsciiDoc. So it\'s kind of a lure.<br><br>In reply to: <a href="#1717845568683209176">1717845568683209176</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1717845568683209176',
    created: 1698401274000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/oleg_nenashev" rel="noopener noreferrer" target="_blank">@oleg_nenashev</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> I think it would be a lot of overhead to do that conversion every time Antora runs. A better approach is to use kramdown-asciidoc to convert the Markdown into a staging repository that Antora draws from. I shared a script that does just that in the chat.<br><br>In reply to: <a href="https://x.com/oleg_nenashev/status/1717526356513214749" rel="noopener noreferrer" target="_blank">@oleg_nenashev</a> <span class="status">1717526356513214749</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1716549656719564869',
    created: 1698092304000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MeganMcCubbin" rel="noopener noreferrer" target="_blank">@MeganMcCubbin</a> <a class="mention" href="https://x.com/ChrisGPackham" rel="noopener noreferrer" target="_blank">@ChrisGPackham</a> I cannot wait to watch it. It\'s in our queue for tonight! So glad to see you all back in action.<br><br>In reply to: <a href="https://x.com/MeganMcCubbin/status/1716547466718253294" rel="noopener noreferrer" target="_blank">@MeganMcCubbin</a> <span class="status">1716547466718253294</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1716407634868265454',
    created: 1698058444000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/StringKing" rel="noopener noreferrer" target="_blank">@StringKing</a> You have know idea how much this inspires me. This is joy.<br><br>In reply to: <a href="https://x.com/StringKing/status/1700160423960478049" rel="noopener noreferrer" target="_blank">@StringKing</a> <span class="status">1700160423960478049</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1716339856744370296',
    created: 1698042284000,
    type: 'post',
    text: 'Inbox &lt; 1,000. I\'m calling that a small victory, but not putting down my axe yet ;)',
    likes: 7,
    retweets: 0
  },
  {
    id: '1715093529561592213',
    created: 1697745137000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/jetbrains" rel="noopener noreferrer" target="_blank">@jetbrains</a> "Sadly" certainly captures my feeling about it. However, I\'m not the right one to hire as I don\'t do IDE development. However, there are people in the Asciidoctor community who could. I\'d be happy to do introductions if asked.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1715086360434946476" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1715086360434946476</span>',
    likes: 5,
    retweets: 0
  },
  {
    id: '1714881325725872271',
    created: 1697694543000,
    type: 'post',
    text: 'Movement is medicine.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1714807661147910345',
    created: 1697676980000,
    type: 'post',
    text: 'YT Music seriously has to be the worst mobile app I\'ve ever used. Not only does it still stop playing randomly, it\'s more confusing to browse than looking for books in a maze.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1714761841753358551',
    created: 1697666056000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/eddiejaoude" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> What I like about a standing desk is that it\'s both. I let my body tell me which one it needs.<br><br>In reply to: <a href="https://x.com/eddiejaoude/status/1714761126364303654" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> <span class="status">1714761126364303654</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1714446003208962137',
    created: 1697590754000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> <a class="mention" href="https://x.com/the_jamezp" rel="noopener noreferrer" target="_blank">@the_jamezp</a> <a class="mention" href="https://x.com/ahus1de" rel="noopener noreferrer" target="_blank">@ahus1de</a> 🙏<br><br>In reply to: <a href="https://x.com/jasondlee/status/1714445006344196337" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">1714445006344196337</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1714426658965622824',
    created: 1697586142000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jasondlee" rel="noopener noreferrer" target="_blank">@jasondlee</a> <a class="mention" href="https://x.com/the_jamezp" rel="noopener noreferrer" target="_blank">@the_jamezp</a> As usual, they ignored AsciiDoc, despite there being an incredible AsciiDoc plugin for IntelliJ (better than for any other IDE). We\'ll see where it goes, but I\'m not holding my breath. Fortunately, we don\'t have to thanks to <a class="mention" href="https://x.com/ahus1de" rel="noopener noreferrer" target="_blank">@ahus1de</a> &amp; friends.<br><br>In reply to: <a href="https://x.com/jasondlee/status/1714372975665742278" rel="noopener noreferrer" target="_blank">@jasondlee</a> <span class="status">1714372975665742278</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1714424474593050674',
    created: 1697585621000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CornellSports" rel="noopener noreferrer" target="_blank">@CornellSports</a> <a class="mention" href="https://x.com/CornellLacrosse" rel="noopener noreferrer" target="_blank">@CornellLacrosse</a> <a class="mention" href="https://x.com/IvyLeague" rel="noopener noreferrer" target="_blank">@IvyLeague</a> <a class="mention" href="https://x.com/USILA_Lax" rel="noopener noreferrer" target="_blank">@USILA_Lax</a> Denver. Let\'s go!!!!!<br><br>In reply to: <a href="https://x.com/CornellSports/status/1714371466756592120" rel="noopener noreferrer" target="_blank">@CornellSports</a> <span class="status">1714371466756592120</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1714424402010587634',
    created: 1697585604000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> I think the poster lost everyone by leading with "no shot clock". Total buzz kill. I do think that something needs to be done other than "dig the ball out of the net" after a goal. I prefer if the goalie (or someone) started from the back line with a new ball. Much cleaner.<br><br>In reply to: <a href="https://x.com/danarestia/status/1714406579867414741" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1714406579867414741</span>',
    likes: 10,
    retweets: 1
  },
  {
    id: '1714337392273080661',
    created: 1697564859000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/LacrosseNetwork" rel="noopener noreferrer" target="_blank">@LacrosseNetwork</a> Without a doubt, the Haudenosaunee must be able to compete. That\'s for certain.<br><br>My main issue with sixes is that the women\'s rules are different. If it weren\'t for that, I\'d totally be on board with the format. I do think some revision is needed for what happens after goal.<br><br>In reply to: <a href="https://x.com/LacrosseNetwork/status/1714306422354198975" rel="noopener noreferrer" target="_blank">@LacrosseNetwork</a> <span class="status">1714306422354198975</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1714211051951296968',
    created: 1697534737000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/EuroNation" rel="noopener noreferrer" target="_blank">@EuroNation</a> DJ Markski, yes! Ski mixes are such a classic! 🎧<br><br>In reply to: <a href="https://x.com/EuroNation/status/1702013483913515045" rel="noopener noreferrer" target="_blank">@EuroNation</a> <span class="status">1702013483913515045</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1714163499423953078',
    created: 1697523400000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GeoffreyDeSmet" rel="noopener noreferrer" target="_blank">@GeoffreyDeSmet</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> And Spring itself uses it to (for those projects that have made the switch to Antora).<br><br>In reply to: <a href="https://x.com/GeoffreyDeSmet/status/1714146474420830659" rel="noopener noreferrer" target="_blank">@GeoffreyDeSmet</a> <span class="status">1714146474420830659</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1714014558082519187',
    created: 1697487890000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GeoffreyDeSmet" rel="noopener noreferrer" target="_blank">@GeoffreyDeSmet</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> I should also add that Asciidoctor Tabs is now supported by the AsciiDoc plugin for IntelliJ, so you get a preview right in the IDE.<br><br>In reply to: <a href="#1714010729198801253">1714010729198801253</a>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1714010729198801253',
    created: 1697486977000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GeoffreyDeSmet" rel="noopener noreferrer" target="_blank">@GeoffreyDeSmet</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> Though, though we would prefer if people used Asciidoctor Tabs, as that is what will be standardized in the AsciiDoc language. The approach the Spring team took lacks association in the document structure between the tab elements.<br><br>In reply to: <a href="https://x.com/GeoffreyDeSmet/status/1713587057052754114" rel="noopener noreferrer" target="_blank">@GeoffreyDeSmet</a> <span class="status">1713587057052754114</span>',
    likes: 5,
    retweets: 1
  },
  {
    id: '1713514961492918662',
    created: 1697368777000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcuslemonis" rel="noopener noreferrer" target="_blank">@marcuslemonis</a> I\'ve been sleeping on the same pillow for a decade. It has served me well, but it\'s time for a new one. I got my current one at #BedBathBeyondDotCom. I\'d love to get another 10 years out of my next one.<br><br>In reply to: <a href="https://x.com/marcuslemonis/status/1713364633333997775" rel="noopener noreferrer" target="_blank">@marcuslemonis</a> <span class="status">1713364633333997775</span>',
    likes: 0,
    retweets: 0,
    tags: ['bedbathbeyonddotcom']
  },
  {
    id: '1712654485603147935',
    created: 1697163623000,
    type: 'post',
    text: 'From my junk folder, too funny to not post:<br><br>&gt; Date: Oct 10 03:35PM<br>&gt;<br>&gt; Іt\'ѕ hаrd tо bеlіеvе the end of August is here already!<br><br>Might I suggest getting some sort of calendar? Maybe a tear off one that says humorous things like in this opening. Remember to remove it each day ;)',
    likes: 2,
    retweets: 0
  },
  {
    id: '1712040607806619696',
    created: 1697017263000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/HFXThunderbirds" rel="noopener noreferrer" target="_blank">@HFXThunderbirds</a> <a class="mention" href="https://x.com/cole_kirst" rel="noopener noreferrer" target="_blank">@cole_kirst</a> <a class="mention" href="https://x.com/NLL" rel="noopener noreferrer" target="_blank">@NLL</a> <a class="mention" href="https://x.com/USAMLax" rel="noopener noreferrer" target="_blank">@USAMLax</a> <a class="mention" href="https://x.com/USA_Lacrosse" rel="noopener noreferrer" target="_blank">@USA_Lacrosse</a> 5 brothers in total, of course ;)<br><br>In reply to: <a href="#1712040339031347412">1712040339031347412</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1712040339031347412',
    created: 1697017199000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/HFXThunderbirds" rel="noopener noreferrer" target="_blank">@HFXThunderbirds</a> <a class="mention" href="https://x.com/cole_kirst" rel="noopener noreferrer" target="_blank">@cole_kirst</a> <a class="mention" href="https://x.com/NLL" rel="noopener noreferrer" target="_blank">@NLL</a> <a class="mention" href="https://x.com/USAMLax" rel="noopener noreferrer" target="_blank">@USAMLax</a> <a class="mention" href="https://x.com/USA_Lacrosse" rel="noopener noreferrer" target="_blank">@USA_Lacrosse</a> Cole is one of a kind...a kind that happens to include 4 awesome brothers. Can\'t wait to see him out on the turf!<br><br>In reply to: <a href="https://x.com/HFXThunderbirds/status/1711817393712812407" rel="noopener noreferrer" target="_blank">@HFXThunderbirds</a> <span class="status">1711817393712812407</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1711860244979314922',
    created: 1696974261000,
    type: 'quote',
    text: 'Feeney had an impact on nearly all of my Cornell experiences, yet I didn\'t know it until recently. The way he lived his life is inspiring and one I\'ll carry with me as I make decisions on my own path.<br><br>Quoting: <a href="https://x.com/Cornell/status/1711483840269562193" rel="noopener noreferrer" target="_blank">@Cornell</a> <span class="status">1711483840269562193</span>',
    likes: 3,
    retweets: 1
  },
  {
    id: '1711487203166347329',
    created: 1696885321000,
    type: 'post',
    text: 'I\'m a descendant of those colonialists, and yet I stand with the Indigenous People in their fight to reclaim their right of ownership. It\'s well known that they have a lot to teach us all about how to respect nature and to live in harmony with it.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1711486477941796974',
    created: 1696885148000,
    type: 'post',
    text: 'In the US, today is Indigenous Peoples’ Day, a day that\'s being reclaimed from the colonialists. As more US states begin to recognize this day, it reaffirms the place of those whose land was stolen. We must commit to a multi-racial democracy that includes Indigenous People.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1710838670536904940',
    created: 1696730699000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lightguardjp" rel="noopener noreferrer" target="_blank">@lightguardjp</a> It\'s more about the latter. These are usually people who were never in the conversation asking for a status update. In OSS, the status update is either I want to help or you\'re just pestering.<br><br>In reply to: <a href="https://x.com/lightguardjp/status/1710788817555230943" rel="noopener noreferrer" target="_blank">@lightguardjp</a> <span class="status">1710788817555230943</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1710605367204552955',
    created: 1696675075000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> That\'s a key reason why I was a speaker and will probably need to be in the future...if I decide to return to it. While it does take a lot of time, when I\'m on stage, I enjoy the hell out of it.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1710605127214612734" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1710605127214612734</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1710601141283860913',
    created: 1696674068000,
    type: 'post',
    text: 'I enjoyed several talks from #Devoxx at home today (while enjoying some tasty Belgian beer). Now I at least don\'t feel like Devoxx just passed by and I had a chance to soak some of it up. Still have a few more talks in the queue.',
    likes: 14,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '1710584830017294373',
    created: 1696670179000,
    type: 'post',
    text: 'We know that hockey is wildly popular at the Winter Olympics, so box lacrosse ends up being the perfect complement at the Summer Games. We aren\'t replacing NCAA lacrosse or PLL, but rather going what works for the audience. To me, box is that format.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1710584428156854629',
    created: 1696670083000,
    type: 'post',
    text: 'After seeing the women\'s #LAXNAI (box lacrosse) championships, I\'m convinced box lacrosse should be the format of lacrosse in the Olympics, not Sixes. I\'m surprising myself by saying that, but for me it\'s about equality and tradition. It\'s a well-established &amp; inclusive format.',
    likes: 0,
    retweets: 0,
    tags: ['laxnai']
  },
  {
    id: '1710545990758113400',
    created: 1696660919000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/oktadev" rel="noopener noreferrer" target="_blank">@oktadev</a> <a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <a class="mention" href="https://x.com/mraible" rel="noopener noreferrer" target="_blank">@mraible</a> <a class="mention" href="https://x.com/Devoxx" rel="noopener noreferrer" target="_blank">@Devoxx</a> I second that.<br><br>In reply to: <a href="https://x.com/oktadev/status/1710410248832659888" rel="noopener noreferrer" target="_blank">@oktadev</a> <span class="status">1710410248832659888</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1710457233543467326',
    created: 1696639757000,
    type: 'post',
    text: '@ mentioning someone on an issue in an OSS project to ask if there\'s been any progress is not cool. It\'s okay to add something of value to the issue, but pinging people is simply bad manners. If you need it badly, you can offer to help (in which case the @ is okay).',
    likes: 2,
    retweets: 0
  },
  {
    id: '1710205242154103266',
    created: 1696579678000,
    type: 'post',
    text: 'I ❤️ this line from GitHub\'s guide on how to communicate like a GitHub engineer:<br><br>Foster a culture that values documentation maintenance.<br><br>💯<br><br>It\'s more than a motto. It really does work. After any discussion, I think to myself "should I update the docs?"<br><br><a href="https://github.blog/2023-10-04-how-to-communicate-like-a-github-engineer-our-principles-practices-and-tools/#from-guiding-principles-to-prescriptive-practices" rel="noopener noreferrer" target="_blank">github.blog/2023-10-04-how-to-communicate-like-a-github-engineer-our-principles-practices-and-tools/#from-guiding-principles-to-prescriptive-practices</a>',
    likes: 14,
    retweets: 4
  },
  {
    id: '1710115012768149543',
    created: 1696558166000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CornellLacrosse" rel="noopener noreferrer" target="_blank">@CornellLacrosse</a> <a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> It looks like Kirst is officially a 3rd year. Can you confirm? If so, this is huge news for Cornell, as it means he will end there instead of transferring.<br><br>In reply to: <a href="https://x.com/CornellLacrosse/status/1709582947005706336" rel="noopener noreferrer" target="_blank">@CornellLacrosse</a> <span class="status">1709582947005706336</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1710114481215521061',
    created: 1696558039000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> I cannot wait to see this guy play. 🔥<br><br>In reply to: <a href="https://x.com/danarestia/status/1709571159371522333" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1709571159371522333</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1710113607093190860',
    created: 1696557830000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> I received both. Perhaps that was the plan?<br><br>In reply to: <a href="https://x.com/danarestia/status/1709647612939268280" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1709647612939268280</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1710113382593159268',
    created: 1696557777000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> <a class="mention" href="https://x.com/tyxanders" rel="noopener noreferrer" target="_blank">@tyxanders</a> Though it seems like Michelle Pehlke really had the inside access on the whole process, based on the documentary he uploaded the day of. So credit to him as well?<br><br>In reply to: <a href="https://x.com/danarestia/status/1709712980047270259" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1709712980047270259</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1710053401852080258',
    created: 1696543476000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/loiane" rel="noopener noreferrer" target="_blank">@loiane</a> I was cheating a bit and I put Achel 8° Bruin in the glass, but still legit Belgian nonetheless ;)<br><br>In reply to: <a href="https://x.com/loiane/status/1710019353171595616" rel="noopener noreferrer" target="_blank">@loiane</a> <span class="status">1710019353171595616</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1709743466941747666',
    created: 1696469582000,
    type: 'post',
    text: 'Bier Central @ home. I raise my glass to all celebrating at #Devoxx 🍻',
    photos: ['<div class="item"><img class="photo" src="media/1709743466941747666.jpg"></div>'],
    likes: 13,
    retweets: 0,
    tags: ['devoxx']
  },
  {
    id: '1709674849810239746',
    created: 1696453223000,
    type: 'quote',
    text: 'That\'s Devoxx culture right there. First class.<br><br>Quoting: <a href="https://x.com/SirJelle/status/1709634783611854907" rel="noopener noreferrer" target="_blank">@SirJelle</a> <span class="status">1709634783611854907</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1709656402883756074',
    created: 1696448824000,
    type: 'post',
    text: 'AsciiDoc is rocking the stage at Devoxx thanks to <a class="mention" href="https://x.com/mrhaki" rel="noopener noreferrer" target="_blank">@mrhaki</a>. I sure I wish I could have been there to witness it in person! But I also love that we have such a strong community that spreads the word!',
    likes: 27,
    retweets: 5
  },
  {
    id: '1709512730506678489',
    created: 1696414570000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/evilyeti" rel="noopener noreferrer" target="_blank">@evilyeti</a> <a class="mention" href="https://x.com/Devoxx" rel="noopener noreferrer" target="_blank">@Devoxx</a> <a class="mention" href="https://x.com/Stephan007" rel="noopener noreferrer" target="_blank">@Stephan007</a> Exactly my plan! (It doesn\'t replace being at Devoxx, but I can at least pretend in my head).<br><br>In reply to: <a href="https://x.com/evilyeti/status/1709511389843251638" rel="noopener noreferrer" target="_blank">@evilyeti</a> <span class="status">1709511389843251638</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1709506921361973742',
    created: 1696413185000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bjschrijver" rel="noopener noreferrer" target="_blank">@bjschrijver</a> <a class="mention" href="https://x.com/alina_yurenko" rel="noopener noreferrer" target="_blank">@alina_yurenko</a> You have a knack for hearing people from other rooms ;)<br><br>In reply to: <a href="https://x.com/bjschrijver/status/1709499841590546466" rel="noopener noreferrer" target="_blank">@bjschrijver</a> <span class="status">1709499841590546466</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1709303340503638225',
    created: 1696364648000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/oleg_nenashev" rel="noopener noreferrer" target="_blank">@oleg_nenashev</a> <a class="mention" href="https://x.com/wiremockorg" rel="noopener noreferrer" target="_blank">@wiremockorg</a> <a class="mention" href="https://x.com/testcontainers" rel="noopener noreferrer" target="_blank">@testcontainers</a> <a class="mention" href="https://x.com/CloudBees" rel="noopener noreferrer" target="_blank">@CloudBees</a> <a class="mention" href="https://x.com/jenkinsci" rel="noopener noreferrer" target="_blank">@jenkinsci</a> <a class="mention" href="https://x.com/CDeliveryFdn" rel="noopener noreferrer" target="_blank">@CDeliveryFdn</a> <a class="mention" href="https://x.com/penpotapp" rel="noopener noreferrer" target="_blank">@penpotapp</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/_whitebluewhite" rel="noopener noreferrer" target="_blank">@_whitebluewhite</a> You are a brave soul. I can\'t help but to notice the AsciiDoc logo in the mix too 🎉<br><br>In reply to: <a href="https://x.com/oleg_nenashev/status/1709167272059711949" rel="noopener noreferrer" target="_blank">@oleg_nenashev</a> <span class="status">1709167272059711949</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1709151593214726496',
    created: 1696328468000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/oleg_nenashev" rel="noopener noreferrer" target="_blank">@oleg_nenashev</a> That shirt is 🔥. I\'m sure it will bring you lots of light in one form or another.<br><br>In reply to: <a href="https://x.com/oleg_nenashev/status/1708349453504061790" rel="noopener noreferrer" target="_blank">@oleg_nenashev</a> <span class="status">1708349453504061790</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1709151159565623423',
    created: 1696328365000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aheritier" rel="noopener noreferrer" target="_blank">@aheritier</a> <a class="mention" href="https://x.com/Devoxx" rel="noopener noreferrer" target="_blank">@Devoxx</a> Lookin\' as good as ever.<br><br>In reply to: <a href="https://x.com/aheritier/status/1709146950736506995" rel="noopener noreferrer" target="_blank">@aheritier</a> <span class="status">1709146950736506995</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1709150456734433535',
    created: 1696328198000,
    type: 'post',
    text: 'I was thrilled to see the women teams compete against other women teams on the same stage as the men using the same format and equipment (including helmets!) during the #LAXNAI lacrosse tournament. This is progress!<br><br>Now if only the same could happen for field lacrosse...',
    likes: 2,
    retweets: 0,
    tags: ['laxnai']
  },
  {
    id: '1708913501060976795',
    created: 1696271703000,
    type: 'quote',
    text: 'Talk about this over beers at #Devoxx. The CRA is threatening the OSS model, putting more burden on an already strained system to assume even more responsibility. The result could be a lot less OSS to choose from or even more projects that stagnate under the burden. #ModifyTheCRA<br><br>Quoting: <a href="https://x.com/EclipseFdn/status/1706774148549148761" rel="noopener noreferrer" target="_blank">@EclipseFdn</a> <span class="status">1706774148549148761</span>',
    likes: 3,
    retweets: 4,
    tags: ['devoxx', 'modifythecra']
  },
  {
    id: '1708803256544366721',
    created: 1696245419000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Stephan007" rel="noopener noreferrer" target="_blank">@Stephan007</a><br><br>In reply to: <a href="https://x.com/Stephan007/status/1708457236224163996" rel="noopener noreferrer" target="_blank">@Stephan007</a> <span class="status">1708457236224163996</span>',
    photos: ['<div class="item"><img class="photo" src="media/1708803256544366721.gif"></div>'],
    likes: 1,
    retweets: 0
  },
  {
    id: '1708277082852077588',
    created: 1696119969000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bk1_168" rel="noopener noreferrer" target="_blank">@bk1_168</a> If I lived in Europe, this is definitely how I\'d get to Devoxx. Way to be!<br><br>In reply to: <a href="https://x.com/bk1_168/status/1708111389296923118" rel="noopener noreferrer" target="_blank">@bk1_168</a> <span class="status">1708111389296923118</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1706762786842550408',
    created: 1695758933000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joekeegs" rel="noopener noreferrer" target="_blank">@joekeegs</a> I think RB calls it window dressing.<br><br>In reply to: <a href="https://x.com/joekeegs/status/1706685141211299933" rel="noopener noreferrer" target="_blank">@joekeegs</a> <span class="status">1706685141211299933</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1706574086162579952',
    created: 1695713943000,
    type: 'post',
    text: 'Devoxx Belgium is next week and I\'m looking forward to attending virtually via YouTube. That means I first need to make a trip to the store to grab some of my favorite Belgian brews. It\'s my favorite time of year 📽️😋',
    likes: 6,
    retweets: 0
  },
  {
    id: '1706491342719963541',
    created: 1695694215000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLArchers" rel="noopener noreferrer" target="_blank">@PLLArchers</a> <a class="mention" href="https://x.com/TomSchreiber26" rel="noopener noreferrer" target="_blank">@TomSchreiber26</a> This is going to become a standard shooting drill.<br><br>In reply to: <a href="https://x.com/PLLArchers/status/1706409025468785119" rel="noopener noreferrer" target="_blank">@PLLArchers</a> <span class="status">1706409025468785119</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1706446644802474092',
    created: 1695683559000,
    type: 'post',
    text: 'A huge thank you to each and every one of you who have provided your financial support. It means so much to me, I appreciate it, and I do not take it for granted.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1706446246544654464',
    created: 1695683464000,
    type: 'post',
    text: 'The support I receive through OpenCollective (<a href="https://opencollective.com/asciidoctor" rel="noopener noreferrer" target="_blank">opencollective.com/asciidoctor</a>) is definitely the reason I\'ve been able to continue working on AsciiDoc and Asciidoctor daily. Without it, there\'s just no way I would have been able to sustain my commitment for this long.',
    likes: 5,
    retweets: 1
  },
  {
    id: '1706230003384656247',
    created: 1695631907000,
    type: 'post',
    text: 'Google Analytics 4 finally convinced me to stop putting stats on web sites. In over 2 decades of adding stats, I\'ve never found any value in them. I just don\'t have the time. I\'m going to base everything on the real feedback I get rather than trying to track anonymous activity.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1706175983454494825',
    created: 1695619028000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> Can we talk about the fact that the shot clock wasn\'t working on that end, that it didn\'t start right away, and that the horn is always late?? They really need to automate this stuff more and add redundancy. It\'s not a good look as is. Let\'s also automate the end line scramble.<br><br>In reply to: <a href="https://x.com/danarestia/status/1706063087189590069" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1706063087189590069</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1706174893195481456',
    created: 1695618768000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLWaterdogs" rel="noopener noreferrer" target="_blank">@PLLWaterdogs</a> I thought y\'all were pretty damn great this year. Hell of a season and a fight to the finish. Congrats!<br><br>In reply to: <a href="https://x.com/PLLWaterdogs/status/1706074259330629840" rel="noopener noreferrer" target="_blank">@PLLWaterdogs</a> <span class="status">1706074259330629840</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1705309209452216606',
    created: 1695412373000,
    type: 'reply',
    text: 'And btw, <a class="mention" href="https://x.com/Pearl_Lacrosse" rel="noopener noreferrer" target="_blank">@Pearl_Lacrosse</a> balls are 🔥. Don\'t waste time practicing with anything else 😉<br><br>In reply to: <a href="#1702405921501323757">1702405921501323757</a>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1704933098423734420',
    created: 1695322701000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jimisola" rel="noopener noreferrer" target="_blank">@jimisola</a> <a class="mention" href="https://x.com/wimdeblauwe" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> <a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> <a class="mention" href="https://x.com/java" rel="noopener noreferrer" target="_blank">@java</a> The callouts are placed in the source code so that when the source code is included using the include directive, it pulls in the callouts.<br><br>In reply to: <a href="https://x.com/jimisola/status/1704617030412861876" rel="noopener noreferrer" target="_blank">@jimisola</a> <span class="status">1704617030412861876</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1704581062607978831',
    created: 1695238769000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/wimdeblauwe" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> <a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> <a class="mention" href="https://x.com/java" rel="noopener noreferrer" target="_blank">@java</a> That reminds me we need to document this.<br><br>In reply to: <a href="https://x.com/wimdeblauwe/status/1704554452168901026" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> <span class="status">1704554452168901026</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1703676148281151598',
    created: 1695023021000,
    type: 'post',
    text: 'I\'m still unsure if I\'m going to try to play in a league. We\'ll see. Right now I\'m just enjoying time outside by myself and also with my spouse, both as a form a physical therapy as well as a form of play. It\'s been nice.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1703674766048334090',
    created: 1695022691000,
    type: 'post',
    text: 'The indigenous peoples who originated the sport of lacrosse refer to it as the medicine game. I\'ve been practicing it daily for several weeks and I can attest that it is healing. I feel so much better physically just from doing the motion of throwing and catching the ball. 🥍',
    likes: 1,
    retweets: 0
  },
  {
    id: '1703513065394966616',
    created: 1694984139000,
    type: 'post',
    text: 'I really just don\'t get how anyone thinks that shaming someone (or their efforts) is a way to get that person to do something for them. In my experience, it produces the exact opposite reaction. So why do people keep trying it?',
    likes: 5,
    retweets: 0
  },
  {
    id: '1702417688109424984',
    created: 1694722980000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/SaskRushLAX" rel="noopener noreferrer" target="_blank">@SaskRushLAX</a> OMG yes! This was sorely needed and the result is stupendous. Let\'s go!<br><br>In reply to: <a href="https://x.com/SaskRushLAX/status/1702019310162690412" rel="noopener noreferrer" target="_blank">@SaskRushLAX</a> <span class="status">1702019310162690412</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1702405921501323757',
    created: 1694720175000,
    type: 'post',
    text: 'Most times when I purchase something online, I end up getting automatically subscribed to their mailinglist, which I detest. <a class="mention" href="https://x.com/guardian_innov" rel="noopener noreferrer" target="_blank">@guardian_innov</a> sends an email asking you for your permission to subscribe you to the list. Now that\'s how you do it!',
    likes: 3,
    retweets: 0
  },
  {
    id: '1701892728743977402',
    created: 1694597820000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLCannons" rel="noopener noreferrer" target="_blank">@PLLCannons</a> An unforgettable season. You dazzled while shattering expectations, from the 2 balls to the crease dives and the highway robbery from end to end. An amazing show of fortitude. 💥<br><br>In reply to: <a href="https://x.com/PLLCannons/status/1701265060780298274" rel="noopener noreferrer" target="_blank">@PLLCannons</a> <span class="status">1701265060780298274</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1701840627011563574',
    created: 1694585398000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> Wall ball.<br><br>In reply to: <a href="https://x.com/danarestia/status/1701021546179903597" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1701021546179903597</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1701838941333393695',
    created: 1694584996000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/LacrosseNetwork" rel="noopener noreferrer" target="_blank">@LacrosseNetwork</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/DU_AthDir" rel="noopener noreferrer" target="_blank">@DU_AthDir</a> <a class="mention" href="https://x.com/DU_MLAX" rel="noopener noreferrer" target="_blank">@DU_MLAX</a> <a class="mention" href="https://x.com/DU_WLAX" rel="noopener noreferrer" target="_blank">@DU_WLAX</a> Nevermind the field, that scoreboard is still dreadful.<br><br>In reply to: <a href="https://x.com/LacrosseNetwork/status/1701708509987869005" rel="noopener noreferrer" target="_blank">@LacrosseNetwork</a> <span class="status">1701708509987869005</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1700812192529133822',
    created: 1694340200000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/LacrosseNetwork" rel="noopener noreferrer" target="_blank">@LacrosseNetwork</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/PLLArchers" rel="noopener noreferrer" target="_blank">@PLLArchers</a> To grow the game, there needs to be walls like this all over the country. It\'s so difficult to find places to do wall ball, and we all know that wall ball is an essential aspect of developing your game (Tom Schreiber being living proof of that).<br><br>In reply to: <a href="https://x.com/LacrosseNetwork/status/1699475204743852356" rel="noopener noreferrer" target="_blank">@LacrosseNetwork</a> <span class="status">1699475204743852356</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1700809499282739246',
    created: 1694339558000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> Agreed. To be fair, <a class="mention" href="https://x.com/JeffTrainor" rel="noopener noreferrer" target="_blank">@JeffTrainor</a> is also having one hell of a summer. So Coach Holman is probably glad to have his energy on the squad in exchange.<br><br>In reply to: <a href="https://x.com/danarestia/status/1699041294574670324" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1699041294574670324</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1700805586546549227',
    created: 1694338625000,
    type: 'quote',
    text: 'The analysis provided by <a class="mention" href="https://x.com/charlotten8rth" rel="noopener noreferrer" target="_blank">@charlotten8rth</a> during this weekend incredible. She had such great chemistry on the broadcast with <a class="mention" href="https://x.com/danaboyle_" rel="noopener noreferrer" target="_blank">@danaboyle_</a> and <a class="mention" href="https://x.com/Ryan_Boyle14" rel="noopener noreferrer" target="_blank">@Ryan_Boyle14</a>. This was by far the best coverage of the season.<br><br>Quoting: <a href="https://x.com/charlotten8rth/status/1699475173538246801" rel="noopener noreferrer" target="_blank">@charlotten8rth</a> <span class="status">1699475173538246801</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1700606241964523831',
    created: 1694291098000,
    type: 'post',
    text: 'My ❤️ goes out to the people of #Morocco and to the families and friends of those who lost their lives. 🇲🇦 I\'ve witnessed first hand the incredible spirit and perseverance that Moroccans have and I know it cannot be shaken.',
    likes: 11,
    retweets: 3,
    tags: ['morocco']
  },
  {
    id: '1700277797887828204',
    created: 1694212791000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mp911de" rel="noopener noreferrer" target="_blank">@mp911de</a> <a class="mention" href="https://x.com/SpringData" rel="noopener noreferrer" target="_blank">@SpringData</a> <a class="mention" href="https://x.com/rob_winch" rel="noopener noreferrer" target="_blank">@rob_winch</a> I wish there was a gigantic heart button to press, because it means so much to me to be a part of your success. The Spring teams truly are great at everything they do. The result speaks for itself. You\'ve not only elevated your docs, but Antora too. Thanks so much!<br><br>In reply to: <a href="https://x.com/mp911de/status/1700088553110208766" rel="noopener noreferrer" target="_blank">@mp911de</a> <span class="status">1700088553110208766</span>',
    likes: 6,
    retweets: 1
  },
  {
    id: '1699981847776886931',
    created: 1694142231000,
    type: 'post',
    text: 'Screw you GitHub for having to enable 2FA by the end of the month for all my bots. I just don\'t have time for this headache.',
    likes: 5,
    retweets: 0
  },
  {
    id: '1699142743644332327',
    created: 1693942173000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sorairolake" rel="noopener noreferrer" target="_blank">@sorairolake</a> It\'s not a bug. This is how AsciiDoc currently works. If you need further help or clarification, I encourage you to join us in the project chat at <a href="https://chat.asciidoctor.org" rel="noopener noreferrer" target="_blank">chat.asciidoctor.org</a>.<br><br>In reply to: <a href="https://x.com/sorairolake/status/1698996877713457483" rel="noopener noreferrer" target="_blank">@sorairolake</a> <span class="status">1698996877713457483</span>',
    likes: 3,
    retweets: 2
  },
  {
    id: '1698526444232589781',
    created: 1693795236000,
    type: 'quote',
    text: 'PLL playoffs eve, for those who observe. 🥍<br><br>Quoting: <a href="https://x.com/PremierLacrosse/status/1698511566231011570" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1698511566231011570</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1697719581320843744',
    created: 1693602864000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLCannons" rel="noopener noreferrer" target="_blank">@PLLCannons</a> I question the decision to put Kavanagh back in. I at least would have kept Charalambides suited up.<br><br>In reply to: <a href="https://x.com/PLLCannons/status/1697712553592586737" rel="noopener noreferrer" target="_blank">@PLLCannons</a> <span class="status">1697712553592586737</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1697134259788800109',
    created: 1693463313000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> In Pehlke voice, "Wooooow!"<br><br>In reply to: <a href="https://x.com/danarestia/status/1696857208019919004" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1696857208019919004</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1696577675429847532',
    created: 1693330613000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> Exactly. I don\'t understand why it is so hard to say "when this meeting is over, can you stick around for an update?" like nearly every automated phone system works.<br><br>In reply to: <a href="https://x.com/gunnarmorling/status/1696561279249404022" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <span class="status">1696561279249404022</span>',
    likes: 6,
    retweets: 0
  },
  {
    id: '1694832619765047787',
    created: 1692914559000,
    type: 'post',
    text: 'To me, customer service is just customer irritation. There\'s 0 service provided.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1694593358784770515',
    created: 1692857515000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/Ryan_Boyle14" rel="noopener noreferrer" target="_blank">@Ryan_Boyle14</a> The detailed analysis you bring to the game of lacrosse is unmatched, topped with a healthy dose of enthusiasm &amp; humor. Keep doing what you do because you make the viewing experience awesome. 👏 Buckets off to you!',
    likes: 8,
    retweets: 1
  },
  {
    id: '1693144779192926457',
    created: 1692512147000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bdowns328" rel="noopener noreferrer" target="_blank">@bdowns328</a> <a class="mention" href="https://x.com/CornellLacrosse" rel="noopener noreferrer" target="_blank">@CornellLacrosse</a> Now let\'s just hope I can find some of the old magic and put it to work.<br><br>In reply to: <a href="https://x.com/bdowns328/status/1693112714074390933" rel="noopener noreferrer" target="_blank">@bdowns328</a> <span class="status">1693112714074390933</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1693112271395041339',
    created: 1692504396000,
    type: 'post',
    text: 'My new lacrosse stick. The stringing is an homage to <a class="mention" href="https://x.com/CornellLacrosse" rel="noopener noreferrer" target="_blank">@CornellLacrosse</a>, specifically my favorite players, Rob Pannell, Jeff Teat, Gavin Adler, and CJ Kirst. #YellCornell',
    photos: ['<div class="item"><img class="photo" src="media/1693112271395041339.jpg"></div>', '<div class="item"><img class="photo" src="media/1693112271395041339-2.jpg"></div>'],
    likes: 3,
    retweets: 0,
    tags: ['yellcornell']
  },
  {
    id: '1692764791608062124',
    created: 1692421551000,
    type: 'post',
    text: 'I\'ve always be skeptical of gin, probably because the gin &amp; tonics that my great relatives had just turned me off. But modern gins are just full of interesting flavors and much less aggressive than bourbons. Paint me impressed! 🍸',
    likes: 0,
    retweets: 0
  },
  {
    id: '1692687397115609147',
    created: 1692403098000,
    type: 'post',
    text: 'Just had gin for the first time. Okay. Wow. What have I been missing? For how many herbs I\'ve had in recent years, this is a vegan delight.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1692645532949262367',
    created: 1692393117000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ilopmar" rel="noopener noreferrer" target="_blank">@ilopmar</a> I think using anoher distro is actually good because it provides perspective. Not everyone should do it, but some doing it is healthy.<br><br>In reply to: <a href="https://x.com/ilopmar/status/1691759321548271773" rel="noopener noreferrer" target="_blank">@ilopmar</a> <span class="status">1691759321548271773</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1692645016315797892',
    created: 1692392994000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/speakjava" rel="noopener noreferrer" target="_blank">@speakjava</a> I\'ve always said that in school, we learn how to learn. Do we learn anything from playing games or sports? Not directly, but we learn skills that then apply somewhere else. Grades, not so important. Experiences, very important.<br><br>In reply to: <a href="https://x.com/speakjava/status/1692574899439648786" rel="noopener noreferrer" target="_blank">@speakjava</a> <span class="status">1692574899439648786</span>',
    likes: 5,
    retweets: 0
  },
  {
    id: '1692330035712053363',
    created: 1692317897000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLRedwoods" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> Glad to see Kirst in the lineup. I have no doubt he\'ll have a big impact on the game. Kirst brothers play with so much heart. And I\'m confident <a class="mention" href="https://x.com/RobPannell3" rel="noopener noreferrer" target="_blank">@RobPannell3</a> knows how to find him. Dimes all night.<br><br>In reply to: <a href="https://x.com/PLLRedwoods/status/1692304351073845481" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> <span class="status">1692304351073845481</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1692314922519032241',
    created: 1692314293000,
    type: 'post',
    text: 'I was an NCAA athlete in college, but it was springboard diving, not lacrosse.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1692314202789274034',
    created: 1692314122000,
    type: 'post',
    text: 'I played lacrosse in high school as an attackman. I leaned to play two handed because I broke my thumb on my dominant hand. Never played in college, but all the practice I did seems to have stuck with me. We\'ll see if all that practice pays off.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1692312241226358974',
    created: 1692313654000,
    type: 'post',
    text: 'Just bought a new lacrosse stick for the first time in well over two decades. Not sure where this is going, but I\'m excited. 😮 Just 🙏 that my body holds up to the task. 🥍',
    likes: 2,
    retweets: 0
  },
  {
    id: '1691994331702587875',
    created: 1692237859000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danielveillard" rel="noopener noreferrer" target="_blank">@danielveillard</a> I also have the good fortune of running said company ;)<br><br>In reply to: <a href="https://x.com/danielveillard/status/1691900294605459590" rel="noopener noreferrer" target="_blank">@danielveillard</a> <span class="status">1691900294605459590</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1691599480758599877',
    created: 1692143719000,
    type: 'post',
    text: 'Speaking of time off, I\'m finally starting to get back into the swing of things after a brief vacation. I think I should take more vacations so I\'m better at making this transition ;)',
    likes: 1,
    retweets: 0
  },
  {
    id: '1691598418303705139',
    created: 1692143465000,
    type: 'post',
    text: 'I\'m very happy that people use the software I create and find it useful. That\'s the bright spot. But I\'m not obligated to create it, nor am I obligated to please anyone. I will put on the breaks any time I think I\'m not getting enjoyment or value out of it myself.',
    likes: 7,
    retweets: 1
  },
  {
    id: '1691598416764396024',
    created: 1692143465000,
    type: 'post',
    text: 'I control my own schedule and my own labor. When I feel like I\'m losing control over either, I will make an decisive change to correct it.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1691598414491054197',
    created: 1692143465000,
    type: 'post',
    text: 'Of course, I\'ll do the very best I can do and take great pride in what I make. But if someone is trying to get their way by manipulating me or my emotions, they can get lost. It\'s all in how the interaction plays out. It\'s not about what they want.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1691598412205158609',
    created: 1692143464000,
    type: 'post',
    text: 'At some point doing my brief time off, I decided I\'d no longer be shamed by people who want me to fix or change software for them because they think it\'s wrong. I\'m done with being pushed around. I\'ll make what I make in the way it feels right to me.',
    likes: 13,
    retweets: 0
  },
  {
    id: '1690609792828743680',
    created: 1691907759000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/ESPNPlus" rel="noopener noreferrer" target="_blank">@ESPNPlus</a> And what an overtime it was! The crowd was on its feet at the edge of the bleachers the whole time. ⏳🥅🥍<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1690571744556515328" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1690571744556515328</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1690580087689625600',
    created: 1691900677000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> Yes. Without a doubt. And the crowd was electric. Everyone on their feet for the whole OT. It was nothing short of thrilling.<br><br>In reply to: <a href="https://x.com/danarestia/status/1690573932418539520" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1690573932418539520</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1690579730842431488',
    created: 1691900591000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/RobPannell3" rel="noopener noreferrer" target="_blank">@RobPannell3</a> I didn\'t get a chance to bump fists with you after the game, but I just wanted to say great game! You seem to always put on quite a show in Denver. Go Big Red! #rollwoods. I love the chemistry between you and Kirst.',
    likes: 1,
    retweets: 0,
    tags: ['rollwoods']
  },
  {
    id: '1689718260244299779',
    created: 1691695201000,
    type: 'post',
    text: 'For the record, ABRP does get this right, so I always kept that app open on my phone to get the full picture. But then you have to juggle with your phone.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1689717836216983552',
    created: 1691695100000,
    type: 'post',
    text: 'You just get this impression when crossing the US just how low tech this country is. For the "richest country in the world", it lives very much in the past. We could do so much better if we even tried.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1689717834518249472',
    created: 1691695099000,
    type: 'post',
    text: 'The other problem is speed limits. They\'re absolutely absurd in the US. On a highway, they can be as low as 55mph. And they change constantly, even more so though road work. The car could go much, much faster, but it\'s severely held back. Sometimes you feel like you\'re crawling.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1689717832756641794',
    created: 1691695099000,
    type: 'post',
    text: 'The real problem I have is that it\'s way oversold. It\'s NOT full self driving. You can only take your hand off the wheel or look way for 1 min. So you\'re still driving. It\'s really driver assist in the form of auto steering and adaptive cruise control. That\'s nice, but not FSD.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1689717831750008833',
    created: 1691695099000,
    type: 'post',
    text: 'After another 4k+ miles of driving in a (rented) Tesla, I can say that the full self driving is both impressive and a complete joke at the same time. It performs better than a human on the highway, and that\'s what you really need it for. For local driving, it\'s a gimmick.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1689714551573594115',
    created: 1691694317000,
    type: 'post',
    text: 'Of course, let\'s talk about a nationwide toll pass. No such thing exists. 🤦‍♂️ You need at least a half dozen to drive around the US, or you have to resort to the stop at toll booth schnanigans. This country is so backwards sometimes.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1689714550634070016',
    created: 1691694316000,
    type: 'post',
    text: 'I find it frustrating that the Tesla navigation system does not provide any information about tolls, even though it has that information from Google. You\'re driving along, then find yourself having to scramble for coins like it\'s the \'70s. Come on now.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1689713268699639808',
    created: 1691694011000,
    type: 'post',
    text: 'The map also provides no information or layer to show where the time zone boundaries are. It makes planning the trip very confusing.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1689713267600670723',
    created: 1691694011000,
    type: 'post',
    text: 'It\'s incomprehensible to me that the built-in navigation in a Tesla does not account for a time zone change between the start and destination. Seriously? That\'s just an amateur oversight.',
    likes: 5,
    retweets: 0
  },
  {
    id: '1689100744492716032',
    created: 1691547974000,
    type: 'post',
    text: 'Introduced two of my nephews (5 and 7) to lacrosse, and they were immediately hooked. All they wanted to do was play or watch more lacrosse.',
    likes: 3,
    retweets: 1
  },
  {
    id: '1689086057340358657',
    created: 1691544472000,
    type: 'post',
    text: 'Just to be clear, I still use Vim and will always use Vim (even if I evaluate IDEs for my users).',
    likes: 1,
    retweets: 0
  },
  {
    id: '1689084442805936129',
    created: 1691544087000,
    type: 'post',
    text: 'I can attest that you truly can go all the way across the country (US) and find vegan establishments. The world is changing.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1688710568817967104',
    created: 1691454948000,
    type: 'quote',
    text: 'I cannot think of a single creator/maintainer who affected my life more than Bram. I live inside of a Vim session every waking minute of my day. Vim is my computing home base. I appreciate what Bram brought to this world in the deepest sense. He will be painfully missed.<br><br>Quoting: <a href="https://x.com/1angdon/status/1688701211673853953" rel="noopener noreferrer" target="_blank">@1angdon</a> <span class="status">1688701211673853953</span>',
    likes: 8,
    retweets: 1
  },
  {
    id: '1687273334528970752',
    created: 1691112285000,
    type: 'post',
    text: 'Introduced <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> to my nieces and nephews (5 - 10 years old), and they were affixed to the screen the whole game, being inquisitive and cheering on their team of choice (which changed goal to goal). #growthegame',
    likes: 1,
    retweets: 0,
    tags: ['growthegame']
  },
  {
    id: '1685316831177826304',
    created: 1690645818000,
    type: 'post',
    text: 'Driving through the Great Smokies into North Carolina, there is a sign recognizing the North Carolina Women\'s Lacrosse team for winning the national championship in 2022. So cool! <a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> #lax',
    likes: 4,
    retweets: 0,
    tags: ['lax']
  },
  {
    id: '1684983872130297870',
    created: 1690566435000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLRedwoods" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> This looks like a roster that could produce. Just don\'t hold the ball on offense!<br><br>In reply to: <a href="https://x.com/PLLRedwoods/status/1684662617401417728" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> <span class="status">1684662617401417728</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1684249844498714624',
    created: 1690391429000,
    type: 'post',
    text: 'For the first time since before the pandemic (actually, it was already happening, we didn\'t just know it yet), I\'m finally taking a vacation. I\'m not sure if I\'m excited or anxious. Maybe both? Either way, the goal is to be relaxed by the end. 🏖️',
    likes: 13,
    retweets: 0
  },
  {
    id: '1683975685508038657',
    created: 1690326064000,
    type: 'post',
    text: 'Well, they also succeed at making me royally upset.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1683974381989683200',
    created: 1690325754000,
    type: 'post',
    text: 'Virtual agents are nothing but a gimmick. They solve nothing and only succeed at wasting your time.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1683947454100828160',
    created: 1690319333000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ksattkb" rel="noopener noreferrer" target="_blank">@ksattkb</a> Yes, you can. You can join the Asciidoctor project chat at <a href="https://chat.asciidoctor.org" rel="noopener noreferrer" target="_blank">chat.asciidoctor.org</a> and actually talk to someone about it ;)<br><br>In reply to: <a href="https://x.com/ksattkb/status/1683681301071790081" rel="noopener noreferrer" target="_blank">@ksattkb</a> <span class="status">1683681301071790081</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1682638111338266624',
    created: 1690007162000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> It just seems to me that the coaching staff is in denial about what the problem is. I don\'t get it. Are they trying to lose games?<br><br>In reply to: <a href="https://x.com/danarestia/status/1680054807896129536" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1680054807896129536</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1682109774106890240',
    created: 1689881196000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joaompinto4" rel="noopener noreferrer" target="_blank">@joaompinto4</a> Well said!<br><br>In reply to: <a href="https://x.com/joaompinto4/status/1682087233162018816" rel="noopener noreferrer" target="_blank">@joaompinto4</a> <span class="status">1682087233162018816</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1682109719983575040',
    created: 1689881183000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/josephbottinger" rel="noopener noreferrer" target="_blank">@josephbottinger</a> The message is not "do it yourself". The message is, if you want it, you need to be responsible for making it happen, however you go about it. There are a plethora of developers in this world available for hire.<br><br>In reply to: <a href="https://x.com/josephbottinger/status/1682053369991507969" rel="noopener noreferrer" target="_blank">@josephbottinger</a> <span class="status">1682053369991507969</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1681976470397394944',
    created: 1689849414000,
    type: 'post',
    text: 'With that said, if you come forward and say "will you collaborate with me (or the person I\'ve hired) to make this better?" then I\'m usually all in.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1681952180822224897',
    created: 1689843623000,
    type: 'post',
    text: 'It\'s really odd to me that many people view open source as someone writing software for them. "I\'m waiting on this enhancement." "Why can\'t you add this feature?" Open source is software someone made which you have rights to use, distribute, and modify. Don\'t get it twisted.',
    likes: 25,
    retweets: 4
  },
  {
    id: '1680511303813677056',
    created: 1689500091000,
    type: 'post',
    text: 'I\'m loving the scheduled messages feature in Zulip 7. It allows me to throttle my responses while still being able to bulk answer questions. It also gives me a little bit of time to think about certain replies after I press send.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1680326956724928512',
    created: 1689456140000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/hsablonniere" rel="noopener noreferrer" target="_blank">@hsablonniere</a> Welcome to one of my childhood fascinations. I listened to that soundtrack more times than I care to count.<br><br>In reply to: <a href="https://x.com/hsablonniere/status/1680312210021666820" rel="noopener noreferrer" target="_blank">@hsablonniere</a> <span class="status">1680312210021666820</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1679964534466056199',
    created: 1689369731000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> Just to be clear, I\'m sharing this because I have a strong desire for the <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> to succeed. But part of succeeding means recognizing when a bad decision was made and doing what needs to be done to fix it.<br><br>In reply to: <a href="#1679963594488975360">1679963594488975360</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1679963594488975360',
    created: 1689369507000,
    type: 'reply',
    text: 'My advice for <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> is to revert the change in the second half of the season. I think the situation (and season) can still be salvaged. Just go back to 52s and leave the experimentation for the ASG.<br><br>In reply to: <a href="#1679963294214533120">1679963294214533120</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1679963294214533120',
    created: 1689369436000,
    type: 'post',
    text: 'Although I continue to tune in because I\'m still very interested in individual performances, to me the whole season is a throwaway. It just looks like I\'m watching a practice.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1679962617253892096',
    created: 1689369274000,
    type: 'post',
    text: 'And, finally, it has become the ultimate distraction. All anyone talks about now is how short the clock is and whether teams are even dressing a face off person. It\'s just made the whole season look disorganized. How anyone thought this was a good idea is beyond me.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1679962615471304704',
    created: 1689369274000,
    type: 'post',
    text: 'Second, it\'s actually slowing down the game instead of speeding it up. With the first possession being a throwaway, now we have to wait until the second possession for anything interesting to happen. As a result, the whole pace of play lags. Turnover city.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1679962613747453954',
    created: 1689369273000,
    type: 'post',
    text: 'First, the face off guys are the most animated players in the league, and this change is taking them off the stage. That makes no sense from a marketing standpoint. It\'s like cutting out the first act and replacing the star actor. Great way to kill a play.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1679962611037933568',
    created: 1689369273000,
    type: 'post',
    text: 'I love watching <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a>, and it\'s the highlight of my week during the summer. But the 32s shot clock, and the impact it\'s having on the face off is frankly ruining the season. It was a completely unnecessary change.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1679960027573145600',
    created: 1689368657000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bdowns328" rel="noopener noreferrer" target="_blank">@bdowns328</a> <a class="mention" href="https://x.com/espn" rel="noopener noreferrer" target="_blank">@espn</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> I get that he\'s smart and knows lacrosse. And he says a lot of correct things during a game. But that\'s just not a legitimate reason for keeping an abrasive commentator. He\'s got to clean up his act or ESPN needs to drop him.<br><br>In reply to: <a href="#1679959632398422017">1679959632398422017</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1679959632398422017',
    created: 1689368563000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bdowns328" rel="noopener noreferrer" target="_blank">@bdowns328</a> <a class="mention" href="https://x.com/espn" rel="noopener noreferrer" target="_blank">@espn</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> To me, it\'s clear as day. If ESPN is even reviewing these broadcasts, anyone who understands communication should be able to recognize it right away. If they don\'t, I severely question whether ESPN is even reviewing the quality of content they are putting out.<br><br>In reply to: <a href="#1679959115601424384">1679959115601424384</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1679959115601424384',
    created: 1689368439000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bdowns328" rel="noopener noreferrer" target="_blank">@bdowns328</a> <a class="mention" href="https://x.com/espn" rel="noopener noreferrer" target="_blank">@espn</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> Right off the bat, I can tell you he interrupted JT dozens of times. He was also putting words in JT\'s mouth, which was super awkward. He does it to Dana Boyle consistently. There was an incident during worlds when the other commentators asked him to cool off.<br><br>In reply to: <a href="https://x.com/bdowns328/status/1679958384374865920" rel="noopener noreferrer" target="_blank">@bdowns328</a> <span class="status">1679958384374865920</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1679958016958038016',
    created: 1689368178000,
    type: 'post',
    text: 'Hey <a class="mention" href="https://x.com/espn" rel="noopener noreferrer" target="_blank">@espn</a> and <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a>, you either train Quint Kessenich to not be so abrasive on air, or drop him as a commentator. He makes the lacrosse games really uncomfortable to watch. I\'m not usually one to even notice this stuff, but it\'s just constant.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1679955775933022208',
    created: 1689367643000,
    type: 'quote',
    text: 'Now there\'s a tech conference that definitely cares about the speakers\' health. Awesome to see and a great way to enjoy that Moroccan ☀️!<br><br>Quoting: <a href="https://x.com/DevoxxMA/status/1672929475669942273" rel="noopener noreferrer" target="_blank">@DevoxxMA</a> <span class="status">1672929475669942273</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1678713988958547970',
    created: 1689071578000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/recena" rel="noopener noreferrer" target="_blank">@recena</a> <a class="mention" href="https://x.com/RedHat" rel="noopener noreferrer" target="_blank">@RedHat</a> <a class="mention" href="https://x.com/SUSE" rel="noopener noreferrer" target="_blank">@SUSE</a> No, SUSE. They explained very fairly why they disagreed with the move by Red Hat. It came across very genuine and constructive.<br><br>In reply to: <a href="https://x.com/recena/status/1678694769264820225" rel="noopener noreferrer" target="_blank">@recena</a> <span class="status">1678694769264820225</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1678691397233430529',
    created: 1689066192000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/recena" rel="noopener noreferrer" target="_blank">@recena</a> <a class="mention" href="https://x.com/RedHat" rel="noopener noreferrer" target="_blank">@RedHat</a> <a class="mention" href="https://x.com/SUSE" rel="noopener noreferrer" target="_blank">@SUSE</a> To me, this is constructive criticism phrased genuinely. They got their message across in a civil manner.<br><br>In reply to: <a href="https://x.com/recena/status/1678687554969784321" rel="noopener noreferrer" target="_blank">@recena</a> <span class="status">1678687554969784321</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1677454995330068480',
    created: 1688771411000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> I was so glad to see Ryder on the pod. What a great catch! (In my mind, this is the best pod in lax). It was awesome to get to know him better and what changed in his approach that led to his breakout season. I can\'t wait to see what creativity he still has in store for us!<br><br>In reply to: <a href="https://x.com/danarestia/status/1676962484736098309" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1676962484736098309</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1677060122638716928',
    created: 1688677266000,
    type: 'post',
    text: 'This affects my personal account. For my business account, I can set trust rules or turn off Drive completely. But personal accounts are just an open target right now.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1677058726891757573',
    created: 1688676933000,
    type: 'post',
    text: 'Google needs to do something about the Google Drive spam. It\'s incredibly unprofessional to have a bunch of smut dropped into your drive. Can\'t they just provide a setting to prevent anyone from sharing a document that isn\'t in your allow list?!? Come on, Google!',
    likes: 6,
    retweets: 0
  },
  {
    id: '1676899715835920384',
    created: 1688639022000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> If I was granted one request to make to the world, it would be for the world to be more patient. I just feel like society wants things as soon as they think of them, rush, rush, rush, now, now, now, instead of understanding that it\'s a process.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1676898652005056513" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1676898652005056513</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1676884063368667136',
    created: 1688635290000,
    type: 'post',
    text: 'Sometimes I\'m simply amazed that I do as much as I do, and yet I still feel like I\'m standing still.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1676651718808043520',
    created: 1688579895000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bdowns328" rel="noopener noreferrer" target="_blank">@bdowns328</a> I appreciate good goalies mostly when they\'re on my team (or the team I\'m cheering on). Not so much when they\'re the facing wall ;) (Right now my two favorite PLL goalies are Blaze and Dobson).<br><br>In reply to: <a href="https://x.com/bdowns328/status/1676632159711535104" rel="noopener noreferrer" target="_blank">@bdowns328</a> <span class="status">1676632159711535104</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1676631024477028352',
    created: 1688574961000,
    type: 'quote',
    text: 'Just secured my tix. I\'m especially looking forward to seeing all my Cornell boys in action in the mile high city. Go Big Red! I can\'t wait!<br><br>Quoting: <a href="https://x.com/PremierLacrosse/status/1674500182455746560" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1674500182455746560</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1676630271540723713',
    created: 1688574781000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLRedwoods" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> I\'ll be there to see some more wizardry happen in person! #rollwoods<br><br>In reply to: <a href="https://x.com/PLLRedwoods/status/1674510926572056597" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> <span class="status">1674510926572056597</span>',
    likes: 0,
    retweets: 0,
    tags: ['rollwoods']
  },
  {
    id: '1676331193279873024',
    created: 1688503475000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> <a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> Happy Birthday, Alex! You\'re always a hero to me. Enjoy your day!<br><br>In reply to: <a href="https://x.com/alexsotob/status/1676296549523943436" rel="noopener noreferrer" target="_blank">@alexsotob</a> <span class="status">1676296549523943436</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1676013786564808705',
    created: 1688427800000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> In my view, not good enough. With 0.3s left on the clock, this was clearly malicious. There are too many deserving players looking for an opportunity. I think he should at least be suspended from the next World Championships at a minimum.<br><br>In reply to: <a href="https://x.com/danarestia/status/1675872027310268418" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1675872027310268418</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1675755367974797314',
    created: 1688366188000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/HAU_Nationals" rel="noopener noreferrer" target="_blank">@HAU_Nationals</a> <a class="mention" href="https://x.com/UPS" rel="noopener noreferrer" target="_blank">@UPS</a> <a class="mention" href="https://x.com/jpiseno5" rel="noopener noreferrer" target="_blank">@jpiseno5</a> <a class="mention" href="https://x.com/WorldLacrosse" rel="noopener noreferrer" target="_blank">@WorldLacrosse</a> <a class="mention" href="https://x.com/UAlbanyMLax" rel="noopener noreferrer" target="_blank">@UAlbanyMLax</a> So well deserved. I kept having to check jersey numbers because I swear there was more than one of him on the field! He was everywhere doing everything, including assisting goals. A true LSE (long stick everywhere!)<br><br>In reply to: <a href="https://x.com/HAU_Nationals/status/1675320842107105280" rel="noopener noreferrer" target="_blank">@HAU_Nationals</a> <span class="status">1675320842107105280</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1674942116940886016',
    created: 1688172294000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/HAU_Nationals" rel="noopener noreferrer" target="_blank">@HAU_Nationals</a> Another gritty game! There\'s no doubt these have been the marquee match-ups of the tournament for me. The endurance and toughness shown was unmatched. And it was exhilarating to see Piseno rise to the occasion and the offense find such creative ways to get to the back of the net.<br><br>In reply to: <a href="https://x.com/HAU_Nationals/status/1674597083104309250" rel="noopener noreferrer" target="_blank">@HAU_Nationals</a> <span class="status">1674597083104309250</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1674599867862204416',
    created: 1688090695000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gscottshaw" rel="noopener noreferrer" target="_blank">@gscottshaw</a> I\'m wondering if we\'ll ever cross that chasm. But one day, one day. ;)<br><br>In reply to: <a href="https://x.com/gscottshaw/status/1674596979609858048" rel="noopener noreferrer" target="_blank">@gscottshaw</a> <span class="status">1674596979609858048</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1674505782438809600',
    created: 1688068264000,
    type: 'post',
    text: 'The weather has been beautiful all week and I\'ve been inside grinding away at work. Today I finally get to take a break to go relax a bit...and, of course, it\'s going to rain all afternoon. This is a snapshot of what this whole summer has been so far.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1674359693123420160',
    created: 1688033433000,
    type: 'quote',
    text: 'This is incredibly cool. A music video by <a class="mention" href="https://x.com/dj_dave____" rel="noopener noreferrer" target="_blank">@dj_dave____</a> and <a class="mention" href="https://x.com/AndrewRolfes" rel="noopener noreferrer" target="_blank">@AndrewRolfes</a> that prominently features the main instrument of a coding DJ, code running on <a class="mention" href="https://x.com/Sonic_Pi" rel="noopener noreferrer" target="_blank">@Sonic_Pi</a>!<br><br>Quoting: <a href="https://x.com/dj_dave____/status/1673824774605377536" rel="noopener noreferrer" target="_blank">@dj_dave____</a> <span class="status">1673824774605377536</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1674155340068429824',
    created: 1687984712000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> I also want to get this poster to hang next to it.<br><br>In reply to: <a href="#1674154743210582022">1674154743210582022</a>',
    photos: ['<div class="item"><img class="photo" src="media/1674155340068429824.jpg"></div>'],
    likes: 1,
    retweets: 0
  },
  {
    id: '1674154743210582022',
    created: 1687984569000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> I have this flag hanging prominently on the wall in the main room of my apartment. It reminds me of the diverse and intersectional world we live in, and that everyone in this world has a right to live and be free.<br><br>In reply to: <a href="https://x.com/alexsotob/status/1674135495276744709" rel="noopener noreferrer" target="_blank">@alexsotob</a> <span class="status">1674135495276744709</span>',
    photos: ['<div class="item"><img class="photo" src="media/1674154743210582022.jpg"></div>'],
    likes: 4,
    retweets: 1
  },
  {
    id: '1674143283239591937',
    created: 1687981837000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> <a class="mention" href="https://x.com/dblevins" rel="noopener noreferrer" target="_blank">@dblevins</a> You\'d be surprised, a lot of employers don\'t allow you to work on open source even on your own time. But it highly depends on country (particularly the US) and how employment contracts are structured.<br><br>In reply to: <a href="https://x.com/abelsromero/status/1674141907256070147" rel="noopener noreferrer" target="_blank">@abelsromero</a> <span class="status">1674141907256070147</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1674133479456657408',
    created: 1687979500000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dblevins" rel="noopener noreferrer" target="_blank">@dblevins</a> I replied "allowed on company time", but then again I started my own company so I could work on open source full time ;)<br><br>In reply to: <a href="https://x.com/dblevins/status/1674122227510575104" rel="noopener noreferrer" target="_blank">@dblevins</a> <span class="status">1674122227510575104</span>',
    likes: 9,
    retweets: 0
  },
  {
    id: '1673794920241594368',
    created: 1687898781000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/talios" rel="noopener noreferrer" target="_blank">@talios</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> Hmm. Does macOS not provide a complete CA bundle? Surely <a href="http://gitlab.com" rel="noopener noreferrer" target="_blank">gitlab.com</a> must be covered by one of them? No?<br><br>Several workarounds are documented here: <a href="https://gitlab.com/antora/antora/-/issues/390" rel="noopener noreferrer" target="_blank">gitlab.com/antora/antora/-/issues/390</a><br><br>In reply to: <a href="https://x.com/talios/status/1673617034889003013" rel="noopener noreferrer" target="_blank">@talios</a> <span class="status">1673617034889003013</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1673607749710798848',
    created: 1687854156000,
    type: 'reply',
    text: 'And <a class="mention" href="https://x.com/ggrossetie" rel="noopener noreferrer" target="_blank">@ggrossetie</a> has been a fantastic mentor and subject matter expert throughout it all.<br><br>In reply to: <a href="#1673603343724453889">1673603343724453889</a>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1673604411636219904',
    created: 1687853360000,
    type: 'post',
    text: 'Here\'s the issue that documents the behind-the-scenes work to study and appreciate the process: <a href="https://github.com/asciidoctor/asciidoctor-browser-extension/issues/648" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-browser-extension/issues/648</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1673603343724453889',
    created: 1687853105000,
    type: 'post',
    text: 'I want to highlight <a href="https://docs.asciidoctor.org/browser-extension/latest/" rel="noopener noreferrer" target="_blank">docs.asciidoctor.org/browser-extension/latest/</a> because it\'s a firm example of how great tech writing can strengthen an open source project. Contributor <a class="mention" href="https://x.com/SturtIson" rel="noopener noreferrer" target="_blank">@SturtIson</a> lent time &amp; expertise to transform a sprawling README into a set of well-written documentation pages. Amazing work!',
    likes: 16,
    retweets: 6
  },
  {
    id: '1673245484037832704',
    created: 1687767785000,
    type: 'post',
    text: 'The two <a class="mention" href="https://x.com/HAU_Nationals" rel="noopener noreferrer" target="_blank">@HAU_Nationals</a> games this weekend (against US, then Canada) were the hardest fought and most exciting in the World Lacrosse Men\'s Championship thus far. They may not have come out with the win, but they were winners in my heart. Such solid stick work.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1672927875815841792',
    created: 1687692061000,
    type: 'post',
    text: 'I want to emphasize that we aren\'t just working on a specification for AsciiDoc, we\'re working on a specification using an open process with an open governance model, producing it under an open license, and certifying implementations (preferably open) using an open TCK. Clear?',
    likes: 28,
    retweets: 6
  },
  {
    id: '1672861253696634881',
    created: 1687676177000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bdowns328" rel="noopener noreferrer" target="_blank">@bdowns328</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/PLLRedwoods" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> <a class="mention" href="https://x.com/NLLwings" rel="noopener noreferrer" target="_blank">@NLLwings</a> I think we might be kindred spirits ;)<br><br>In reply to: <a href="https://x.com/bdowns328/status/1672843727747059714" rel="noopener noreferrer" target="_blank">@bdowns328</a> <span class="status">1672843727747059714</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1672514275586260992',
    created: 1687593451000,
    type: 'post',
    text: 'Last weekend, my spouse and I discovered the card-oriented game Wingspan. We were immediately hooked. It\'s fun, informative, and beautifully illustrated. If you\'re looking for something to do with your family away from screens, give it a try.',
    likes: 7,
    retweets: 0
  },
  {
    id: '1672395524698415104',
    created: 1687565139000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> I meant to say thanks for pulling together this list. Being able to see where everyone is playing has really enhanced my viewing experience. I appreciate it!<br><br>In reply to: <a href="#1672392340726763521">1672392340726763521</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1672392340726763521',
    created: 1687564380000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> One more small nitpick. DeLuca is spelled with a capital L (shown this way on the PLL site).<br><br>In reply to: <a href="#1672391887259578369">1672391887259578369</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1672391887259578369',
    created: 1687564272000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> Bobby Russo from Rutgers is also missing on the list of players for Italy.<br><br>In reply to: <a href="#1672201807366598657">1672201807366598657</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1672201807366598657',
    created: 1687518953000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> FYI, I was able to confirm by watching the first Italy game that it is John Piatelli from Cornell playing. There\'s an error in his entry in the post (it lists his college as Italy).<br><br>In reply to: <a href="https://x.com/danarestia/status/1671870242622955521" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1671870242622955521</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1671994596929449985',
    created: 1687469550000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emilianbold" rel="noopener noreferrer" target="_blank">@emilianbold</a> <a class="mention" href="https://x.com/OpenSourceOrg" rel="noopener noreferrer" target="_blank">@OpenSourceOrg</a> Thanks for providing that additional context.<br><br>In reply to: <a href="https://x.com/emilianbold/status/1671990054246309892" rel="noopener noreferrer" target="_blank">@emilianbold</a> <span class="status">1671990054246309892</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1671987305597530119',
    created: 1687467812000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emilianbold" rel="noopener noreferrer" target="_blank">@emilianbold</a> <a class="mention" href="https://x.com/OpenSourceOrg" rel="noopener noreferrer" target="_blank">@OpenSourceOrg</a> The idea of people out there just writing software for free with no income is a fantasy world that has no basis in reality. (It could exist if we had universal basic income, but we don\'t).<br><br>In reply to: <a href="#1671987069118476293">1671987069118476293</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1671987069118476293',
    created: 1687467755000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emilianbold" rel="noopener noreferrer" target="_blank">@emilianbold</a> <a class="mention" href="https://x.com/OpenSourceOrg" rel="noopener noreferrer" target="_blank">@OpenSourceOrg</a> It appeals to me from an eating perspective. I have brushed rock bottom as a maintainer of open source software. Unless I can rely on an inheritance (which I don\'t have), I can\'t continue to live and write software without some way to make money. And there\'s such a model.<br><br>In reply to: <a href="https://x.com/emilianbold/status/1671985133916659714" rel="noopener noreferrer" target="_blank">@emilianbold</a> <span class="status">1671985133916659714</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1671983737305968640',
    created: 1687466961000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emilianbold" rel="noopener noreferrer" target="_blank">@emilianbold</a> <a class="mention" href="https://x.com/OpenSourceOrg" rel="noopener noreferrer" target="_blank">@OpenSourceOrg</a> I don\'t see open source that way (perhaps a flaw of mine). To me, open source is about the licenses and the open source definition. At heart, I\'m really not a fan of "free software" because I think it leads to being taken advantage of. Open source is freedom + sustainability.<br><br>In reply to: <a href="https://x.com/emilianbold/status/1671982630282076168" rel="noopener noreferrer" target="_blank">@emilianbold</a> <span class="status">1671982630282076168</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1671982499160023040',
    created: 1687466666000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emilianbold" rel="noopener noreferrer" target="_blank">@emilianbold</a> <a class="mention" href="https://x.com/OpenSourceOrg" rel="noopener noreferrer" target="_blank">@OpenSourceOrg</a> Interesting to note that the program page is using the OSI Certified trademark, so OSI may still be interested in how that term is being used adjacent to the open source brand. Or perhaps we\'ve already lost this battle.<br><br>In reply to: <a href="#1671982173367443456">1671982173367443456</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1671982173367443456',
    created: 1687466588000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/emilianbold" rel="noopener noreferrer" target="_blank">@emilianbold</a> <a class="mention" href="https://x.com/OpenSourceOrg" rel="noopener noreferrer" target="_blank">@OpenSourceOrg</a> Interesting. Regarding "Because the phrase “open source” cannot be trademarked, we must rely<br>on market pressure to protect the concept from abuse", hopefully my voice is helping to apply that market pressure.<br><br>In reply to: <a href="https://x.com/emilianbold/status/1671981289858015264" rel="noopener noreferrer" target="_blank">@emilianbold</a> <span class="status">1671981289858015264</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1671981578493513729',
    created: 1687466446000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joaompinto4" rel="noopener noreferrer" target="_blank">@joaompinto4</a> I recognize there\'s nuance here. In terms of software, no part of GitHub is open, whereas at least the core of GitLab is open. GitLab has also been more receptive to better Asciidoctor integration, which matters to me. In terms of GitHub supporting OSS, you may have a good point.<br><br>In reply to: <a href="https://x.com/joaompinto4/status/1671959597798531089" rel="noopener noreferrer" target="_blank">@joaompinto4</a> <span class="status">1671959597798531089</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1671955445928562688',
    created: 1687460216000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> <a class="mention" href="https://x.com/OpenSourceOrg" rel="noopener noreferrer" target="_blank">@OpenSourceOrg</a> The wording JetBrains uses is slightly more honest, despite the same label. I think it\'s because it starts with "non-commercial open source". It\'s still very concerning that open source is being split into tiers which benefits affluent people who don\'t need to make a living.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1671893627088568322" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1671893627088568322</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1671953601772818433',
    created: 1687459776000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gjtorikian" rel="noopener noreferrer" target="_blank">@gjtorikian</a> <a class="mention" href="https://x.com/gitlab" rel="noopener noreferrer" target="_blank">@gitlab</a> Oh, I hear you. I was just adding some data points from my vantage point. I\'m still taking it all in.<br><br>In reply to: <a href="https://x.com/gjtorikian/status/1671935807198986241" rel="noopener noreferrer" target="_blank">@gjtorikian</a> <span class="status">1671935807198986241</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1671952806826364929',
    created: 1687459587000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/pxquim" rel="noopener noreferrer" target="_blank">@pxquim</a> That\'s why we\'re making a specification ;) But it\'s no small task. This is one of the hardest problems I\'ve ever had to solve in my professional career.<br><br>In reply to: <a href="https://x.com/pxquim/status/1671854262232416257" rel="noopener noreferrer" target="_blank">@pxquim</a> <span class="status">1671854262232416257</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1671845501006905344',
    created: 1687434003000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexellisuk" rel="noopener noreferrer" target="_blank">@alexellisuk</a> <a class="mention" href="https://x.com/OpenSourceOrg" rel="noopener noreferrer" target="_blank">@OpenSourceOrg</a> To their credit, they did make an exemption for Antora. My intent is not to knock them. I just think the framing is unjust and I\'d like to see it corrected. I\'m hopeful they\'ll do the right thing.<br><br>In reply to: <a href="https://x.com/alexellisuk/status/1671841385966628866" rel="noopener noreferrer" target="_blank">@alexellisuk</a> <span class="status">1671841385966628866</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1671840988640456705',
    created: 1687432927000,
    type: 'post',
    text: 'In other news, I\'m continuing to make great headway with the effort to wrangle AsciiDoc into a formal grammar. While it\'s often two steps forward, one step back, the picture is getting clearer each week. I\'m getting ever more excited about the possibilities it\'s going to open up.',
    likes: 12,
    retweets: 0
  },
  {
    id: '1671804637681451008',
    created: 1687424260000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/yasegumi" rel="noopener noreferrer" target="_blank">@yasegumi</a> <a class="mention" href="https://x.com/OpenSourceOrg" rel="noopener noreferrer" target="_blank">@OpenSourceOrg</a> If GitLab pulls the plug on the whole program, I wouldn\'t object. They are not required to offer anything they don\'t want to offer. But they can\'t discriminate against certain open source projects in an open source program. That\'s putting tiers on open source.<br><br>In reply to: <a href="#1671804134406909952">1671804134406909952</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1671804134406909952',
    created: 1687424140000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/yasegumi" rel="noopener noreferrer" target="_blank">@yasegumi</a> <a class="mention" href="https://x.com/OpenSourceOrg" rel="noopener noreferrer" target="_blank">@OpenSourceOrg</a> You\'re simply incorrect. They are using the banner Open Source, then saying that a requirement is that the project maintainers not seek profit. That IS redefining open source, even if you don\'t think it is.<br><br>In reply to: <a href="https://x.com/yasegumi/status/1671803736220635136" rel="noopener noreferrer" target="_blank">@yasegumi</a> <span class="status">1671803736220635136</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1671803811592302593',
    created: 1687424063000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/yasegumi" rel="noopener noreferrer" target="_blank">@yasegumi</a> <a class="mention" href="https://x.com/OpenSourceOrg" rel="noopener noreferrer" target="_blank">@OpenSourceOrg</a> While I would be disappointed if GitLab decided to keep this requirement under a new program name, I wouldn\'t be objecting to it as I am.<br><br>In reply to: <a href="#1671803626992603136">1671803626992603136</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1671803626992603136',
    created: 1687424019000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/yasegumi" rel="noopener noreferrer" target="_blank">@yasegumi</a> <a class="mention" href="https://x.com/OpenSourceOrg" rel="noopener noreferrer" target="_blank">@OpenSourceOrg</a> First, I want to be clear GitLab is free to offer whatever they want. The problem I have is the terminology they\'re using. They\'re labeling this "GitLab for Open Source", then redefining what open source is. If they labeled it "GitLab for Not-For-Profit", it would acceptable.<br><br>In reply to: <a href="https://x.com/yasegumi/status/1671802354927288320" rel="noopener noreferrer" target="_blank">@yasegumi</a> <span class="status">1671802354927288320</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1671780322977955840',
    created: 1687418463000,
    type: 'post',
    text: 'I\'m also shocked to see former Red Hat employees (and colleagues) behind this program. I would expect this from newcomers to open source, not ones with that kind of background. I expect better.',
    likes: 5,
    retweets: 1
  },
  {
    id: '1671779627138777092',
    created: 1687418297000,
    type: 'post',
    text: 'I think if GitLab is allowed to do this, then it sends a message that anyone can define open source however they please. To me, this spells trouble for open source.',
    likes: 5,
    retweets: 1
  },
  {
    id: '1671778345598566400',
    created: 1687417992000,
    type: 'post',
    text: 'I\'d be interested to know how <a class="mention" href="https://x.com/OpenSourceOrg" rel="noopener noreferrer" target="_blank">@OpenSourceOrg</a> feels about GitLab using the term "Open Source" for this program &amp; attaching the "must not seek profit" clause to it, especially given they specifically make reference to OSI licenses. <a href="https://about.gitlab.com/handbook/marketing/developer-relations/community-programs/opensource-program/#who-qualifies-for-the-gitlab-for-open-source-program" rel="noopener noreferrer" target="_blank">about.gitlab.com/handbook/marketing/developer-relations/community-programs/opensource-program/#who-qualifies-for-the-gitlab-for-open-source-program</a>',
    likes: 9,
    retweets: 0
  },
  {
    id: '1671769836429664257',
    created: 1687415963000,
    type: 'quote',
    text: 'Kroki is an amazing tool.<br><br>Quoting: <a href="https://x.com/verhas/status/1670878853235867649" rel="noopener noreferrer" target="_blank">@verhas</a> <span class="status">1670878853235867649</span>',
    likes: 7,
    retweets: 1
  },
  {
    id: '1671694599411363841',
    created: 1687398025000,
    type: 'quote',
    text: 'I\'ve never been upset about a PLL game until this one. <a class="mention" href="https://x.com/rsgarnz50" rel="noopener noreferrer" target="_blank">@rsgarnz50</a> was unreal, no doubt about it. But he was getting 0 support from the midfield or the coaching staff. There better be some changes before the next game.<br><br>Quoting: <a href="https://x.com/PLLRedwoods/status/1670233697512042498" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> <span class="status">1670233697512042498</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1671692877196595200',
    created: 1687397615000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PeteSchirrick17" rel="noopener noreferrer" target="_blank">@PeteSchirrick17</a> <a class="mention" href="https://x.com/PLLRedwoods" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> 💯<br><br>In reply to: <a href="https://x.com/PeteSchirrick17/status/1670234333490151424" rel="noopener noreferrer" target="_blank">@PeteSchirrick17</a> <span class="status">1670234333490151424</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1671692757730209792',
    created: 1687397586000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLRedwoods" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> Please bench Jones and Perkovic or watch this season go down the tubes. That loss was all on the coaching staff. And what were those defensive mishaps at the end of the game? Come on!!<br><br>In reply to: <a href="https://x.com/PLLRedwoods/status/1670233697512042498" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> <span class="status">1670233697512042498</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1671680173723516928',
    created: 1687394586000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gjtorikian" rel="noopener noreferrer" target="_blank">@gjtorikian</a> <a class="mention" href="https://x.com/gitlab" rel="noopener noreferrer" target="_blank">@gitlab</a> I\'ll say that my experience working with GitLab up to this point has been very positive, though specifically in other contexts. The Asciidoctor integration is the best out there, and they\'ve been receptive to enhancements. This situation very much took me by surprise.<br><br>In reply to: <a href="https://x.com/gjtorikian/status/1671677574332944386" rel="noopener noreferrer" target="_blank">@gjtorikian</a> <span class="status">1671677574332944386</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1671674035242164224',
    created: 1687393122000,
    type: 'post',
    text: 'Here\'s my formal objection: <a href="https://gitlab.com/gitlab-com/marketing/community-relations/open-source-program/gitlab-for-open-source/-/issues/44" rel="noopener noreferrer" target="_blank">gitlab.com/gitlab-com/marketing/community-relations/open-source-program/gitlab-for-open-source/-/issues/44</a>',
    likes: 16,
    retweets: 0
  },
  {
    id: '1671673889683046405',
    created: 1687393088000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sugaroverflow" rel="noopener noreferrer" target="_blank">@sugaroverflow</a> Here\'s my feedback: <a href="https://gitlab.com/gitlab-com/marketing/community-relations/open-source-program/gitlab-for-open-source/-/issues/44" rel="noopener noreferrer" target="_blank">gitlab.com/gitlab-com/marketing/community-relations/open-source-program/gitlab-for-open-source/-/issues/44</a><br><br>In reply to: <a href="https://x.com/sugaroverflow/status/1671386700600455169" rel="noopener noreferrer" target="_blank">@sugaroverflow</a> <span class="status">1671386700600455169</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1671642607972384768',
    created: 1687385630000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joaompinto4" rel="noopener noreferrer" target="_blank">@joaompinto4</a> We choose it because we believed it aligned better with our open source values (the GitLab platform itself is open). That turned out to be a real slap in the face when our project was the one that got flagged as not open source.<br><br>In reply to: <a href="https://x.com/joaompinto4/status/1671602221593550861" rel="noopener noreferrer" target="_blank">@joaompinto4</a> <span class="status">1671602221593550861</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1671642019670933507',
    created: 1687385489000,
    type: 'post',
    text: 'After my stern objection to this decision, GitLab has decided to grant an exemption for Antora and renew it into the program. However, I still take issue with the fact that an exemption is even necessary and continue to be concerned how this impacts open source in general.',
    likes: 20,
    retweets: 0
  },
  {
    id: '1671434392332533760',
    created: 1687335987000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/verhas" rel="noopener noreferrer" target="_blank">@verhas</a> <a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> Nice analysis. Is "code language" here referring to a programming language, or does it mean "using coded language" (as in cryptic communication)?<br><br>In reply to: <a href="https://x.com/verhas/status/1671398611391791104" rel="noopener noreferrer" target="_blank">@verhas</a> <span class="status">1671398611391791104</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1671421340384124928',
    created: 1687332875000,
    type: 'post',
    text: '"We can\'t let you support yourself, but we will give you free minutes on our platform that makes money using your software" is not how you support open source maintainers. You know, for those who need to hear it.',
    likes: 7,
    retweets: 2
  },
  {
    id: '1671407547151187969',
    created: 1687329587000,
    type: 'post',
    text: 'I plan on filing an issue to request that GitLab either a) stop using the term open source for this program or b) stop dictating how maintainers support themselves. As written now, I believe GitLab is violating the meaning of open source and, by doing so, harming open source.',
    likes: 41,
    retweets: 5
  },
  {
    id: '1671396757773905920',
    created: 1687327014000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sugaroverflow" rel="noopener noreferrer" target="_blank">@sugaroverflow</a> Thank you for reaching out. While the decision may have been automated, the original message was definitely written by a GitLab team member. While an exemption has since been offered, I have a real problem with this policy &amp; how it affects open source in general. It\'s not right.<br><br>In reply to: <a href="https://x.com/sugaroverflow/status/1671386602680221696" rel="noopener noreferrer" target="_blank">@sugaroverflow</a> <span class="status">1671386602680221696</span>',
    likes: 21,
    retweets: 0
  },
  {
    id: '1671312267118510081',
    created: 1687306870000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/karawapo" rel="noopener noreferrer" target="_blank">@karawapo</a> That\'s precisely what I suggested to them, so we\'re thinking alike!<br><br>In reply to: <a href="https://x.com/karawapo/status/1671312014462050304" rel="noopener noreferrer" target="_blank">@karawapo</a> <span class="status">1671312014462050304</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1671308692397117440',
    created: 1687306018000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/leifgruenwoldt" rel="noopener noreferrer" target="_blank">@leifgruenwoldt</a> Regardless, they\'re attempting to change the very well-established definition of open source, which is both surprising and unacceptable coming from a company that positions itself as an open source leader.<br><br>In reply to: <a href="https://x.com/leifgruenwoldt/status/1671307583041204225" rel="noopener noreferrer" target="_blank">@leifgruenwoldt</a> <span class="status">1671307583041204225</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1671303843412606976',
    created: 1687304862000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/leifgruenwoldt" rel="noopener noreferrer" target="_blank">@leifgruenwoldt</a> Correct.<br><br>In reply to: <a href="https://x.com/leifgruenwoldt/status/1671303167630254082" rel="noopener noreferrer" target="_blank">@leifgruenwoldt</a> <span class="status">1671303167630254082</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1671301123028955136',
    created: 1687304213000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/leifgruenwoldt" rel="noopener noreferrer" target="_blank">@leifgruenwoldt</a> In other words, they are attempting to classify us (and thus charge as) as non-open source.<br><br>In reply to: <a href="#1671300740927860736">1671300740927860736</a>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1671300740927860736',
    created: 1687304122000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/leifgruenwoldt" rel="noopener noreferrer" target="_blank">@leifgruenwoldt</a> It\'s an organization status, which allows us to use GitLab to run the project without having to pay a fee as though we\'re a business customer.<br><br>In reply to: <a href="https://x.com/leifgruenwoldt/status/1671298664554287106" rel="noopener noreferrer" target="_blank">@leifgruenwoldt</a> <span class="status">1671298664554287106</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1671300422320156672',
    created: 1687304046000,
    type: 'post',
    text: 'I also want to be clear that this was a renewal. This same application for Antora had already been approved in previous years under the exact same set of circumstances. They\'re now taking this status away. So they\'re also being hypocritical.',
    likes: 23,
    retweets: 1
  },
  {
    id: '1671277055420383232',
    created: 1687298475000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lofidewanto" rel="noopener noreferrer" target="_blank">@lofidewanto</a> <a class="mention" href="https://x.com/khmarbaise" rel="noopener noreferrer" target="_blank">@khmarbaise</a> <a class="mention" href="https://x.com/gitlab" rel="noopener noreferrer" target="_blank">@gitlab</a> Yep, if open source means you can\'t run a business to support it, then there\'s no more open source. They clearly want open source to die by choosing this stance.<br><br>In reply to: <a href="https://x.com/lofidewanto/status/1671276713819471872" rel="noopener noreferrer" target="_blank">@lofidewanto</a> <span class="status">1671276713819471872</span>',
    likes: 11,
    retweets: 0
  },
  {
    id: '1671273963408826369',
    created: 1687297738000,
    type: 'post',
    text: 'You can call me lots of things, but I won\'t accept being labeled as "not open source enough". If you claim that, clearly you know nothing about me. I work and communicate in the open to a fault every single day.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1671272458530471936',
    created: 1687297379000,
    type: 'post',
    text: 'Antora is also MPL-2.0-licensed and thus free for anyone to use however they please. We do all work on Antora in the open and have since day #1. Antora is as pure an open source project as you\'ll find. We never do anything in private. I even encourage all users to be open too.',
    likes: 34,
    retweets: 0
  },
  {
    id: '1671271653077315585',
    created: 1687297187000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/iphigenie" rel="noopener noreferrer" target="_blank">@iphigenie</a> Lack of being able to live, apparently. I think they prefer dead maintainers.<br><br>In reply to: <a href="https://x.com/iphigenie/status/1671271371018588160" rel="noopener noreferrer" target="_blank">@iphigenie</a> <span class="status">1671271371018588160</span>',
    likes: 13,
    retweets: 0
  },
  {
    id: '1671271543798898690',
    created: 1687297161000,
    type: 'reply',
    text: 'I also helped advocate for the <a class="mention" href="https://x.com/EclipseFdn" rel="noopener noreferrer" target="_blank">@EclipseFdn</a> to adopt GitLab. The AsciiDoc WG and AsciiDoc Language projects were one of the first projects to use it, again going against the grain. I\'ve done too much for GitLab for them to show such disrespect to me now.<br><br>In reply to: <a href="#1671271043238092800">1671271043238092800</a>',
    likes: 17,
    retweets: 0
  },
  {
    id: '1671271043238092800',
    created: 1687297042000,
    type: 'post',
    text: 'We also took a huge risk to choose GitLab for Antora instead of GitHub because we wanted to promote open source from top to bottom. We could have easily chosen GitHub, but we didn\'t it. To be classified as "not open source enough" has me thoroughly pissed off.',
    likes: 29,
    retweets: 0
  },
  {
    id: '1671270342923542530',
    created: 1687296875000,
    type: 'reply',
    text: 'If GitLab doesn\'t reconsider this decision, I\'ll never contribute to GitLab again, nor will I continue to advocate for the platform. I\'m not going to be treated this way. I\'ve put too much for too long into open source to be brushed aside like this. cc: <a class="mention" href="https://x.com/k33g_org" rel="noopener noreferrer" target="_blank">@k33g_org</a> <a class="mention" href="https://x.com/RJDickenson" rel="noopener noreferrer" target="_blank">@RJDickenson</a><br><br>In reply to: <a href="#1671269140538204161">1671269140538204161</a>',
    likes: 21,
    retweets: 0
  },
  {
    id: '1671269710602866690',
    created: 1687296724000,
    type: 'post',
    text: 'To provide more context, I offer services and support for Asciidoctor and Antora so I can work on those projects full time. Otherwise, both projects would have no maintainer. There has to be some way to pay the damn bills. Do people understand nothing about how this world works?',
    likes: 45,
    retweets: 2
  },
  {
    id: '1671269140538204161',
    created: 1687296588000,
    type: 'post',
    text: 'GitLab denied the open source application for Antora, claiming we\'re using it for business-related, revenue-generating purposes. This is outrageous! I maintain Asciidoctor, which is used in GitLab for them to generate revenue. Double standards! I\'m furious.',
    likes: 264,
    retweets: 73
  },
  {
    id: '1670913644631293952',
    created: 1687211831000,
    type: 'post',
    text: 'I\'m doing much better as of late, but it was a long road to get back to basic strength. I essentially had to do a full rebuild and suffer all the muscle pulls that came with that process. Pain free days are fun.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1670913431439032320',
    created: 1687211780000,
    type: 'post',
    text: 'A hard lesson I\'ve learned from the covid era, and as a tech professional in general, is that muscle atrophy is painful. My advice is that even if you hate working out, you\'ll hate being in constant pain more. You\'re working out to avoid pain and dumb injuries that cause it.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1670912139991879681',
    created: 1687211472000,
    type: 'post',
    text: 'While driving back from Vail Valley this weekend, it was *snowing* at Independence Pass. In the middle of June! When can it finally be summer? When?!?',
    likes: 1,
    retweets: 0
  },
  {
    id: '1670895549212692485',
    created: 1687207517000,
    type: 'quote',
    text: '"We firmly believe that working to dismantle white supremacy is a condition of membership in the climate movement."<br><br>💯<br><br>Quoting: <a href="https://x.com/350_US/status/1670823692836696066" rel="noopener noreferrer" target="_blank">@350_US</a> <span class="status">1670823692836696066</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1670894641460449282',
    created: 1687207300000,
    type: 'post',
    text: 'Reparations now. #Juneteenth2023',
    likes: 0,
    retweets: 0,
    tags: ['juneteenth2023']
  },
  {
    id: '1670393581213982720',
    created: 1687087838000,
    type: 'post',
    text: 'YouTube Music is an embarrassment. It can\'t even make it though one song playing the the background before it crashes. I can\'t believe Google actually puts its name on this app, or maybe this is just the new normal for Google. Shameful. I long for Google Music, which just worked.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1670335887576821762',
    created: 1687074083000,
    type: 'post',
    text: 'The answer to my own question is "yes, all 107 games".',
    likes: 0,
    retweets: 0
  },
  {
    id: '1669593593559023618',
    created: 1686897106000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/unleashedwlax" rel="noopener noreferrer" target="_blank">@unleashedwlax</a> YES! That\'s coming first and foremost from caring about the safety of the athletes. I also happen to think it will drastically improve the game. Now just add gloves and we\'ll be well on our way to women athletes being properly equipped to play this sport.<br><br>In reply to: <a href="https://x.com/unleashedwlax/status/1669085132886622210" rel="noopener noreferrer" target="_blank">@unleashedwlax</a> <span class="status">1669085132886622210</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1669119662687424512',
    created: 1686784112000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/WorldLacrosse" rel="noopener noreferrer" target="_blank">@WorldLacrosse</a> "All 107 games on ESPN+"<br><br>That\'s everything I needed to know. Sweet.<br><br>In reply to: <a href="https://x.com/WorldLacrosse/status/1668960710947397632" rel="noopener noreferrer" target="_blank">@WorldLacrosse</a> <span class="status">1668960710947397632</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1668789442512056322',
    created: 1686705382000,
    type: 'quote',
    text: 'I wish I would have had this wisdom in high school. I made the wrong choice too many times...but no longer.<br><br>Quoting: <a href="https://x.com/SidneyMiller2/status/1478720258579910656" rel="noopener noreferrer" target="_blank">@SidneyMiller2</a> <span class="status">1478720258579910656</span>',
    likes: 9,
    retweets: 0
  },
  {
    id: '1668789058804551681',
    created: 1686705290000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> For the record, I hate the shot clock change, namely that it changes based on situation. It\'s just dumb. This has to go. It\'s terrible for spectators. I\'m so focused on the damn clock I can\'t enjoy the game. It\'s ruining a good thing.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1668788525922410496',
    created: 1686705163000,
    type: 'post',
    text: 'Not only do we see players confused, it\'s also becoming an obsession for the commentators. It\'s distracting. I can only imagine that new viewers just don\'t have a damn clue. There\'s just no reason for this. In our household, we don\'t like it. Just pick a number and stick with it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1668787996777414657',
    created: 1686705037000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> I\'ve watched every minute of every game so far this season. First, the lacrosse is amazing, exciting &amp; often unexpected. ♥️ it. But don\'t love the shot clock adjustments. IMO it was a mistake. Not because it\'s too short. It\'s too confusing. Just lock it at ~45s.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1668785521366274049',
    created: 1686704447000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLChaos" rel="noopener noreferrer" target="_blank">@PLLChaos</a> <a class="mention" href="https://x.com/Joshbyrne94" rel="noopener noreferrer" target="_blank">@Joshbyrne94</a> <a class="mention" href="https://x.com/nickrow37" rel="noopener noreferrer" target="_blank">@nickrow37</a> <a class="mention" href="https://x.com/c_cloutier45" rel="noopener noreferrer" target="_blank">@c_cloutier45</a> <a class="mention" href="https://x.com/The_Fraze95" rel="noopener noreferrer" target="_blank">@The_Fraze95</a> <a class="mention" href="https://x.com/JNEU_88" rel="noopener noreferrer" target="_blank">@JNEU_88</a> <a class="mention" href="https://x.com/Smith_Ryan5" rel="noopener noreferrer" target="_blank">@Smith_Ryan5</a> <a class="mention" href="https://x.com/wperry1032" rel="noopener noreferrer" target="_blank">@wperry1032</a> This is how you lacrosse. Plus, it\'s super exciting.<br><br>In reply to: <a href="https://x.com/PLLChaos/status/1668783538047692800" rel="noopener noreferrer" target="_blank">@PLLChaos</a> <span class="status">1668783538047692800</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1668574818147655680',
    created: 1686654211000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLCannons" rel="noopener noreferrer" target="_blank">@PLLCannons</a> <a class="mention" href="https://x.com/asher_nolting" rel="noopener noreferrer" target="_blank">@asher_nolting</a> Best goal of the weekend. So clutch. So clutch. It counted for sure. A<br><br>In reply to: <a href="https://x.com/PLLCannons/status/1668327204277862401" rel="noopener noreferrer" target="_blank">@PLLCannons</a> <span class="status">1668327204277862401</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1668574299790409729',
    created: 1686654088000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLRedwoods" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> <a class="mention" href="https://x.com/ChampionUSA" rel="noopener noreferrer" target="_blank">@ChampionUSA</a> Oh, I will. These uniforms are sick!<br><br>In reply to: <a href="https://x.com/PLLRedwoods/status/1668342523557998592" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> <span class="status">1668342523557998592</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1668165232395534337',
    created: 1686556559000,
    type: 'post',
    text: 'I recently discovered The Charismatic Voice on YouTube and I\'m down a rabbit hole reexamining grunge vocalists, from Cobain to Staley to Cornell. What a time to have lived through! 🎸',
    likes: 4,
    retweets: 0
  },
  {
    id: '1667055792212832258',
    created: 1686292047000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/fanf42" rel="noopener noreferrer" target="_blank">@fanf42</a> In lacrosse, we call this hero ball. Everyone wants to be one who scores the goal, but few want to be a good team player that contributes the off-ball help.<br><br>In reply to: <a href="https://x.com/fanf42/status/1667050936647274501" rel="noopener noreferrer" target="_blank">@fanf42</a> <span class="status">1667050936647274501</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1666755300576329728',
    created: 1686220405000,
    type: 'quote',
    text: 'I would also add becoming vegan, which is really a no-brainer at this point.<br><br>(If you don\'t think vegan food is as good as "normal" food, then you\'ve been conditioned to think so. In my experience, you end up eating better tasting food that\'s also better for you.)<br><br>Quoting: <a href="https://x.com/leahstokes/status/1666666305586360320" rel="noopener noreferrer" target="_blank">@leahstokes</a> <span class="status">1666666305586360320</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1666725956445417473',
    created: 1686213408000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> I think it\'s also worth pointing out that the last defender to check him was none other than Gavin Adler. That\'s not just any defender. That was some legit dodging.<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1665049381211627521" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1665049381211627521</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1666725384124264449',
    created: 1686213272000,
    type: 'reply',
    text: '@RedwoodsRing I don\'t know what\'s more sick, that effort or wearing that uniform while doing it. I\'ll take both, please!<br><br>In reply to: @RedwoodsRing <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1666724615769710592',
    created: 1686213089000,
    type: 'quote',
    text: 'This is absolutely sick. When people tell you not to give up, this is what they mean. Stick with it, literally.<br><br>Quoting: <a href="https://x.com/PremierLacrosse/status/1665049381211627521" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1665049381211627521</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1666589647802355714',
    created: 1686180910000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/TheCQC" rel="noopener noreferrer" target="_blank">@TheCQC</a> I get what you\'re saying, and you are indeed right. At the same time, a majority of the people I know across the US vote for terrible candidates that support this capitalistic system (or just don\'t care how they vote). So there is blame there.<br><br>In reply to: <a href="https://x.com/TheCQC/status/1666572707851378690" rel="noopener noreferrer" target="_blank">@TheCQC</a> <span class="status">1666572707851378690</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1666564148287901696',
    created: 1686174830000,
    type: 'quote',
    text: 'Just remember that this is a choice. And this is the choice we\'ve all made so far.<br><br>Quoting: <a href="https://x.com/WorkingFamilies/status/1666540432119963649" rel="noopener noreferrer" target="_blank">@WorkingFamilies</a> <span class="status">1666540432119963649</span>',
    likes: 15,
    retweets: 4
  },
  {
    id: '1664886065642061825',
    created: 1685774744000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/WMajorSeriesLax" rel="noopener noreferrer" target="_blank">@WMajorSeriesLax</a> Are any of the games during the season being broadcast? I\'d be interested in watching (but I\'m far away).<br><br>In reply to: <a href="https://x.com/WMajorSeriesLax/status/1664745699366371328" rel="noopener noreferrer" target="_blank">@WMajorSeriesLax</a> <span class="status">1664745699366371328</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1664754703131983873',
    created: 1685743425000,
    type: 'post',
    text: 'One of the things I love about Ruby is that it adds common enumerable methods to just about every kind of collection. In JavaScript, there\'s no Set#join. But there is in Ruby. Just makes life simpler.',
    likes: 6,
    retweets: 0
  },
  {
    id: '1664723485401260035',
    created: 1685735982000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> Atlas in Denver. How could <a class="mention" href="https://x.com/trevorbaptiste9" rel="noopener noreferrer" target="_blank">@trevorbaptiste9</a> not call this home? The stands are filled with his jersey even at Pios home games. Let\'s go!<br><br>(My second choice would be Redwoods, but that\'s just a personal preference. I could see the Dogs here too).<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1661819623010885634" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1661819623010885634</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1664361449861554176',
    created: 1685649666000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> I know it\'s tough, esp when it involves the US gov\'t. It\'s just drives me mad to think Canadians have trouble at the border. It\'s possible nothing can be done other than to just be patient. Damn politics. That aside, looking forward to an awesome summer! Let\'s go!<br><br>In reply to: <a href="#1664209602651770880">1664209602651770880</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1664209602651770880',
    created: 1685613463000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> The PLL really needs to get in front of this in the future, but they probably already realize that.<br><br>In reply to: <a href="https://x.com/danarestia/status/1664005658486755328" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1664005658486755328</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1664200187118751744',
    created: 1685611218000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/IoloWilliams2" rel="noopener noreferrer" target="_blank">@IoloWilliams2</a> <a class="mention" href="https://x.com/HannahStitfall" rel="noopener noreferrer" target="_blank">@HannahStitfall</a> <a class="mention" href="https://x.com/BBCSpringwatch" rel="noopener noreferrer" target="_blank">@BBCSpringwatch</a> <a class="mention" href="https://x.com/NHS" rel="noopener noreferrer" target="_blank">@NHS</a> <a class="mention" href="https://x.com/ChrisGPackham" rel="noopener noreferrer" target="_blank">@ChrisGPackham</a> <a class="mention" href="https://x.com/michaelastracha" rel="noopener noreferrer" target="_blank">@michaelastracha</a> <a class="mention" href="https://x.com/MeganMcCubbin" rel="noopener noreferrer" target="_blank">@MeganMcCubbin</a> <a class="mention" href="https://x.com/SwanseabayNHS" rel="noopener noreferrer" target="_blank">@SwanseabayNHS</a> Get well soon and know that you will surely be missed.<br><br>In reply to: <a href="https://x.com/IoloWilliams2/status/1662045024786210821" rel="noopener noreferrer" target="_blank">@IoloWilliams2</a> <span class="status">1662045024786210821</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1664197349097504770',
    created: 1685610541000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bjschrijver" rel="noopener noreferrer" target="_blank">@bjschrijver</a> 🤣<br><br>In reply to: <a href="https://x.com/bjschrijver/status/1664169547908120577" rel="noopener noreferrer" target="_blank">@bjschrijver</a> <span class="status">1664169547908120577</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1664053256132399104',
    created: 1685576187000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLRedwoods" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/RobPannell3" rel="noopener noreferrer" target="_blank">@RobPannell3</a> <a class="mention" href="https://x.com/MylesJones_15" rel="noopener noreferrer" target="_blank">@MylesJones_15</a> <a class="mention" href="https://x.com/ChampionUSA" rel="noopener noreferrer" target="_blank">@ChampionUSA</a> I love the acceleration stripes. That definitely says to me "roll woods", or more like "nitro woods"!<br><br>In reply to: <a href="https://x.com/PLLRedwoods/status/1663923948084506628" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> <span class="status">1663923948084506628</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1664029013868949504',
    created: 1685570407000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/ESPNPlus" rel="noopener noreferrer" target="_blank">@ESPNPlus</a> Is the men\'s World Lacrosse Championship going to be available on ESPN+? Asking as a subscriber.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1663981459684864000',
    created: 1685559069000,
    type: 'reply',
    text: 'Oh, and congratulations to the <a class="mention" href="https://x.com/zulip" rel="noopener noreferrer" target="_blank">@zulip</a> leadership team and the entire community that contributes to and supports it. 🎉<br><br>In reply to: <a href="#1663981093215948801">1663981093215948801</a>',
    likes: 7,
    retweets: 1
  },
  {
    id: '1663981093215948801',
    created: 1685558982000,
    type: 'quote',
    text: 'Zulip continues to get better and better (which I didn\'t even think was possible, but so it is). Being a user of the hosted version, we get to be the test subject for features as they get developed. Let me say from experience, there are some nice enhancements in this bundle.<br><br>Quoting: <a href="https://x.com/zulip/status/1663960819602976769" rel="noopener noreferrer" target="_blank">@zulip</a> <span class="status">1663960819602976769</span>',
    likes: 13,
    retweets: 4
  },
  {
    id: '1663818518591320064',
    created: 1685520221000,
    type: 'post',
    text: 'Just had some delicious eats from the Grateful Planet Foods truck for dinner. If you\'re looking for a plant-based meal to pair with some brews in the Denver area, be sure to look up where the truck will stop next! I know I will. <a href="https://www.gratefulplanetfoods.com/" rel="noopener noreferrer" target="_blank">www.gratefulplanetfoods.com/</a>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1663672218688319488',
    created: 1685485341000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> Wow! Congrats to Dad, Mom, and daughter! I wish you all a delightful day!<br><br>In reply to: <a href="https://x.com/starbuxman/status/1663568465125593092" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">1663568465125593092</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1663430712182341632',
    created: 1685427761000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/NDlacrosse" rel="noopener noreferrer" target="_blank">@NDlacrosse</a> It\'s hard to know who to award the Tewaaraton Trophy to this year. If I were on the committee, I\'d vote to give it to both Pat &amp; Chris. For one player to step into the other\'s shoes, yet still have them both on the field as a threat, is rare. They\'re a double-headed 🥍 beast.<br><br>In reply to: <a href="https://x.com/NDlacrosse/status/1663322396575580160" rel="noopener noreferrer" target="_blank">@NDlacrosse</a> <span class="status">1663322396575580160</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1663314177279266817',
    created: 1685399977000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> We absolutely need bagpipes at the start of every Woods game this summer. It\'s non-negotiable.<br><br>In reply to: <a href="https://x.com/danarestia/status/1663297053450010627" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1663297053450010627</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1663312830974803968',
    created: 1685399656000,
    type: 'post',
    text: 'Colorado live in Buffalo\'s nightmares rent free. How Colorado even got to the finals is inexplicable. But here we are. Mammoth one game away from a repeat. Let\'s go! #nll',
    likes: 2,
    retweets: 0,
    tags: ['nll']
  },
  {
    id: '1663311969330536450',
    created: 1685399450000,
    type: 'post',
    text: 'Game 3. Bring it on. <a class="mention" href="https://x.com/MammothLax" rel="noopener noreferrer" target="_blank">@MammothLax</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1663303182561460225',
    created: 1685397356000,
    type: 'post',
    text: 'I don\'t think I\'ve witnessed a grittier performance from an athlete, not just in lacrosse, than from Chris Kavanagh. Dam. Five stitches to the head and what looked like a concussion, and he still stayed in there to win the national championship. Wow. Insane. 🥍',
    likes: 0,
    retweets: 0
  },
  {
    id: '1662918715023962112',
    created: 1685305691000,
    type: 'post',
    text: 'In addition to strong, em, and mark in HTML, it would be nice to have a formatting tag for contrast. This would put the enclosed text in a contrasting color as decided by the theme. Sometimes boldface is not enough to see the emphasis. Just an idea.',
    likes: 3,
    retweets: 1
  },
  {
    id: '1662633078714675201',
    created: 1685237590000,
    type: 'post',
    text: 'Sorry, but Duke did NOT win that game. Lack of video review of a game-deciding goal in 2023 is unacceptable. Are we living in the stone ages? Sometimes it feels like it. <a class="mention" href="https://x.com/NCAA" rel="noopener noreferrer" target="_blank">@NCAA</a> <a class="mention" href="https://x.com/espn" rel="noopener noreferrer" target="_blank">@espn</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1662523098015608832',
    created: 1685211369000,
    type: 'post',
    text: 'Anyone who\'s well-versed in git, is this the correct logic to begin implementing git gc? <a href="https://github.com/isomorphic-git/isomorphic-git/issues/1117#issuecomment-1565474155" rel="noopener noreferrer" target="_blank">github.com/isomorphic-git/isomorphic-git/issues/1117#issuecomment-1565474155</a>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1662318744239239168',
    created: 1685162647000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DU_WLAX" rel="noopener noreferrer" target="_blank">@DU_WLAX</a> That was incredible. Sportscenter top 10 for sure. We were howling!<br><br>In reply to: <a href="https://x.com/DU_WLAX/status/1662193557602697216" rel="noopener noreferrer" target="_blank">@DU_WLAX</a> <span class="status">1662193557602697216</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1662201186936823808',
    created: 1685134619000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joaompinto4" rel="noopener noreferrer" target="_blank">@joaompinto4</a> I agree that this is a strong reason why users should look elsewhere. But Red Hat and its contributors come into my project and ask me to make changes to it to accommodate these outdated policies, so it affects me directly regardless of what users decide.<br><br>In reply to: <a href="https://x.com/joaompinto4/status/1662147951748169730" rel="noopener noreferrer" target="_blank">@joaompinto4</a> <span class="status">1662147951748169730</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1662200855947522048',
    created: 1685134540000,
    type: 'post',
    text: 'I\'ve been a subscriber of <a class="mention" href="https://x.com/netflix" rel="noopener noreferrer" target="_blank">@netflix</a> since they were a DVD rental company before Y2K. It infuriates me that they\'re imposing a policy that puts limits on how I can use my subscription within my family. This is absolute BS. In the modern era, success == screwing over your customers.',
    likes: 6,
    retweets: 1
  },
  {
    id: '1661899714961489920',
    created: 1685062743000,
    type: 'post',
    text: 'Please respect these dates: <a href="https://www.ruby-lang.org/en/downloads/branches/" rel="noopener noreferrer" target="_blank">www.ruby-lang.org/en/downloads/branches/</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1661899376414048257',
    created: 1685062662000,
    type: 'post',
    text: 'I don\'t understand why Red Hat thinks it can hold back the version of a language runtime like Ruby on a "stable" operating system like RHEL/CentOS. Ruby 2.5 has been EOL for over 2 years now. Stop expecting maintainers to cater to your antiquated policies &amp; upgrade your runtimes!',
    likes: 8,
    retweets: 1
  },
  {
    id: '1661863316581670914',
    created: 1685054065000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alexsotob" rel="noopener noreferrer" target="_blank">@alexsotob</a> I\'ll be there!!<br><br>In reply to: <a href="https://x.com/alexsotob/status/1661860631824281603" rel="noopener noreferrer" target="_blank">@alexsotob</a> <span class="status">1661860631824281603</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1661672787369689089',
    created: 1685008639000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jnorthr" rel="noopener noreferrer" target="_blank">@jnorthr</a> Please start with the demo found here: <a href="https://docs.antora.org/antora/latest/install-and-run-quickstart/#create-a-playbook" rel="noopener noreferrer" target="_blank">docs.antora.org/antora/latest/install-and-run-quickstart/#create-a-playbook</a><br><br>If you have questions about Antora, please ask them in the project chat at <a href="https://chat.antora.org" rel="noopener noreferrer" target="_blank">chat.antora.org</a>.<br><br>In reply to: <a href="https://x.com/jnorthr/status/1661648979011010561" rel="noopener noreferrer" target="_blank">@jnorthr</a> <span class="status">1661648979011010561</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1661559336332840961',
    created: 1684981590000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/paulajean2020" rel="noopener noreferrer" target="_blank">@paulajean2020</a> It\'s sickening. There\'s just no other word for it.<br><br>In reply to: <a href="https://x.com/paulajean2020/status/1661554690276970497" rel="noopener noreferrer" target="_blank">@paulajean2020</a> <span class="status">1661554690276970497</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1661509798108753920',
    created: 1684969779000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/PaulRabil" rel="noopener noreferrer" target="_blank">@PaulRabil</a> <a class="mention" href="https://x.com/ChampionUSA" rel="noopener noreferrer" target="_blank">@ChampionUSA</a> One thing I\'d request is to make sure the names (and numbers) are readable from a distance. We struggled to read the names on jerseys for Chrome in the Championship Series. That font was too gothic. I think it\'s important for both players and fans that the names be legible.<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1661474910223609856" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1661474910223609856</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1661101151037841409',
    created: 1684872350000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ESPNStatsInfo" rel="noopener noreferrer" target="_blank">@ESPNStatsInfo</a> Let\'s not forget about <a class="mention" href="https://x.com/DU_WLAX" rel="noopener noreferrer" target="_blank">@DU_WLAX</a>, who are in the final four (first the first time) this weekend and have a shot at a national title too.<br><br>In reply to: <a href="https://x.com/ESPNStatsInfo/status/1661039292779737094" rel="noopener noreferrer" target="_blank">@ESPNStatsInfo</a> <span class="status">1661039292779737094</span>',
    likes: 5,
    retweets: 1
  },
  {
    id: '1660222484992643073',
    created: 1684662860000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Vitamix" rel="noopener noreferrer" target="_blank">@Vitamix</a> <a class="mention" href="https://x.com/AnthonyJTropea" rel="noopener noreferrer" target="_blank">@AnthonyJTropea</a> Try not compatible with all Android devices. It\'s so out of date, I wouldn\'t dare install it anyway. I think Vitamix really needs to take this situation seriously because it\'s not a good look for such a reputable brand.<br><br>In reply to: <a href="https://x.com/Vitamix/status/1632114658474700800" rel="noopener noreferrer" target="_blank">@Vitamix</a> <span class="status">1632114658474700800</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1659736108328951808',
    created: 1684546899000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> <a class="mention" href="https://x.com/spring_io" rel="noopener noreferrer" target="_blank">@spring_io</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> And soon, they\'re going to be like "you\'re the Austen guy!" We\'re going to help get you there. Game on ;)<br><br>In reply to: <a href="https://x.com/abelsromero/status/1659675648091381760" rel="noopener noreferrer" target="_blank">@abelsromero</a> <span class="status">1659675648091381760</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1659114745998327808',
    created: 1684398754000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/steveparks" rel="noopener noreferrer" target="_blank">@steveparks</a> Sadly.<br><br>In reply to: <a href="https://x.com/steveparks/status/1659112778257379329" rel="noopener noreferrer" target="_blank">@steveparks</a> <span class="status">1659112778257379329</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1659114632248688642',
    created: 1684398727000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lasombra_br" rel="noopener noreferrer" target="_blank">@lasombra_br</a> <a class="mention" href="https://x.com/ublockorigin" rel="noopener noreferrer" target="_blank">@ublockorigin</a> I already had an ad-blocker, but it wasn\'t doing the job of managing cookie consents (to deny all). But it seems like Ghostery does both. I\'m going to give it a spin for a couple of days and see how it performs.<br><br>In reply to: <a href="https://x.com/lasombra_br/status/1659109080105906176" rel="noopener noreferrer" target="_blank">@lasombra_br</a> <span class="status">1659109080105906176</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1659114363570077697',
    created: 1684398663000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> I tried that one, but had some trouble getting it working. I then discovered Ghostery, which seems to do the job perfectly.<br><br>In reply to: <a href="https://x.com/abelsromero/status/1659114153531908096" rel="noopener noreferrer" target="_blank">@abelsromero</a> <span class="status">1659114153531908096</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1659109433585041410',
    created: 1684397488000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/steveparks" rel="noopener noreferrer" target="_blank">@steveparks</a> I give up and just never return to that site ;)<br><br>In reply to: <a href="https://x.com/steveparks/status/1659109224427790338" rel="noopener noreferrer" target="_blank">@steveparks</a> <span class="status">1659109224427790338</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1659108834604875779',
    created: 1684397345000,
    type: 'post',
    text: 'Unless I have an account and I\'m trying to do something that needs a profile or cart, I want to deny all cookies forever.',
    likes: 3,
    retweets: 1
  },
  {
    id: '1659108482191065092',
    created: 1684397261000,
    type: 'post',
    text: 'Cookie consent dialogs make me totally hate using the web. I can\'t tell you how many times I\'ve left a site rather than to go through yet another dialog. There HAS to be a way to set this globally in the browser. Maybe there\'s an extension I should be using?',
    likes: 8,
    retweets: 0
  },
  {
    id: '1658340709026648066',
    created: 1684214210000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dberkholz" rel="noopener noreferrer" target="_blank">@dberkholz</a> <a class="mention" href="https://x.com/YouTube" rel="noopener noreferrer" target="_blank">@YouTube</a> It drives me nuts too. I have to aggressively remove items from my watch history to prevent YouTube from going off the rails with its recommendations.<br><br>In reply to: <a href="https://x.com/dberkholz/status/1658339267800711170" rel="noopener noreferrer" target="_blank">@dberkholz</a> <span class="status">1658339267800711170</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1658245050177028096',
    created: 1684191403000,
    type: 'post',
    text: 'I do not understand why the OpenJS Foundation / Node.js project does not consider a git client to be as an important as an HTTP client. A git client should come standard with the runtime.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1658179638328442880',
    created: 1684175807000,
    type: 'post',
    text: 'Lacrosse commenters really need to stop with the "mom goal" trope. Not only is it sexist, it\'s also alienating. Many fans get tricked when the ball hits the backside of the net and makes it move, so I suggest we call it a "fan goal". That\'s inclusive and accurate. #Lacrosse 🥍🥅',
    likes: 2,
    retweets: 0,
    tags: ['lacrosse']
  },
  {
    id: '1658003276758134784',
    created: 1684133760000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RobPannell3" rel="noopener noreferrer" target="_blank">@RobPannell3</a> I 1,000% agree with you that the best quality of CJ Kirst is the way he lifts up his teammates at every opportunity. He\'s both the #1 point getter and the #1 fan of the team. That\'s just so fun to watch. And you really start to notice other teams that don\'t have a CJ.<br><br>In reply to: <a href="https://x.com/RobPannell3/status/1657088259619528704" rel="noopener noreferrer" target="_blank">@RobPannell3</a> <span class="status">1657088259619528704</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1657954288675745792',
    created: 1684122080000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CornellLacrosse" rel="noopener noreferrer" target="_blank">@CornellLacrosse</a> It was nothing short of exciting. Way to fight until the end. There was a lot of #YellCornell going on here. Looking forward to many more such moments at Cornell and beyond! 🥍<br><br>In reply to: <a href="https://x.com/CornellLacrosse/status/1657867743989755906" rel="noopener noreferrer" target="_blank">@CornellLacrosse</a> <span class="status">1657867743989755906</span>',
    likes: 3,
    retweets: 0,
    tags: ['yellcornell']
  },
  {
    id: '1657075887836385280',
    created: 1683912653000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jimisola" rel="noopener noreferrer" target="_blank">@jimisola</a> <a class="mention" href="https://x.com/marcioendo" rel="noopener noreferrer" target="_blank">@marcioendo</a> You are certainly welcome to ask questions about it in the project chat at <a href="https://chat.asciidoc.org" rel="noopener noreferrer" target="_blank">chat.asciidoc.org</a>. I\'m usually very responsive.<br><br>In reply to: <a href="https://x.com/jimisola/status/1656997601072521216" rel="noopener noreferrer" target="_blank">@jimisola</a> <span class="status">1656997601072521216</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1656783767279910912',
    created: 1683843006000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jimisola" rel="noopener noreferrer" target="_blank">@jimisola</a> <a class="mention" href="https://x.com/marcioendo" rel="noopener noreferrer" target="_blank">@marcioendo</a> This is all laid out in the plan for the AsciiDoc Language Specification, so I won\'t restate it here. You can find information in that project and in the AsciiDoc WG.<br><br>In reply to: <a href="https://x.com/jimisola/status/1656627790530113536" rel="noopener noreferrer" target="_blank">@jimisola</a> <span class="status">1656627790530113536</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1656616368173903872',
    created: 1683803095000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/benignbala" rel="noopener noreferrer" target="_blank">@benignbala</a> <a class="mention" href="https://x.com/marcioendo" rel="noopener noreferrer" target="_blank">@marcioendo</a> The spec will be defined using a PEG grammar set. I couldn\'t understand ANTLR, nor do I think it\'s a good fit.<br><br>In reply to: <a href="https://x.com/benignbala/status/1656612930304225281" rel="noopener noreferrer" target="_blank">@benignbala</a> <span class="status">1656612930304225281</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1656615954942656512',
    created: 1683802996000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/benignbala" rel="noopener noreferrer" target="_blank">@benignbala</a> <a class="mention" href="https://x.com/marcioendo" rel="noopener noreferrer" target="_blank">@marcioendo</a> Already in communication ;)<br><br>In reply to: <a href="https://x.com/benignbala/status/1656612755619848192" rel="noopener noreferrer" target="_blank">@benignbala</a> <span class="status">1656612755619848192</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1656609759607525377',
    created: 1683801519000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jnorthr" rel="noopener noreferrer" target="_blank">@jnorthr</a> Compatibility is always paramount to me.<br><br>In reply to: <a href="#1656609569366499329">1656609569366499329</a>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1656609569366499329',
    created: 1683801474000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jnorthr" rel="noopener noreferrer" target="_blank">@jnorthr</a> They\'re largely about how the parsing is approached so it\'s more accurate and robust. It will change some rules, but very likely only to match how you expect it to be parsed. With only a handful of exceptions, mostly refinements, the language will be the same.<br><br>In reply to: <a href="https://x.com/jnorthr/status/1656605557682216960" rel="noopener noreferrer" target="_blank">@jnorthr</a> <span class="status">1656605557682216960</span>',
    likes: 1,
    retweets: 1
  },
  {
    id: '1656188474255618048',
    created: 1683701077000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> Exciting!<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1656105813948211200" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1656105813948211200</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1656105624424554496',
    created: 1683681324000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CornellSports" rel="noopener noreferrer" target="_blank">@CornellSports</a> <a class="mention" href="https://x.com/PLLAtlas" rel="noopener noreferrer" target="_blank">@PLLAtlas</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/CornellLacrosse" rel="noopener noreferrer" target="_blank">@CornellLacrosse</a> <a class="mention" href="https://x.com/IvyLeague" rel="noopener noreferrer" target="_blank">@IvyLeague</a> Congratulations Gavin! Well deserved! I can\'t wait to see you on the field this summer for the Atlas alongside Teater. I think I may have finally decided what my favorite team is! #YellCornell<br><br>In reply to: <a href="https://x.com/CornellSports/status/1656082196069851137" rel="noopener noreferrer" target="_blank">@CornellSports</a> <span class="status">1656082196069851137</span>',
    likes: 2,
    retweets: 0,
    tags: ['yellcornell']
  },
  {
    id: '1656052487143723010',
    created: 1683668655000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcioendo" rel="noopener noreferrer" target="_blank">@marcioendo</a> The issue tracker is really the best stream to follow. The project chat is mostly status updates about what\'s going on in the project repo.<br><br>In reply to: <a href="https://x.com/marcioendo/status/1656051383957610501" rel="noopener noreferrer" target="_blank">@marcioendo</a> <span class="status">1656051383957610501</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1656052201377402881',
    created: 1683668587000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcioendo" rel="noopener noreferrer" target="_blank">@marcioendo</a> Yes, though you can always reference the user docs as a starting point for conversation. That said, we are likely changing the inline parsing model completely (though aiming for a compatible result within reason).<br><br>In reply to: <a href="https://x.com/marcioendo/status/1656051383957610501" rel="noopener noreferrer" target="_blank">@marcioendo</a> <span class="status">1656051383957610501</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1656051775840096258',
    created: 1683668485000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcioendo" rel="noopener noreferrer" target="_blank">@marcioendo</a> Always glad to have you whenever you can join. No pressure. Still plenty to do and to decide.<br><br>In reply to: <a href="https://x.com/marcioendo/status/1656049713118953472" rel="noopener noreferrer" target="_blank">@marcioendo</a> <span class="status">1656049713118953472</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1656040085073305602',
    created: 1683665698000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcioendo" rel="noopener noreferrer" target="_blank">@marcioendo</a> Keep in mind that the authority for what is AsciiDoc is now the asciidoc-lang project. Asciidoctor is the initial contribution, and we are following it as closely as we can, but there are some fundamental differences that have now been agreed on.<br><br>In reply to: <a href="https://x.com/marcioendo/status/1655583151450734592" rel="noopener noreferrer" target="_blank">@marcioendo</a> <span class="status">1655583151450734592</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1656039626904330243',
    created: 1683665589000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcioendo" rel="noopener noreferrer" target="_blank">@marcioendo</a> We\'re still considering whether this should be the case because it makes the line preprocessor a lot harder to implement. I strongly encourage you to follow (and perhaps participate in) the asciidoc-lang project for discussions such as this one.<br><br>In reply to: <a href="https://x.com/marcioendo/status/1656000329606107136" rel="noopener noreferrer" target="_blank">@marcioendo</a> <span class="status">1656000329606107136</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1655853474914144257',
    created: 1683621207000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Don\'t forget the environmental impact of air travel, which is a big part of why I\'m not chasing the circuit rn.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1655575647949914114" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1655575647949914114</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1655853111402168320',
    created: 1683621120000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> Funny how most of the replies miss your point. In person conferences may become less viable, so where\'s the adaptation? Most replies argue why the are better, which we already know. But better doesn\'t mean possible.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1655575647949914114" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1655575647949914114</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1655744075495088128',
    created: 1683595124000,
    type: 'quote',
    text: 'Oh look, more Markdown divergence. This is a key reason why we are working so hard to formalize the AsciiDoc syntax. We can\'t just have reactionary syntax changes every time a conflict emerges.<br><br>Quoting: <a href="https://x.com/GHchangelog/status/1655697002192150531" rel="noopener noreferrer" target="_blank">@GHchangelog</a> <span class="status">1655697002192150531</span>',
    likes: 10,
    retweets: 3
  },
  {
    id: '1654233833682919424',
    created: 1683235054000,
    type: 'post',
    text: '...and yet, I fixed the bug anyway. Even when I\'m upset, that\'s just who I am. While I won\'t tolerate being spoken to like this, I still do what is best for the project and the vast community of users who depend on it.',
    likes: 7,
    retweets: 0
  },
  {
    id: '1654232880879652864',
    created: 1683234827000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/benignbala" rel="noopener noreferrer" target="_blank">@benignbala</a> 💯<br><br>In reply to: <a href="https://x.com/benignbala/status/1654181148128432146" rel="noopener noreferrer" target="_blank">@benignbala</a> <span class="status">1654181148128432146</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1654077118547099648',
    created: 1683197690000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/USMMA_Athletics" rel="noopener noreferrer" target="_blank">@USMMA_Athletics</a> <a class="mention" href="https://x.com/USMMA_WomensLAX" rel="noopener noreferrer" target="_blank">@USMMA_WomensLAX</a> Way to go Kaitlyn! We\'ll be watching you run with your team in the tourney! Let\'s go!<br><br>In reply to: <a href="https://x.com/USMMA_Athletics/status/1653416554548322305" rel="noopener noreferrer" target="_blank">@USMMA_Athletics</a> <span class="status">1653416554548322305</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1653853225605537792',
    created: 1683144310000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_gettalong" rel="noopener noreferrer" target="_blank">@_gettalong</a> I see a case where there\'s a problem and would accept a new issue on the matter. The escape character is being left behind because substitutions are not fully applied to the value of mantitle. We need to go one way or the other (remove the backslash or finish the subs).<br><br>In reply to: <a href="https://x.com/_gettalong/status/1653851580586655746" rel="noopener noreferrer" target="_blank">@_gettalong</a> <span class="status">1653851580586655746</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1653852832330833920',
    created: 1683144216000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> It was the follow-up post that was the problem. I pointed out that frustration should be directed to the project chat and not the issue tracker. The person didn\'t like that, so decided to get back at me by being rude and talking down to me.<br><br>In reply to: <a href="https://x.com/RalfDMueller/status/1653845983661375497" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <span class="status">1653845983661375497</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1653851099831607296',
    created: 1683143803000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> I\'ve also learned that letting stuff like this fly is what degrades the quality of the community. It has to be addressed IMO, and that is one of my responsibilities as the project lead.<br><br>In reply to: <a href="https://x.com/RalfDMueller/status/1653846753911685121" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <span class="status">1653846753911685121</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1653844800100241412',
    created: 1683142301000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> FYI, "sorry" is easily understood in any language and it\'s not there. Instead, the person made a conscious decision to be a smart ass about it.<br><br>In reply to: <a href="#1653844131326857218">1653844131326857218</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1653844131326857218',
    created: 1683142142000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> Of course, why do my feelings matter? It\'s always my fault. I\'m overreacting. Who cares about the maintainer? The maintainer should just suck it up and fix the issues. No, I\'m not playing that way. He made me feel like shit and he knows he did it. It\'s clear as day.<br><br>In reply to: <a href="https://x.com/RalfDMueller/status/1653841861444812803" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <span class="status">1653841861444812803</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1653840206506885120',
    created: 1683141206000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jnorthr" rel="noopener noreferrer" target="_blank">@jnorthr</a> Thanks Jim! You\'ve always been a bright spot in my TL ;)<br><br>In reply to: <a href="https://x.com/jnorthr/status/1653839534373609518" rel="noopener noreferrer" target="_blank">@jnorthr</a> <span class="status">1653839534373609518</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1653838148148035584',
    created: 1683140716000,
    type: 'post',
    text: 'It really does make me not want to work on open source anymore. I don\'t want that to happen, but where do I find the motivation when certain people seemingly go out of their way to combat me in a selfish attempt to get what they want? Some days I really hate this world.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1653835035764731904',
    created: 1683139973000,
    type: 'post',
    text: 'I try not to let it get to me, but shit like this just ruins the start of my day.',
    likes: 3,
    retweets: 1
  },
  {
    id: '1653834588081524737',
    created: 1683139867000,
    type: 'post',
    text: 'I am NOT going to be spoken to this way. This backtalk is unacceptable. If shutting it down means that person isn\'t going to use Asciidoctor anymore, FINE. I don\'t want them around the project. <a href="https://github.com/asciidoctor/asciidoctor/issues/4445#issuecomment-1533152216" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor/issues/4445#issuecomment-1533152216</a>',
    likes: 13,
    retweets: 0
  },
  {
    id: '1652464738675355648',
    created: 1682813269000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/CornellLacrosse" rel="noopener noreferrer" target="_blank">@CornellLacrosse</a> <a class="mention" href="https://x.com/CornellSports" rel="noopener noreferrer" target="_blank">@CornellSports</a> <a class="mention" href="https://x.com/IvyLeague" rel="noopener noreferrer" target="_blank">@IvyLeague</a> What a way to honor Richie Moran\'s memory and legacy!! That game was unreal. And there was nothing better than that ground ball by Gavin Adler in OT. I\'ve never been so excited by a ground ball in my life. There was definitely some #YellCornell going on in my apartment.<br><br>In reply to: <a href="https://x.com/CornellLacrosse/status/1652413837029564419" rel="noopener noreferrer" target="_blank">@CornellLacrosse</a> <span class="status">1652413837029564419</span>',
    likes: 8,
    retweets: 1,
    tags: ['yellcornell']
  },
  {
    id: '1650439251992150016',
    created: 1682330356000,
    type: 'post',
    text: 'I think what I love so much about Zulip is that I feel like I can talk to the community of my open source projects whenever and without barriers. I don\'t know why other means of communication didn\'t allow me to feel that way, they just didn\'t. There\'s a certain je ne sais quoi.',
    likes: 3,
    retweets: 1
  },
  {
    id: '1649674530007416833',
    created: 1682148032000,
    type: 'reply',
    text: '@momohoudai Would kramdown-asciidoc (kramdoc) or downdoc help?<br><br><a href="https://github.com/asciidoctor/kramdown-asciidoc" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/kramdown-asciidoc</a><br><a href="https://github.com/opendevise/downdoc/" rel="noopener noreferrer" target="_blank">github.com/opendevise/downdoc/</a><br><br>In reply to: <a href="https://x.com/momolabo7/status/1649672210033016834" rel="noopener noreferrer" target="_blank">@momolabo7</a> <span class="status">1649672210033016834</span>',
    likes: 4,
    retweets: 1
  },
  {
    id: '1649489936394108928',
    created: 1682104021000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RalfDMueller" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <a class="mention" href="https://x.com/annegentle" rel="noopener noreferrer" target="_blank">@annegentle</a> <a class="mention" href="https://x.com/docToolchain" rel="noopener noreferrer" target="_blank">@docToolchain</a> There are more implementations of AsciiDoc coming, but only one specification. We\'re working extremely hard to make sure that the implementations adhere to a single interpretation of AsciiDoc. No flavors. That mission is stated up front in the working group charter.<br><br>In reply to: <a href="https://x.com/RalfDMueller/status/1649436074672791555" rel="noopener noreferrer" target="_blank">@RalfDMueller</a> <span class="status">1649436074672791555</span>',
    likes: 6,
    retweets: 2
  },
  {
    id: '1649328784280256515',
    created: 1682065599000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sam_brannen" rel="noopener noreferrer" target="_blank">@sam_brannen</a> <a class="mention" href="https://x.com/nipafx" rel="noopener noreferrer" target="_blank">@nipafx</a> 💯<br><br>In reply to: <a href="https://x.com/sam_brannen/status/1649326640219004929" rel="noopener noreferrer" target="_blank">@sam_brannen</a> <span class="status">1649326640219004929</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1648266006467592192',
    created: 1681812213000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/oleg_nenashev" rel="noopener noreferrer" target="_blank">@oleg_nenashev</a> <a class="mention" href="https://x.com/wiremockorg" rel="noopener noreferrer" target="_blank">@wiremockorg</a> <a class="mention" href="https://x.com/docusaurus" rel="noopener noreferrer" target="_blank">@docusaurus</a> <a class="mention" href="https://x.com/GoHugoIO" rel="noopener noreferrer" target="_blank">@GoHugoIO</a> <a class="mention" href="https://x.com/antoraproject" rel="noopener noreferrer" target="_blank">@antoraproject</a> One thing I can assure you is that you will find no better help anywhere else than in the Antora community. Ask anyone around ;)<br><br>In reply to: <a href="https://x.com/oleg_nenashev/status/1648264366603530241" rel="noopener noreferrer" target="_blank">@oleg_nenashev</a> <span class="status">1648264366603530241</span>',
    likes: 6,
    retweets: 1
  },
  {
    id: '1648070939631042562',
    created: 1681765706000,
    type: 'post',
    text: 'I\'m rather disappointed that the Eclipse Foundation choose Matrix over Zulip for chat. At the very least, I wish they would have offered both options. To their credit, they have been supportive of the AsciiDoc WG and project\'s decision to use Zulip instead.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1648061696609771520',
    created: 1681763502000,
    type: 'quote',
    text: 'Even works for AsciiDoc files if you install the extension from the marketplace.<br><br>Quoting: <a href="https://x.com/shanselman/status/1647852460252483584" rel="noopener noreferrer" target="_blank">@shanselman</a> <span class="status">1647852460252483584</span>',
    photos: ['<div class="item"><img class="photo" src="media/1648061696609771520.jpg"></div>'],
    likes: 6,
    retweets: 0
  },
  {
    id: '1647845394339217408',
    created: 1681711932000,
    type: 'post',
    text: 'I\'ve entertained out of town guest twice this month. Dare I say it\'s nice to see people in person again (still in limited capacity) for the first time in years? My nerves were still a bit on edge going in, but like riding a bike, it felt totally natural once we got going. 🚵',
    likes: 2,
    retweets: 0
  },
  {
    id: '1647693058241359872',
    created: 1681675612000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/dailyburn" rel="noopener noreferrer" target="_blank">@dailyburn</a> Awesome to see Erika back on the schedule! She helped me wake up some sleeping muscles. I hope we get to see her again soon!<br><br>In reply to: <a href="https://x.com/dailyburn/status/1645139520021725184" rel="noopener noreferrer" target="_blank">@dailyburn</a> <span class="status">1645139520021725184</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1646275532882083840',
    created: 1681337648000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/raclepoulpe" rel="noopener noreferrer" target="_blank">@raclepoulpe</a> <a class="mention" href="https://x.com/CommitStrip_fr" rel="noopener noreferrer" target="_blank">@CommitStrip_fr</a> <a class="mention" href="https://x.com/hsablonniere" rel="noopener noreferrer" target="_blank">@hsablonniere</a> Salut <a class="mention" href="https://x.com/hsablonniere" rel="noopener noreferrer" target="_blank">@hsablonniere</a> !<br><br>In reply to: <a href="https://x.com/raclepoulpe/status/1646070101899190275" rel="noopener noreferrer" target="_blank">@raclepoulpe</a> <span class="status">1646070101899190275</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1646008848229232640',
    created: 1681274065000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/DU_MLAX" rel="noopener noreferrer" target="_blank">@DU_MLAX</a> Congrats on the highest position in the polls this season. Well deserved! Keep locking it down on defense!<br><br>In reply to: <a href="https://x.com/DU_MLAX/status/1645458872419885057" rel="noopener noreferrer" target="_blank">@DU_MLAX</a> <span class="status">1645458872419885057</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1646005018510917632',
    created: 1681273152000,
    type: 'post',
    text: 'To those participating in #DevoxxFR, have fun! I wish I was there with you!',
    likes: 3,
    retweets: 0,
    tags: ['devoxxfr']
  },
  {
    id: '1645975280555794432',
    created: 1681266062000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/paulcarcaterra" rel="noopener noreferrer" target="_blank">@paulcarcaterra</a> <a class="mention" href="https://x.com/ESPNPR" rel="noopener noreferrer" target="_blank">@ESPNPR</a> Sorry, but Handley is overrated. Are you even watching the NCAA season?<br><br>In reply to: <a href="https://x.com/PremierLacrosse/status/1645843038890254337" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <span class="status">1645843038890254337</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1645974954104717313',
    created: 1681265984000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLRedwoods" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/rsgarnz50" rel="noopener noreferrer" target="_blank">@rsgarnz50</a> Saw it live. Still can\'t believe it.<br><br>In reply to: <a href="https://x.com/PLLRedwoods/status/1645892324625448961" rel="noopener noreferrer" target="_blank">@PLLRedwoods</a> <span class="status">1645892324625448961</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1645549109137539072',
    created: 1681164455000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bmarwell" rel="noopener noreferrer" target="_blank">@bmarwell</a> Stay tuned for news. We need to get the TCK sorted out before development can reasonably start, but we are only a few weeks away from that milestone at most.<br><br>In reply to: <a href="https://x.com/bmarwell/status/1645546657650741249" rel="noopener noreferrer" target="_blank">@bmarwell</a> <span class="status">1645546657650741249</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1645542930067902466',
    created: 1681162981000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bmarwell" rel="noopener noreferrer" target="_blank">@bmarwell</a> <a class="mention" href="https://x.com/marcioendo" rel="noopener noreferrer" target="_blank">@marcioendo</a> The project that\'s poised to replace (or at least provide an alternative to) Asciidoctor on Java is Eclipse Austen. It will be one of the first compliant AsciiDoc processors based on the specification currently in progress. We anticipate development on Austen to start very soon.<br><br>In reply to: <a href="https://x.com/bmarwell/status/1645449851914657793" rel="noopener noreferrer" target="_blank">@bmarwell</a> <span class="status">1645449851914657793</span>',
    likes: 3,
    retweets: 1
  },
  {
    id: '1645542135444418562',
    created: 1681162792000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marekgoldmann" rel="noopener noreferrer" target="_blank">@marekgoldmann</a> I\'m feeling the same way.<br><br>In reply to: <a href="https://x.com/marekgoldmann/status/1645531732018597893" rel="noopener noreferrer" target="_blank">@marekgoldmann</a> <span class="status">1645531732018597893</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1645247111926251520',
    created: 1681092453000,
    type: 'post',
    text: 'Of course, I\'ll still be considered the difficult one for saying something...because clearly other people know how to run my project better than me. Funny how some people expect the rules to be different just for them.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1645197084189134848',
    created: 1681080525000,
    type: 'post',
    text: 'I do, however, welcome well-reasoned and well-written feedback. I think being able to understand the difference between that and a selfish plea is something that\'s reasonable to expect.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1645192520127901697',
    created: 1681079437000,
    type: 'post',
    text: 'If I see a dissertation posted to an issue tracker for one of my projects, that\'s almost certainly triggers an instant close. It\'s out of line and, frankly, not acceptable behavior either.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1645191108727812096',
    created: 1681079101000,
    type: 'post',
    text: 'If you want a change, you must take the time to describe the change and justify it with reasonable scope and robust details. No dissertations! Even better is if you can propose how to make the change (though it\'s understandable that not everyone can do that).',
    likes: 0,
    retweets: 0
  },
  {
    id: '1645191106785849345',
    created: 1681079100000,
    type: 'post',
    text: 'I really think this should go without saying, but some people just have to be reminded. The issue tracker of an open source project is not your personal support portal. It\'s not the job of the project to make sure you have covered all your requirements. It just isn\'t.',
    likes: 21,
    retweets: 4
  },
  {
    id: '1644795204967497729',
    created: 1680984710000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <a class="mention" href="https://x.com/united" rel="noopener noreferrer" target="_blank">@united</a> What?!? I can\'t even comprehend that behavior. I\'m so sorry to hear you were treated that way. Inexcusable.<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1644792573817507843" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1644792573817507843</span>',
    likes: 354,
    retweets: 3
  },
  {
    id: '1644297708973461504',
    created: 1680866098000,
    type: 'post',
    text: 'I should add that you never know how much a visit is going to cost until well after it took place. It\'s not like I had any idea going in what the price would be. Compare that to literally any other expenditure of money.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1644289499776839681',
    created: 1680864140000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> Next time, I\'m seriously going to consider driving to Canada.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1644289315457970177" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1644289315457970177</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1644289138332680193',
    created: 1680864054000,
    type: 'post',
    text: 'The message this sends really is: don\'t get sick and don\'t get injured or solve problems yourself (which I try to do as much as possible).',
    likes: 4,
    retweets: 0
  },
  {
    id: '1644286890621943808',
    created: 1680863518000,
    type: 'post',
    text: 'I had rotator cuff pain/stiffness on one side of my body. I took 3 visits to physical therapy to debug/correct it. Each visit cost me $200 out of pocket. I have insurance. That\'s $600 to be told what exercises I had to do to fix my problem. Healthcare in the US is a joke.',
    likes: 7,
    retweets: 0
  },
  {
    id: '1644078618157678592',
    created: 1680813862000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/liran_tal" rel="noopener noreferrer" target="_blank">@liran_tal</a> Thanks for the kind words, Liran! And thanks for participating in Asciidoctor. You\'re contributions are already helping others along on their journey!<br><br>In reply to: <a href="https://x.com/liran_tal/status/1644002544681664512" rel="noopener noreferrer" target="_blank">@liran_tal</a> <span class="status">1644002544681664512</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1643925488094826497',
    created: 1680777353000,
    type: 'post',
    text: 'He truly was a great friend of Java and OSS, a thought leader, famous (at least to us Java folks), and someone without whom this world just won\'t be the same. I\'m sad I\'ll never see him again, and I\'m sad he won\'t see the wonderful messages that have been posted in his memory. 😭',
    likes: 3,
    retweets: 0
  },
  {
    id: '1643925486551339009',
    created: 1680777353000,
    type: 'post',
    text: 'Although our work intersected many times (particularly JSR-330🤝JSR-299), and I often found myself merely feet away from him at events, I only had a chance to hang out and converse with him a few times. I remember him being relaxed, humble, and joyful. Someone you\'d want to know.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1643925484932321288',
    created: 1680777352000,
    type: 'post',
    text: 'I\'m having difficulty making sense of the unfair and untimely death of <a class="mention" href="https://x.com/crazybob" rel="noopener noreferrer" target="_blank">@crazybob</a>. At first I thought it was fake, and now I\'m only left wishing that were true. It just feels so surreal. I\'ll always think of him whenever I use @ Inject.',
    likes: 5,
    retweets: 1
  },
  {
    id: '1643177635953119237',
    created: 1680599051000,
    type: 'post',
    text: 'I\'d really like to see search engines start indexing public streams in Zulip. Is there a reason this isn\'t happening yet? I don\'t really see how this is any different from indexing newsgroups way back in the day (and later Google Groups).',
    likes: 1,
    retweets: 1
  },
  {
    id: '1641646551066877958',
    created: 1680234012000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/sethgoings" rel="noopener noreferrer" target="_blank">@sethgoings</a> Indeed. But you can ignore the hype train.<br><br>In reply to: <a href="https://x.com/sethgoings/status/1641625421186842624" rel="noopener noreferrer" target="_blank">@sethgoings</a> <span class="status">1641625421186842624</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1641645989978152960',
    created: 1680233878000,
    type: 'post',
    text: 'I should add it\'s complicated for a language that already has inconsistent rules, multiple parsing phases, and expectations to have semantic escaping in certain cases.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1641577642859061253',
    created: 1680217583000,
    type: 'post',
    text: 'Defining rules for backslash escaping is more complicated than it seems.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1641200490422870016',
    created: 1680127663000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/abelsromero" rel="noopener noreferrer" target="_blank">@abelsromero</a> <a class="mention" href="https://x.com/wasm_io" rel="noopener noreferrer" target="_blank">@wasm_io</a> <a class="mention" href="https://x.com/MadridJUG" rel="noopener noreferrer" target="_blank">@MadridJUG</a> <a class="mention" href="https://x.com/MadridGUG" rel="noopener noreferrer" target="_blank">@MadridGUG</a> <a class="mention" href="https://x.com/spring_io" rel="noopener noreferrer" target="_blank">@spring_io</a> You have been crushing it! Way to go! 🏆<br><br>In reply to: <a href="https://x.com/abelsromero/status/1641158285498695684" rel="noopener noreferrer" target="_blank">@abelsromero</a> <span class="status">1641158285498695684</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1639739834947502086',
    created: 1679779416000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/apr" rel="noopener noreferrer" target="_blank">@apr</a> <a class="mention" href="https://x.com/Docker" rel="noopener noreferrer" target="_blank">@Docker</a> <a class="mention" href="https://x.com/BriceDutheil" rel="noopener noreferrer" target="_blank">@BriceDutheil</a> Rightfully so.<br><br>In reply to: <a href="https://x.com/apr/status/1639698414668480513" rel="noopener noreferrer" target="_blank">@apr</a> <span class="status">1639698414668480513</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1639392637819183104',
    created: 1679696637000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Docker" rel="noopener noreferrer" target="_blank">@Docker</a> <a class="mention" href="https://x.com/BriceDutheil" rel="noopener noreferrer" target="_blank">@BriceDutheil</a> Thank you for listening to the concerns of the open source community.<br><br>In reply to: <a href="https://x.com/Docker/status/1639379940796936192" rel="noopener noreferrer" target="_blank">@Docker</a> <span class="status">1639379940796936192</span>',
    likes: 6,
    retweets: 0
  },
  {
    id: '1638502568606449665',
    created: 1679484428000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rmannibucau" rel="noopener noreferrer" target="_blank">@rmannibucau</a> <a class="mention" href="https://x.com/myfear" rel="noopener noreferrer" target="_blank">@myfear</a> Trust me, it\'s different because of the pandemic. I\'m seeing it everywhere. But it\'s probably also true that the project is more mainstream. So both things can be true at the same time.<br><br>In reply to: <a href="https://x.com/rmannibucau/status/1638496670752165888" rel="noopener noreferrer" target="_blank">@rmannibucau</a> <span class="status">1638496670752165888</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1638489128038965248',
    created: 1679481224000,
    type: 'post',
    text: 'This is very hand-wavy, but I have this feeling that ever since the pandemic started, OSS has become more "do this for me" rather than "please help me through this". I know these are trying times, but it\'s no excuse for not lifting from your end.',
    likes: 10,
    retweets: 1
  },
  {
    id: '1638342138017771523',
    created: 1679446179000,
    type: 'post',
    text: 'At Grandma\'s House (no, not my grandma\'s house, just the Grandma\'s House). I mean....',
    photos: ['<div class="item"><img class="photo" src="media/1638342138017771523.jpg"></div>', '<div class="item"><img class="photo" src="media/1638342138017771523-2.jpg"></div>'],
    likes: 2,
    retweets: 0
  },
  {
    id: '1638331884156628994',
    created: 1679443734000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/eddiejaoude" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> As a maintainer, I can tell you I much prefer the issue to be created first (or shortly after). I don\'t like out of context pull requests, unless they are documentation fixes. The reason is that software changes almost always require design, and thus discussion.<br><br>In reply to: <a href="https://x.com/eddiejaoude/status/1638142384620879872" rel="noopener noreferrer" target="_blank">@eddiejaoude</a> <span class="status">1638142384620879872</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1638330768010403840',
    created: 1679443468000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/gunnarmorling" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> Me to myself, x100<br><br>In reply to: <a href="https://x.com/gunnarmorling/status/1638090382926807042" rel="noopener noreferrer" target="_blank">@gunnarmorling</a> <span class="status">1638090382926807042</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1638073280501530626',
    created: 1679382078000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> ...and apparently with Big Bird too. Come to think of it, I vaguely remember once having a Big Bird themed cake.<br><br>In reply to: <a href="#1638028987699195904">1638028987699195904</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1638029735732326400',
    created: 1679371696000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/bobbytank42" rel="noopener noreferrer" target="_blank">@bobbytank42</a> Thanks Robert! I\'m raising my glass to you right now. Thanks for being a part of a great community year in and year out. That\'s a gift I\'ll always treasure!<br><br>In reply to: <a href="https://x.com/bobbytank42/status/1637931982855483393" rel="noopener noreferrer" target="_blank">@bobbytank42</a> <span class="status">1637931982855483393</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1638028987699195904',
    created: 1679371518000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> Wow, I share a birthday with your daughter. I feel honored!<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1637906254264963076" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1637906254264963076</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1637949230575931392',
    created: 1679352502000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/liran_tal" rel="noopener noreferrer" target="_blank">@liran_tal</a> <a class="mention" href="https://x.com/myfear" rel="noopener noreferrer" target="_blank">@myfear</a> Thanks! The fact that you are happy with Asciidoctor is the best present you could give ;) 🎁🍻<br><br>In reply to: <a href="https://x.com/liran_tal/status/1637944819107364866" rel="noopener noreferrer" target="_blank">@liran_tal</a> <span class="status">1637944819107364866</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1637944015873245185',
    created: 1679351259000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/myfear" rel="noopener noreferrer" target="_blank">@myfear</a> Thanks Markus! You\'re always the first one in my TL to raise your glass. And so you\'ll be the name I mention when I hold up my first glass tonight in return! 🍻++<br><br>In reply to: <a href="https://x.com/myfear/status/1637844622067073025" rel="noopener noreferrer" target="_blank">@myfear</a> <span class="status">1637844622067073025</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1637929451991486465',
    created: 1679347787000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/liran_tal" rel="noopener noreferrer" target="_blank">@liran_tal</a> 🥰<br><br>In reply to: <a href="https://x.com/liran_tal/status/1637898934449258499" rel="noopener noreferrer" target="_blank">@liran_tal</a> <span class="status">1637898934449258499</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1635947168526905345',
    created: 1678875174000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> And it isn\'t free for us to provide them with the product they are selling (which is the contents of the registry). They are asking to use our work (which is fine), then asking us to do more for them to be able to sell it (which is not fine).<br><br>In reply to: <a href="https://x.com/maxandersen/status/1635945309552386051" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1635945309552386051</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1635937373308723201',
    created: 1678872838000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/liran_tal" rel="noopener noreferrer" target="_blank">@liran_tal</a> Of course, but so is installing any dependency. All npx really does is cut `npm i` and the command exec down to a single command. You always, always, always have to know and trust your dependencies.<br><br>In reply to: <a href="https://x.com/liran_tal/status/1635936290683203585" rel="noopener noreferrer" target="_blank">@liran_tal</a> <span class="status">1635936290683203585</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1635936976682745856',
    created: 1678872744000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> ...and how much effort we have to put in to prove it. I spend so much time applying for OSS these days, as if developing and managing OSS wasn\'t hard enough.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1635910869090549760" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1635910869090549760</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1635905646217539585',
    created: 1678865274000,
    type: 'post',
    text: 'DockerHub would be worthless if those of us who maintain OSS projects weren\'t preparing and publishing images to it. The disrespect is incredible.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1635905078241402880',
    created: 1678865138000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aheritier" rel="noopener noreferrer" target="_blank">@aheritier</a> <a class="mention" href="https://x.com/glours" rel="noopener noreferrer" target="_blank">@glours</a> <a class="mention" href="https://x.com/ndeloof" rel="noopener noreferrer" target="_blank">@ndeloof</a> <a class="mention" href="https://x.com/morlhon" rel="noopener noreferrer" target="_blank">@morlhon</a> <a class="mention" href="https://x.com/dgageot" rel="noopener noreferrer" target="_blank">@dgageot</a> I checked the status of my teams and I\'m still not sure. This could be the straw that compels me to finally switch to <a href="http://quay.io" rel="noopener noreferrer" target="_blank">quay.io</a>.<br><br>In reply to: <a href="https://x.com/aheritier/status/1635901783615631361" rel="noopener noreferrer" target="_blank">@aheritier</a> <span class="status">1635901783615631361</span>',
    likes: 3,
    retweets: 1
  },
  {
    id: '1635904257810640896',
    created: 1678864943000,
    type: 'quote',
    text: 'If Docker requires any more effort on my part as an OSS maintainer to publish images to DockerHub (such as applying for an OSS plan every year), I\'m just going to stop publishing images there. I\'m not taking on any more work than I already have to do what I\'m already doing.<br><br>Quoting: <a href="https://x.com/alexellisuk/status/1635679295891812359" rel="noopener noreferrer" target="_blank">@alexellisuk</a> <span class="status">1635679295891812359</span>',
    likes: 12,
    retweets: 3
  },
  {
    id: '1635903204427964417',
    created: 1678864692000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/justincormack" rel="noopener noreferrer" target="_blank">@justincormack</a> <a class="mention" href="https://x.com/alexellisuk" rel="noopener noreferrer" target="_blank">@alexellisuk</a> I had the same fear as the original poster when I read this email. This was communicated horribly. It would be nice if your company issued a clarification to all owners of open source organizations so our stress levels can return to normal (which are already at an all-time high).<br><br>In reply to: <a href="https://x.com/justincormack/status/1635704358355468307" rel="noopener noreferrer" target="_blank">@justincormack</a> <span class="status">1635704358355468307</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1634852198525054976',
    created: 1678614112000,
    type: 'post',
    text: 'It\'s cool to see that RubyGems (&gt;= 3.4.8) now offers a command that\'s akin to npm exec (npx) for Node.js. This command provides a way to run a CLI application without having to worry about the details of installing the package.<br><br>For example:<br><br>gem exec asciidoctor -v',
    likes: 6,
    retweets: 0
  },
  {
    id: '1633923052525490176',
    created: 1678392587000,
    type: 'post',
    text: 'Whenever you observe a situation in which a woman is being expected to be better than a man just to be recognized, call that out. I will too, of course.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1633775965066125312',
    created: 1678357518000,
    type: 'post',
    text: 'In recognizing the impacts made by women around me, there\'s a problem inherent in my statement. Our society expects women to be better than men (often by several factors) to be recognized. That\'s wrong. Women deserve respect, equal rights, and privileges, period, abilities aside.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1633775961987747840',
    created: 1678357518000,
    type: 'post',
    text: 'I\'m late on this, but it\'s never too late to say it. I have the privilege of working every day to build a business and numerous open source communities with an extremely adept and versatile woman. I couldn\'t do it without her. Everywhere in tech there are women like her.',
    likes: 9,
    retweets: 0
  },
  {
    id: '1633621834238427136',
    created: 1678320771000,
    type: 'post',
    text: 'My spouse (who is a women) won\'t watch women\'s lacrosse because she feels it supports the idea that it\'s okay for women to have to play by different rules. And the constant reminder that women are treated differently is too painful for her to enjoy it.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1633621085995548677',
    created: 1678320592000,
    type: 'post',
    text: 'I get that there are athletes who have heavily invested in becoming specialists under the current rules. But that\'s no excuse to perpetuate this gender gap. Times change.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1633619189117362177',
    created: 1678320140000,
    type: 'post',
    text: 'There are two issues at hand, aside from the difference itself. First is safety. Women play with no helmets and no gloves. As someone who played, that\'s insane. The second is that a lot of the typical lacrosse play is restricted. So they aren\'t allowed to play the game fully.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1633618085709238274',
    created: 1678319877000,
    type: 'post',
    text: 'I\'m of the firm belief that women\'s lacrosse should have the same rules and equipment as men\'s lacrosse. How is it acceptable they they don\'t? How is this not considered sexist?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1633617752442425345',
    created: 1678319797000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/unleashedwlax" rel="noopener noreferrer" target="_blank">@unleashedwlax</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/WorldLacrosse" rel="noopener noreferrer" target="_blank">@WorldLacrosse</a> I\'m really interested to know when women are going to be allowed to play with the lacrosse rules from the men\'s game. It seems very gendered (some even say sexist) that they have different rules which prevent a lot of the typical game play that is firmly a part of lacrosse.<br><br>In reply to: <a href="https://x.com/unleashedwlax/status/1633160420302405633" rel="noopener noreferrer" target="_blank">@unleashedwlax</a> <span class="status">1633160420302405633</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1633614905306910720',
    created: 1678319119000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ghillert" rel="noopener noreferrer" target="_blank">@ghillert</a> <a class="mention" href="https://x.com/OracleCoherence" rel="noopener noreferrer" target="_blank">@OracleCoherence</a> Excellent! Thanks for reaching out. You may get a message from Paul from the Eclipse Foundation.<br><br>In reply to: <a href="https://x.com/ghillert/status/1633591767785406465" rel="noopener noreferrer" target="_blank">@ghillert</a> <span class="status">1633591767785406465</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1633587342777909250',
    created: 1678312547000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> Will do!<br><br>In reply to: <a href="https://x.com/aalmiray/status/1633580126678966272" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">1633580126678966272</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1633578042995150848',
    created: 1678310330000,
    type: 'post',
    text: 'Is there someone who I can speak to at Oracle about their use of AsciiDoc for documentation? If you\'re that person, or you know the person who is, please reach out.',
    likes: 6,
    retweets: 2
  },
  {
    id: '1633261944735145984',
    created: 1678234966000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kenkousen" rel="noopener noreferrer" target="_blank">@kenkousen</a> <a class="mention" href="https://x.com/codeJENNerator" rel="noopener noreferrer" target="_blank">@codeJENNerator</a> 😆<br><br>In reply to: <a href="https://x.com/kenkousen/status/1633255037945147394" rel="noopener noreferrer" target="_blank">@kenkousen</a> <span class="status">1633255037945147394</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1633261734885736448',
    created: 1678234916000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/kenkousen" rel="noopener noreferrer" target="_blank">@kenkousen</a> <a class="mention" href="https://x.com/codeJENNerator" rel="noopener noreferrer" target="_blank">@codeJENNerator</a> Same about family, except to them I\'ll always be Danny. No use trying to argue it in my case. (Except my father, who has always called me Daniel just because...that\'s just the way it has always been).<br><br>In reply to: <a href="https://x.com/kenkousen/status/1633248559695888384" rel="noopener noreferrer" target="_blank">@kenkousen</a> <span class="status">1633248559695888384</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1633246952929501184',
    created: 1678231392000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/codeJENNerator" rel="noopener noreferrer" target="_blank">@codeJENNerator</a> <a class="mention" href="https://x.com/kenkousen" rel="noopener noreferrer" target="_blank">@kenkousen</a> In my case, college killed my former moniker (Danny). My future college friends took one look at me and said, "yeah, we\'re calling you Dan." And I didn\'t object in the slightest. For me, it was time to turn the page.<br><br>In reply to: <a href="https://x.com/codeJENNerator/status/1633236426341875715" rel="noopener noreferrer" target="_blank">@codeJENNerator</a> <span class="status">1633236426341875715</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1633222164798390272',
    created: 1678225482000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/MarcusHolman1" rel="noopener noreferrer" target="_blank">@MarcusHolman1</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/PLLCannons" rel="noopener noreferrer" target="_blank">@PLLCannons</a> I think this is great news. I really want to see Cannons be competitive this year. There\'s so much potential on that team. It just needs some glue to make it come together ;)<br><br>In reply to: <a href="https://x.com/MarcusHolman1/status/1633069696751681537" rel="noopener noreferrer" target="_blank">@MarcusHolman1</a> <span class="status">1633069696751681537</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1632954803449114624',
    created: 1678161738000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLCannons" rel="noopener noreferrer" target="_blank">@PLLCannons</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/MarcusHolman1" rel="noopener noreferrer" target="_blank">@MarcusHolman1</a> Cannons! Boom!<br><br>In reply to: <a href="https://x.com/PLLCannons/status/1632882607414181889" rel="noopener noreferrer" target="_blank">@PLLCannons</a> <span class="status">1632882607414181889</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1632877527814520832',
    created: 1678143314000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PLLWhipsnakes" rel="noopener noreferrer" target="_blank">@PLLWhipsnakes</a> <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> <a class="mention" href="https://x.com/WillManny_1" rel="noopener noreferrer" target="_blank">@WillManny_1</a> Checks out. There was an article this summer about how Stags was not able to find the right fit on the left. Manny may slide right in there.<br><br>In reply to: <a href="https://x.com/PLLWhipsnakes/status/1632854176148406274" rel="noopener noreferrer" target="_blank">@PLLWhipsnakes</a> <span class="status">1632854176148406274</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1632311517696905216',
    created: 1678008367000,
    type: 'post',
    text: 'It\'s an unending saga. It makes me imagine that there are signs somewhere inside GitHub offices that declare "Users shall not use color!"',
    likes: 2,
    retweets: 0
  },
  {
    id: '1632311003529768961',
    created: 1678008244000,
    type: 'post',
    text: 'It really is quite amusing (in a cynical sort of way) to watch GitHub fight so hard to prevent users from adding colorized text to README files. It\'s like GeoCities in reverse.',
    likes: 6,
    retweets: 0
  },
  {
    id: '1631595091348561921',
    created: 1677837558000,
    type: 'post',
    text: 'Over the course of the last month, I\'ve been hard at work to get the AsciiDoc Language specification off the ground. It\'s a daunting task, but I enjoy the challenge. Lo and behold, we\'re finally starting to feel some lift. More to come soon.',
    likes: 11,
    retweets: 1
  },
  {
    id: '1631219581162512385',
    created: 1677748029000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/GeoffreyDeSmet" rel="noopener noreferrer" target="_blank">@GeoffreyDeSmet</a> <a class="mention" href="https://x.com/OpenRewrite" rel="noopener noreferrer" target="_blank">@OpenRewrite</a> We\'re going to do our best to try to maintain as much compatibility with existing documents as possible...and I think it\'s very achievable given that the syntax is fairly restrictive. Users are then free to remove redundant workarounds for cleanliness when the time comes.<br><br>In reply to: <a href="https://x.com/GeoffreyDeSmet/status/1631178040293072896" rel="noopener noreferrer" target="_blank">@GeoffreyDeSmet</a> <span class="status">1631178040293072896</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1631119400802865152',
    created: 1677724144000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/nearyd" rel="noopener noreferrer" target="_blank">@nearyd</a> That\'s for damn sure.<br><br>In reply to: <a href="https://x.com/nearyd/status/1631090245063114752" rel="noopener noreferrer" target="_blank">@nearyd</a> <span class="status">1631090245063114752</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1631080233666490369',
    created: 1677714806000,
    type: 'post',
    text: 'I don\'t understand how it\'s March.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1631079900336758784',
    created: 1677714726000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/GeoffreyDeSmet" rel="noopener noreferrer" target="_blank">@GeoffreyDeSmet</a> At long last, I\'m happy to share that universal backslash escaping is coming to AsciiDoc as part of the spec. I\'ve never forgotten your request, and I\'m finally able to understand how to do it. Stay tuned for details in an early milestone ;)',
    likes: 1,
    retweets: 0
  },
  {
    id: '1631078633493389312',
    created: 1677714424000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> I loved listening to your take on the sixes championship series. For me, it rounds out the experience. Thanks for adding depth to the emergence of lacrosse in the modern era. It\'s so valuable.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1631077825024520193',
    created: 1677714232000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> I would also like to see a consistent restart. If a shot ends in a miss (turnover) or a goal, the goalie should have to go back to the end line to get a new ball. It\'s super awkward to restart from digging the ball out of the cage.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1631077255630958592',
    created: 1677714096000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> I 💯 agree with you that golden goal makes no sense in sixes. My idea is to win by 2 or play to the end of the extra time. I think a 2 ball here is a decisive victory. Short of that, extra time decides. The first single just isn\'t a decisive victory in my mind.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1631028143934042112',
    created: 1677702387000,
    type: 'post',
    text: 'My head is deeper into the AsciiDoc syntax than it has ever been, and I didn\'t even think that was possible. It\'s amazing how many different ways you can look at something. Kind of reminds me of visiting a painting many times and always seeing something new.',
    likes: 15,
    retweets: 0
  },
  {
    id: '1630856066169962496',
    created: 1677661360000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rafal_codes" rel="noopener noreferrer" target="_blank">@rafal_codes</a> Noted. Thanks for the tip.<br><br>In reply to: <a href="https://x.com/rafal_codes/status/1630810835894280192" rel="noopener noreferrer" target="_blank">@rafal_codes</a> <span class="status">1630810835894280192</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1630773967547138048',
    created: 1677641786000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> Yes. Love it.<br><br>In reply to: <a href="https://x.com/starbuxman/status/1630767826196652032" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">1630767826196652032</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1630702402742685697',
    created: 1677624724000,
    type: 'post',
    text: 'The fact that HTML only provides h1 - h6 is a recurring problem when translating from other document formats to HTML. I don\'t get it? Why can\'t the depth/level be arbitrary? Even the role=heading aria-level combination in ARIA is limited to just 6. What gives?',
    likes: 4,
    retweets: 0
  },
  {
    id: '1629996020259237888',
    created: 1677456309000,
    type: 'post',
    text: 'I\'m getting much more comfortable asking people to be polite or to leave the community. While one incident doesn\'t bother me so much, it hurts the culture when they\'re allowed to add up. Civil discourse is essential for the long-term health of a community.',
    likes: 17,
    retweets: 3
  },
  {
    id: '1629596516288712706',
    created: 1677361060000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/venkat_s" rel="noopener noreferrer" target="_blank">@venkat_s</a> <a class="mention" href="https://x.com/shelajev" rel="noopener noreferrer" target="_blank">@shelajev</a> I like to think of it a little differently. Instead of using println, write a test to study what\'s going on. As you learn about the behavior of the code and get it to function the way you expect, refine that into a true test. Effectively the same, but closer to reality IMO.<br><br>In reply to: <a href="https://x.com/venkat_s/status/1629136539933634562" rel="noopener noreferrer" target="_blank">@venkat_s</a> <span class="status">1629136539933634562</span>',
    likes: 2,
    retweets: 1
  },
  {
    id: '1628657328261300224',
    created: 1677137140000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sharat_Chander" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> Facts. My grandfather, and many others who taught me stuff along the way, used to always say "choose the right tool for the job". That one really stuck with me to.<br><br>In reply to: <a href="https://x.com/Sharat_Chander/status/1628649214166335493" rel="noopener noreferrer" target="_blank">@Sharat_Chander</a> <span class="status">1628649214166335493</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1628590981997273088',
    created: 1677121322000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Ultraapex1" rel="noopener noreferrer" target="_blank">@Ultraapex1</a> Dude, I\'m talking about software integration. Take your rants somewhere else.<br><br>In reply to: <a href="https://x.com/Ultraapex1/status/1628589754693431296" rel="noopener noreferrer" target="_blank">@Ultraapex1</a> <span class="status">1628589754693431296</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1628001787595878406',
    created: 1676980847000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ilopmar" rel="noopener noreferrer" target="_blank">@ilopmar</a> Fortunately, I\'m doing fine. I\'ve had some minor issues in recent years, but nothing out of the ordinary. However, I\'m becoming very worried about what will happen if it is something serious. The outlook is bleak.<br><br>In reply to: <a href="https://x.com/ilopmar/status/1627995689090265090" rel="noopener noreferrer" target="_blank">@ilopmar</a> <span class="status">1627995689090265090</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1627994036773740544',
    created: 1676978999000,
    type: 'post',
    text: 'What happens is that you often just learn to live with the problem because the financial pain is worse.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1627993924781621248',
    created: 1676978972000,
    type: 'post',
    text: 'Given the way healthcare currently works in the US, you hope on your lucky stars that your issue gets resolved on your first visit. Each time you have go back, you pay more for the doctors to try to figure it out what\'s wrong and how to fix it.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1627992605584932864',
    created: 1676978658000,
    type: 'reply',
    text: '@Topgun_Joe I\'ll consider filling out a survey if I get a 10% discount on services for the month that follows. If I\'m doing work for a company, I\'m going to get compensated for it.<br><br>In reply to: <a href="#1627992302693281792">1627992302693281792</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1627992302693281792',
    created: 1676978586000,
    type: 'reply',
    text: '@Topgun_Joe No, I do not. I\'d contact them directly and report the bad experience. This idea of making customers do the work of figuring out how to provide good service is absurd &amp; disrespectful of my time. I\'m already paying through roof for healthcare. I don\'t need to pay with my time too.<br><br>In reply to: <a href="https://x.com/machinepoi2011/status/1627988700402532355" rel="noopener noreferrer" target="_blank">@machinepoi2011</a> <span class="status">1627988700402532355</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1627761162917154816',
    created: 1676923478000,
    type: 'post',
    text: 'Every single time I go to the doctors, I get hit up by both phone &amp; email to complete a survey about my experience. It\'s not my job to review your services! I have enough to handle just worrying about my health. Keep this damn corporate optimization crap out of my healthcare!',
    likes: 3,
    retweets: 0
  },
  {
    id: '1627597417221857280',
    created: 1676884438000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ivan_stefanov" rel="noopener noreferrer" target="_blank">@ivan_stefanov</a> <a class="mention" href="https://x.com/speakjava" rel="noopener noreferrer" target="_blank">@speakjava</a> <a class="mention" href="https://x.com/noctarius2k" rel="noopener noreferrer" target="_blank">@noctarius2k</a> <a class="mention" href="https://x.com/BrianGoetz" rel="noopener noreferrer" target="_blank">@BrianGoetz</a> Many of the other points have reasonable justification. Better is often the enemy of good enough. But, in my opinion, there\'s no excuse for not supporting println (or insert sensible name here). Any jdeveloper has wanted that from the moment they wrote their first Java program.<br><br>In reply to: <a href="https://x.com/ivan_stefanov/status/1627595235059548160" rel="noopener noreferrer" target="_blank">@ivan_stefanov</a> <span class="status">1627595235059548160</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1627596565002215424',
    created: 1676884235000,
    type: 'post',
    text: 'I\'m establishing a new policy. If something that was working all of a sudden stops working because of a change in an operating system, and the fix isn\'t obvious, I\'m going to stop supporting the integration on that OS. I\'m so tired of compensating for other people\'s mistakes.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1627594051074818048',
    created: 1676883635000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/speakjava" rel="noopener noreferrer" target="_blank">@speakjava</a> <a class="mention" href="https://x.com/noctarius2k" rel="noopener noreferrer" target="_blank">@noctarius2k</a> I am behind you 100%. It\'s just silly.<br><br>In reply to: <a href="https://x.com/speakjava/status/1626992062288613379" rel="noopener noreferrer" target="_blank">@speakjava</a> <span class="status">1626992062288613379</span>',
    likes: 4,
    retweets: 0
  },
  {
    id: '1627111938718310401',
    created: 1676768691000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> <a class="mention" href="https://x.com/FlowGo37" rel="noopener noreferrer" target="_blank">@FlowGo37</a> <a class="mention" href="https://x.com/BackoftheBird" rel="noopener noreferrer" target="_blank">@BackoftheBird</a> Has to be on YouTube or ESPN+ or it\'s dead to me. I\'m not going to go hunting down access when I have PLL, NLL, and NCAA on the menu. It makes no sense they are not broadcasting this far and wide. Now, if there were women\'s PBLA, well then.<br><br>In reply to: <a href="https://x.com/danarestia/status/1623745027590569984" rel="noopener noreferrer" target="_blank">@danarestia</a> <span class="status">1623745027590569984</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1626924845215936514',
    created: 1676724084000,
    type: 'post',
    text: 'My spouse has difficulty watching women\'s lacrosse because she so fundamentally disagrees that they are not permitted to wear helmets. And I totally agree. If you ever got hit by a lacrosse ball, you would know why this is a problem in an instant. It can do serious damage.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1625681123304001536',
    created: 1676427558000,
    type: 'post',
    text: '...specifically parsing lightweight markup. In lightweight markup, there is no required structure. You can type any sequence of characters. But only some sequences have meaning. And non-PEG parsers seems to be lost trying to deal with this situation.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1625679329249329153',
    created: 1676427130000,
    type: 'post',
    text: 'My brain seems to only be able to understand PEG grammars. Every other kind of grammar I try (antlr, tree-sitter, etc), I just slam my face into a wall trying to do the most basic things like alternatives.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1624900940431265793',
    created: 1676241548000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/headius" rel="noopener noreferrer" target="_blank">@headius</a> I had that exactly question recently, and JaCoCo is what I settled on. The JVM agent was perfect for extending the coverage in code running in a subprocess (in my case, a Gradle invocation to test a Gradle plugin).<br><br>In reply to: <a href="https://x.com/headius/status/1624726532194197512" rel="noopener noreferrer" target="_blank">@headius</a> <span class="status">1624726532194197512</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1624576045302296576',
    created: 1676164087000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> That Georgetown Hopkins game felt like it had championship intensity, didn\'t it? And the broadcast was top notch. I very much hope that sets the bar for the rest of the season.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1623429387860287488',
    created: 1675890702000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alyaabbott" rel="noopener noreferrer" target="_blank">@alyaabbott</a> <a class="mention" href="https://x.com/zulip" rel="noopener noreferrer" target="_blank">@zulip</a> Hooray! Thank you so much for your attentiveness to this matter. I have so much respect for what you do and the care you put into doing it. Thanks again!<br><br>In reply to: <a href="https://x.com/alyaabbott/status/1623427772113879040" rel="noopener noreferrer" target="_blank">@alyaabbott</a> <span class="status">1623427772113879040</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1623133156621815809',
    created: 1675820075000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/alyaabbott" rel="noopener noreferrer" target="_blank">@alyaabbott</a> <a class="mention" href="https://x.com/zulip" rel="noopener noreferrer" target="_blank">@zulip</a> It would be a dream to have a dedicated permission option to resolve topics. Because we can\'t just have anyone moving topics around. But anyone should be allowed to mark a topic as resolved, esp the OP.<br><br>In reply to: <a href="https://x.com/alyaabbott/status/1623099474192244736" rel="noopener noreferrer" target="_blank">@alyaabbott</a> <span class="status">1623099474192244736</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1623132829990387713',
    created: 1675819997000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/chmrr" rel="noopener noreferrer" target="_blank">@chmrr</a> <a class="mention" href="https://x.com/zulip" rel="noopener noreferrer" target="_blank">@zulip</a> It could be that. Since I can\'t see what they see, it\'s so hard to tell what\'s going on. But people say all the time they they don\'t see the checkmark to resolve their own topic. And I\'m at my wits end to know what to do about it.<br><br>In reply to: <a href="https://x.com/chmrr/status/1623082898550145026" rel="noopener noreferrer" target="_blank">@chmrr</a> <span class="status">1623082898550145026</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1623075880401801216',
    created: 1675806419000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/zulip" rel="noopener noreferrer" target="_blank">@zulip</a> I\'m getting reports from a lot of users that that "mark as resolved" checkbox isn\'t visible to some users. I have tried to tweak the settings dozens of times, but nothing seems to solve it. It\'s becoming very distracting to our conversations. Are you aware of this issue?',
    likes: 0,
    retweets: 0
  },
  {
    id: '1623073772256530435',
    created: 1675805917000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Brian_Fox" rel="noopener noreferrer" target="_blank">@Brian_Fox</a> <a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> 💯<br><br>And for the record, I tend to use namespaces. However, for CLI packages, that makes them more cumbersome to use because of how npx works. That\'s why in this case I wanted the bare name.<br><br>In reply to: <a href="https://x.com/Brian_Fox/status/1623042964938231841" rel="noopener noreferrer" target="_blank">@Brian_Fox</a> <span class="status">1623042964938231841</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1622939223107207169',
    created: 1675773838000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Brian_Fox" rel="noopener noreferrer" target="_blank">@Brian_Fox</a> <a class="mention" href="https://x.com/brunoborges" rel="noopener noreferrer" target="_blank">@brunoborges</a> That\'s great and all, but it doesn\'t address the fact that I was allowed to claim the package only to have it ripped away from me when I tried to publish a stable release. Frankly, this is outrageous.<br><br>In reply to: <a href="https://x.com/Brian_Fox/status/1622924399790723075" rel="noopener noreferrer" target="_blank">@Brian_Fox</a> <span class="status">1622924399790723075</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1622806079251038210',
    created: 1675742094000,
    type: 'post',
    text: 'I just learned I\'ve been publishing an npm package (using the latest tag) into a previously unpublished package. When I went to publish the 1.0.0 version, only then did it tell me the version had already been published. This is an inconsistent policy and a gaping security hole.',
    likes: 7,
    retweets: 0
  },
  {
    id: '1620966392576225280',
    created: 1675303478000,
    type: 'quote',
    text: 'This is such heartening news. Although long overdue, I\'m so glad to see that justice has prevailed. 🎉<br><br>Quoting: <a href="https://x.com/olabini/status/1620616329375150081" rel="noopener noreferrer" target="_blank">@olabini</a> <span class="status">1620616329375150081</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1620015640974352385',
    created: 1675076801000,
    type: 'post',
    text: 'I\'m really quite surprised there\'s no <a class="mention" href="https://x.com/NLL" rel="noopener noreferrer" target="_blank">@NLL</a> team in Washington, D.C., especially for how popular lacrosse is in Maryland and Virginia.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1619969072405565442',
    created: 1675065699000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/TheRealHNK" rel="noopener noreferrer" target="_blank">@TheRealHNK</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> That chat history is a wealth of information. I\'m glad you found your answer there!<br><br>In reply to: <a href="https://x.com/TheRealHNK/status/1619949507634155521" rel="noopener noreferrer" target="_blank">@TheRealHNK</a> <span class="status">1619949507634155521</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1619834549755863040',
    created: 1675033626000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/TheRealHNK" rel="noopener noreferrer" target="_blank">@TheRealHNK</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> I encourage you to ask your question in the project chat at <a href="https://chat.asciidoctor.org" rel="noopener noreferrer" target="_blank">chat.asciidoctor.org</a>. You\'re more likely to get a response there.<br><br>In reply to: <a href="https://x.com/TheRealHNK/status/1619659759749332993" rel="noopener noreferrer" target="_blank">@TheRealHNK</a> <span class="status">1619659759749332993</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1619090674703548416',
    created: 1674856272000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/WootzApp" rel="noopener noreferrer" target="_blank">@WootzApp</a> <a class="mention" href="https://x.com/AsciiDoc" rel="noopener noreferrer" target="_blank">@AsciiDoc</a> Please ask questions at either <a href="https://chat.asciidoctor.org" rel="noopener noreferrer" target="_blank">chat.asciidoctor.org</a> (if it\'s specific to Asciidoctor) or <a href="https://chat.asciidoc.org" rel="noopener noreferrer" target="_blank">chat.asciidoc.org</a>.<br><br>In reply to: <a href="https://x.com/WootzApp/status/1619080235298983938" rel="noopener noreferrer" target="_blank">@WootzApp</a> <span class="status">1619080235298983938</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1618761798769070080',
    created: 1674777862000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/BrianVerm" rel="noopener noreferrer" target="_blank">@BrianVerm</a> I\'ve definitely been feeling the same, which is weird for me since I can\'t ever remember feeling this way. To counter it, I\'ve been setting tougher boundaries and taking time to do things I might have felt guilty or reluctant to do in the past. And that\'s been rewarding.<br><br>In reply to: <a href="https://x.com/BrianVerm/status/1618307986254073856" rel="noopener noreferrer" target="_blank">@BrianVerm</a> <span class="status">1618307986254073856</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1618426411781591042',
    created: 1674697900000,
    type: 'post',
    text: 'After more investigation, I\'ve determined that this is most certainly a bug in GitLab CI. The behavior does not match the documentation: <a href="https://docs.gitlab.com/ee/ci/pipelines/index.html#prefill-variables-in-manual-pipelines" rel="noopener noreferrer" target="_blank">docs.gitlab.com/ee/ci/pipelines/index.html#prefill-variables-in-manual-pipelines</a><br><br>Quoting: <a href="#1611496880177045506">1611496880177045506</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1618210770453680129',
    created: 1674646487000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/EuroNation" rel="noopener noreferrer" target="_blank">@EuroNation</a> 🔥🔥🔥<br><br>In reply to: <a href="https://x.com/EuroNation/status/1616385941559459843" rel="noopener noreferrer" target="_blank">@EuroNation</a> <span class="status">1616385941559459843</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1618008595228164116',
    created: 1674598284000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ggrossetie" rel="noopener noreferrer" target="_blank">@ggrossetie</a> Of course, it doesn\'t help that the emoji renders differently on different platforms, such as here on Twitter.<br><br>I\'m hoping it means something positive...because my fear is that it means "throwing my hands up" and I\'ve been missing people trying to communicate that to me.<br><br>In reply to: <a href="https://x.com/ggrossetie/status/1618004744659955712" rel="noopener noreferrer" target="_blank">@ggrossetie</a> <span class="status">1618004744659955712</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1617833273027358721',
    created: 1674556484000,
    type: 'post',
    text: 'What\'s the 🐙 supposed to mean when used as a reaction in a discussion thread? I\'m left scratching my head.',
    likes: 3,
    retweets: 0
  },
  {
    id: '1617626142667870209',
    created: 1674507101000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/catallman" rel="noopener noreferrer" target="_blank">@catallman</a> I\'m really sorry to hear that. I can vouch for the impact of the work you\'ve done at Google. When I think of Google, your name almost certainly comes to mind. All the best in your next chapter! You have lots to build on!<br><br>In reply to: <a href="https://x.com/catallman/status/1617598741296611328" rel="noopener noreferrer" target="_blank">@catallman</a> <span class="status">1617598741296611328</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1617290434946158592',
    created: 1674427062000,
    type: 'reply',
    text: '@eshepelyuk On the first occurrence, I typically do. I try to be a reasonable person.<br><br>In reply to: <a href="https://x.com/bug_jelly_fish/status/1617290158423920640" rel="noopener noreferrer" target="_blank">@bug_jelly_fish</a> <span class="status">1617290158423920640</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1617290259406151683',
    created: 1674427020000,
    type: 'reply',
    text: '@eshepelyuk Often times, but not always, the person asking for support over a private channel believe themselves to be more important than others in the community. I don\'t want to perpetuate that illusion by indulging them.<br><br>In reply to: <a href="#1617289113052516353">1617289113052516353</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1617289565064298496',
    created: 1674426854000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jpkrohling" rel="noopener noreferrer" target="_blank">@jpkrohling</a> I\'ve definitely responded on private channels in the past. But each time I do it, it creates a precedent I wish I had not set, and one that\'s hard to get out of. So I\'ve learned to be up front about it and avoid that rabbit hole.<br><br>In reply to: <a href="https://x.com/jpkrohling/status/1617258499418820608" rel="noopener noreferrer" target="_blank">@jpkrohling</a> <span class="status">1617258499418820608</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1617289113052516353',
    created: 1674426746000,
    type: 'reply',
    text: '@eshepelyuk I will let people know when they are crossing boundaries that are not acceptable. Honestly, they should know better. But I politely remind them if they don\'t.<br><br>In reply to: <a href="https://x.com/bug_jelly_fish/status/1617155691428429828" rel="noopener noreferrer" target="_blank">@bug_jelly_fish</a> <span class="status">1617155691428429828</span>',
    likes: 3,
    retweets: 0
  },
  {
    id: '1617099151896117248',
    created: 1674381456000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/joschi83" rel="noopener noreferrer" target="_blank">@joschi83</a> I have to add that disclaimer since that\'s what my company offers ;) If I didn\'t have a company that offers OSS support, then I wouldn\'t have that option either.<br><br>In reply to: <a href="https://x.com/joschi83/status/1617089445462953984" rel="noopener noreferrer" target="_blank">@joschi83</a> <span class="status">1617089445462953984</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1617080713563746304',
    created: 1674377060000,
    type: 'post',
    text: 'If you need 1-on-1 support, you can hire me. Otherwise, post your question to the community and I will follow-up when I\'m willing and able.',
    likes: 18,
    retweets: 0
  },
  {
    id: '1617080536958394368',
    created: 1674377018000,
    type: 'post',
    text: 'When I work on OSS, I don\'t answer questions in private messages. Public channels only. No one gets preferential treatment, regardless of how discrete they consider their situation to be. That\'s a core tenet of my OSS philosophy &amp; the terms under which I dedicate my time to it.',
    likes: 104,
    retweets: 10
  },
  {
    id: '1616615473319407618',
    created: 1674266138000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> This would be a fantastic initiative for the AsciiDoc WG...and a great example of why it exists. Advocacy is a central part of the mission of that group.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1616603246759387138" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1616603246759387138</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1616572497906003969',
    created: 1674255892000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/aalmiray" rel="noopener noreferrer" target="_blank">@aalmiray</a> Thanks for that insight. I can tell you that I\'m fighting my instinct to say "Gradle Thing Plugin". Knowing that Maven branding rules require the inverse, I think I\'ll switch back to "Thing Gradle Plugin". (I already have the ID sorted out, so this is just about the proper name).<br><br>In reply to: <a href="https://x.com/aalmiray/status/1616422513143152640" rel="noopener noreferrer" target="_blank">@aalmiray</a> <span class="status">1616422513143152640</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1616413063598727169',
    created: 1674217880000,
    type: 'post',
    text: 'Is a Gradle plugin supposed to be named Thing Gradle Plugin or Gradle Thing Plugin? What\'s the official rule (not just what you prefer)? I\'m quite confused.',
    likes: 1,
    retweets: 0
  },
  {
    id: '1616170861341609984',
    created: 1674160135000,
    type: 'quote',
    text: '<a class="mention" href="https://x.com/hans_d" rel="noopener noreferrer" target="_blank">@hans_d</a> Congratulations! Thanks for all you do to keep the Java community building! 🐘<br><br>Quoting: <a href="https://x.com/Java_Champions/status/1616120162377428993" rel="noopener noreferrer" target="_blank">@Java_Champions</a> <span class="status">1616120162377428993</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1615629169559470081',
    created: 1674030985000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/systemcodegeeks" rel="noopener noreferrer" target="_blank">@systemcodegeeks</a> That\'s precisely what asciidoctor-reducer is designed to do. See <a href="https://github.com/asciidoctor/asciidoctor-reducer" rel="noopener noreferrer" target="_blank">github.com/asciidoctor/asciidoctor-reducer</a><br><br>In reply to: <a href="https://x.com/systemcodegeeks/status/1615408658233397248" rel="noopener noreferrer" target="_blank">@systemcodegeeks</a> <span class="status">1615408658233397248</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1615417390749319173',
    created: 1673980493000,
    type: 'post',
    text: 'It always surprises me that people don\'t realize that GitHub repositories are a professional work environment. Don\'t talk like you\'re out on the town.',
    likes: 8,
    retweets: 0
  },
  {
    id: '1615144866727747585',
    created: 1673915518000,
    type: 'quote',
    text: 'One of my favorite quotes. Always take the high road.<br><br>Quoting: <a href="https://x.com/PLLAtlas/status/1615052573551251456" rel="noopener noreferrer" target="_blank">@PLLAtlas</a> <span class="status">1615052573551251456</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1615143904403402752',
    created: 1673915289000,
    type: 'post',
    text: 'I would kind of be okay if the visits are fixed. But there\'s something to say to be able to go anywhere if the market is picking up. Maybe fixed plus some discretionary visits.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1615143398813626369',
    created: 1673915168000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> Personally, I hope the PLL never goes city-based. Personally, I think it ruins sports. I love having personal allegiance. Then there\'s never a down season. You just love who you love.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1615142659663990784',
    created: 1673914992000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/danarestia" rel="noopener noreferrer" target="_blank">@danarestia</a> Did you catch that Albany / Halifax match up? Wow, intense. And PLL is noticably all over the NLL. This is exciting.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1614744260179525632',
    created: 1673820006000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Oblomov" rel="noopener noreferrer" target="_blank">@Oblomov</a> At this point, I would not recommend that plugin as its likely going to be sunset due to lack of interest to maintain it. I would nudge you towards Hugo or even Antora.<br><br>In reply to: <a href="https://x.com/Oblomov/status/1614644765932281856" rel="noopener noreferrer" target="_blank">@Oblomov</a> <span class="status">1614644765932281856</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1614190346602426368',
    created: 1673687943000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/mogztter" rel="noopener noreferrer" target="_blank">@mogztter</a> Antora plugin for Gradle ;)<br><br>In reply to: <a href="https://x.com/ggrossetie/status/1614189590906019847" rel="noopener noreferrer" target="_blank">@ggrossetie</a> <span class="status">1614189590906019847</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1614076023292952577',
    created: 1673660686000,
    type: 'reply',
    text: '@UncontainJava <a class="mention" href="https://x.com/wimdeblauwe" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> <a class="mention" href="https://x.com/maciejwalkowiak" rel="noopener noreferrer" target="_blank">@maciejwalkowiak</a> <a class="mention" href="https://x.com/springcloud" rel="noopener noreferrer" target="_blank">@springcloud</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/intellijidea" rel="noopener noreferrer" target="_blank">@intellijidea</a> Personally, I consider my viewpoint to be humane and kind. I wouldn\'t be investing in it otherwise. I find WYSIWYIG to be maddening and a drain on my valuable writing time. But if you don\'t believe that, use something else. This is what works for me.<br><br>In reply to: <a href="#1614075275599544322">1614075275599544322</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1614075275599544322',
    created: 1673660508000,
    type: 'reply',
    text: '@UncontainJava <a class="mention" href="https://x.com/wimdeblauwe" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> <a class="mention" href="https://x.com/maciejwalkowiak" rel="noopener noreferrer" target="_blank">@maciejwalkowiak</a> <a class="mention" href="https://x.com/springcloud" rel="noopener noreferrer" target="_blank">@springcloud</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/intellijidea" rel="noopener noreferrer" target="_blank">@intellijidea</a> If AsciiDoc isn\'t for someone, then it isn\'t for someone. No one is forcing anyone to use it. In my opinion, a lightweight markup language is and will always be code. And that is mental model under which I will proceed as long as I\'m working on it.<br><br>In reply to: <a href="https://x.com/UncontainAI/status/1614073772658798594" rel="noopener noreferrer" target="_blank">@UncontainAI</a> <span class="status">1614073772658798594</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1614070108372217857',
    created: 1673659276000,
    type: 'post',
    text: 'I guess it\'s also optimized to maximize LOC so you get a big bonus! 🤣',
    likes: 2,
    retweets: 0
  },
  {
    id: '1614068326409605120',
    created: 1673658851000,
    type: 'post',
    text: 'The only thing going for it is that you can set your beer on either side of the screen. 🍻',
    likes: 1,
    retweets: 0
  },
  {
    id: '1614066064618565632',
    created: 1673658312000,
    type: 'post',
    text: 'Btw, why am I hard on the Google Java format? Just look at this madness! Ridiculous use of space.',
    photos: ['<div class="item"><img class="photo" src="media/1614066064618565632.jpg"></div>'],
    likes: 2,
    retweets: 0
  },
  {
    id: '1614064528588931073',
    created: 1673657946000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> I would advocate for the least controversial formatting, at least to start. I think we all generally know what that is. Something that looks familiar but smooths out some of the inconsistencies that we would also recognize right away. Of course, people could still build on it.<br><br>In reply to: <a href="#1614063943722602498">1614063943722602498</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1614063943722602498',
    created: 1673657806000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> If nothing else, having a central, vendor-neutral provider will be what wins...and also a profile that is allowed to evolve (unlike Sun Java Conventions). Maybe there\'s a strong case for changing how type annotations on a field are formatted. It can evolve this way.<br><br>In reply to: <a href="#1614063027636273152">1614063027636273152</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1614063472769404928',
    created: 1673657694000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> My feeling is that most people don\'t really care what the low-level rules are. They just want it to use space efficiently and be consistent. I\'m willing to bet that most take a profile like Sun Java or Eclipse and tweak a handful of things. But everyone changes those same things.<br><br>In reply to: <a href="#1614063027636273152">1614063027636273152</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1614063027636273152',
    created: 1673657588000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/maxandersen" rel="noopener noreferrer" target="_blank">@maxandersen</a> I think we\'ll find we have more in common than we don\'t...and most would be willing to give up small hills (a space here a newline there) in order to conquer the fragmentation. The only thing we couldn\'t do is settle tabs vs spaces, which is why I recommend having two profiles.<br><br>In reply to: <a href="https://x.com/maxandersen/status/1614060095331094528" rel="noopener noreferrer" target="_blank">@maxandersen</a> <span class="status">1614060095331094528</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1614048475448238080',
    created: 1673654118000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> That\'s cool. spotless also has a ratchet feature, which will only apply formatting to commits since that one.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1614047435034136580" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1614047435034136580</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1614044847064907780',
    created: 1673653253000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/marcsavy" rel="noopener noreferrer" target="_blank">@marcsavy</a> <a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> I tend to use 120. It gives enough more than enough space for one-liners and fits readily on most modern screens with one sidebar. I wouldn\'t go over that, but I could be convinced to go under that. But 80 is too old-fashioned.<br><br>In reply to: <a href="https://x.com/marcsavy/status/1614043968588718080" rel="noopener noreferrer" target="_blank">@marcsavy</a> <span class="status">1614043968588718080</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1614043992605487105',
    created: 1673653049000,
    type: 'post',
    text: 'I really think this has potential. Anyone involved in <a class="mention" href="https://x.com/adoptium" rel="noopener noreferrer" target="_blank">@adoptium</a> agree? We could really, really use something like this.<br><br>Quoting: <a href="#1614031017429594114">1614031017429594114</a>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1614040399626252289',
    created: 1673652193000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> <a class="mention" href="https://x.com/ghusta" rel="noopener noreferrer" target="_blank">@ghusta</a> 2 or 4 spaces is so universal across programming languages that it\'s fundamentally more accessible. Consistency means tools can work with spaces intelligently (as just about every IDE does with autoindent).<br><br>The arguments for tabs are flimsy and almost always based on hearsay.<br><br>In reply to: <a href="#1614039836033437696">1614039836033437696</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1614039836033437696',
    created: 1673652058000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> <a class="mention" href="https://x.com/ghusta" rel="noopener noreferrer" target="_blank">@ghusta</a> I find it difficult to believe that argument. If anything, setting a tab size to 8 makes the code even harder to read because then you get tons of horizontal scrolling, which itself is an accessibility issue.<br><br>In reply to: <a href="https://x.com/starbuxman/status/1614037861304786944" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">1614037861304786944</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1614031849705308160',
    created: 1673650154000,
    type: 'post',
    text: 'The reason it would work is because Eclipse Adoptium would provide the necessary governance...something every other effort seems to lack. The current situation is just a bunch of groups screaming "choose mine!" which keeps it fractured.',
    likes: 2,
    retweets: 0
  },
  {
    id: '1614031503889174533',
    created: 1673650072000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/starbuxman" rel="noopener noreferrer" target="_blank">@starbuxman</a> I like the initiative, but it\'s just too much configuration. (Not Spring\'s fault...more that there\'s just so many places to configure it). Either way, I prefer spotless. It took me only a few minutes to set up (once I had an Eclipse formatting profile to feed it).<br><br>In reply to: <a href="https://x.com/starbuxman/status/1613744527793344513" rel="noopener noreferrer" target="_blank">@starbuxman</a> <span class="status">1613744527793344513</span>',
    likes: 0,
    retweets: 1
  },
  {
    id: '1614031017429594114',
    created: 1673649956000,
    type: 'post',
    text: 'I was thinking, a Java formatting style would be a really great subproject for Eclipse Adoptium. I would totally...adopt it (given it doesn\'t end up being obscure like Google Java format and also provides a profile with spaces over tabs). I think this might be the right approach.',
    likes: 4,
    retweets: 0
  },
  {
    id: '1614029875337396224',
    created: 1673649684000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ghusta" rel="noopener noreferrer" target="_blank">@ghusta</a> I can live with 2 or 4. For Java, I think 4 fits the culture better. But I wouldn\'t blink if a project preferred 2. Code still looks the same.<br><br>In reply to: <a href="https://x.com/ghusta/status/1614029265678245889" rel="noopener noreferrer" target="_blank">@ghusta</a> <span class="status">1614029265678245889</span>',
    likes: 2,
    retweets: 0
  },
  {
    id: '1614029619409358848',
    created: 1673649623000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/preslavrachev" rel="noopener noreferrer" target="_blank">@preslavrachev</a> &gt; abundance of choices<br><br>This is the heart of the matter. If there weren\'t so many choices, there wouldn\'t be so much variance. And many of those choices are either too nuanced (nearly the same) or obscure (something so strange looking it\'s hard to recognize the code).<br><br>In reply to: <a href="https://x.com/preslavrachev/status/1613856769670844417" rel="noopener noreferrer" target="_blank">@preslavrachev</a> <span class="status">1613856769670844417</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1614029057213206540',
    created: 1673649489000,
    type: 'reply',
    text: '@UncontainJava <a class="mention" href="https://x.com/wimdeblauwe" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> <a class="mention" href="https://x.com/maciejwalkowiak" rel="noopener noreferrer" target="_blank">@maciejwalkowiak</a> <a class="mention" href="https://x.com/springcloud" rel="noopener noreferrer" target="_blank">@springcloud</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/intellijidea" rel="noopener noreferrer" target="_blank">@intellijidea</a> So yes, WYSIWYG runs counter to the very essence of what a lightweight markup language provides. If we introduce WYSIWYG, we might as well go back to writing in DocBook or HTML. And I will never do that.<br><br>In reply to: <a href="#1614028689020424196">1614028689020424196</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1614028689020424196',
    created: 1673649401000,
    type: 'reply',
    text: '@UncontainJava <a class="mention" href="https://x.com/wimdeblauwe" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> <a class="mention" href="https://x.com/maciejwalkowiak" rel="noopener noreferrer" target="_blank">@maciejwalkowiak</a> <a class="mention" href="https://x.com/springcloud" rel="noopener noreferrer" target="_blank">@springcloud</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/intellijidea" rel="noopener noreferrer" target="_blank">@intellijidea</a> My stance, no matter how harsh you think it is, is firmly rooted in experience. I hate, and have always hated, WYSIWYG editors because they prevent me from focusing on writing. I got into AsciiDoc to get away from them. That opinion is at the very heart of why I do what I do.<br><br>In reply to: <a href="https://x.com/UncontainAI/status/1613924872098574336" rel="noopener noreferrer" target="_blank">@UncontainAI</a> <span class="status">1613924872098574336</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1614028318818570243',
    created: 1673649313000,
    type: 'reply',
    text: '@UncontainJava <a class="mention" href="https://x.com/wimdeblauwe" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> <a class="mention" href="https://x.com/maciejwalkowiak" rel="noopener noreferrer" target="_blank">@maciejwalkowiak</a> <a class="mention" href="https://x.com/springcloud" rel="noopener noreferrer" target="_blank">@springcloud</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/intellijidea" rel="noopener noreferrer" target="_blank">@intellijidea</a> A WYSIWIG just makes a mess under the covers, completely unbeknownst to the writer. And then it requires special dialogs to edit non-visible metadata, which further complicates the experience. We\'ve seen WYSIWIG HTML editors fail for decades, but still keep trying it anyway.<br><br>In reply to: <a href="https://x.com/UncontainAI/status/1613924872098574336" rel="noopener noreferrer" target="_blank">@UncontainAI</a> <span class="status">1613924872098574336</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1614025516721524736',
    created: 1673648644000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/jonas_willaredt" rel="noopener noreferrer" target="_blank">@jonas_willaredt</a> <a class="mention" href="https://x.com/lucianct2k2" rel="noopener noreferrer" target="_blank">@lucianct2k2</a> 💯 That\'s what I\'m now doing.<br><br>However, no matter how hard I try, I can\'t get IntelliJ to format exactly the same way as the Eclipse formatting profile I pass to spotless. Hence my initial gripe. No agreement on a universal standard (like JavaScript Standard, for example).<br><br>In reply to: <a href="https://x.com/jonas_willaredt/status/1614024967485546496" rel="noopener noreferrer" target="_blank">@jonas_willaredt</a> <span class="status">1614024967485546496</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1614024428182175745',
    created: 1673648385000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/Sc_Meerkat" rel="noopener noreferrer" target="_blank">@Sc_Meerkat</a> I think there\'s a reasonable way to format Java. But what I see is tools having too many rules without enough agreement on what those rules do to arrive at anything close to something universal. IntelliJ and Eclipse, in particular, disagree on where to break a line in many cases.<br><br>In reply to: <a href="https://x.com/Sc_Meerkat/status/1613979402685091840" rel="noopener noreferrer" target="_blank">@Sc_Meerkat</a> <span class="status">1613979402685091840</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1614023818112303104',
    created: 1673648240000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lucianct2k2" rel="noopener noreferrer" target="_blank">@lucianct2k2</a> They can\'t be ignored because you literally cannot see what changed when there are code formatting changes, esp when someone decides to reflow the entire file. I\'m not super critical about what the style is, just as long as its enforced (and there are no tabs, which are absurd).<br><br>In reply to: <a href="#1614023533394538497">1614023533394538497</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1614023533394538497',
    created: 1673648172000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lucianct2k2" rel="noopener noreferrer" target="_blank">@lucianct2k2</a> I beg to differ. For any project I\'ve worked on without a formatting profile in place (and automated), you immediately start getting thrashing of code style with each subsequent commit...esp on mid to large sized teams. Enforcing code style is absolutely essential.<br><br>In reply to: <a href="https://x.com/lucianct2k2/status/1614023177172033537" rel="noopener noreferrer" target="_blank">@lucianct2k2</a> <span class="status">1614023177172033537</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1614023093453983746',
    created: 1673648067000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ghusta" rel="noopener noreferrer" target="_blank">@ghusta</a> Spaces. It\'s not even a worthwhile debate. Tabs will always be applied inconsistently for various reasons, some of which is language compatibility...and because you can\'t see them without help from the editor. Spaces are universal. I\'d never use tabs on a project.<br><br>In reply to: <a href="https://x.com/ghusta/status/1613943950976454673" rel="noopener noreferrer" target="_blank">@ghusta</a> <span class="status">1613943950976454673</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1614022508583288837',
    created: 1673647927000,
    type: 'reply',
    text: '@dhubau That\'s great, but it\'s just one more formatting profile that\'s only partially adopted. If it could be picked up by spotless, I\'d be inclined to give it a try.<br><br>In reply to: @dhubau <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1614021944084680704',
    created: 1673647793000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/lucianct2k2" rel="noopener noreferrer" target="_blank">@lucianct2k2</a> But we have to pay attention or else every commit tries to introduce differences that are peripheral to the code change. The problem is, no two tools agree on how to format when using essentially the same formatting profile. So you have to run a tool like spotless before commit.<br><br>In reply to: <a href="https://x.com/lucianct2k2/status/1613858449405915137" rel="noopener noreferrer" target="_blank">@lucianct2k2</a> <span class="status">1613858449405915137</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1614021181899964417',
    created: 1673647611000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/j2r2b" rel="noopener noreferrer" target="_blank">@j2r2b</a> <a class="mention" href="https://x.com/JFabianMeier" rel="noopener noreferrer" target="_blank">@JFabianMeier</a> <a class="mention" href="https://x.com/rotnroll666" rel="noopener noreferrer" target="_blank">@rotnroll666</a> Yep, spotless has been a blessing. The only thing I can\'t do is get IntelliJ to completely agree on the rules, so if you format the code in IntelliJ, there are sometimes differences. Annoying, but so it goes.<br><br>In reply to: <a href="https://x.com/j2r2b/status/1614005803438202882" rel="noopener noreferrer" target="_blank">@j2r2b</a> <span class="status">1614005803438202882</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1613973609781035009',
    created: 1673636269000,
    type: 'reply',
    text: '@thomas_turrell I find Google style rules to be extremely strange. It\'s like they tried to do the opposite of what anyone would naturally do.<br><br>In reply to: @thomas_turrell <span class="status">&lt;deleted&gt;</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1613973409603682307',
    created: 1673636221000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/john_blum" rel="noopener noreferrer" target="_blank">@john_blum</a> I don\'t believe so because a) that formatting was done long before modern features of Java and b) it\'s very obscure when compared to both intuition and formatting in other languages. But I like the idea of having such a standard.<br><br>In reply to: <a href="https://x.com/john_blum/status/1613955193745596416" rel="noopener noreferrer" target="_blank">@john_blum</a> <span class="status">1613955193745596416</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1613809251373428740',
    created: 1673597083000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/rotnroll666" rel="noopener noreferrer" target="_blank">@rotnroll666</a> That\'s just the thing. Devs who use other programming languages don\'t even talk about this. They just use something like eslint / JavaScript standard / RuboCop and move on to the actual programming work. I don\'t know what it is with the Java world that they get stuck on this.<br><br>In reply to: <a href="https://x.com/rotnroll666/status/1613796586601189376" rel="noopener noreferrer" target="_blank">@rotnroll666</a> <span class="status">1613796586601189376</span>',
    likes: 4,
    retweets: 1
  },
  {
    id: '1613808845637455874',
    created: 1673596986000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/oluwasayo_" rel="noopener noreferrer" target="_blank">@oluwasayo_</a> <a class="mention" href="https://x.com/intellijidea" rel="noopener noreferrer" target="_blank">@intellijidea</a> 💯<br><br>I think that spotless does this pretty well with an Eclipse formatting profile. But it doesn\'t work with anything that IntelliJ can export.<br><br>In reply to: @oluwasayo_ <span class="status">&lt;deleted&gt;</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1613680358679277568',
    created: 1673566352000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/RickEdwards9" rel="noopener noreferrer" target="_blank">@RickEdwards9</a> That\'s become quite clear ;)<br><br>In reply to: <a href="https://x.com/RickEdwards9/status/1613675664858644488" rel="noopener noreferrer" target="_blank">@RickEdwards9</a> <span class="status">1613675664858644488</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1613671393828433920',
    created: 1673564215000,
    type: 'post',
    text: 'Could we agree on Java formatting? Wow, what a rabbit hole this is to get a linter set up in a Java project. No two tools do the exact same thing.',
    likes: 37,
    retweets: 2
  },
  {
    id: '1613658797494657024',
    created: 1673561212000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/wimdeblauwe" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> @UncontainJava <a class="mention" href="https://x.com/maciejwalkowiak" rel="noopener noreferrer" target="_blank">@maciejwalkowiak</a> <a class="mention" href="https://x.com/springcloud" rel="noopener noreferrer" target="_blank">@springcloud</a> <a class="mention" href="https://x.com/asciidoctor" rel="noopener noreferrer" target="_blank">@asciidoctor</a> <a class="mention" href="https://x.com/intellijidea" rel="noopener noreferrer" target="_blank">@intellijidea</a> I fundamentally don\'t believe in WYSIWYG and never will. I think it goes against the philosophy of a lightweight markup language. If someone wants to create it, great. But I would not advocate for it.<br><br>In reply to: <a href="https://x.com/wimdeblauwe/status/1613495986923380737" rel="noopener noreferrer" target="_blank">@wimdeblauwe</a> <span class="status">1613495986923380737</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1613516313976242177',
    created: 1673527241000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/PaulRabil" rel="noopener noreferrer" target="_blank">@PaulRabil</a> <a class="mention" href="https://x.com/espn" rel="noopener noreferrer" target="_blank">@espn</a> Please have ESPN introduce the second midfield too at the start of the game. They come on in the first few possessions and we have no idea who they are (well, we know them, but we didn\'t know it was going to be them).<br><br>In reply to: <a href="https://x.com/PaulRabil/status/1613305537873035264" rel="noopener noreferrer" target="_blank">@PaulRabil</a> <span class="status">1613305537873035264</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1613259830424391680',
    created: 1673466091000,
    type: 'reply',
    text: 'I appreciate the fact <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> cuts the music when the game clock is running. I find it very disconcerting that music is blasting during the action in <a class="mention" href="https://x.com/NLL" rel="noopener noreferrer" target="_blank">@NLL</a> games. It\'s also the most annoying music mix you could possibly make, like shuffle on your whole music collection.<br><br>In reply to: <a href="#1613106608808263680">1613106608808263680</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1613111759187107840',
    created: 1673430788000,
    type: 'post',
    text: 'And by every broadcast, I\'m talking NCAA men/women tourneys, women\'s worlds, MSL/Mann Cup, Minto Cup, Baggataway Cup, IIJL, World Games, Athletics Unlimited, NLL, U21, etc. Anything that got broadcast.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1613109779202965505',
    created: 1673430316000,
    type: 'reply',
    text: 'There\'s also no introduction of the starting roster, like in <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a>. So you have no idea who is even playing until well into the game, and you have to figure it out mostly for yourself. And this matters because starting lines up frequently change.<br><br>In reply to: <a href="#1613108961217245185">1613108961217245185</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1613108961217245185',
    created: 1673430121000,
    type: 'reply',
    text: 'Don\'t get me wrong. I love watching the <a class="mention" href="https://x.com/NLL" rel="noopener noreferrer" target="_blank">@NLL</a>, especially when the broadcast is functioning properly. But there\'s much room for improvement. Hopefully ESPN can help out here a bit more.<br><br>In reply to: <a href="#1613107192722526208">1613107192722526208</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1613108274899058690',
    created: 1673429957000,
    type: 'reply',
    text: 'Simply put, <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> is in a league of its own. Keep up the great work, because it\'s sorely needed. You, Mike, ESPN, and the team are raising the bar, and raising it very high.<br><br>In reply to: <a href="#1613107897873104897">1613107897873104897</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1613107897873104897',
    created: 1673429867000,
    type: 'post',
    text: '<a class="mention" href="https://x.com/PaulRabil" rel="noopener noreferrer" target="_blank">@PaulRabil</a> I just want to share that, as a spectator, I absolutely recognize the professionalism you\'re bringing to the sport of lacrosse. I have watched every lacrosse broadcast starting with the 2022 NCAA tourney and nothing comes close to the presentation of <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a>.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1613107192722526208',
    created: 1673429699000,
    type: 'reply',
    text: 'We\'re also a quarter way into the <a class="mention" href="https://x.com/NLL" rel="noopener noreferrer" target="_blank">@NLL</a> season and the individual player pages are still offline. So no information about players. The <a class="mention" href="https://x.com/NLL" rel="noopener noreferrer" target="_blank">@NLL</a> really needs to get this all worked out before the season begins. Nothing like testing in production.<br><br>In reply to: <a href="#1613106608808263680">1613106608808263680</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1613106608808263680',
    created: 1673429560000,
    type: 'post',
    text: 'When you watch <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> and <a class="mention" href="https://x.com/NLL" rel="noopener noreferrer" target="_blank">@NLL</a>, you just can\'t help but to notice the difference in quality of broadcast. <a class="mention" href="https://x.com/PremierLacrosse" rel="noopener noreferrer" target="_blank">@PremierLacrosse</a> is so sharp with amazing camera angles &amp; replays. In <a class="mention" href="https://x.com/NLL" rel="noopener noreferrer" target="_blank">@NLL</a>, you\'re lucky if you\'re seeing a game, shot, &amp; penalty clock, or even the score at times.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1613019959717949440',
    created: 1673408901000,
    type: 'post',
    text: 'It looks like this might be some sort of rollout artifact. If I run the pipeline manually at least once, the inputs come back. Let\'s hope.<br><br>Quoting: <a href="#1611496880177045506">1611496880177045506</a>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1612361088435384320',
    created: 1673251814000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/ColumbiaMama31" rel="noopener noreferrer" target="_blank">@ColumbiaMama31</a> <a class="mention" href="https://x.com/culturaltutor" rel="noopener noreferrer" target="_blank">@culturaltutor</a> When we used to cross the bridge when I was a child, it felt as through we were traveling through a portal to another universe. And in a way, we were. The ocean! Such a magical experience that I\'ll never forget.<br><br>In reply to: <a href="https://x.com/ColumbiaMama31/status/1612023322673573889" rel="noopener noreferrer" target="_blank">@ColumbiaMama31</a> <span class="status">1612023322673573889</span>',
    likes: 1,
    retweets: 0
  },
  {
    id: '1612347286658633728',
    created: 1673248523000,
    type: 'post',
    text: 'I really dislike fighting in the <a class="mention" href="https://x.com/NLL" rel="noopener noreferrer" target="_blank">@NLL</a>. I find it disconcerting, pointless, and unsportsmanlike. Above all, I think it disrespects the game. I quickly lose respect for players who do it. Just stop.',
    likes: 0,
    retweets: 0
  },
  {
    id: '1611496880177045506',
    created: 1673045771000,
    type: 'post',
    text: 'It used to be the case that if I defined a variable in .gitlab-ci.yml with a description and empty value, an entry for that variable would show up on the Run Pipeline form page in GitLab CI. That\'s no longer the case. That is a MAJOR set back for triggering an automated release.',
    photos: ['<div class="item"><img class="photo" src="media/1611496880177045506.png"></div>', '<div class="item"><img class="photo" src="media/1611496880177045506-2.png"></div>'],
    likes: 3,
    retweets: 0
  },
  {
    id: '1611463362763976704',
    created: 1673037779000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/_Mark_Atwood" rel="noopener noreferrer" target="_blank">@_Mark_Atwood</a> 💯<br><br>(I would even call them intentional).<br><br>In reply to: <a href="https://x.com/_Mark_Atwood/status/1611459294180028416" rel="noopener noreferrer" target="_blank">@_Mark_Atwood</a> <span class="status">1611459294180028416</span>',
    likes: 0,
    retweets: 0
  },
  {
    id: '1610853574254419968',
    created: 1672892394000,
    type: 'post',
    text: 'When I say documentation engineer, that encompasses tech writer as well.',
    likes: 5,
    retweets: 0
  },
  {
    id: '1610852928151236608',
    created: 1672892240000,
    type: 'post',
    text: 'If you\'re looking for a documentation engineer who has exceptional skills and depth of experience in Antora and AsciiDoc, reach out to <a class="mention" href="https://x.com/eskwayrd" rel="noopener noreferrer" target="_blank">@eskwayrd</a> now. Don\'t miss this opportunity to snatch up a top talent!',
    likes: 17,
    retweets: 6
  },
  {
    id: '1610193148302753799',
    created: 1672734937000,
    type: 'reply',
    text: '<a class="mention" href="https://x.com/markdalgleish" rel="noopener noreferrer" target="_blank">@markdalgleish</a> <a class="mention" href="https://x.com/hsablonniere" rel="noopener noreferrer" target="_blank">@hsablonniere</a> OMG same<br><br>In reply to: <a href="https://x.com/markdalgleish/status/1610067909241622532" rel="noopener noreferrer" target="_blank">@markdalgleish</a> <span class="status">1610067909241622532</span>',
    likes: 1,
    retweets: 0
  }
])
