// {{{ domMenu_main: data

domMenu_data.set('domMenu_main', new Hash(
    1, new Hash(
        'contents', 'Home',
        'contentsHover', 'Quick Links',
        'uri', '/',
        'target', '_self',
        'statusText', 'Mojavelinux.com homepages',
        1, new Hash(
            'contents', 'News',
            'uri', '/',
            'target', '_blank',
            'statusText', 'Latest mojavelinux.com news (new window)'
        ),
        2, new Hash(
            'contents', 'Cooker',
            'uri', '/projects',
            'statusText', 'mojavelinux.com open source programs',
            1, new Hash(
                'contents', 'DOM Tooltip',
                'uri', '/projects/domtooltip',
                'statusText', 'DOM Tooltip'
            ),
            2, new Hash(
                'contents', 'DOM Menu',
                'uri', '/projects/dommenu',
                'statusText', 'DOM Menu'
            )),
        3, new Hash(
            'contents', 'Wiki',
            'uri', '/wiki',
            'statusText', 'Various notes I have put together'
		),
        4, new Hash(
            'contents', 'Invalid',
            'uri', 'javascript: alert("This menu item was styled based on its ID:\\n#" + currentTarget.id)',
            'statusText', 'Click to demonstrate javascript execution'
        )),
    2, new Hash(
        'contents', 'About',
        'contentsHover', 'All About Us',
        'uri', '/about',
        'statusText', 'About mojavelinux.com',
        1, new Hash(
            'contents', 'Contact',
            'uri', '/about/contact.php',
            'statusText', 'Contact mojavelinux.com'
        ),
        2, new Hash(
            'contents', 'Geek Speak',
            'uri', '/about/geekspeak.php',
            'statusText', 'Learn what powers mojavelinux.com'
        ),
        3, new Hash(
            'contents', 'Biography',
            'uri', '/about/biography.php',
            'statusText', 'Read about Dan'
		)),
    3, new Hash(
        'contents', 'Demos',
        'contentsHover', 'Live Demos',
        'uri', '',
        'statusText', 'Demo Sites',
        1, new Hash(
            'contents', 'DOM Menu',
            'uri', '/cooker/demos/domMenu',
            'statusText', 'Dynamic hierarchial menu using the DOM'
        ),
        2, new Hash(
            'contents', 'DOM Tooltip',
            'uri', '/cooker/demos/domTT',
            'statusText', 'Dynamic tooltips using the DOM'
        ),
        3, new Hash(
            'contents', 'Popup Calendar',
            'uri', '/cooker/demos/popupCalendar',
            'statusText', 'Date selector popup calendar'
		)),
    4, new Hash(
        'contents', 'Tutorials',
        'contentsHover', 'Tutorials',
        'uri', '',
        'statusText', 'Various tutorials',
        1, new Hash(
            'contents', 'Hash Tables',
            'uri', 'http://www.example.com',
            'statusText', 'Creating hash tables in javascript'
        ),
        2, new Hash(
            'contents', 'Understanding Events',
            'uri', 'http://www.example.com',
            'statusText', 'Understanding Events in javascript'
        ),
        3, new Hash(
            'contents', 'Using setTimeout',
            'uri', 'http://www.example.com',
            'statusText', 'Using the setTimeout method in javascript'
        ),
        4, new Hash(
            'contents', 'Tips & Tricks',
            'uri', 'http://www.example.com',
            'statusText', 'Random tips and tricks'
		)),
    5, new Hash(
        'contents', 'Portfolio',
        'contentsHover', 'Site Designs',
        'uri', '/portfolio',
        'statusText', 'Sites designed by mojavelinux.com',
        1, new Hash(
            'contents', 'Ashley White',
            'uri', 'http://students.risd.edu/yr2006/awhite01/web/',
            'statusText', 'An artist portfolio site'
        ),
        2, new Hash(
            'contents', 'Mojavelinux.com',
            'uri', '/',
            'statusText', 'Of course I designed my own site'
		)),
    6, new Hash(
        'contents', 'Reading',
        'contentsHover', 'Must Reads!',
        'uri', '/reading',
        'statusText', 'Recommended reading',
        1, new Hash(
            'contents', 'Tech Books',
            'uri', '/reading',
            'statusText', 'Recommended tech books'
		))
));

// }}}
// {{{ domMenu_main: settings

domMenu_settings.set('domMenu_main', new Hash(
    'subMenuWidthCorrection', -1,
    'verticalSubMenuOffsetX', -1,
    'verticalSubMenuOffsetY', -1,
    'horizontalSubMenuOffsetX', domLib_isOpera ? 0 : 1,
    'horizontalSubMenuOffsetY', domLib_isOpera ? -1 : 0,
    'openMouseoverMenuDelay', 100,
    'closeMouseoutMenuDelay', 300,
    'expandMenuArrowUrl', 'arrow.gif',
    'baseUri', 'http://www.mojavelinux.com'
));

// }}}
// {{{ domMenu_keramik: data

domMenu_data.set('domMenu_keramik', new Hash(
    1, new Hash(
        'contents', 'Home',
        'uri', '',
        'statusText', 'Home',
        1, new Hash(
            'contents', 'Main Page',
            'uri', 'http://www.example.com',
            'statusText', 'Mojave Page'
        ),
        2, new Hash(
            'contents', 'Contact mojavelinux.com',
            'uri', '',
            'statusText', 'Contact mojavelinux.com',
            1, new Hash(
                'contents', 'Dan',
                'uri', 'http://www.example.com',
                'statusText', 'Dan'
            ),
            2, new Hash(
                'contents', 'Sarah',
                'uri', 'http://www.example.com',
                'statusText', 'Sarah'
            )
        ),
        3, new Hash(
            'contents', 'Terms of Use',
            'uri', 'http://www.example.com',
            'statusText', 'Terms of Use'
        ),
        4, new Hash(
            'contents', 'Search this site',
            'uri', 'http://www.example.com',
            'statusText', 'Search this site'
        ),
        5, new Hash(
            'contents', 'Customize',
            'uri', '',
            'statusText', 'Customize',
            1, new Hash(
                'contents', 'Style Schemas',
                'uri', '',
                'statusText', 'Style Schemas'
            ),
            2, new Hash(
                'contents', 'Blue',
                'uri', 'http://example.com',
                'statusText', 'Blue'
            ),
            3, new Hash(
                'contents', 'Green',
                'uri', 'http://example.com',
                'statusText', 'Green',
                1, new Hash(
                    'contents', 'Green',
                    'uri', 'http://example.com',
                    'statusText', 'Green'
                )
            )
        )
    ),
    2, new Hash(
        'contents', 'CSS',
        'uri', '',
        'statusText', 'CSS',
        1, new Hash(
            'contents', 'Tutorials',
            'uri', '',
            'statusText', 'Tutorial Links'
        ),
        2, new Hash(
            'contents', 'Using Stylesheets',
            'uri', 'http://www.example.com',
            'statusText', ''
        ),
        3, new Hash(
            'contents', 'CSS Positioning',
            'uri', 'http://www.example.com',
            'statusText', 'Learning how to position elements with CSS'
        )
    ),
    3, new Hash(
        'contents', 'JavaScript',
        'uri', '',
        'statusText', 'JavaScript Section',
        1, new Hash(
            'contents', 'Tutorials',
            'uri', '',
            'statusText', 'JavaScript Tutorials'
        ),
        2, new Hash(
            'contents', 'Custom Hash() Class',
            'uri', 'http://www.example.com',
            'statusText', 'Making your own associative arrays in javascript'
        )
    ),
    4, new Hash(
        'contents', 'DHTML',
        'uri', '',
        'statusText', 'Dynamic HTML',
        1, new Hash(
            'contents', 'Tutorials',
            'uri', '',
            'statusText', 'Dynamic HTML Tutorials'
        ),
        2, new Hash(
            'contents', 'DOM Tooltip',
            'uri', 'http://www.example.com',
            'statusText', 'Making custom tooltips using the DOM'
        )
    ),
    5, new Hash(
        'contents', 'PHP',
        'uri', '',
        'statusText', 'PHP Section',
        1, new Hash(
            'contents', 'Tutorials',
            'uri', '',
            'statusText', 'PHP Tutorials'
        ),
        2, new Hash(
            'contents', 'Handling actions',
            'uri', 'http://www.example.com',
            'statusText', 'Form actions in PHP'
        )
    )
));

// }}}
// {{{ domMenu_keramik: settings

domMenu_settings.set('domMenu_keramik', new Hash(
    'menuBarWidth', '0%',
    'menuBarClass', 'keramik_menuBar',
    'menuElementClass', 'keramik_menuElement',
    'menuElementHoverClass', 'keramik_menuElementHover',
    'menuElementActiveClass', 'keramik_menuElementHover',
    'subMenuBarClass', 'keramik_subMenuBar',
    'subMenuElementClass', 'keramik_subMenuElement',
    'subMenuElementHoverClass', 'keramik_subMenuElementHover',
    'subMenuElementActiveClass', 'keramik_subMenuElementHover',
    'subMenuMinWidth', 'auto',
    'horizontalSubMenuOffsetX', -5,
    'horizontalSubMenuOffsetY', 3,
    'distributeSpace', false,
    'openMouseoverMenuDelay', -1,
    'openMousedownMenuDelay', 0,
    'closeClickMenuDelay', 0,
    'closeMouseoutMenuDelay', -1,
    'expandMenuArrowUrl', 'arrow.gif'
));

// }}}
// {{{ domMenu_BJ: data

domMenu_data.set('domMenu_BJ', domLib_clone(domMenu_data.get('domMenu_keramik')));

// }}}
// {{{ domMenu_BJ: settings

domMenu_settings.set('domMenu_BJ', new Hash(
    'menuBarWidth', '0%',
    'menuBarClass', 'BJ_menuBar',
    'menuElementClass', 'BJ_menuElement',
    'menuElementHoverClass', 'BJ_menuElementHover',
    'menuElementActiveClass', 'BJ_menuElementActive',
    'subMenuBarClass', 'BJ_subMenuBar',
    'subMenuElementClass', 'BJ_subMenuElement',
    'subMenuElementHoverClass', 'BJ_subMenuElementHover',
    'subMenuElementActiveClass', 'BJ_subMenuElementHover',
    'subMenuMinWidth', 'auto',
    'distributeSpace', false,
    'openMouseoverMenuDelay', -1,
    'openMousedownMenuDelay', 0,
    'closeClickMenuDelay', 0,
    'closeMouseoutMenuDelay', -1,
    'expandMenuArrowUrl', 'arrow.gif'
));

// }}}
// {{{ domMenu_vertical: data

domMenu_data.set('domMenu_vertical', domLib_clone(domMenu_data.get('domMenu_main')));

// }}}
// {{{ domMenu_vertical: settings

domMenu_settings.set('domMenu_vertical', new Hash(
    'axis', 'vertical',
    'verticalSubMenuOffsetX', -2,
    'verticalSubMenuOffsetY', -1,
    'horizontalSubMenuOffsetX', 1,
    'baseUri', 'http://www.mojavelinux.com'
));

// }}}
// {{{ domMenu_lvertical: data

domMenu_data.set('domMenu_lvertical', domLib_clone(domMenu_data.get('domMenu_main')));

// }}}
// {{{ domMenu_lvertical: settings

domMenu_settings.set('domMenu_lvertical', new Hash(
    'axis', 'vertical',
    'verticalSubMenuOffsetX', -1,
    'verticalSubMenuOffsetY', -1,
    'horizontalSubMenuOffsetX', 1,
    'horizontalSubMenuOffsetY', 0,
    'expandMenuArrowUrl', 'larrow.gif',
	'horizontalExpand', 'west',
	'subMenuMinWidth', 'auto',
    'baseUri', 'http://www.mojavelinux.com'
));

// }}}
