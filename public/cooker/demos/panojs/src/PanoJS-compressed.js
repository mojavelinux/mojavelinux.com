// compressed with http://dojotoolkit.org/docs/shrinksafe
function EventUtils(){
throw "RuntimeException: EventUtils is a static utility class and may not be instantiated";
};
EventUtils.addEventListener=function(_1,_2,_3,_4){
var _5=true;
if(_1.addEventListener){
_1.addEventListener(_2,_3,_4);
}else{
if(_1.attachEvent){
_5=_1.attachEvent("on"+_2,_3,_4);
}else{
_1["on"+_2]=_3;
}
}
return _5;
};
EventUtils.findTarget=function(e,_7){
var _8;
if(window.event){
_8=window.event.srcElement;
}else{
if(e){
_8=e.target;
}else{
_8=window;
}
}
if(!_7&&_8.nodeType==3){
_8=_8.parentNode;
}
return _8;
};
EventUtils.getMousePosition=function(e){
var _a=0;
var _b=0;
if(!e){
e=window.event;
}
if(e.pageX||e.pageY){
_a=e.pageX;
_b=e.pageY;
}else{
if(e.clientX||e.clientY){
_a=e.clientX+document.body.scrollLeft;
_b=e.clientY+document.body.scrollTop;
}
}
return {x:_a,y:_b};
};
function TextSelectionEvent(_c,_d){
this.selectedText=_c;
this.x=_d.x;
this.y=_d.y;
};
function PanoJS(_e,_f){
this.viewerMovedListeners=[];
this.viewerZoomedListeners=[];
if(typeof _e=="string"){
this.viewer=document.getElementById(_e);
}else{
this.viewer=_e;
}
if(typeof _f=="undefined"){
_f={};
}
if(typeof _f.tileUrlProvider!="undefined"&&PanoJS.isInstance(_f.tileUrlProvider,PanoJS.TileUrlProvider)){
this.tileUrlProvider=_f.tileUrlProvider;
}else{
this.tileUrlProvider=new PanoJS.TileUrlProvider(_f.tileBaseUri?_f.tileBaseUri:PanoJS.TILE_BASE_URI,_f.tilePrefix?_f.tilePrefix:PanoJS.TILE_PREFIX,_f.tileExtension?_f.tileExtension:PanoJS.TILE_EXTENSION);
}
this.tileSize=(_f.tileSize?_f.tileSize:PanoJS.TILE_SIZE);
this.zoomLevel=(typeof _f.initialZoom=="undefined"?-1:parseInt(_f.initialZoom));
this.maxZoomLevel=(typeof _f.maxZoom=="undefined"?0:Math.abs(parseInt(_f.maxZoom)));
if(this.zoomLevel>this.maxZoomLevel){
this.zoomLevel=this.maxZoomLevel;
}
this.initialPan=(_f.initialPan?_f.initialPan:PanoJS.INITIAL_PAN);
this.initialized=false;
this.surface=null;
this.well=null;
this.width=0;
this.height=0;
this.top=0;
this.left=0;
this.x=0;
this.y=0;
this.border=-1;
this.mark={"x":0,"y":0};
this.pressed=false;
this.tiles=[];
this.cache={};
var _10=_f.blankTile?_f.blankTile:PanoJS.BLANK_TILE_IMAGE;
var _11=_f.loadingTile?_f.loadingTile:PanoJS.LOADING_TILE_IMAGE;
this.cache["blank"]=new Image();
this.cache["blank"].src=_10;
if(_10!=_11){
this.cache["loading"]=new Image();
this.cache["loading"].src=_11;
}else{
this.cache["loading"]=this.cache["blank"];
}
this.moveCount=0;
this.slideMonitor=0;
this.slideAcceleration=0;
PanoJS.VIEWERS[PanoJS.VIEWERS.length]=this;
};
PanoJS.PROJECT_NAME="PanoJS";
PanoJS.PROJECT_VERSION="1.0.0";
PanoJS.REVISION_FLAG="";
PanoJS.SURFACE_STYLE_CLASS="surface";
PanoJS.WELL_STYLE_CLASS="well";
PanoJS.CONTROLS_STYLE_CLASS="controls";
PanoJS.TILE_STYLE_CLASS="tile";
PanoJS.MSG_BEYOND_MIN_ZOOM="Cannot zoom out past the current level.";
PanoJS.MSG_BEYOND_MAX_ZOOM="Cannot zoom in beyond the current level.";
PanoJS.TILE_BASE_URI="tiles";
PanoJS.TILE_PREFIX="tile-";
PanoJS.TILE_EXTENSION="jpg";
PanoJS.TILE_SIZE=256;
PanoJS.BLANK_TILE_IMAGE="blank.gif";
PanoJS.LOADING_TILE_IMAGE="blank.gif";
PanoJS.INITIAL_PAN={"x":0.5,"y":0.5};
PanoJS.USE_LOADER_IMAGE=true;
PanoJS.USE_SLIDE=true;
PanoJS.USE_KEYBOARD=true;
PanoJS.MOVE_THROTTLE=3;
PanoJS.SLIDE_DELAY=40;
PanoJS.SLIDE_ACCELERATION_FACTOR=5;
PanoJS.DOM_ONLOAD=(navigator.userAgent.indexOf("KHTML")>=0?false:true);
PanoJS.GRAB_MOUSE_CURSOR=(navigator.userAgent.search(/KHTML|Opera/i)>=0?"pointer":(document.attachEvent?"url(grab.cur)":"-moz-grab"));
PanoJS.GRABBING_MOUSE_CURSOR=(navigator.userAgent.search(/KHTML|Opera/i)>=0?"move":(document.attachEvent?"url(grabbing.cur)":"-moz-grabbing"));
PanoJS.VIEWERS=[];
PanoJS.isInstance=function(_12,_13){
while(_12!=null){
if(_12==_13.prototype){
return true;
}
_12=_12.__proto__;
}
return false;
};
PanoJS.prototype={fitToWindow:function(_14){
if(typeof _14!="number"||_14<0){
_14=0;
}
this.border=_14;
var _15=0;
var _16=0;
if(window.innerWidth){
_15=window.innerWidth;
_16=window.innerHeight;
}else{
_15=(document.compatMode=="CSS1Compat"?document.documentElement.clientWidth:document.body.clientWidth);
_16=(document.compatMode=="CSS1Compat"?document.documentElement.clientHeight:document.body.clientHeight);
}
_15=Math.max(_15-2*_14,0);
_16=Math.max(_16-2*_14,0);
if(_15%2){
_15--;
}
if(_16%2){
_16--;
}
this.width=_15;
this.height=_16;
this.viewer.style.width=this.width+"px";
this.viewer.style.height=this.height+"px";
this.viewer.style.top=_14+"px";
this.viewer.style.left=_14+"px";
},init:function(){
if(document.attachEvent){
document.body.ondragstart=function(){
return false;
};
}
if(this.width==0&&this.height==0){
this.width=this.viewer.offsetWidth;
this.height=this.viewer.offsetHeight;
}
var _17=this.tileSize;
if(this.zoomLevel>=0&&this.zoomLevel<=this.maxZoomLevel){
_17=this.tileSize*Math.pow(2,this.zoomLevel);
}else{
this.zoomLevel=-1;
_17=this.tileSize/2;
do{
this.zoomLevel+=1;
_17*=2;
}while(_17<Math.max(this.width,this.height));
if(this.zoomLevel>this.maxZoomLevel){
var _18=this.zoomLevel-this.maxZoomLevel;
this.zoomLevel=this.maxZoomLevel;
_17/=Math.pow(2,_18);
}
}
this.x=Math.floor((_17-this.width)*-this.initialPan.x);
this.y=Math.floor((_17-this.height)*-this.initialPan.y);
for(var _19=this.viewer;_19;_19=_19.offsetParent){
this.top+=_19.offsetTop;
this.left+=_19.offsetLeft;
}
for(var _1a=this.viewer.firstChild;_1a;_1a=_1a.nextSibling){
if(_1a.className==PanoJS.SURFACE_STYLE_CLASS){
this.surface=_1a;
_1a.backingBean=this;
}else{
if(_1a.className==PanoJS.WELL_STYLE_CLASS){
this.well=_1a;
_1a.backingBean=this;
}else{
if(_1a.className==PanoJS.CONTROLS_STYLE_CLASS){
for(var _1b=_1a.firstChild;_1b;_1b=_1b.nextSibling){
if(_1b.className){
_1b.onclick=PanoJS[_1b.className+"Handler"];
}
}
}
}
}
}
this.viewer.backingBean=this;
this.surface.style.cursor=PanoJS.GRAB_MOUSE_CURSOR;
this.prepareTiles();
this.initialized=true;
},prepareTiles:function(){
var _1c=Math.ceil(this.height/this.tileSize)+1;
var _1d=Math.ceil(this.width/this.tileSize)+1;
for(var c=0;c<_1d;c++){
var _1f=[];
for(var r=0;r<_1c;r++){
var _21={"element":null,"posx":0,"posy":0,"xIndex":c,"yIndex":r,"qx":c,"qy":r};
_1f.push(_21);
}
this.tiles.push(_1f);
}
this.surface.onmousedown=PanoJS.mousePressedHandler;
this.surface.onmouseup=this.surface.onmouseout=PanoJS.mouseReleasedHandler;
this.surface.ondblclick=PanoJS.doubleClickHandler;
if(PanoJS.USE_KEYBOARD){
window.onkeypress=PanoJS.keyboardMoveHandler;
window.onkeydown=PanoJS.keyboardZoomHandler;
}
this.positionTiles();
},positionTiles:function(_22,_23){
if(typeof _22=="undefined"){
_22={"x":0,"y":0};
}
for(var c=0;c<this.tiles.length;c++){
for(var r=0;r<this.tiles[c].length;r++){
var _26=this.tiles[c][r];
_26.posx=(_26.xIndex*this.tileSize)+this.x+_22.x;
_26.posy=(_26.yIndex*this.tileSize)+this.y+_22.y;
var _27=true;
if(_26.posx>this.width){
do{
_26.xIndex-=this.tiles.length;
_26.posx=(_26.xIndex*this.tileSize)+this.x+_22.x;
}while(_26.posx>this.width);
if(_26.posx+this.tileSize<0){
_27=false;
}
}else{
while(_26.posx<-this.tileSize){
_26.xIndex+=this.tiles.length;
_26.posx=(_26.xIndex*this.tileSize)+this.x+_22.x;
}
if(_26.posx>this.width){
_27=false;
}
}
if(_26.posy>this.height){
do{
_26.yIndex-=this.tiles[c].length;
_26.posy=(_26.yIndex*this.tileSize)+this.y+_22.y;
}while(_26.posy>this.height);
if(_26.posy+this.tileSize<0){
_27=false;
}
}else{
while(_26.posy<-this.tileSize){
_26.yIndex+=this.tiles[c].length;
_26.posy=(_26.yIndex*this.tileSize)+this.y+_22.y;
}
if(_26.posy>this.height){
_27=false;
}
}
if(!this.initialized){
this.assignTileImage(_26,true);
_26.element.style.top=_26.posy+"px";
_26.element.style.left=_26.posx+"px";
}
if(_27){
this.assignTileImage(_26);
}
_26.element.style.top=_26.posy+"px";
_26.element.style.left=_26.posx+"px";
}
}
if(_23){
this.x+=_22.x;
this.y+=_22.y;
}
},assignTileImage:function(_28,_29){
var _2a,src;
var _2c=(_29?true:false);
if(!_2c){
var _2d=_28.xIndex<0;
var _2e=_28.yIndex<0;
var _2f=_28.xIndex>=Math.pow(2,this.zoomLevel);
var low=_28.yIndex>=Math.pow(2,this.zoomLevel);
if(_2e||_2d||low||_2f){
_2c=true;
}
}
if(_2c){
_2a="blank:"+_28.qx+":"+_28.qy;
src=this.cache["blank"].src;
}else{
_2a=src=this.tileUrlProvider.assembleUrl(_28.xIndex,_28.yIndex,this.zoomLevel);
}
if(_28.element!=null&&_28.element.parentNode!=null&&_28.element.relativeSrc!=src){
this.well.removeChild(_28.element);
}
var _31=this.cache[_2a];
if(_31==null){
_31=this.cache[_2a]=this.createPrototype(src);
}
if(_2c||!PanoJS.USE_LOADER_IMAGE||_31.complete||(_31.image&&_31.image.complete)){
_31.onload=function(){
};
if(_31.image){
_31.image.onload=function(){
};
}
if(_31.parentNode==null){
_28.element=this.well.appendChild(_31);
}
}else{
var _32="loading:"+_28.qx+":"+_28.qy;
var _33=this.cache[_32];
if(_33==null){
_33=this.cache[_32]=this.createPrototype(this.cache["loading"].src);
}
_33.targetSrc=_2a;
var _34=this.well;
_28.element=_34.appendChild(_33);
_31.onload=function(){
if(_33.parentNode&&_33.targetSrc==_2a){
_31.style.top=_33.style.top;
_31.style.left=_33.style.left;
_34.replaceChild(_31,_33);
_28.element=_31;
}
_31.onload=function(){
};
return false;
};
if(!PanoJS.DOM_ONLOAD){
_31.image=new Image();
_31.image.onload=_31.onload;
_31.image.src=_31.src;
}
}
},createPrototype:function(src){
var img=document.createElement("img");
img.src=src;
img.relativeSrc=src;
img.className=PanoJS.TILE_STYLE_CLASS;
img.style.width=this.tileSize+"px";
img.style.height=this.tileSize+"px";
return img;
},addViewerMovedListener:function(_37){
this.viewerMovedListeners.push(_37);
},addViewerZoomedListener:function(_38){
this.viewerZoomedListeners.push(_38);
},notifyViewerZoomed:function(){
var _39=(100/(this.maxZoomLevel+1))*(this.zoomLevel+1);
for(var i=0;i<this.viewerZoomedListeners.length;i++){
this.viewerZoomedListeners[i].viewerZoomed(new PanoJS.ZoomEvent(this.x,this.y,this.zoomLevel,_39));
}
},notifyViewerMoved:function(_3b){
if(typeof _3b=="undefined"){
_3b={"x":0,"y":0};
}
for(var i=0;i<this.viewerMovedListeners.length;i++){
this.viewerMovedListeners[i].viewerMoved(new PanoJS.MoveEvent(this.x+(_3b.x-this.mark.x),this.y+(_3b.y-this.mark.y)));
}
},zoom:function(_3d){
if(this.zoomLevel+_3d<0){
if(PanoJS.MSG_BEYOND_MIN_ZOOM){
alert(PanoJS.MSG_BEYOND_MIN_ZOOM);
}
return;
}else{
if(this.zoomLevel+_3d>this.maxZoomLevel){
if(PanoJS.MSG_BEYOND_MAX_ZOOM){
alert(PanoJS.MSG_BEYOND_MAX_ZOOM);
}
return;
}
}
this.blank();
var _3e={"x":Math.floor(this.width/2),"y":Math.floor(this.height/2)};
var _3f={"x":(_3e.x-this.x),"y":(_3e.y-this.y)};
var _40={"x":Math.floor(_3f.x*Math.pow(2,_3d)),"y":Math.floor(_3f.y*Math.pow(2,_3d))};
this.x=_3e.x-_40.x;
this.y=_3e.y-_40.y;
this.zoomLevel+=_3d;
this.positionTiles();
this.notifyViewerZoomed();
},clear:function(){
this.blank();
this.initialized=false;
this.tiles=[];
},blank:function(){
for(imgId in this.cache){
var img=this.cache[imgId];
img.onload=function(){
};
if(img.image){
img.image.onload=function(){
};
}
if(img.parentNode!=null){
this.well.removeChild(img);
}
}
},moveViewer:function(_42){
this.positionTiles({"x":(_42.x-this.mark.x),"y":(_42.y-this.mark.y)});
this.notifyViewerMoved(_42);
},recenter:function(_43,_44){
if(_44){
_43.x+=this.x;
_43.y+=this.y;
}
var _45={"x":Math.floor((this.width/2)-_43.x),"y":Math.floor((this.height/2)-_43.y)};
if(_45.x==0&&_45.y==0){
return;
}
if(PanoJS.USE_SLIDE){
var _46=_45;
var x,y;
if(_46.x==0){
x=0;
y=this.slideAcceleration;
}else{
var _49=Math.abs(_46.y/_46.x);
x=Math.round(Math.pow(Math.pow(this.slideAcceleration,2)/(1+Math.pow(_49,2)),0.5));
y=Math.round(_49*x);
}
_45={"x":Math.min(x,Math.abs(_46.x))*(_46.x<0?-1:1),"y":Math.min(y,Math.abs(_46.y))*(_46.y<0?-1:1)};
}
this.positionTiles(_45,true);
this.notifyViewerMoved();
if(!PanoJS.USE_SLIDE){
return;
}
var _4a={"x":_43.x+_45.x,"y":_43.y+_45.y};
var _4b=this;
this.slideAcceleration+=PanoJS.SLIDE_ACCELERATION_FACTOR;
this.slideMonitor=setTimeout(function(){
_4b.recenter(_4a);
},PanoJS.SLIDE_DELAY);
},resize:function(){
if(!this.initialized){
return;
}
var _4c=this.viewer.offsetWidth;
var _4d=this.viewer.offsetHeight;
this.viewer.style.display="none";
this.clear();
var _4e={"x":Math.floor(this.width/2),"y":Math.floor(this.height/2)};
if(this.border>=0){
this.fitToWindow(this.border);
}else{
this.width=_4c;
this.height=_4d;
}
this.prepareTiles();
var _4f={"x":Math.floor(this.width/2),"y":Math.floor(this.height/2)};
if(this.border>=0){
this.x+=(_4f.x-_4e.x);
this.y+=(_4f.y-_4e.y);
}
this.positionTiles();
this.viewer.style.display="";
this.initialized=true;
this.notifyViewerMoved();
},resolveCoordinates:function(e){
return {"x":(e.pageX||(e.clientX+(document.documentElement.scrollLeft||document.body.scrollLeft)))-this.left,"y":(e.pageY||(e.clientY+(document.documentElement.scrollTop||document.body.scrollTop)))-this.top};
},press:function(_51){
this.activate(true);
this.mark=_51;
},release:function(_52){
this.activate(false);
var _53={"x":(_52.x-this.mark.x),"y":(_52.y-this.mark.y)};
this.x+=_53.x;
this.y+=_53.y;
this.mark={"x":0,"y":0};
},activate:function(_54){
this.pressed=_54;
this.surface.style.cursor=(_54?PanoJS.GRABBING_MOUSE_CURSOR:PanoJS.GRAB_MOUSE_CURSOR);
this.surface.onmousemove=(_54?PanoJS.mouseMovedHandler:function(){
});
},pointExceedsBoundaries:function(_55){
return (_55.x<this.x||_55.y<this.y||_55.x>(this.tileSize*Math.pow(2,this.zoomLevel)+this.x)||_55.y>(this.tileSize*Math.pow(2,this.zoomLevel)+this.y));
},resetSlideMotion:function(){
if(this.slideMonitor!=0){
clearTimeout(this.slideMonitor);
this.slideMonitor=0;
}
this.slideAcceleration=0;
}};
PanoJS.TileUrlProvider=function(_56,_57,_58){
this.baseUri=_56;
this.prefix=_57;
this.extension=_58;
};
PanoJS.TileUrlProvider.prototype={assembleUrl:function(_59,_5a,_5b){
return this.baseUri+"/"+this.prefix+_5b+"-"+_59+"-"+_5a+"."+this.extension+(PanoJS.REVISION_FLAG?"?r="+PanoJS.REVISION_FLAG:"");
}};
PanoJS.mousePressedHandler=function(e){
e=e?e:window.event;
if(e.button<2){
var _5d=this.backingBean;
var _5e=_5d.resolveCoordinates(e);
if(_5d.pointExceedsBoundaries(_5e)){
e.cancelBubble=true;
}else{
_5d.press(_5e);
}
}
return false;
};
PanoJS.mouseReleasedHandler=function(e){
e=e?e:window.event;
var _60=this.backingBean;
if(_60.pressed){
_60.release(_60.resolveCoordinates(e));
}
};
PanoJS.mouseMovedHandler=function(e){
e=e?e:window.event;
var _62=this.backingBean;
_62.moveCount++;
if(_62.moveCount%PanoJS.MOVE_THROTTLE==0){
_62.moveViewer(_62.resolveCoordinates(e));
}
};
PanoJS.zoomInHandler=function(e){
e=e?e:window.event;
var _64=this.parentNode.parentNode.backingBean;
_64.zoom(1);
return false;
};
PanoJS.zoomOutHandler=function(e){
e=e?e:window.event;
var _66=this.parentNode.parentNode.backingBean;
_66.zoom(-1);
return false;
};
PanoJS.doubleClickHandler=function(e){
e=e?e:window.event;
var _68=this.backingBean;
coords=_68.resolveCoordinates(e);
if(!_68.pointExceedsBoundaries(coords)){
_68.resetSlideMotion();
_68.recenter(coords);
}
};
PanoJS.keyboardMoveHandler=function(e){
e=e?e:window.event;
for(var i=0;i<PanoJS.VIEWERS.length;i++){
var _6b=PanoJS.VIEWERS[i];
if(e.keyCode==38){
_6b.positionTiles({"x":0,"y":-PanoJS.MOVE_THROTTLE},true);
}
if(e.keyCode==39){
_6b.positionTiles({"x":-PanoJS.MOVE_THROTTLE,"y":0},true);
}
if(e.keyCode==40){
_6b.positionTiles({"x":0,"y":PanoJS.MOVE_THROTTLE},true);
}
if(e.keyCode==37){
_6b.positionTiles({"x":PanoJS.MOVE_THROTTLE,"y":0},true);
}
}
};
PanoJS.keyboardZoomHandler=function(e){
e=e?e:window.event;
for(var i=0;i<PanoJS.VIEWERS.length;i++){
var _6e=PanoJS.VIEWERS[i];
if(e.keyCode==109){
_6e.zoom(-1);
}
if(e.keyCode==107){
_6e.zoom(1);
}
}
};
PanoJS.MoveEvent=function(x,y){
this.x=x;
this.y=y;
};
PanoJS.ZoomEvent=function(x,y,_73,_74){
this.x=x;
this.y=y;
this.percentage=_74;
this.level=_73;
};

