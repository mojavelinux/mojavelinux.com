= X Startup
:toc:

One of the most misunderstood workflows is the scripts that lead up to the start of the window manager. At least, I have always found myself at a loss of what actually takes place. This page details some of my findings when working with the Fedora and Debian distributions. In then end, I should hope that you know where to stick that environment variable setting so that it evaluates on X login (window manager environment).

== Debian Process

. GDM (the login manager) presents the user with a graphical login screen (alternatively, startx can be invoked from a virtual console)
. X sessions started by xinit (startx) use the global configuration file `/etc/X11/xinit/xinitrc`
. `xinitrc` immediately hands off the login to the `/etc/X11/Xsession` script
. Xsession defines two user session scripts, `$HOME/.xsession` and `$HOME/.Xsession`
. Xsession runs through the `/etc/X11/Xsession.d/` directory and executes the scripts in numeric order (allows the control of execution order)
.. The `/etc/X11/Xsession.d/50xorg-common_determine-startup` script checks if the user session script exists (and is executable), and if so stores that script in the `$STARTUP` variable
.. otherwise, the value of `$STARTUP` is set to a system default window manager (usually controlled through symlinks)
. the very last script, `/etc/X11/Xsession.d/99xorg-common_start` executes the `$STARTUP` variable by running `exec $STARTUP`

*Note:* Fedora calls the `exec` command with the login flag (-l), which triggers the shell profile scripts to execute. Debian, on the other hand, uses a raw `exec` call, so it is necessary to create a `$HOME/.xsession` script in order to execute the shell login scripts, such as `~/.bash_profile`

== Red Hat/Fedora Process

. XDM (the login manager) presents the user with a graphical login screen (alternatively, startx can be invoked from a virtual console)
. X initialization script is called
.. X sessions started by xinit (startx) use the global configuration file `/etc/X11/xinit/xinitrc`
.. X sessions managed by XDM execute the `/etc/X11/xdm/Xsession` script
. initialization script sources the `/etc/X11/xinit/xinitrc-common` script
. `xinitrc-common` runs through all of the shell scripts in `/etc/X11/xinit/xinitrc.d/`
. User specific startup script is checked
.. `xinitrc` looks for the presence of `~/.Xclients` and if present, is executed with the `exec` command
.. `Xsession` looks for the precense of `~/.Xclients` or `~/.xsession` and if present, is executed with the `exec -l -c` command
. the window manager startup script is now running

*Note:* In order to have login specific environment variables, simply place them in `~/.bash_profile`

== User Session Script

If the user session script (`$HOME/.xsession` or `$HOME/.Xsession`) is used, that script must conclude with a call to a window manager start script (gnome-session, startkde, twm, x-session-manager, ...). It is necessary to do so because this script now +++replaces+++ the system window manager script in the execution chain and that chain ends at the bottom of this script.

Example $HOME/.xsession (calling the bash profile script before startup) `#!/bin/sh

. ~/.bash_profile

exec x-session-manager`
