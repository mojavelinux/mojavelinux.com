= Eclipse Configuration and Usage Tips
:toc:

While there are many good Java IDEs in the market, hands down Eclipse is the best my money can buy. Ironically enough, it is free. Since so many of the IDEs share similar features, the only way to make someone understand why I prefer Eclipse is to explain that Eclipse just works with me. It is well thoughtout, well designed, and extremely powerful. The only downside is the lack of web development support out of the box. However, that is coming soon with the Eclipse WebTools and most likely, its standards will be on par with the rest of the platform's features.

However, no program can truly suffice without a few tweaks. Below, I have summarized some of my preferences for my own resource and to provide insite to other users.

== Launcher Script

No java program is complete without a nice long launch file, right? All kidding aside, Eclipse offers many configuration options when it starts and it would be a shame not to use them to make your Eclipse experience better. For as long as I have been using Eclipse, I have had some variation of the launch script listed below. This script allows me to tweak my Java Runtime Environment, add JVM options, configure the location of my workspace, setup GTK themes, and ensure that the proper Mozilla libraries are available to Eclipse. I typically rename the Eclipse binary to `eclipse.bin` and put this script in its place. Be sure to remove the eclipse.ini file that is next to the binary as this script will add all the necessary initialization parameters.

I am currently using this launch script for Eclipse 3.3.

[source,bash]
----
#!/bin/sh

LAUNCHER_DIR=`dirname $0`
ECLIPSE_HOME=`readlink -f $LAUNCHER_DIR`
ECLIPSE_USERDIR=$HOME/.eclipse

# Setup Java environment if not already available
if [ -z $JAVA_HOME ]; then
    export JAVA_HOME=/usr/lib/jvm/java-1.5.0-sun-1.5.0.06
    export PATH=$JAVA_HOME/bin:$PATH
fi

# Sun JDK Options
JAVA_OPTS="-Xms128m -Xmx512m -Dsun.rmi.dgc.client.gcInterval=3600000 -Dsun.rmi.dgc.server.gcInterval=3600000 -XX:+UseConcMarkSweepGC -XX:+CMSPermGenSweepingEnabled -XX:+CMSClassUnloadingEnabled -XX:MaxPermSize=128m -Dosgi.bundlefile.limit=100"

# Add the following flag to vmargs to enable jmx remote management (Sun JVM)
#JAVA_OPTS="$JAVA_OPTS -Dcom.sun.management.jmxremote"

# Link to correct mozilla libraries
if [ -z $MOZILLA_FIVE_HOME ]; then
    export MOZILLA_FIVE_HOME=`ls -1rd /usr/lib/*firefox*/chrome 2>/dev/null | head -1 | xargs dirname 2>/dev/null`
fi

# Load JNI libraries (native library interface)
export LD_LIBRARY_PATH=/usr/lib/jni

if [ -e $ECLIPSE_USERDIR/gtkrc-2.0 ]; then
    export GTK2_RC_FILES=$ECLIPSE_USERDIR/gtkrc-2.0
fi

$ECLIPSE_HOME/eclipse.bin \
    -showsplash org.eclipse.platform \
    --launcher.XXMaxPermSize 128M \
    -user $ECLIPSE_USERDIR \
    -data $ECLIPSE_USERDIR/workspace \
    -showLocation \
    -clean \
    $@ \
    -vmargs $JAVA_OPTS
exit 0
----

Of course, you may want a nice pretty icon for your launcher too: image:eclipse.svg[eclipse.svg]

== Keyboard Shortcuts

This is a collection of keyboard shortcuts that are useful. You could always use `Ctrl-Shift-L` to browse all the available shortcuts in a given context, but where is the fun in that (and who has the time?)

 +
*Opening Stuff*

* *Ctrl+Shift+T* -- Search for Java type, with live search. Use uppercase letters to search by acronym (PL matches PhaseListener)
* *Ctrl+Shift+R* -- Search for a resource (not as sexy as searching for a Java type)
* *Ctrl+T* -- Quick Hierarchy (use when focused on method name), which can be used to jump to parent or descendent classes
* *Ctrl+O* -- Quick Outline (use when focused on method name), which can be used to jump to other methods
* *F3* -- Open declration (use when focused on method name)
* *Ctrl+Alt+H* -- Open Call Hierarchy (use when focused on method name)

 +
*Moving Around*

* *Alt+left/right* -- Move to next/previous editor in sequence they were openned
* *Ctrl+J* -- Incremental search
* *Ctrl+E* -- Display quick list of open files for switching
* *Ctrl+F6* -- Display another quick list of open files for switching

 +
*Editing*

* *Alt+up/down* -- Grab selected lines (or current line) and move it up and down
* *Ctrl+1* -- Quick Fix: press while cursor is positioned at member variable, parameter, selection, warnings, errors, ...
* *Ctrl+Space* -- Context Assist: press after a ., or to use macros. Press in class-scope to automatically create method declarations.
* *Ctrl+Shift+O* -- Organize Imports
* *Ctrl+Shift+F* -- Reformat (Java) source
* *Alt+Shift+T* -- Quick Refactor Menu
* *Alt+Shift+S* -- Quick Source Menu
* *Ctrl+Shift+/* -- Toggle comments on selected lines
* *Ctrl+Shift+Q* -- Toggle Quick Diff

== Look and Feel

On Linux, hands down the http://jp.bizet.free.fr/themes/gtk2.html[Clarity GTK2] theme is the best looking with Eclipse. (Of course, just about any theme on that page looks good). To setup this theme, create a gtkrc-2.0 file and place it in the ~/.eclipse directory. Next, modify the eclipse startup script to set the GTK2 environment to use this configuration file for the GTK look and feel setting.

**eclipse startup script **

....
GTK2_RC_FILES=~/.eclipse/gtkrc-2.0
....

*~/.eclipse/gtkrc-2.0*

....
/usr/share/themes/Clarity/gtk-2.0/gtkrc
....

The next time you start Eclipse, it will be using this new theme, without affecting your active GTK2 theme setting.

*Note:* It is necessary to have the gtk2-engines package installed since these theme depend on those libraries.

== Plugins

Plugins are really the heart of Eclipse. Here is a http://blog.exis.com/colin/archives/2004/12/23/managing-plugins-in-eclipse/[good article] on how to manage plugins in Eclipse most effectively.

The following is a list of the plugins I use regularly while hacking with Eclipse.

* http://quantum.sourceforge.net/[QuantumDB] - a simple, yet powerful, database management plugin
* http://www.muermann.org/gotofile/[GotoFile] - a quicksearch feature for finding an opening files
* http://www.junginger.biz/eclipse/[RSS View] - A simple, but effective RSS feed reader view (with favicons!) image:/tips/eclipse_update.gif[eclipse_update.gif]
* http://www.psnet.nu/eclipse/[QuickImage] - A quick and dirty image viewer for eclipse, with directory slideshow image:/tips/eclipse_update.gif[eclipse_update.gif]

image:/tips/eclipse_update.gif[eclipse_update.gif] = _plugins with an update site_

*Note:* The http://shinzui.org/space/Eclipse/Eclipse+Plugins[following site] has a nice list of useful plugins for Eclipse. I'm still working my way through the list.

== Plugin Ideas

Now that I have taken a crack at learning how to write my own Eclipse plugin, I need to get together some ideas for useful plugin to make.

* Working Set Generator (create working set from selection, open files, search result, add new project to working set dialog)
* Agenda (could draw from existing Task plugin)
* Double click to open/close file in viewer (more of an eclipse hack)
* Filter ant targets with preference (alternative Ant view)
* Hack http://yasd.cc/google/[Google plugin] to have a text input
* Organize imports (java) on save _shouldn't this be in eclipse by default?_

== Resources

* http://dev2dev.bea.com/pub/a/2005/09/eclipse_web_tools_platform.html[Overview of the Web Tools Platform]
* http://www.easyeclipse.org/site/home[EasyEclipse] - Packages together Eclipse, the open-source software development platform, and selected open source plugins.
