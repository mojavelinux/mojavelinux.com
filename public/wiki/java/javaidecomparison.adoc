= Java IDE Comparison
:toc:

This page strives to answer the age old question, "What is your favorite IDE?" Let's see how I do...

After years of programming, I have finally been held down and coereced into using an IDE for my Java development. When I began using Borland's JBuilder at work, I instantly recognized the efficiency gained by using an IDE for Java server-side development. While tools like Ant, Vim, and the commandline are certainly very powerful, they are not context aware and therefore cannot offer timesaving utilities directly related to development, such as build wizards, refactoring, code completion and templates, and automatic deployment...at least, not without a significant amount of planning and preperation. IDEs offer these features right out of the box.

However, after logging a bunch of hours using JBuilder, I found myself cursing at it quite often...okay, *very* often. While I recognized the importance of its time saving utilities, I came to the conclusion that JBuilder had too many annoying deficiencies to ignore. It was not working with me and certainly not thinking the way that I thought. To find greener pastures, I embarked on a quest to discover the best Java IDE for me. Below is a summary of the ongoing results of this comparison (dated Summer 2004), based on likes and dislikes for each product. Needless to say, this debate is a hot topic right now and there is good reason. _If I make any statements which are untrue, please correct me so that the points made in this list remain current._

== Eclipse 3.0 (Eclipse Project)

*Price:* Free

*License:* Open Source (CPL)

*URL:* http://eclipse.org

*Summary:* Eclipse is an open platform for tool integration built by an open community of tool providers. Operating under a open source paradigm, with a common public license that provides royalty free source code and world wide redistribution rights, the eclipse platform provides tool developers with ultimate flexibility and control over their software technology.

=== Likes:

* IDE looks amazing, across all systems, and it is quite responsive...great interface overall!
* anti-aliased text everywhere, even in UI!
* ability to format to your line wrapping width
* refactoring support ascends from heaven!
* easy to share code between projects
* ant integration very well done, ant launcher makes managing project tree very easy
* content assist is used whereever possible, not just in editor (wizards scrrens)
* amazing control over the formatting of java source code, which can be saved as a profile
* pasting code formats code automatically!
* editor tabs can be moved around with drag-and-drop
* can double-click editor tabs to expand to full workspace view
* has both docked views and fast views (most editors only offer one or the other)
* very powerful control over how the java compiler handles warnings and errors
* very small project-specific footprint on directory tree (.project, .classpath)
* package view has very nice filtering abilities, for what to show/not show (can now filter closed packages, selected working set)
* code assist in ant resolves location properties and displayes the path in a tooltip!!
* code assist shows documentation for each option, especially helpful for ant tasks
* awesome integrated diff engine and viewer for comparing files and refactoring changes
* can rearrange the order of properties and methods in the "outline" view
* context aware templates, tab/shift-tab to jump to different insertion points (markers)

=== Dislikes:

* editor does not have a split file view!!
* lacks general syntax highlighting out of the box (property file highlighting added in 3.1)
* no "soft" line wrapping (only hard wrapping by issuing the format command)
* doesn't come with native servlet/J2EE aware plugins (you have to configure a plugin, such as lomboz, webtools or myeclipse)
* must manually create ant scripts, though the code assist support is very well done
* cannot create new buffer without creating linked file (annoyance, fixed in 3.1)
* double clicking on file in package explorer does not close editor if it is currently open (JEdit feature)

== IDEA (IntelliJ)

*Price:* $500

*License:* Proprietary

*URL:* http://www.jetbrains.com/idea

*Summary:* IntelliJ IDEA is an intelligent Java IDE intensely focused on developer productivity. It provides a robust combination of enhanced development tools, including: refactoring, J2EE support, Ant, JUnit, and CVS integration. Packaged with an intelligent Java editor, coding assistance and advanced code automation tools, IDEA enables Java programmers to boost their productivity while reducing routine time consuming tasks.

=== Likes:

* Superb block editing, the best I have seen, has a distinct "column mode" setting which can be toggled in the context menu
* refactoring works flawlessly, has a nice preview, doesn't hose project
* Excellent keymapping support, going so far as to support key sequences ala emacs
* Interesting support for anywhere editing (automatically places spaces to fill)
* Once tld is accessible (in classpath), instant tag support in JSP, no need to "assign" it to the IDE
* once setup, tomcat integrate is very streamlined, offers a "dump threads" utility for a sudo-reload, JSPs recompile after save
* project view is very simple, easy to understand...arranging projects is user-friendly and intuitive
* best looking theme next to Eclipse (even on Linux), limited only by java and fonts
* anti-aliased text in editor, also in docked tabs
* Struts Console plugin offers great Struts config and tld file support
* excellent community, nice website, great resources for product overall
* very nice integrated class browser whenever a class is required for input
* keymap hints in UI menus update to reflect current keymappings, not just the default set
* ant view allows for filtering of targets using a pick list (great for huge build files)

=== Dislikes:

* deployment server must be setup separately
* No "X" button on editor tabs to close a file, must right click or use keymapping

== JBuilder X (Borland)

*Price:* $3,500

*License:* Proprietary

*URL:* http://borland.com/jbuilder

*Summary:* JBuilder is a complete Java IDE for EJB, Web and Web Services, offering integration with application servers (namely BEA Weblogic), a Struts designer, unit testing, refactoring and support for several source control systems.

=== Likes:

* code template expansions are very helpful, prevents redundant typing (expand with CTRL-J)
* can view images in separate tab
* archive builder offers a nice wizard replacement for many ant tasks
* decent built-in database plugin for managing and querying a SQL database
* built-in makefile task for build, clean, rebuild, no need to create ant script
* sound effects are a nice touch
* JSTL-EL syntax highlighting support
* antialiased text editor
* can make edit window full screen by double clicking tab

=== Dislikes:

* EXPENSIVE!
* options in hard to find places
* code completion is WAY too aggressive and stupid, constantly gets in the way and causes irritation
* auto-indentation is just plain stupid, flaky and often times incorrect
* cannot reformat non-java code, such as XML/XHTML
* renaming a project is a recipie for disaster, everything gets screwed (most crucially the paths)!
* deploying to weblogic is flaky at best, seems to have major caching issues, "web run" conflicts with "deploy" and having both options is confusing
* recompiling with an active "web run" will sometimes redeploy, sometimes won't redeploy; it's completely random, so you constantly have to restart the weblogic server
* cannot "web run" from two different projects at the same time
* spawn browser to test webapp doesn't give option to launch external browser (uses an internal browser instead)
* selecting a test page (and query string) for "web run" is very hard to find, nested deep in the "Run..." configuration
* too many different ways to "rename" things, some with refactor support, some without...should be integrated so you cannot screw it up
* refactoring is crap, even renaming a field screws up the code; possible to hose your project in untold ways by renaming things
* no tld support/taglib creation, you are on your own
* adding fields to class doesn't allow specification of access (private|protected|public)
* some files show up outside of project but they are linked to files inside the project???
* xslt stylesheets have a confusing interface (transform view doesn't work, but browser view does work)
* cannot delete a directory or a package in a project
* sometimes when you add a class in a new package, JBuilder won't include it in the build until you restart
* jsp validation is not very helpful, just tells you on what line an error occurs
* claims it cannot view generated jsp servlet source, even though it exists on disk
* the block editing that exists is not really block editing at all
* key customizations are limited to a single sequence, so emacs bindings are impossible
* split windows are jumpy, they tend to scroll when you don't want them to scroll, making them useless
* keymap hints don't change in UI when the default set are not being used, hardcoded
* TODO project tags have to be in *.java source files, does not support jsp, xml, etc.
* editing dependencies of a web module overwrites tlds, liters directories with unnecessary tld files
* SVN directories are not respected, killed on a "make clean" operation
* crappy built-in database plugin for managing and querying a SQL database (fields don't resize to show data, no sorting)
* using "Find" in regexp mode replaces regexp with first matched string (erasing your regexp), regexp "Replace in path..." does not correctly insert backreference
* web run requires a full deployment to weblogic, so it is slow (embedded servlet would be better)
* wizard for "implement interface" but no wizard for "extend class"
* when creating new files, you get a different create screen depending on where you selected the option, CONFUSING!!
* no syntax highlighting for java properties files, and no editor
* code formatting is tied to project (maybe even file?) and you find yourself constantly adjusting them/reimporting them
* many times the error insight will detect an error, but it won't tell you what the error is until you try to compile
* several step process for code assist to import a missing symbol
* no mechanism for importing files, instead you have to "save as copy..." after opening file
* no syntax highlighting for CSS files, and no editor (and no plugin)
* multiple projects can not be viewed at once, must repeatedly toggle which project is displayed
* no support for ant imports

== JDeveloper (Oracle)

*Price:* $995 (free for non-commercial use)

*License:* Proprietary

*URL:* http://www.oracle.com/technology/products/jdev

*Summary:* Oracle JDeveloper is an integrated development environment with end-to-end support for modeling, developing, debugging, optimizing, and deploying Java applications and Web services. Oracle JDeveloper 10g introduces a new approach to J2EE development with features that enable visual and declarative development. The innovative OAF simplifies J2EE development.

=== Likes:

* very nice integration with embedded OC4J container, very easy to execute test page, updates reflected immediately
* browser launcher is configurable (good for tabbed browsing and multiple launches)
* JSP engine and code editor is the best I have seen, super easy to work with, delightful!
* code insight for EL expressions!
* auto insert of taglib uri directive when inserting a taglib in JSP page source
* several ways to do block indent formatting (highlight block and tab/shift-tab will indent/unindent)
* very nice forms for creating all aspects of classes
* very nice forms for creating classes from *.tld files and keeping the two in-sync/connected
* adding taglibs to a project is somewhat intuitive...if you know what to press, it works just fine
* message windows informative on build
* deployment descriptors have previews so you know what they are going to encapsulate
* nice refactoring support
* code template support
* the timeout on most "auto" aspects can be configured to happen after a time interval, configured with a slider bar...nice concept!
* Nice import mechanism for sources, very comprehensive wizard

=== Dislikes:

* old stuff tends to hang around, constant have issues with "artifacts" of a project, no "make clean"
* files can get really messed up if you don't remove them properly from disk...learn the order of doing things and stick with it
* no clear way to remove a class altogether, if created by accident (I found it, located in file, NOT intuitive)
* no way to rename a *.tld file once it is created with wizard
* somewhat slow, not quite as quick as netbeans
* several places where a class or interface is required that you have to do manual typing, no integrated class browser
* look and feel is descent, some descent icons
* cannot maximize a tab to take up the whole window.
* no way to make textarea anti-aliased text
* doesn't remember last openned directory...argh!!

== NetBeans 3.x (Sun Microsystems)

*Price:* Free

*License:* Open Source (SPL)

*URL:* http://netbeans.org

*Summary:* The NetBeans IDE is a development environment - a tool for programmers to write, compile, debug and deploy programs. It is written in Java - but can support any programming language. It is a free product with no restrictions on how it can be used. It includes syntax highlighting and language support for Java, JSP, XML/XHTML, visual design tools, code generators, ant and CVS support.

=== Likes:

* very nice code completion engine, never seems too aggressive, very thorough in what it includes, esp jsp
* code formatting/reindent task (located in context menu), especially nice for XML/XHTML
* creating custom tag libraries was very easy, step by step process straightforward (once I knew where to look)
* very nice integration with Tomcat, no setup required, direct pages testing, spawns browser, always updates correctly
* can specify default request parameters when testing a page
* view generated source code for jsp
* the whole Tomcat integration module is superb!
* nice jsp tag repository support in jsp editor, very easy to add tag libraries to jsp page
* HTTP monitor integration very nice, again, part of integration with servlet engine, A++
* can move around tabs, I love this (tabs also don't wrap to next line, big plus)
* validate JSP is very helpful and quite informative...tells you exactly what is missing
* javadoc wizard could come in handy, definitely a cool feature, more than just a GUI
* method/field navigator as a select box is a nice idea on main UI, don't take up space
* can move around all views, group as tabs, etc...easy to customize placements, but doesn't get too complicated like with "workspaces" in eclipse
* block indenting/unindenting function, great for XML/XHTML (hardly need it with the code reformatting)
* very responsive and quick UI
* anti-aliased text in editor
* very acceptable IDE layout
* can right-click file and "Save as template..." which is especially nice for JSP content pages
* very nice database browser, manipulator, easy to setup

=== Dislikes:

* UI looks crappy (metal), icons suck
* no refactoring or refactoring templates (try/catch, rename field, etc...)
* no JSTL-EL support in syntax highlighting/code completion
* cannot wrap arbitrary text in editor
* wierd issues with "mounting" core libraries, causes errors and features not to work correctly, such as code completion...the whole "mounting" thing is very strange
* new file wizards too minimial
* limited control over formatting of java source code (though the defaults won't kill you and it can reformat nicely)
* Very difficult (for newbies impossible) to add a regular ol' jar file to a project. Every mechanism seems to work against you.
* Cannot import sources from somewhere else into project, rather you can only mount them where they are...need a copy mechanism
