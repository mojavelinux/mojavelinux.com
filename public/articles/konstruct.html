<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 //EN"
	"http://www.w3.org/TR/REC-html40/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Mojavelinux Showcase: Konstructing a New KDE Desktop</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="author" content="Dan Allen" />
		<meta name="copywrite" content="2004 Mojavelinux, Inc. All Rights Reserved." />
		<meta name="date" content="2004-05-04" />
		<meta name="version" content="1.2.1" />
		<style media="screen">
@import url(konstruct.css);
		</style>
	</head>
	<body>
		<h1>
			<span class="title">Konstructing a New KDE Desktop</span>
			<span class="byline">by Dan Allen</span>
		</h1>

		<p class="intro">The purpose of this little tutorial is to introduce a better solution to upgrading KDE, one that will allow a user to get a release up and running the night it hits the mirrors, without having to hose the base OS install.  "A miracle?" you say.  Well...perhaps just the saving grace we have been looking for.  The solution is to <strong>Konstruct</strong> a new desktop rather than using RPM or DEB to upgrade the system packages.</p>

		<p>While many of the programs written for the Linux operating system can easily be installed using one of the various <a href="http://www.linuxplanet.com/linuxplanet/tutorials/4161/1/" target="_blank">package management utilities</a>, there are those programs which are so broad, with so many dependencies, that installing them requires nothing short of a dist-upgrade (to use <a href="http://www.debian.org/doc/manuals/apt-howto/ch-apt-get.en.html" target="_blank">apt</a> terms).  Even if packages were made available for a specific platform version, the installation is generally irreversible, so it is an acute risk as there is just no turning back.  You are in a sense letting Pandora out of your Linux box.</p>

		<p>One such program of significant impact is the <a href="http://www.kde.org" target="_blank">K Desktop Environment</a>, currently in its third version.  Aside from being a massive project, KDE brings exciting, integrated features to the Linux desktop.  In fact, the enhancements made in KDE from one version to the next are usually so significant to the desktop experience that users start screaming to their respective Linux vendors for binary packages to be made available the minute that a new release is announced on <a href="http://www.kde.org" target="_blank">kde.org</a>.  Granted, the KDE project does make such packages available for various standard Linux distributions, but those configurations are typically generic and do not receive as widespread testing.  Thus, the kde.org packages have a reasonable chance of being incompatible with the target system.</p>

		<p>Alas, there is hope! <a href="http://developer.kde.org/build/konstruct/" target="_blank">Konstruct</a> is a build utility based on <a href="http://www.gnome.org/~jdub/garnome/" target="_blank">Garnome</a> from the Gnome project.  It takes the process of building from source to a whole new level by making it...<i>simple</i>!  Jason Brooks of eWeek <a href="http://www.eweek.com/article2/0,1759,1490983,00.asp" target="_blank">claims</a> that "Konstruct Makes Loading KDE Easier."  Personally, I think it just makes life easier.  At least now you won't have to sleep on the couch because you broke the KDE installation on your spouse's computer (which of course also happens to be your test box).  Instead, you can use the newest version of KDE without your spouse even knowing it is installed!  In fact, using the Konstruct version of KDE won't even affect your own KDE profile from the base system.</p>

		<h2>The Old Way</h2>

		<p>Time and time again I have seen web pages crop up around the internet boasting the following brief tutorial for installing KDE on a linux distribution and frankly, it scares me.</p>

		<pre class="code">
cd /tmp/kde-rpms/
sudo rpm -Uvh *</pre>

		<p><i>Yikes</i>!  That advice is sort of like saying that all you need to get rich is to give someone your life savings and within a few days you will see a huge return on your "investment".  The next thing you know, you are out on your butt with nothing and you have to start over from scratch.  The problem with this plan is that there is no way to go back and no insurance to protect you of potential ill-will.  The instructions to use RPM to install all of the KDE packages at once does not take into consideration some very important questions, such as whether the RPMs will play nicely with the other OS packages, what dependencies might exist with the base packages and whether the binary packages will even work at all.  It is very much a leap of faith.</p>

		<p>You are probably thinking by now, "What are we waiting for?  Let's get started already!"  Very well, I will show you how simple it is to get a second KDE desktop running on your system using Konstruct without every affecting your base KDE installation.</p>

		<h2>Preparation</h2>

		<p>As with any instructions, <i>read everything before doing anything</i>! While the steps in this article are not complicated, they are all very important in achieving a stable KDE environment.</p>

		<p>Before running a Konstruct build, it is necessary to ensure that the proper development libraries are present.  In some cases, the Konstruct build will halt when a missing library is detected.  In other cases, the feature that requires the dependent library in order to build will just be excluded. The dependencies can usually be gathered using the package management tool on your system.  The required libraries include:</p>

		<ul>
			<li>lesstif lesstif-devel</li>
			<li>pcre libpcre0 libpcre0-devel</li>
			<li>libxml2 libxml2-devel</li>
			<li>openssl libopenssl libopenssl-devel</li>
		</ul>

		<p>A detailed description of these requirements can be found at the <a href="http://developer.kde.org/build/compile_cvs.html" target="_blank">KDE Developer's Corner</a>. To determine if you have each of these packages, either use your package management utility or, from a console, you can issue the command <code>rpm -qa | grep %s</code>, where "%s" is a fragment of the package name.  (For instance, if you are looking for the lesstif libraries, you would execute <code>rpm -qa | grep lesstif</code>, which would show the full package name and version listed on stdout if matching libraries are found.) <i>Another way to check for these packages is by running <code>make</code> from <code>misc/detektive</code> directory, which scans for required packages.</i>  Chances are, you won't be missing very many of these libraries if you installed a complete Linux system from a major vendor. Once you have verified that your system has all of the packages listed above, regardless of the version, you are ready to move onto the build.</p>

		<h2>The Build Process</h2>

		<p>To begin, head on over to <a href="http://developer.kde.org/build/konstruct/" target="_blank">http://developer.kde.org/build/konstruct/</a> and download the Konstruct tarball to a temporary location where the build will eventually take place.  Next, extract the tarball and enter the resulting <code>konstruct/</code> directory created adjacent to the tarball using the following commands:</p>

		<pre class="code">
tar -jxf konstruct-stable.tar.bz2
cd konstruct</pre>

		<p>The first step in the Konstruct build process is setting the destination directory.  In the <code>konstruct/</code> directory just created, open the file <code>gar.conf.mk</code> and set the target directory by modifying the <code>prefix</code> variable around line 38.  I recommend using the prefix <code>/opt/kde3.2.x</code> (<i>where the 'x' is the current minor version</i>) so that your build is portable between systems (i.e. you can share the build with your friends by providing a tarball of the Konstruct build output using <code>tar -zcpf</code>).  The reason it is necessary to choose a neutral directory is because once KDE is compiled, the name or location of the directory in which it was compiled cannot be changed.  Since <code>/opt</code> is the stardard directory for optional software packages in Unix, it is a perfect choice.  The third and final step for building Konstruct is extremely straightforward.  The directions in the README and on the website state that "a complete KDE installation should be as easy as executing, from the <code>konstruct/</code> directory:</p>

		<pre class="code">
cd meta/kde
make install</pre>

		<p>More information about this command can be found in the README file, but if you are looking for a basic install, the <code>meta/kde</code> directory will get you there. I recommend that you run this build as the <i>root</i> user so that the build can be used system-wide and so that the binaries have the proper permissions to perform necessary tasks.  If you are looking just to run it in your home directory, then you can stick to running the build as a normal user.</p>

		<p>As a word of warning, a complete Konstruct build does take about one night to complete, depending of course on the capabilities of the box on which it is being built.  When dawn arrives, and all goes well, <code>/opt/kde3.2.x</code> should be populated with a fresh copy of KDE 3.2.x!</p>

		<h2>Post-Build Configuration</h2>

		<p>In order to use the new version of KDE provided by the Konstruct build, rather than the one from the base system, several hints have to be provided by way of environment variables that point in the direction of the alternate environment.  The easiest way to provide these settings is to create a short shell script named <code>~/.bash/env-konstruct.sh</code> and source it from the <code>~/.bashrc</code> script using the command <code>. ~/.bash/env-konstruct.sh</code>. By using this approach, it allows you to "turn on" and "turn off" the use of the Konstruct build by simplying choosing to source or not to source this script at login.  In addition to setting up the path to the new KDE environment, these settings instruct KDE to use a seperate folder for your profile so that it does not conflict with your KDE profile from the base system.  The script is listed below:</p>

		<pre class="filename">file: ~/.bash/env-konstruct.sh</pre>
		<pre class="code">
#!/bin/sh

# Konstruct settings for KDE 3.2.x (where 'x' is the current minor version)
export KONSTRUCTDIR=/opt/kde3.2.x
export KDEHOME=~/.kde3.2.x

export QTDIR=$KONSTRUCTDIR
export KDEDIR=$KONSTRUCTDIR
export KDEDIRS=$KONSTRUCTDIR
export LD_LIBRARY_PATH=$KONSTRUCTDIR/lib
export PATH=$KONSTRUCTDIR/bin:$KONSTRUCTDIR/sbin:$PATH</pre>

		<p>After running KDE 3.2 from a Konstruct build for a while, I recognized that I consistently had problems at KDE startup and shutdown.  Stepping through the KDE start process, I figured out the source of these errors.  In order to ensure that the proper login sequence is followed, several changes must be made to the KDE statup scripts.</p>

		<p>When KDE is starting up, somewhere in the login sequence the <code>PATH</code> environment variable is being overwritten and thus the login scripts use the <code>/usr/bin/startkde</code> script rather than the one from <code>/opt/kde3.2.x</code>.  To solve this problem, it is necessary to edit <code>/usr/bin/startkde</code> so that it tests for the presence of the environment variable <code>KONSTRUCTDIR</code>.  If that variable is found to be non-empty, it is necessary to update the <code>PATH</code> environment variable to include <code>$KONSTRUCTDIR/bin</code> and <code>$KONSTRUCTDIR/sbin</code> and then fork <code>startkde</code>, followed by an immediate exit.  While this change must be made to the system-wide startkde program, it does not affect users that do not have the <code>KONSTRUCTDIR</code> environment variable set and is thus transparent to all other users.  This change can be made by adding the following code immediately after the first line of the <code>/usr/bin/startkde</code> script.</p>

		<pre class="filename">file: /usr/bin/startkde</pre>
		<pre class="code">
if test -n "$KONSTRUCTDIR"; then
	export PATH=$KONSTRUCTDIR/bin:$KONSTRUCTDIR/sbin:$PATH
	startkde
	exit 0
fi</pre>

		<p><i><b>A note about fonts</b></i>. Although I originally was under the impression that KDE running under <code>/opt/kde3.2.x/</code> does not using the system installed fonts, I later determined that I was incorrect in making this assumption.  Since KDE makes use of fontconfig to handle font configuration, adhearing to the freedesktop.org standard, you will find all the great fonts that you have under your original environment available in the konstruct-built KDE.</p>

		<p>The final step is to logout of KDE and log back in.  You should immediately be presented with the KDE 3.2.x splash screen.  Pat yourself on the back, for you have successfully built KDE using Konstruct!  Certainly a monumental accomplishment!</p>

		<h2>Supplemental Tweaks</h2>

		<p>Finally, we get down to the gory details.  In order to enable certain features that require temporary root access, such as modifying filesharing and changing a user password, it is necessary to set the SETUID bits on any such binaries.  One case, in particular, is the <code>fileshareset</code> script, which requires temporary root access to update the <code>smb.conf</code> file when the "Local Net Sharing" tab in used in the folder properties dialog to add or remove a share.  Additionally, the <code>kcheckpass</code> script requires temporary root access to allow a user write access to <code>/etc/shadow</code>.  The permission change is done by executing a <code>chmod</code> on these binaries. <i><b>The SETUID problem is now fixed in the KDE 3.2.2 version of Konstruct</b></i>.</p>

		<pre class="code">
chmod u+s $KONSTRUCTDIR/bin/fileshareset
chmod u+s $KONSTRUCTDIR/bin/kcheckpass</pre>

		<p>If you login to your new Konstruct-built KDE desktop, you might notice that a significant number of entries in the Kmenu hierarchy are missing an icon.  Looking closely, you will notice that most of these entries are either Gnome, GTK or Java applications.  The problem is that KDE is using the <code>$KDEDIR/share/pixmaps/</code> as the prefix for the icon listed in the respective <code>.desktop</code> file for these items, a folder which obviously does not exist.  This can easily be solved by adding a symlink to the system pixmaps folder.</p>

		<pre class="code">
ln -s /usr/share/pixmaps /opt/kde3.2.x/share/pixmaps</pre>

		<p><i><b>One other word of warning</b></i>.  If you have any KDE programs installed that are not part of the base distribution, you are going to exprience problems with icons.  In short, you will have to recompile those programs into the <code>/opt/kde3.2.x</code> prefix if you want to use them without issue.  You can recognize these programs in the Kmenu since they will be the only ones left without icons.</p>

		<h2>Conclusion</h2>

		<p>The next time you plan on upgrading your KDE desktop, do not overlook Konstruct, especially if you are afraid that it will be too complicated.  Compared to using RPM/DEB packages, Konstruct is significantly easier and far more dependable. I kicked myself after I built KDE 3.2 the first time using Konstruct for not giving it a try sooner.  I am seriously considering the possibility of never using RPMs to upgrade KDE again.  The configuration of the Konstruct-built KDE 3.2.1 on my system is stable and integrated enough to use it as my main desktop, while my wife is still enjoying the base KDE 3.1 installation from Mandrake 9.2.  Don't delay another minute, start konstructing today!</p>

		<p class="footer">&copy; <a href="http://www.mojavelinux.com">Mojavelinux, Inc</a> &middot; Document Version: 1.2.1 &middot; Last Modified: Tue Mar 04, 2004 &middot; License: Creative Commons</p>
	</body>
</html>
