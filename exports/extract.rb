require 'set'
require 'time'
require 'fileutils'

keep_meta = %i(author title slug status)

%w(source/blog/posts source/blog/comments source/articles).each {|path| FileUtils.mkdir_p path }

%w(blog articles).each do |site|
  ((File.read %(exports/#{site}.txt)).split %r/^-{8}$/).each do |post|
    parts = post.split %r/^-{5}$/
    meta = parts.shift
    body = parts.shift.strip.sub %r/\ABODY:\n/, ''
    extended_body = parts.shift.strip.sub %r/\AEXTENDED BODY:\n?/, ''
    body += %(\n\n#{extended_body}) unless extended_body.empty?
    excerpt = parts.shift.strip.sub %r/\AEXCERPT:\n?/, ''
    keywords = (parts.shift.strip.sub %r/\AKEYWORDS:\n?/, '').split ', '
    parts.pop if parts[-1].strip.empty?
    comments = parts.map {|comment| comment.strip.sub %r/\ACOMMENT:\n/, '' }.join %(\n\n-----\n\n) if site == 'blog'
    meta = (meta.split ?\n).reduce({}) do |accum, line|
      next accum if line.empty?
      key, val = line.split ': ', 2
      key = (key.tr ' ', '_').downcase.to_sym
      val = %('#{val.gsub ?', "''"}') if key == :title && val.chr == ?"
      if key == :primary_category || key == :category
        (accum[:categories] ||= Set.new).add val
      elsif key == :date
        accum[:date] = (Time.strptime val, '%m/%d/%Y %H:%M:%S %p').strftime '%Y-%m-%d %H:%M:%S'
      elsif key == :convert_breaks
        accum[:breaks] = val == '0' ? false : true
      elsif keep_meta.include? key
        accum[key] = val
      end
      accum
    end
    meta[:excerpt] = excerpt unless excerpt.empty?
    meta[:keywords] = keywords
    meta[:categories] = meta[:categories].to_a
    source = ['---']
    meta.each do |key, val|
      source << %(#{key}: #{Array === val ? ?[ + (val.to_a.join ', ') + ?] : val})
    end
    source << '---'
    source << body
    source << ''
    basename = %(#{(meta[:date].split ' ')[0]}-#{meta[:slug]})
    if site == 'blog'
      File.write (File.join 'source', 'blog', 'posts', %(#{basename}.md)), (source.join ?\n)
      File.write (File.join 'source', 'blog', 'comments', %(#{basename}.txt)), comments unless comments.empty?
    else
      File.write (File.join 'source', 'articles', %(#{basename}.md)), (source.join ?\n)
    end
  end
end
