AUTHOR: Thai Dang Vu
EMAIL: dxxvi@yahoo.com
IP: 71.74.71.11
URL:
DATE: 08/07/2008 04:32:16 AM
Hi,

If you could postpone the book printing for one or two months and include some features of Seam 2.1 (e.g. GWT, single sign-on support ...), it would benefit all of us.

Regards.

-----

AUTHOR: Andre Eugenio
EMAIL: andre.eugenio@gmail.com
IP: 160.33.66.118
URL:
DATE: 08/07/2008 08:31:53 AM
I have a early version of the e-book and there are some chapters that cover the new features of 2.1.

-----

AUTHOR: Dan Allen
EMAIL: dan.allen@mojavelinux.com
IP: 72.66.112.204
URL: http://www.mojavelinux.com
DATE: 08/08/2008 12:41:41 PM
As @Andre points out, this stuff is already covered in the book. GWT integration has been part of Seam since 2.0 and I just updated the security chapter to cover the identity management and permissions modules. I also am sure to identify throughout the book when something has changed in Seam 2.1.

With that said, the "official" version of Seam that the source code aligns with is 2.0.3.GA.

-----

AUTHOR: Dan Allen
EMAIL: dan.allen@mojavelinux.com
IP: 72.66.112.204
URL: http://www.mojavelinux.com
DATE: 08/09/2008 12:40:37 AM
<p>And the winners are...</p>

<ul><li>Vu Pham</li>
<li>Jason Porter</li>
<li>Padmarag Lokhande</li>
<li>Jignesh Patel</li></ul>

<p>Thanks everyone for participating! I think we had a very successful session and I had lots of fun ranting and raving about Seam and Java EE. I hope you did to.</p>