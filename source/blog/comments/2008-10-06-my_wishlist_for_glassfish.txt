AUTHOR: John Clingan
EMAIL: John.Clingan@Sun.COM
IP: 192.18.43.225
URL: http://blogs.sun.com/jclingan
DATE: 10/06/2008 10:30:34 AM
Thanks for the followup, and we're taking note of your feedback. Also, please note the pre-defined ant tasks available through asant: http://docs.sun.com/app/docs/doc/819-3662/6n5s9hmt5?a=view

John Clingan
GlassFish Group Product Manager

-----

AUTHOR: Peter Williams
EMAIL: pete.williams@sun.com
IP: 66.167.121.91
URL:
DATE: 10/06/2008 11:35:50 AM
Re:  #1 - authentication over SMTP(S) -- See http://forums.java.net/jive/message.jspa?messageID=283824#283824

Or can you be more specific on exactly what you want?

-----

AUTHOR: Dan Allen
EMAIL: dan.allen@mojavelinux.com
IP: 72.66.112.204
URL: http://mojavelinux.com
DATE: 10/06/2008 11:49:58 AM
@Peter, now that you have shown me that code, I recall the exact problem. It requires me to write Java code to inject the username and password into the transport at the time the message is sent. What doesn't work is to initiate a MailSession that has an Authenticator registered so that the client code doesn't not have to know these details. Specifically, this comes up when attempting to use Seam's mail support.

I found a <a href="http://blog.augmentedfragments.com/2008/09/javamail-smtp-authentication.html">blog entry</a> that explains the details. I believe users of Roller bump into the <a href="http://www.nabble.com/Setting-up-JavaMail-datasource-with-SSL,-username-and-password-td18203657s12275.html">same problem</a>.

-----

AUTHOR: Dan Allen
EMAIL: dan.allen@mojavelinux.com
IP: 72.66.112.204
URL: http://mojavelinux.com
DATE: 10/06/2008 11:57:28 AM
@John, asant looks useful for creating and deploying packages, but what I am looking for is somewhere along the lines of what <a href="http://ant.apache.org/ivy/history/latest-milestone/ant.html">Ivy</a>  provides. You import functionality into your Ant build using Ant's  tag and that gives you a new namespace with a collection of task elements. In the beginning, you could just start with  perhaps.

If this feature is already available, I am not sure the documentation explains it right.

-----

AUTHOR: Peter Williams
EMAIL: pete.williams@sun.com
IP: 192.18.43.225
URL:
DATE: 10/06/2008 03:20:27 PM
That's true.  JavaMail never reads passwords from properties and GlassFish mail resources (V2 and earlier) didn't enhance this in any way.

Mail resources are being rewritten for GlassFish V3 and authentication was already my list to support in some reasonable way.  Thanks for blog references, we'll take those into account.

Some comments:

1A) It sounds like you want the mail session resource authenticated a single time and then shared between several sessions for sending email?   Are you concerned with security for the authentication information stored in the resource file (and if so, what steps do you take to secure it?)

1B) Or are you asking for the mail sessions to somehow authenticated per client, but not by the client?  Where do you want or expect the authentication to be performed in this case?

2) I looked up Seam's mail configuration information ( http://docs.jboss.org/seam/2.0.0.GA/reference/en/html/components.html#components.mail ) and while I don't think the JNDI setting will work, the explicit session parameters should work.  Or are you saying even that doesn't work?

-----

AUTHOR: Dan Allen
EMAIL: dan.allen@mojavelinux.com
IP: 72.66.112.204
URL: http://mojavelinux.com
DATE: 10/06/2008 03:44:42 PM
@Peter What I am basically looking for is the same type of arrangement as with JNDI database connections. You pull a resource from JNDI so that the credentials do not appear in the code itself (to me, that is secure).

In this case, yes, the same credentials would be shared by all calls to that MailSession (perhaps because it is a blog sending notifications). This has long been <a href="http://www.atlassian.com/software/jira/docs/latest/smtpconfig.html#N100F4">supported</a> in both JBoss and Tomcat. It works by initiating an Authenticator when the resource is bound and that Authenticator is then consulted at the point the mail is sent. Somewhere inside the application server needs to be code that looks like this:

<pre>authenticator = new Authenticator() {
    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(
            getUsername(),
            getPassword());
    }
};
session = javax.mail.Session.getInstance(properties, authenticator);</pre>

Obviously, the authentication has to happen immediately prior to the send() call or else the mail server is going to reject the message (it has a short timeout).

As you have observed, the explicit session parameters certainly work to configure a mail session, but then the credentials are once again in the project (components.xml). We really need to get those credentials abstracted out into the application server configuration.

-----

AUTHOR: vince
EMAIL: vince.kraemer@sun.com
IP: 64.9.239.16
URL: http://blogs.sun.com/vkraemer
DATE: 10/07/2008 12:49:11 AM
There is a work-around for bone #2...

Use the string "()" to signify an empty string.

I think I have a hint for bone #3...

GF assumes that jar files in an exploded EAR file that contain EJB's are exploded...  Last time I looked, one of the seam jars had EJBs in it, but the jar was not exploded.  YMMV on this one.

I think there is also a hint for bone #4...

Read through the code in this file: http://hg.netbeans.org/main/file/6022c5d694f5/j2ee.sun.appsrv81/src/org/netbeans/modules/j2ee/sun/ide/j2ee/ant-deploy.xml

-----

AUTHOR: Kem Elbrader
EMAIL: kem.elbrader@elementline.com
IP: 66.29.180.146
URL:
DATE: 10/09/2008 12:43:06 PM
We don't use seam-gen but do successfully directory deploy our projects on Glassfish. We use maven to build our seam projects and have it expand wars and ejbs during the build. If you'd like I can send you an example of what we're doing.

-----

AUTHOR: Dan Allen
EMAIL: dan.j.allen@gmail.com
IP: 72.66.112.204
URL: http://mojavelinux.com
DATE: 11/19/2008 06:52:51 PM
@vince, the suggestion about exploding jboss-seam.jar did the trick! I just deployed an explode EAR Seam application to GlassFish for the first time!

-----

AUTHOR: Peter Williams
EMAIL: pete.williams@sun.com
IP: 192.18.43.225
URL:
DATE: 08/18/2009 03:18:49 PM
@Dan - Support for passwords in mail resources is committed to GlassFish V2.1.1 and will be in GlassFish V3 shortly.  See <a href="https://glassfish.dev.java.net/issues/show_bug.cgi?id=5247">https://glassfish.dev.java.net/issues/show_bug.cgi?id=5247</a>

-----

AUTHOR: Dan Allen
EMAIL: dan.allen@mojavelinux.com
IP: 72.152.138.121
URL: http://mojavelinux.com
DATE: 08/18/2009 04:13:06 PM
Awesome! This is great news. I'll be sure to spread the word!