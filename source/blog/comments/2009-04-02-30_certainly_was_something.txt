AUTHOR: Christos Vasilakis
EMAIL: cvasilak@gmail.com
IP: 193.92.234.158
URL: http://cvasilak.blogspot.com
DATE: 04/04/2009 05:59:30 AM
Hi Dan!

I did enjoy meeting you at Devoxx (my first java conference to attend), truly inspirational...

I am deeply amazed and aspired on how deep in the ground you stay despite the take off in your career...

Looking forward for years ahead!

Regards,
Christos

PS Thanks for the photo! :)
http://dl.getdropbox.com/u/155050/dan.allen.jpg

-----

AUTHOR: deman
EMAIL: deman2k@yahoo.com
IP: 202.151.219.147
URL: http://codersifu.blogspot.com
DATE: 04/29/2009 04:13:52 AM
Hi,

Wonderful story. Congratulation!

I'm starting to pick-up Seam. It's something new since mostly before this I'm involved more with ASP.NET and Rails. However, with you book, I believe I can learn it very fast.

Thanks in advance for the knowledge.