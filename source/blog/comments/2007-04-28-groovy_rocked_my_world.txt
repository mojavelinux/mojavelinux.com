AUTHOR: David Sachdev
EMAIL: david@innotac.com
IP: 65.242.47.185
URL:
DATE: 04/30/2007 12:17:44 PM
Well, I will be the first to state my excitement about Java in a Grooovy world!

If you've been developing for a while, when you start a new project one of the questions in the back of your mind is how robust and scalable does this project need to be.  Who is the intended pool of users, and how will that grow over time.

These questions are of course being asked to determine what tools are needed, and how easy it is to scale with those terms.

And unfortunately, the need for scaling has meant that many RAD languages and tools just weren't going to cut it.  Sure, the RAD developer could get something out the door 3 times as quickly as you could - but you get your "I told you so" when shit hits the fan in production, and the only answer for them is migration to another platform or totally unmanageable methods of scaling are proposed.

Well - I for one am excited about a new stack that complements each other well and lets you choose the best for the job at hand, while not closing the door on the future.

Maven + Java + JSF + Seam + Groovy + JPA3.0/Hibernate3.0 - with some GWT sprinkled in there for effect.  Seems like alot, but I promise you with less headaches then ever before.

Oh....and prototypes that can evolve into the real code!

-----

AUTHOR: Sam Doyle
EMAIL: sdoyle_2@yahoo.com
IP: 64.81.53.12
URL:
DATE: 10/21/2007 10:12:45 PM
So Dan, now that are writing a book on Seam how do you feel about Groovy/Grails vs Seam? I'd really be interesting in knowing :)

-----

AUTHOR: Samuel Doyle
EMAIL: sdoyle_2@yahoo.com
IP: 64.81.53.12
URL:
DATE: 10/22/2007 01:07:16 AM
Heh, I just found out Seam 2.0 supports Groovy. So I guess when it comes to my previous question about the comparison, when would you suggest using a Groovy based deployment over a standard Java based Seam one?

S.D.