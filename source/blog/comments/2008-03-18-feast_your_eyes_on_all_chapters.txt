AUTHOR: Thomas Moerman
EMAIL: thomas.moerman@gmail.com
IP: 78.23.85.24
URL:
DATE: 03/18/2008 03:09:52 PM
I have read three quarters of the book by now through the Manning Early Access program and all i can say is this is THE book on Seam i have been waiting for. Incredible depth, fluent narrative, highly recommended.

-----

AUTHOR: Kenneth Mark
EMAIL: kennethmark8888@yahoo.com
IP: 202.175.77.98
URL:
DATE: 03/21/2008 06:56:03 AM
Bought the electronic version few days ago and although I'm still in the middle of it, this book already answered lot of mines questions about Seam. Finally found THE book that will let me understand Seam. I've tried many others and none fulfill what Seam in Action offers.
Great work !

-----

AUTHOR: Jason Porter
EMAIL: lightguard.jp@gmail.com
IP: 63.118.230.7
URL:
DATE: 03/25/2008 04:18:05 PM
Looking over the table of contents, in the rich section do you talk about ICEFaces at all, or are all of the examples in the book based around RichFaces?

-----

AUTHOR: Sebastien Michea
EMAIL: sebastien.michea@manaty.net
IP: 88.162.92.98
URL: http://www.manaty.net
DATE: 03/25/2008 06:46:25 PM
We developed few applications in Seam and Ajax4JSF was such a pain... I'm eager to read chapter 12

-----

AUTHOR: Dan Allen
EMAIL: dan.allen@mojavelinux.com
IP: 216.27.105.99
URL: http://www.mojavelinux.com
DATE: 03/25/2008 09:47:29 PM
Jason,

For the most part, I use RichFaces in the examples simply to choose something other than the standard JSF components. Regardless of which component palette you choose, Seam works exactly the same since it honors the JSF component life cycle.

Only in chapter 12 is there really a focus on specific features of RichFaces/Ajax4jsf. I would very much like to include a couple of examples of ICEFaces in that chapter, but I am bursting at the seams as far as page count goes, so it is going to be tricky (I will still try, though).

As a concession, one way or the other, the example code will include both a RichFaces and ICEFaces version of the application.

-----

AUTHOR: Jason Porter
EMAIL: lightguard.jp@gmail.com
IP: 63.118.230.7
URL:
DATE: 03/26/2008 11:11:50 AM
@Dan:

Excellent!  I understand the page limit, and I'm very happy with the above mentioned concession if that's all that makes it into the final product.  Thanks so much for getting back and responding.

-----

AUTHOR: rk
EMAIL: raja_kill@yahoo.com
IP: 218.186.12.11
URL:
DATE: 03/30/2008 02:20:28 AM
excellent piece, really helping me get started quickly with Seam.

RK
http://www.RentalAndRealEstate.com
http://theindiastockmarket.blogspot.com/