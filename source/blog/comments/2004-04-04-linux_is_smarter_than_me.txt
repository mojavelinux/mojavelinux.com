AUTHOR: Jason
EMAIL: jrust@rustyparts.com
IP: 12.107.12.130
URL: http://rustyparts.com/blip
DATE: 07/23/2004 05:21:53 PM
Fascinating...just who is this braniac friend of yours?

-----

AUTHOR: Dan Allen
EMAIL: dan.allen@mojavelinux.com
IP: 68.55.51.191
URL: http://www.mojavelinux.com
DATE: 07/24/2004 01:47:07 PM
Well, he just let me know that it was daylight savings time.  I then figured out that if the clock had to move forward an hour, then it meant that an hour had to be skipped.

What facinated me was that the calendar program was smart enough to recognize that exact hour as being invalid for that particular time zone.  It just goes to show the detail that goes into to these programs.