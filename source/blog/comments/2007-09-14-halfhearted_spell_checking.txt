AUTHOR: Jason
EMAIL: jason@rustedcode.com
IP: 71.231.204.231
URL: http://rustedcode.com
DATE: 09/14/2007 12:53:27 PM
Umm, you sure about that?  I just do a fat-right-click on the word and walaa there are my suggestions!

-----

AUTHOR: emacsen
EMAIL: emacsen@emacsen.net
IP: 138.88.89.40
URL:
DATE: 09/14/2007 08:11:33 PM
Works for me. Word is underlined in red, right click and you get a context sensitive menu that includes spelling suggestions (if there are any).