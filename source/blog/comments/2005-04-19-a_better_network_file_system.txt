AUTHOR: Dan Allen
EMAIL: dan@mojavelinux.com
IP: 68.50.139.221
URL: http://www.mojavelinux.com
DATE: 04/19/2005 12:27:53 AM
I just tripped over another project, <a href="http://lufs.sourceforge.net/lufs/intro.html">lufs</a>, that does something similar with virtual filesystems, except it seems to focus on a wider subset of protocols.

-----

AUTHOR: Dan Allen
EMAIL: dan@mojavelinux.com
IP: 68.50.139.221
URL: http://www.mojavelinux.com
DATE: 04/19/2005 02:28:06 AM
Just tripped over another great utility, <a href="http://www.lysator.liu.se/fsh/">fsh</a>.  fsh is a drop in replacement for ssh when used to execute remote commands.  The only difference is that it will reuse a secure tunnel once it has been established.  This is excellent for repetitive rsh use such as CVS over ssh.

-----

AUTHOR: Dan Allen
EMAIL: dan@mojavelinux.com
IP: 206.182.187.37
URL: http://www.mojavelinux.com
DATE: 04/20/2005 01:48:58 PM
I just discovered that fsh does have to be installed on both the client and the server to work properly.

-----

AUTHOR: Dan Allen
EMAIL: dan@mojavelinux.com
IP: 68.50.139.221
URL: http://www.mojavelinux.com
DATE: 04/23/2005 12:45:37 AM
Oh, and one other tip.  Unless you have been living under a rock for the past year, you should have heard about the <a href="http://docs.kde.org/en/3.3/kdebase/kioslave/fish.html">fish ioslave</a> for KDE.  It is built using the sftp protocol, but also leverages remote execution of commands via ssh to increase efficiency and transparency.

For instance, opening a file using a KDE program via the fish:// protocol allows you to read and write that file as if it were   on the local filesystem.  If the file is opened using a non-KDE app, KDE will manage that file and upload it back to the server when the application terminates.  KDE is very powerful like that.

-----

AUTHOR: Tommy Li
EMAIL: realitymage@adelphia.net
IP: 69.172.230.198
URL: http://tommy.impulsestorm.com
DATE: 05/28/2005 10:03:00 PM
FISH is different from SFTP. It uses only SSH (compatible with SSHv1). SFTP uses the sftp subsystem of SSH2.

KDE has it's own sftp ioslave. I use sftp.

-----

AUTHOR: Dan Allen
EMAIL: dan@mojavelinux.com
IP: 65.199.11.141
URL: http://www.mojavelinux.com
DATE: 06/15/2005 02:56:45 PM
Thanks for the clarification Tommy Li!  I was wondering what the main differences between FISH and SFTP were.  I also heard the FISH uses perl if it is available on the remote system to perform more efficient operations.

Does anyone have a link to a detailed breakdown of the two implementations?

-----

AUTHOR: Dan Allen
EMAIL: dan.j.allen@gmail.com
IP: 68.50.139.221
URL: http://www.mojavelinux.com
DATE: 05/23/2006 11:19:55 PM
I was confused by the similarity in names, but it appears that <a href="http://fuse.sourceforge.net/sshfs.html">SSHFS</a> is actually a distinct project that accomplishes the same task as shfs.  It appears to be inspired by the aforementioned lufs.  The author makes the following claim: <blockquote>There were some limitations of that codebase, so I rewrote it.</blockquote>

SSHFS boasts the following features:
<ol>
<li>Based on FUSE (the best userspace filesystem framework for linux ;-)</li>
<li>Multithreading: more than one request can be on it's way to the server</li>
<li>Allowing large reads (max 64k)</li>
<li>Caching directory contents</li>
</ol>

-----

AUTHOR: dyssident
EMAIL: none@gmail.com
IP: 24.17.241.183
URL:
DATE: 08/31/2006 04:19:00 AM
Ive had great luck with OpenSSH to share files, SSHFS to mount and Avahi for easy host naming.

Here is a tutorial I did covering something similar with FTP: http://www.ubuntuforums.org/showthread.php?t=218630

-----

AUTHOR: SLX
EMAIL: no_mail@email.pl
IP: 82.143.155.11
URL:
DATE: 08/23/2009 08:38:59 AM
Network File System is something other than sharing files between computers. NFS can stream files. You can watch films, music, etc. stored on remote machine, without first downloading them onto your computer.

-----

AUTHOR: sm0ke
EMAIL: msx@archlinux.us
IP: 190.194.115.239
URL:
DATE: 06/02/2012 03:04:17 AM
Hi Dan,
Your post is rather all by now, did you tried lately NFS4? It isn't the panacea for network data-transport but it works quite well the most of time and if you partner it with AUTOFS you have a great tool in your hands.
From my experience SSHFS is only useful as long as you need an encrypted tunnel between two machines but's impractical for mundane data flow tasks -cp, mv, stream, etc.- because the _excesive_ overhead it adds to the local computer as it's needs a lot of CPU to run it.
Anyways, NFS4 is good enough for a small cluster of machines because the way it's designed to work, for a larger amount of boxes (+20) and for tigher security Kerberos would be a better solution.
Regards!