---
author: Dan Allen
title: Clipboard Management that Works
slug: clipboard_management_that_works
status: Publish
breaks: true
categories: [Linux]
date: 2005-05-07 16:30:22
keywords: [clipboard, xclipboard, KDE, mouse selection]
---
It is incredible how few Linux users really appreciate the power of the X clipboard.  Most people get tripped up using clipboard management in Linux trying to do things the Windows way.  By default, the X clipboard captures the current mouse selection rather than waiting for a Ctrl-C command.  Because of this feature, most people end up losing their selection prematurely.  Another hurdle is that the selection is dropped when the application that created it is terminated.

After hearing a rant on LugRadio about broken clipboard management in Linux, I wrote in with the following <a href="http://forums.lugradio.org/viewtopic.php?t=648">response</a>:

<blockquote><div>I believe that the concern is well founded. Clipboard management in Linux is totally botched. The problem is that once the application holding the clipboard contents quits, the content is flushed from the primary buffer. However, if you are running a super-daemon, such as KDE's <a href="http://docs.kde.org/en/HEAD/kdebase/klipper/">Klipper</a>, which is just <strong>brilliant</strong> by the way, the clipboard content stays alive even after the death of the application. In fact, it can keep up to 25 of the most recent selections. If you have never taken advantage of this feature in Klipper, you are really missing out!</div></blockquote>

<img src="/assets/pics/klipper.png" alt="Klipper" class="avatar" />Regardless of which desktop you choose, you should definitely give Klipper a try.  Klipper allows you to keep several selections in memory which you can then use to toggle with the primary buffer.  There are several other clipboard implementations, but they all pale in comparison to Klipper.  In a followup to my original post I highlighted a few reasons why it is better:

<ol>
<li>Klipper is simply good at what it does. It can pull from both the xselection and the copy buffer and it does a very good job at keeping the history ordered properly and in sync.</li>
<li>Klipper is compliant with the freedesktop.org standard for dock applets.</li>
<li>Klipper can retain clipboard contents between logins using KDE's session management.</li>
<li>Klipper can perform actions on the clipboard contents (though I don't really use this feature).</li>
<li>Klipper is accessible through dcop.</li>
<li>Klipper can popup anywhere, not just out of the docklet icon.</li>
</ol>
<em>One feature that I really wish Klipper had is the ability to "freeze" an item.  This would be immensly helpful for storing usernames, passwords or form data temporarily.</em>

If you still aren't convinced as to why we need a clipboard manager, try one for a few days and then attempt to go back. Nothing sucks worse then when you select something into your xselection, then accidently select something else and then are surprised when you go to paste that email you copied and get an endline instead.  I do realize the Klipper is essentially a hack, since it acts as a vacuum cleaner to suck up all selections wherever it finds them.  Perhaps a better solution should be developed in the future. But, until there is an alternative, you are going to have to pry Klipper from my dead body if you want to take it away from me, because Linux is unusable without it. Even now that I run Gnome, I still have Klipper kicking in my panel.
