---
author: Dan Allen
title: GnuPG and Beer
slug: gnupg_and_beer
status: Publish
breaks: true
categories: [Technology, Open Source]
date: 2005-10-04 21:44:05
keywords: [PGP, GnuPG, security, signature, trust, email]
---
These two topics, <a href="http://www.gnupg.org">GnuPG</a> and beer, may, at first glance, appear to be entirely unrelated.  However, once you become acquainted with the purpose of a keyring party, you will realize that they have everything to do with one another.

Last week, I got together with one of the Debian Java packager maintainers and all around open source advocate, <a href="http://www.yepthatsme.com">Barry Hawkins</a>, at my favorite <a href="http://www.duclaw.com">watering hole</a>.  We had such an awesome time that we almost stayed long enough close down the place.  So what does all this have to do with GnuPG?

Ah, glad you asked, because I almost got off topic.  But hold on.  You first need to know just a smidgen about <a href="http://en.wikipedia.org/wiki/PGP">PGP</a> before I tell you.  Hate SPAM?  Of course you do.  Well, there has been a solution for oh...about 15 years!  It's called <em>Pretty Good Privacy</em>, or PGP for short.  PGP is essentially a way for two people to exchange documents or text (including email) that are either signed or encoded in such a way that they cannot be forged or even opened by anyone else.  Each person generates his or her own secret key and then publishes a public key that can be used to verify the contents of the message.  <strong>So in order to trust a public key, you must have beer?</strong>  No, of course not!  But, you should, at least, meet in person and trade signatures so that you can later crosscheck it against the public key of the other person.  Once that is done, the key is considered "trusted" and you can send and receive messages with confidence (and start junking the rest).

If the US government had any sense, it would have used all that money accumulated for the CAN-SPAM initiative and advocated GnuPG.  GnuPG is the open source implementation of the PGP idea.  I, myself, have been intending on getting my keys setup for nearly 2 years now, but I have procrastinated in the worst way.  I am proud to say this is no longer the case.  If you are still one of the ones holding out, you have no more excuses!  The <a href="http://enigmail.mozdev.org">Enigmail plugin</a> for Thunderbird makes setting up and sharing GnuPG keys and absolute cinch!  In fact, I almost feel like it is too easy.

When you are deciding what to get people for the next holiday season, give them your public PGP key, your trust, and perhaps buy them a beer.
