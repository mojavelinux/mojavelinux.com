---
author: Dan Allen
title: Deploying a seam-gen project to GlassFish
slug: deploying_a_seamgen_project_to_glassfish
status: Publish
breaks: true
categories: [Java]
date: 2008-10-03 03:45:00
keywords: [GlassFish, asadmin, scripting, automation, seam-gen, Seam in Action, Ant]
---
seam-gen creates projects that deploy to JBoss AS out of the box. However, a Seam application can run on any Java EE application server or servlet container with the proper packaging. I have created a page on the <a href="http://code.google.com/p/seaminaction/wiki/DeployingToGlassFish">Seam in Action wiki</a> that provides instructions for how to modify the build script of a seam-gen WAR project to support deployment to <a href="https://glassfish.dev.java.net/">GlassFish</a> (in addition to JBoss AS), as I promised I would do in <a href="http://mojavelinux.com/seaminaction">Seam in Action</a>. Unfortunately, the configuration on applies to WAR projects at this time, though it's certainly possible to deploy seam-gen EAR projects to GlassFish. It's just a matter of putting the files in the right place and I haven't yet committed the time to finding the right build configuration. Keep an eye out for updates as I will likely update the instructions when I nail it down.

Before you run off to discover how to modify your build, I want to mention a couple of things about GlassFish. It's fairly well-known amongst my colleagues and readers of my book that I like GlassFish, and I have cited those reasons in the appendix of Seam in Action. But there is a specific reason that I like GlassFish as it pertains to the build configuration I set forth on the wiki page.

GlassFish has a very sexy administrative console, but it also has a very sexy commandline tool known as asadmin. The asadmin tool gives you virtually unbounded control over the application server, including core tasks such as starting and stopping the application server, deploying and undeploying applications, and setting up database connection pools, amidst a plethora of other controls. You'll see that my modified seam-gen tool takes advantage of a handful of these commands. I encourage you to execute the help command to find out what else asadmin is capable of doing.

When choosing software, there is one thing that you should demand as a developer: efficiency. GlassFish gives you efficiency through automation, which is undoubtedly the most effective way to become efficient. As soon as you have to open up a configuration file, know that you are wasting time. Manual processes are not only slow, though. They are non-reproducible. As the wise authors of <a href="http://www.pragprog.com/the-pragmatic-programmer">The Pragmatic Programmer</a> advise, if you have to do something twice, script it. GlassFish volunteers itself to participate in a script and is the reason why I choose it as my preferred application server.
