---
author: Dan Allen
title: Wireless Grab-bagging
slug: wireless_grabbagging
status: Publish
breaks: false
categories: [Technology]
date: 2004-05-15 19:54:53
keywords: [wireless grab-bagging, wireless, wifi, hotspots, mobile, linux savvy]
---
<p>This morning I had to take my car in for service and an oil change, an ordeal which usually lasts just long enough for boredom to set in; so, I went prepared.  I collected together my laptop, two books and a magazine and set out for the dealership.</p>

<p>As soon as I handed over my keys, I immediately cracked open my laptop to determine how much time I had left before I would have to tether myself to the wall for power.  While contemplating life without the internet (a very painful thought indeed), an idea crossed my mind, "I wonder if I can get a wireless connection in here?"  "Do car dealerships even have wireless connections?"  Before I could think twice, I was already at the console issuing the command:</p>

<code>/sbin/iwlist scanning</code>

<p>Sure enough, two cells!  I could already feel the wireless signal running through my veins.  I checked the listed entries and sure enough, there was one reading "Encryption key:off".  I simply updated my network scripts with the new ESSID, issued an <code>/sbin/ifup eth1</code>, and I was pinging google.com!  I felt like I had just won the lottery, even if it was only a scratchoff ticket from 7-11.</p>

<p>I present to you the newest fad in tech culture. Remember, you heard it here first, folks!</p>
<dl>
  <dt>wireless grab-bagging</dt>
  <dd>The insidious art of fishing for a wireless connection in an unplanned or unexpected location.</dd>
</dl>
<p>It is sort of like an adventure, and the treasure is in finding an exposed connection.  Of course, it comes with a nice little perk in that you can waste away your time browsing weblogs like this from virtually anywhere, even when you least expect it, from a car dealership. ;)</p>
