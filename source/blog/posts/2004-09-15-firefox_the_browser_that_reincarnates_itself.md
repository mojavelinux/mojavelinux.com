---
author: Dan Allen
title: Firefox: The Browser that Reincarnates Itself
slug: firefox_the_browser_that_reincarnates_itself
status: Publish
breaks: true
categories: [Open Source]
date: 2004-09-15 16:40:12
keywords: [firefox, browser, phoenix, firebird, mozilla, innovation]
---
Even though Mozilla's second coming no longer sports the emblem of a <a href="http://en.wikipedia.org/wiki/Phoenix">legendary bird</a>, it still soars above the crowd.  (<em>The brower's original name, Phoenix, was actually quite fitting since the browser, like its mythical namesake, would periodically burn to ashes and be <a href="http://en.wikipedia.org/wiki/Mozilla_Firefox#History">reborn</a> again.</em>)  Just when you thought that this browser had nothing more it could possibly offer, the Firefox team has surprised us again!  I spent part of the morning previewing the release candidate for version 1.0 and I love what I see.  I will highlight a few of exciting new features...

<strong>Live Bookmarks</strong>:  This new integrated feature automatically detects when a website offers an RSS feed, prompting the user with an orange icon in the lower right-hand corner.  You may click on this icon and "subscribe" to the corresponding news feed.  Doing this will add the page to your bookmarks as a folder, with pseudo entries for each of the latest posts.  While it is not a full blown aggregator solution, it is definitely a good start to a great idea.

<strong>Type-Ahead Find Toolbar</strong>: Up until now, type-ahead find has been for the ubergeek only, since it required the brain to paint a picture around its functionality.  The paint is now on the canvas!  This toolbar squeezes in above the statusbar when type-ahead find is activated and gives the user full control over the process, including an input field, buttons for "find next" and "find previous", a "highlight" button for showing matches, and a color indicator when a word is entered that cannot be found.  Type-ahead find is now a killer feature that will be often duplicated.

<strong>Information Bar</strong>: Why block popups from webpages and then annoy the user with unnecessary message boxes?  That is why Firefox has embraced the information bar concept, which uses a thin bar that slides into view when something interesting has happened, such as a blocked resource.  Differing from its ancestor, the status bar, an information bar is interactive, allowing the user to take furthur action if necessary, or to just ignore it all together.  I see a lot of growth in this idea.

<strong>Better Defaults</strong>: The essence of a great program is good defaults.  While spending time tweaking the heck out of something is an enjoyable way to piss away an afternoon, most users just don't have that luxury.  Seeing that five major search engines were added to the Search Box is a perfect example of what needs to be done to achieve this goal.

<strong>Mail Integration</strong>: After installing Firefox for a friend on Windows, I noticed that under the <em>Tools</em> menu, there were two additional entries pertaining to e-mail.  One of the entries even showed how many new messages were available.  I am not even sure what mail client my friend uses, but obviously Firefox does.  Again, this is a great default integration to have for attracting the newer crowd.

Firefox has many other excellent features, many of which have been covered time and again by articles.  By no means is this entry a comprehensive list of what Firefox has to offer.  My point is that innovation is proceeding at a rapid pace.  Instead of having dessert for dinner, go <a href="http://spreadfirefox.com/community/?q=affiliates&id=0&t=1">download</a> Firefox.  It will be a heck of a lot more fun, it will help a good <a href="http://www.spreadfirefox.com/community/">cause</a>, and it costs exactly 0 carbs!

Oh, and if you would like to piss away an afternoon, you can start by browsing my <a href="http://mojavelinux.com/wiki/doku.php?id=tips:firefox">favorite extensions</a>.
