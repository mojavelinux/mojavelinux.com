---
author: Dan Allen
title: A concise and eloquent look at Seam
slug: a_concise_and_eloquent_look_at_seam
status: Publish
breaks: false
categories: [Seam News, Seam, Seam in Action]
date: 2010-03-30 01:05:48
keywords: [Seam, overview, Seam in Action, contextual components]
---
<p>Despite all that I have written, explained and presented about <a  href="http://seamframework.org">Seam</a>, I often find myself struggling to sum it up in a few short breaths. Fortunately, Matt Campbell does an superb job of defining the essence of what Seam provides eloquently and concisely in his blog series <em>An Honest Look at Seam</em>. (And I'm not just saying that because he credits <a href="http://mojavelinux.com/seaminaction">Seam in Action</a> as being his guide in his exploration of Seam).</p>

<ul>
   <li><a href="http://blogs.citytechinc.com/mcampbell/?p=150">An Honest Look at Seam - Part 1: How Seam builds on Spring</a></li>
   <li><a href="http://blogs.citytechinc.com/mcampbell/?p=165">An Honest Look at Seam - Part 2: The Conversation</a></li>
   <li><a href="http://blogs.citytechinc.com/mcampbell/?p=176">An Honest Look at Seam - Part 3: Learning Seam</a></li>
</ul>

<p>In part 1, Matt explains how the attention to scoping of components is what sets Seam apart from Spring and makes it more suited for the web environment. In my talks on Java EE 6, I often say that JSR-299 (CDI) considers the scope of a bean (where it's stored) to be just as important as the component instance itself. Speaking of Java EE 6, Matt does some comparisons of his own between Seam and CDI.</p>

<p>Having established the importance of context, Matt opens part 2 introducing the conversation scope. He quickly delves into the symbiotic relationship between this scope, the persistence context and the multi-request use case (which is just about any use case on the web). He raises the ever important issue of manual flush mode in Hibernate and how it enables use of an optimistic transaction.</p>

<p>Matt takes a break from the theory in part 3 to address the developer's first experience with Seam. He calms the anxiety a typical newcomer might have the first time the developer observes seam-gen churn out application. While some may appreciate the huge boast that a fully-functional application provides, the shear number of artifacts is daunting for those expecting "Hello World". But as Matt clarifies,</p>

<blockquote>There is a lot to Seam, but not becuase Seam itself is vastly huge and complex, but because Seam integrates so many things together.</blockquote>

<p>So take your time and explore it all. Use what parts you need and skip the parts you don't.</p>

<p>In the future, rather than struggling to find the words to describe Seam on a trip in an elevator, I'm just going to hand the interested listener a card with the URL to these blogs on it ;)</p>

<p>It's important to zero in on what Seam 2 provides, especially as we look ahead to Java EE 6 and the development of the Seam 3 portable extension library. So regardless of where you are in your adoption of Seam 2 or Java EE 6, take a moment to read through these entries.</p>
