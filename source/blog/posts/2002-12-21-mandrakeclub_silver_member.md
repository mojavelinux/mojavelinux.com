---
author: Dan Allen
title: MandrakeClub Silver Member
slug: mandrakeclub_silver_member
status: Publish
breaks: true
categories: [Linux]
date: 2002-12-21 10:50:00
keywords: [Mandrake, MandrakeClub, membership, subscription, linux]
---
That's right, you are at the homesite of a proud silver member of mandrake club. I have been meaning to join for quite some time, but after the wedding an all we were a little short on cash.

As you may or may not know, Mandrake is certainly hurting and really needs members if they are going to be able to stay afloat. If you use Linux Mandrake or you know someone who does, tell them to <a href="http://www.mandrakeclub.com">join</a>! Mandrake offers an amazing product and even though they don't get a lot of the commercial support that RedHat recieves doesn't mean they don't deserve it. Show your thanks for a good cause and get benefits at the same time! <a href="http://www.mandrakeclub.com">join</a>, <a href="http://www.mandrakeclub.com">join</a>, <a href="http://www.mandrakeclub.com">join</a>!
