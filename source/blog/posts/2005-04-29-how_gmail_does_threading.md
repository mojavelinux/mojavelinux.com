---
author: Dan Allen
title: How Gmail Does Threading
slug: how_gmail_does_threading
status: Publish
breaks: true
categories: [Technology]
date: 2005-04-29 23:07:21
keywords: [gmail, email, webmail, threading, conversation view]
---
A lot of folks have focused on the storage capacity when talking about one of Google's recent initiatives, <a href="http://gmail.google.com" style=" padding: 0; border: 0;"><img src="/assets/gfx/gmail.gif" alt="Gmail" style="height: 12px; width: 56px;" /></a>.  However, apart from the initial shock factor, the size offering is really not what Gmail is all about.  What I find the most interesting is the way that Gmail handles <a href="http://taint.org/wk/GmailThreadingDetails">threading</a> with its <a href="http://gmail.google.com/gmail/help/images/inbox2.gif">conversation view</a>.

When Google allowed one of it's developer's 20% projects to be rolled out to the public, it did so in a style that one would expect from one of the most creative companies in the world.  Gmail revolutionizes the way that e-mail is presented to the user by making use of conversations.

In the conversation view, all related messages appear as a stack of cards, which can be thumbed through by clicking on the exposed portion of each message.  An important element, which I think people fail to pick up on <em>(probably because it is so intuitive that one quickly takes it for granted)</em>, is that the conversation view not only includes messages that have arrived, but it also ties in the related messages which <u>you</u> have sent.  Including these messages allows the entire context of the conversation to be captured between the various parties. While some people have claimed that this feature is in fact just threading, it is most certainly <strong>more</strong> than threading.  It is threading as if your email were a public mailinglist and every message on both sides were included in the thread.

Another nice aspect of conversation view is that groups of messages that have been archived jump back into the inbox when a new related message arrives (or is sent out).  If you have ever used threading in an e-mail client you know that, well, messages can get missed if the sorting is not configured just right (or if it is even possible to bump threads in the client).  So Gmail really makes it easy to stay on top of your active messages.

Finally, Google helps to cut down on scrolling when working with conversations by hiding all quoted text that is not part of the message reply.  After all, why would you need the quoted text since you already holding the entire conversation stack on the very same view?

Gmail is certainly worth checking out for the conversation view feature alone.  My hope is that this method of organizing messages makes its way into desktop clients such as <a href="http://www.mozilla.org/products/thunderbird">Mozilla Thunderbird</a>.
