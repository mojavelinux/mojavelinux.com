---
author: Dan Allen
title: Fixing Maryland Traffic
slug: fixing_maryland_traffic
status: Publish
breaks: true
categories: [General]
date: 2006-06-04 17:48:10
keywords: [traffic, metro, district 21, route one]
---
A while back I received a postcard in the mail asking how I would solve the Maryland Route One traffic problems.  Given that all I seem to complain about is the traffic in this area, I felt obliged to respond.  Below is the letter I wrote to the task force.  Hopefully my ideas will align with other voters in the area and we can start to see some motion in the battle to solve this serious issue.

<blockquote><p>Members of the Route One Task Force,</p>
<p>I want to start off by relating how pleasantly surprised I was to receive the "Route One Task Force" postcard in my mailbox and to know that an initiative to improve the situation is underway.  While there are always many problems to tackle, there is no question that traffic in this area is the most significant in the minds of the metro-area working force.  Discussions of traffic congestion take center stage in discussions at every social event I attend and the level of frustrations are always high.</p>
<p>For over two years, I rode the metro/MARC train to my job in the district.  Riding the metro allowed me to save money on gas, avoid accidents, rest on the way home from work, and be productive when I found myself in a pinch.  However, when I took a new job in Bethesda, riding the metro was no longer an option.</p>
<p>Each morning I fight through 495W into Montgomery County.  Doing so makes me tired, irritable, wastes gas (not to mention the emissions), and puts me in danger of having an accident.  If it were available, I wouldn't even think twice about riding the metro.  However, there is no way I am going to take the green line all the way into downtown D.C. just to get on the red line out to Bethesda.  It would simply take too long.  There isn't a person I talk to that opposes the idea of a purple line.  In fact, many conversations I have had concluded that it was a huge oversight not to build it originally.</p>
<p>Having grown up in the area, I am very well aware of the history of the Intercounty Connector (ICC).  It certainly isn't an awful idea as there is currently no direct way to get over to I-270 from the Laurel area.  I lost track a long time ago the number of times I have passed by an ICC sign while meandering back and forth across the middle Maryland area.  However, as ideal as it might sound, mass transit is a much better solution in the long run.  The fact that the D.C. metro can only take you to one of the three airports in the area is flawed.  Additionally, one has to question why the metro only has two tracks, limiting the number of trains it can carry at one time.</p>
<p>I strong encourage you to continue to find ways to solve the traffic problems in the area by reducing the number of vehicles on the roads.</p></blockquote>
