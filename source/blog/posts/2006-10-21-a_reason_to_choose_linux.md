---
author: Dan Allen
title: A Reason to Choose Linux
slug: a_reason_to_choose_linux
status: Publish
breaks: true
categories: [Linux, Open Source]
date: 2006-10-21 18:29:05
keywords: [screensaver, xscreensaver, linux]
---
<img src="/assets/pics/glblur.jpg" class="avatar" style="margin-left: 4px;" width="200" height="150" alt="GLBlur" /> If for no other reason, choose Linux for its screensavers.  I have received more comments about the screensavers on Linux than any other application.  You get particularly strong reactions if you have the OpenGL animations in the rotation.  It never fails that while talking to someone at my desk, I am interrupted to find them memorized by the abstract forms fluctuating on my screen.  A couple of them can really send people into a trance.

The screensavers never cease to amaze me.  I get so sick of the bouncing line animation that is so common on the Windows platform.  By default, the screensavers on Linux are choosen at random.  I prefer this setup so that each time I return to my desk, I am refreshed to find a different piece of artwork awaiting me.

The screensaver package, <a href="http://www.jwz.org/xscreensaver">xscreensaver</a> to be specific, is a collection of free screen savers for X11 (and Mac).  The screensavers are written and maintained by Jamie Zawinski, as well as many others.  Rumor has it that the motivation behind the screensaver designs is to make the kiosks in the DNA Lounge fit the club mood.

Of course, the most popular screensaver of all might be the 3D version of "The Matrix" terminal, known best for cryptic green characters scrolling downwards on a phosphorus screen.  Install Linux today or find someone with a Linux desktop to get memorized.
