---
author: Dan Allen
title: Why you didn't know the Unified EL is being updated
slug: why_you_didnt_know_the_unified_el_is_being_updated
status: Publish
breaks: false
categories: [Java]
date: 2009-08-02 01:46:24
keywords: [Unified EL, JSR-245 MR2, 2.1.2, Java EE, JSP, JSF, specification]
---
<p>Information about the proposed Unified EL update slated for Java EE 6 is hard to come by, so I decided to put together a blog entry with some useful links.</p>

<p>If you haven't heard yet, which is understandable given the lack of publicized information, the Unified EL is getting an update for Java EE 6. The update consists of functionality we have all wanted for a long time. Most notably, <strong>literal method names</strong> in value expressions for more flexible data retrieval and <strong>method arguments</strong> for both value and method expressions. To see examples of these feature enhancements, take a look at <a href="http://blogs.sun.com/kchung/entry/jsr_245_mr_part_i">this blog entry</a> by Kim-Man Chung. (If you are using Seam or Spring Web Flow, you are already enjoying these enhancements courtesy of the JBoss EL).</p>

<p>But you probably don't just want to take my word for it. You want to know where these enhancements are officially documented. That's the point of this entry :)</p>

<p>Let's start by looking at the big picture. The Unified EL is defined in Part II of the JSR 245: JavaServer Pages 2.1 specification, lead by Kim-Man Chung. The <a href="http://jcp.org/aboutJava/communityprocess/maintenance/jsr245/index2.html">maintainence release 2 of JSR 245</a> is the one on the ballot for Java EE 6.</p>

<p>I'll pause in anticipation of your pressing question. No, there is no dedicated specification for the Unified EL. (I'm lobbying for one, though). The Unified EL evolved out of JSP (where it was just the EL) and was later adopted by JSF. It's just as much a part of JSF as it is a part of JSP, which is why the situation is so awkward right now.</p>

<p>To find the proposed changes for the Unified EL, you have to look at the <a href="http://jcp.org/aboutJava/communityprocess/maintenance/jsr245/245ChangeLog2.html">JSR 245 MR2 Change Log</a>. Specifically, focus your attention on the <a href="http://jcp.org/aboutJava/communityprocess/maintenance/jsr245/245-MR2_2.html">Expression Language page</a>. Believe it not, that's as official as it gets. Trust me, <em>the absence of process disturbs me too</em>.</p>

<p>Great, so what about the implementation? Can you try this stuff out? I had some difficulty finding that information too, but alas, I have more links for you.</p>

<p>The reference implementation of the Unified EL is hosted at java.net. The project is named <a href="https://uel.dev.java.net">uel</a>, short for <u>U</u>nified <u>EL</u>. There are several <a href="https://uel.dev.java.net/source/browse/uel/tags">tags</a> corresponding to beta releases in the Subversion repository and the artifacts (JARs) for most of the beta releases have been published to the java.net Maven repository (<a href="http://download.java.net/maven/glassfish/javax/el/el-api">EL api</a>, <a href="http://download.java.net/maven/glassfish/org/glassfish/web/el-impl">EL impl</a>). However, I can't find any announcements about these releases.</p>

<p>The version of the Unified EL you are looking for is <strong>2.1.2</strong>. "How did the version jump from 1.1 to 2.1.2?" you may ask. I happen to know the answer to this question because I was in the EG meeting at JavaOne in which Kim-Man Chung gave a status update of the JSR 245 MR2 spec. He said that the version was bumped to align it with the JSP version, which will also be 2.1.2. Given that the Unified EL should be it's own spec, this makes absolutely no sense, but at least you know the reasoning behind it (and don't think you missed a haf-dozen versions in between). I'm also not happy at all about the closed-door decision, but I digress.</p>

<p>At some point, the Unified EL update will show up in GlassFish V3. As far as I can tell, it is not bundled today (but could be by the time you read this). How can you tell if it's there or not? The EL API can be found in the glassfish/modules/javax.servlet.jsp.jar file and the implementation is glassfish/modules/web/jsf-impl.jar. Extract the JARs and look at the META-INF/MANIFEST.MF or the pom.xml file to determine which version it is. You could also look for the presence of the <span style="font-family: monospace;">javax.el.ValueReference</span>, which is a new class in the API.</p>

<p><em>In case you were wondering, JBoss EL 2.0 is not (yet) and implementation of the Unified EL 2.1.2 API. Stay tuned.</em></p>

<p>So there you have it, information about the Unified EL update. I'm sorry this information is so hard to piece together. I think this would all be a lot simpler if EL was its own spec and/or there was decent information published about JSP. Oh, if the spec were actually open. The thing is, I don't give a care about JSP and frankly I want to divorce that spec in every way.</p>

<p><strong>UPDATE:</strong> I put together a <a href="http://seamframework.org/Documentation/EL21">formal list</a> of proposed enhancements requested by the Seam community on seamframework.org .</p>

<p><strong>UPDATE:</strong> Ryan Lubke also <a href="http://blogs.sun.com/rlubke/entry/unified_expression_language_is_and">blogged</a> about the Unified EL update for Java EE 6. He included tips on how to update GlassFish with the latest EL build.</p>

<p><strong>UPDATE:</strong> The EL spec document has been updated, making the changelog I cited above official. Grab the EL spec document from the "for evaluation" link on the JSR-245 maintenance release <a href="http://jcp.org/aboutJava/communityprocess/mrel/jsr245/index.html">download page</a>.
