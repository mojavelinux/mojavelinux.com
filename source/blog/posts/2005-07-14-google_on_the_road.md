---
author: Dan Allen
title: Google on the Road
slug: google_on_the_road
status: Publish
breaks: true
categories: [Technology]
date: 2005-07-14 09:46:07
keywords: [Google, search, text message, SMS]
---
Google is a tremendous search engine and a huge timesaver.  Besides indexed searches, it provides maps, directions, weather, stock quotes, definitions, etc.  However, all of this information requires you to be sitting in front of a networked computer...or so you thought!  If you are a total gearhead you might be shouting by now "mobile web!"  Well, not all of us have such deep pockets and fancy phones.  For the rest of us, there happens to be another way.  Google has had in beta for some time now an <a href="http://www.google.com/sms/">SMS interface</a>.  To put it in laymen's terms, you can Google using <strong>text messaging</strong>!

<strong>Update:</strong>  Google just added an <a href="http://www.google.com/sms/demo.html">SMS demo site</a>, so you can practice without having to waste any text messages.

Okay, so how might you find this useful?  Let's look at a case study.  This morning, while running out the door, I completely forgot to lookup directions to the repair shop where I was going to be dropping off my car.  I had two options, either turn back around and go home, or Google It!  I opted for option B (of course).

Before I could lookup the directions, I had to locate the address.  So, I whipped out my phone, started a new text message to 46645 (GOOGL), and entered the name of the shop along with the closest zip code.  A few seconds later, I had a text message with the full address and phone number of the shop.  Next, I needed to get directions, since I didn't recognize the name of the road in the address.  I started a new text message, this time entering my home address and the address of the repair shop.  Again, a few seconds later, my phone alerted me of 1, then 2, then 3, then 4 text messages. (Google splits up the message when it is large).  All hooked up with directions to the repair shop, I was again on my way.  I only lost the time it took me to stop and type the messages into my phone.

Granted, text messaging isn't the most efficient way to search, but it sure can get you out of a bind.  So far, I have used it for looking up addresses, phone numbers, directions, movie showtimes, and word definitions.  The only limitation I have run into is that I often don't know what zip code to enter when looking for local information.  (It sure would be nice if your phone could tell you what zip code it is in at any given time).

If you haven't yet, give the Google SMS interface a try.  At the very least, you can impress your officemates with this fancy new tool.  Since just about every modern cell phone has the ability to send and receive text messages, you already have everything you need to get started.
