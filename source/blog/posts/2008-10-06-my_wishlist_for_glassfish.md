---
author: Dan Allen
title: My wishlist for GlassFish
slug: my_wishlist_for_glassfish
status: Publish
breaks: false
categories: [Java]
date: 2008-10-06 02:08:57
keywords: [GlassFish, ideas, feedback, asadmin, exploded EAR, seam-gen, email, MailSession, JavaMail, JPA provider, Hibernate, TopLink Essentials]
---
<p>I received a <a href="http://www.mojavelinux.com/blog/archives/2008/10/deploying_a_seamgen_project_to_glassfish/#comment1137">response</a> to my post about how to <a href="http://www.mojavelinux.com/blog/archives/2008/10/deploying_a_seamgen_project_to_glassfish/">deploy a seam-gen project to GlassFish</a> from the GlassFish Group Project Manager, John Clingan. He asked me to provide feedback on how to improve GlassFish. I drafted a message to him with several items, which I want to share publicly.</p>
<p>My first long standing wish has already been addressed by a <a href="http://blogs.sun.com/jluehe/entry/retain_session_data_during_redeployment">recent announcement</a>. In GlassFish V3, session data is now retained across a restart. It has always been an Achilles heel of Java EE that redeployments are disruptive to manual testing (let's face it, that's how most developers work). Hot deployment was a step forward, but ultimately preserving session data is what makes the deployment truly smooth. Good job and keep working on ways to make a redeployment or partial deployment transparent to the developer's process.</p>
<p>Here are my other "bones" I have with GlassFish:</p>
<ol>
<li>The JNDI MailSession in GlassFish does not support authentication over SMTP. In my opinion, this is a huge let down. With Gmail, and other providers, allowing their mail server to be used for free, it's the absolute easiest way for Java EE developers to setup JavaMail. GlassFish is all about easy, but alas, this task is not so since there is no support for authentication over SMTP.</li>
<li>It's not possible to establish a database connection in the console that uses an empty password. While it may appear silly at first, know that embedded databases such a HSQLDB work out of the box with the username "sa" and a blank password, so it just causes an inconvenience.</li>
<li>Developers have responded to my last post about GlassFish complaining that they cannot get the exploded EAR seam-gen project to work. There are two parts to this. First, perhaps there are some limitations with the exploded EAR in GlassFish. Second, I think it would go a long way for GlassFish developers to work with Seam developers to document how to customize a seam-gen application to deploy under GlassFish.</li>
<li>asadmin is a great tool, but one of the things it lacks is a set of built-in Ant tasks that a developer can incorporate into their build. You may notice from the <a href="http://code.google.com/p/seaminaction/wiki/DeployingToGlassFish">wiki page</a> that I have to create an Ant macro that calls out to asadmin from the commandline. I think it would be worthwhile to make &lt;asadmin> a bonafide target in Ant. Any other build tool you want to target would also be helpful, but at the end of the day, Ant is king.</li>
<li>A fairly common task when you settle into an application server is to bring your own JPA provider. In fact, it's extremely common for newcomers to GlassFish to want to use Hibernate, specifically. But to use Hibernate requires customization since GlassFish uses TopLink Essentials (i.e., EclipseLink) as the default JPA provider. I'm not going to make the argument here that one is better than the other, but rather focus on the task of swapping between them. I feel that the administration console should provide a way to register a new provider and perhaps even know how to get the JARs (or perhaps you have to upload them as a zip file). Either way, you should not have to go into your GlassFish installation and start replacing JAR files because it's tedious and error prone.</li>
<li>It would be nice to have a JDBC driver manager. This is another JAR
I have to manually copy into glassfish/domains/$domain/lib/ext. Plus, I would get a dropdown of Driver implementations when creating a new JDBC connection pool.</li>
</ol>
<p>By no means is this a comprehensive list, but just happens to be what has been on my mind lately. If I think of other ideas, I will likely follow up with another post. However, what I am really interested in is if you have suggestions for John and the GlassFish team. Feel free to post them in the comments.</p>
<p><strong>UPDATE:</strong> I forgot to mention that I was impressed by how quickly Sun picked up on my initial post and was willing to ask for input. It shows that Sun (specifically the GlassFish team) is keeping a thumb on the pulse of the community, which I feel is critical part of having a successful and useful project.</p>
<p><strong>UPDATE 2:</strong> I had forgotten to add my point about managing the JPA provider</p>
<p><strong>UPDATE 3:</strong> The only limitation with GlassFish deploying an exploded EAR is that all EJB modules must be exploded. That means the jboss-seam.jar must be placed into the EAR as an exploded archive.</p>
<p><strong>UPDATE 4:</strong> Added point about managing JDBC drivers.</p>
