---
author: Dan Allen
title: An Advocate, Not a Zealot
slug: an_advocate_not_a_zealot
status: Draft
breaks: true
categories: [Open Source]
date: 2004-10-23 04:45:52
keywords: []
---
I have often been faced with the question of why I favor open source, being posed by both others and myself, as well as what it means to favor open source.  I have often woken up in the middle of the night thinking about out where I draw the line between free and not free.  Since mojavelinux.com is all about the advocacy of open source, I felt that the article <a href="http://www.newsforge.com/article.pl?sid=04/10/14/219203">How to be a Free Software zealot</a> was particularly relevant its purpose.  After reading the article, clearly, I am not an open source zealot.  As much as I understand the points of Richard Stallman, et al, I hate it when people nit pick.  It leads to bickering and confusion, which is exactly what I was escaping when I headed for the hills of open source.

Why am I an advocate rather than a zealot and why do I advocate at all?  I originally latched on to Linux and open source because it offered me a choice.  There are many reasons why choice is vital to the human existence, but to but it simply, having a choice allows us to be happier.  Since software exists to aid our daily existence, we want it to make us happy.  The best example of a piece of software that demonstrates this power is Firefox.  By using Firefox, you choose how you want to surf the web, rather than having a company decide for you.  From the roots of choice comes having a voice.  Choosing software from the open source community allows you to play a role in its future.  When I receive feedback on an open source program, I know that I can turn around and share than information with the developers.  That connection not only allows us to feel better about the program, but it makes the program better.  Each open source program is the culmination of all the ideas and feedback from the pool of minds this world has to offer.  Finally, and most radically, open source disrupts power.  We all want power and it is not a bad thing.  But power is bad when it remains in the hands of a few, because it takes away both choice and our voice.  No one owns you, so why should they own your software, something which your daily existence depends on.

For the three reasons listed above, I find that the Firefox slogan encapsulates them well.  "Take back the web."  This slogan signifies that you want a choice, that you are going to share your vision of what it should be, and that the web is in the wrong people's hands.  From that, I will extend the slogan in a general way by saying "Take back your computer."  You choose what to run, how it will run, and than your computer owns the software it runs and not the otherway around.
