---
author: Dan Allen
title: Why Are You Bare?
slug: why_are_you_bare
status: Publish
breaks: true
categories: [CSS &amp; Design]
date: 2006-04-05 18:01:41
excerpt: Look ma, no styles!
keywords: [css, styles, semantic web, web standards, April 5th]
---
Today, April 5th, is the First Annual Naked Day. This means that if you are reading this on the <a href="http://www.mojavelinux.com">website</a> and not via RSS, what you are seeing is mojavelinux.com with the CSS stripped off (hence with default browser styles).

So why CSS Naked Day? The <a href="http://www.dustindiaz.com/naked-day">original idea</a> came from Dustin Diaz and is intended to promote <a href="http://www.webstandards.org/">Web Standards</a>.  In short, that means that a website uses semantic, meaningful markup and not just thrown together to look pretty. Even the inventor of CSS, H&aring;kon Wium Lie, is keen on the idea:

<blockquote><div>This is a fun idea, fully in line with the reasons for creating CSS in the first place. While most designers are attracted by the extra presentational capabilities, saving HTML from becoming a presentational language was probably a more important motivation for most people who participated in the beginning.</div></blockquote>

So is your site laid bare? Is your site still understandable with the CSS peeled off?  Take the challenge!

You can see what this site looks like without styles in Firefox on any ol' day by simplying selecting the menu item <strong>View &gt; Page Style &gt; No Style</strong>.
