---
author: Dan Allen
title: Eclipse Resource Filtering using 'Derived'
slug: eclipse_resource_filtering_using_derived
status: Publish
breaks: true
categories: [Java]
date: 2006-03-27 09:04:44
keywords: [eclipse, tip, WTP, maven]
---
Once in a great while you stubble upon a tip that provides a solution to a problem you had all but given up on.  Today I will pass on a tip in hopes that it provides you with the same result as it did for me.

As the author of <a href="http://mrj.woo.dk/squareroot/2006/03/06/marking-resources-in-eclipse-as-derived/">this blog entry</a> describes, often times Eclipse is a bit too helpful in the options it presents to you when searching for a resource (non-java file) to open via the Ctrl-Shift-R key command.  The extra options include working directories for both external tools, such as maven, as well as Eclipse itself.  One of the most common examples is the <em>.deployables</em> directory that Eclipse used for web projects prior to WTP 1.0.

The trick is to open the "Navigator" view, right click on the folder to be ignored, and check the "Derived" property.  This option informs Eclipse that this folder consists of generated resources that should not be directly edited.  Once this is done, the "Open Resource..." view will only show matches that would be relevant to the developer.
