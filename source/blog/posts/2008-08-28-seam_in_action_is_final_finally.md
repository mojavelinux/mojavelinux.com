---
author: Dan Allen
title: Seam in Action is final, finally!
slug: seam_in_action_is_final_finally
status: Publish
breaks: true
categories: [Seam in Action, Java, Seam, Seam News]
date: 2008-08-28 13:32:37
keywords: [Seam in Action, Seam, book writing, finishing, ebook, source code, the last mile]
---
In the writing business, it ain't over until the book is printed. Well, in response to the question I have been asked countless times by family, friends, and colleagues over the last 15 months, "How's the book?", I can finally shout, "It's done!" (followed by an immediate collapse from exhaustion). In Winston Churchill's <a href="http://www.thinkarete.com/quotes/print/4123">words</a>, I have finally killed the monster and flung him out into the public.

<a href="http://mojavelinux.com/seaminaction">Seam in Action</a> went to the printer two weeks ago and will be available September 5th on the bookstore shelves, online (<a href="http://mojavelinux.com/seaminaction">manning.com</a> or <a href="http://www.google.com/products/catalog?q=%22seam+in+action%22&btnG=Search&hl=en&show=dd&cid=425417071307208272">elsewhere</a>), and at the <a href="http://jsfone.com">JSFOne Conference</a>. I have held off doing an annoucement until today since there was still work to be done to get the ebook ready and I didn't want to say it is done until it is really available. I just checked in the official release of the <a href="http://seaminaction.googlecode.com/files/seaminaction-20080828.zip">source code</a> for the book this morning, which means that ebook can now be released into the wild. As of today, the ebook is being sent to subscribers and is available for purchase on <a href="http://mojavelinux.com/seaminaction">manning.com</a>. If you order the print version of the book, you get the ebook for free, exactly as it should be.

The last 15 months have been incredibly challenging on so many levels. With each weekend that passed by it seemed as though the end was getting furthur and furthur away, yet life did not pause. I had one job, took a sabatical, left that job, started another job, took another sabatical, changed computers three times, bought a car, my sister moved to NY to Knoxville to Atlanta and switched jobs three times, my college roommate got married, a life-long friend a baby, one of my cousins had a baby, my wife's cousin passed away, two of my neighbors moved away, I had my first speaking engagement at a conference, I attended four conferences, I traveled to Europe for the first time in my life, a family vacation came and went, an Olympics came and went, an NFL season came and went, and the list goes on.

The story of <a href="http://ramya.bhaavana.net/chaitanya/?p=38">"the last mile"</a> told by the authors of the <a href="http://www.manning.com/affiliate/idevaffiliate.php?id=647_106">SOA Security</a> book mirrors exactly what I went through during the long tail of this project, so I will refrain from retelling it here. Thankfully, my enthusiasm for Seam and the support of my wife kept me going and I can now share with you the extensive research I have done over that time period. My only regret is that I would have gotten the book to you sooner, but Seam has such a bright future that by no means was a window missed. Thanks to all of you who remained patient and helped keep me atop Manning's MEAP bestseller list throughout most of the process.

There is still plenty I want to say about this experience, both at a personal and professional level, but right now I am having difficulty getting the words to flow. Keep watching for additional entries that are to come. Right now, I am just happy to say that I am finally done and can share the book with you. <strong>Get it. Read it. Learn from it.</strong>
