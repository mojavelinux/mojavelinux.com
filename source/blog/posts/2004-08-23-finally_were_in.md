---
author: Dan Allen
title: Finally, We're In!
slug: finally_were_in
status: Publish
breaks: true
categories: [General]
date: 2004-08-23 01:09:22
keywords: [our pile of dirt, townhouse, ryland, moving]
---
After waiting what has seemed like an eternity for our new townhouse to be completed, my wife and I are finally able to call ourselves homeowners!  This past year has had more than its share of ups and downs.  After coming off the excitement of signing our sales agreement for a January 2004 closing in September of last year, progress on the house was literally non-existent for more than five months. We grew so frustrated with the schedule slips that we named the lot "Our Pile of Dirt."  We even established a <a href="/personal/pileofdirt/">photoblog</a> to keep us busy, and to entertain our inquisitive friends and family, while we anxiously awaited the final phase of construction.  That date, of course, didn't occur until nearly a year had passed, in August 2004.

By the time our move-in day rolled around, we could hardly believe it was actually happening.  As I walked up the front stoop for the first time as a homeowner, I found a penny resting on my doorstep.  Now, I'm not one to believe in signs, but that just has to bode well for us as we become acquainted with our new life.  Frankly, at this point, we could use the luck. Each day since we moved in we have slowly been putting the pieces (in the form of boxes) of our lives back together.  Some of the boxes we opened the other night contained belongings neither Sarah nor I have seen for several years.  It's as though a very large phase of our lives have come full circle.  Now we look forward to paving our future together, a future which has been on hold since we married in September of 2002.  Although we face some very tough decisions, many of which will affect us long into the future, we are both ready for whatever comes our way.  Sarah and I have made it through one of the most trying times in our life and we are happier now than ever before.

Despite all the problems we had getting Ryland to actually work on our house, it is remarkably beautiful and our neighbors are awesome!  It is honestly more than either of us could have ever hoped for.  Even more surpising is that after choosing all the design options for the house in a single afternoon, a day which now I can hardly recall, both Sarah and I love every bit of the house and would hardly change a thing.  One of our favorite features is the intercom system.  Since Sarah and I have been single-level living for our entire relationship, we are likely to lose each other fairly easily in a multi-level townhome.  The intercom system allows us to communicate without yelling, something we both appreciate.  Adding on our new high definition television and electronic washer and dryer to the mix was a nice touch as well ;)

Over the last few years, Sarah and I have practically been playing musical house.  Moving is not an easy task for me. In fact, I despise it.  I just feel utterly lost and frustrated during a move. It never fails that I cannot locate the one thing I need to most until long after I have grown tired of searching for it and stuff always seems to gets damaged.  As I was walking down the street the other day, I spotted a sign in a store window which is quite fitting.  The next time Sarah and I move, it will be this sign you will find in our window.

<div style="text-align: center;"><img src="/assets/pics/moved_to_internet.jpg" /></div>
