---
author: Dan Allen
title: Putting Entities to Rest with Groovy, Hibernate, and JPA
slug: putting_entities_to_rest_with_groovy_hibernate_and_jpa
status: Publish
breaks: false
categories: [Java]
date: 2007-05-09 18:49:44
keywords: [groovy, persistence, hibernate, jpa, entity class, entities, sessionfactory, entitymanager]
---
<p>I mentioned in my previous entry about how I am totally sold on Groovy.  I immediately went off and converted a couple of shell scripts that I use daily into Groovy scripts.  I was pleasantly surprised by the <span style="font-family: monospace;">CliBuilder</span> for processing command-line arguments, but that is another story.  What I am really excited about is what I was able to do next.  While perusing <a href="http://www.manning.com/bauer2">Java Persistence with Hibernate</a>, I started thinking about how Groovy might make object-relational persistence easier.  I tend to dread working with Hibernate or JPA just because it takes a while to get it all setup, usually requiring the formal step of setting up a project in Eclipse.</p>
<p>That is where our story begins.  The challenge:  How hard is it to use Groovy to persist an entity class?  The answer: Amazingly simple.  So simple, in fact, that there is almost no reason to access the database any other way (unless you are a database purist and then you have your reasons).  For me, I prefer the Hibernate or JPA route, if I can take it.</p>
<p>There exist a couple of <a href="http://www.curious-creature.org/2007/03/25/persistence-made-easy-with-groovy-and-jpa/">other</a>  blog <a href="http://www.jroller.com/page/buggybean?entry=using_groovy_spring_and_javaconfig1">entries</a> on this topic, but I promise you that I have an exciting conclusion.  Let's get started.</p>

<p><b>Step 0. Groovy 1.1</b></p>
<p>You will need to be using Groovy 1.1, which has annotation support, to run these examples.  At the time of writing this version is still in beta, but I have had no problems with it.</p>
<p><b>Step 1. Get the JARs</b></p>
<p>Easily the most annoying part of working with any library is having to aggregate all of the dependencies.  Eventually we are going to have a clean, universal solution for this problem.  Until then, Groovy helps to hide some of the nastiness.  If you use Maven 2, which I happen to recommend, you will likely have most of these JARs already sitting in your local repository.  Copy the list of JARs, shown below, to the <span style="font-family: monospace;">~/.groovy/lib/</span> folder.  I have included both Hibernate and JPA JARs just so that I don't have to do this step twice.  I went with MySQL for this example. If you would like to use another database, then you will need to include the appropriate JDBC driver and change the connection strings in the examples to suit that database.</p>
<ul>
<li>antlr-2.7.6.jar</li>
<li>cglib-nodep-2.1_2.jar (fix incompatibility with Groovy asm library)</li>
<li>commons-collections-3.2.jar</li>
<li>commons-httpclient-2.0.2.jar</li>
<li>commons-logging-1.1.jar</li>
<li>dom4j-1.6.1.jar</li>
<li>geronimo-j2ee_1.4_spec-1.1.jar (JTA: Java Transaction API)</li>
<li>hibernate-3.2.1.ga.jar</li>
<li>hibernate-annotations-3.2.1.ga.jar</li>
<li>hibernate-entitymanager-3.2.1.ga.jar (Hibernate JPA)</li>
<li>jboss-archive-browsing-5.0.0alpha-200607201-119.jar (Hibernate JPA)</li>
<li>mysql-connector-java-5.0.5.jar</li>
<li>persistence-api-1.0.jar</li>
</ul>
<p>Please note that any JTA implementation will work. I just happen to use the Geronimo implementation since it is available in the public Maven 2 repository.</p>
<p><b>Step 2. Create an Entity class</b></p>
<p>So that I make no assumptions about existing tables, I am going to create a new entity class that will generate a table automatically.  If you already have a table that you would like to use, then create an appropriate entity for it.</p>
<pre>import javax.persistence.*

@Entity
@Table(name="contact")
class Contact implements Serializable {
    @Id @GeneratedValue
    Long id

    String name

    String email
}</pre>
<p><b>Step 3. Create a SessionFactory or PersistentUnit configuration</b></p>
<p>Depending on whether you are going to do a Hibernate SessionFactory or a JPA PersistenceUnit, you will either create a hibernate.cfg.xml configuration or a META-INF/persistence.xml configuration.  I will include both below.  Choose the one appropriate for your choice of implementation.  I will stick with the ordering of Hibernate snippets followed by JPA snippets.</p>
<pre>&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;!DOCTYPE hibernate-configuration PUBLIC
    "-//Hibernate/Hibernate Configuration DTD 3.0//EN"
    "http://hibernate.sourceforge.net/hibernate-configuration-3.0.dtd"&gt;
&lt;hibernate-configuration&gt;
    &lt;session-factory&gt;
        &lt;property name="hibernate.dialect"&gt;org.hibernate.dialect.MySQLDialect&lt;/property&gt;
        &lt;property name="hibernate.connection.driver_class"&gt;com.mysql.jdbc.Driver&lt;/property&gt;
        &lt;property name="hibernate.connection.url"&gt;jdbc:mysql://localhost:3306/database&lt;/property&gt;
        &lt;property name="hibernate.connection.username"&gt;username&lt;/property&gt;
        &lt;property name="hibernate.connection.password"&gt;password&lt;/property&gt;
        &lt;property name="hibernate.hbm2ddl.auto"&gt;create&lt;/property&gt;
        &lt;mapping class="Contact"/&gt;
    &lt;/session-factory&gt;
&lt;/hibernate-configuration&gt;</pre>
<p>or</p>
<pre>
&lt;persistence xmlns="http://java.sun.com/xml/ns/persistence"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://java.sun.com/xml/ns/persistence http://java.sun.com/xml/ns/persistence/persistence_1_0.xsd"
    version="1.0"&gt;
    &lt;persistence-unit name="default" transaction-type="RESOURCE_LOCAL"&gt;
        &lt;class&gt;Contact&lt;/class&gt;
        &lt;properties&gt;
            &lt;property name="hibernate.dialect" value="org.hibernate.dialect.MySQLDialect" /&gt;
            &lt;property name="hibernate.connection.driver_class" value="com.mysql.jdbc.Driver" /&gt;
            &lt;property name="hibernate.connection.url" value="jdbc:mysql://localhost:3306/database" /&gt;
            &lt;property name="hibernate.connection.username" value="username" /&gt;
            &lt;property name="hibernate.connection.password" value="password" /&gt;
            &lt;property name="hibernate.hbm2ddl.auto" value="create" /&gt;
        &lt;/properties&gt;
    &lt;/persistence-unit&gt;
&lt;/persistence&gt;</pre>
<p><b>Step 4. Startup a session and do persistence operations</b></p>
<p>In this step, we are going to do everything in one script, including starting up the Hibernate SessionFactory or JPA PersistenceUnit and then executing some persistence operations.</p>
<pre>import org.hibernate.*
import org.hibernate.cfg.*

sf = new AnnotationConfiguration().configure().buildSessionFactory()
sess = sf.openSession()
tx = sess.beginTransaction()
sess.createQuery("delete Contact").executeUpdate()
sess.save new Contact(name: 'Dan Allen', email: 'dallen@example.com')
tx.commit()
sess.close()
sf.close()</pre>
<p>or</p>
<pre>
import javax.persistence.*

emf = Persistence.createEntityManagerFactory("default")
em = emf.createEntityManager()
em.getTransaction().begin()
em.createQuery("delete Contact").executeUpdate()
em.persist new Contact(name: 'Dan Allen', email: 'dallen@example.com')
em.getTransaction().commit()
em.close()
emf.close()</pre>
<p><b>Step 5. Get groovy and create a template</b></p>
<p>Now we are going to take a pragmatic approach by creating a reusable template, which we can use to wrap the persistence operations.  That way, it isn't necessary to do all the nasty work of starting up the persistence engine each time we write a script (even though it is still only a couple of lines).  I call these two classes HibernateTemplate and JPATemplate respectively.  They use an anonymous code block argument to a method that will execute the operations within a persistence session.</p>
<pre>import org.hibernate.cfg.*
import org.hibernate.*

class HibernateTemplate {

    SessionFactory sf

    Session sess

    HibernateTemplate(config) {
        if (!config) config = "/hibernate.cfg.xml"
        sf = new AnnotationConfiguration().configure(config).buildSessionFactory()
    }

    void doInSession(c, shutdown) {
        sess = sf.openSession()
        def tx = sess.beginTransaction()
        c(sess)
        tx.commit()
        sess.close()
        if (shutdown) shutdown()
    }

    void shutdown() {
        sf.close()
    }
}</pre>
<p>or</p>
<pre>import javax.persistence.*

class JpaTemplate {

    EntityManagerFactory emf

    EntityManager em

    JpaTemplate(pu) {
        if (!pu) pu = "default"
        emf = Persistence.createEntityManagerFactory(pu)
    }

    void doInEntityManager(c, shutdown) {
        em = emf.createEntityManager()
        em.getTransaction().begin()
        c(em)
        em.getTransaction().commit()
        em.close()
        if (shutdown) shutdown()
    }

    void shutdown() {
        emf.close()
    }
}</pre>
<p><b>Step 6. Take advantage of the templates</b></p>
<p>Now I will take the code from Step 4 and rewrite it using the template objects.</p>
<pre>new HibernateTemplate().doInSession({ sess -&gt;
    sess.createQuery("delete Contact").executeUpdate()
    sess.save new Contact(name:'Dan Allen', email: 'dallen@example.com')
}, true)</pre>
<p>or</p>
<pre>new JpaTemplate().doInEntityManager({ em -&gt;
    em.createQuery("delete Contact").executeUpdate()
    em.persist new Contact(name:'Dan Allen', email: 'dallen@example.com')
}, true)</pre>
<p>Look at that!  I am using 4 lines of code to clean out the table and persist an entity!!  Now that is groovy!  If that doesn't get you excited about Groovy, then I don't know what will.</p>
