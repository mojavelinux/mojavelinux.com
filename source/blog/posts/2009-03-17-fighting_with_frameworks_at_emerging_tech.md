---
author: Dan Allen
title: Fighting with Frameworks at Emerging Tech
slug: fighting_with_frameworks_at_emerging_tech
status: Publish
breaks: false
categories: [Seam News, Java, Seam, Seam in Action]
date: 2009-03-17 02:25:01
keywords: [Manning, Emerging Tech, Chariot Solutions, Web Framework Shootout, Seam, Seam in Action, panel]
---
<p>I'm speaking again this year at the <a href="http://phillyemergingtech.com">Emerging Technologies for the Enterprise</a> conference in Philadelphia, PA hosted by <a href="http://www.chariotsolutions.com">Chariot Solutions</a>. It's one of few enterprise development conferences held on the East Coast that attracts high caliber speakers (and lots of framework passion) with such a remarkably low price tag (which is music to our savings accounts these days). Last year I gave an introductory to Seam talk. This year, I'll be presenting on <a href="http://phillyemergingtech.com/speakers.php">Seam Security</a>, highlighting its ease of use and showcasing the new identity and permission management API that was added in Seam 2.</p>

<p>I got involved with the conference last year through Manning, who, as event sponsor, was rounding up a bunch of its authors to speak so that they could take part the Web Framework Shootout panel. If you missed it, the audio from that panel discussion is available online, divided into two Chariot TechCast episodes (<a href="http://techcast.chariotsolutions.com/index.php?post_id=339701">part 1</a>, <a href="http://techcast.chariotsolutions.com/index.php?post_id=341310">part 2</a>). It's worth checking out because there certainly was a lot of shooting...off at the mouth, that is.</p>

<p>You can listen to the whole discussion to get the context. But I've distilled my responses for you into three short segments. I realize this post is long overdue, but since it retains more than just historical value, I've decided to move forward with publishing it.</p>

<ul>
	<li><a href="https://www.dropbox.com/s/zbpl9c5647yowtt/emergingtech08-dan-overview-of-seam.mp3?dl=0">Segment #1</a>: (01:10) I define what exactly Seam is in response to Marjan's poignant question about it</li>
	<li><a href="https://www.dropbox.com/s/occvxb1n40gyxwv/emergingtech08-dan-seam-business-value.mp3?dl=0">Segment #2</a>: (02:35) I introduce myself and give a broad overview of what Seam provides</li>
	<li><a href="https://www.dropbox.com/s/dvg6egcvlgs5lny/emergingtech08-dan-what-seam-is-about.mp3?dl=0">Segment #3</a>: (01:30) I identify the business value in adopting Seam and contrast that with how little Struts 1 provides</li>
</ul>

<p>The shootout was supposed to be about Web Frameworks, but the debate ended up playing out on two separate playing fields. The first debate was about Ruby vs the World, which got quite heated at times thanks to Obie and someone denouncing Twitter's ability to scale (which it mistakes for a fail whale). The second debate was about RIA built on JavaScript vs the more traditional server-side UI component model.</p>

<p>As you'll hear, I abstained from making incendiary comments and instead tried to emphasize the value Seam provides and how it really allows you to forget about much of this debate. First, because it makes the Java EE platform attractive and accessible and second, because it can plug into a wide variety of other UI frameworks, whether it be GWT, Wicket, Flex, or JSF and non-web frameworks, like JMS, Quartz, and jBPM.</p>

<p>Keep in mind that these excerpts are nearly a year old at the time of this post. However, it's interesting to hear what I had to say about Seam at the halfway point of writing <a href="http://mojavelinux.com/seaminaction">Seam in Action</a>. Seam's message hasn't changed all that much since then, even though the number and quality of the integrations has improved substantially. I wouldn't be surprised if I look back in a year and have the very same thing to say. Seam is all about trimming the fat (i.e., framework code) resident in the application so that what is left is core business logic. Some of the business logic may even get wrapped up in Seam's declarative programming model.</p>
