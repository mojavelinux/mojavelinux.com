---
author: Dan Allen
title: Seam in Action promotion at JavaRanch
slug: seam_in_action_promotion_at_javaranch
status: Publish
breaks: true
categories: [Seam in Action]
date: 2008-08-06 20:02:38
keywords: [promotion, book, Seam in Action, Seam, giveaway, forums, questions]
---
<p>In the midst of the final countdown to the publication of <a href="http://mojavelinux.com/seaminaction">Seam in Action</a>, I will be answering questions about the book, and Seam in general, on the JavaRanch forums. Ask a question and enter to win a copy of the book. Better yet, just stop on by to say "Hi!" and pick my brain about Seam.</p>

<p>Here's the official announcement:</p>

<blockquote><p>We are thrilled to have Dan Allen on the ranch to promote the book
"Seam in Action".
The promotion will be held in the Application Frameworks  forum which can be found here:</p>
<p><a href="http://saloon.javaranch.com/cgi-bin/ubb/ultimatebb.cgi?ubb=forum&amp;f=83">Application Frameworks forum</a></p>
<p>Participate in this week's giveaway by asking Dan Allen a question or two and you may win a copy of the book!</p>
<p>The giveaway starts on Tuesday, August 5th 2008.<br />
The drawing will be held on Friday, August 8th 2008.</p>
</blockquote>
<p>See you at the ranch! And wish me luck on finishing the book. I'll need it!</p>
