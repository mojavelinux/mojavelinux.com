---
author: Dan Allen
title: A game-changing Maven 2 plugin you absolutely must use
slug: a_gamechanging_maven_2_plugin_you_absolutely_must_use
status: Publish
breaks: false
categories: [Java, Programming, Usability]
date: 2009-05-04 14:18:33
keywords: [Maven 2, plugin, console, alias, speed, Ant, deployment, profiles]
---
<p>Ever since I first started using <a href="http://maven.apache.org">Maven 2</a>, I envisioned having a console in which I could execute life-cycle goals without having to incur Maven's startup cost between every run. It just seemed to me such a waste for Maven to build up the project object model (POM) from the pom.xml only to run a single sequence of goals and immediately shutdown. It also gives the impression that Maven is slow. In fact, it's extremely slow when it's used in this way, especially multi-module projects. But it doesn't have to be if you take advantage of...</p>

<h3>The maven-cli-plugin</h3>

<p>Today my vision has come true thanks to Mr. Don (and the ease of collaboration made possible by git). Having a <a href="http://www.jroller.com/mrdon/entry/speed_up_maven_through_the">similar vision to mine</a>, he wrote the maven-cli-plugin, hosted at <a href="http://wiki.github.com/mrdon/maven-cli-plugin">github.com</a>. This plugin uses the jline library to create an execution console for Maven 2 (i.e., command shell), shown here:

<pre>maven2></pre>

<p>At the prompt you can issue Maven 2 life-cycle phases or plugin goals. The console even supports tab completion of commands! In this entry, I'll explain how you setup and use this console to put Maven into hyperdrive.</p>

<p>Technically, the plugin has two consoles. One is for executing Maven 2 life-cycle phases, including all prerequisite phases, and the other is used to only run plugin goals (the life cycle is not executed). Those are the same two ways you use Maven 2 from your normal shell. In this entry, I'll be focusing on the execute-phase console.</p>

<h3>A fork in the road</h3>

<p>I was thrilled when I first discovered the plugin, but after giving it a shot I was disappointed to discover that it wasn't honoring profiles, which are a critical piece of the build (see <a href="http://github.com/mrdon/maven-cli-plugin/issues/#issue/2">issue 2</a>). I didn't give up hope though. Since Mr. Don had done most of the legwork in creating the plugin, I figured it wouldn't be that difficult for me to figure out why the profiles weren't working. Sure enough, in under an hour I discovered the source of the problem and was able to apply a fix. That left me with the dilemma of how to distribute my changes. I wanted to be able to use the plugin in the Seam 3 examples, so I couldn't just maintain a hacked version on my computer. Github.com (and git) saved the day.</p>

<p>Following the <a href="http://github.com/guides/fork-a-project-and-submit-your-modifications">process</a> for creating a fork on github.com (for which they practically spoon feed you the instructions), I forked the code and committed my changes to a publicly accessible repository. You can access both the <a href="http://github.com/mrdon/maven-cli-plugin/tree/master">master tree</a> and my <a href="http://github.com/mojavelinux/maven-cli-plugin/tree/master">forked tree</a>. You'll need the version from my tree to use all the features I cover in this entry. Hopefully Mr. Don will merge my changes into the master tree soon, but the fact that you can get my code today is a true testament to the influence <a href="http://git-scm.com">git</a> can have on open source collaboration.</p>

<p>Now let's "git" on with the presentation.</p>

<h3>Getting started</h3>

<p>To get started, add a plugin repository where the maven-cli-plugin is hosted as a top level element in your pom.xml. I recommend using the JBoss maven repository because it hosts my forked version.</p>

<pre>&lt;pluginRepositories>
    &lt;pluginRepository>
        &lt;id>repository.jboss.org&lt;/id>
        &lt;name>JBoss Repository&lt;/name>
        &lt;url>http://repository.jboss.org/maven2&lt;/url>
    &lt;/pluginRepository>
&lt;/pluginRepositories></pre>

<p>Alternatively, you can publish the plugin to your local or company repository. Grab the source from github.com or the binaries from the JBoss Maven repository. Remember, you need my forked version until my features get merged in. Follow the project at github.com to find out when that happens.</p>

<p>Next, define the maven-cli-plugin inside the build > plugins element in your POM. This just allows you to use the plugin prefix from the commandline, which is <span class="code">cli</span>. You're also going to use this section to add some configuration options later.</p>

<pre>&lt;plugin>
   &lt;groupId>org.twdata.maven&lt;/groupId>
   &lt;artifactId>maven-cli-plugin&lt;/artifactId>
&lt;/plugin></pre>

<p>With the setup out of the way, I'm going to demonstrate three reasons why this plugin is game-changing. By the end, I guarantee you'll be jumping out of your chair.</p>

<h3>#1 - Speed</h3>

<p>Let's face it, Maven is dog-slow. That's because it has to parse that heap of XML the Maven developers call a POM. Then it has to figure out what it's supposed to execute. Then it pings the internet for updates. Then it enforces constraints. Then it runs through all the precursor phases (likely including tests). And finally it arrives at the phase you really want it to execute.</p>

<p>Of course, some of those steps can be trimmed using various flags, switches, and properties. But that means having to type, and remember, a ridiculously long command that is just something no human should be expected to do. More on that later. Let's deal with this startup cost once and for all.</p>

<p>We'll begin by firing up the maven-cli-plugin phase executor console. You run it just like you would any other Maven plugin:</p>

<pre>mvn cli:execute-phase</pre>

<p>After Maven goes through it's normal loading process, you are presented with a command prompt:</p>

<pre>maven2></pre>

<p>From here you can type any of Maven's <a href="http://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html#Lifecycle_Reference">life-cycle phases</a>, such as package, or a plugin goal (more on plugins later). Give it a try:</p>

<pre>maven2> package</pre>

<p>The first time the life cycle executes, you'll see a noticeable improvement in speed. By the second execution, it's <em>blazing</em> fast! You can just feel years being added back to your life. <strong>Power up!</strong></p>

<p>Now it's time to extend the build with...</p>

<h3>#2 - Profiles</h3>

<p>One of the most powerful features of Maven is profiles. In fact, I think Maven is pretty useless without them. That's because in Maven, you really only have one "command" you can execute, the Maven 2 life cycle. There must be a way, then, to instruct that execution pass to perform different steps along the way. That's what profiles are for. With a profile, you can hook additional plugins to a phase as a way to weave that extra behavior into the build.</p>

<p>But we have a dilemma. Profiles are typically activated from the commandline either explicitly using the -Pprofile flag or through an activation, typically by assigning a property such as -Dproperty=value. How can we set the profile once we are in the command console? This is where my contribution to the maven-cli-plugin comes in.</p>

<p>The commands typed in the console are processed by the jline ConsoleReader from the maven-cli-plugin, not by Maven. That means we can allow any command we want, including flags like -P and -D. Fortunately, Maven was designed in such a way that an execution is isolated internally from parsing the POM. So it's possible to execute a life-cycle phase with a different set of profiles and properties, or even put Maven into offline mode for a single execution, without having to start the console again.</p>

<p>I hacked up the maven-cli-plugin to support the following flags:</p>

<ul>
<li><strong>-P</strong> activates the profile specified immediately after the flag (no spaces); this flag can be used multiple times</li>
<li><strong>-D</strong> assigns a property specified immediate after the flag (no spaces) in the form name=value; this flag can be used multiple times</li>
<li><strong>-o</strong> puts Maven in offline mode for a single execution; if not specified, will inherit the setting used to start the console</li>
<li><strong>-N</strong> instructs Maven not to recurse into projects in the reactor</li>
<li><strong>-S</strong> skip tests, an alias for -Dmaven.test.skip=true</li>
</ul>

<p>Here's an example of a command you can issue:</p>

<pre>maven2> clean package -Prun-integration-tests -o</pre>

<p>That would activate the run-integration-tests profile, run the clean plugin and the life cycle up to the package phase, all while executing Maven in offline mode (to avoid checks for missing pom files, snapshots, and plugin updates).</p>

<p>In addition, the maven-cli-plugin already supported specifying individual projects by artifactId. This allows you to execute a phase on a sub-project without having to descend into that project or use the -f flag.</p>

<p>Let's say you are working with an standard EAR project and you want to build the WAR. To package just the WAR, you would either have to change into the war directory and execute Maven or use the -f flag as follows:</p>

<pre>mvn -f war/pom.xml package</pre>

<p>With the maven-cli-plugin, you can accomplish the same thing using this command (note that "seam-booking-war" is the artifactId of the module):</p>

<pre>maven2> seam-booking-war package</pre>

<p>Ah, the simplicity! And now, for the grand finale!</p>

<h3>#3 - Aliases</h3>

<p>Aliases are the <strong>Holy Grail</strong> of Maven 2. When I switch people from Ant to Maven, the first thing they get annoyed about is the ridiculous commands they are required to type. To put it simply, if you want to run clean and package, there is no way to specify that with a single command. You have to type:</p>

<pre>mvn clean package</pre>

<p>Things get worse with plugins. All plugin goals must be namespaced. That's because Maven 2 technically supports any command in the world, as long as there is a plugin to execute it. To run Jetty on a WAR project, for instance, you have to type:</p>

<pre>mvn run:jetty</pre>

<p>If you want to run clean, package, and then start jetty, you have to type:</p>

<pre>mvn clean package run:jetty</pre>

<p>Let's say that you also need to expand the WAR in-place and you want to run offline. Then the command becomes:</p>

<pre>mvn -o clean package war:inplace run:jetty</pre>

<p>I think you can see where this is going. When I first setup the booking examples for Seam 3, the record for the longest commandline in the readme went to this command, which undeploys, packages, and redeploys an EAR to JBoss AS:</p>

<pre>mvn -o -f ear/pom.xml jboss:undeploy &amp;&amp; \
  mvn -o package &amp;&amp; \
  mvn -o -f ear/pom.xml jboss:deploy</pre>

<p><em>Uuuuugly!</em> That's why the aliases feature of the maven-cli-plugin is absolutely game-changing (perhaps even life changing). In fact, combined with the other two features I have covered, they make Maven 2 better and faster than Ant, hands down. I'll go so far as to say that there has never been a faster, more convenient way to execute builds.</p>

<p>So what is an alias? Quite simply, a string of commands you would otherwise have to type in the console, aliased to a single word. You define them in the plugin configuration. Here's an alias I put together to deploy an exploded EAR archive to JBoss AS in the Seam booking example:</p>

<pre>&lt;plugin>
   &lt;groupId>org.twdata.maven&lt;/groupId>
   &lt;artifactId>maven-cli-plugin&lt;/artifactId>
   &lt;configuration>
       &lt;userAliases>
           &lt;explode>package -o -Pexplode&lt;/explode>
       &lt;/userAliases>
   &lt;/configuration>
&lt;/plugin></pre>

<p>After starting up the console, the user only has to type one word:</p>

<pre>maven2> explode</pre>

<p>It's no longer even necessary to prefix commands with mvn (or ant in the old days). Just one command. One word.</p>

<p>The execute-phase console also supports direct execution of plugin goals. That means you can include them in the alias command. The only limitation is that when you include one in an alias, you have to specify the fully qualified name of the plugin (groupId:artifactId) before the goal rather than just it's prefix (e.g., org.codehaus.mojo:jboss-maven-plugin rather than jboss). This next alias invokes the harddeploy goal of the jboss-maven-plugin after packing the project.</p>

<pre>&lt;userAliases>
   &lt;deploy>seam-booking-ear package -o org.codehaus.mojo:jboss-maven-plugin:harddeploy&lt;/deploy>
&lt;/userAliases></pre>

<p>You can even mix aliases with regular commands. Perhaps you want to clean first:</p>

<pre>maven2> clean deploy</pre>

<p>I find it nice to alias commonly used built-in Maven goals too, such as the one that lists the active profiles:</p>

<pre>&lt;userAliases>
   &lt;profiles>org.apache.maven.plugins:maven-help-plugin:active-profiles -o&lt;/profiles>
&lt;/userAliases></pre>

<p>The maven-cli-plugin also provides a handful of built-in aliases.</p>

<p>If this plugin isn't game changing, I don't know what is. All I can say as <strong>hell yeah</strong>!</p>

<p>See the <a href="http://anonsvn.jboss.org/repos/seam/examples/trunk/booking/">Seam 3 booking example</a> to see this plugin in action and read additional commentary.</p>

<p><strong>Update:</strong> There is also a <a href="http://www.jroller.com/mrdon/entry/maven_without_all_the_slowness">cli client for IntelliJ IDEA</a> that can invoke builds remotely. Of course, the plugin supports this from the commandline too.</p>

<p><strong>Update:</strong> I should also mention that this is a great way to debug Maven since you get an opportunity to attach a debugger before executing a command. First, set the MAVEN_OPTS environment variable:</p>

<pre>MAVEN_OPTS="-Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,address=8787,server=y,suspend=n"</pre>

<p>Then run cli:execute-phase, attach a debugger (port 8787 in this case) and execute a command.</p>
