---
author: Dan Allen
title: Taking the 'dist-upgrade' Plunge
slug: taking_the_distupgrade_plunge
status: Publish
breaks: true
categories: [Linux]
date: 2006-06-04 18:43:30
keywords: [apt, apt-get, dist-upgrade, Ubuntu, Debian, dpkg, Dapper Drake]
---
<em>I'm not afraid anymore!</em> Afraid of what?  <strong>I'm no longer afraid of upgrading Linux.</strong>  Perhaps it can be attributed to the conditioning I received as a user of non-debian distros, but, undoubtably, I dreaded having to upgrade my Linux installation.  Moving to the next release of a distro used to mean backing up my entire home directory, perhaps cleaning out any desktop settings I didn't want to carry over, formatting my harddrive, and basically starting from scratch.  This whole process would usually take an entire weekend, and a couple of late nights in the beginning of the week until I was back in my comfort zone, having explored all the new offers along the way.

One phrase comes to mind that has perhaps already been coined, but continues to prove itself time and again.

<blockquote><p>Once you go Debian, you never go back.</p></blockquote>

As a user of Ubuntu, I had the opportunity to size up the rumors and attempt to upgrade my distro by simply executing:

<strong>apt-get dist-upgrade</strong>

I was a little nervous when I hit the "OK" button to begin, but I kept saying to myself, "Ubuntu hasn't let me down yet!"  I went for it...and it worked!  In a little under two hours (mostly consumed by download time) I had migrated from Breezy Badger to Dapper Drake.

There are a couple of things that Ubuntu has done to make this process seamless.  To begin with, the Ubuntu distribution offers a one-click GUI that automates the process of changing the repositories to point to the new release and kicking off the upgrade process. Previously, this involved manually editing the /etc/apt/sources.list file and then executing the apt-get dist-upgrade command.  In addition to this interface, there is a dialog window that presents the differences that were introduced in any system configuration file since its installation.  This step protects you from losing any settings you might have forgotten that you made to your system, and an opportunity to back them up before they are trampled.

As a Mandrake user, I can remember criticizing Debian for having an ugly, text-based installer.  Even though Ubuntu has now rectified that situation in Dapper Drake, I found that it didn't even matter.  If done right, the installation of Debian happens exactly once, and from then on, the distro continuously matures via the magical workings of dist-upgrade.
