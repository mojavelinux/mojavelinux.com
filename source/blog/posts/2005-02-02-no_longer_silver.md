---
author: Dan Allen
title: No longer Silver
slug: no_longer_silver
status: Publish
breaks: true
categories: [Linux]
date: 2005-02-02 00:35:24
keywords: [Mandrake, MandrakeClub, linux, subscription, membership]
---
A while back, I <a href="/blog/archives/2002/12/mandrakeclub_silver_member/">posted</a> with excitement about becoming a Silver member of MandrakeClub.  Last month, that membership finally expired after two years and I am deciding not to renew it.  While I could probably go on and on as to why I decided to let this membership expire, the simple explanation is that I just wanted to try something new.  That's why I choose Linux in the first place and likely, that underlying motivation will always be present.  Luckily, the average person is acquiring more and more personal computers, so there is still plenty of opportunity for diversity.

Below is the note I wrote to Mandrake upon leaving, which they had requested of me before I went.

<blockquote><p>To the Mandrake Team,</p>
<p>My Silver Club membership has expired and I am choosing not to renew at this time.  Please, don't take this as an intended snub.  The work you have done is amazing and I expect to continuely see a good product coming out of Mandrake.  I need a chance to explore the other options out there, including Fedora, Ubuntu, Linspire and SuSe.  Once I do the rounds, I will have many more ideas of what makes a good desktop and will be able to contribute those ideas back to Mandrake.</p>
<p>That being said, there are several reasons I began to explore.  After having my repositories for Mandrake 9.2 break due to apparent changes in the mirror structure, I grew frustrated with maintaining the box running that distro.  Secondly, as many have already communicated, including myself, the websites of Mandrake are simply outdated.  As a web developer, I appreciate how difficult it is to rework something that is so fundamental to the business.  However, looking at just about any other distro these days, the websites are pretty slick.  Many of us choose to move to Linux from Windows because we had a voice and could make our beds the way we wanted.  I cannot change the Mandrake website situation, so I simply choose to use a different distro's website.</p>
<p>Now, as far as the principles of Mandrake, I love them.  Keep fighting the open source fight and keep up the great work, even if there are many areas of improvement.  Don't look at them as shortcomings but as challenges.  I will be back to see how you are doing.</p>
<p>Sincerely,</p>
<p>Daniel Allen (aka mojavelinux)</p></blockquote>
