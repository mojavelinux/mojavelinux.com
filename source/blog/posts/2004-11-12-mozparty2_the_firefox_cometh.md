---
author: Dan Allen
title: mozparty2 :: The Firefox Cometh
slug: mozparty2_the_firefox_cometh
status: Publish
breaks: true
categories: [Open Source]
date: 2004-11-12 08:48:00
keywords: [firefox, launch, mozparty2, devil's milk, DuClaw]
---
Mozilla Firefox has turned 1.0 and it is time to celebrate!  Now, I know all of you are using the fox (cough, cough) because it is the best browser on the planet.  And, of course, you are joining in on the battlefield by getting everyone you know to see the light as well.  And after all that hard work, there shall be much rejoicing!

As a loyal <a href="http://duclaw.com">DuClaw</a> patron and <a href="http://getfirefox.com">Mozilla Firefox</a> user, I have taken an effort to coordinate a <a href="http://www.openforce.at/mozparty2/?party=484">gathering</a> at DuClaw in Arundel Mills on Thursday, Nov 18th, as an official Mozilla Firefox 1.0 party location.  DuClaw will be having a seasonal beer release for their award-winning Devil's Milk dark amber barleywine.  DuClaw brews the best beer in Maryland, and this beer tops that list. What better way to celebrate the coming of Firefox 1.0 than to pair it with great beer.

If you are in the area, come on by!  Oh, and be sure to sport your official firefox <a href="http://www.mozillastore.com/products/clothing/launchtshirt">paraphernalia!</a>

<strong>Update:</strong> I am still waiting on my Firefox t-shirts ;(  They must be really popular this year.
