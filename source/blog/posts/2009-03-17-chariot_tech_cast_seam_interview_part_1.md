---
author: Dan Allen
title: Chariot Tech Cast: Seam Interview Part 1
slug: chariot_tech_cast_seam_interview_part_1
status: Publish
breaks: false
categories: [Seam News, Java, Seam, Seam in Action]
date: 2009-03-17 02:52:27
keywords: [Emerging Tech, Chariot Solutions, podcast, interview, Seam, Seam in Action]
---
<p>Ken Rimple from <a href="http://www.chariotsolutions.com">Chariot Solutions</a> connected with me over Skype to discuss Seam (which in truth turned out to be a series of long monologues by me about Seam and web application development).</p>
<p>In this <a href="http://techcast.chariotsolutions.com/index.php?post_id=443923">first part</a> of a two-part interview</a>, I introduce the Seam framework and how it represents departure from both J2EE of old and Spring. We discuss ways in which it marries JSF to POJOs and EJB components, provides a stateful view of the world and makes programming easier for APIs such as Java Persistence. The most intriguing point you'll learn from this first part is how Seam appealed to me as an application developer working out in the field, a testament to the fact that Seam is first and foremost a practical solution.</p>
<p>There are three ways you can track down the podcast:</p>
<ol>
    <li>Search iTunes for Chariot TechCast (Look ma, I'm on iTunes!)</li>
    <li>Subscribe to the <a href="http://chariottechcast.libsyn.org/rss">RSS feed</a></li>
    <li>Cheat and get the MP3 using a <a href="http://media.libsyn.com/media/chariottechcast/TechCast-Episode-23-03-16-2009.mp3">direct download</a></li>
</ol>
<p>This Tech Cast promotes the upcoming <a href="http://phillyemergingtech.com/">Emerging Technologies for the Enterprise</a> conference in Philadelphia, PA hosted by Chariot Solution, at which I will be speaking about <a href="http://phillyemergingtech.com/speakers.php">Seam Security</a>.</p>
<p><strong>This entry is the 100th post I have made on this blog. Add that to the list of major milestones I've hit while being 30.</strong></p>
