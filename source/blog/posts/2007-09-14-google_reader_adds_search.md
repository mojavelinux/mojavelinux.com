---
author: Dan Allen
title: Google Reader adds search
slug: google_reader_adds_search
status: Publish
breaks: false
categories: [Technology]
date: 2007-09-14 17:26:22
keywords: [Google Reader, search, filter, RSS, feeds, news reader]
---
<p>You would expect that a company that specializes in, some might even go so far as to say perfects, the art of search would include the functionality in their own news reader. However, until now, the Google Reader application has been without a search feature. At the volume in which news entries flow through the door, this oversight is practically inexcusable.</p>

<p>Well, Google finally pardoned itself and added search capability to Google Reader, as you can see in this screenshot.

<div><img src="/assets/pics/google-reader-search.png" alt="Google Reader search screenshot" /></div>

<p>The search allows you to restrict by folder (tag), feed, starred items, and shared items. It does not appear to offer date filtering, which is a big disappointment. I am hoping that other search options will come in time.</p>

<p>I have been dying to have a search feature for my blogroll for a long time. This feature will be especially powerful for the JavaBlogs feed since their search is downright horrible. Someone needs to enlighten those Atlassian folks about Lucene and its ORM complements, Compass and Hibernate Search.</p>
