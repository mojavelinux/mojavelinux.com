---
author: Dan Allen
title: 15 Minutes of Open Source Fame
slug: 15_minutes_of_open_source_fame
status: Draft
breaks: true
categories: [Open Source]
date: 2006-03-02 01:02:14
keywords: []
---
Free software on TV.  What a sight!  The next time I pause our show to catch a glimpse of another open source program being featured on a staged computer on the set of a popular network series, I am positive my wife is going to beat me over the with the remote after stealing it away from me.  I simply cannnot help myself, for it is such a proud moment.

I all began one night when we were watching <em>Law and Order</em>.  It seems that someone let a couple of hackers in the building the day the set was being prepared and they left behind a little treat for the actors.  When the camera panned to the desktop, it presented a freshly installed version of Firefox awaiting input, complete with the signature default bookmarks "Getting Started" and "Latest Headlines".

While seeing Firefox on TV is exciting, it isn't totally unexpected given its recent growth in popularity.  Anways, the screenshots are still taken on a Windows desktop, so it detracts from the moment a bit.  However, the real kicker came on e night when my wife and I were watching <em>Without a Trace</em>.  Instead of a typical Windows or Mac desktop, the creators of the show invented a completely alternative FBI branded desktop.  I had been following the use of this desktop ever since the first season, but it never revealed much more than a static background...that is, until this particular night.  As the camera focused in on the desktop, I spotted not one, but two Gnome applications!  On top was "The Gimp" and underneath, disguised with some heavy color customizations was "Evolution"!  The cat is out of the bag now!  An episode or two later the actors were watching a video in kmplayer with the Geramik GTK theme.  I realize that I am really geeking out on this sighting, but it is really something when it requires zero marketing dollars to get recognition on TV.
