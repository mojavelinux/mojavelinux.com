---
author: Dan Allen
title: Seam in Action Translations
slug: seam_in_action_translations
status: Publish
breaks: true
categories: [Seam in Action]
date: 2012-01-02 15:00:32
keywords: [Seam in Action, translations, korean, simplified chinese]
---
A long overdue post, I'm still excited to share that <a href="http://mojavelinux.com/seaminaction">Seam in Action</a> was translated (in 2010) into Korean and Simplified Chinese--two languages I can't even pretend to understand. It's pretty strange to see words that you spend countless hours revising look complete foreign to you. But exciting at the same time!

<a href="https://plus.google.com/u/0/photos/114112334290393746697/albums/5693116498840747553" style="border: 0;"><img src="/assets/pics/seam-in-action-translations-collage.png" style="width: 500px; height: 357px" alt="Seam in Action translations collage"/></a>

These books arrived in a box that clearly looked like it came out of a shipping container that was aboard an ocean carrier. My goal is that sometime soon (2012 perhaps?) I'll get to travel to where these texts originated :) (but hopefully on an airplane).

I have a whole stack of these books, so if you are in the DC area (or the US for that matter) and you want one of these books, just let me know. I'll mail it to you, free of charge.
