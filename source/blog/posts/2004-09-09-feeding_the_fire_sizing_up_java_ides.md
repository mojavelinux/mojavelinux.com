---
author: Dan Allen
title: Feeding the Fire: Sizing Up Java IDEs
slug: feeding_the_fire_sizing_up_java_ides
status: Publish
breaks: true
categories: [Java]
date: 2004-09-09 02:20:27
keywords: [Java IDE, refactoring, development, editor, project]
---
There is nothing that piques a jihad quite like a tools debate.  Whether it is a discussion comparing text editors, <a href="http://www.mozilla.org/products/firefox/why/">web browsers</a> or <a href="http://weblogs.java.net/pub/wlg/1784">Java IDEs</a>, strong opinions are always present on both sides of the table and the intensity usually escalates until someone not party to the debate has to finally cut it off.  These discussions are by far the most prolific threads on the internet.

With that introduction, you may be asking me why I would want to contribute to the debate.  I'll tell you.  I hate to see a development group select a Java IDE arbitrarily and then not exercise its true power.  After years of programming, I have finally been coerced into using an IDE for my Java development. When I began using one at work, I instantly recognized the gains in efficiency I was experiencing. While tools like Ant, Vim, and the commandline are certainly very powerful, they are not <em>context aware</em> and therefore cannot offer project-wide timesaving utilities, such as those offered by build wizards, code refactoring, code templates, and automatic deployment...at least, not without a significant amount of preparation and scripting. IDEs offer these features right out of the box, and developers should take advantage of them.

When I started at my current position, developing web applications, I was required to use JBuilder X, and it constantly pissed me off.  It pissed me off because, for how good IDEs are today, JBuilder just isn't.  My frustrations with JBuilder launched <a href="http://mojavelinux.com/wiki/doku.php?id=javaidecomparison">a quest</a> for the ideal Java IDE.  After months of evaluation, the conclusion I have to draw is that it comes down to personal choice and project requirements, so you have to evaluate the best candidate.  I hope that <a href="http://mojavelinux.com/wiki/doku.php?id=javaidecomparison">my discussion</a> can serve as a starting point for developers searching for their tool of choice and to provide feedback to those who develop the IDE editors.  More importantly, there is always something new to learn about one of these IDEs.  Each time I discover a cool new feature, I am going to add it to my list of likes and/or dislikes, so that I can easily rediscover the right tool for the job.  Today it could be Eclipse, tomorrow IntelliJ IDEA, but never JBuilder, that's for sure.
