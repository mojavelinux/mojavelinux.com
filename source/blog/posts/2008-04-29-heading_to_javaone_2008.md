---
author: Dan Allen
title: Heading to JavaOne 2008
slug: heading_to_javaone_2008
status: Publish
breaks: true
categories: [Java]
date: 2008-04-29 02:19:47
keywords: [JavaOne 2008, conference, kayak, sidestep, hotels, flights, search]
---
I almost decided to pass on <a href="http://java.sun.com/javaone/sf/">JavaOne</a> this year, but the free <a href="http://developers.sun.com/events/communityone/">CommunityOne</a> pre-conference and the <a href="http://www.jboss.org/files/events/javaone2008terms.htm">JBoss Party</a> gave me reason enough to make the trip (and the effort). Attendance at CommunityOne gets you a free pass to the General Sessions and the pavilion on the first day of JavaOne. How could I pass on free?

I am looking forward to the JBoss party as an opportunity to meet with the Seam developers, most of whom I have only communicated with via e-mail. Aside from that, though, I am going to play it cheap and not actually pay for the conference pass. I have other plans for that money<sup>1</sup>. Still, I couldn't just sit this one out. Given that I have pretty much dedicated this entire last year to Seam--and Java in general--I am very excited to mingle with all the Java enthusiasts in San Francisco this year. Catch up with me if you want to chat about Seam (and perhaps I will hook you up with a Seam in Action coupon).

<small><sup>1</sup> I need to purchase myself a computer so that I actually own the one I use.</small>

The rest of this entry is somewhat of a diary about how I made the arrangements. If you know me, you know that I freak out when it comes to selecting an itinerary. The problem is that I am too much of a perfectionist. Actually, that's not it. I just hate to discover later that I selected the wrong dates and then have to live with them. This year, I am trying to keep the cost as low as possible and still make the trip worthwhile. The problem is, CommunityOne is on Monday and the JBoss party is on Thursday, the two events I want to attend. That puts me in San Francisco for the entire length of the conference. That's when I went hunting for a deal...

I already knew about Kayak.com, and my brother clued me in on SideStep.com. Both sites look identical, but in the end I found Kayak to return more consistent results for airline tickets. Before I get to the results, I want to say a word about these meta-search engines and online travel sites in general.

The information these sites are able to bring back is truly amazing. The trouble is that the task of nailing down itinerary used to be a task (burden?) that a travel agent or assistant would handle. But with the power at your finger tips, you feel inclined to find the deal yourself. While you can potentially save a lot of money, what you lose out on is <em>time</em>. You now have to dedicate at least a half a day to flipping through result after result trying to find a deal that lets you sleep at night. If you ever hope to have a chance at success, you have to know what your plans are, or you quickly get lost in the sea of potential results. As a person who writes software, I can admit to having made the comment "We can have to user handle that task." As I user, I can testify that I feel that extra burden. Sometimes I just want to have the computer tell me when to go, how to get there, and where to stay. Are you with me?

Okay, on to the details. I used Kayak.com to find my tickets. It was giving me great prices for round-trip tickets from Washington, DC to San Francisco (right around $400). However, I was attempting to fit a very tight window of time so as to minimize the hotel cost. When I got one side of the trip locked down, the other side went up in price when using the same airline. Then I got smart and realized that using the same airline on the return trip wasn't necessary. Once I started searching the two legs individually, I had the maximum flexibility and ultimately found the best price. I am getting there and back now for $338 at the exact times I want! The outbound is even a direct flight. I am saving on hotel cost by flying out early on Monday and then taking the redeye back late Thursday night.

Hotels were a bit tougher, because the search is not just about price, but also about quality, comfort, and distance from the Moscone Center. Trying to fit all of that into a search is pretty difficult. I found that I needed to supplement my Kayak search with a Hotels.com search. I like how both offer a distance from address option (e.g. the Moscone Center), in addition to all of the other information. They still lack the ability to filter by distance and price at the same time, which would have helped me tremendously.

The biggest factor in a hotel search, aside from proximity, is the reviews. I just have to know that someone else could tolerate the hotel before I even step foot in it. After skipping over the Bay Bridge Inn (because of its name) for at least the first ten searches, I finally gave it a chance. The reviews were very positive. The real kicker, though, was that many of the reviewers recounted attending conferences at the Moscone center and how this little family-owned inn was a well kept secret for that purpose. At a room rate of $93 and wireless internet, it just couldn't be beat. It practically made the decision for me. What's interesting, though, is that room rate didn't come from hotels.com, but from the partner website of the hotel itself. Even with all of its discount parading, hotels.com still couldn't beat that deal.

So I am ready for the Java frenzy, all for under $700!
