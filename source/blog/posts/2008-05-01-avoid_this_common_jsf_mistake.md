---
author: Dan Allen
title: Avoid this common JSF mistake
slug: avoid_this_common_jsf_mistake
status: Publish
breaks: false
categories: [JavaServer Faces, Java, Seam]
date: 2008-05-01 02:51:21
keywords: [UIData, JSF, Seam, factory, context variable, bad practice, EL value expression]
---
<p>Don't access the database in a method that feeds a UIData component! (e.g. <span style="font-family: monospace;">&lt;h:dataTable&gt;</span>) I see this <a href="http://java.dzone.com/articles/use-mediator-pattern-jsf-appli">mistake</a> being made <a href="http://www.developeriq.com/articles/2008/apr/08/using-database-jave-server-faces/">all the time</a>. It's bad advice and just plain bad practice. <strong>Don't do it!</strong></p>

<p>What most people don't realize (perhaps because they are not watching the SQL log output in their ORM tool) is that value expressions are resolved more than once during the JSF life cycle--usually <em>a lot</em> more than once. Every time the value expression that feeds the UIData component is resolved in this scenario, your database takes a hit. On top of that, the result set could change depending on what you are retrieving and how you are doing it.</p>

<p>I will present a brief example and then show how to fix it using Seam.</p>

<p>Let's assume that you are using the class shown below to retrieve a list of <span style="font-family: monospace;">User</span> entities. The bean delegates the task of querying the database to a <span style="font-family: monospace;">UserService</span> instance.</p>

<pre>package example;

public class UserListBean {
    private UserService userService;

    public List<User> getUsers() {
        return userService.findAll();
    }
}
</pre>

<p>Now let's configure this class as a managed bean in the JSF configuration file. The <span style="font-family: monospace;">UserService</span> instance is injected using an EL value expression (perhaps using an EL resolver that pulls the bean from the Spring container). <em>By the way, choosing the session scope for this bean would be another bad practice that I am not going to get into here. Take a look at Seam's conversion scope if you are tempted to use the session.</em></p>

<pre>&lt;manage-bean&gt;
  &lt;managed-bean-name&gt;userList&lt;/managed-bean-name&gt;
  &lt;managed-bean-class&gt;example.UserListBean&lt;/managed-bean-class&gt;
  &lt;managed-bean-scope&gt;request&lt;/managed-bean-scope&gt;
  &lt;managed-property&gt;
    &lt;property-name&gt;userService&lt;/property-name&gt;
    &lt;property-value&gt;#{userService}&lt;/property-value&gt;
  &lt;/managed-property&gt;
&lt;/manage-bean&gt;
</pre>

<p>Feeding to <span style="font-family: monospace;">getUsers()</span> method from the <span style="font-family: monospace;">UserListBean</span> managed bean to a UIData component demonstrates the bad practice I am talking about.</p>

<pre>&lt;h:dataTable id="users" var="user" value="#{userList.users}"&gt;
  &lt;h:column&gt;
    &lt;f:facet name="header"&gt;Name&lt;/f:facet&gt;
    #{user.name}
  &lt;/h:column&gt;
&lt;/h:dataTable&gt;
</pre>

<p>In this case, the SQL log would show several queries during render and several additional queries on a JSF postback, assuming there is a UICommand component somewhere on the page.</p>

<p>The correct way to do this would be to bind the list to a context variable using a Seam factory component so that the list of users is populated exactly once per page, regardless of how many times the variable is resolved. Note that we are scrapping the managed bean definition and using a Seam component instead. The choice of page scope for the <span style="font-family: monospace;">users</span> variable ensures that the list is not refetched on a JSF postback either.</p>

<pre>package example;

@Name("userList")
public class UserListBean {
    @In
    private UserService userService;

    @Factory(value = "users", scope = ScopeType.PAGE)
    public List<User> getUsers() {
        return userService.findAll();
    }
}
</pre>

<p>Now we can use the context variable named <span style="font-family: monospace;">users</span> in the JSF view.</p>

<pre>&lt;h:dataTable id="users" var="user" value="#{users}"&gt;
  &lt;h:column&gt;
    &lt;f:facet name="header"&gt;Name&lt;/f:facet&gt;
    #{user.name}
  &lt;/h:column&gt;
&lt;/h:dataTable&gt;
</pre>

<p><strong>Please don't fall into the aforementioned trap again!</strong> If you want to know how to inject Spring beans in your Seam components using bijection, check out my latest series, <a href="http://www.javaworld.com/javaworld/jw-04-2008/jw-04-spring-seam.html">Spring into Seam</a>.</p>
