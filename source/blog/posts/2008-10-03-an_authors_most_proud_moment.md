---
author: Dan Allen
title: An author's most proud moment
slug: an_authors_most_proud_moment
status: Publish
breaks: false
categories: [Personal]
date: 2008-10-03 03:50:00
keywords: [Seam in Action, bookstore, beach]
---
<p>There are many "firsts" in a person's life, but as you grow older you have to start working harder to achieve them. I certainly didn't slack off leading up to the moment when I could take my book off the bookstore shelf and hold it in my hands.</p>

<p><img src="/assets/pics/dan-holding-book.jpg" alt="Dan holding Seam in Action"/></p>

<p>Apart from experiencing this "first", I went into the bookstore to see my book because sometimes I forget that it actually exists outside of my office. Sure enough, there is was. And it was even facing out! (Honest, I did not give myself a better shelf placement).</p>

<p><img src="/assets/pics/dans-book-on-shelf.jpg" alt="Seam in Action on the shelf"/></p>

<p>While I am enjoying this hard-earned moment, at the same time I am starting to wonder whether I will have the energy to do it again. Having said that, you are probably wondering if I ever go on vacation. Well, here's proof that I do...occasionally.</p>

<p><img src="/assets/pics/dan-walking-on-beach.jpg" alt="Dan walking on beach"/></p>

<p>Granted, the day after this picture was taken (July 2008) I hopped on a plane to go to Italy for the Seam and Hibernate Planning Meeting. Hey, at least my feet touched the sand!</p>
