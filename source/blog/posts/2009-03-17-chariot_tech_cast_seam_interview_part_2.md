---
author: Dan Allen
title: Chariot Tech Cast: Seam Interview Part 2
slug: chariot_tech_cast_seam_interview_part_2
status: Publish
breaks: false
categories: [Seam, Java, Seam News]
date: 2009-03-17 11:22:12
keywords: [Seam, interview, Chariot Solutions, JSR-299, Web Beans, Java EE]
---
<p>I didn't expect Ken Rimple for <a href="http://www.chariotsolutions.com/">Chariot Solutions</a> to be so quick in getting up part 2 of my <a href="http://techcast.chariotsolutions.com/index.php?post_id=444317">Seam interview</a>, which I introduced in <a href="http://www.mojavelinux.com/blog/archives/2009/03/chariot_tech_cast_seam_interview_part_1/">a previous entry</a>, so you get two posts in one day. If you had iTunes or your RSS feed reader working, you'd already be in the know. </p>

<p>In the <a href="http://techcast.chariotsolutions.com/index.php?post_id=444317">second part</a> of this two-part interview, we focus on the future of Java EE 6, including JSR-299, formerly known as Web Beans, and how Seam will change as the Java EE specification evolves.  We also discuss varying front-end technologies such as Flex and AJAX, and a bit about workflow.</p>

<p>Resources we mentioned in the talk include:</p>

 <ol>
    <li><a href="http://www.graniteds.org/">Granite DS</a> - A Flex remoting framework that includes support for Seam as well as other platforms such as Guice, Spring and POJOs</li>
    <li><a href="http://www.exadel.com/flamingo">Flamingo</a> - Another Flex (and JavaFX) remoting framework that exposes Seam and Spring services using a variety of protocols including AMF and Hessian.</li>
   <li><a href="http://jcp.org/en/jsr/detail?id=299">JSR-299</a>.  I emphasize that it is well worth your while to read it and something I think every Java EE developer needs to be aware of at some point in the near future.</li>
   <li><a href="http://seamframework.org/WebBeans">Web Beans</a> - The namesake of the reference implementation (RI), which is being developed by Red Hat and its community as an open source project.</li>
</ol>

<p>Thanks again to Ken for the hard work that I know went into publishing this interview.</p>
