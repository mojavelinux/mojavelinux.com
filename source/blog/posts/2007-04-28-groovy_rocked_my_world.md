---
author: Dan Allen
title: Groovy Rocked My World!
slug: groovy_rocked_my_world
status: Publish
breaks: false
categories: [Java]
date: 2007-04-28 01:03:25
excerpt: Today, I learned what my blinders have been hiding from me. After sitting through an introduction to <a href="http://groovy.codehaus.org">Groovy</a> given by Scott Davis at the <a href="http://www.nofluffjuststuff.com">No Fluff, Just Stuff</a> conference, I am experiencing a paradigm shift.  My eyes are revealing to me Wavy Gravy.
keywords: [groovy, java, ruby, syntax, dynamic language]
---
<p>Regardless of the aspect of life, trying to ensure that you take advantage of all the opportunities that knock on your door is next to impossible (especially if you are caught up killing spam all day). I consider myself remarkably diligent at staying on the leading (bleeding?) edge of the technology curve. However, like everyone else, I too have blinders. Today, I learned what my blinders have been hiding from me: <a href="http://groovy.codehaus.org">Groovy</a>. After sitting through an introduction to Groovy given by Scott Davis at the <a href="http://www.nofluffjuststuff.com">No Fluff, Just Stuff</a> conference, I am experiencing a paradigm shift.  My eyes are revealing to me Wavy Gravy.  Where have I been??? During the talk, I had to delete all the gibberish from the screen that my jaw had pounded out when it dropped onto my keyboard. I should have listened to James Strachan back in 2004 when he nonchalantly dropped me the hint, "Groovy is just so...groooovy." Since Groovy only hit 1.0 this past December, I certainly haven't missed the boat, so I'm not going to beat myself over it. But, I do need to jump into now without delay.</p>

<p>If Ruby <a href="http://www.eweek.com/article2/0,1895,1880280,00.asp">makes you happy</a>, then Groovy makes you sexy, baby! The Groovy syntax is so concise, readable, and logical that you have to wonder why you would torture yourself with native Java syntax ever again. I like how Scott Davis mediates this debate. "You should use the right tool for the job. When Groovy is appropriate, use Groovy. When Java is appropriate, use Java." Makes sense. You certainly would not want to implement Hibernate in Groovy. You could, however, write application code that uses Hibernate in Groovy. Beautiful. I can already think of a number of other possibilities for using Groovy. Unit tests implemented in Groovy are already a well-paved path, especially given that GroovyTestCase is part of the core language. JSF components written in Groovy are another terrific possibility, and would make component development much easier to stomach. My favorite idea, though, is to use Groovy to create Maven 2 plugins. For that, I can think of a perfectly fitting name: Groovy Mojo!</p>

<p>One of the reasons that my first impression of Groovy is leaving such a strong impression can be attributed to my programming background. I started out my programming career at Border's. No, really! I would move from one shelf to another picking up the next language that enticed me. At that time I was working on my P.H.D. (sort of) at USCB. I woke up one morning and realized that I was a software developer in a Material Science graduate student's shoes. So, I went out and found a job as a web developer. I spent the next 5 years developing in scripting languages, which included PHP, Perl, and JavaScript. I even created a couple of <a href="/projects">my own</a> open source projects built using these languages. There is no doubt that I was committed to them. During those years, I was almost militant in my disdain for Java. Then, I woke up one morning and realized that Java wasn't all bad, which I attribute entirely to Struts. I had seen the light! But even after handing my career over to Java, I never really turned my back on scripting. I will admit, though, that I have to hide my affinity for scripting languages while trying to evangelize Java. I feel guilty every time I smile when writing JavaScript code based on Prototype. Thanks to Groovy, I no longer have to live in shame! Finally, there is a happy medium. Finally, I can do all the cool things I did with PHP and still take advantage of all the great engineering investment in Java. I can deploy the Groovy code to a JavaEE container and get all of its benefits as well.</p>

<p>So what do I think is so sexy about Groovy? From just a very brief exposure, I picked up on...</p>

<ul>
<li>transparent generation of getter and setter methods</li>
<li>named arguments (param:"value") for methods</li>
<li>groovysh interactive interpreter (similar to Ruby's and Python's interactive shell)</li>
<li>multi-line, HEREDOC style strings using """</li>
<li>variable interpolation (EL expressions) in strings</li>
<li>system commands executed directly from a string</li>
<li>and the list goes on...</li>
</ul>

<p>Understand that the breadth of Groovy's strengths is wider than just the syntax. The killer feature is the two-way communication and integration between Groovy and Java. This language isn't a one off like BeanShell, Jython, or Rhino. You can start using this today where ever you use Java. Spring 2.0 even let's you use Groovy script files, read from the classpath, as beans that can be injected into other beans. Groovy can be compiled to Java bytecode and run just like any other compiled Java code (a very grooovy CAFEBABE). I can see myself converting all my little shell scripts into Groovy. I can see myself writing my unit tests in Groovy. I cannot wait to get started. Hold the boat!</p>
