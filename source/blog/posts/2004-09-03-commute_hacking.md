---
author: Dan Allen
title: Commute Hacking
slug: commute_hacking
status: Publish
breaks: true
categories: [Programming]
date: 2004-09-03 16:07:05
keywords: [programming, commute, Metro, MARC, open source]
---
If you read the recent blog entry about <a href="/blog/archives/2004/08/metromarc_sorry_for_the_business/">my frustrations</a> with the mass transit system in Maryland and D.C., you will know that I haven't had much in the way of free time lately.  To squeeze a little extra time out of each day, I decided to make use of all the time I was spending on the train.  So, I purchased a new, extended-life battery and began taking my laptop with me to work so that I could program during my commute, which typically lasts about 1 1/2 hours.  So if you see some crazy hacker sitting across from you on Metro/MARC hacking away at a black terminal, that would be me!

I am sure that many other open source projects can claim roots in similar environments and I would love to hear about them!  So, I am going to pose the following question as the topic for comments.  <em>What crazy places have you programmed and on what projects?</em>  It is always neat to know I little bit more about the background of various projects.  For me, it has been the <a href="/projects/studs/">Studs MVC Framework+</a> project that has been victim of my commute hacking.
