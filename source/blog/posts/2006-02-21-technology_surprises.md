---
author: Dan Allen
title: Technology Surprises
slug: technology_surprises
status: Publish
breaks: true
categories: [Business]
date: 2006-02-21 07:34:24
keywords: [testing, QA, unit test, software business, Alan Cooper, unstable programs]
---
As Alan Cooper points out, the author of <em>The Inmates are Running the Asylum</em>, we, as a society, have come to expect that machines and their interfaces with which we interact <strong>will</strong> misbehave.  Often they do so at the most inopportune times.  During the course of reading this book, I received an e-mail from my CEO about excessive "technology surprises" that we, as a company, have been springing on our users.  He asked for us to respond with ideas of how to prevent these occurances in the future.  In response, I wrote the following:

<blockquote><p>Ahh...this is an easy one.  Software is a business that is far more complex than we ever imagined.  This is because software development is really about more than just the engineering, unlike most other disciplines.</p>
<p>Civil engineers must build a bridge that people can cross, preferably one that stays up.  Software engineers have to build bridges too, but they must also wrap into their work the goals, responsibilities, and liabilities of the business itself, and be flexible enough to react to its ever changing strategy.  In short, the software <strong>is</strong> the business.</p>
<p>That's a challenge that is almost impossible for a single group of engineers to master, and to be expected to maintain, on their own.  Therefore, engineers (the ones who up to this point have been "running the asylum") need to enlist the help of a much large process.  To jump right to the point, there needs to be a well established QA cycle, that begins at the low level with unit tests and extends up to acceptance tests that client managers can utilize to ensure the customers are not hitting buggy interfaces.</p>
<p>The software industry, in is current state, is fighting very hard to move away from allowing customers to find bugs and bring the process back into the natural development cycle.  Does it take time?  <em>Yes.</em>  Does it take more money?  <em>Depends on how you look at it.</em>  Is it often slighted?  <em>Almost always.</em>  Finding and fixing issues in software will always take the same amount of time, regardless of how you draft the schedule.  The difference is whether you plan for that time, or whether your customers plan it for you.  It is better to have one application that customers love, then to have ten that they cannot rely on.  The engineers much prefer the first, but are repeatedly forced into delivering the second because "there isn't enough time."  That is to say, there isn't enough time to do it <strong>right</strong>.</p></blockquote>
