---
author: Dan Allen
title: We finally exist!
slug: we_finally_exist
status: Publish
breaks: true
categories: [General]
date: 2005-02-21 00:59:37
keywords: [google, google maps, pennington drive]
---
I have been waiting for this day to come.  Our street has now made its way onto the internet!  Perhaps now people coming to visit us won't get lost since online directions will now take them straight to our doorstep.

I decided to look up my street again while checking out one of google's latest hacks.  It is somewhat ironic that I came across this new service of Google tonight because it was just yesterday that my cousin, Bryan, asked me what new stuff Google labs had up its sleeve.  At this time, I had nothing.  I guess my response should have been "<a href="http://maps.google.com">Google Maps</a>."  Those sure are some purty maps.

Here is proof we exist:

<a style="border: 0;" href="http://maps.google.com/maps?q=laurel%2C%20md&ll=39.108002%2C-76.804272&spn=0.022308%2C0.039036"><img  src="/assets/pics/pennington_small.png" title="Google Maps showing Pennington Drive, Laurel, MD" alt="Pennington Drive, Laurel, MD" /></a>
