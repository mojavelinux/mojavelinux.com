---
author: Dan Allen
title: Broken Access Keys in Firefox 2.0
slug: broken_access_keys_in_firefox_20
status: Publish
breaks: false
categories: [Usability]
date: 2007-04-06 18:00:42
keywords: [access keys, Firefox 2.0, preferences, about:config]
---
<p>At least, that was the title of an e-mail that I received from one of our product managers yesterday.  Of course, I couldn't believe that this was true, so I immediately decided to do some digging.  As it turns out, access keys in Firefox 2.0 are not really "broken", they have just <em>changed</em>.  You just need to know the trick for using them, or reverting them to the old behavior, and perhaps an understanding of why the Firefox team made the switch.</p>

<p>For those of you who are not familiar with access keys, they are a browser feature that map sequences of keystrokes to HTML page elements so that those elements can be focused and activated using only the keyboard. An example would be associating the letter "S" with the primary submit button on the page.  Typically, this feature is activated by pressing the <strong>Alt</strong> modifier key in combination with the character assigned to the page element.  In the example, the user would press <strong>Alt+S</strong>, which would move the cursor to the submit button and activate it as if it were clicked.</p>
<p>However, the problem with this convention is that it allows the access keys to conflict with the reserved keystroke combinations that are used to control functions in either the browser or the operating system.  Therefore, the Firefox team instrumented a new default for access keys in Firefox 2.0.   Instead of being triggered by the <strong>Alt</strong> modifier key, access keys in Firefox 2.0 are now activated using the <strong>Shift+Alt</strong> combination modifier in conjunction with the character key.  That means that you now have to hold down not just two, but <em>three</em> keys to make them work!  If you don't like the new gymnastics that your fingers have to perform, then, like most features in Firefox, you can change the setting to make it work the way it used to back in version 1.5.</p>

<p>Below is a list of instructions for making the change. I will go slow so that everyone can follow along.</p>

<ol>
    <li>Open a new browser window or tab in Firefox 2.0.</li>
    <li>Type <strong>about:config</strong> in the location bar.</li>
    <li>Press enter. This will bring up a configuration window with a text input above it named <em>Filter</em>.</li>
    <li>Type <strong>ui.key.contentAccess</strong> into the Filter input box.</li>
    <li>The filter will narrow down the list of "Preference Names" to the single line item of interest.</li>
    <li>Right-click your mouse button on this line-item to bring up a small drop-down menu and then select <strong>Modify</strong> from that drop down menu.</li>
    <li>Enter a value of <strong>4</strong> in the prompt that appears and press <strong>OK</strong>.</li>
    <li>You're all done! Close that window or tab and you will be back to Firefox 1.5 behavior for access keys.</li>
</ol>

<p><img src="/assets/pics/accesskey-setting.png" style="width: 487px; height: 146px;" alt="Firefox access key preference" /></p>

<p>To find additional information about this setting, please see the <a href="http://kb.mozillazine.org/Ui.key.contentAccess">ui.key.contentAccess</a> wiki page on the MozillaZine site.</p>
