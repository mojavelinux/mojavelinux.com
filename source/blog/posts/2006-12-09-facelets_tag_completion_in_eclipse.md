---
author: Dan Allen
title: Facelets Tag Completion in Eclipse
slug: facelets_tag_completion_in_eclipse
status: Publish
breaks: false
categories: [Java]
date: 2006-12-09 23:03:06
keywords: [facelets, eclipse wtp, jspx, jsf, tld, taglib]
---
<p>For those of you who use the Eclipse Web Tools Project (WTP) to develop JSF applications with Facelets, you are likely familiar with the lack of tag library support when editing Facelets compositions.  By default, Facelets expects pages to use the *.xhtml file extension, which is the XML variant of HTML.  There are two discernible problems with this configuration.  For one, Eclipse does not recognize files of this type as pertaining to JSF. As a result, Eclipse does not offer tag completion or validation for the libraries registered in the XML namespaces.  The second issue is that these pages contain more than just XHTML content.  While it may be possible to get away with masking the signature of most tags using the "jsfc" attribute, it turns out to be more trouble than it is worth.</p>
<p>The solution that I came up with, and one that Rick Hightower <a href="http://www.thearcmind.com/confluence/display/SHJFT/Getting+started+with+JSF%2C+Facelets%2C+Eclipse+WTP+and+Tomcat#GettingstartedwithJSF,Facelets,EclipseWTPandTomcat-Createsomepages">recommends</a> in his latest round of JSF tutorials, is to use the XML variant of JSP in your Facelets pages.  Since the tag completion in Eclipse WTP is built to support JSPX, and all that Facelets requires is a valid XML page, it is a match made in heaven!</p>
<pre>
&lt;?xml version="1.0" encoding="ISO-8859-1" ?>
&lt;jsp:root xmlns:jsp="http://java.sun.com/JSP/Page"
  xmlns:ui="http://java.sun.com/jsf/facelets"
  xmlns:h="http://java.sun.com/jsf/html"
  xmlns:f="http://java.sun.com/jsf/core"
  version="2.0">
  &lt;ui:composition>
    &lt;html xmlns="http://www.w3.org/1999/xhtml">
      &lt;head>
        &lt;title>Page Title&lt;/title>
      &lt;/head>
      &lt;body>
        &lt;h:outputText value="#{backingBean.property}"/>
      &lt;/body>
    &lt;/html>
  &lt;/ui:composition>
&lt;/jsp:root>
</pre>
<p>Facelets is none the wiser that the XML being used is rooted as JSP.  The declarations appearing in the XML namespaces now provide Eclipse with what it needs to offer tag library support while dually informing Facelets which components to load.  The only requirement is to ensure that the corresponding TLD file for each tag library is located on the classpath.  Even though Facelets does not require TLDs, Eclipse needs them to load the tag libraries definitions, as it does not understand the tag configurations used by Facelets.</p>
<p>If you are going to use this trick, be sure to override the default suffix used by Facelets in the web.xml file.</p>
<pre>
&lt;context-param>
  &lt;param-name>javax.faces.DEFAULT_SUFFIX&lt;/param-name>
  &lt;param-value>.jspx&lt;/param-value>
&lt;/context-param>
</pre>
<p><strong>UPDATE:</strong> In order to get tag completion, you need to include the <a href="http://repository.mojavelinux.com/svn/public/basslineframework/trunk/bassline-facelets/src/main/resources/META-INF/facelets.tld">Facelets TLD</a> in your WEB-INF/tlds folder. The Facelets TLD is not bundled in the Facelets JAR.</p>
