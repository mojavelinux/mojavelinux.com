---
author: Dan Allen
title: Hey Java! Let's Talk Open Source
slug: hey_java_lets_talk_open_source
status: Publish
breaks: true
categories: [Open Source]
date: 2004-07-02 03:21:55
keywords: [java, eclipse, swt, open source]
---
I'm not going to beat around the bush like Sun, let's get right to it. The time is right for open source Java.  James Gosling, the father of Java and t-shirt launcher extraordinaire that we know and love, has blinders on when it comes to the adaptation of Java in the world of open source, especially on the Linux desktop. He made a claim during "The Big Question" debate at JavaOne 2004 that Java has been integrated into the Linux platform.  When it comes to the deskop, he is flat wrong. Let's face it, Linux users don't use Java (we are not talking server-side). Hell, Linux users cannot even get the J2SE out of the box right now, so how can you expect someone to start developing on it??  Even if the license to ship Java with Linux was corrected, the UI still looks like crap and doesn't play nicely in the Linux sandbox of GTK2 and QT3.

As much as Sun emphasizes compatibility and fears a code fork, fragmentation is already happening, namely because the JCP isn't open source (examples include SWT and velocity).   When I asked Erich Gamma about using SWT for Eclipse, he defended by saying "the JCP isn't open source, so we created a project (SWT) that was."  Eclipse is great because it is open source.  It is a perfect example of what Java could become if it allowed people in the door.  Eclipse is by far the most popular Java desktop application on Linux, and it isn't even using core J2SE, opting for its own open source framework. The truth is, the Linux desktop ships with (virtually) no Java programs. It should be shipping with a whole assortment of them.

<strong>Update:</strong> It was just announced today that <a href="http://www.javalobby.com/nl/archive/jlnews_20040818o.html">Sun pulled their API docs from jdocs.com</a>, more evidence that closed source Java hurts the community.

Sure, during the open source debate at JavaOne there were not that many people clapping for open source Java, many applauding the status quo.  I will tell you this. Most of the people that are proponents of an open source Java are NOT the ones attending the JavaOne conference.  Those are the people Sun needs to get interested in Java.  If anyone thought the debate favored keeping Sun the steward of Java, it was because they were preaching to the choir.

In one of the sessions at JavaOne, a speaker asked why the Java platform isn't used more in Linux, which consistently hangs on to C and C++. Perhaps the speaker should have asked why those same developers are even now choosing to move to Mono. Mono should scare Sun, not because it is better than Java or because of the breadcrumbs leading back to Microsoft, but because here you have a case where developers are transistioning from one language to another (which happens about as often as you change cars) and they are NOT using Java. Swing is ugly as hell on Linux, so you aren't going to find people choosing it when they want to get a nice screenshot up on their sourceforge.net project site (hey, Mono has GTK2). Even if GTK bindings for Swing are in the works in Java 5.0, where are the QT bindings?

Thoughout the debate there was this paranoid concern that an open Java would not be compatible, as if open source cannot follow a spec. I say that if Java was open, the community could be defining the next technologies for Java, avoiding forks like SWT and velocity. Additionally, if Java doesn't allow itself to evolve and adapt to the needs of the developers, it will become yesterday's news.  As an example in the open source community, there exists exactly one Linux kernel, and it remains both compatible and stable. Sun could play the equivalent role that Linus plays, putting down a solid fist to ensure compliance.  Most issues with compatibility and fragmentation come when people don't talk, not when code is open sourced.

In conclusion, the question remains, "Where is Java on the Linux desktop and what will get it there?"  I respond, "If you open source the process, the apps will come."
