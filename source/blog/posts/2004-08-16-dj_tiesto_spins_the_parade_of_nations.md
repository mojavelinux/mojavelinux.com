---
author: Dan Allen
title: DJ Tiesto Spins the Parade of Nations
slug: dj_tiesto_spins_the_parade_of_nations
status: Publish
breaks: true
categories: [Music]
date: 2004-08-16 02:19:33
keywords: [olympics, trance, athens 2004, opening ceremonies]
---
I have been anxiously awaiting the start of the 2004 Olympics in Athens.  I even splurged on an HDTV and HD service in preparation for the games.  However, I wasn't originally all that interested in watching the opening ceremonies.  Watching people march around a track just didn't provide much appeal.  Regardless, my wife was looking forward to a relaxing evening enjoying this broadcast.  Since watching the Olympics together with my wife is a very important part our relationship, I wasn't going to pass up this opportunity.

To my surprise, it was none other than <a href="http://www.tiesto.com/">DJ Tiesto</a> who got the party started during the <em>Parade of Nations</em>!  This appearance completely made my night!  DJ Tiesto was awesome!  I absolutely love listening to Trace and it was even better to combine this music with the Olympics, one of our most anticipated events of the year.

The network cameras focused on DJ Tiesto twice while doing his thing and both times he looked like he was having the time of his life.  From what I was hearing in the stadium, his enthusiasm definitely showed as he kept the beat rolling for more than 90 minutes.  Many of the delegations were really getting into the rhythms as they basked in the limelight, dancing or stomping down the track to the beat.  The energy in the crowd was very high, which vigorously circulated "the wave" around the statium several times.

There is no question that this was one of the most energetic, fun, and entertaining open ceremonies in the history of the Olympics...and I certainly enjoyed it.  It helps to prove my theory that electronic music <a href="http://www.opencircles.nl/english/faq.htm#faq4">injects you with endorphins</a>.  I cannot even count the number of days it has raised my spirits, helping me to be more active or productive.

<em>Way to go DJ Tiesto</em>!
