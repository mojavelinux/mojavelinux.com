---
author: Dan Allen
title: Disabling Session Cookies in Jetty
slug: disabling_session_cookies_in_jetty
status: Publish
breaks: false
categories: [Java]
date: 2006-11-26 17:25:18
keywords: [jetty, jetty6, session cookies, url rewriting, maven]
---
<p>When testing Java web applications, the preferred setting for session handling is url rewriting rather than the use of a browser cookie.  This configuration allows the developer to maintain isolated sessions across various tabs and windows, a fairly common practice in pre-production environments.  While there are many references available on the web that explain how to configure Jetty 5.x to make this switch, I found it much more difficult to pinpoint instructions for how to do it in the Jetty 6.x series.  To save folks the headache, I am documenting the configuration below.  Create the file <kbd>jetty-web.xml</kbd> and place it in the <kbd>WEB-INF/</kbd> directory.  Populate that file with the following contents.</p>

<pre>&lt;?xml version="1.0" encoding="UTF-8"?>
&lt;!DOCTYPE Configure PUBLIC "-//Mort Bay Consulting//DTD Configure//EN"
    "http://jetty.mortbay.org/configure.dtd">
&lt;Configure class="org.mortbay.jetty.webapp.WebAppContext">
    &lt;Get name="sessionHandler">
        &lt;Get name="sessionManager">
            &lt;Set name="usingCookies" type="boolean">false&lt;/Set>
        &lt;/Get>
    &lt;/Get>
&lt;/Configure></pre>

<p>The <a href="http://docs.codehaus.org/display/JETTY/Syntax+Reference">syntax</a> for the tags used in this file are detailed on the <a href="http://docs.codehaus.org/display/JETTY/Jetty+Wiki">Jetty Wiki</a>.  Much like the Spring configuration, it allows declarative assignment of properties via accessor methods.</p>

<p>By the way, if you aren't using Jetty to deploy your Maven 2 powered application, then you aren't getting the most out of your development time.  Take a couple of minutes and follow this <a href="http://jetty.mortbay.org/maven-plugin/howto.html">maven-jetty-plugin howto</a> to get setup before another minute of hacking goes into the books.</p>
