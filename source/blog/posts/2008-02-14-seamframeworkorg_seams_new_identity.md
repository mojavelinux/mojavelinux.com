---
author: Dan Allen
title: SeamFramework.org: Seam's new identity
slug: seamframeworkorg_seams_new_identity
status: Publish
breaks: false
categories: [Seam, Java, Open Source, Seam News]
date: 2008-02-14 02:50:33
keywords: [JBoss Seam, Seam framework, community, Hibernate, RichFaces]
---
<p>Today, the Seam developers announced <a href="http://seamframework.org/">SeamFramework.org</a>, the official community site for the JBoss Seam project. This site is the new home of the Seam forums, knowledge base (wiki), and FAQs. It is also the heartbeat of the Seam development project, syndicating the latest SVN commits, JIRA activity, and developer blogs.</p>

<p>The Seam community site is exciting for a number of reasons:</p>
<ol>
<li><em>Proof.</em> First and foremost, the site is powered entirely by Seam, Hibernate, and RichFaces. This commitment is referred to as "eating your own dog food." You cannot sincerely advocate a web framework if you don't feel comfortable enough to use it for your own website. So, there you have it, <a href="http://www.manning.com/affiliate/idevaffiliate.php?id=647_105">Seam in Action</a>!</li>
<li><em>Identity.</em> Like it or not, Seam lies in the shadow of JBoss, which to some is a very dark cloud. The advent of SeamFramework.org is a coming out party for Seam, helping others to see that it is an individual project and not just a subsidiary of RedHat/JBoss stuck in the confines of the JBoss Labs. Hopefully, developers will take this opportunity to become members of the Seam community, contribute to its success, and steer its future.</li>
<li><em>Consolidation.</em> In a world bubbling over with information, it is nice to be able to get your daily dose from a single source. SeamFramework.org has a variety of feeds that you can choose to digest, ranging from forum topics and blogs to SVN commits and JIRA activity. In addition, the project information is all summarized in <a href="http://seamframework.org/Community/Contribute">one place</a>. Effort is even underway to make the site accessible to mobile clients, so keep your iPhone, Blackberry, or Nokia handy!</li>
<li><em>Community.</em> In open source software, nothing is more important than the community. The Seam project is enriched with brilliant minds, but no matter how much talent you throw at a project, there is no replacement for feedback from those who have used it, either successfully or unsuccessfully, in real world situations. Whether it be bug reports, exceptions encountered, caveats, best practices, or tips, this information needs to be communicated so that Seam can better serve developers in the field.</li>
</ol>

<p>The announcement of SeamFramework.org comes shortly after the release of 2.0.1.GA and the preparation of the 2.1 branch. <a href="http://seamframework.org/userRegister_d.seam">Get involved today</a> and help make the next iteration of Seam even better!</p>
