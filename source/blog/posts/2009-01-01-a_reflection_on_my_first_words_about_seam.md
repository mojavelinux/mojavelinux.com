---
author: Dan Allen
title: A reflection on my first words about Seam
slug: a_reflection_on_my_first_words_about_seam
status: Draft
breaks: false
categories: [Seam]
date: 2009-01-01 22:09:20
keywords: []
---
<p>I was sorting some old e-mail when I came across an announcement I sent to my company after first discovering Seam.</p>

<blockquote>I will be holding a learning session on the Seam framework following
today's engineering meeting.
<br/><br/>
I am extremely excited to announce that that a JSF-based framework has
landed that promises to solve some of the core problems that the Java
team has faced while developing the new CodeRyte applications such as
VIR, Lemur and User Management.  The project is titled Seam and it is
the brainchild of Gavin King, the person who brought us Hibernate.  The
framework is open source, offered under the JBoss/Red Hat umbrella, and
has been submitted as an official JSR (official java specification).
<br/><br/>
Like Facelets, Seam is a compliment to JSF, offering extended
functionality while still adhering to the basic architecture provided by
framework.  I will discuss some of the pain points that we have
encountered and how Seam will alleviate these pains.</blockquote>
<p>Little did I know that I would eventually write close to a thousand pages on the topic a couple years later.</p>
