---
author: Dan Allen
title: 30 certainly was something
slug: 30_certainly_was_something
status: Publish
breaks: false
categories: [Personal, Seam in Action]
date: 2009-04-02 10:27:40
keywords: [reflection, birthday, conference, travel, Seam in Action]
---
<p>What a year! There's no question that year 30 was the most eventful and life changing year of my life to this point. I truly feel like I have grabbed life by the horns and got it steered in the direction I really want it to head.</p>

<p><img src="/assets/gfx/seaminaction.png" class="avatar"/>By far, the biggest accomplishment of the year was getting <a href="http://mojavelinux.com/seaminaction">Seam in Action</a> published. But writing my first book was the catalyst for many of the other events that took place. From start to finish of year 30, I presented for the first time at a software conference, which led to a half a dozen speaking engagements, more travel than I've ever done in one year, including my first ever trip to Europe, and the chance to meet a lot of prominent industry leaders. Writing the book also helped me be more prolific. I passed my 100th blog entry and posted 1000 tweets on Twitter, which I began using just before JavaOne. The gutsiest and most permanent decision I made all year was deciding to get LASIK, and in the words of Tiger Woods, "the results were fantastic". What follows is an account of this passage into my 30 somethings, most of which can be found in my <a href="http://twitter.com/mojavelinux">tweet archive</a>.</p>

<p>My first talk was an introduction to Seam which I gave at the Emerging Technologies for the Enterprise in Philadelphia, PA. I had the privilege of meeting many of the Manning authors, including Chris Richardson and Emmanuel Bernard, and my publisher, Marjan Bace.</p>

<p>I then traveled out to San Francisco to attend JavaOne, where I did a podcast for JSFCentral. It was there that I got to meet Gavin King, the founder of Seam, Hibernate, and Web Beans, for the first time. The complete list of people I meet every year at JavaOne is astonishing and too long to remember in a single flashback.</p>

<p>Over the summer I crossed the Atlantic Ocean for the very first time in my life on my way to Zurich, Switzerland to speak about the Seam-Spring integration and bijection at Jazoon. There I had the opportunity to meet Christian Bauer (Hibernate), Tedd Goddard (ICEfaces), and Thomas Mueller (H2).</p>

<p>Before I knew it I was heading back to Europe after being invited to attend a Seam and Hibernate retreat in Tuscan region of Italy (Chiusi). I had to cut my presence at my family's biannual summer vacation short to make it, but it played such a significant roll in the events that would follow that it was worth the sacrifice. At the villa I met the remainder of the Seam development team, including project led, Pete Muir, and my future manager, Rodney Russ. Little did I know I would be walking amongst the Roman ruins with Jay Balunas on the last day of the trip.</p>

<p>On the way back from Italy, and for the dwindling weeks of summer, I worked on polishing my book to get it published in time for JSFOne in Reston, VA, where I gave three talks in one conference: one on conversations and pageflows in JSF, one on the RichFaces CDK, and my Spring-Seam talk again. It felt amazing to be able to hold up my book and declare it complete and to be able to sign copies for folks, especially my parents.</p>

<p>It took some convincing, but after giving it some thought, I decided to take up Pete and Rodney on their offer to work for Red Hat seriously. I joined the company in October, which I blogged about in <a href="http://www.mojavelinux.com/blog/archives/2009/01/im_a_red_hatter/">this post</a>. I traveled to Raleigh for my orientation and got to see the Red Hat offices. One of the terms I put forth for my employment was to be sent to conferences to talk about Seam and related technologies. My first conference as a Red Hat employee would be Devoxx.</p>

<p>Back at home I spoke at my local Java Users Group (NOVAJUG) to warm up my Seam in Action presentation for the Devoxx conference. I did my first online presentation too, covering how to deploy a Seam application to GlassFish on GlassFish TV.</p>

<p><img src="/assets/pics/danandjoshbloch.jpg" class="avatar"/>In preparation for my first visit to Europe with my wife, I purchased my first DSLR camera, a Canon EOS Rebel XTi to capture high definition memories. I then headed off to Anterep, Belgium to give my 3 hour Seam in Action talk. There I got to see the Seam developers again, lots of other JBoss people, and meet tons of fans of my book. I did my first official book signing there. During one of the signings I sat next to Joshua Bloch and even got my picture taken with him. I annexed a vacation onto the end of the trip with my wife to sightsee around Belgium and France. We made stops in Brussels, Bruge, and Paris, France. I discovered the perfection of Begium chocolate and beer. The trip concluded on a high note sharing a kiss under the Eiffiel Tower at midnight.</p>

<p>Having been on the road for business I decided on a change in pace and went up to Bethel, Maine to snowboard at Sunday River with my family. That trip was padded on either side by day trips with my wife to Whitetail and Ski Liberty. I revelled at being able to leverage the flexibility of my hours with Red Hat and take a day off midweek.</p>

<p>With a break in travel following that vacation, I took the opportunity to address all of the unread messages in my overflowing Inbox, making good on a New Year's Resolution to get ahead, get organized, and get my Inbox (all messages) to zero. I also invested in a new set of eyes by getting LASIK and a pair of Oakley Straights to celebrate my discovery of the fountain of youth.</p>

<p><img src="/assets/pics/thestrip.jpg" class="avatar"/>To wrap up the year, I left my contacts and glasses behind and headed off to speak about Seam Security at back-to-back conferences, first in Las Vegas, NV then back to ETE in Philadelphia, PA. The Las Vegas trip was annexed by another vacation with my wife, which allowed us to see our first Vegas show, Mystere, and do some sinning on the Strip.</p>

<p>I'm sure I left out some stuff and many of the names of people I met, but only to spare you from too lengthy an entry. Trust that nothing has been forgotten. Let's just hope that I will have as much, if not more, to write about next year. I don't need to wait until I'm 40 to have another one of these eventful years. Life goes by too fast!</p>
