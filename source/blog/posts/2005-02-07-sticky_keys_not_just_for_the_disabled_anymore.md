---
author: Dan Allen
title: Sticky Keys: Not Just for the Disabled Anymore
slug: sticky_keys_not_just_for_the_disabled_anymore
status: Publish
breaks: true
categories: [Usability]
date: 2005-02-07 21:15:43
keywords: []
---
Although the accessibility feature <a href="http://www.usabilityfirst.com/glossary/main.cgi?function=display_term&term_id=234">sticky keys</a> has been around since the dawn of modern operating systems (and quite possiblity before that), it wasn't until recently, when I was playing with <a href="http://www.nbpepa.org/typeonehand.htm">one-handed typing</a>, that I discovered their value.  At first, I couldn't figure out how I would be able to type uppercase characters or combo commands using the one-handed typing.  When I came across sticky keys, it all came into focus.  The solution.  Sequences.

What frustrates me, and probably the reason I didn't discover them earlier, is that they are always <a href="http://www.apple.com/education/accessibility/technology/sticky_keys.html">documented</a> as a feature for disabled people who are unable to press two keys down at once.  I have found that, in fact, they are so useful that I believe they should be considered a feature for all users.  Many key combinations make you do finger acrobatics just to get all the keys down at once.  Even those moves that don't require such stretching are much simpler when done in sequence.

Sticky Keys are availabe in both <a href="http://accessibility.kde.org/features/">KDE</a> and <a href="http://www.gnome.org/learn/access-guide/2.8/ch02s02.html#dtconfig-17">Gnome</a>.    Even if you have two good hands, definitely it a try.  Your mangled wrists will thank you.

<strong>Update:</strong> Thinking back to my grade-school days, I recalled an experiment that was done in which the proficiency of finger movement was observed when one finger was held fixed contrasted with finger movement of a free hand.  The impaired speed is <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=PubMed&list_uids=11069962&dopt=Abstract">explained</a> by the workings of the sympathetic nervous system.  The nervous system is attempting to move the coorelated digit, but since it is restricted, it affects the performance of the intial movement.

Piano players and guitar players alike must combat this effect through training, which is apparently possible to overcome.  Regardless, enabling independent key strokes for combination commands will almost always out perform typical counterpart.
