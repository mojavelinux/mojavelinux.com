---
author: Dan Allen
title: '"Penguin Rush Hour"'
slug: penguin_rush_hour
status: Publish
breaks: true
categories: [Linux]
date: 2002-10-21 15:03:00
keywords: [penguins, metro]
---
Fowl plays role as unofficial mascot for downtown. Most cities have pigeons. Silver Spring has <a href="http://www.homestead.com/silverspringhistory/penguin.html">penguins</a>. You heard right. Penguins. As my friends and I traveled to the Silver Spring Metro in D.C. this past weekend for a dinner downtown, we practically tripped over a whole heard of penguins waddling home from a day at the office. This larger-than-life mural entitled "Penguin Rush Hour" by Sally Calmer is painted under the bridge entrance to the Silver Spring Metro Station in Maryland. However, says <a href="http://www.gazette.net/200029/silverspring/news/19102-1.html">Theodore Kim</a>, the 'Penguins are pretty likable [and they] don't offend anyone, I don't think.'
