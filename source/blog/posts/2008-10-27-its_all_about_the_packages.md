---
author: Dan Allen
title: It's all about the packages
slug: its_all_about_the_packages
status: Publish
breaks: false
categories: [Linux, Open Source, Usability]
date: 2008-10-27 19:17:23
keywords: [Red Hat, Fedora, RPM, yum, Ubuntu, apt, deb, yumex, synaptic, package management, installation, repositories, universe, multiverse]
---
<p>I just finished migrating my files to a Red Hat Enterprise Linux (<a href="http://fedoraproject.org/wiki/RHEL">RHEL</a>) 5 installation after more than three years of using Ubuntu (Hoary &rarr; Breezy &rarr; Dapper &rarr; Feisty &rarr; Gutsy) exclusively. Trust that by no means I am not parting ways with Ubuntu. It's just that when I came on board with Red Hat / JBoss, the HelpDesk team put notable effort into setting up my "corporate standard build" with all the cryptohome, VPN, and backup goodness ready to go. I felt that in return I could give RHEL 5 a concerted effort. Besides, it's good research to understand the product offering that Red Hat sticks out there. I have also been having a lot of difficulty lately with the stability and performance of my Linux desktop (partly Ubuntu, partly my fault) and I needed a reliable installation so that I can maximize my productivity. Since both Red Hat and Ubuntu standardize on Gnome, there isn't that much of a change to the desktop experience. Where things diverge is in the administrative tasks. The topic I want to focus on in this entry is package management.</p>

<p>When I think back to my experiences using Red Hat, Fedora, and Mandrake, what I remember most is installing RPMs. And it was more than just installing them. It was locating them, locating their dependencies, resolving conflicts, and rebuilding them when all else failed. Then there was this general discomfort over whether I choose the right RPM from the right repository and whether it would jeopardize my ability to upgrade down the road. I would say that RPM management took up 80% of my time using the distribution until I really got settled in. If I needed to change anything, that 80% tax would have to be incurred again.</p>

<p>When I switched to Ubuntu, I completely forgot about the whole package management nightmare. The reason is that Ubuntu's package management is completely centralized. You don't have to go digging through third-party RPM providers (<a href="http://dag.wieers.com/rpm/">DAG/rpmforge</a>, <a href="http://plf.zarb.org/">PLF</a> and <a href="http://rpm.pbone.net/">rpm.pbone.net</a> come to mind) to find that magic stack of RPMs you are interested in. Instead, there is just central, universe, and multiverse. The names are indicatory because they are all-encompassing repositories. It's very rare that you fire up synaptic (an apt frontend) and search for a package with no results. If it exists and can be compiled by someone, it is likely in one of the Ubuntu repositories. The multiverse repository is especially important because it has the packages we <em>really</em> want: the good, bad, and ugly gstreamer plugins and mplayer and its slew of codecs. In contrast, when you fire up YumEx  (a yum frontend) you cross your fingers hoping that the search turns up a package.</p>

<p>To me, the state of package management is the deciding difference between the two distributions, so significant in fact that it's enough to say that Ubuntu is far more enjoyable to use. Only because of my long nights spent wrestling with yum and RPM repositories can I make RHEL 5 work for me.</p>

<p>But hold the ranting. As evidenced by the recent <a href="http://www.redhatmagazine.com/2008/06/19/fedoras-extra-packages-for-enterprise-linux-%e2%80%93-the-extra-mile/">announcement</a> about the Extra Packages for Enterprise Linux (EPEL) project, Red Hat is not blind to the issue. "<a href="http://fedoraproject.org/wiki/EPEL">EPEL</a> is a volunteer-based community effort from the Fedora project to create a repository of high-quality add-on packages that complement the Fedora-based Red Hat Enterprise Linux (RHEL) and its compatible spinoffs." There is hope after all! Or is there?</p>

<p>While I applaud the the effort to mimic the centralized universe repository in Ubuntu, it comes up short because:
<ol type="a">
<li>it's not as universal as Ubuntu's repository</li>
<li>it's not complimented by a multiverse equivalent</li>
</ol>
<p>Even with EPEL activated in yum, you still have to keep your fingers crossed that the package you are searching for will show up in the results. And because of Red Hat's ethical stance to keep a vacuum between Red Hat distributions and "ugly" packages (non-free packages or packages with license problems), you still have to look elsewhere. Unfortunately, EPEL isn't yet entirely compatible with DAG/rpmforge, which you absolutely must use to get all your not-so-legal-yet-necessary packages. Thus, memories of RPM conflicts and missing dependencies once again keeps you up at night.</p>

<p>Ubuntu is definitely leading in the category of package management. But I think that Red Hat / Fedora only needs to make a small shift to even the playing field. The EPEL needs to grow to be more comprehensive to match the universe repository and it needs to be compatible with DAG/rpmforge, which would become the unofficial multiverse-equivalent for RHEL. Then, finally, the package management nightmare will be over. Oh, and it would be nice if RHEL had some checkboxes to get this all setup rather than making the user to the legwork of getting the repositories setup.</p>

<p><strong>Update:</strong> In fairness, I have not tried Fedora 9 or 10 yet, so the package management situation may very well be improved. The focus of this entry was on the most recent RHEL, which is Red Hat's supported option. Regardless, my point stands: package management should be seamless, and it should include the packages the average Joe really needs (i.e., multiverse).</p>
