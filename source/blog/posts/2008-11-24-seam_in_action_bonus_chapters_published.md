---
author: Dan Allen
title: Seam in Action Bonus Chapters Published
slug: seam_in_action_bonus_chapters_published
status: Publish
breaks: false
categories: [Seam in Action, Java, Seam]
date: 2008-11-24 11:16:32
keywords: [Seam in Action, bonus chapter, jBPM, Spring]
---
<p>I'm a bit late on delivering this news, but as of two weeks ago, the two bonus chapters from Seam in Action (<a href="http://manning.com/dallen/SeamIACH14_bonus.pdf">chapter 14</a> and <a href="http://manning.com/dallen/SeamIACH15_bonus.pdf">chapter 15</a>) have been published on the <a href="http://mojavelinux.com/seaminaction">Seam in Action website</a> after being polished, typeset, distilled to PDF. The chapters have been available online as Word documents since the release of the book, but have since been improved quite significantly, so I encourage you to scan through them even if you have already done so.</p>
<p>Chapter 14 covers Seam's business process integration (jBPM), starting off with a very gentle introduction to business processes that will be sure to get you excited about trying out a business process in your application. Seam's integration with Spring is covered inside and out in chapter 15, giving you a plethora of ideas on how to leverage the two frameworks together.</p>
<p>The best part is, the chapters are <b>free</b>! That means there are now <b>four</b> free chapters available! (1, 7, 14, 15) And those are complete chapters, not excerpts of a chapter with vital pages missing. There are two reasons for this. First, I really want you to get the information you need to use Seam effectively. It's also because I want you to appreciate how in-depth I went with this book so that you are confident in your decision to go "all in" and purchase it (<a href="http://mojavelinux.com/seaminaction">Manning.com</a>, <a href="http://www.amazon.com/dp/1933988401/?tag=mojavelinux-20">Amazon.com</a>). And please, after you read the book, consider posting a review on Amazon to share with others what you thought of it.</p>
