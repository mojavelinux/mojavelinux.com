---
author: Dan Allen
title: XM is Everywhere!
slug: xm_is_everywhere
status: Publish
breaks: true
categories: [Technology]
date: 2005-01-13 23:29:41
keywords: [XM radio, satellite, roady2, commute, trance, swing]
---
<img src="/assets/pics/xmradio.gif" alt="XM Radio" class="avatar" /> Well, at least for me it is. Over the holiday, I received Delphi's <a href="http://shopdelphi.com/products/consumers/xmroady/">Roady2</a> and a subscription to the <a href="http://ww.xmradio.com">XM</a> satellite music service and I must say, I have either been enlightened or completely spoiled.  I don't think I can ever go back to local radio, just like I could never go back to a modem or a hardwired LAN.  In fact, I have almost completely forgotten about local radio and therefore didn't even take notice when 99.1 WHFS, one of Washington's biggest radio stations in history, abruptly disappeared from the airwaves.

The Roady2 is essentially a satellite to FM converter, which can then be picked up by any nearby radio tuned to one of the select frequencies on which it broadcasts.  The FM transmitter technology has gone through several iterations in the marketplace of varying qualities, but has now emerged as an acceptable replacement to hardwired audio cables.  (It uses the same technology as the new <a href="http://www.monstercable.com/productpage.asp?pin=2084">iCarPlay</a> from Monster).  As long as I have my Roady and a nearby radio, I have XM.  This means in my car, anywhere in my house, and wherever else I choose to take it.  Finally! I can listen to electronic music (trance, eurodance, progressive) at any point during the day, instead of only when I am in front of my computer tuning in to <a href="http://di.fm">DI</a>.  But it doesn't stop there, because XM actually has a lot of other great stations as well, including swing and big band.  (One of XM's strong suits is its superb programming).  Sometimes, I just get really crazy and listen to a little bit of African, bluegrass, and world music.  It definitely keeps me entertained and gives me something to look forward to every morning on my trip to work.

I believe that the future is all about directed and comprehensive programming.  I can get the type of music or talk radio I want, whenever I want.  Instead of waiting for the hourly traffic and weather, I immediately tune to the channel for my city, catch the report, and go back to my previous muse.  For those of you out there who cannot imagine paying for radio, let me allow your answer to this next question lead the way.  How do you react when someone tells you that they don't have high-speed internet?  <em>Exactly.</em>
