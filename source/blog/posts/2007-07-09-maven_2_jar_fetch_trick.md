---
author: Dan Allen
title: Maven 2 JAR Fetch Trick
slug: maven_2_jar_fetch_trick
status: Publish
breaks: false
categories: [Java]
date: 2007-07-09 00:26:54
keywords: [Maven 2, H2, groovy, POM file, pom.xml, repository]
---
<p>This tip probably belongs under the category "stupid Maven 2 tricks." While writing a tutorial that uses Maven 2, I thought to myself, "There must be a way to pull an artifact into the local Maven 2 repository without a build script." Unfortunately, a build script is required, but with a little shell magic, we can hide those details.</p>

<p>So what's the basic premise? First, you need to create a minimalistic Maven 2 POM file (pom.xml). The next step is to add the dependency that you would like to fetch. Finally, you execute the dependency:resolve Maven 2 goal. (By the way, I highly recommend checking out the dependency plugin. It can produce very useful output in a pinch.)</p>

<p>Here is the minimalistic POM file that you can use to pull down the H2 database jar file.</p>

<pre>
&lt;project&gt;
    &lt;modelVersion&gt;4.0.0&lt;/modelVersion&gt;
    &lt;groupId&gt;null&lt;/groupId&gt;
    &lt;artifactId&gt;null&lt;/artifactId&gt;
    &lt;version&gt;0&lt;/version&gt;
    &lt;dependencies&gt;
        &lt;dependency&gt;
            &lt;groupId&gt;com.h2database&lt;/groupId&gt;
            &lt;artifactId&gt;h2&lt;/artifactId&gt;
            &lt;version&gt;1.0.20070429&lt;/version&gt;
        &lt;/dependency&gt;
    &lt;/dependencies&gt;
&lt;/project&gt;
</pre>

<p>Now execute the Maven 2 goal to resolve artifacts, which will download the dependencies declared in this project file and place them into the local repository . I have also added the flag to display the absolute paths to those files, just in case you need to copy it for use in a configuration wizard.</p>

<p><strong>mvn dependency:resolve -DoutputAbsoluteArtifactFilename=true</strong></p>

<p>But if you use the groovy script below, you can get this all done in one line without having to create a POM file at all.</p>

<p><strong>mvnfetch -a com.h2database -g h2 -v 1.0.20070429</strong></p>

<pre>#!/usr/bin/env groovy

def cli = new CliBuilder(usage: 'mvnfetch [OPTION]')
cli.a(longOpt:'artifactId', args:1, required:true)
cli.g(longOpt:'groupId', args:1, required:true)
cli.v(longOpt:'version', args:1, required:true)

def options = cli.parse(args)

def artifactId = options.a;
def groupId = options.g;
def version = options.v;

new File('.mvnfetch.xml').write("""
&lt;project&gt;
  &lt;modelVersion&gt;4.0.0&lt;/modelVersion&gt;
  &lt;groupId&gt;none&lt;/groupId&gt;
  &lt;artifactId&gt;none&lt;/artifactId&gt;
  &lt;version&gt;1.0&lt;/version&gt;
  &lt;dependencies&gt;
    &lt;dependency&gt;
      &lt;groupId&gt;${groupId}&lt;/groupId&gt;
      &lt;artifactId&gt;${artifactId}&lt;/artifactId&gt;
      &lt;version&gt;${version}&lt;/version&gt;
    &lt;/dependency&gt;
  &lt;/dependencies&gt;
&lt;/project&gt;""")

def proc = "mvn -f .mvnfetch.xml dependency:resolve".execute()
proc.in.eachLine { println it }
proc.waitFor()

"/bin/rm .mvnfetch.xml".execute()</pre>

Once again, Groovy comes in handy for its scripting magic.
