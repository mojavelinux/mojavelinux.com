---
author: Dan Allen
title: The Road to Enlightenment
slug: the_road_to_enlightenment
status: Publish
breaks: true
categories: [General, CSS &amp; Design]
date: 2004-07-09 22:30:41
keywords: [web standards, CSS, css Zen Garden, launch, mojavelinux.com]
---
<blockquote><p>Littering a dark and dreary road lay the past relics of browser-specific tags, incompatible DOMs, and broken CSS support.</p></blockquote>

This introduction can be found on the notable <a href="http://www.csszengarden.com">CSS ZenGarden</a>, a website which serves as a reference implementation for standards compliance web design.  The new version of mojavelinux.com is my leap into this new and exciting dimension of content delivery.  You will find that the presentation of the site is controlled entirely by CSS and the content remains completely semantic.  It is also the first time I have implemented a blogging framework and organized all my files under a single, seamless design.  While it comes a few days earlier than expected, I trust that you will pardon the dust and enjoy the content which I have prepared for your reading pleasure.  If you are having any trouble accessing existing resources, please make use of my <a href="/about/contact.php">contact form</a> to drop me an email.

On that note, I introduce you to the <strong>NEW</strong> mojavelinux.com!  Become one with us!
