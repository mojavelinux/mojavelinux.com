---
author: Dan Allen
title: Linux is Smarter Than Me
slug: linux_is_smarter_than_me
status: Publish
breaks: true
categories: [Linux]
date: 2004-04-04 03:00:00
keywords: [daylight-savings time, linux savvy]
---
Tonight, while I was using my computer, I noticed that my clock was an hour fast.  I reved up my clock applet to set the date and time.  However, when I tried to adjust the clock to 2:45am it game me an error "Can not set date."  WTF?

At first I thought it was a problem with the program, so I tried a few more things and received the same cryptic message.  I knew it was time for some commandline action using the `date` command.  I quickly scanned the man page and issued

<pre>bash$ date 04040245</pre>

only to be denied with the message "invalid date."  At this point I was thoroughly confused.  Other dates worked, but not the hours between 2:00 and 3:00 am on April 04, 2004.

I went to my roommate and he informed me that tonight was a daylight-savings time change.  Then it all made sense.  The hour between 2:00 am and 3:00 am on Apr 04, 2004 doesn't technically exist.  Mathmatically it is a legal date, but according to the calendar rules, it does not exist.  Incredible.  Hence, I arrive at my conclusion; Linux is smarter than me.
