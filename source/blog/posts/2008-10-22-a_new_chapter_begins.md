---
author: Dan Allen
title: A new chapter begins
slug: a_new_chapter_begins
status: Publish
breaks: false
categories: [Personal]
date: 2008-10-22 02:01:00
keywords: [Red Hat, JBoss, Seam, employment, career]
---
<p><img src="/assets/pics/redhatboxfront.jpg" alt="Red Hat welcome box (front)" style="width: 450px; height="338px;"/></p>
<p><img src="/assets/pics/redhatboxback.jpg" alt="Red Hat welcome box (back)" style="width: 450px; height="337px"/></p>
