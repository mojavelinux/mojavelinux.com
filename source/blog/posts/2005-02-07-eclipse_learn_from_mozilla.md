---
author: Dan Allen
title: Eclipse, Learn from Mozilla
slug: eclipse_learn_from_mozilla
status: Publish
breaks: true
categories: [Usability, Open Source]
date: 2005-02-07 20:51:42
keywords: [Eclipse, Firefox, download, advocacy]
---
When I first began advocating the Mozilla web browser, the biggest obstacle I encoutered was helping those interested actually find the <a href="http://web.archive.org/web/20020601102230/www.mozilla.org/releases/">download link</a>.  No matter how many times I told these folks, they still could not seem to locate the link.  In truth, they probably were afraid of downloading the wrong file and therefore didn't even try to click on anything.

After several years of watching people use computers, I have learned that it is necessary to make the target button <strong>VERY</strong> clear.  You would be absolutely amazed at how long it takes a person's eye to find a target on the page the very first time, and often, this is your program's only shot.  There is nothing shameful about a really huge button.  Case in point, Mozilla.  The <a href="http://www.mozilla.org">download for firefox</a> takes up nearly the entire first half of mozilla.org, but at least people cannot claim that the link was too hard to find.  Mission accomplished.

I have <a href="http://simon.incutio.com/archive/2005/01/20/eclipse">read</a> that Eclipse is working on a <a href="http://www.eclipse.org/downloads/index2.php">prototype</a> for the new download page, but I have to say, even that page is overwhelming.  The feedback I usually receive about Eclipse is that the SDK is either too large or it doesn't seem like the right download for doing development.  I happen to agree.  Why do most developers need the whole SDK just to write some Java code?  Eclipse should make a "developer's" package available that just includes the platform and the JDT (since that is the primary plugin for Eclipse).
