---
author: Dan Allen
title: Podcast from JavaOne
slug: podcast_from_javaone
status: Publish
breaks: false
categories: [Seam in Action, Java, JavaServer Faces, Seam, Seam News]
date: 2008-08-22 10:05:50
keywords: [Seam, Spring, Seam in Action, JSF 2, persistence context, Aspect J, JavaOne, annotations, EJB 3, accessible, podcast, interview]
---
<p><a href="http://www.jsfcentral.com/articles/allen-08-08.html"><img src="/assets/gfx/podcast.png" alt="Podcast icon" class="avatar" border="0" style="margin: -2px 15px 0 0;"/></a>In between crashing parties at JavaOne, I was tracked down by Kito Mann to do a <a href="http://www.jsfcentral.com/articles/allen-08-08.html">podcast interview</a> for <a href="http://jsfcentral.com">JSFCentral</a>. Kito is the author of <a href="http://www.manning.com/affiliate/idevaffiliate.php?id=647_10">JavaServer Faces in Action</a>, runs JSFCentral, and holds a seat on the JSF 2 expert group. He asked me about Seam, my book (<a href="http://www.mojavelinux.com/seaminaction">Seam in Action</a>), and Seam's integration with Spring. Kito has diligently made the <a href="http://www.jsfcentral.com/articles/allen-08-08.html">transcript</a> available for those of you who don't suffer from long commutes.</p>
<p>The best way to extract loads of good information from me is to catch me in the middle of one of my rants. The fervor of JavaOne had me fired up on this particular evening, so this episode is sure to please. I was especially excited because this is my first podcast. I can proudly say that I am no longer a podcast virgin!</p>
<p>In the podcast, I discuss my book and what to expect from it, how Seam makes Java EE accessible, why integration with Spring is desirable and some of the differences in philosophies of the two frameworks (and its creators), how to respect the persistence context, and some of my wishes for JSF 2. I even talk some about my PHP port of Struts, a fling from my past. Pay close attention to what I have to say about the syntax for the view technology in JSF 2 because it brings to light an oversight in every view technology I know of.</p>
<p>I hope that this podcast gives you incentive to check out my book. As I say during the interview in the words of my readers, "nothing has gone into the detail and we have spent a lot of time with these problems and you have addressed [them in Seam in Action]." I really did labor over this book for my readers. This interview should help you understand why I did.</p>
