---
author: Dan Allen
title: Studs Has Landed!
slug: studs_has_landed
status: Publish
breaks: true
categories: [Programming]
date: 2004-07-23 02:04:43
keywords: [studs, struts, MVC, object-oriented, PHP, stratus, horizon, phase, PHP server pages]
---
Well folks, it's finally here!  After almost a year of ups and downs scratching an itch for a better object-oriented framework for PHP, I am pleased to announce the initial release (0.9) of the <a href="/projects/studs/">Studs MVC Framework+</a>.  I use the '+' to designate the fact that the project has become much more than just a port of Apache's Jakarta Struts to PHP.  In fact, I ended up porting a good portion of the Java Servlet stack, including a JSP parser and a long-term application context (within reason).  Think about that.  JSP custom tag libraries in PHP, including the JSTL expression language!  Add to that logging, runtime exception handling, an XML digester and an enhanded object environment and things start to get very interesting.

Why Studs?  People are going to ask, so I am going to explain.  <strong>Experience.</strong>  I wanted to learn deep down how Java Servlets and Struts worked and there is no better way to accomplish this than by reviewing the code.  More importantly, I learned about the design concepts behind this source code, which in the end is much more valuable.  <strong>Practice.</strong>  After a few false starts, I recognized the only way I was going to make this happen was by starting small.  I followed one of the principles of eXtreme Programming and did <em>the simplest thing that could possibly work.</em>  I continually refactored the design until I reached the point of compatibility with the design of Struts.

While there is still work to be done, I encourage anyone interested in object-oriented design, J2EE or web frameworks to give Studs a try.  Start with <a href="http://sourceforge.net/project/showfiles.php?group_id=76836&package_id=77681">studs basic</a>, which is a simple "Hello World" application that can be run on any webserver with PHP 4 installed.  Give it a try today!
