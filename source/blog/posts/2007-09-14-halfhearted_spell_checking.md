---
author: Dan Allen
title: Half-hearted spell checking
slug: halfhearted_spell_checking
status: Publish
breaks: false
categories: [Open Source]
date: 2007-09-14 04:44:03
keywords: [spell check, firefox, rant, GTK]
---
<p><strong>Disclaimer:</strong> This entry has become more of a satire. Clearly there is something strange with my desktop configuration that is preventing Firefox on Linux from working as designed. I still feel that the situation is rather amusing, so with that...</p>

<p><img src="/assets/pics/spell-check-feature.png" style="border: 1px outset black; margin-top: 5px; margin-left: 5px;" class="avatar" />I just love the spell check support in Firefox 2. I am witnessing it now as I fat-finger a couple of words in this entry. Firefox will underline a word that is spelled wrong, but it offers no support to correct it. <em>What's that all about? What were the developers thinking when they developed this feature?</em> It would be like someone coming up to you and saying, "You have something on your face, but if you want to know where, you are going to have to go find a mirror." Bastards.

<p>Just to prove it to you I have provided a screenshot.</p>

<p><strong>Update:</strong> It appears that this is an integration feature with GTK-spell, so it has nothing to do with Firefox itself. I have tested it out on a couple of different computers running the latest version of Ubuntu. I have found that it works on some installations and not on others. Perhaps there is some magic package that is missing. Either way, it is downright annoying.</p>
