---
author: Dan Allen
title: Spring into Seam series published
slug: spring_into_seam_series_published
status: Publish
breaks: false
categories: [Seam in Action, Java, Seam, Seam News]
date: 2008-04-18 14:18:44
keywords: [Seam in Action, JBoss Seam, Spring, integration, article]
---
<p>Needless to say, I have been busy. A fair amount of that time has been dedicated to the production of <a href="http://www.mojavelinux.com/seaminaction">Seam in Action</a>. But that doesn't mean I want to leave my readership left hanging while I push characters around on the screen. So today, I have news that you are going to be <em>thrilled</em> to hear...</p>

<p>As you may have gathered, I am a big fan of both Spring and Seam. In fact, when I first learned Seam, I found myself torn between the two. That was, until I discovered that it is possible to use them simultaneously, each for their strengths. To advocate this integration, I decided to spin off a portion of the online Spring integration chapter from <a href="http://www.mojavelinux.com/seaminaction">Seam in Action</a> as a three-part series for JavaWorld titled <em>Spring into Seam</em>, set to be released over a three week period. The <a href="http://www.javaworld.com/javaworld/jw-04-2008/jw-04-spring-seam.html">first part</a> in the series explains how to build a Spring-Seam hybrid component, a managed object that benefits from functionality provided by both the Seam and Spring containers. In <a href="http://www.javaworld.com/javaworld/jw-04-2008/jw-04-spring-seam2.html">Part 2</a>, you'll learn how to infuse state into traditionally stateless Spring beans by allowing them to reside in Seam contexts, and how to inject stateful Seam components into Spring beans. Finally, in <a href="http://www.javaworld.com/javaworld/jw-05-2008/jw-05-spring-seam3.html">Part 3</a>, you'll learn how to integrate Seam and Spring at the most basic level by having them share a persistence manager.</p>

<p>The good news about this series is that not only do you get the content for free, but you also get it sooner in its final revised form! By the end of the series, you will walk away as an enlightened developer, no longer interested in the trite Spring versus Seam debates, but rather looking for more ways to extract value out of the unmatched features of both frameworks. To you, it's all gravy!</p>

This post is syndicated from <a href="http://www.amazon.com/gp/blog/post/PLNK3KQSDA96PTNHF">Dan Allen's Amazon Blog</a>.
