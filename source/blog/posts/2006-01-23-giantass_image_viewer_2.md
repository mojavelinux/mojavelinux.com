---
author: Dan Allen
title: Giant-Ass Image Viewer 2
slug: giantass_image_viewer_2
status: Publish
breaks: true
categories: [Javascript]
date: 2006-01-23 10:25:46
keywords: [pan, zoom, GSV, google maps, image tiles, image viewer]
---
<img src="/assets/pics/gsv.gif" alt="GSV2" class="avatar" /> I am proud to announce that the next generation of the Giant Ass Image Viewer has been released to the world!  For those of you who are not familiar with the <a href="http://mike.teczno.com/giant/pan/">original program</a>, the Giant-Ass Image Viewer (GSV) is a javascript driven user interface for viewing large, high-resolution images, behaving very similarly to the Google Map interface.  This interface allows a browser client to pan and zoom an image that is much larger than the screen, fetching tiled sections on demand.  As one edge of the image moves off the screen in one direction, new portions of the image appear on the opposing side.  Using this type of interface allows the image being viewed to theoretically be infinite.  As such, the taxation on the network connection is minimized tremendously by the fact that only portions of the image are needed at any given time.

This next version offers a host of new features that were absent in the first cut, in addition to a new code design.  (I want to add that the original program is an excellent script and the additions in no way dimish the accomplishments of the author.  So before continuing on, hats off to <a href="http://mike.teczno.com">Michal Migurski</a>).

After hacking on GSV for a while, I got to the point where the code was starting to get tied in knots as I tried to weave in new features.  Before continuing on, I decided that it would benefit tremendously from a new design, so I didn't waste a minute before rewriting the code from the ground up as a javascript object.  The program now has a very clean and readable design that is quite extensible.  It makes use of the observer pattern as well, that notifies listeners of events such as move and zoom.  This pattern allows other programs to track its status.

So what else is different?  <a href="/projects/panojs">GSV 2</a> adds a significant boost in performance by using more concise logic and throttling the response to mouse move events.  It also fixes rounding errors in the tile placement that was causing artifacts, takes advantage of image preloading and caching, caps the maximum zoom level, disables pan events for areas outside of boundary, recenters on a double click of the mouse, smooth scrolls when repositioning, displays and optional loading image while fetching an uncached tile, fits the viewport to the size of the window, and uses a cross-browser grabbing mouse cursor, amidst other changes.  Finally, the workhorse of the program, the python <a href="/cooker/demos/gsv/scripts/tilemaker.py">tilemaker</a> script, has also been rewritten to be more informative and flexible.

I will be contributing the code back to the original author, Michal Migurski, so the location of the project page is undecided at this point.  However, I have made a <a href="/cooker/demos/gsv/">demo</a> available in the cooker, which offers a NASA view of the Mars venturer, Spirit.
