---
author: Dan Allen
title: Don't overlook this framework
slug: dont_overlook_this_framework
status: Publish
breaks: false
categories: [Seam News, Java, Seam, Seam in Action]
date: 2010-06-10 12:19:47
keywords: [Seam, interview, Java EE, Weld]
---
<p>During my long flight to Frankfurt last month, I responded to an interview by <a href="http://community.jboss.org/people/asiandub">Jan Groth</a> about Seam for the German <a href="http://www.javamagazin.de">Java Magazin</a>. In the interview, I reflect on the value of Open Source, how I got involved in Seam and where we are headed with Weld and Seam 3. I ended up writing so much (surprise, surprise) it had to be split into two parts. I justify my thoroughness in my first response:</p>
<blockquote>I'm going to be a little long-winded [...], but I think it's important to communicate what led me to Seam because it says a lot about the software itself.</blockquote>
<p>The full, 2-part interview is available online at the <a href="http://jaxenter.com">JAXenter Magazine</a> website:</p>
<ul>
<li><a href="http://jaxenter.com/i-didn-t-want-others-to-overlook-this-framework-11947.html">Part 1: I Didn't Want Others to Overlook this Framework</a></li>
<li><a href="http://jaxenter.com/cdi-and-seam-3-are-the-future-of-both-the-seam-community-and-red-hat-13472.html"> Part 2: CDI and Seam 3 are the Future of Both the Seam Community and Red Hat</a></li>
</ul>
<p>In <a href="http://jaxenter.com/i-didn-t-want-others-to-overlook-this-framework-11947.html">part 1</a>, I share the thoughts I had after reading the Seam reference documentation the first time:</p>
<blockquote>It's like the Seam developers were reading my mind. [...] I felt like a kid in a candy store. I started banging out applications in no time and I was happy with how they looked.</blockquote>
<p>I go on to explain that it wasn't just enough for me to know about it. I wanted other to benefit too.</p>
<blockquote><p>I didn't want others to overlook this framework given how much it helped me. While the manual made sense, I knew it wasn't enough. [...] I decided to yell from the mountaintops by writing <a href="http://www.ibm.com/developerworks/views/java/libraryview.jsp?search_by=seamless+jsf">a series about it</a> for IBM developerWorks.</p><p>The overwhelming feedback from that series made it pretty clear, someone needed to write <a href="http://mojavelinux.com/seaminaction">a book about it</a> (a higher mountaintop) that was going to explain every last detail. I took on that challenge (and believe me, it was quite a challenge.) [...]</p><p>I quite literally immersed myself in Hibernate, JPA, EJB 3, JTA, JSF and Java EE 5 in general. I came to appreciate the value of the platform, the remaining limitations and what Seam had to do to fill in the gaps.</p></blockquote>
<p>I squeeze in a short review of the <a href="http://artofcommunityonline.org">Art of Community</a> at the end of the first part because it really sums up my vision of this project:</p>
<blockquote>The Art of Community is a truly inspiring book and it reminded me why I love doing what I do...it's really about the people and the ideas. We are not just writing software for the community and publishing it as Open Source. It's the community's software, solutions to problems that come from the real world. We want to support and foster that engine and the Seam Community Liaison is the spark plug of the engine and ensures it's properly greased.</blockquote>
<p><a href="http://jaxenter.com/cdi-and-seam-3-are-the-future-of-both-the-seam-community-and-red-hat-13472.html">Part 2</a> dives more into the technical details and where we are headed.</p>
<blockquote>CDI and Seam 3 are the Future of Both the Seam Community and Red Hat.</blockquote>
<p>I explain our standards strategy as a defender of choice:</p>
<blockquote>Red Hat is strong when the Java EE platform is strong. Our strategy does not depend on the platform remaining weak to justify the existence of our projects. It's quite the opposite. If we make one of our own projects obsolete, or some part of it, that's progress.</blockquote>
<p>To quote <a href="http://community.jboss.org/people/jbalunas@redhat.com">Jay Balunas</a>, it's cyclic:</p>
<blockquote>Find a void. Fill the void. Standardize the fill.</blockquote>
<p>I liken Seam modules to JSF UI component libraries and explain how we will ensure portability:</p>
<blockquote><ol><li>Seam 3 is primarily built on the CDI portable extension SPI, insulated from handling low-level concerns that run a high risk of breaking portability.</li><li>One of the principal requirements of Seam 3 modules is portability.</li><li>The <a href="http://jboss.org/arquillian">Arquillian</a> in-container test framework allows modules to be continuously tested against an array of application servers.</li></ol></blockquote>
<p>But with the improvements to Java EE, the question comes up whether Seam is still needed:</p>
<blockquote>Is Java EE 6 a solid platform? Yes. Will you still need extensions? Yes. And CDI was designed specifically to be able to root and foster such an ecosystem. You can even write your own extensions, which really wasn't possible with Seam 2. Now we just need a <a href="http://in.relation.to/Bloggers/TheCDIExtensionsDirectory">plug-in site</a> ;)</blockquote>
<p>I'm very excited about all the innovation that is taking place in the <a href="http://community.jboss.org">JBoss Community</a> right now and I encourage you to become a part of it. You never know where it will lead, but I can guarantee you it will open doors for you.</p>
<p><em>This post is syndicated from <a href="http://community.jboss.org/people/dan.j.allen/blog/2010/06/10/dont-overlook-this-framework">my JBoss Community blog</a>.</em></p>
