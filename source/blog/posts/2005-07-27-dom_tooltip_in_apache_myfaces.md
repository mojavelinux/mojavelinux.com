---
author: Dan Allen
title: DOM Tooltip in Apache MyFaces
slug: dom_tooltip_in_apache_myfaces
status: Publish
breaks: true
categories: [Open Source, Java, Javascript]
date: 2005-07-27 11:11:44
keywords: [domTT, tooltip, JSF, MyFaces, Apache]
---
The <a href="http://www.mojavelinux.com/projects/domtooltip">DOM Tooltip</a> library has been selected as the tooltip widget for a new <a href="http://myfaces.apache.org">Apache MyFaces</a> component named <a href="http://myfaces.apache.org/sandbox/schedule.html">Schedule</a>.  This component is described in the javadoc as " A schedule component similar to the ones found in Outlook or Evolution."

The recent release of DOM Tooltip under the Apache 2.0 license was part of this release into the MyFaces sandbox.  In case you need to see to believe, here is <a href="https://svn.apache.org/repos/asf/myfaces/sandbox/trunk/src/java/org/apache/myfaces/custom/schedule/resource/javascript/">proof</a>.
I am extremely thrilled about this pairing.
