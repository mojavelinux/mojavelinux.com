---
author: Dan Allen
title: Linux Springing Up
slug: linux_springing_up
status: Draft
breaks: true
categories: [Linux]
date: 2004-09-14 15:55:01
keywords: []
---
There is no doubt that "it" is finally happening, where the "it" is Linux becoming generally present.  For the longest time, Linux users had to deal with the fact Linux wasn't recognized by the general population, including major coorporations.  While Linux does not need the approval of anyone to be successful in its own right, it feels good to be noticed.

Just the other day I was in Lowe's Home Improvement store when I noticed a very familiar face.  Looking over the shoulder of the employee doing a product check for me, I immediately recognized the desktop to be the one I use, KDE.  Sure enough, the computer was running Linux.

A few weeks before this encounter, I was getting my high speed internet setup with Comcast when I ran into a snag.  While searching on Comcast's site for the appropriate support number, I noticed <a href="http://faq.comcast.net/faq/answer.jsp?name=18028&cat=Connection&subcategory=1">this page</a> that explains how to setup the comcast network on Linspire using KDE.
