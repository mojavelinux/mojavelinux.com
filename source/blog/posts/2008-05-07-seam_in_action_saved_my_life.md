---
author: Dan Allen
title: '"Seam in Action saved my life"'
slug: seam_in_action_saved_my_life
status: Publish
breaks: false
categories: [Seam in Action, Java, Seam]
date: 2008-05-07 03:58:05
keywords: [Seam in Action, review, praise, writing]
---
<p>If you know me, you know that I don't really like to talk about my successes. I guess part of it is because I absolutely hate when I am the target of marketing, so I like to avoid putting other people in that situation. However, when I read the following <a href="http://www.seamframework.org/Community/TwoFreeChaptersFromSeamInActionAvailable#comment20624">post</a> on the <a href="http://seamframework.org/Community/SeamUsers">Seam Forums</a>, I decided I just had to share it.

<blockquote>Dan,
<br /><br />
I have never written a book review before but am unable to contain myself in this case. I bought the pre-release version from Manning out of desperation to make head or tails out of what I have been doing with Seam, JSF, EJB etc.
<br /><br />
My perspective may be a bit different in that I am relatively new (7-8 months) to web development of any sort and fairly new to Java. The learning curve has been steep and painful. Compounded with this is that I am running solo with no peer group to keep me on the narrow path.
<br /><br />
I chose SEAM after an initial web app strictly with JSF, EJB3. I was relieved at how much less code I had to write to accomplish things. However, Seam introduces a whole new set of complexities and some of the nuances have really thrown me.
<br /><br />
The Michael Yuan/Thomas Heute book is a good resource for whetting the appetite. However it covers v1.0 Seam and it fairly introductory.
<br /><br />
What I had been missing and was desperately needing was a full understanding of what is happening under the covers; something that fully connects the dots. Your book does just that! It has eliminated much of the confusion that was besetting me. I having been reading it voraciously since I downloaded it.
<br /><br />
The community will greatly benefit from this resource. I love the thorough, careful, methodical explanations that step you through what is really going one. The abundant charts and graphics expose detail and subtleties that a developer really needs to know.
<br /><br />
This will be my primary goto resource. I hope the example code will be available for download soon.
<br /><br />
Thanks for hitting a home run with this.
<br /><br />- <a href="http://www.seamframework.org/Community/TwoFreeChaptersFromSeamInActionAvailable#comment20624">Andy Conn</a></blockquote>

Writing <a href="http://www.mojavelinux.com/seaminaction">Seam in Action</a> has been intriguing, but extremely laborious for me. Therefore, when I read something like this, it makes it all worthwhile. Hell, it almost brings me to tears, like when gold medalists cry after having dedicated their lives to achieving the Olympic grail.</p>

<p>BTW, if you are wondering what is going on with Seam in Action, I am currently pushing it through copy editing. I am taking my time because I want it to be right.</p>
