---
author: Dan Allen
title: Seam in Action at JavaRanch: Round 2
slug: seam_in_action_at_javaranch_round_2
status: Publish
breaks: false
categories: [Seam in Action]
date: 2008-10-07 13:37:45
keywords: [Seam in Action, JavaRanch, Q&A, forums, giveaway, free book]
---
<p>Now that <a href="http://mojavelinux.com/seaminaction">Seam in Action</a> is finally complete, I'm back for some more Q&A on the <a href="http://saloon.javaranch.com/cgi-bin/ubb/ultimatebb.cgi?ubb=forum&amp;f=83">JavaRanch forums</a>. This is your opportunity to challenge the author on some tough (or not so tough) Seam questions and win a signed copy of the book. The participation last time was great and we just about emptied the water cooler discussing Seam-related topics.</p>
<p>The giveaway starts on Tuesday, October 7th 2008.<br />
The drawing will be held on Friday, October 10th 2008.</p>
</blockquote>
<p>See you at the ranch! And thanks for those that wished me luck on finishing the book during the last promotion. It must have done the trick, because I had the book out only a couple of weeks later.</p>
