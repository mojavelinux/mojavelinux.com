---
author: Dan Allen
title: Metro/MARC, Sorry for the Business
slug: metromarc_sorry_for_the_business
status: Publish
breaks: true
categories: [Business]
date: 2004-08-27 15:04:18
keywords: [metro, MARC, commute, late, inconvenience]
---
I would like to know when the phrase "sorry for the inconvenience" replaced the need to serve customers.  That phrase has been entirely overused lately and has thus utterly lost its resourcefulness.  I am one of the many Washingtonians whom have been rolling over and taking it from Metro/MARC over the last year; that is, until now.  If the people in the D.C. Metropolitan area had any guts at all, they would all tell Metro/MARC to go shove it and go back to their gas guzzling SUVs.  The trains are late, the service sucks, many employees have bad attitudes, and there is ALWAYS an excuse. Absolutely no care is given to what people go through to ride on that system (and I use the term "ride" very loosely).  Oh, and just to thank us, they raise the fares.

Don't get me wrong, I am not one to get political.  Just to prove it to you, I am going to analyze this situation in terms of the business...

<strong>Update:</strong> Now MARC is parking the trains almost a half-mile out from Union Station, making it a sizable walk even for a person with two healthy legs.  I am stunned that the handicap croud isn't crying "foul!"

<strong>Update:</strong> I was stopped this morning an issued a ticket for having my morning coffee in my hand on the Metro.  Goodbye Metro, it really hasn't been all that great.  You broke the camel's back.

MARC recently published a newsletter in which they report that the trains on the Camden Line (Washington to Baltimore) have been on time an average of 75% over the last couple of months.  Let's pause and think about that figure for a moment.  Let's say that when you order a pizza, the restaurant only feels like cooking 3/4 of the pie.  How about if you buy a new house and the builders leave off the roof.  Or, perhaps, you invest in mutual fund and after one year it declines to 75% of the principle.  Perhaps you will be happy to know that your employeer only has funds to pay you 75% of your salary.

What am I trying to say?  Don't make excuses, this is a terrible number.  What it means to us riders is that 1 out of every 4 days, we get home an hour late, if not more.  And of course, whomever is picking us up from the station is also wasting time.  It should just BE ON TIME, PERIOD!  There have been days that I have sat on the train at Union Station for more than an hour AFTER the scheduled departure time.  Then there are days when a delay on the Metro causes me to miss the MARC train at Union Station and, by virtue of the fact that I have to talke a later train, I am late.  And what do we get in return?  Well, that's the part that just makes my day!  "Sorry for the inconvenience."  In fact, what they are really saying is, "We are glad that you are all pushovers and that you will keep riding on this sub-par transit system because you have no other way to get to work nor to get to a job interview, so we will see you tomorrow."

How about this for a solution.  Everytime the train is late, I get to ride for free.  Additionally, the next ride is also free.  What MARC, you are broke?  THEN GO OUT OF BUSINESS!  Let some entrepeneur start up a new company that perhaps will give a damn about its customers!  Perhaps he/she can figure out how to prevent me from getting cancer in 20 years from the air in Union Station.  Until then, my response to "Sorry for the inconvenience" is "Sorry for the business" because it won't be around much longer.

If businesses could only follow one rule, it should be "Make it happen."  As Trump has been know to say, "I hate excuses."  There are no excuses in business.  It either works, or you don't have customers.  None of the apology crap.  I don't care if you have signal problems, track derailments, scheduling problems, problems paying your bills, random (and unconstitutional) security checks, or broken down trains.  Fix the problem!  That's what I pay for.  But I'm not going to continue paying if excuses are continually dished out, regardless of the level of detail.  All I hear is "Blah, blah, blah, we cannot run our business."

I listen to people say, in defense, that the New York transit system is much worse.  So what does that mean, as long as you are not as bad as the worst transit system in the world, you are doing well?  That's a great way to think.  That's probably why this system sucks so much.
