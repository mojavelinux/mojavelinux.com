---
author: Dan Allen
title: Excerpts from the Javascript API
slug: excerpts_from_the_javascript_api
status: Publish
breaks: true
categories: [Javascript]
date: 2004-11-19 07:46:16
keywords: [javascript, DOM, IE, Mozilla, EMCAScript]
---
Programming in Javascript is like trying to hit a moving target.  Below are a few excerpts you would expect to find in the non-existent Javascript API.  I will leave it as an exercise to identify the methods to which I am referring.

<blockquote>Sometimes this method works, sometimes it doesn't work.  Some clients don't even implement this method.  The only way to know if the method is active is to try to run it and see if it does anything.</blockquote>

<blockquote><div>The name of this property is different for just about every client.  On some clients, the same name is used for a completely different purpose.  Refer to the countless charts that exist on the web to help decifer this situation.</div></blockquote>

<blockquote><div>Some runtime environments will causes leaks if calling this method in a particular way since those clients don't implement garbage collection for this type of object.</div></blockquote>

<blockquote><div>Some runtimes will implement security restrictions on this method, causing it to fail, but there is no way to know ahead of time which restrictions are being imposed.  It depends on the decision of the PR department.</div></blockquote>

<blockquote><div>When this method is invoked by the client, it either takes one argument or no arguments.  When it takes no arguments, the caller's state must be retrieved from the global scope.  However, that object is completely different from the one passed as an argument.</div></blockquote>

<blockquote><div>This method returns the dimensions of an object, but often times it returns a value slightly more or slightly less, depending on the client, the rendering mode, and the attributes of the object.  In standards compliance mode, this method may even be located on a completely different object.  To be sure, handle it conditionally by detecting the client version.  However, that is often difficult since some clients will fake their signature.  It is usually necessary to test it a couple hundred times before you see the way it actually works.</div></blockquote>

<blockquote><div>In some cases, the client intepreter may be disabled entirely.  Under this circumstance, it is necessary to handle the problem using a completely different solution.</div></blockquote>
