---
author: Dan Allen
title: Salvage Your Project!
slug: salvage_your_project
status: Publish
breaks: true
categories: [Programming]
date: 2005-06-17 03:03:14
keywords: [presentation, J2EE, enterprise, best practice, third party frameworks]
---
If you are an enterprise developer, particularly one that works with Java, I <strong>highly</strong> recommend watching <a href="http://www.theserverside.com/news/thread.tss?thread_id=34532">Rod Johnson's talk</a> at The Server Side Symposium entitled <em>Why J2EE Projects Fail</em>.  I tend to agree with a lot of his points about what drives a project to start going down in flames, as many of them do.  The talk hits very close to home if you have ever worked on a J2EE project.

For those of you who many not know, Rod Johnson is the original architect and developer of the <a href="http://springframework.org">Spring Framework</a>, which, along with Hiberate, is at the top of the recommended tools to learn right now.  Another strong advocate of these methodologies is Bruce Tate, who recently published a book with a similar spin entited <em>Better, Faster, Lighter Java</em>.

I think it is downright shocking how bad J2EE projects have become (or perhaps always were) and I certainly hope that we can all learn from the experience of these consultants and turn around these massive sinking ships.
