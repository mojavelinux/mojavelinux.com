---
author: Dan Allen
title: Open Source Provides "The Tipping Point"
slug: open_source_provides_the_tipping_point
status: Publish
breaks: true
categories: [Open Source]
date: 2004-10-08 13:34:09
keywords: [the tipping point, annoyances, change, user interface]
---
For a while now, I have been theorizing on a simple approach to making life more enjoyable.  The key is to identify the <a href="http://www.isixsigma.com/dictionary/Low_Hanging_Fruit-562.htm">low hanging fruit</a> and take care of it.  What I now realize is that this theory answers the question of why I feel open source applications are so much more preferable.  It is all about increasing the quality of your experiences.  In short, it is <a href="http://www.mozilla.org/products/firefox/why/">all about YOU</a>!

There are obviously many things that we cannot control in our lives, such as the weather or the traffic.  However, what is not so obvious is that there are many small things that can be fixed, quite easily.  But, for whatever reason, people do not take the time to correct them.

Think hypothetically with me for a moment. How many times have you run over that pothole in your driveway, hit your leg on the corner of your desk, been unable to find a pen while on the phone, or had to juggle two remote controls just to use your TV?  Each of these problems may seem completely trivial, but occurring in sequence, day after day, these problems lead to a general sense of frustration.

By now you are wondering what all this theorizing has to do with open source.  If the conclusion is drawn that it is the small annoyances that wreak havoc on a satisfying life, it is natural to consider an environment in which a large amount of the day is spent, computers and the accompanying software.  Think about all the annoyances that you face each day on your computer.  If you are using Windows, it could be a long list!  I know that when I am forced to use Windows as work, each and every time I have to navigate through "My Computer" to get to my personal directory (because there is no concept of a /home directory), it drives me through the roof!  If you could call Microsoft and tell them that this makes you frustrated, it would be nice.  But they don't care.  Now turn to open source.  Think of all the people sharing all of their annoyances in a single location and then imagine how much better the user interface can be.

I see these changes happening in Mozilla Firefox, Eclipse, Open Office, KDE, and Gnome, to name a few.  Most features of these programs are shared by their commercial counterparts.  However, that last 1% is what makes all the difference in the world.  It is <a href="http://www.gladwell.com/books2.html#q_and_a">the tipping point</a>.  When using these programs, you get the feeling that the program is working with you, rather than against you.  The trouble is, it is difficult to relate the effect of these subtleties because individually they appear trivial and difficult to sell.  However, if you just get someone to sit down and try out the program, the difference becomes evident.  It is a very good feeling.  In fact, this is the very essence of a good product and, in my opinion, a good life.

In closing, I encourage you to try this theory on your own daily existence.  Never settle for something that annoys you.  Either use something else or go out and try to make it better (open source, of course, allows you to play this role).
