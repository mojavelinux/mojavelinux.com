---
author: Dan Allen
title: Ubuntu Redefines Linux
slug: ubuntu_redefines_linux
status: Publish
breaks: true
categories: [Linux]
date: 2006-01-10 19:55:13
keywords: [ubuntu, breezy, just works, out of the box, easy to use]
---
Redefining it by saying that <strong>it just works</strong>! I am back again telling the world how great Ubuntu is because, well, it deserves the praise.  The intentions for Breezy Badger, the latest cut of the Ubuntu distribution, were to make it easy to use and to work right out of the box.  I had heard that it lived up to these expectations and I even had several colleagues in my office come to me, virtually star struck by its aptitude, recounting their pleasant installation experiences.  However, nothing could prepare me for how I felt when I it asked me for my credentials for my (previously non-functional) wireless card 30 seconds after booting my computer with the installer CD.  It almost brought a tear to my eye...not because I didn't think it was possible under Linux, but because I will now long for those long, cold nights googling for answers to obscure kernel messages trying to get something to work.  I am going to have to step back and spend some time actually using the desktop (so that's why we install these operating systems)!

To the Ubuntu team and the entire community, job very, very well done!  I could continue on, raving about how many things work so seamlessly in Breezy, but then I would be sqaundering all the time I get to actually use the darn thing!
