---
author: Dan Allen
title: Nice, FedEx
slug: nice_fedex
status: Publish
breaks: true
categories: [Business]
date: 2005-07-25 01:02:23
excerpt: After purchasing a Canon SD300 camera online, the package I received from FedEx showed up completely mangled, so much so that even the inside box was destroyed.
keywords: [FedEx, destroyed package, online shopping]
---
<img src="/assets/pics/beatup_fedex.jpg" width="480" height="440" alt="Mangled FedEx delivery box" />

Only pictures can do this story justice, but I do have to throw in the kicker.  When the FedEx delivery man handed my wife the <a href="/assets/pics/beatup_fedex2.jpg" title="Mangled FedEx delivery box">box</a>, he said, "Sorry, it got a little beat up."

<strong>Ya think?</strong>

It's pretty ironic that the box contained a camera.  Worse for FedEx.
