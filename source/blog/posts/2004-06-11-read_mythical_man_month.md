---
author: Dan Allen
title: Read Mythical Man Month
slug: read_mythical_man_month
status: Publish
breaks: true
categories: [Technology]
date: 2004-06-11 04:13:21
keywords: [fredrick brooks, mythical man month, project management, software design, unit testing, recommended book]
---
Read this book.  Read it! Put everything else down and read it! I just don't know how else to say it.  And don't think for a second that just because this book happens to be circa the internet (aka "old" in IT years) that its lessons no longer apply.    It contains vital history of the software profession.  Many people seem to think that just because computers weren't as capable in the past as they are today that somehow different forces are in effect.  These people couldn't be more naive.  The forces at work in the software community are as timeless as gravity, with much the same strength.

When I first began reading this book, I commented how much I enjoyed reading it.  As I progressed a few more chapters, I was suprised I had never come across this manuscript before.  At this point, quite captivated, I started tearing through the book with great interest.  One morning I arrived at my office and proclaimed that all software developers should read the accounts of Fredrick Brooks, project manager of the IBM 360 project.  Once I completed the book, my final conclusion was much more audacious.  Now, I have come to believe that a person should be entitled to call him/herself a software developer without having digested this saga.

Why? What is so great about the story of Fred Brook's project?  The answer.  It has all been done before.  Whatever problem you might be having, whether it be technical or managerial, it has happened before.  We might as well learn from the problems of old rather than standing in the present and pretending like the problems we face are new ones.  "Those who do not remember the past are condemned to repeat it."  The only way to remember the past is to sit down and read about it.  A signficant reason as to why I assimilated so strongly with the words of Brooks is because he was recounting stories which I was witnessing first hand!

The question that remains is why programmers choose to ignore their history while other disciplines study it so meticulously.  It should be no suprise that an author that long predated Brooks coined the term "priesthood" of programmers.  These are people who take perverse pride in their ability to work in machine code using techniques and tricks few others could fathom.  Surely any talented programmer can recall the day where "visual programming" was looked down upon as the art of less skilled craftsman.  In fact, I encoutered this debate just last week at the JavaOne conference.  At the conference, I came to the realization that if I shunned the use of tools (which include IDEs, layout designers, refactoring and code generators) I would soon go the way of the assembly programmer.  Productivity demands the use of high level languages, and high level tools.  While there are certainly limits on the control an IDE should assume, no amount of hand coding can produce the benefits of suiting up one of these environments.

Why would one give up the use of the VIM editor as an entire application management tool?  Following the mantra of JUnit, there are plenty of new issues to master that it makes no sense to beat our heads against old ones.  Let the computer do what the computer does best, automation.
