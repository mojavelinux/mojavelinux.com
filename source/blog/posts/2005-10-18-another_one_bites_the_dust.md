---
author: Dan Allen
title: Another One Bites the Dust
slug: another_one_bites_the_dust
status: Publish
breaks: true
categories: [Technology]
date: 2005-10-18 00:52:48
keywords: [backup, hard drive crash, data loss, storage, rdiff-backup]
---
I was beginning to wonder how many times I had to lose my data before I sat down to setup backups for my computers.  Backing up is sort of like exercise in that everyone says you should do it, but no one ever does.  Ironically enough, once you do it, you feel much better and wonder what was holding you up in the first place.

<strong>UPDATE:</strong> I came across the following quote, which I believe serves as a very fitting introduction to this entry.

<blockquote><div>There are two types of people in the world -- those who backup their data and those who which they had.</div></blockquote>

<a href="http://www.flackstudio.com"><img src="/assets/pics/grim_reaper.png" class="avatar" style="margin-left: 4px;" width="120" height="150" alt="Grim Reaper" title="Image courtesy of flackstudio.com" /></a>Ignore for a moment the fact that I am the Grim Reaper of hardware, as my co-workers now call me.  If I get anywhere near a hard drive, I seem to have this touch of death that just puts the needle to the disk, if you get what I am saying.  Not one, not two, but three hard drives have crapped out on me in the past six months.  In addition, I have had one data-wiping exploit on my webserver and, finally, one "oops" by sourceforge that terminated my project wiki.  And yes, I lost data, but luckily nothing irreplaceable.  The reason for this blog entry is to give you fair warning not to let this happen to you.

<blockquote><div>If you cannot pick up your computer right this minute and drop it into a bath full of water, you don't have good backups.</div></blockquote>

Let me say that another way.

<blockquote><div>If your data isn't backed up right this instant, you should be counting your lucky stars that nothing has happened yet, while at the same time working on a backup script.</div></blockquote>

I recommend <a href="http://www.nongnu.org/rdiff-backup">rdiff-backup</a>.

Oh, and you can be sure that when your data does go, <a href="http://en.wikipedia.org/wiki/Murphy's_law">Murphy</a> will have you by your bits and bytes because in a situation like that, you do stupid things.  Cycling the hard drive one more time ain't going to make it spin right again.  At that point, its nothin' but a noisemaker.  Certain companies make a living off of this disaster and can charge you in the thousands just to get your data back, data which <strong>you could have backed up earlier for free</strong>.  Mind you, I am not a paranoid or skeptical person, but the five times I lost data in the last half year were just enough to teach me a lesson.  Don't think for a second, either, that there is a good time to deal with recovering, or dealing with, lost data.  Hard drives don't work 9 to 5.  Try 3:00 in the morning.

Don't be stupid like I was and wait until serious data loss occurs to setup backups.  It is so easy to get backups running that you will slap yourself for not having done it earlier.  Everything will just feel more organized and you might actually start to clean up some of that home directory to reduce the size of the backups, which I found that I was able to do.

One final note about backups.  Your backup drive is <strong>not</strong> just additional storage.  The label itself <a href="http://en.wikipedia.org/wiki/Backup">implies</a> that it is not the only copy.  In my first attempt at a backup system, I moved all my files onto the backup drive, only to see it fail.  Buy a backup drive with the sole intent of mirroring your data in its original location, and think long and hard about even trusting that drive.
