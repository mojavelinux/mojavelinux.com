---
author: Dan Allen
title: Why do you participate?
slug: why_do_you_participate
status: Publish
breaks: true
categories: [Open Source]
date: 2005-06-15 15:02:49
keywords: [open source, community, contribute, advocacy]
---
I am often asked why I participate in open source software.  Being an advocate of open source, you would think that I would have a quick, concise answer ready to whip out.  Truth is, it is a very tough question for which I don't have a definitive answer.

Recently, I posted the following comment in response to this question on <a href="http://jroller.com/page/BillDudney?anchor=eclipse_community">Bill Dudney's</a> blog:

<blockquote><p>Why do I participate? It seems that whenever I ask myself or try to explain it to someone else, the answer is always different. I have cited reasons such as "sticking it to the man", "freedom to explore", "taking part in something bigger than myself", "making great software better". However, none of these reasons alone really capture the true essence of what it feels like to be a part of an open source community.</p>
<p>Perhaps we do it because no one can take it away from us, and we can share it with whomever we choose. A lot of times we boast that the open source code is better in some way than its proprietary counterpart, but even if the proprietary software were better, we would still stick to the open version. Why? The best things in life are earned, that's why. The journey is more exciting than the destination. People strive to stand out and be the hero. Knowing that the software is great and you played a part in its greatness. I guess you could accomplish this by sending bug reports to a company, but you are still shackled by how much you can contribute. The open source version let's you give however much you are willing to give. Life isn't about money or having it all, it is able having abilities. Open source grows those abilities as well as showcases them.</p>
<p>That certainly isn't the whole answer, and it isn't the only one either. But, I think that parts of what I wrote there could help to understand the motivation behind this grand movement of open source software.</p></blockquote>

It would be interesting to hear from others.  Tell us your story!
