---
author: Dan Allen
title: The Great JAXB API Blunder
slug: the_great_jaxb_api_blunder
status: Publish
breaks: true
categories: [Java]
date: 2006-09-15 23:26:11
keywords: [jaxb, jaxb2, xjc, java bean, xml binding]
---
<p>You've got to hand it to Sun for screwing this one up.  It's one thing to write software that doesn't adhere to a specification when the documentation is as thick as a textbook.  Take, for example, just about anything created by the W3C.  However, it's really bad when it is your own spec that you can't follow, especially when it is the most well known part of it.  That's right, Sun missed by a mile on their own spec when they created the JAXB 2.0 API.  The JAXB 2.0 compiler (XJC) incorrectly uses the prefix "is" rather than "get" when generating the getter method for a <span style="font-family: monospace">java.lang.Boolean</span> property.  While the JavaBean spec states that read methods for primitive booleans can use the alternate "is" prefix, this flexibility does not extend to its boolean wrapper counterpart.</p><blockquote>8.3.2 Boolean Properties<br />
In addition, for boolean properties, we allow a getter method to match the pattern:<br />
<span style="font-family: monospace">public boolean is<PropertyName>();</span><br />
This "is<PropertyName>" method may be provided instead of a "get<PropertyName>" method, or it may be provided in addition to a "get<PropertyName>" method. In either case, if the "is<PropertyName>" method is present for a boolean property then we will use the "is<PropertyName>" method to read the property value.<br />
An example boolean property might be:<br />
<span style="font-family: monospace">public boolean isMarsupial();
public void setMarsupial(boolean m);</span></blockquote><p>Given that JAXB is a code generation framework, and the idea behind code generation frameworks is that the code is to be used "as is" and not modified thereafter, this is a pretty big "oops".  While this issue has been <a href="https://jaxb.dev.java.net/issues/show_bug.cgi?id=131">reported</a>, the response from Sun is "sorry, its too late". </p><blockquote>This behavior is governed by the spec, and unfortunately it's just too late for the spec to change now.<br />
In terms of the user experience, thanks to auto-boxing, I don't think this will be a real issue for people. Is the problem that you are using Introspector and it's missing the property?</blockquote><p>Too late?  Not a real issue?  It's BROKEN.  FIX IT!  I also don't like the naive statement that it probably won't affect frameworks.  Um, yes it will, considering other projects did happen to adhere to the spec (hibernate, spring, myfaces, etc.)<br/>
<strong>UPDATE:</strong> Stevo Slavic informed me that this has been fixed in JAXB 2.1.13. See <a href="http://java.net/jira/browse/JAXB-131">JAXB-131</a> for details. Yeah!</p>

Of course, this blunder brings several issues to the table.  For one, it proves the evilness of getter/setter methods and for so called "convenience" naming prefixes that, in the end, just create more work for framework builders.  When the "generate getters/setters" option showed up in Java IDEs, it should have been a rather obvious indication that it was time to rethink the problem.  It also shows, once again, what a dangerous idea it was to separate primitives from objects in the language.  More importantly, however, it emphasizes how counterproductive specs can be and why frameworks such as Spring come to life.  If a spec is broken, the response should not be "oh well, too late".  Rather, it should be treated as the vermin it is, and eradicated immediately.

I do want to end on a positive note, given that this entry has been a tad negative.  JAXB 2.0 is a great framework and makes working with XML data structures a very restful process.  I especially like the use of annotations to offer POJO simplicity to the generated classes.  I previously wrote about <a href="http://www.mojavelinux.com/blog/archives/2005/11/jibx_hibernate_for_xml/">how I loath code generation frameworks</a>, but this is definitely one I can handle...even if I am having to deal with a rather annoying bug at the current moment.
