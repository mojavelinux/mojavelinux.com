---
author: Dan Allen
title: Feast your eyes on all chapters!
slug: feast_your_eyes_on_all_chapters
status: Publish
breaks: false
categories: [Seam in Action, Java, Seam]
date: 2008-03-18 12:41:44
keywords: [Seam in Action, JBoss Seam, security, CAPTCHA, Ajax, PDF, email, business process, jBPM]
---
<p>I am thrilled (and tremendously relieved) to announce that the full manuscript of <a href="http://www.amazon.com/gp/product/1933988401">Seam in Action</a> has been released through the <a href="http://www.manning.com/affiliate/idevaffiliate.php?id=647_105">Manning Early Access Program</a> (MEAP). This release entails a completely revamped <a href="http://www.manning-source.com/books/dallen/meap_dallench1.pdf">first chapter</a> and editorial improvements through chapter 7 based on the feedback from a very dedicated group of reviewers. I am expecting to have at least one more MEAP release that incorporates the feedback into the remaining chapters of the book.</p>

<p>If you are one of those folks who crave in depth information about Seam right now are willing to absorb the rough spots, then your wait is over! As one reviewer put it, "You did an amazing amount of work on this book." Yes, I agree.</p>

<p>For those of you who demand that last mile to be completed before reading it, you won't have to wait too much longer. The book has officially entered into production! I am dedicating what little energy I have left, supplemented by my relief team at Manning, to complete this book and get it into your hands before the lazy days of summer arrive.</p>

<p><em><strong>Note:</strong> Keep in mind that these are early release chapters. They have not yet been through the rigorous technical and copy editing process that you can expect to be done for the final manuscript.</em></p>

<p><em>So what's new?</em></p>

<p><strong>Chapter 11</strong> shows how quickly you can weave security into a Seam application. At the most basic level, you only have to write a single method on a POJO class and you have both authentication and role-based authorization. You can block off access using either annotations or a special EL function. Going deeper, you are given a crash course on the Drools rule engine and then learn how to use it to add rule-based security at the page, business method, and entity levels. Finally, to keep out those pesky spammers and bots, you learn that CAPTCHA is virtually a zero effort integration in a Seam application.</p>

<p>Security is critical, but it can be dry for some. But everyone loves Ajax! <strong>Chapter 12</strong> demonstrates the two flavors of Ajax that you can use in a Seam application. The first honors the JSF component model by using Ajax-enabled UI components from the RichFaces/Ajax4jsf library. These components allow you to adopt Ajax without having to suffer from cross-browser JavaScript and CSS nightmares that keep you up at night. If you would rather do things your own way and prefer to navigate around the JSF component model, you can interact directly with Seam components on the server-side using the JavaScript Remoting library. This approach is best if you are planning on designing a single-page application, perhaps even using GWT as a front-end rather than JSF.</p>

<p>If the last three chapters are a full course meal, then <strong>chapter 13</strong> is definitely dessert. In this chapter, you discover how to make your application pop. It starts by learning how to accept file uploads with a simple JSF input binding. You then discover the versatility of Facelets templates by using them create and serve PDF documents and compose and send email messages with attachments. You round it off by learning what themes and i18n messages have in common and how they can be used to customize the look and feel of your application.</p>

<p>That is where the book ends and the bonus online material picks up. <strong>Chapter 14</strong> takes you into the world of business processes and shows you that they aren't nearly as scary as they seem. In fact, you learn in this chapter that if you are on FaceBook, you may be playing a role in a business process more often then you think. When was the last time you added a friend? Seam treats business processes with the same consistent approach that is used across the framework and therefore they aren't any more complex than single-user conversations. If you want to see what the excitement is all about, start with this chapter first.</p>

<p>You already saw <strong>chapter 15</strong> as part of the last release, which details how to integrate Seam and Spring using Seam's Inversion of Control (IoC) bridge. For those of you who won't pick up another framework if you have to let go of Spring, this is definitely the chapter for you.</p>

<p><a href="http://www.manning.com/affiliate/idevaffiliate.php?id=647_105">Enjoy your feast</a>!</p>
