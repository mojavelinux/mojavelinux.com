---
author: Dan Allen
title: Destination: Seam in Action
slug: destination_seam_in_action
status: Publish
breaks: false
categories: [Seam in Action, Java, Seam]
date: 2008-02-05 00:28:35
keywords: [Seam in Action, Seam, Amazon.com, Manning]
---
<p><a href="http://www.amazon.com/gp/product/1933988401"><img src="/assets/gfx/seaminaction.png" alt="Seam in Action" class="avatar" border="0" align="left"/></a>Hey there! My name is Dan and I'll be your author, guide, and teacher on your journey through the vast landscape of <a href="http://www.seamframework.org">JBoss Seam</a>, which I present to you in my book, <a href="http://www.manning.com/affiliate/idevaffiliate.php?id=647_105">Seam in Action</a>. You are going to see exciting features like CRUD generation, components, contexts, bijection, conversations, workspaces, stateful page flows, JavaScript remoting, PDF creation and business processes. The list goes on. It's going to be a fun and informative adventure.</p>

<p>In your hands, you each have something called the EL. This device, or more accurately an expression language, gives you the power to access any of the components in Seam whenever you want them. The creators call it the glue that holds Seam together. It works by putting the name of a context variable between #{ and } and clicking to evaluate. The result is an instance of a Seam component. You can also append a path expression to the context variable. In that case, the possibilities are endless. Use it wisely.</p>

<p>I know that you have a lot of choices when it comes to technologies, and from there, additional choices when it comes to which book to read to learn about that technology. JBoss Seam is worth knowing because it touches on nearly every aspect of Java EE, yet manages to make every facet remarkably accessible. But, even if you are on the fence about Seam, I promise you that this book will be entertaining, informative, and worth the time you spend reading it. I don't just have a knack to inform, I have also been told I have the ability entertain (at least in prose).</p>

<p>Make plans today to read <a href="http://www.amazon.com/gp/product/1933988401">Seam in Action</a>. If it isn't yet available in print when you make those plans, join the <a href="http://www.manning.com/affiliate/idevaffiliate.php?id=647_105">Manning Early Access Program</a> to get your mouse on the electronic pre-release version. When you are done reading this book, you will each get a certificate in the form of knowledge and expertise that you can take with you on your next job interview or contract.</p>
<p style="font-size: smaller;">This post is syndicated from <a href="http://www.amazon.com/gp/blog/post/PLNK3KQSDA96PTNHF">Dan Allen's Amazon Blog</a>.</p>
