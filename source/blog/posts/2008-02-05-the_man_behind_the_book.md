---
author: Dan Allen
title: The man behind the book
slug: the_man_behind_the_book
status: Publish
breaks: true
categories: [Seam in Action]
date: 2008-02-05 00:08:21
keywords: [AmazonConnect, Amazon.com, blog, Seam in Action]
---
<p>I am fortunate to be publishing a book during a very progressive time for Amazon.com. I have just established my <a href="http://www.amazon.com/gp/pdp/profile/A194EA6FA6IEU1">AmazonConnect profile</a> and my <a href="http://www.amazon.com/gp/blog/A194EA6FA6IEU1">Amazon.com blog</a> to accompany my upcoming book, <a href="http://www.amazon.com/gp/product/1933988401">Seam in Action</a>. Entries posted to that blog appear on the Seam in Action product page at Amazon.com and are also available as an RSS feed. As an author, it is pretty amazing to have the ability to contribute content to the book's product page.</p>

<p>I like to think of the AmazonConnect program as "the other half of the story," going on the premise that all stories have two sides. Up until recently, readers have had there chance to laud or tear up a manuscript by posting a review. This feedback helps other potential readers tremendously with the decision to invest time and money into the book. Unfortunately, this situation leaves the author without a voice (aside from the manuscript itself). The AmazonConnect profile gives the author a chance to chime in, but also provides a way for the author to reach out to readers and reveal "the man (or woman) behind the book." I invite you to discover the author, Dan Allen, by subscribing to my Amazon.com blog. I will be posting topics there in addition to writing posts here on mojavelinux.com, so be sure to subscribe to both feeds.</p>
