---
author: Dan Allen
title: The open source ideals espoused on Inauguration Day
slug: the_open_source_ideals_espoused_on_inauguration_day
status: Publish
breaks: false
categories: [Open Source]
date: 2009-01-21 03:15:51
keywords: []
---
<p>As were many, I was moved by the passion of Inauguration Day 2009 in Washington. But one phrase, in particular, from President Obama's speech resonated especially strong with me because I feel it captured the principle of which a majority of us believe in as open source developers and supporters (substitute public dollars with community-developed software):</p>

<blockquote>And those of us who manage the public's dollars will ... do our business in the light of day - because only then can we restore the vital trust between a people and their government."</blockquote>

<p>The words that are not said, but somehow implied, is that by being open, you mobilized an enormous workforce to assist, guide, and support your effort, thus making your cause even stronger.</p>

<p>One can't help but to wonder what government would be like if it were run on mailing lists, IRC chats, and web forums instead of behind closed doors. Time will tell whether Obama will steer the US government in that direction, or perhaps a more suitable variant. There's no doubting his intent that he is going to try. In fact, <a href="http://www.whitehouse.gov/blog/change_has_come_to_whitehouse-gov/">the first post</a> on the new whitehouse.gov blog states this goal quite candidly.</p>
