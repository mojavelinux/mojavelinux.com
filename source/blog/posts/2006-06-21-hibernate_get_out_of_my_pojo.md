---
author: Dan Allen
title: Hibernate, Get Out of My POJO!
slug: hibernate_get_out_of_my_pojo
status: Publish
breaks: false
categories: [Java]
date: 2006-06-21 01:09:26
keywords: [hibernate, lazy loading, lazy fetching, cglib proxy, object graph]
---
<p>If you do a Google search on <a href="http://www.hibernate.org">Hibernate</a>, you are bound to find a rather large bag of posts on the infamous lazy loading exception that Hibernate produces as part of its <a href="http://www.javalobby.org/java/forums/t20533.html">lazy fetching strategy</a>.  The output of this exception is typically in the form:</p>
<p><strong>org.hibernate.LazyInitializationException: failed to lazily initialize a collection - no session or session was closed</strong></p>
<p>A majority of the time, the error is on the part of the programmer, who did not take the time to understand the Hibernate API and attempted to perform an illegal operation given the resources that were available to Hibernate at the time of the call.  Most of the time, <em>but not all of the time</em>.</p>
<p>In fact, my colleagues and I have come across a number of instances in our current project when we need to purge all Hibernate-specific references from our POJO.  In this particular case, our goal is to get back to the native collections and object references so that client code can walk the object, processing the current set of data that it offers.  We don't want to risk the chance of bumping into this nagging exception when we no longer have a need for its services.</p>

<p>Before I continue to explain our use case, I want to quickly go over some background that will help to clarify our problem.</p>
<p>In order to provide a clean <a href="http://en.wikipedia.org/wiki/Plain_Old_Java_Object">POJO programming model</a>, Hibernate hides itself inside of your POJO by using either its own implementation of the JDK collections or by using a CGLIB proxy to surround an object reference, depending on the type of association it is managing.  Since object graphs can be quite large, and in some cases infinite, it is mandatory to draw the line somewhere when loading an object and claim these associations as <em>lazy</em>.  This deferral means that at some later point, it may be necessary to fetch the associated objects from the database when this line is crossed.</p>
<p>It would be impossible for a pure POJO to instrument this functionality since java relationships do not talk the language of database connections.  Thus, what Hibernate does for you is to "enhance" your objects by using "persistent" collections and proxies to make them aware of the database, without tying them in any way to the Hibernate API (the object instances still implement the same interfaces).  Of course, if one of these lazy lines is crossed (and was not previously initialized), it is mandatory that a Hibernate session be open or else that injected code is going to fail when it attempts to use the session it references.  These programming errors are actually quite easy to solve by either redesigning the data access layer or settling for the <a href="http://www.hibernate.org/43.html">OpenSessionInView</a> solution.</p>
<p>Our use case is a bit different.  Once we retrieve an object using the Hibernate API and initialize any lazy collections needed, we then want to be able to detach this object graph from Hibernate's management.  To our excitement, Hibernate does offer a method in its API to detach an object, but the devil is in the details.  The method that Hibernate offers merely informs the hidden code in the POJO that it should no longer consider itself a part of the current session, and thus allows the object to be reconnected with either the same or perhaps a different Hibernate session.  However, the hidden code is still present all the while.  Attempting to access uninitialized lazy collections once detached will still throw up the lazy initialization exception.  This functionality is not quite what we want.  In short, we need to completely cleanse Hibernate from our POJO so that we are left with an object that has absolutely no Hibernate signature left in it.</p>
<p>Let's look at why we need this functionality.  In our application, we need to be able to pull data out of the database using Hibernate and then export that object's data so that it can be shipped off to another system.  The encoded format we use is XML, so we choose to use a tool such as XMLEncoder or <a href="http://xstream.codehaus.org">XStream</a>.  The trouble was, every time we attempted to use either of these tools, we either received lazy loading exceptions or ended up with a bunch of Hibernate-specific class names in the output (such as PersistentSet).  Due to the complexity of our graph, and considering all the circular references, simply cloning was not enough since each object held nested references which themselves were Hibernate-specific implementations of the interfaces.</p>
<p>I am relieved to say that a solution has arrived!  Finally, after many, many nights of banging my head against the screen, I experienced a breakthrough that would lead to the solution to this problem.  I recognized that by working recursively through the object graph with a map of previously-seen references, it would be possible to reconstruct the graph as a clone of the original without any fingerprint of Hibernate left behind.  I call this operation "exporting" the object.  Once this export is complete, any serialization tool will be able to traverse the graph without fear of triggering a lazy loading exception.</p>
<p><pre>
public MyObject export( Map&lt;String, Object&gt; references ) {
  String refId = ExportUtils.buildRefId( this );
  if ( references.containsKey( refId ) ) {
    return (MyObject) references.get( refId );
  }

  // perform a shallow clone (no references attached)
  MyObject export = shallowClone();
  references.put( refId, export );

  // now we cleanse the references and collections
  List&lt;Child&gt; childrenExport = new ArrayList&lt;Child&gt;();
  for ( Child child : children ) {
    childrenExport.add( child.export( references ) );
  }
  export.setChildren( childrenExport );
  export.setRelatedObject( relatedObject.export( references ) );

  // and so on...
}</pre></p>
<p>Of course, this process could be done automatically using reflection, but this snippet is simply pseudocode that seeks to demonstrate the spirit of the process.  I could have also used custom converters on XStream.  In our case, we choose to step through the objects manually for other reasons.  The point that I am trying to get across is that you should be aware of the presence of these custom collections and proxies and be ready to address them in the case that you need to get back to the pure POJO.</p>
