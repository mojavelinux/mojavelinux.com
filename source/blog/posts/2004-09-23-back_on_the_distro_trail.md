---
author: Dan Allen
title: Back on the Distro Trail
slug: back_on_the_distro_trail
status: Publish
breaks: true
categories: [Linux]
date: 2004-09-23 00:44:32
keywords: [linux, SuSE, install, wireless, laptop]
---
The itch has been growning stronger and stronger over the last couple of months for me to toy with a Linux installation and finally I had to opportunity to return to one of my favorite pasttimes, Linux Distro Hunting.  One of the main reasons I started using Linux was to experience something new and different.  So much uncertainty, yet so much to achieve.

I struggled with this decision for a while since there are so many excellent Linux distributions available, but I finally decided on SuSE 9.1 Personal <i>(available for download at <a href="http://linuxiso.org/distro.php?distro=2">linuxiso.org</a>)</i>.  I have always been a Mandrake Linux guy and I don't regret sticking with them for so long.  But I was really starting to get bored with the monoculture that was developing in my little server garden and I needed a few fresh faces.  The target machine was my old, old Quantex laptop.  The goal was to get Linux up and running on it with wireless access so that it can be used for recipes in the kitchen.  The PII processor doesn't leave much room for anything else anyway.

As <a href="http://dmawww.epfl.ch/roso.mosaic/dm/murphy.html">Murphy</a> would have it, I ended up waiting half the night downloading the SuSE 9.1 CD image only to discover that my CD-ROM drive no longer worked.  What else do you expect from a 6 year old laptop?  The only option left was to install it from FTP.  Fortunately, doing this with SuSE is extremely easy.  Less than a day later (perhaps I could have used a better mirror), I had SuSE up and running with only the wireless card to configure.  To my delight, SuSE recognized the card and configuration was pretty straightforward using YaST.  Going into the install I was a bit worried that I would be too unfamiliar with SuSE's configuration to setup a wireless card without delay, but the chameleon eased my pain.  The structure of /etc/sysconfig/network resembles Mandrake's setup, so finding the necessary files was only a matter of scoping out the territory.

I definitely won't claim that the SuSE installation is easier than Mandrake's.  I recognize the flexibility SuSE is adding here, but for a first time installer, it was somewhat high maintanance.  However, YaST really does live up to the hype.  It seems that no stone is left unturned in terms of configuration.  If SuSE keeps this up, they just might have found another convert.

The outcome of this distro hunt was very successful.  Naturally I had to rule out any LiveCD distros because of the bum CD drive.  SuSE 9.1 was a perfect choice because it is small (the equivalent of one CD), it has recent software (kernel 2.6, wireless modules), and it is user friendly (an important factor considering it is used as a shared computer).  I am going to be cheap and give it "two thumbs up" like Siskel and whatever living sidekick he is with now.
