---
author: Dan Allen
title: Remote Debugging with Jetty
slug: remote_debugging_with_jetty
status: Publish
breaks: false
categories: [Java]
date: 2007-03-07 00:58:48
keywords: [jetty, maven 2, remote debug, remote debugging, timeout, eclipse]
---
<p>Jetty is one of those fantastic tools which seems to lack readily available information.  Specifically, I am talking about remote debugging.  Now that Jetty works so nicely with Maven 2, it is almost a no-brainer to use it to run your application right from the project tree.  If you go down that path, you won't stray far before you start trying to figure out how to enable remote debugging.  That is where things get a little tricky.</p>

<p>Before we go there, let's take a look at the Maven 2 plugin configuration for running jetty.</p>
<pre>&lt;plugin&gt;
  &lt;groupId&gt;org.mortbay.jetty&lt;/groupId&gt;
    &lt;artifactId&gt;maven-jetty-plugin&lt;/artifactId&gt;
    &lt;dependencies&gt;
      &lt;dependency&gt;
        &lt;groupId&gt;org.apache.geronimo.specs&lt;/groupId&gt;
        &lt;artifactId&gt;geronimo-j2ee_1.4_spec&lt;/artifactId&gt;
        &lt;version&gt;1.1&lt;/version&gt;
    &lt;/dependency&gt;
  &lt;/dependencies&gt;
  &lt;configuration&gt;
    &lt;connectors&gt;
      &lt;connector implementation="org.mortbay.jetty.nio.SelectChannelConnector"&gt;
      &lt;port&gt;8080&lt;/port&gt;
      &lt;maxIdleTime&gt;3600000&lt;/maxIdleTime&gt;
      &lt;/connector&gt;
    &lt;/connectors&gt;
    &lt;scanIntervalSeconds&gt;5&lt;/scanIntervalSeconds&gt;
  &lt;/configuration&gt;
&lt;/plugin&gt;</pre>
<p>Now, if you are already familiar with this configuration, I want you to look it over again, rather than proudly skipping it. This time, pay attention to the geronimo dependency and the <span class="code">maxIdleTime</span> setting, because I will be discussing both of them shortly.</p>
<p>Once this plugin configuration is installed in your <span class="code">pom.xml</span> file, running jetty is simply a matter of executing the command <span class="code">mvn jetty:run</span>.  That sure beats having to deploy it to Tomcat or JBoss, especially when you are doing iterative development.</p>
<p>I want to start by mentioning the geronimo dependency.  When deploying your application to a full appserver, the JavaEE APIs are already going to be loaded.  However, on a servlet container like Jetty (or Tomcat), these APIs have to be supplied.  In Tomcat you can stuff the jar file in the shared library directory, but with Jetty running directly from Maven 2, the library has to be supplied at runtime.  The dependency nodes within the Jetty plugin allow you to include these extra libraries, if your application requires them, of course.</p>
<p>Another important setting is the <span class="code">scanIntervalSeconds</span>.  Since Jetty is running directly from the project tree, you would expect it to be able to detect changes.  Even without this setting, any web resource that has changed, such as a JSP, Facelet template, Javascript, CSS, or image file, will automatically be made available to the running application.  With this setting enabled, Jetty will  scan your Java source directories at the interval provided.  If it detects a change in a Java class or Java property file, it will reload the application to update the classpath.  If you do not want this feature, simply set the value to <kbd>0</kbd>.</p>
<p>I promised that I would answer the question about how to remote debug, so here it is.  Unfortunately, because of details regarding how the JVM enables remote debugging, the configuration for enabling it lies outside the scope of Maven 2 and thus the Jetty plugin as well.  Instead, arguments must be supplied to the Maven 2 command itself.  If you have ever setup remote debugging of another Java application, these arguments should look familiar to you.</p>
<pre>export MAVEN_OPTS="-Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,address=4000,server=y,suspend=n"
mvn jetty:run</pre>
<p>As you can see, details about the port and the suspend flag cannot be configured within the <span class="code">pom.xml</span>.  Instead, they are passed to maven using the <span class="code">MAVEN_OPTS</span> environment variable.  I tend to setup a script called <span class="code">run-jetty.sh</span> to handle all of these details for me, because I certainly don't want to be typing them every time I need to kick off jetty in debug mode.  Once invoked, you then hook to the running server using the <a href="http://help.eclipse.org/help32/index.jsp?topic=/org.eclipse.jdt.doc.user/tasks/task-remotejava_launch_config.htm">normal method</a> in your <a href="http://www.eclipse.org">IDE of choice</a>.</p>
<p><strong>Ah, it keeps dying!</strong>  That was my initial reaction when I first began as well.  When debugging with Tomcat, I would routinely leave the debugger hanging at a breakpoint while I leisurely strolled to a <em>long</em> developer meeting.  Upon my return, the debugger would still be waiting patiently for me to allow it to continue.  (It would also greet me as "master"). On the other hand, remote debugging with Jetty wouldn't even stick around long enough to let me inspect the code while I was giving it dedicated attention.  As it turns out, the default timeout in Jetty for idle connections is conservatively low.  That's where the <span class="code">maxIdleTime</span> setting comes into play.  This value is the number of seconds that Jetty will allow you to hang on to a connection idly. You are going to want to bump this up to a really high number, especially since you are likely using Jetty strictly for development purposes.  Allow it to be your servant.  Make it wait.  Unfortunately, there doesn't seem to be a way to set it to an infinite value, but any large number will be sufficient.  Eventually, you have to get back to coding.</p>
<p>Now that the Jetty force is with you, go forth and be productive!</p>
<p><strong>Update:</strong> If you're working under windows, don't put the double quotation marks when you set the MAVEN_OPTS variable. Otherwise the variable will not be taken.</p>
<pre>set MAVEN_OPTS=-Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,address=4000,server=y,suspend=n</pre>
<p>Thanks dux!</p>
