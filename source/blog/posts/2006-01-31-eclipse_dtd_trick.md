---
author: Dan Allen
title: Eclipse DTD Trick
slug: eclipse_dtd_trick
status: Publish
breaks: true
categories: [Java]
date: 2006-01-31 00:50:38
keywords: [DTD, Eclipse, CVS, SVN]
---
Love it or hate it, Eclipse is very insistent on going out to the web to fetch DTDs to validate the active XML file.  When it can't find one, it whines, complains and puts an ugly yellow mark on your file, tagging it as a warning condition.  Perhaps this is a small itch, but I felt like scratching it, so I have included an equally small solution to the problem.

The problem with DTDs is (and has always been?) that they don't necessarily point to actual internet resources.  Authors use DTD URIs as namespaces to keep the elements in one definition totally seperate from ones (perhaps with the same name) in another.  In the early days, it was even encouraged that authors just make up any old URL, as long as it was, and would remain to be, unique.  Well, maybe Eclipse missed that day in class, so we hack on.

The question remains, where can you find the DTD in a consistent location, from whereever you are?  Sure, you can load it into your local catalog, but as soon as you are on another computer, the problem is back.  We want a <strong>permanent</strong> solution.  The source code repository, of course!  These servers are always available, serve up the files without interference from a website rebranding, and better yet, they are already versioned!  <em>Brilliant!</em>  Simply use the "download" link from the web-based CVS/SVN tool that points to the DTD file in the source code repository in your DOCTYPE definition at the top of the file (we have to at least assume the author checked it in!).  I will leave you with an example:

<code>&lt;?xml version="1.0"?&gt;
&lt;!DOCTYPE facelet-taglib PUBLIC
  "-//Sun Microsystems, Inc.//DTD Facelet Taglib 1.0//EN"
  "https://facelets.dev.java.net/source/browse/*checkout*
/facelets/src/etc/facelet-taglib_1_0.dtd"&gt;</code>

<em>* remove the endline in the URL when copying</em>
