---
author: Dan Allen
title: Javascript Logging via Meta
slug: javascript_logging_via_meta
status: Publish
breaks: true
categories: [Javascript]
date: 2005-01-10 23:11:02
keywords: [log4js, javascript, DOM, logging, debugging, alert]
---
Sometimes, it is the simplest ideas that we programmers always seem to overlook.  I cannot count the number of times I have typed some variant of a javascript alert message to debug some pesky event handler or form validator.  Often times, I attempt to run the script before fully understanding the execution path of the debugging code I entered.  As a result, I send the javascript engine into an infinite loop of alerts, forcing me to have to issue an xkill on the browser.

During my late nights burning the midnight oil with javascript, I have come up with various ways to stuff debug messages into the output, such as in the statusbar, the document title, appending to the DOM and of course the infamous alert message.  However, each of these methods have issues and are far from an ideal solution.

What I needed was a consistent, lightweight way to trace the javascript execution paths.  Finally, while driving to work, it came to me.  All this time I have only considered appending to the body of DOM, but never the head.  As soon as I looked up towards the head of the document, the answered seemed to practically fall on me.  Since Mozilla has an embedded viewer for reading the content of a document's META tags, I recognized that I could simply write a log function that uses the DOM to add a new META tag entry for each statement, suffixing the  name attribute with the current line number.

<code>&lt;meta
  name="X-jslog:1"
  content="Hello World! This is your friendly logging service." /&gt;</code>

To view the messages, I can simply go to "View &gt; Page Info" and browse the entries in the Meta section.  It almost seems too perfect.  The only issue is that Mozilla does not dynamically update this list, so it is necessary to close and reopen the Page Info dialog.

I have added the Logger script to the <a href="/wiki/doku.php?id=scriptsandbox">script sandbox</a> page of my wiki, which describes how to configure and use this micro logging framework.  I hope you find this to be useful in the wee hours of the night and relish never having to xkill your browser again because of an infinite alert loop.  Enjoy!
