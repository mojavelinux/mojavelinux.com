---
author: Dan Allen
title: A Better Network File System
slug: a_better_network_file_system
status: Publish
breaks: true
categories: [Linux]
date: 2005-04-19 00:08:24
keywords: [network file system, samba, nfs, shfs, ssh, sharing]
---
After many years of using Linux, there is still remains one problem that I seem to run into at every turn.  <em>Remote file systems</em>.  In short, they <strong>suck</strong>.  Recently, I asked around about this topic because I felt like I was missing something.  I was thinking that perhaps there is some perfectly elegant file sharing protocol that has somehow escaped my radar all these years.  Alas, the responses I got were the expected ones.  "Use samba."  "You can configure NFS."  Forget Samba, forget NFS.  Why?  Because we already have a better way.

The problem with both Samba and NFS is that they require (a lot of) work to get setup right.  There are mappings to deal with, file permissions to configure, and firewalls that try to disrupt the whole episode.  To throw more issues into the mix, there is the problem of compatibility between versions (NFS3 vs NFS4, Samba 2 vs Samba 3).

So if I'm complaining, you would that I have a solution to present.  I do have one, just bear with me.  One of the common responses I get to the question "How do you share files between two Linux computers?" is the terse response "SCP."  Okay, so how much of a stretch would it be to imagine one computer performing an SSH mount to another computer?

As it turns out, one team already has, with <a href="http://shfs.sourceforge.net/">shfs</a>.

<blockquote><div>Shfs is a simple and easy to use Linux kernel module which allows you to mount remote filesystems using a plain shell (ssh) connection.</div></blockquote>

<strong>Brilliant!</strong>  It is the perfect solution since it is already secure, it requires only an account on the remote machine, and all permissions are inherited from the native pam implementation.  Forget portmapper, rpc daemons and flaky netbois broadcasts, just use what has been there all along, the grassroots SSH protocol!
