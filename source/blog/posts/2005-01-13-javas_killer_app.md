---
author: Dan Allen
title: Java's Killer App
slug: javas_killer_app
status: Publish
breaks: true
categories: [Java]
date: 2005-01-13 10:42:50
keywords: [Java, JEdit, text editor, open source, IDE, plugins]
---
Technology has a funny way of failing the first time around.  There is no question that Java is currently going through a renaissance after a very dark age of applets, ugly swing GUIs, and html-infested servlets.  The developers have been hard at work on both the server and the client-side of Java, but there is one application in particular that is deserving of the title "Java's Killer App."  That program is <a href="http://jedit.org/">JEdit</a>, <em>the</em> programmers text editor.  (Finally the age-old VI vs Emacs debate can be put to rest)!

I had planned on writing a nice long blog entry about how great and flexible JEdit is, except that <a href="http://communitymx.com/author.cfm?cid=2144">Thomas Pletcher</a> beat me to it with his article <a href="http://communitymx.com/content/article.cfm?cid=7302C">JEdit 4.2: Cross-Platform Perfection</a>.  Thomas favors the cross-platform aspect of JEdit, which I definitely support.  However, I believe that the strongest feature of JEdit (and of Firefox and Thunderbird) is the plugin (or extension) manager.  This design allows the core application to be lightweight, small and fast, yet gives the user the power to customize the program to create an ideal environment.  Adding extensions also gives the user a sense of ownership since each configuration is unique, just like the person using it.

If you haven't yet witnessed the power and grace of JEdit, be sure to follow the link to the article and then go download JEdit today!
