---
author: Dan Allen
title: MandrakeMove First Impressions
slug: mandrakemove_first_impressions
status: Publish
breaks: true
categories: [Reviews]
date: 2004-01-18 18:57:00
keywords: [MandrakeMove, Mandrake, live cd, linux, review]
---
I want to start off by saying that MandrakeMove is an incredible distribution and I am going to focus on some rather particular points in this review. My hope is to make the community aware of some of the outstanding issues with running MandrakeMove and not to discredit the countless hours Mandrake employees spent on making such a polished product. We all want Linux to succeed and that can only be accomplished by continuing to test, report and ask, <a href="http://www.wisdomquotes.com/001607.html" title='Some men see things as they are and say, "Why?" I dream of things that never were and say, "Why not?"' target="_blank">"Why not?"</a> The lifecycle of linux is like an organism, it has to keep breathing to stay alive.

<a href="http://www.mojavelinux.com/articles/mandrakemove.html" target="_blank">View Article</a>

<a href="http://www.pclinuxonline.com/article.php?sid=8308">View post at pclinuxonline.com</a>
