---
author: Dan Allen
title: Seamless JSF
slug: seamless_jsf
status: Publish
breaks: true
categories: [Tutorials]
date: 2007-04-17 23:43:28
excerpt: JavaServer Faces (JSF) is the first standardized user interface (UI) framework for Java Web applications. JBoss Seam is a powerful application framework that extends JSF. In this <a href="http://www-128.ibm.com/developerworks/java/library/j-seam1/index.html">three-part series</a>, you will discover how the strong chemistry between these two frameworks can streamline your development efforts.
keywords: [JSF, JavaServer Faces, JBoss Seam, EJB 3, Ajax Remoting]
---
JavaServer Faces (JSF) is the first standardized user interface (UI) framework for Java Web applications. JBoss Seam is a powerful application framework that extends JSF. Discover the strong chemistry that these two frameworks share in this <a href="http://www.ibm.com/developerworks/search/searchResults.jsp?searchType=1&searchSite=dW&searchScope=javaZ&query=%22seamless+jsf%22&Search=Search&rankprofile=9">three-part series</a> published on <a href="http://www.ibm.com/developerworks/java">IBM developerWorks</a>.
