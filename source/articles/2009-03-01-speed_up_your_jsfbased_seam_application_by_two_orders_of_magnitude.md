---
author: Dan Allen
title: Speed up your JSF-based Seam Application by Two Orders of Magnitude
slug: speed_up_your_jsfbased_seam_application_by_two_orders_of_magnitude
status: Publish
breaks: false
categories: [Tutorials]
date: 2009-03-01 02:28:41
excerpt: In this <a href="http://jsfcentral.com/articles/speed_up_your_jsf_app_1.html">two-part article</a>, you'll be presented with a series of four lessons for eliminating performance problems in a JSF-based Seam application one-by-one until you have observed a remarkable, two orders of magnitude improvement in performance.
keywords: [JavaServer Faces, performance, Seam, Ajax, RichFaces, data tables]
---
<p>JavaServer Faces (JSF) has a reputation for having poor performance. Some claim that this "runtime tax" is simply the cost of using a component-based abstraction layer. After focused research, I have determined that by following a handful of best practices, you can get your JSF data tables to perform almost as well as hand-crafted HTML while still being able to retain the benefits of developing with an event-driven programming model. In this <a href="http://jsfcentral.com/articles/speed_up_your_jsf_app_1.html">two-part article</a> published on JSFCentral, I identify some performance problems that occur when using JSF UI components, Seam components, and the EL carelessly, and then present, through a series of four lessons, ways to eliminate these problems one-by-one until you have observed a remarkable, two orders of magnitude improvement in performance.</p>
