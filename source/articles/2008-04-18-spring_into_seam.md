---
author: Dan Allen
title: Spring into Seam
slug: spring_into_seam
status: Publish
breaks: false
categories: [Tutorials]
date: 2008-04-18 13:33:02
excerpt: JBoss Seam unifies the Java EE platform under an accessible component-based model. Its reach also extends to Spring. In this <a href="http://www.javaworld.com/javaworld/jw-04-2008/jw-04-spring-seam.html">series</a>, excerpted from <a href="http://www.manning.com/affiliate/idevaffiliate.php?id=647_105">Seam in Action</a>, you'll learn how to leverage the Spring container from Seam and vice-versa.
keywords: [Seam in Action, Spring, JBoss Seam, hybrid component, XML configuration, container]
---
<p>JBoss Seam is an intriguing framework that unifies JSF, EJB 3, JPA, and JAAS under an accessible component-based model. But adopting Seam doesn't mean that you have to lose the advantages of other frameworks, such as Spring. In this series, excerpted from <a href="http://www.manning.com/affiliate/idevaffiliate.php?id=647_105">Seam in Action</a> (forthcoming from Manning Publications), author Dan Allen shows you how to leverage the Spring container from Seam and vice-versa.</p>
<p>In Part 1, you'll be introduced to the Spring-Seam hybrid component, which is a managed object that benefits from functionality provided by both the Seam and Spring containers. In Part 2, you'll learn how to infuse state into traditionally stateless Spring beans by allowing them to reside in Seam contexts, and how to inject stateful Seam components into Spring beans. Finally, in Part 3, you'll learn how to integrate Seam and Spring at the most basic level by having them share a persistence manager.</p>
