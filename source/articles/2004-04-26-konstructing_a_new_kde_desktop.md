---
author: Dan Allen
title: Konstructing a New KDE Desktop
slug: konstructing_a_new_kde_desktop
status: Publish
breaks: true
categories: [Tutorials]
date: 2004-04-26 06:03:00
keywords: [KDE, konstruct, linux, desktop, window manager, tutorial]
---
The purpose of this little tutorial is to introduce a better solution to upgrading KDE, one that will allow a user to get a release up and running the night it hits the mirrors, without having to hose the base OS install. "A miracle?" you say. Well...perhaps just the saving grace we have been looking for. The solution is to <strong>Konstruct</strong> a new desktop rather than using RPM or DEB to upgrade the system packages.

<a href="http://www.mojavelinux.com/articles/konstruct.html" target="_blank">View  Article</a>

<a href="http://osnews.com/comment.php?news_id=6836">View Post at osnews.com</a>
