---
author: Dan Allen
title: A Meeting Between a Guru and a Newbie
slug: a_meeting_between_a_guru_and_a_newbie
status: Publish
breaks: true
categories: [Experiences]
date: 2003-01-12 15:42:00
keywords: [newbie, linux, Red Hat, RPM, Gnome, review]
---
Like most linux advocates, I too have that "interested" friend who wants to finally ditch windows (windows xp) and take the Linux "plunge" (as seen by that individual). After debating with myself over which installation to start him out with, I decided to go with RedHat 8.0 simply because it has been classified as one of the most "newbie" oriented distros thanks to the "integrated" look and the superb documentation, and because it has a free download. Was my choice the right one and how did he react to it? Read on to learn exactly how a person who has never seen linux reacts when a long time user attempts to show that person "the ropes."

<a href="http://www.mojavelinux.com/articles/guru_newbie_redhat8.html" target="_blank">View article</a>

<a href="http://www.pclinuxonline.com/modules.php?name=News&file=article&sid=4238">View post at pclinuxonline.com</a>

<a href="http://www.osnews.com/story.php?news_id=2578">View post at osnews.com</a>
