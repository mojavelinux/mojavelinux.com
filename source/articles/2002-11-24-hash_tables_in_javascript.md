---
author: Dan Allen
title: Hash Tables in Javascript
slug: hash_tables_in_javascript
status: Publish
breaks: true
categories: [Tutorials]
date: 2002-11-24 12:57:15
keywords: []
---
Javascript is a prototype based object-oriented language where an objects is a mapping from property names to values. This tutorial demonstrates how to wrap these native objects with an hash table implementation similar to that found in the Java API.

<a href="http://www.mojavelinux.com/articles/javascript_hashes.html" target="_blank">View  Article</a>
